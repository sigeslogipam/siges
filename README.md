Ceci est le fichier README.TXT pour la distribution courante de SIGES

SIGES est diffusé suivant les termes de la Licence Publique Générale GNU (GPL) version 2.0.

Pour toute question ou commentaire sur cette application, vous pouvez contacter les développeurs du projet:

LOGIPAM (info@logipam.com)
Jean Bapstiste Marc (jbmarc@logipam.com)
Metuschael Prosper (mprosper@logipam.com)
Jean Eder Hilaire (jehilaire@logipam.com)
Jean Came Poulard (jcpoulard@logipam.com)

Comment installer SIGES Zotolan (version 2.02)

Windows
Prérequis
Windows 10    57028
Configuration recommandée
WAMP 2.5 avec PHP version 5.6.35 et MySQL version 5.6.17

Étape de l’installation 
1)	Télécharger les fichiers SIGES et décompresser le ZIP. 
2)	Copier le répertoire SIGES décompressé et placer le dans le répertoire C:\wamp\www\  
3)	Ouvrir PhpMyadmin (localhost/phpmyadmin) ou tout autre client MySQL et créer une base de données avec le fichier siges\db\siges_blank.sql pour créer une base de données SIGES vierge ou siges\db\siges_demo.sql pour créer une base de données de démonstration de SIGES. 
4)	Ouvrir le fichier siges\protected\config\db.php 
5)	Changer les paramètres de connexion à la base de données dans le fichier siges/protected/config/db.php

return array(
    'connectionString' => 'mysql:host=localhost;dbname=siges_blank', 
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => ', // Pour Windows avec WAMP 2.5 laisser le « password » vide, mais à adapter suivant votre système d’exploitation et la configuration du serveur de base de données
    'charset' => 'utf8',
);
-	La partie db_name représente le nom de la base de données utilise pour faire fonctionner SIGES. 
-	La partie username représente le nom d’utilisateur de la base de données
-	La partie de password représente le mot de passe de l’utilisateur de la base de données.   
6)	Ensuite ouvrir localhost/nom_du_repertoire_installation pour démarrer SIGES
7)	Utiliser le nom d’utilisateur : admin et le mot de passe admin pour effectuer une première connexion. 








Linux

Linux (Distribution Ubuntu)
Configuration recommandée
Ubuntu version 16.04
Apache version 2.4.18
MySQL version 5.6.16-1
PHP version 5.6.31

Étape de l’installation 
1)	Télécharger les fichiers SIGES et décompresser le ZIP. 
2)	Copier le répertoire SIGES décompressé et placer le dans le répertoire /var/www/html/
3)	Passer les droits d’écriture, de lecture et d’exécution sur le répertoire SIGES décompressé (sudo chmod 777 -R repertoire_siges) 
4)	Ouvrir PhpMyadmin (localhost/phpmyadmin) ou tout autre client MySQL et créer une base de données avec le fichier siges/db/siges_blank.sql pour créer une base de données SIGES vierge ou siges/db/siges_demo.sql pour créer une base de données de démonstration de SIGES. 
5)	Ouvrir le fichier siges/protected/config/db.php 
6)	Changer les paramètres de connexion à la base de données 

return array(
    'connectionString' => 'mysql:host=localhost;dbname=siges_blank', 
    'emulatePrepare' => true,
    'username' => 'root',
    'password' =>'mot_de_passe' , // Adapter suivant votre système d’exploitation et la configuration du server de base de données
    'charset' => 'utf8',
);
-	La partie db_name représente le nom de la base de données utilise pour faire fonctionner SIGES. 
-	La partie username représente le nom d’utilisateur de la base de données
-	La partie de password représente le mot de passe de l’utilisateur de la base de données.   
7)	Ensuite ouvrir localhost/nom_du_repertoire_installation pour démarrer SIGES
8)	Utiliser le nom d’utilisateur : admin et le mot de passe admin pour effectuer une première connexion. 







MAC OS 
Prérequis
Version minimum Yosemite 10.10.2 
MAMP 3.0.7.3 
MySQL version 5.5.38 
PHP version 5.6.2 
Apache 2.2.29 

Étape de l’installation 
1)	Télécharger les fichier SIGES et décompresser le ZIP. 
2)	Copier le répertoire SIGES décompressé et placer le dans le répertoire /Applications/MAMP/htdocs/
3)	Passer les droits d’écriture, de lecture et d’exécution sur le répertoire SIGES décompressé (sudo chmod 777 -R repertoire_siges) 
4)	Ouvrir PhpMyadmin (localhost/phpmyadmin) ou tout autre client MySQL et créer une base de données avec le fichier siges/db/siges_blank.sql pour créer une base de données SIGES vierge ou siges/db/siges_demo.sql pour créer une base de données de démonstration de SIGES. 
5)	Ouvrir le fichier siges/protected/config/db.php 
6)	Changer les paramètres de connexion à la base de données 

return array(
    'connectionString' => 'mysql:host=localhost;dbname=siges_blank', 
    'emulatePrepare' => true,
    'username' => 'root',
    'password' =>'mot_de_passe' , // Adapter suivant votre système d’exploitation et la configuration du server de base de données
    'charset' => 'utf8',
);
-	La partie db_name représente le nom de la base de données utilise pour faire fonctionner SIGES. 
-	La partie username représente le nom d’utilisateur de la base de données
-	La partie de password représente le mot de passe de l’utilisateur de la base de données.   
7)	Ensuite ouvrir localhost/nom_du_repertoire_installation pour démarrer SIGES
8)	Utiliser le nom d’utilisateur : admin et le mot de passe admin pour effectuer une première connexion. 


 
============================================== FIN INSTRUCTION VERSION 2.02 =====================================================================






Ceci est le fichier README.TXT pour la distribution courante de SIGES

SIGES est diffusé suivant les termes de la Licence Publique Générale GNU (GPL) version 2.0.

Pour toute question ou commentaire sur cette application, vous pouvez contacter les développeurs du projet:

LOGIPAM (info@logipam.com)
Jean Bapstiste Marc (jbmarc@logipam.com)
Metuschael Prosper (mprosper@logipam.com)
Jean Eder Hilaire (jehilaire@logipam.com)
Jean Came Poulard (jcpoulard@logipam.com)

Comment installer SIGES Wosiyòl (version 1.0)

Windows
Prérequis
Windows 10    57028
Configuration recommandée
WAMP 2.5 avec PHP version 5.6.35 et MySQL version 5.6.17

Étape de l’installation 
1)	Télécharger les fichiers SIGES et décompresser le ZIP. 
2)	Copier le répertoire SIGES décompressé et placer le dans le répertoire C:\wamp\www\  
3)	Ouvrir PhpMyadmin (localhost/phpmyadmin) ou tout autre client MySQL et créer une base de données avec le fichier siges\db\siges_blank.sql pour créer une base de données SIGES vierge ou siges\db\siges_demo.sql pour créer une base de données de démonstration de SIGES. 
4)	Ouvrir le fichier siges\protected\config\db.php 
5)	Changer les paramètres de connexion à la base de données 

return array(
    'connectionString' => 'mysql:host=localhost;dbname=siges_blank', 
    'emulatePrepare' => true,
    'username' => 'root',
    'password' => ', // Pour Windows avec WAMP 2.5 laisser le « password » vide, mais à adapter suivant votre système d’exploitation et la configuration du serveur de base de données
    'charset' => 'utf8',
);
-	La partie db_name représente le nom de la base de données utilise pour faire fonctionner SIGES. 
-	La partie username représente le nom d’utilisateur de la base de données
-	La partie de password représente le mot de passe de l’utilisateur de la base de données.   
6)	Ensuite ouvrir localhost/nom_du_repertoire_installation pour démarrer SIGES
7)	Utiliser le nom d’utilisateur : admin et le mot de passe admin pour effectuer une première connexion. 








Linux

Linux (Distribution Ubuntu)
Configuration recommandée
Ubuntu version 16.04
Apache version 2.4.18
MySQL version 5.6.16-1
PHP version 5.6.31

Étape de l’installation 
1)	Télécharger les fichiers SIGES et décompresser le ZIP. 
2)	Copier le répertoire SIGES décompressé et placer le dans le répertoire /var/www/html/
3)	Passer les droits d’écriture, de lecture et d’exécution sur le répertoire SIGES décompressé (sudo chmod 777 -R repertoire_siges) 
4)	Ouvrir PhpMyadmin (localhost/phpmyadmin) ou tout autre client MySQL et créer une base de données avec le fichier siges/db/siges_blank.sql pour créer une base de données SIGES vierge ou siges/db/siges_demo.sql pour créer une base de données de démonstration de SIGES. 
5)	Ouvrir le fichier siges/protected/config/db.php 
6)	Changer les paramètres de connexion à la base de données 

return array(
    'connectionString' => 'mysql:host=localhost;dbname=siges_blank', 
    'emulatePrepare' => true,
    'username' => 'root',
    'password' =>'mot_de_passe' , // Adapter suivant votre système d’exploitation et la configuration du server de base de données
    'charset' => 'utf8',
);
-	La partie db_name représente le nom de la base de données utilise pour faire fonctionner SIGES. 
-	La partie username représente le nom d’utilisateur de la base de données
-	La partie de password représente le mot de passe de l’utilisateur de la base de données.   
7)	Ensuite ouvrir localhost/nom_du_repertoire_installation pour démarrer SIGES
8)	Utiliser le nom d’utilisateur : admin et le mot de passe admin pour effectuer une première connexion. 







MAC OS 
Prérequis
Version minimum Yosemite 10.10.2 
MAMP 3.0.7.3 
MySQL version 5.5.38 
PHP version 5.6.2 
Apache 2.2.29 

Étape de l’installation 
1)	Télécharger les fichier SIGES et décompresser le ZIP. 
2)	Copier le répertoire SIGES décompressé et placer le dans le répertoire /Applications/MAMP/htdocs/
3)	Passer les droits d’écriture, de lecture et d’exécution sur le répertoire SIGES décompressé (sudo chmod 777 -R repertoire_siges) 
4)	Ouvrir PhpMyadmin (localhost/phpmyadmin) ou tout autre client MySQL et créer une base de données avec le fichier siges/db/siges_blank.sql pour créer une base de données SIGES vierge ou siges/db/siges_demo.sql pour créer une base de données de démonstration de SIGES. 
5)	Ouvrir le fichier siges/protected/config/db.php 
6)	Changer les paramètres de connexion à la base de données 

return array(
    'connectionString' => 'mysql:host=localhost;dbname=siges_blank', 
    'emulatePrepare' => true,
    'username' => 'root',
    'password' =>'mot_de_passe' , // Adapter suivant votre système d’exploitation et la configuration du server de base de données
    'charset' => 'utf8',
);
-	La partie db_name représente le nom de la base de données utilise pour faire fonctionner SIGES. 
-	La partie username représente le nom d’utilisateur de la base de données
-	La partie de password représente le mot de passe de l’utilisateur de la base de données.   
7)	Ensuite ouvrir localhost/nom_du_repertoire_installation pour démarrer SIGES
8)	Utiliser le nom d’utilisateur : admin et le mot de passe admin pour effectuer une première connexion. 


 



