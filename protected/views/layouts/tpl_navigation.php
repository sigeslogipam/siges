<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

?>
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="navbar-inner">
     
     <div class="container">

        <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </a>


          <!-- Be sure to leave the brand out there if you want it shown -->
          <!--
          <a class="brand" href="#">
                                         SIGES

                                         <small> </small>

                                         </a>
          -->

<div class="nav-collapse">


    <?php
$display_behavior_menu = infoGeneralConfig('display_behavior_menu');
$display_billing_menu = infoGeneralConfig('display_billing_menu');

 $library_display = infoGeneralConfig('library_display');
 
 $display_communication = infoGeneralConfig('display_communication');

 $formalms_module = infoGeneralConfig('display_formalms');
         
 $infirmerie_module = infoGeneralConfig('display_infirmerie');
         
         
 $acad=Yii::app()->session['currentId_academic_year'];
$last_acad=AcademicPeriods::model()->searchLastAcademicPeriod();
                        
                     $dashbord1 = array('label'=>'');
                     $dashboard0 = array('label'=>'');
                  if(isset($last_acad->id) )
                  {
		  	if($last_acad->id==$acad){
                            $dashbord1 = array('label'=>'<span class="fa fa-tachometer" aria-hidden="true"> '.Yii::t('app','Room overview').'</span>', 'url'=>array('/reports/customReport/dashboard','from1'=>'rpt'),'visible'=>!Yii::app()->user->isGuest);
                            $dashboard0 = array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest);
                        }
                      //    else{
			  // $dashbord1 = array('label'=>'');
                          //  $dashboard0 = array('label'=>'');
                       //   }
                  }

if($display_communication==1)
      $communication_menu = array('label'=>'<span class="fa fa-rss" > </span> ','url'=>array('/portal/cmsArticle/index'),'linkOptions'=> array('title' => Yii::t('app','Communication')) );
else
    $communication_menu = array('label'=>'');

// Affiche le module kindergarden
$kindergarden_module = infoGeneralConfig('kindergarden_module');
if($kindergarden_module == 1){
    $kindergarden_menu = array('label'=>'<span class="fa fa-child"> '.Yii::t('app','Kindergarden').'</span>', 'url'=>array('/kindergarden/kinderEvaluation/index?part=conf'),'visible'=>!Yii::app()->user->isGuest);

}else{
    $kindergarden_menu = array('label'=>'');
}

// Ajouter le menu du elearning 
if($formalms_module == 1){
     $formalms_menu = array('label'=>'<span class="fa fa-graduation-cap" >'.Yii::t('app','E-Learning').' </span> ','url'=>array('/formalms/dashboard'),'linkOptions'=> array('title' => Yii::t('app','E-Learning')) );

}else{
     $formalms_menu = array('label'=>'');
}


if($infirmerie_module == 1){
$infirmerie_menu = array('label'=>'<span class="fa fa-stethoscope"> </span>', 'url'=>array('/Infirmerie/recordCase/index'),'linkOptions'=> array('title' => Yii::t('app','Infirmerie'),),'visible'=>!Yii::app()->user->isGuest);

}else{
     $infirmerie_menu = array('label'=>'');
}

if($display_behavior_menu==1)
			  $behavior_menu = array('label'=>'<span class="fa fa-legal"> '.Yii::t('app','Discipline').'</span>','url'=>array('/discipline/recordInfraction/index','mn'=>'std','from'=>'stud'));
			elseif($display_behavior_menu==0)
			   $behavior_menu = array('label'=>'');


if($display_billing_menu==1)
			  $billing_menu = array('label'=>'<span class="fa fa-usd"> '.Yii::t('app','Billings').'</span>', 'url'=>array('/billings/billings/dashboard/part/rec/from/stud'),'visible'=>!Yii::app()->user->isGuest);
			elseif($display_billing_menu==0)
			   $billing_menu = array('label'=>'');

$library_menu = array('label'=>'');
if($library_display==1)
	$library_menu = array('label'=>'<span class="fa fa-book"> '.Yii::t('app','Library').'</span>', 'url'=>array('/guest/grades/index','mn'=>'lbr'),'visible'=>!Yii::app()->user->isGuest);

if(isset(Yii::app()->session['currentId_academic_year'])&&(Yii::app()->session['currentId_academic_year']!=''))
 {
        if(Yii::app()->session['profil_selector']=='emp')
           {
           	     Yii::app()->user->setState('profil', Yii::app()->session['main_profil'] );
           	     //Yii::app()->session['last_profil_selected']= Yii::app()->session['main_profil'];



             }
         elseif(Yii::app()->session['profil_selector']=='teach')
           {
           	        Yii::app()->user->setState('profil', 'Teacher' );
           	         //Yii::app()->session['last_profil_selected']= 'Teacher';

             }



 	   if(isset(Yii::app()->user->profil))

		  {





		  $mail_unread = Mails::model()->getTotalUnreadMails(Yii::app()->user->userid);
		  	   $teacher_id=0;


		  	   if(isset(Yii::app()->user->userid))
                        {
                            $userid = Yii::app()->user->userid;
                        }
                        else
                        {
                            $userid = null;
                        }


		  	 $person_ID=Persons::model()->getIdPersonByUserID($userid);
			   $person_ID= $person_ID->getData();

			     foreach($person_ID as $c)
					$person_id= $c->id;



		  	if(Yii::app()->user->profil=='Admin')
			    {

                 if(isset(Yii::app()->user->name))
		            {

		            if((Yii::app()->user->name=='admin')||(Yii::app()->user->name=='logipam')||(Yii::app()->user->groupid==1))
		              {
		              	 if( (Yii::app()->user->groupid!=1)&&(Yii::app()->user->name!='logipam') )
		              	   {  $this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
			                       $dashboard0,
                                              //  array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

						//array('label'=>'<span class="fa fa-gears"> '.Yii::t('app','School settings').'</span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'visible'=>!Yii::app()->user->isGuest),

			                       $dashbord1,
			                       $kindergarden_menu,
			                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                                $formalms_menu,
			                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                        array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),
                                               // $formalms_menu,    
						            $behavior_menu,

						            $billing_menu,

						            array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                            $infirmerie_menu,
                                                array('label'=>'<span class="fa fa-gears"> </span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'linkOptions'=> array('title' => Yii::t('app','School settings'),),'visible'=>!Yii::app()->user->isGuest),

			                        $communication_menu,
                                                

			                      array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),

			                       array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),

			                        //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
	                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
	                            'visible'=>!Yii::app()->user->isGuest,
	                            'items'=>array(

	                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                                    array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
	                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),
	                            array('label'=>'<span class="fa fa-sign-out"style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

	                        )),

                                

			                    ),
			                ));
		              	   } //end Yii::app()->user->groupid !=1
		              	 elseif( (Yii::app()->user->name=='logipam') )
		              	   {
		              	   	    $this->widget('zii.widgets.CMenu',array(
								'htmlOptions'=>array('class'=>'pull-right nav'),
								'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
								'encodeLabel'=>false,
								'items'=>array(
                                                                           $dashboard0,
									//  array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

									// array('label'=>'<span class="fa fa-gears"> '.Yii::t('app','School settings').'</span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'visible'=>!Yii::app()->user->isGuest),

									  // $dashbord1,
                                                                          $kindergarden_menu,

									//array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                                                        $formalms_menu,
									array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



									   // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

								   array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),
                                                                    
								 //  $behavior_menu,

								//   $billing_menu,

								   array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                                   $infirmerie_menu,
								   array('label'=>'<span class="fa fa-gears"> </span>', 'url'=>array('/help/help/index','mn'=>'sset'),'linkOptions'=> array('title' => Yii::t('app','School settings'),),'visible'=>!Yii::app()->user->isGuest),
                          
                                                                   
									array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
								  array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),
								   //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
							array('label'=>'<span class="fa fa-sign-out" style=""> '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
								'visible'=>!Yii::app()->user->isGuest,
								'items'=>array(

								//-- sou tet preference nan tpl_navigation (developer)
							/*  array('label'=>Yii::t('app','SIGES Billings Tracking'),'url'=>array('/billings/sigespayment/index','year'=>$acad,'part'=>'sig_p','from'=>'oth')),
								*/
								array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),

								array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
								array('label'=>'<span class="fa fa-sign-out"style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

							)),

								),
							));
		              	   	}
						else
		              	   {
		              	   	    $this->widget('zii.widgets.CMenu',array(
		                    'htmlOptions'=>array('class'=>'pull-right nav'),
		                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
							'itemCssClass'=>'item-test',
		                    'encodeLabel'=>false,
		                    'items'=>array(
                                        $dashboard0,
		                        //  array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

		                        // array('label'=>'<span class="fa fa-gears"> '.Yii::t('app','School settings').'</span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'visible'=>!Yii::app()->user->isGuest),

			                       $dashbord1,
                                               $kindergarden_menu,

			                    array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                        $formalms_menu,
		                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                   array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),
                                           // $formalms_menu,
					           $behavior_menu,

						       $billing_menu,

						        array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                                   $infirmerie_menu,
                                        array('label'=>'<span class="fa fa-gears"> </span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'linkOptions'=> array('title' => Yii::t('app','School settings'),),'visible'=>!Yii::app()->user->isGuest),

                                       $communication_menu,
                                        
                                        
		                        array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
			                  array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),
		                       //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                            'visible'=>!Yii::app()->user->isGuest,
                            'items'=>array(

                            //-- sou tet preference nan tpl_navigation (developer)
							array('label'=>Yii::t('app','SIGES Billings Tracking'),'url'=>array('/billings/sigespayment/index','year'=>$acad,'part'=>'sig_p','from'=>'oth')),
							array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),

                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
                            array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),

		                    ),
		                ));
		              	   	}




		                }
		              else
		                {
					       $this->widget('zii.widgets.CMenu',array(
		                    'htmlOptions'=>array('class'=>'pull-right nav'),
		                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
							'itemCssClass'=>'item-test',
		                    'encodeLabel'=>false,
		                    'items'=>array(
                                        $dashboard0,
		                         // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

							    //array('label'=>'<span class="fa fa-gears"> '.Yii::t('app','School settings').'</span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'visible'=>!Yii::app()->user->isGuest),

			                      $dashbord1,
			                      $kindergarden_menu,

			                    array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                        $formalms_menu,
		                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                    array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),
                                           // $formalms_menu,
					            $behavior_menu,

						        $billing_menu,

						             array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                                   $infirmerie_menu,
                                        array('label'=>'<span class="fa fa-gears"> </span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'linkOptions'=> array('title' => Yii::t('app','School settings'),),'visible'=>!Yii::app()->user->isGuest),

		                        $communication_menu,
                                        

								array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
			                        array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),

			                        //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                            'visible'=>!Yii::app()->user->isGuest,
                            'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),
                            array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),


		                    ),
		                ));

		               }



		            }


			      }
			    elseif(Yii::app()->user->profil=='Manager')
			       {
			      	   //get the person ID
			            	$personInfo = Persons::model()->getIdPersonByUserID($userid);

			            	$personInfo = $personInfo->getData();
			            	foreach($personInfo as $p)
			            	 {
			            	 	$personId = $p->id;
			            	 	}

			            	$last_pay_id =0;
					         $month_=0;
					         $year_=0;

					          $result = Payroll::model()->getInfoLastPayrollForOne($personId);
					          if($result!=null)
					            {   $result = $result->getData();
					            	foreach($result as $r)
					            	  { $last_pay_id = $r->id;

					                     $month_=$r->payroll_month;

					                     $time = strtotime($month_);
                                         $year_=date("Y",$time);


					            	  }
					             }


					        if( ($last_pay_id!=0)&&($billing_menu==1) )
					          {

					          	$url = array('/billings/payroll/view/id/'.$last_pay_id.'/month_/'.$month_.'/year_/'.$year_.'/di/1/from/stud');
					          	$billings = array('label'=>'<span class="fa fa-usd"> '.Yii::t('app','Billings').'</span>', 'url'=>$url, 'visible'=>!Yii::app()->user->isGuest);



					           }
					         else
					           {
					           	    $billings = array('label'=>'');

					           	 }





			            	$groupid=Yii::app()->user->groupid;
					                   $group=Groups::model()->findByPk($groupid);
					                    //$group= $group->getData();

					                     //foreach($group as $g)
					                          $group_name=$group->group_name;
					   if($group_name=='Discipline')
					      {
						       $this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                $dashboard0,
			                       // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

                                                 $kindergarden_menu,

			                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                                $formalms_menu,
			                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                        array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),

						            $behavior_menu,

						            $billings,
                                                                    array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                                   $infirmerie_menu,
                                                $communication_menu,
                                                

			                      array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
			                       array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),

			                          //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
	                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
	                            'visible'=>!Yii::app()->user->isGuest,
	                            'items'=>array(
	                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
	                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
	                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),
	                            array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

	                        )),

                                

			                    ),
			                ));


			               }  //end Goup = Discipline
			             elseif(($group_name=='Pedagogie'))
					      {
						       $this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                $dashboard0,
			                      // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

			                        array('label'=>'<span class="fa fa-gears"> '.Yii::t('app','School settings').'</span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'visible'=>!Yii::app()->user->isGuest),

                                                 $kindergarden_menu,

			                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                                $formalms_menu,
			                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                        array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),

						            $behavior_menu,

						            $billings,

			                        array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                                   $infirmerie_menu,
                                                $communication_menu,
                                                

			                      array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
			                      array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),
			                          //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
	                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
	                            'visible'=>!Yii::app()->user->isGuest,
	                            'items'=>array(
	                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
	                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
	                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),
	                            array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

	                        )),

                                 

			                    ),
			                ));


			               }  //end Goup = Pedagogie
			             elseif($group_name=='Kinder')
					      {
						       $this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                $dashboard0,
			                       // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

                                                 $kindergarden_menu,

			                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                                $formalms_menu,
			                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                        array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),

						            $behavior_menu,

						            $billings,

									 array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                                   $infirmerie_menu,
                                                $communication_menu,
                                                

			                      array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
			                       array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),

			                          //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
	                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
	                            'visible'=>!Yii::app()->user->isGuest,
	                            'items'=>array(
	                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
	                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
	                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),
	                            array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

	                        )),

                                 

			                    ),
			                ));


			               }//end Goup = Kinder
                                   else
			              {
						       $this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                $dashboard0,
			                       // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

			                       $dashbord1,
                                                $kindergarden_menu,

			                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                                $formalms_menu,
			                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                        array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),
                                                
						            $behavior_menu,

						            $billing_menu,

						            array('label'=>'<span class="fa fa-bar-chart"> '.Yii::t('app','Reports').'</span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'visible'=>!Yii::app()->user->isGuest),
                                                array('label'=>'<span class="fa fa-gears"> </span>', 'url'=>array('/configuration/academicperiods/index','mn'=>'sset'),'visible'=>!Yii::app()->user->isGuest),
			                       $communication_menu,
                                                

			                      array('label'=>'<span class="fa fa-globe"> </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
			                      array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),
			                          //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
                                               array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
	                            'visible'=>!Yii::app()->user->isGuest,
	                            'items'=>array(
	                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
	                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
	                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),
	                            array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

	                        )),

                                 

			                    ),
			                ));


			               }


			        }
			       elseif(Yii::app()->user->profil=='Billing')
			          {
			          	$this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                $dashboard0,
			                       // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),
			                       /* */

                                                $dashbord1,

			                        $kindergarden_menu,

			                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/academic/postulant/viewListAdmission/part/enrlis/pg/'),'visible'=>!Yii::app()->user->isGuest),
                                                $formalms_menu,

			                        array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Staff').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),



						           // array('label'=>'<span class="fa fa-user"> '.Yii::t('app','Employees').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'emp','mn'=>'emp'),'visible'=>!Yii::app()->user->isGuest),

			                        array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),

						            $billing_menu,

						             array('label'=>'<span class="fa fa-bar-chart"> </span>', 'url'=>array('/reports/customReport/list','from1'=>'rpt','cat'=>'stud'),'linkOptions'=> array('title' => Yii::t('app','Rapports'),),'visible'=>!Yii::app()->user->isGuest),
                                                                   $infirmerie_menu,
                                                $communication_menu,
                                                

							array('label'=>'<span class="fa fa-globe"> '.Yii::t('app','Site web').' </span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Portal'),),'visible'=>!Yii::app()->user->isGuest),
                                                        array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),

			                        //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
	                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                            'visible'=>!Yii::app()->user->isGuest,
                            'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),
                            array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),


			                    ),
			                ));

			          }
			         elseif(Yii::app()->user->profil=='Teacher')
			            {
			            	//get the person ID
			            	$personInfo = Persons::model()->getIdPersonByUserID($userid);

			            	$personInfo = $personInfo->getData();
			            	foreach($personInfo as $p)
			            	 {
			            	 	$personId = $p->id;
			            	 	}
                                         // Detect if the teacher is working on kindergarden
                                                $all_teacher_in_kinder = KinderConcept::model()->findAllByAttributes(array('teacher'=>$personId));
                                                if(empty($all_teacher_in_kinder)){
                                                    $kindergarden_menu = array('label'=>'');

                                                }else{
                                                     $kindergarden_menu = $kindergarden_menu;
                                                }
			            	$last_pay_id =0;
					         $month_=0;
					         $year_=0;

					          $result = Payroll::model()->getInfoLastPayrollForOne($personId);
					          if($result!=null)
					            {   $result = $result->getData();
					            	foreach($result as $r)
					            	  { $last_pay_id = $r->id;

					                     $month_=$r->payroll_month;

					                     $time = strtotime($month_);
                                         $year_=date("Y",$time);


					            	  }
					             }


					        if($last_pay_id!=0)
					          {

					          	$url = array('/billings/payroll/view/id/'.$last_pay_id.'/month_/'.$month_.'/year_/'.$year_.'/di/1/from/stud');
					          	$billings = array('label'=>'<span class="fa fa-usd"> '.Yii::t('app','Billings').'</span>', 'url'=>$url, 'visible'=>!Yii::app()->user->isGuest);



					           }
					         else
					           {
					           	    $billings = array('label'=>'');

					           	 }









			            	$this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                $dashboard0,
			                       // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),
			                        $communication_menu,
                                                $formalms_menu,
                                                array('label'=>'<span class="fa fa-male"> '.Yii::t('app','Teachers').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'teach','isstud'=>0,'mn'=>'teach'),'visible'=>!Yii::app()->user->isGuest),
			                       $kindergarden_menu,
						            array('label'=>'<span class="fa fa-group"> '.Yii::t('app','Students').'</span>', 'url'=>array('/academic/persons/listForReport', 'from'=>'stud','isstud'=>1,'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),//la fe kou pou yo

						           $billings,

								 // array('label'=>'<span class="fa fa-bar-chart"> '.Yii::t('app','Statistics').'</span>', 'url'=>array('/reports/reportcard/generalreport','from1'=>'rpt','cat'=>'stud'),'visible'=>!Yii::app()->user->isGuest),




			                      array('label'=>'<span class="fa fa-globe"> '.Yii::t('app','Portal').'</span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
			                      array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),
									//Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
	                        array('label'=>'<span class="fa " style="font-size:14px;"> '.$mail_unread.' </span><span class="fa fa-envelope" style="font-size:14px;" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                                        'visible'=>!Yii::app()->user->isGuest && $mail_unread!=0,
                                        'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),

                                                        array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),
                        array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                                        'visible'=>!Yii::app()->user->isGuest && $mail_unread==0,
                                        'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),

                                                        array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),


			                    ),
			                ));

			            }
			           elseif(Yii::app()->user->profil=='Guest')
			               {   $contact='';


						       if($display_behavior_menu==1)
								 { $behavior_menu = array('label'=>'<span class="fa fa-legal"> '.Yii::t('app','Discipline').'</span>', 'url'=>array('/guest/recordInfraction/viewParent','mn'=>'std'),'visible'=>!Yii::app()->user->isGuest);
								   $behavior_menu_stud = array('label'=>'<span class="fa fa-legal"> '.Yii::t('app','Discipline').'</span>', 'url'=>array('/guest/recordInfraction/view','mn'=>'std'),'visible'=>!Yii::app()->user->isGuest);
								 }
								elseif($display_behavior_menu==0)
								  { $behavior_menu = array('label'=>'');
								     $behavior_menu_stud  = array('label'=>'');
								  }


					          if($display_billing_menu==1)
								  $billing_menu = array('label'=>'<span class="fa fa-usd"> '.Yii::t('app','Billings').'</span>', 'url'=>array('/guest/fees/index'),'visible'=>!Yii::app()->user->isGuest);
								elseif($display_billing_menu==0)
								   $billing_menu = array('label'=>'');


                                                    if($library_display==1)
                                                            $library_menu = array('label'=>'<span class="fa fa-book"> '.Yii::t('app','Library').'</span>', 'url'=>array('/guest/grades/index','mn'=>'lbr'),'visible'=>!Yii::app()->user->isGuest);
                                                     elseif($library_display==0)
                                                         $library_menu = array('label'=>'');



			               	   if(isset(Yii::app()->user->groupid))
					            {
					                   $groupid=Yii::app()->user->groupid;
					                   $group=Groups::model()->findByPk($groupid);
					                    //$group= $group->getData();

					                     //foreach($group as $g)
					                          $group_name=$group->group_name;
					            if($group_name=='Parent')
					              {
					              	   $contact=null;
					              	   $contact_ID=ContactInfo::model()->getIdContactByUserID($userid);
					              	   $contact_ID= $contact_ID->getData();

					                     foreach($contact_ID as $c)
					                       $contact= $c->id;


              					$this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                    $dashboard0,

				               //        array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),


				                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic info').'</span>', 'url'=>array('/guest/grades/index','mn'=>'aset'),'visible'=>!Yii::app()->user->isGuest),


							           // array('label'=>'<span class="fa fa-list-alt"> '.Yii::t('app','Grade & Homework').'</span>', 'url'=>array('/guest/grades/index','mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),


							            $behavior_menu,

						                $billing_menu,

						                array('label'=>'<span class="fa fa-globe"> '.Yii::t('app','Site web').'</span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Portal'),),'visible'=>!Yii::app()->user->isGuest),


			                        //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
                        array('label'=>'<span class="fa fa-sign-out" style="color: #000" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                            'visible'=>!Yii::app()->user->isGuest,
                            'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword?id='.$userid.'&from=guest')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/guest/contactInfo/view?id='.$contact.'&from=guest')),
							array('label'=>Yii::t('app','Contact Info'),'url'=>array('/guest/contactInfo/viewcontact?id='.$contact.'&from=user')),
                            array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),



				                    ),
				                ));

					              }
					            elseif($group_name=='Student')
					              {  $person_id='';
					                 $contact= '';


					              	 $contact_ID=ContactInfo::model()->getIdContactByUserID($userid);
					              	   $contact_ID= $contact_ID->getData();

					                     foreach($contact_ID as $c)
					                       $contact= $c->id;

					                   $person_ID=Persons::model()->getIdPersonByUserID($userid);
									   $person_ID= $person_ID->getData();

									     foreach($person_ID as $c)
											$person_id= $c->id;

					            $this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                    $dashboard0,

				               //         array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),
                                                        array('label'=>'<span class="fa fa-envelope" style="font-size:14px;" >  ('.$mail_unread.')</span> ','url'=>array('/academic/mails/mailbox/mn/std/from/stud/loc/inb')),


				                        array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic info').'</span>', 'url'=>array('/guest/grades/index','mn'=>'aset'),'visible'=>!Yii::app()->user->isGuest),

							           // array('label'=>'<span class="fa fa-list-alt"> '.Yii::t('app','Grade & Homework').'</span>', 'url'=>array('/guest/grades/index', 'mn'=>'std'),'visible'=>!Yii::app()->user->isGuest),

							            $behavior_menu_stud,

						                $billing_menu,

						                $library_menu,


                                      array('label'=>'<span class="fa fa-globe"> '.Yii::t('app','Site web').'</span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Portal'),),'visible'=>!Yii::app()->user->isGuest),


			                         //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
                        array('label'=>'<span class="fa fa-sign-out" style="" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                            'visible'=>!Yii::app()->user->isGuest && $mail_unread==0,
                            'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword?id='.$userid.'&from=guest')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/guest/persons/viewForUpdate?id='.$person_id.'&from=user')),
							array('label'=>Yii::t('app','Contact Info'),'url'=>array('/guest/contactInfo/viewcontact?id='.$person_id.'&from=user')),

                                                        array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),
                        array('label'=>'<span class="fa " style="font-size:14px;"> '.$mail_unread.' </span><span class="fa fa-envelope" style="font-size:14px;" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                            'visible'=>!Yii::app()->user->isGuest && $mail_unread!=0,
                            'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword?id='.$userid.'&from=guest')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/guest/persons/viewForUpdate?id='.$person_id.'&from=user')),
							array('label'=>Yii::t('app','Contact Info'),'url'=>array('/guest/contactInfo/viewcontact?id='.$person_id.'&from=user')),

                                                        array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),


				                    ),
				                ));


					                }
			               	    elseif($group_name=='Default Group')
					              {

							            $this->widget('zii.widgets.CMenu',array(
					                    'htmlOptions'=>array('class'=>'pull-right nav'),
					                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
										'itemCssClass'=>'item-test',
					                    'encodeLabel'=>false,
					                    'items'=>array(
                                                                $dashboard0,
						                 //      array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),

						                array('label'=>'<span class="fa fa-globe"> '.Yii::t('app','Site web').'</span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Portal'),),'visible'=>!Yii::app()->user->isGuest),
                                                                array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),
			                               //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
		                        array('label'=>'<span class="fa fa-sign-out" style="font-size:14px;" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
		                            'visible'=>!Yii::app()->user->isGuest,
		                            'items'=>array(

		                            array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

		                        )),

						                    ),
						                ));


					                }



					     }


			       }
			    elseif(Yii::app()->user->profil=='Information')
			      {



			                $this->widget('zii.widgets.CMenu',array(
			                    'htmlOptions'=>array('class'=>'pull-right nav'),
			                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
								'itemCssClass'=>'item-test',
			                    'encodeLabel'=>false,
			                    'items'=>array(
                                                $dashboard0,
			                       // array('label'=>'<span class="fa fa-home"> </span>', 'url'=>array('/site/index'),'visible'=>!Yii::app()->user->isGuest),
			                       $kindergarden_menu,
                                                array('label'=>'<span class="fa fa-briefcase"> '.Yii::t('app','Academic settings').'</span>', 'url'=>array('/schoolconfig/calendar/index'),'visible'=>!Yii::app()->user->isGuest),

                                                $formalms_menu,
			                      array('label'=>'<span class="fa fa-globe"> '.Yii::t('app','Portal').'</span>', 'url'=>array('/portal'),'linkOptions'=> array('title' => Yii::t('app','Site web'),),'visible'=>!Yii::app()->user->isGuest),
                                              array('label'=>'<span class="fa fa-question-circle"> </span>', 'url'=>array('/help/default'),'linkOptions'=> array('title' => Yii::t('app','Help'),),'visible'=>!Yii::app()->user->isGuest),

			                      $communication_menu,
                                               
									//Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
	                        array('label'=>'<span class="fa fa" style="font-size:14px;" > '.Yii::app()->user->partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
                                        'visible'=>!Yii::app()->user->isGuest,
                                        'items'=>array(
                            array('label'=>Yii::t('app','My Preferences'),'url'=>array('/users/user/preferences','from'=>'oth','ref'=>'pre')),
                            array('label'=>Yii::t('app','Change password'),'url'=>array('/users/user/changePassword','id'=>$userid,'from'=>'oth')),
                            array('label'=>Yii::t('app','Edit personal Info'),'url'=>array('/academic/persons/viewForUpdate?id='.$person_id.'&from=user')),

                                                        array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

                        )),



			                    ),
			                ));




			    	}








		    }
 }
 else
 {
 	$partname =  '';

 	 if(isset(Yii::app()->user->partname))
 	    $partname = Yii::app()->user->partname;

 	$this->widget('zii.widgets.CMenu',array(
					                    'htmlOptions'=>array('class'=>'pull-right nav'),
					                    'submenuHtmlOptions'=>array('class'=>'dropdown-menu'),
										'itemCssClass'=>'item-test',
					                    'encodeLabel'=>false,
					                    'items'=>array(

						                       			                               //Laisser cette partie inchange au cas ou nous allons implementer quelques choses pour les utilisateurs
		                        array('label'=>'<span class="fa fa-sign-out" style=""> '.$partname.'</span><span class="caret"></span>', 'url'=>array('#'),'itemOptions'=>array('class'=>'dropdown','tabindex'=>"-1"),'linkOptions'=>array('class'=>'dropdown-toggle','data-toggle'=>"dropdown"),
		                            'visible'=>!Yii::app()->user->isGuest,
		                            'items'=>array(

		                            array('label'=>'<span class="fa fa-sign-out" style="color: #000"> '.Yii::t('app','Logout').' ('.Yii::app()->user->name.')'.'</span>', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest),

		                        )),

						                    ),
						                ));


 	}
                ?>


    	</div>





    </div>
	</div>
</div>

<div class="subnav navbar navbar-fixed-top">


    <div class="navbar-inner">
    	<div class="container">


        <!--
           <form class="navbar-search pull-right" action="">

           <input type="text" class="search-query span2" placeholder="Search">

           </form> -->



    	</div>
    	<!-- container -->

    </div>
    <!-- navbar-inner -->
 <div class="rt_academic">
















   <div>

</div>
<!-- subnav -->
