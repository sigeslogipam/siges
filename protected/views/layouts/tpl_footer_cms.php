<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */




$acad=Yii::app()->session['currentId_academic_year'];
        $acad_name=Yii::app()->session['currentName_academic_year'];

	$school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
    $school_address = infoGeneralConfig('school_address');
        //School Phone number
    $school_phone_number = infoGeneralConfig('school_phone_number');

    $school_acronym = infoGeneralConfig('school_acronym');

    $devise_school = infoGeneralConfig('slogan');

    $school_facebook = infoGeneralConfig('facebook_page');

    $school_twitter = infoGeneralConfig('twitter_page');

    $school_youtube = infoGeneralConfig('youtube_page');



?>




	<footer>


		</div>






<div class="subFooter">
 <?php
 
$display_communication = infoGeneralConfig('display_communication');
$display_portal = infoGeneralConfig('display_portal');

if( ($display_communication==1) && ($display_portal==1) )
  {
 
 ?>                           
        <div class="container footerWrap">
          <div class="row">


           <div class="colfoot col-lg-3 col-md-6 col-sm-12">
            <h4><a  href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/contact"> Contact</a></h4>

                <ul class="list-unstyled">
                    
                    <p>
                    <a  href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/contact">
                        <i class="fas fa-envelope-square"></i>
                    Nous &eacute;crire
                    </a>
                        <br/><br/>
                        <i class="fas fa-map-marker-alt float-left"></i> &nbsp;
                      <?php
                          if($school_address!=""){
                              echo $school_address;
                          }else{
                              echo '';
                          }
                      ?>

                    <br>  <br>
                        <i class="fas fa-phone"></i>&nbsp;
                      <?php
                          if($school_phone_number!=""){
                              echo $school_phone_number;
                          }else{
                              echo '';
                          }
                      ?>

  <br>  <br>

                        <i class="fas fa-calendar"></i> &nbsp;
              Heure de cours <br/>
                    <?php

                    $data_ = Shifts::model()->findAll();

                    foreach($data_ as $d){
                        if($d->shift_name!="" || $d->shift_name!=null ){
                            echo $d->shift_name.': ' .$d->time_start.' - '.$d->time_end.'<br/>';
                        }else{

                        }
                        }

                        ?>
               </p>

                </ul>
                <!-- SOCIAL MEDIA PLACE --- KANNKALE WILL CORRECT THEM -->
                  <div class="social_media">
                  <a target="_blank" href="https://facebook.com/<?php echo $school_facebook; ?>"> <i class="fab fa-facebook-square"></i></a>

                  <a target="_blank" href="https://twitter.com/<?php echo $school_twitter; ?>"><i class="fab fa-twitter"></i></a>

                  <a target="_blank" href="<?php echo $school_youtube; ?>"><i class="fab fa-youtube-square"></i></a>

                </div>

               <!-- FIN SOCIAL MEDIA --->

    </div>


         <div class="colfoot col-lg-3 col-md-6 col-sm-12">
               <h4>Liens utiles</h4>
                <ul class="list-unstyled">
                    <!-- A activer quand c'est pret
                     <li>
                        <i class="fas fa-caret-right"></i>
                         <a href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/admission">Formulaire d'admission</a>
                      </li>

                      -->

                         <li>
                             <i class="fas fa-angle-right"></i>
                             <a href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/gallerie">Galerie photos</a>
                        </li>

                        <li>
                             <i class="fas fa-angle-right"></i>
                             <a href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/download">Documents &agrave; t&eacute;l&eacute;charger</a>
                        </li>
                        <li>
                             <i class="fas fa-angle-right"></i>
                             <a href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/allanouncements">Annonces</a>
                        </li>
                        
                        <?php 
                             if(infoGeneralConfig('is_admission_open')==1){
                        ?>
                        <li>
                             <i class="fas fa-angle-right"></i>
                             <a href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/admission?menu=adm">Admission</a>
                        </li>
                        <?php } ?>

                </ul>
            </div>

            <div class="colfoot col-lg-3 col-md-6 col-sm-12">
                 <h4>Menu</h4>
                  <ul class="list-unstyled">

                    <?php if($this->portal_position == "index"){ ?>
                    <li class="souliye">  <i class="fas fa-angle-right"></i> <a href="<?php echo Yii::app()->baseUrl ?>/index.php/portal">Accueil</a></li>
                             <?php } else {?>
                                <li>  <i class="fas fa-angle-right"></i>&nbsp;<a href="<?php echo Yii::app()->baseUrl ?>/index.php/portal">Accueil</a></li>
                             <?php } ?>

                                <!-- Prendre les menus de la base de donnees -->
                                <?php
                                   $model_article = new CmsArticle();
                                   $menu_ = CmsMenu::model()->findAllBySql("SELECT * from cms_menu WHERE is_home IS NULL AND is_publish = 1  AND id IN (SELECT article_menu FROM cms_article WHERE is_publish = 1) ORDER BY menu_position LIMIT 10");
                                   foreach($menu_ as $m){
                                     if(isset($menu)){
                                       if($menu == $m->id){
                                           $class_ = "souliye";
                                       }else{
                                           $class_="";
                                       }
                                       }else{
                                           $class_="";
                                       }

                                       ?>

                                <li class="<?php echo $class_ ?>">
                                    <i class="fas fa-angle-right"></i>
                                    <a href="<?php echo Yii::app()->baseUrl."/index.php/portal/default/articledetails?id=" ?><?= $model_article->getLastArticleIdByMenu($m->id) ?>&menu=<?= $m->id; ?>"><?php  echo $m->menu_label?></a>
                                </li>
                                <?php
                                   }
                                ?>

                  </ul>
              </div>


            <div class="colfoot col-lg-3 col-md-6 col-sm-12">
                <ul class="unstyled">


                                         <span>

                                             <?php if($school_acronym != ""){
                                                 echo $school_acronym;

                                             }else{
                                                 echo '';
                                             }


                                         ?>

                                       </span>


                      <a href="<?php  echo Yii::app()->baseUrl; ?>">
                                    <div id="im_foot">
                        <img src="<?php  echo Yii::app()->baseUrl.'/css/images/school_logo.png'; ?> " alt="">
                                              </div>

                                  </a>

                                      <p>
                      <?php
                                     if($devise_school!=''){
                                          echo $devise_school;
                                     }

                                     ?>
                      </p>


                  </ul>
              </div>


        </div>
                              <!-- Ligne pour afficher devise  -->
            </div>

 <?php
 
    }
 
 ?>   

				<div class="pull-center">
<img src="<?php  echo Yii::app()->baseUrl.'/css/images/sigeslogo.png'; ?> " alt="">
				<?php

					echo Yii::t('app','Fourni par ').'<a href="http://www.logipam.com" target="_new">LOGIPAM</a>. &copy; '.Yii::t('app','All rights reserved.');
			    ?>


				</div>
			</div>
</footer>
