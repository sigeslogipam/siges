<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


$acad=Yii::app()->session['currentId_academic_year']; 
						
  
	
	
	if(isset(Yii::app()->user->userid))
                        {
                            $userid = Yii::app()->user->userid;
                        }
                        else 
                        {
                            $userid = null;
                        }
                        
       if(Yii::app()->user->profil=='Guest')
           {   
                $contact='';
                if(isset(Yii::app()->user->groupid))
                  {    
                    $groupid=Yii::app()->user->groupid;
                    $group=Groups::model()->findByPk($groupid);
                    $group_name=$group->group_name;
                    if($group_name=='Parent')
                     {
                       $contact=null;
                       $contact_ID=ContactInfo::model()->getIdContactByUserID($userid);
                       $contact_ID= $contact_ID->getData();

                         foreach($contact_ID as $c)
                           $contact= $c->id;
                  }elseif($group_name='Student'){
                      $person_ID=Persons::model()->getIdPersonByUserID($userid);
                      $person_ID= $person_ID->getData();
                      foreach($person_ID as $c)
			$person_id = $c->id;
                  }
            }
           }elseif( (Yii::app()->user->name=='_developer_')||(Yii::app()->user->name=='logipam_it')||(Yii::app()->user->name=='logipam')){
               $person_id = "";
               $groupid=Yii::app()->user->groupid;
               $group=Groups::model()->findByPk($groupid);
               $group_name=$group->group_name;
           }
           else{
               $group_name = ""; 
                $person_ID=Persons::model()->getIdPersonByUserID($userid);
		$person_ID= $person_ID->getData();
											                    
                    foreach($person_ID as $c)
			$person_id = $c->id;
           }
                        
             
?>

 <div class="coontainer">
 
 <ul class="nav nav-tabs nav-justified"> 
     
     
     <?php 
        if($group_name==""){
     ?>
       
     <li class="<?php if(isset($_GET['ref']) && $_GET['ref']=="pre") echo "active"; else echo ""; ?>">
         <a href="<?= Yii::app()->baseUrl.'/index.php/users/user/preferences?from=oth&ref=pre'; ?>">
             <?= Yii::t('app','Preferences');?>
         </a>
     </li>
     <li class="<?php if(isset($_GET['ref']) && $_GET['ref']=="") echo "active"; else echo ""; ?>">
         <a href="<?= Yii::app()->baseUrl.'/index.php/academic/persons/viewForUpdate?id='.$person_id.'&ref=per&from=user'; ?>">
             <?= Yii::t('app','Edit personal Info');?>
         </a>
     </li>
        <?php }elseif($group_name=="Student"){ ?>
        <li class="<?php if(isset($_GET['ref']) && $_GET['ref']=="pre") echo "active"; else echo ""; ?>">
         <a href="<?= Yii::app()->baseUrl.'/index.php/users/user/preferences?from=oth&ref=pre'; ?>">
             <?= Yii::t('app','Preferences');?>
         </a>
     </li>
     <li class="<?php if(isset($_GET['ref']) && $_GET['ref']=="") echo "active"; else echo ""; ?>">
         <a href="<?= Yii::app()->baseUrl.'/index.php/guest/persons/viewForUpdate?id='.$person_id.'&ref=per&from=user'; ?>">
             <?= Yii::t('app','Edit personal Info');?>
         </a>
     </li>
        <?php }elseif($group_name=="Parent"){ ?>
     <li class="<?php if(isset($_GET['ref']) && $_GET['ref']=="pre") echo "active"; else echo ""; ?>">
         <a href="<?= Yii::app()->baseUrl.'/index.php/users/user/preferences?from=oth&ref=pre'; ?>">
             <?= Yii::t('app','Preferences');?>
         </a>
     </li>
     <li class="<?php if(isset($_GET['ref']) && $_GET['ref']=="") echo "active"; else echo ""; ?>">
         <a href="<?= Yii::app()->baseUrl.'/index.php/guest/contactInfo/view?id='.$contact.'&from=guest'; ?>">
             <?= Yii::t('app','Edit personal Info');?>
         </a>
     </li>
        <?php } ?>
     
</ul>
     
 </div>
