<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */



$acad=Yii::app()->session['currentId_academic_year'];

						
if($acad!=null){  
							
if(isset(Yii::app()->user->profil))
{   $profil=Yii::app()->user->profil;
   switch($profil)
     {
        case 'Guest':
                  if(isset(Yii::app()->user->groupid))
            {    
                   $groupid=Yii::app()->user->groupid;
                   $group=Groups::model()->findByPk($groupid);
                    
                    
                          $group_name=$group->group_name;
            if($group_name=='Parent')
              {
                 $this->widget('zii.widgets.CMenu', array(
					'activeCssClass'=>'active',
					'encodeLabel'=>false,     
					'activateParents'=>true,
					'items'=>array(
					
					array('label'=>'<span class="fa fa-2y" style="font-size: 23px;">  '.Yii::t('app','Discipline').'</span>', 
					//'linkOptions'=>array('id'=>'menuBillings'),
					//'itemOptions'=>array('id'=>'itemBillings'),
					
					'items'=>array(
						
					    array('label'=>'<i class="fa fa-clock-o"></i> '.Yii::t('app','Attendance Journal'),'url'=>array('/guest/recordInfraction/viewParent')),
					    
					   
					    
					        )),
					
					))); 
                 
               }
              elseif($group_name=='Student')
                { 
                     $this->widget('zii.widgets.CMenu', array(
					'activeCssClass'=>'active',
					'encodeLabel'=>false,     
					'activateParents'=>true,
					'items'=>array(
					
					array('label'=>'<span class="fa fa-2y" style="font-size: 23px;">  '.Yii::t('app','Discipline').'</span>', 
					//'linkOptions'=>array('id'=>'menuBillings'),
					//'itemOptions'=>array('id'=>'itemBillings'),
					
					'items'=>array(
						
					    array('label'=>'<i class="fas fa-stopwatch"></i> '.Yii::t('app','Attendance Journal'),'url'=>array('/guest/recordInfraction/view')),
					    
					    					    
					        )),
					
					))); 
                     
                  
                  }
               
            }

               
               break;
               
          case 'Admin':
                  $this->widget('zii.widgets.CMenu', array(
					'activeCssClass'=>'active',
					'encodeLabel'=>false,     
					'activateParents'=>true,
					'items'=>array(
					
					array('label'=>'<span class="fa fa-2y" style="font-size: 23px;">  '.Yii::t('app','E-Learning').'</span>', 
					//'linkOptions'=>array('id'=>'menuBillings'),
					//'itemOptions'=>array('id'=>'itemBillings'),
					
					'items'=>array(
						array('label'=>'<i class="fas fa-magic"></i> '.Yii::t('app','Migrations'), 'url'=>array('/formalms/dashboard')),
					    
					  // array('label'=>'<i class="fas fa-chalkboard-teacher"></i> '.Yii::t('app','Courses'),'url'=>array('/discipline/recordPresence/admin')),
                                          //  array('label'=>'<i class="fas fa-clipboard-list"></i> '.Yii::t('app','Exams'),'url'=>array('/discipline/recordPresence/admin')),
                                          //  array('label'=>'<i class="fas fa-user-graduate"></i> '.Yii::t('app','E-learning students'),'url'=>array('/discipline/recordPresence/admin')),
                                            array('label'=>'<i class="fas fa-star-half-alt"></i> '.Yii::t('app','E-learning grades'),'url'=>array('/formalms/default/portailGrades')),
                                            array('label'=>'<i class="fas fa-magic"></i> '.Yii::t('app','Mise a jour cours'),'url'=>array('/formalms/default/updateCourse')),
                                            
                                           // array('label'=>'<i class="fas fa-stopwatch"></i> '.Yii::t('app','Special Attendance'),'url'=>array('/discipline/')),
					   // $array_presence_mn,
					   // $array_checkout_mn,
                                            // $array_special_mn,
					        )),
					
					))); 


                 break;
                 
          case 'Manager':
                  $this->widget('zii.widgets.CMenu', array(
					'activeCssClass'=>'active',
					'encodeLabel'=>false,     
					'activateParents'=>true,
					'items'=>array(
					
					array('label'=>'<span class="fa fa-2y" style="font-size: 23px;">  '.Yii::t('app','Discipline').'</span>', 
					//'linkOptions'=>array('id'=>'menuBillings'),
					//'itemOptions'=>array('id'=>'itemBillings'),
					
					'items'=>array(
						array('label'=>'<i class="fa fa-legal"></i> '.Yii::t('app','Infraction record'), 'url'=>array('/discipline/recordInfraction/index')),
					    
					    array('label'=>'<i class="fa fa-clock-o"></i> '.Yii::t('app','Attendance Journal'),'url'=>array('/discipline/recordPresence/admin')),
					   $array_presence_mn,
					   $array_checkout_mn,
                                            $array_special_mn,
                                           // array('label'=>'<i class="fa fa-clock-o"></i> '.Yii::t('app','Special Attendance'),'url'=>array('/discipline/')),
					   
					    
					        )),
					
					))); 


          
                 break;
                 
          case 'Billing':
          
                 break;
                 
          case 'Teacher':
          
                 break;
                 
          }

}//fen issetProfil




}



?>					 