<?php

    /*
     * To change this license header, choose License Headers in Project Properties.
     * To change this template file, choose Tools | Templates
     * and open the template in the editor.
     */

    /*
     * © 2019 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
     *
     * This file is part of SIGES.

     SIGES is free software: you can redistribute it and/or modify
     it under the terms of the GNU General Public License as published by
     the Free Software Foundation, either version 3 of the License.

     SIGES is distributed in the hope that it will be useful,
     but WITHOUT ANY WARRANTY; without even the implied warranty of
     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
     GNU General Public License for more details.

     You should have received a copy of the GNU General Public License
     along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
     *
     */
    $model_article = new CmsArticle();
    ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author" content="">

<title>SIGES - Système Intégré de Gestion Scolaire</title>
<?php
    $baseUrl = Yii::app()->baseUrl;
    // Appel des css
    $css = Yii::app()->getClientScript();
    $css->registerCssFile($baseUrl.'/portal_assets/bootstrap/css/bootstrap.min.css');
    $css->registerCssFile($baseUrl.'/portal_assets/css/custom.min.css');
    $css->registerCssFile($baseUrl.'/portal_assets/css/custom.css');
    $css->registerCssFile($baseUrl.'/portal_assets/css/animate.css');

    
    // Appel des JS

    $css->registerScriptFile($baseUrl.'/portal_assets/bootstrap/js/jquery.min.js');
    $css->registerScriptFile($baseUrl.'/portal_assets/bootstrap/js/bootstrap.min.js');

    // Place this script on the bottom of the page
    Yii::app()->clientScript->registerScriptFile(
                                                 Yii::app()->request->baseUrl.'/portal_assets/js/custom.min.js',CClientScript::POS_END
                                                 );

    $cs = Yii::app()->clientScript;
    $cs->scriptMap = array(
                           //'jquery.min.js'=>false,
                           // 'Chart.min.js'=>false,
                           // 'styles.min.css'=>false
                           //'*.css' => false,
                           //'*'=>false
                           );

    /**
     * chargement des scripts pour les differents themes du portail de siges
     *
     */
    /* A decommenter pour charger les themes de siges */

    $css_theme = Yii::app()->getClientScript();
    $theme_portal = infoGeneralConfig('theme_portail_siges');
    if($theme_portal!=null){
        switch ($theme_portal){
            case 'siges':
                $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_siges.css');
                break;
            case 'zaboka':
                $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_zaboka.css');
                break;
            case 'zoranj':
                $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_zoranj.css');
                break;
                case 'anana':
                    $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_anana.css');
                    break; 
                    case 'kann':
                        $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_kann.css');
                        break;
            case 'seriz':
                $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_seriz.css');
                break;
            case 'frez':
                $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_frez.css');
                break;
                case 'siges light':
                    $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_siges_light.css');
                    break;
            case 'tamaren':
                $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_tamaren.css');
                break;
            default:
                $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_siges.css');
        }
    }else{
        $css_theme->registerCssFile($baseUrl.'/css/css_cms/style_cms_siges.css');
    }

    $css->registerCssFile($baseUrl.'/css/font-awesome.min.css');
    $css->registerCssFile("https://use.fontawesome.com/releases/v5.8.2/css/all.css");
    ?>

</head>
<body id="page-top">

<?php require_once('tpl_navigation_cms.php')?>

<div class="site_min">
<?php echo $content; ?>
</div>

<!-- Require the footer -->
<?php require_once('tpl_footer_cms.php')?>
<!-- End Require the footer -->
</body>

</html>

