<!-- Navigation -->
<nav class="navbar navbar-expand-lg bg-secondary fixed-top text-uppercase" id="mainNav">
<div class="container">

        <a class="navbar-brand js-scroll-trigger" href="<?php echo Yii::app()->baseUrl ?>/index.php/portal">
        <img class="img-fluid" src="<?= Yii::app()->baseUrl.'/css/images/school_logo.png'; ?>" alt=""></a>

        <div class="non_lekol" class="navbar-brand js-scroll-trigger"  >
        <div class="school_name">
           <?php

            $school_name = infoGeneralConfig('school_name');
              echo $school_name;

           ?>
         </div>
        <br/>
         <div class="school_devise">
          <?php  $school_devise = infoGeneralConfig('devise_school');
        echo $school_devise;
          ?></div>

<?php
//  echo $school_name;
  //  echo "<br/>";
  //  echo $school_devise; // Voicie la devise de l'ecole c'est en commentaire paske li ap bay pwoblem retire l nan komante a pou kgade kijan w ka ranje l
    ?>
</div>
<button class="navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
<i class="fas fa-bars"></i>
</button>
<div class="collapse navbar-collapse" id="navbarResponsive">
<ul class="navbar-nav ml-auto">

<?php
 
$display_communication = infoGeneralConfig('display_communication');
$display_portal = infoGeneralConfig('display_portal');

if( ($display_communication==1) && ($display_portal==1) )
  {
 
 ?>
<?php if($this->portal_position == "index"){ ?>
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo Yii::app()->baseUrl ?>/index.php/portal">Accueil</a></li>
<?php } else {?>
<li class="nav-item mx-0 mx-lg-1 ">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger <?php if(!isset($_GET['menu'])){ echo "active"; } ?>" href="<?php echo Yii::app()->baseUrl ?>/index.php/portal">
Accueil
</a>
</li>
<?php } ?>

<!-- Prendre les menus de la base de donnees -->
<?php
    $menu_ = CmsMenu::model()->findAllBySql("SELECT * from cms_menu WHERE is_home IS NULL AND is_publish = 1 AND parent_menu IS NULL ORDER BY menu_position LIMIT 10");
    foreach($menu_ as $m){

        if(isset($menu)){
            if($menu == $m->id){
                $class_ = "active";
            }else{
                $class_="";
            }
        }else{
            $class_="";
        }
        // Si c'est un menu parent (constient des sous menus)
        if($m->is_parent_menu == 1){


            ?>
<li class="dropdown nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger dropdown-toggle <?php if(isset($_GET['menu']) && $model_article->getMenuBySubMenu($_GET['menu'])==$m->id){ echo "active"; } ?>" id="login-trigger" href="#?log=wi"  data-toggle="dropdown">
<?= $m->menu_label; ?>
</a>
<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
<?php
    $sub_menu = CmsMenu::model()->findAllBySql("SELECT * FROM cms_menu WHERE parent_menu = $m->id  and is_publish = 1 ORDER BY id ASC");
    foreach($sub_menu as $sb){
        // Verifie s'il y a des articles liees au menu pour l'afficher
        if($model_article->getLastArticleIdByMenu($sb->id)){
            ?>
<li><a class="dropdown-item" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($sb->id); ?>&menu=<?= $sb->id; ?>"><?= $sb->menu_label; ?></a></li>
<?php
    }else{

    }
    }
    ?>


</ul>
</li>
<?php

    }else{
        if($model_article->getLastArticleIdByMenu($m->id)){
            ?>
<li class="nav-item mx-0 mx-lg-1 <?php echo $class_ ?> active">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger <?php if(isset($_GET['menu']) && $_GET['menu']==$m->id){ echo "active"; } ?>" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($m->id); ?>&menu=<?= $m->id; ?>"><?php  echo $m->menu_label?></a>
</li>
<?php

    }
    }
    ?>




<?php




    }
    ?>
<!-- Fin des menus de la base de donnees -->

<!-- ============================================================================= -->
<!-- On enleve le contact
<?php if($this->portal_position == "contact")  {?>
<li class="nav-item mx-0 mx-lg-1 nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/contact">Contact</a>
</li>
<?php } else { ?>
<li class="nav-item mx-0 mx-lg-1 nav-item mx-0 mx-lg-1"> <a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo Yii::app()->baseUrl ?>/index.php/portal/default/contact">Contact</a>
</li>
<?php  } ?>
-->

<!-- Pour le menu de gestion des admission quand la periode d'admission est active -->
<?php
    if(infoGeneralConfig('is_admission_open')==1){
?>
<li class="dropdown nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger dropdown-toggle <?php if(isset($_GET['menu']) && $_GET['menu']=='adm'){ echo "active"; } ?>" id="login-trigger" href="#?log=wi"  data-toggle="dropdown">
<?= Yii::t('app','Admission'); ?>
</a>
<ul class="dropdown-menu" role="menu" aria-labelledby="dropdownMenu1">
<?php
    $sub_menu = CmsMenu::model()->findAllBySql("SELECT * FROM cms_menu WHERE is_publish = 1 AND is_special = 1  AND menu_label <> 'Accueil' ORDER BY menu_label");
    foreach($sub_menu as $sb){
        // Verifie s'il y a des articles liees au menu pour l'afficher
        
            ?>
<li>
    <a class="dropdown-item" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($sb->id); ?>&menu=<?= $sb->id;?>">
    <?= $sb->menu_label; ?>
    </a>
</li>
<?php
    
    }
 ?>
<li>
    <a class="dropdown-item" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/admission?menu=adm">
        <?= Yii::t('app','Formulaire admission');?>
    </a>
</li>
<?php 
$is_admission_publish = infoGeneralConfig('publish_admission_result');
if($is_admission_publish == 1){
?>
<li>
    <a class="dropdown-item" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/resultatadmission?menu=adm">
        <?= Yii::t('app','R&eacute;sultat admission');?>
    </a>
</li>
<?php } ?>

</ul>
</li>

<?php
    }

?>

<li href="javascript:void(0);" class="icon" onclick="myFunction()">
<!-- <i class="fa fa-bars"></i> -->
</li>

<?php

   }

?>

<!-- ===========================  login navbar ===========================-->
<?php if(Yii::app()->user->isGuest) { ?>
<li class="dropdown nav-item mx-0 mx-lg-1">

<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger dropdown-toggle" id="login-trigger" href="#?log=wi"class="" data-toggle="dropdown"><?= Yii::t('app','Login'); ?></a>

      <ul class="dropdown-menu login" role="menu" aria-labelledby="dropdownMenu1">
              <div class="login-container">
                <li class="nav-item mx-0 mx-lg-1">
              <div class="text-center">

              <div class="login_message">
              <?php
                  $model=new LoginForm;
                  if($this->message)
                  echo Yii::t('app','Please see your the administrator to set new academic year.');
                  ?>
              </div>
              </br>


              <div id="logo"></div>

              <?php
                  $form=$this->beginWidget('CActiveForm', array(
                                                                'id'=>'login-form',
                                                                'action'=>Yii::app()->baseUrl.'/index.php/site/index',
                                                                ));

                  echo $this->renderPartial('/default/login',array(
                                                                   'model'=>new loginForm,
                                                                   //'controller'=>'siteController',
                                                                   'form'=>$form, ));
                  ?>

              <?php $this->endWidget();

                  ?>
              </div><!--loginbox-->
              </li>

              </div>
      </ul>
</li>
<?php }else{ ?>
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo Yii::app()->baseUrl ?>">
<?php echo Yii::app()->user->partname; ?>
</a>
</li>
<li class="nav-item mx-0 mx-lg-1">
<a class="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger" href="<?php echo Yii::app()->baseUrl.'/index.php/site/logout'; ?>" title="<?= Yii::t('app','Logout')?>">
<i class="fa fa-sign-out-alt"></i>
</a>
</li>
<?php } ?>


<!-- ========================= End login in the menu =====================================-->

</ul>
</div>
</div>
</nav>
