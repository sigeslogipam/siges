<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


$acad=Yii::app()->session['currentId_academic_year'];




	if(isset(Yii::app()->user->userid))
                        {
                            $userid = Yii::app()->user->userid;
                        }
                        else
                        {
                            $userid = null;
                        }

                $groupid=Yii::app()->user->groupid;
		$group=Groups::model()->findByPk($groupid);
                $group_name=$group->group_name;

?>

 <div class="coontainer kinder-content">

 <ul class="nav nav-tabs nav-justified">
<?php





         echo '<li class="';  if($this->part == 'eval') echo "active"; else echo "";
         echo '"><a href="'.Yii::app()->baseUrl.'/index.php/kindergarden/kinderEvaluation/index?part='.$this->part.'">';
         echo Yii::t('app','Evaluation Kindergarden');
         echo'</a></li>';

         echo '<li class="';  if($this->part == 'rpt') echo "active"; else echo "";
         echo '"><a href="'.Yii::app()->baseUrl.'/index.php/kindergarden/kinderEvaluation/reportcard?part='.$this->part.'">';
         echo Yii::t('app','Reportcard kindergarden');
         echo'</a></li>';
         
         echo '<li class="';  if($this->part == 'lst') echo "active"; else echo "";
         echo '"><a href="'.Yii::app()->baseUrl.'/index.php/kindergarden/kinderEvaluation/list?part='.$this->part.'">';
         echo Yii::t('app','List student in kindergarden');
         echo'</a></li>';
         
         echo '<li class="';  if($this->part == 'tea') echo "active"; else echo "";
         echo '"><a href="'.Yii::app()->baseUrl.'/index.php/kindergarden/kinderEvaluation/listteacher?part='.$this->part.'">';
         echo Yii::t('app','List Teacher in kindergarden');
         echo'</a></li>';

         echo '<li class="';  if($this->part == 'conf') echo "active"; else echo "";
         echo '"><a href="'.Yii::app()->baseUrl.'/index.php/kindergarden/kinderPeriod/index?part='.$this->part.'">';
         echo Yii::t('app','Settings kindergarden');
         echo'</a></li>';








?>
</ul>

 </div>


<!-- </div> -->
