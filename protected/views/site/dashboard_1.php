<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


/* @var $this SiteController */
$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));


$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->baseUrl;

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/css/AdminLTE.min.css");
$cs->registerCssFile($baseUrl."/css/new-dashboard.css");
$cs->registerCssFile("https://use.fontawesome.com/releases/v5.5.0/css/all.css");

?>
<?php
/* @var $this SiteController */
// Set the page title at the index (dashboard) with the profil name and the user name of the login user.
$this->pageTitle = Yii::app()->name;//.' - '.Yii::app()->user->profil. ' '.Yii::app()->user->name;
?>

<?php // echo Yii::t('app','Welcome to {name}', array('{name}'=>Yii::app()->name));?>
<?php
	     $acad_sess = acad_sess();
             
$acad=Yii::app()->session['currentId_academic_year'];
$siges_structure = infoGeneralConfig('siges_structure_session');


$display_announcement_in_menu = infoGeneralConfig('display_announcement_in_menu');

     $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
     
if($acad !=  $current_acad->id){
    $acad = $acad_sess;
}     



 if($current_acad==''){
        $condition = '';
          $condition1 ='';
 }
    else
    {  if($acad!=$current_acad->id)
       {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
      else
        {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
    }



$modelAcad=new AcademicPeriods;


if(isset(Yii::app()->user->profil))
  {
	if(Yii::app()->user->profil!='Guest')
	  {
			$lastAcadYear=AcademicPeriods::model()->denyeAneAkademikNanSistemLan();
			if(isset($lastAcadYear)&&($lastAcadYear!=null))
			 {
				if($acad==$lastAcadYear['id'])
				  {

					$num_month = date_diff ( date_create(date('Y-m-d'))  , date_create($lastAcadYear['date_end']))->format('%R%a');

					$message = Yii::t('app','Please run "End Year Decision", {name} day(s) left to move to a new academic year.',array('{name}'=>substr($num_month,1) ));

					if(($num_month < 15))
					  Yii::app()->user->setFlash(Yii::t('app','Warning'), $message);

				  	}

			  }

	  }
  }




	$school_name = infoGeneralConfig('school_name');

	//echo $school_name;
     $school_acronym = infoGeneralConfig('school_acronym');


      $path=null;

     $explode_basepath= explode("protected",substr(Yii::app()->basePath, 0));
     echo '<input type="hidden" id="basePath" value="'.$explode_basepath[0].'" />';

?>


    <div class="container-fluid" style="text-align:center;" id="index_dash1">

    <div class="row-fluid">






<div style="clear:both"></div>
<div class="span12" style=" text-align:justify;" id="index_dash">



				<!-- ================================================== -->

	 <?php





							            $criteria = new CDbCriteria;
							            $criteria->select = 'count(*) as total_stud';


							              $criteria->condition= $condition1.' is_student=:item_name ';

							            $criteria->params=array(':item_name'=>1,);
							            $total_stud = Persons::model()->find($criteria)->count();





							        $tot_stud_s=0;
							        $female_s = 0;
							        $male_s = 0;

							        $tot_stud=0;
							        $female = 0;
							        $male = 0;
							        //teachers
							        $tot_teach_s=0;
							        $female1_s = 0;
							        $male1_s = 0;

							        $tot_teach=0;
							        $female1 = 0;
							        $male1 = 0;
							        //employees
							        $tot_emp_s=0;
							        $female2_s = 0;
							        $male2_s = 0;

							        $tot_emp=0;
							        $female2 = 0;
							        $male2 = 0;

							         $level = array();




							        // Student data
							       $dataProvider= Persons::model()->searchStudentsReport($condition,$acad_sess);

							        if($dataProvider->getItemCount()!=0)
							            $tot_stud =$dataProvider->getItemCount();


							        //reccuperer la qt des diff. sexes
							        $person=$dataProvider->getData();

							        foreach($person as $stud)
							         {
								        if($stud->gender==1)
								            $female++;
								        elseif($stud->gender==0)
								            $male++;
							         }
							        // Fin student data

							        // Teachers data

							        $dataProvider1= Persons::model()->searchTeacherReport($condition,$acad_sess);

							        if($dataProvider1->getItemCount()!=0)
							        $tot_teach =$dataProvider1->getItemCount();


							        //reccuperer la qt des diff. sexes
							        $person=$dataProvider1->getData();

							        foreach($person as $teacher)
							         {
								        if($teacher->gender==1)
								             $female1++;
								        elseif($teacher->gender==0)
								              $male1++;
							         }
							        // Fin teachers

							        // debut Employes
							        $dataProvider2= Persons::model()->searchEmployeeReport($condition,$acad_sess);

							        if($dataProvider2->getItemCount()!=0)
							           $tot_emp =$dataProvider2->getItemCount();


							        //reccuperer la qt des diff. sexes
							        $person=$dataProvider2->getData();

							        foreach($person as $employee)
							         {
								        if($employee->gender==1)
								             $female2++;
								        elseif($employee->gender==0)
								            $male2++;
							          }

							// fin employes


							// Total pesonnes
							$tot_pers = $tot_stud + $tot_emp + $tot_teach;

							//reccuperer la qt des diff. sexes
							$tot_fem = $female  + $female1 + $female2;
							$tot_mal = $male  + $male1 + $male2;
							// fin total personne

							// Stat sur Rooms
							$tot_rooms=0;
							$countRooms = Rooms::model()->searchReport();
							if($countRooms->getItemCount()!=0)
							    $tot_rooms = $countRooms->getItemCount();

							// Stat sur Subjects
							$tot_sub=0;
							$countSubjects = Subjects::model()->searchReport();
							if($countSubjects->getItemCount()!=0)
							    $tot_sub = $countSubjects->getItemCount();

							// Stat sur Courses
							 $countCourses = null;
							$tot_course=0;
							if($acad!='')
							  $countCourses = Courses::model()->searchReport($condition1,$acad_sess);



							if($countCourses->getItemCount()!=0)
							    $tot_course = $countCourses->getItemCount();

							 ?>



			 <!--===========================================-->



		<?php
		       $span="span12 r-tibwat";
		?>



		  <div class="row ">

                      <!--  Test du nouveau design du dashboard
                      Le test etant concluant on implement le dashboard ak sa yo
                      -->
                      <!-- Student -->
                      <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-blue" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_stud;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Students') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Female')?> : <?php echo $female; ?>
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Male')?> : <?php echo $male; ?>
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fa fa-group"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>
                      <!-- Fin du test -->
                      <!-- Pwofese -->
                      <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-fuchsia" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_teach;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Teachers') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Female')?> : <?php echo $female1; ?>
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Male')?> : <?php echo $male1; ?>
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fas fa-chalkboard-teacher"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>
			<!-- Salle de classe -->
		     <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-green" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_rooms;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Rooms') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        &nbsp;
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        &nbsp;
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fas fa-building"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>
                      <!-- Matiere -->
                      <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-yellow" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_sub;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Subjects') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        &nbsp;
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        &nbsp;
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fa fa-file-o"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>


		     </div>
                    <div class="row">
                    <!-- Cours -->
                    <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-espesyalizasyon" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_course;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Courses') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        &nbsp;
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        &nbsp;
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fa fa-folder-open-o"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>
                    <!--  Staff -->
                    <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-estaf" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_emp;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Staff') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Female')?> : <?php echo $female2; ?>
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Male')?> : <?php echo $male2; ?>
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fa fa-male"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>

                    <!-- Nouvo elev -->
                    <?php

                        $acad_sess = acad_sess();
                               $modelPos=new Postulant();
                               $modelPostulant = $modelPos->searchApproved($acad_sess);

                               $modelPostul = $modelPostulant->getData();
                               $female3=0;
                               $male3=0;

                               $tot_admi=0;

                               if($modelPostul!=null)
                                {
                                    foreach ($modelPostul as $potul)
                                      {
                                        if($potul!=null)
                                         {
                                            if($potul->gender==1)
                                                 $female3++;
                                            elseif($potul->gender==0)
                                                  $male3++;

                                             $tot_admi++;

                                         }
                                      }

                                 }

                        ?>
                    <!-- Nouvo elev -->
                    <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-red" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_admi;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','New students') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Female')?> : <?php echo $female3; ?>
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Male')?> : <?php echo $male3; ?>
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fas fa-user-friends"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>

                    <?php


                               $modelPos=new Postulant();
                               $modelPostulant = $modelPos->search($acad_sess);

                               $modelPostul = $modelPostulant->getData();
                               $female4=0;
                               $male4=0;

                               $tot_postul=0;

                               if($modelPostul!=null)
                                {
                                    foreach ($modelPostul as $potul)
                                      {
                                        if($potul!=null)
                                         {
                                            if($potul->gender==1)
                                                 $female4++;
                                            elseif($potul->gender==0)
                                                  $male4++;

                                             $tot_postul++;

                                         }
                                      }

                                 }

                        ?>

                    <!-- Postulants -->

                    <div class="span3 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-postilan" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_postul;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Postulants') ?></span></h3>

                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Female')?> : <?php echo $female4; ?>
                                    </p>
                                    <p class="nom-piti teks-blan">
                                        <?= Yii::t('app','Male')?> : <?php echo $male4; ?>
                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fas fa-user-graduate"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>



                    </div>






		</div>

		<div class="row" style="">

                </div> <!--end fluid 2-->
	<br/>

                       <?php

              $last_acad=AcademicPeriods::model()->searchLastAcademicPeriod();
              $data = AcademicPeriods::model()->findAllBySql("SELECT id,name_period,is_year,year,date_start,date_end FROM academicperiods where id = $acad_sess");

              $date_start = '';
              $date_end = '';

              foreach ($data as $d)
              {
                  $date_start = $d->date_start;
                   $date_end = $d->date_end;
              }

               $acad_name=Yii::app()->session['currentName_academic_year'];

                             $month = getMonth(date('Y-m-d'));
                             $month_name = getLongMonth($month);

                             $year = getYear(date('Y-m-d'));

                   if($last_acad->id==$acad)
                       {
                             ?>
              <!--  <div style=" width: 67%;">
                    <!--  <div class="inner" style="background-color:#ECF0F5">
                            <h4><span class='percent teks-blan'><?= Yii::t('app','School life').': '.$acad_name ?></span></h4>
                         </div> -->
 									<div class="row c2" >
                     <div id="container1" class='span4 l-tibwat' style="float: left;  height: 400px;"> </div>

               	     <div id="container" class='span4 c-tibwat' style="float: left;  height: 400px;  "></div>
              <!--  </div> -->

                    <div id="container-right" class='span4 r-tibwat' style="float: left;  height: 400px;  "></div>
  							</div>
                    <!--
                    TABLEAU d'HONNEUR
                -->
                <?php
                
               $all_period_name = AcademicPeriods::model()->findAllBySql("SELECT a.id, a.name_period FROM academicperiods a INNER JOIN evaluation_by_year eby ON (a.id = eby.academic_year) WHERE eby.id IN (SELECT evaluation_by_year FROM average_by_period WHERE academic_year = $acad_sess) ORDER BY a.id DESC");
$current_period_object  = AcademicPeriods::model()->findAllBySql("SELECT a.id, a.name_period FROM academicperiods a INNER JOIN evaluation_by_year eby ON (a.id = eby.academic_year) WHERE eby.id IN (SELECT evaluation_by_year FROM average_by_period WHERE academic_year = $acad_sess) ORDER BY a.id DESC LIMIT 1");
               $period_id = 0;
               $period_name = null;
               foreach($current_period_object as $cpo){
                   $period_id = $cpo->id;
                   $period_name = $cpo->name_period;
               }
               /** a integrer
               $max_grade = getMaxAverageStudent($acad, 10, 17);
               print_r($max_grade);
                *
                */
                ?>
                <div class="row">
                    <div class="row-fluid roll-honor">
                        <div class="span6">
                        <h4>  <?= Yii::t('app','Roll of Honor')?> </h4>
                        </div>
                        <div class="span2" style="float: left;">
                                    <select name="academic_period"  id="academic-period" class="form-control">
                                        <?php foreach($all_period_name as $ap){

                                            ?>
                                       <option value="<?php  echo $ap->id; ?>"><?php echo $ap->name_period; ?></option>
                                        <?php
                                        } ?>
                                    </select>
                        </div>
                    </div>

                    <div id="roll-honor">

                    </div>




                </div>
                <!-- FIN TABLEAU d'HONNEUR -->


                  <?php

                       }

                  ?>




                </div>

            </div>







	</div>




</div>

<!-- ================================================== -->




	</br>
	</br>


<div style="clear:both"></div>


<?php




 $string_sql_present = 'SELECT count(presence_type) as total, presence_type FROM record_presence rp WHERE date_record BETWEEN \''.$date_start.'\' and \''.$date_end.'\' group by presence_type';
     $data2 = RecordPresence::model()->findAllBySql($string_sql_present);

   $tab_presen='';
   $tot_tab_presen='';
   $s1=0;
 if($data2 !=null)
 {
 foreach($data2 as $da){

     switch($da->presence_type)
        {
            case 0:
               // $tab_presen =Yii::t('app','P').','.$da->total.',false';
                break;
            case 1:
                 $tab_presen =Yii::t('app','ANE').','.$da->total.',false';
                 break;
            case 2:
                $tab_presen =Yii::t('app','AWE').','.$da->total.',false';
                break;
            case 3:
                $tab_presen =Yii::t('app','TNE').','.$da->total.',false';
                break;
            case 4:
                $tab_presen =Yii::t('app','TWE').','.$da->total.',false';
                break;

            }

     if($s1==0)
       {  if($tab_presen!='')
          {
           $tot_tab_presen = $tab_presen;

           $s1=1;
          }
        }
     else
        {
          $tot_tab_presen = $tot_tab_presen.'/'.$tab_presen;

        }



 }
 }
 $string_sql_infract = 'SELECT count(infraction_type) AS total, infraction_type, it.name as infraction_name FROM record_infraction ri INNER JOIN infraction_type it on(it.id = ri.infraction_type) WHERE incident_date BETWEEN \''.$date_start.'\' and \''.$date_end.'\' group by infraction_type limit 10 ';
     $data1 = RecordInfraction::model()->findAllBySql($string_sql_infract);
  $category_infract ='';
  $tot_category_infract ='';
     $s = 0;

 if($data1 !=null)
 {
 foreach($data1 as $ap){

     if($s==0)
       {   $category_infract = $ap->infraction_name;
         $tot_category_infract = $ap->total;
           $s=1;
        }
     else
        {
          $category_infract = $category_infract.','.$ap->infraction_name;
          $tot_category_infract = $tot_category_infract.','.$ap->total;
        }



 }
 }



 $string_sql_cycle = "SELECT COUNT( DISTINCT gpStudent.students) AS Total,Cycle, Section
                                        FROM room_has_person rhp
                                        INNER JOIN level_has_person lhp ON(lhp.students=rhp.students AND rhp.academic_year=lhp.academic_year )
                                        INNER JOIN ( SELECT rhp1.students,c.cycle_description as Cycle, s.section_name AS Section
                                                      FROM room_has_person rhp1 INNER JOIN level_has_person lhp1 ON(lhp1.students=rhp1.students and rhp1.academic_year=lhp1.academic_year) INNER JOIN section_has_cycle shc ON(shc.level=lhp1.level AND shc.academic_year=lhp1.academic_year) INNER JOIN sections s ON(s.id=shc.section) INNER JOIN cycles c ON(shc.cycle=c.id)
                                                       WHERE rhp1.academic_year=$acad) gpStudent
                                                  ON rhp.students=gpStudent.students
                                      GROUP BY cycle";
  $data3 = RoomHasPerson::model()->findAllBySql($string_sql_cycle);

  $tab_cycle='';
   $tot_tab_cycle='';
   $s3=0;

if($data3 !=null)
 {
 foreach($data3 as $da){

    $tab_cycle= $da->Cycle.','.$da->Total.','.$da->Section;


     if($s3==0)
       {  if($tab_cycle!='')
          {
           $tot_tab_cycle = $tab_cycle;

           $s3=1;
          }
        }
     else
        {
          $tot_tab_cycle = $tot_tab_cycle.'/'.$tab_cycle;

        }
      }

 }




 echo '<input type="hidden" id="data_category_infract" value="'.$category_infract.'" />';
 echo '<input type="hidden" id="tot_category_infract" value="'.$tot_category_infract.'" />';

 echo '<input type="hidden" id="tot_tab_presen" value="'.$tot_tab_presen.'" />';

 echo '<input type="hidden" id="cycle" value="'.$tot_tab_cycle.'" />';

 echo '<input type="hidden" id="titleAbs" value="'.Yii::t('app','Attendance/Punctuality').'" />';
 echo '<input type="hidden" id="yaxis" value="'.Yii::t('app','Number of student').'" />';
 echo '<input type="hidden" id="title_inf" value="'.Yii::t('app','Top 10').' '.Yii::t('app','Infractions').'" />';
 echo '<input type="hidden" id="subtitle_inf" value=" " />';
 echo '<input type="hidden" id="title_cycle" value="'.Yii::t('app','Number of student by Section and Cycle').'" />';




?>




<script>



$(document).ready( function() {
    var period_id = 2 <?php // $period_id; ?>;
    var acad = <?= $acad; ?>;
    $.get('<?= Yii::app()->baseUrl ?>/index.php/site/rollhonor',{period_id:period_id,acad:acad},function(data){
                    $("#roll-honor").html(data);

       });

    $("#academic-period").click(function(){
        var new_period = $("#academic-period").val();
        $.get('<?= Yii::app()->baseUrl ?>/index.php/site/rollhonor',{period_id:new_period,acad:acad},function(data){
                    $("#roll-honor").html(data);

       });

    });


var basePath=document.getElementById("basePath").value;

$('#fileTreeDemo_1').fileTree({ root: '../images/', script: 'connectors/jqueryFileTree.php' }, function(file) {
        alert(file);
});


var titleAbs = document.getElementById("titleAbs").value;
var data_tab_presen=document.getElementById("tot_tab_presen").value;
	var tab_present = data_tab_presen.split("/");
 var j;
 var k;
 var tab_present_split;
  var final_tab=[];


   //table value for
   for(j=0;j<tab_present.length; j++)
    {
        tab_present_split = tab_present[j].split(",");

     var a=1;
     var temp=[];

      for(k=0;k<tab_present_split.length; k++)
       {
        if(a!=2)
        temp.push(tab_present_split[k]);
        else
            temp.push(parseFloat(tab_present_split[k]));

        a++;
        if(a==4)
          a=1;

        }

       final_tab.push(temp);



    	}

   $('#container').highcharts({

        chart: {
        styledMode: true
    },

    title: {
        text: titleAbs
    },



plotOptions: {
        pie: {
            dataLabels: {
                enabled: true,
                format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                distance: -50,
                filter: {
                    property: 'percentage',
                    operator: '>',
                    value: 4
                },

            }
        }
    },
    series: [{
        type: 'pie',
        allowPointSelect: true,
        keys: ['name', 'y', 'selected', 'sliced'],
        name: 'Total',
        data: final_tab,
        showInLegend: false,

    }]
 });


var title_inf = document.getElementById("title_inf").value;
 var subtitle_inf = document.getElementById("subtitle_inf").value;
var data_category_infract=document.getElementById("data_category_infract").value;
	var tab_infract = data_category_infract.split(",");
 var data_tot_category_infract=document.getElementById("tot_category_infract").value;
	var tab_tot_infract = data_tot_category_infract.split(",");
  var tab_tot=[];
   var i;

   //table value for male
   for(i=0;i<tab_tot_infract.length; i++)
    {
    	tab_tot.push(parseFloat(tab_tot_infract[i]));
    	}

    $('#container1').highcharts({


    title: {
        text: title_inf
    },

    subtitle: {
        text: subtitle_inf
    },
       xAxis: {
        categories: tab_infract
    },
       yAxis: {
        title: {
            text: ' '
        }
    },

    series: [{
        type: 'column',
        colorByPoint: true,
        name: 'Total',
        data: tab_tot,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 5, // 10 pixels down from the top
            style: {
                fontSize: '9px',
                fontFamily: 'Verdana, sans-serif'
            }
        },
        showInLegend: false
    }]



    });



   var yAxis_ = document.getElementById("yaxis").value;
   var title_cycle = document.getElementById("title_cycle").value;

    var data_cycle=document.getElementById("cycle").value;
	var tab_cycle = data_cycle.split("/");

    var l;
 var temp='';
 var tab_cycle_split;
  var final_tab_cycle=[];

  var a;
     var b=[];
     var c;
     var m;

     var temp_='';

 var categ= [];
   for(m=0;m<tab_cycle.length; m++)
    {
        tab_cycle_split = tab_cycle[m].split(",");

        if(temp_!=tab_cycle_split[2])
         {  temp_= tab_cycle_split[2];
             categ.push(tab_cycle_split[2]);
         }

    }


        //table value for
   for(l=0;l<tab_cycle.length; l++)
    {   b= [];
        tab_cycle_split = tab_cycle[l].split(",");

        a = tab_cycle_split[0];
        c= tab_cycle_split[2];

        for(m=0;m<categ.length; m++)
        {
           if(categ[m]==c)
             b.push(parseFloat(tab_cycle_split[1]));
           else
               b.push(null);
        }

    final_tab_cycle.push({
        name: a,
        data: b,
        stack: c
    });

    }



      $('#container-right').highcharts({

     chart: {
        type: 'column'
    },
    title: {
        text: title_cycle //' '
    },
    xAxis: {
        categories: categ
    },
    yAxis: {
        min: 0,
        title: {
            text: yAxis_ //' '
        }
    },
    tooltip: {
       // pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>',
        //shared: true

         formatter: function () {
            return '<b>' + this.x + '</b><br/>' +
                this.series.name + ': ' + this.y + '<br/>' +
                'Total: ' + this.point.stackTotal;
        }
    },
    plotOptions: {
        column: {
            stacking: 'normal',  // 'percent',

            dataLabels: {
                enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: -25, // 10 pixels down from the top
            style: {
                fontSize: '9px',
                fontFamily: 'Verdana, sans-serif'
            }

            }


         }


    },
    series: final_tab_cycle

    });



});






</script>
