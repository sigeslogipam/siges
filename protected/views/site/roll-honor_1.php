<?php 
// echo $period_id; 
$all_rooms = Rooms::model()->findAllBySql("SELECT * from rooms ORDER BY short_room_name ASC"); 

?>
<p> &nbsp;</p>
<table class="table table-striped responsive table-hover table-sm">
                          
      <tbody>
          <?php
        
          $k =0;
          foreach ($all_rooms as $ar){
              $student_max[$k] = getMaxAverageStudent($acad, $period_id, $ar->id);
              if(isset($student_max[$k]['max_average'])){
                  if(Persons::model()->findByPk($student_max[$k]['student'])->image==NULL){
                       // $string_image_location = "<img class='direct-chat-img' src='".Yii::app()->baseUrl."/css/images/no_pic.png'>";
                        $string_image_location = "<div class='icon'>
                                        <i class='fas fa-user-graduate fa-3x'></i>
                               </div>"; 
                    }else{
                        $string_image_location = "<img class='direct-chat-img' src='".Yii::app()->baseUrl."/documents/photo-Uploads/1/".Persons::model()->findByPk($student_max[$k]['student'])->image."'>";
                        }
                ?>
          
          <tr>
              <th scope="row"><?= $k+1; ?></th>
              <td><?= $string_image_location; ?></td>
              <td><?= Persons::model()->findByPk($student_max[$k]['student'])->first_name; ?></td>
              <td><?= Persons::model()->findByPk($student_max[$k]['student'])->last_name; ?></td>
              <td><?= $ar->level0->short_level_name; ?></td>
              <td><?= $ar->short_room_name; ?></td>
              <td><?= $student_max[$k]['max_average']; ?></td>
          </tr>
          <?php 
           $k++;
              }else{
                  
              }
         
          }
          
          ?>
        
      </tbody>
</table>