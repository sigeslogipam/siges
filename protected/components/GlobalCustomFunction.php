<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>
<?php
	
// auto-loading




	function numberAccountingFormat($number)
	{
		//string number_format ( float $number , int $decimals = 0 , string $dec_point = "." , string $thousands_sep = ",” )
       // english notation with thousands separator
		$format_number = preg_replace( '/(-)([\d\.\,]+)/ui','($2)', number_format($number, 2, '.', ',')  );
		// french notation with thousands separator
		//$format_number = number_format($number, 2, ',', '.');
		return $format_number;
	}
	
	
	function pa_daksan()
	{
		 return array('Š'=>'S', 'š'=>'s', 'Ž'=>'Z', 'ž'=>'z', 'À'=>'A', 'Á'=>'A', 'Â'=>'A', 'Ã'=>'A', 'Ä'=>'A', 'Å'=>'A', 'Æ'=>'A', 'Ç'=>'C', 'È'=>'E', 'É'=>'E', 'Ê'=>'E', 'Ë'=>'E', 'Ì'=>'I', 'Í'=>'I', 'Î'=>'I', 'Ï'=>'I', 'Ñ'=>'N', 'Ò'=>'O', 'Ó'=>'O', 'Ô'=>'O', 'Õ'=>'O', 'Ö'=>'O', 'Ø'=>'O', 'Ù'=>'U', 'Ú'=>'U', 'Û'=>'U', 'Ü'=>'U', 'Ý'=>'Y', 'Þ'=>'B', 'ß'=>'Ss', 'à'=>'a', 'á'=>'a', 'â'=>'a', 'ã'=>'a', 'ä'=>'a', 'å'=>'a', 'æ'=>'a', 'ç'=>'c', 'è'=>'e', 'é'=>'e', 'ê'=>'e', 'ë'=>'e', 'ì'=>'i', 'í'=>'i', 'î'=>'i', 'ï'=>'i', 'ð'=>'o', 'ñ'=>'n', 'ò'=>'o', 'ó'=>'o', 'ô'=>'o', 'õ'=>'o', 'ö'=>'o', 'ø'=>'o', 'ù'=>'u', 'ú'=>'u', 'û'=>'u', 'ý'=>'y', 'ý'=>'y', 'þ'=>'b', 'ÿ'=>'y' );

    
    }
 
 function capital_aksan()
	{
		 return ['Š'=>'Š', 'š'=>'Š', 'Ž'=>'Ž', 'ž'=>'Ž', 'À'=>'À', 'Á'=>'Á', 'Â'=>'Â', 'Ã'=>'Ã', 'Ä'=>'Ä', 'Å'=>'Å', 'Æ'=>'Æ', 'Ç'=>'Ç', 'È'=>'È', 'É'=>'É', 'Ê'=>'Ê', 'Ë'=>'Ë', 'Ì'=>'Ì', 'Í'=>'Í', 'Î'=>'Î', 'Ï'=>'Ï', 'Ñ'=>'Ñ', 'Ò'=>'Ò', 'Ó'=>'Ó', 'Ô'=>'Ô', 'Õ'=>'Õ', 'Ö'=>'Ö', 'Ø'=>'Ø', 'Œ'=>'Œ', 'Ù'=>'Ù', 'Ú'=>'Ú', 'Û'=>'Û', 'Ü'=>'Ü', 'Ū'=>'Ū', 'Ý'=>'Ý', 'Ÿ'=>'Ÿ', 'Þ'=>'B', 'à'=>'À', 'á'=>'Á', 'â'=>'Â', 'ã'=>'Ã', 'ä'=>'Ä', 'å'=>'Å', 'æ'=>'Æ', 'ç'=>'Ç', 'è'=>'È', 'é'=>'É', 'ê'=>'Ê', 'ë'=>'Ë', 'ì'=>'Ì', 'í'=>'Í', 'î'=>'Î', 'ï'=>'Ï', 'ñ'=>'Ñ', 'ò'=>'Ò', 'ó'=>'Ó', 'ô'=>'Ô', 'õ'=>'Õ', 'ö'=>'Ö', 'ø'=>'Ø', 'œ'=>'Œ', 'ù'=>'Ù', 'ú'=>'Ú', 'û'=>'Û', 'ü'=>'Ü', 'ū'=>'Ū', 'ý'=>'Ý', 'ÿ'=>'Ÿ' ];

    
    }
 
function headerLogo()
  { 
    $string_add_logo = "<img src='".Yii::app()->baseUrl."/css/images/school_logo.png' style=\"margin-right:10px; max-height:50px; width:auto; \">";
     
     return $string_add_logo;
  
   }
   

function pdfHeaderPosition()
  {
  	    $header_position = infoGeneralConfig('header_position');
  	    
  	  switch($header_position)
       {  //L or empty string: left align (default value)
          //C: center
          //R: right align
          //J: justify
          
       	  case 1: $position = 'L';  //L or empty string: left align (default value)
       	          return $position;
       	          break;
       	     
       	  case 2: $position = 'C';  //C: center
       	          return $position;
       	          break;
       	     
       	  case 3: $position = 'R';  //R: right align
       	          return $position;
       	          break;
       	  case 4: $position = 'J';  //J: justify
       	          return $position;
       	          break;
       	     
       	   default: $position = 'L';
       	            return $position;
       	          break;
       	    
       	}

  	
  	}
  	
  	
 function pdfHeaderTextColor()
  {
  	    $header_schoolname_color = infoGeneralConfig('header_schoolname_color');
  	    
  	  switch($header_schoolname_color)
       {
       	  
       	  case 0: $color = [0,0,0];  //'#000000';   //0: noire;
       	          return $color;
       	          break;
       	     
       	  case 1: $color = [255,0,0];   //'#ff0000';   //1: rouge;
       	           return $color;
       	          break;
       	     
       	  case 2:  $color = [0,51,255];   //'#000cff';   //2: bleue;
       	          return $color;
       	          break;
       	  
       	  case 3: $color = [41,153,7];   //'#008000';   //3: verte;
       	          return $color;
       	          break;
       	     
       	  case 4: $color = [255,165,0];   //'#FFA500';   //4: orange;
       	         return $color;
       	          break;
       	     
       	     
       	   default: $color = [0,0,0];   //'#000000';   //0: noire;
       	           return $color;
       	            break;
       	  
       	}

  	
  	}


function pdfHeaderLineColor()
  {
  	    $header_line_color = infoGeneralConfig('header_line_color');
  	    
  	  switch($header_line_color)
       {
       	  
       	  case 0: $color = [0,0,0];  //'#000000';   //0: noire;
       	          return $color;
       	          break;
       	     
       	  case 1: $color = [255,0,0];   //'#ff0000';   //1: rouge;
       	           return $color;
       	          break;
       	     
       	  case 2:  $color = [0,51,255];   //'#000cff';   //2: bleue;
       	          return $color;
       	          break;
       	  
       	  case 3: $color = [41,153,7];   //'#008000';   //3: verte;
       	          return $color;
       	          break;
       	     
       	  case 4: $color = [255,165,0];   //'#FFA500';   //4: orange;
       	         return $color;
       	          break;
       	     
       	     
       	   default: $color = [0,0,0];   //'#000000';   //0: noire;
       	           return $color;
       	            break;
       	  
       	}

  	
  	}


  	
  	
function pdfHeading()
  { 
    $position = 'left';
    $color = '#000000'; // noire/black
    
    
    $school_name = infoGeneralConfig('school_name');
	//echo $school_name;
        // School address
    $school_address = infoGeneralConfig('school_address');
        //School Phone number 
     $school_phone_number = infoGeneralConfig('school_phone_number');
      
      $school_email_address = infoGeneralConfig('school_email_address');

     $school_acronym = infoGeneralConfig('school_acronym');
    
    $header_position = infoGeneralConfig('header_position');
    
    $header_schoolname_color = infoGeneralConfig('header_schoolname_color');
    
    switch($header_position)
       {
       	  case 1: $position = 'left';
       	     break;
       	     
       	  case 2: $position = 'center';
       	     break;
       	     
       	  case 3: $position = 'right';
       	     break;
       	     
       	   default: $position = 'left';
       	    
       	}
       	
     switch($header_schoolname_color)
       {
       	  case 0: $color = '#000000';
       	     break;
       	     
       	  case 1: $color = '#ff0000';
       	     break;
       	     
       	  case 2:  $color = '#000cff';
       	     break;
       	  
       	  case 3: $color = '#008000';
       	     break;
       	     
       	  case 4: $color = '#FFA500';
       	     break;
       	     
       	     
       	   default: $color = '#000000';
       	  
       	}
    
     
$school_name_school_acronym = $school_name; 

if($school_acronym!='')
   $school_name_school_acronym = $school_name.' ('.$school_acronym.')';
                  	          	
$path = Yii::app()->baseUrl."/css/images/school_logo.png";




	$hearder ='<table  style="width:100%; margin-bottom:-17px; " ><tr><td rowspan="2" style="width:22%" ><span style="padding-top:0px; "><img src="'.$path.'" style="margin-right:10px; max-height:50px; width:auto; "></span></td><td colspan="4" style="text-align:'.$position.' " > <br/><span style="font-size:18px; color:'.$color.'" >'.$school_name_school_acronym.'</span> <br/> <span style="font-size:13px;" >'.$school_address.'</span> <br/>  <span style="font-size:11px;" > Tel: '.$school_phone_number.' / E-mail: '.$school_email_address.'</span><hr style=" color:#000; margin-top:5px; width:85%;" /></td><td rowspan="2" style="width:22%" ></td></tr></table>';
			
     return $hearder;
  
   }
   
     
    
 function acad_sess()
  {
		$acad_sess = 0;
			
	$siges_structure = infoGeneralConfig('siges_structure_session');
	     
	   if($siges_structure==1)
	    {
	         $sess=Yii::app()->session['currentId_academic_session'];  
                  $sess_name=Yii::app()->session['currentName_academic_session'];	
	      }

$acad=Yii::app()->session['currentId_academic_year']; 
		
 if($siges_structure==1)
 {  if( $sess=='')
        $acad_sess = 0;
    else 
		$acad_sess = $sess;
 }
 elseif($siges_structure==0)
   $acad_sess = $acad;


		
		 return $acad_sess;
 
    }
 

//return an object
function currentAcad()
 {
	 $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
                   
       return $current_acad;
 } 

//-1: migration not yet done; 1: migration is not completed 2: migration done; 0: no migration to do  
function getYearMigrationCheck($acad)
{
    $result=2;
    $data = YearMigrationCheck::model()->getValueYearMigrationCheck($acad);
    
         if($data!=null)
           {  foreach($data as $d){
    	         if( ($d['period']==0)&&($d['student']==0)&&($d['postulant']==0)&&($d['course']==0)&&($d['evaluation']==0)&&($d['passing_grade']==0)&&($d['reportcard_observation']==0)&&($d['fees']==0)&&($d['taxes']==0)&&($d['pending_balance']==0) )
    	           {
    	           	  $result=-1; 
    	           	  break;
    	           }
    	         else
    	           {
		    	         if( ($d['period']==0)||($d['student']==0)||($d['postulant']==0)||($d['course']==0)||($d['evaluation']==0)||($d['passing_grade']==0)||($d['reportcard_observation']==0)||($d['fees']==0)||($d['taxes']==0)||($d['pending_balance']==0) )
		    	         $result=1; 
		                    break;
    	           }
                      
              }
           }
         else
            $result = 0;
          
           
	return $result;
       
  }
  
  
  
function lastBillingTransactionID($tud, $acad)
  { 
  	$fee_status = 'fl.status IN(0,1) AND ';  
  	
     $last_bill_id =0;
          $result = Billings::model()->getLastTransactionID($tud,$fee_status, $acad);
          if($result!=null)
            $last_bill_id = $result;
        
        return $last_bill_id;
  
   }
	
function getStudentGradeByCoursePeriod($student, $course,$period)
  {
		 $sql = "SELECT  grade_value FROM grades WHERE student=".$student." AND course=".$course.' AND evaluation='.$period;
										
          $command = Yii::app()->db->createCommand($sql);
 
           $result = $command->queryAll();
           
           $grade = null;
           
           if($result!=null)
             {
             	 
             	 foreach($result as $r)
             	  { 
             	  	$grade = $r['grade_value'];
             	  	   
             	   
               	  }
               	  
               	 return $grade;
             	  
             }
           else
             return  null;
             
 	
	}
	


function getStudentFullName()
 {	
      $code= array();
      $code[]= Yii::t('app','-- Please select student --');
      $criteria = new CDbCriteria(array('order'=>'last_name','condition'=>'is_student=1 AND active IN (1,2)'));

         $modelStudent =  Persons::model()->findAll($criteria);
            
                     
      foreach($modelStudent as $students)
         { 
			    $code[$students->id]= $students->getFullName().' ('.$students->id_number.')';
			    
		  }
		    
		return $code;
      
  }

  
function sumPeriodWeight($pastp,$current_p)
  {
  	  if($pastp!=null)
  	    {
  	    	 $sum_ = 0;
  	    	 $weight_null=false;
  	    	 $weight_100=true;
  	    	 
  	    	 foreach($pastp as $pId)
  	    	   {
  	    	   	 //jwenn peryod eval sa ye
  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($pId);
  	    	   	  
  	    	   	   foreach($p_acad as $p_weight)
  	    	   	     {  
  	    	   	     	if($p_weight['weight']!=null)
  	    	   	          { $sum_ =  $sum_ + $p_weight['weight'];
  	    	   	             
  	    	   	             if($p_weight['weight']!=100)
  	    	   	               $weight_100=false;
  	    	   	                
  	    	   	          }
  	    	   	        else
  	    	   	           $weight_null=true;
  	    	   	          
  	    	   	     }
  	    	   	  
  	    	   	 
  	    	   	}
  	    	   
  	    	if($current_p!='')
  	    	  {
  	    	  	   //jwenn peryod eval sa ye
  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($current_p);
  	    	   	  
  	    	   	   foreach($p_acad as $p_weight)
  	    	   	     {  
  	    	   	     	if($p_weight['weight']!=null)
  	    	   	          { $sum_ =  $sum_ + $p_weight['weight'];
  	    	   	             
  	    	   	             if($p_weight['weight']!=100)
  	    	   	               $weight_100=false;
  	    	   	                
  	    	   	          }
  	    	   	        else
  	    	   	           $weight_null=true;
  	    	   	          
  	    	   	     }
  	    	  	}
  	    	   	
  	    	   	if( ($sum_==0) && ($weight_null==true) ) //tout peryod yo gen menm koyefisyan
  	    	   	  return -1;   
  	    	   	elseif( ($sum_!=0) && ($weight_null==true) )
  	    	   	       return $sum_;
  	    	   	    elseif( ($sum_!=0) && ($weight_null==false) )
  	    	   	        {
  	    	   	        	if( ($weight_100==true) )//tout peryod yo gen menm koyefisyan
  	    	   	        	  return -1;
  	    	   	        	else
  	    	   	        	   return $sum_;
  	    	   	          }
  	    	
  	      }
  	  else
  	     return 0;
  	
  	}
  	
  function isCoursereferenceHasGradeByEvaluation($course, $evaluation)
  { 
  	  $result = Grades::model()->findAll(array('select'=>'id',
                                     'condition'=>'course=:course AND evaluation=:eval',
                                     'params'=>array(':course'=>$course,':eval'=>$evaluation),
                               ));
          if($result!=null)
           { foreach($result as $r)
               {  if( (isset($r->id))&&($r->id!='') )
                     return 1;
                  else
                    return 0;
                    
               }
           	
           }
          else
            return 0;
        
        
  
   }
	

	

	//************************  loadPreviousLevel  ******************************/
function loadPreviousLevel($idLevel)
	{    
       	  
		  $code= array();
          $code[null]= Yii::t('app','-- Select --');
	      
	       $modelLevel= new Levels();

	      $pLevel_id=$modelLevel->findAll(array('select'=>'previous_level',
                                     'condition'=>'id=:levelID',
                                     'params'=>array(':levelID'=>$idLevel),
                               ));
			if(isset($pLevel_id))
			 {  
			    foreach($pLevel_id as $i){			   
					 					   
					  
					  
					  $level=$modelLevel->findAll(array('select'=>'id, level_name',
												 'condition'=>'id=:levelID OR id=:IDLevel',
												 'params'=>array(':levelID'=>$i->previous_level,':IDLevel'=>$idLevel),
										   ));
						
					if(isset($level)){
						  foreach($level as $l)
						       $code[$l->id]= $l->level_name;
					    }  
							   }						 
		    
						  }	
			
		return $code;
         
	}
		

	
	
	//************************  loadLevel ******************************/
function loadAllLevel()
	{    
	    $modelLevel= new Levels();
		
		 
           $code= array();
		   
		  $modelPersonLevel=$modelLevel->findAll();
            $code[null]= Yii::t('app','-- Select --');
		    foreach($modelPersonLevel as $level){
			    $code[$level->id]= $level->level_name;
		           
		      }
		   
		return $code;
         
	}


	//************************  loadAllLevelToSort ******************************/
function loadAllLevelToSort()
	{    
	    $modelLevel= new Levels();
		
		 
           $code= array();
		   
		  $modelPersonLevel=$modelLevel->findAll();
            $code[null]= Yii::t('app','-- Sort by level --');
		    foreach($modelPersonLevel as $level){
			    $code[$level->id]= $level->level_name;
		           
		      }
		   
		return $code;
         
	}


	//************************  loadLevelForMenfpExam ******************************/
function loadLevelForMenfpExam()
	{    
	    $modelLevel= new Levels();
		
		 
           $code= array();
		   
		  $modelPersonLevel=$modelLevel->findAll(array('alias'=>'l',
		  										 'select'=>'l.id, l.level_name',
												 'join'=>'inner join examen_menfp em on(em.level=l.id)',
												 //'condition'=>'l.section=:sectionID',
												 //'params'=>array(':sectionID'=>$idSection),
										   ) );
            $code[null]= Yii::t('app','-- Sort by level --');
		    foreach($modelPersonLevel as $level){
			    $code[$level->id]= $level->level_name;
		           
		      }
		   
		return $code;
         
	}
	
	//************************  loadInactiveReason ******************************/
function loadInactiveReason()
	{    
	    $modelPers= new Persons();
		
		 
           $code= array();
		   
		  
            $code[null]= Yii::t('app','-- Select reason --');
		    
			 $code[1]= Yii::t('app','Abandonment');
			 $code[2]= Yii::t('app','Expulsion');
			 $code[5]= Yii::t('app','Fail in the official exam');
			 $code[3]= Yii::t('app','Illness');
			 $code[4]= Yii::t('app','Trip');
			 $code[6]= Yii::t('app','Death');
                         $code[7]= Yii::t('app','To delete');
			 
		           
		 
		   
		return $code;
         
	}

function getInactiveReason($num)
	{   
		$reason = '';
		 
	    if($num!=null)
		  { 
		  	switch($num)
		      {
               case 1: return Yii::t('app','Abandonment');
				 case 2: return Yii::t('app','Expulsion');
				 case 3: return Yii::t('app','Illness');
				 case 4: return Yii::t('app','Trip');
				 case 5: return Yii::t('app','Fail in the official exam');
				 case 6: return Yii::t('app','Death');
                                 case 7: return Yii::t('app','To delete');
		      }
		           
		  }
		 else  
		   return $reason;
         
	}

	
	
function getAverageBase($room,$acad)
	{
		 $cycle_id ='';
		 $average_base = '';
		 
		 //gade si average_base defini nan tab cycles, sinon pran sa ki nan general_config			
				$modelRoom = Rooms::model()->findByPk($room);
				//al pran section an nan tab level
				$modelLevel = Levels::model()->findByPk($modelRoom->level);
				
				$modelCHS= new SectionHasCycle;
				$modelCHS=SectionHasCycle::model()->getCycleBySectionIdLevelId($modelLevel->section,$modelRoom->level,$acad);
							  	 
				  if(isset($modelCHS)&&($modelCHS!=null))
					{ 
					  $cycle_id =$modelCHS->cycle;
																		    
					 }
			  if($cycle_id !='')
			  {  $modelCycle = Cycles::model()->findByPk($cycle_id);
				  
				  if($modelCycle->average_base!=null)
				     $average_base = $modelCycle->average_base;
				   else
					{
					  //Extract average base
                      $average_base = infoGeneralConfig('average_base');	
                     }
			  }
			 else
			 {
				 //Extract average base
                $average_base = infoGeneralConfig('average_base');	

			 }
			 
		return $average_base;
	}
        
        
 function isSubjectDebaseInRoom($room_id,$acad_)
   {
               $modelCourse = Courses::model()->findAll(array('alias'=>'c',
                                                             'select'=>'c.id, c.teacher',
                                                             'condition'=>'room='.$room_id.' and academic_period='.$acad_.' and debase=1 and old_new=1',
                                                             //'params'=>array(':sectionID'=>$idSection),
                                                           ) );
               if($modelCourse!='')
               {
                   foreach ($modelCourse as $course)
                   {
                       if(isset($course->id)&&($course->id!=''))
                         {  return 1;
                                break;
                          }
                        else { return 0;

                        }
                   }
               }
              else { return 0;

                        }
     
   }

//************************  loadSuccessFailureDecision ******************************/
 function loadSuccessFailureDecision()
	{    
		$decision_s = infoGeneralConfig('decision_finale_success');
		$decision_f = infoGeneralConfig('decision_finale_failure');		
		
		$decision_success_finale_array= explode("/",substr($decision_s, 0));
           
        $decision_failure_finale_array= explode("/",substr($decision_f, 0));
       
        $code= array();
		 
		 $code[0]=Yii::t('app','Changer Décision');
		 
		 $i=1;  
		    foreach($decision_success_finale_array as $decision_success_finale){
			        $code[$i]= $decision_success_finale;
		              $i++;
		              
		           }
			
		    foreach($decision_failure_finale_array as $decision_failure_finale){
			        $code[$i]= $decision_failure_finale;
		             $i++;
		           }
			
		   
		return $code;
        
        
      	}	
 
function getSuccessFailureDecision($num)
	{    
		$decision_s = infoGeneralConfig('decision_finale_success');
		$decision_f = infoGeneralConfig('decision_finale_failure');		
		
		$decision_success_finale_array= explode("/",substr($decision_s, 0));
           
        $decision_failure_finale_array= explode("/",substr($decision_f, 0));
       
        $code= array();
		 
		 $code[0]=Yii::t('app','Changer Décision');
		 
		 $i=1;  
		    foreach($decision_success_finale_array as $decision_success_finale){
			        $code[$i]= $decision_success_finale;
		              $i++;
		           }
			
		    foreach($decision_failure_finale_array as $decision_failure_finale){
			        $code[$i]= $decision_failure_finale;
		             $i++;
		           }
			
		   
		return $code[$num];
        
        
      	}		

	

	//************************  loadFeeName ******************************/
function loadFeeName($status,$level,$acad_sess,$student)
	{    
	   	    $currency_name = Yii::app()->session['currencyName'];
                if(infoGeneralConfig("multiple_devise")==1){
                     $currency_symbol = ""; 
                }else{     
                    $currency_symbol = Yii::app()->session['currencySymbol'];
                }
	       
	       $previous_year= AcademicPeriods::model()->getPreviousAcademicYear($acad_sess);
	   	
	   	$code= array();
	   	//gad si elev la gen balans ane pase ki poko peye
	   	$modelPendingBal=PendingBalance::model()->findAll(array('select'=>'id, balance',
												 'condition'=>'student=:stud AND is_paid=0 AND academic_year=:acad',
												 'params'=>array(':stud'=>$student,':acad'=>$previous_year),
										   ));
			//si gen pending, ajoutel nan lis apeye a			
					if(isset($modelPendingBal)){
						  foreach($modelPendingBal as $bal)
						     {  
						     	$criteria1 = new CDbCriteria(array('alias'=>'f', 'join'=>'inner join fees_label fl on(fl.id=f.fee)', 'order'=>'fee','condition'=>'fl.fee_label LIKE ("Pending balance") AND fl.status='.$status.' AND level='.$level.' AND academic_period = '.$acad_sess));
						     	
						     	$model_feesPend_level = Fees::model()->findAll($criteria1);
						     	
						     if($model_feesPend_level!=null)
						     	{  foreach($model_feesPend_level as $model_feesPend_level_)
						     	     $code[$model_feesPend_level_->id]= Yii::t('app',$model_feesPend_level_->fee0->fee_label).' '.$model_feesPend_level_->level0->level_name.' '.$currency_symbol.' '.numberAccountingFormat($bal->balance);
						     	}
						     	 						     
						     }
					    } 
		 
           
		   
		  $criteria = new CDbCriteria(array('alias'=>'f', 'join'=>'inner join fees_label fl on(fl.id=f.fee)', 'order'=>'fee','condition'=>'fl.fee_label NOT LIKE("Pending balance") AND fl.status='.$status.' AND level = '.$level.' AND academic_period = '.$acad_sess));
		  
		  $modelFeeName=Fees::model()->findAll($criteria);
            //$code[null]= Yii::t('app','-- Please select fee name ---');
		    if($modelFeeName!=null)
			  {  foreach($modelFeeName as $fee_name){
			        $code[$fee_name->id]= $fee_name->feeName;
			      }
		      }
		   
		return $code;
         
	}

function defaultPaymentMethod()
	  {
	     $sql='SELECT pm.id FROM payment_method pm WHERE is_default=1 ';
	    $is_there = Yii::app()->db->createCommand($sql)->queryAll();
				  
            if($is_there!=null)
               {  $method=null;
                   foreach($is_there as $method_def)
		    {
			$method = $method_def['id'];
                        break;
                    }
                    
                  return $method;
               }
            else
               return null;
           
                        
           
           
	  	}       
         
        /*
         * Get month from a date 
         */
      function getMonth($date){
            if(($date!='')&&($date!='0000-00-00'))
              {  $time = strtotime($date);
                         $month=date("n",$time);
                         
            return $month;
              }
           else
              return null;
        }
       
       
        /*
         * Get day from a date 
         */
      function getDay($date){
            if(($date!='')&&($date!='0000-00-00'))
              {  $time = strtotime($date);
                         $day=date("j",$time);
                         
            return $day;
              }
           else
              return null;
        }
        
     
        /*
         * Get year form a date 
         */
      function getYear($date){
            if(($date!='')&&($date!='0000-00-00'))
              {  $time = strtotime($date);
                        $year=date("Y",$time);
                         
                return $year;
              }
           else
              return null;
        }
        
      
      function ChangeDateFormat($d){
            if(($d!='')&&($d!='0000-00-00'))
              { $time = strtotime($d);
                         $month=date("m",$time);
                         $year=date("Y",$time);
                         $day=date("j",$time);
                         
               return $day.'/'.$month.'/'.$year; 
               }
             else
                return '00/00/0000'; 
        }


         
        // Return the name of a month in long format 
  function getLongMonth($mois){
    
    if($mois!='')
    {
    switch ($mois){
        case 0:
            return Yii::t('app','BONIS');
            break;
        case 1:
            return Yii::t('app','January');
            break;
        case 2:
            return Yii::t('app','February');
            break;
        case 3:
            return Yii::t('app','March');
            break;
        case 4:
            return Yii::t('app','April');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','June');
            break;
        case 7:
            return Yii::t('app','July');
            break;
        case 8:
            return Yii::t('app','August');
            break;
        case 9:
            return Yii::t('app','September');
            break;
        case 10:
            return Yii::t('app','October');
            break;
        case 11:
            return Yii::t('app','November');
            break;
        case 12:
            return Yii::t('app','December');
            break;
        case 13:
            return Yii::t('app','13e');
            break;
        case 14:
            return Yii::t('app','14e');
            break;
        }
        
    }
    
   }
    
    
  function getShortMonth($mois){
    
    if($mois!='')
    {
    switch ($mois){
        case 0:
            return Yii::t('app','BONIS');
            break;
        case 1:
            return Yii::t('app','Jan');
            break;
        case 2:
            return Yii::t('app','Feb');
            break;
        case 3:
            return Yii::t('app','Mar');
            break;
        case 4:
            return Yii::t('app','Apr');
            break;
        case 5:
            return Yii::t('app','May');
            break;
        case 6:
            return Yii::t('app','Jun');
            break;
        case 7:
            return Yii::t('app','Jul');
            break;
        case 8:
            return Yii::t('app','Aug');
            break;
        case 9:
            return Yii::t('app','Sep');
            break;
        case 10:
            return Yii::t('app','Oct');
            break;
        case 11:
            return Yii::t('app','Nov');
            break;
        case 12:
            return Yii::t('app','Dec');
            break;
        case 13:
            return Yii::t('app','13e');
            break;
        case 14:
            return Yii::t('app','14e');
            break;
        }
        
      }
    
   }

	
  		
	function getLongDay($day){
		switch($day){
				case 1:
					return Yii::t('app','Monday');
				     break;
				case 2:
					return Yii::t('app','Tuesday');
					break;
				case 3:
					return Yii::t('app','Wednesday');	
					break;
				case 4:
					return Yii::t('app','Thursday');
					break;
				case 5:
					return Yii::t('app','Friday');
					break;
				case 6:
					return Yii::t('app','Saturday');
					break;	
				case 7:
					return Yii::t('app','Sunday');
				      break;
				default: 
					return Yii::t('app','Unknow');
				}
		}

	
    function getShortDay($day){
		switch($day){
				case 1:
					return Yii::t('app','Mon');
				     break;
				case 2:
					return Yii::t('app','Tue');
					break;
				case 3:
					return Yii::t('app','Wed');	
					break;
				case 4:
					return Yii::t('app','Thu');
					break;
				case 5:
					return Yii::t('app','Fri');
					break;
				case 6:
					return Yii::t('app','Sat');
					break;	
				case 7:
					return Yii::t('app','Sun');
				      break;
				default: 
					return Yii::t('app','Unknow');
				}
		}

	
    function getDayNumberByShortDay($day){
		switch($day){
				case Yii::t('app','Mon'):
					return 1;
				     break;
				case Yii::t('app','Tue'):
					return 2;
					break;
				case Yii::t('app','Wed'):
					return 3;	
					break;
				case Yii::t('app','Thu'):
					return 4;
					break;
				case Yii::t('app','Fri'):
					return 5;
					break;
				case Yii::t('app','Sat'):
					return 6;
					break;	
				case Yii::t('app','Sun'):
					return 7;
				      break;
				default: 
					return Yii::t('app','Unknow');
				}
		}


 function getPassingGrade($id_level, $id_academic_period)
	   	{
	   		$criteria = new CDbCriteria;
	   		$criteria->condition='level_or_course=0 AND level=:idLevel AND academic_period=:idAcademicLevel';
	   		$criteria->params=array(':idLevel'=>$id_level,':idAcademicLevel'=>$id_academic_period);
	   		$pass_grade = PassingGrades::model()->find($criteria);
	 
	   	  if(isset($pass_grade))
	   	  return $pass_grade->minimum_passing; 
	   	  else 
	   	    return null;
	   	}

 function getPassingGradeCourse($id_course, $id_academic_period)
	   	{
	   		$criteria = new CDbCriteria;
	   		$criteria->condition='level_or_course=1 AND course=:idCourse AND academic_period=:idAcademicLevel';
	   		$criteria->params=array(':idCourse'=>$id_course,':idAcademicLevel'=>$id_academic_period);
	   		$pass_grade = PassingGrades::model()->find($criteria);
	 
	   	  if(isset($pass_grade))
	   	  return $pass_grade->minimum_passing; 
	   	  else 
	   	    return null;
	   	}                
		
function isAchiveMode($acad)   
    {
    	//fok li pa ane ancour epi fok desizyon final pran pou tout elev aktif
    	
    	
    	$result = false;
   
      if($acad!='')
	    {
	    		    	 	   	    if( ( (totalActiveStudentsInRoomByAcad($acad) !=0 ) && ( totalDecisionTakenByAcad($acad)!=0) )&&( totalActiveStudentsInRoomByAcad($acad) == totalDecisionTakenByAcad($acad) ) )
		                    $result = true;
	    	 	      	 
	       }
	       
       return $result;           
    	
      }
      
      
 function totalActiveStudentsInRoomByAcad($acad)
    {   //SELECT count(rh.students) FROM room_has_person rh INNER JOIN persons p ON(p.id=rh.students)WHERE p.active IN(1,2) AND rh.academic_year=1
    	$total = 0;
    	    $command= Yii::app()->db->createCommand('SELECT count(rh.students) as tot_stud FROM room_has_person rh INNER JOIN persons p ON(p.id=rh.students) WHERE p.active IN(1,2) AND rh.academic_year='.$acad); 
	
	     $data_ = $command->queryAll();
    
         foreach($data_ as $d){
    	         $total = $d['tot_stud']; 
                    break;
                      
         }

    	 return $total;
    	 
    	}
    	
 
 function totalDecisionTakenByAcad($acad)
   {   //SELECT count(df.student) FROM decision_finale df INNER JOIN persons p ON(p.id=df.student)WHERE p.active IN(1,2) AND df.academic_year=1
   	    $total1 = 0;
   	       $command1= Yii::app()->db->createCommand('SELECT count(df.student) as tot_stud1 FROM decision_finale df INNER JOIN persons p ON(p.id=df.student)WHERE p.active IN(1,2) AND df.academic_year='.$acad.' AND is_move_to_next_year <> NULL'); 
	
	     $data_1 = $command1->queryAll();
    
         foreach($data_1 as $d){
    	         $total1 = $d['tot_stud1']; 
                    break;
                      
         }
   	    return $total1;
   	    
   	 }
 
 
 
 
	
//return 0,1 ou total-weight
function evaluationWeightCheck($old_weight)	
  { 
  	 $total_weight=0;
  	 $_weight_=0;
  	 
  	 $pass = false;
  	 
  	 $acad=Yii::app()->session['currentId_academic_year'];   
  	//searchAllEvaluations($acad)
		$eval = Evaluations::model()->searchAllEvaluations($acad);
		
	  if($eval!=null)
	   {
		foreach($eval as $e)
		  {  //getEvaluationWeight($eval_id)
		    // $weight = '';
		  	 $weight = Evaluations::model()->getEvaluationWeight($e['id']);
		  
		  if($weight!=NULL)
		    {
			 foreach($weight as $w)
		      {   
		      	 if( ($w['weight']==NULL)||($w['weight']==100) )	
			  	    {   $pass = true;
			  	    
			  	         if($w['weight']==NULL)
			  	            $_weight_= 0;
			  	            
			  	         if($w['weight']==100)
			  	            $_weight_= 1;
			  	    }
			  	  else
			  	      $total_weight = $total_weight + $w['weight'];
			  	      
		        }
		      
		      }
		     
		   }
		
           
          if($pass==true)
              return $_weight_;
           else
             {
             	if( ($total_weight-$old_weight)==0)
             	   return ($total_weight);
             	else
             	   return ($total_weight-$old_weight);
             }
	   }
	  else
	     return -1;
  
 }
	

//return true/false
function isCourseInPeriod($course,$period)
{
	$result = false;
	
   	       $command1= Yii::app()->db->createCommand('SELECT c.weight FROM courses c WHERE c.id='.$course.' AND c.academic_period='.$period); 
	
	     $data_1 = $command1->queryAll();
    
        if($data_1!=null)
         { foreach($data_1 as $d)
            {
    	        if($d['weight']!='') 
    	         { $result = true;
                    break;
    	         }
             }
         
         }
   	    return $result;
 
 }	  
 
 //return id course
function isCourseAReference($id_course)
 {
	
   	       $command1= Yii::app()->db->createCommand('SELECT c.id FROM courses c WHERE c.reference_id='.$id_course); 
	
	     $data_1 = $command1->queryAll();
    
        if($data_1!=null)
         { foreach($data_1 as $d)
            {
    	        if($d['id']!='') 
    	         { return $d['id'];
                    
    	         }
    	        else
    	           return null;
             }
         
         } 
   	    return null;
 
 }	  
 
  //return 
function isLevelExamenMenfp($level, $acad)
 {
	       $command1= Yii::app()->db->createCommand('SELECT em.id as id, s.subject_name,em.weight FROM examen_menfp em inner join subjects s on(s.id=em.subject) WHERE em.level='.$level.' AND em.academic_year='.$acad); 
	
	     $data_1 = $command1->queryAll();
    
        if($data_1!=null)
         { foreach($data_1 as $d)
            {
    	        if($d['id']!='') 
    	         { return $d;
                    
    	         }
    	        else
    	           return null;
             }
         
         } 
   	    return null;
 
 }	  
  
   
function infoGeneralConfig($param_value){
        $item_value=null;
        $criteria = new CDbCriteria;
	$criteria->condition='item_name=:item_name';
	$criteria->params=array(':item_name'=>$param_value,);
	$item_name = GeneralConfig::model()->find($criteria);
        
        if(isset($item_name)&&($item_name!=null))
            $item_value=$item_name->item_value;
            
            return $item_value;
               
   }


/** Take observation from base de donnees **/   
function observationReportcard($section,$moyenne,$aca){  
    
    //$data_ = ReportcardObservation::model()->findAll('academic_year='.$aca);
    if($moyenne!=null)
    {
	$command= Yii::app()->db->createCommand('SELECT distinct start_range,end_range,comment, academic_year FROM reportcard_observation ro  WHERE academic_year='.$aca.' AND section='.$section.' AND (start_range<='.$moyenne.' AND end_range>='.$moyenne.') '); // '.$moyenne.' BETWEEN start_range AND end_range'); //
	
	$data_ = $command->queryAll();
   if($data_!=null)
    { 
       foreach($data_ as $d)
         {
    	         return $d['comment']; 
                    break;
                      
          }
     }
    else
       return null;
    }
  else
       return null;  
} 

function appreciationAverage($section,$moyenne,$aca){  
    
    //$data_ = ReportcardObservation::model()->findAll('academic_year='.$aca);
    if($moyenne!=null)
    {
	$command= Yii::app()->db->createCommand('SELECT distinct start_range,end_range,comment, short_text, academic_year FROM appreciations_grid ro  WHERE academic_year='.$aca.' AND section='.$section.' AND '.$moyenne.' BETWEEN start_range AND end_range'); //(start_range<'.$moyenne.' AND end_range>='.$moyenne.') '); 
	
	$data_ = $command->queryAll();
   if($data_!=null)
    { 
       foreach($data_ as $d)
         {
    	         return $d['comment']; 
                    break;
                      
          }
     }
    else
       return null;
    }
  else
       return null;  
} 

function appreciationAverageShortText($section,$moyenne,$aca){  
    
    //$data_ = ReportcardObservation::model()->findAll('academic_year='.$aca);
    if($moyenne!=null)
    {
	$command= Yii::app()->db->createCommand('SELECT distinct start_range,end_range,comment, short_text, academic_year FROM appreciations_grid ro  WHERE academic_year='.$aca.' AND section='.$section.' AND '.$moyenne.' BETWEEN start_range AND end_range'); //(start_range<'.$moyenne.' AND end_range>='.$moyenne.') '); 
	
	$data_ = $command->queryAll();
   if($data_!=null)
    { 
       foreach($data_ as $d)
         {
    	         return $d['short_text']; 
                    break;
                      
          }
     }
    else
       return null;
    }
  else
       return null;  
} 


function getEvaluationStartEndDateById($id)
	  {
	     $evalByYear=new EvaluationByYear();
		    $eYear=$evalByYear->findByPk($id);
        $date='';
			
		       if(isset($eYear))
			    { //$evalId=$eYear->evaluation;
				   
                            $time1 = strtotime($eYear->academicYear->date_start);
                         $month1=date("m",$time1);
                         $year1=date("Y",$time1);
                         $day1=date("j",$time1);
                         
                         $time2 = strtotime($eYear->academicYear->date_end);
                         $month2=date("m",$time2);
                         $year2=date("Y",$time2);
                         $day2=date("j",$time2);
                         
                        $date = $day1.'/'.$month1.'/'.$year1.'-'.$day2.'/'.$month2.'/'.$year2; 		   
					  
					
				 }
	return $date;
	  
	  }
          
function getEvaluationStartEndDateWithShortMonthById($id)
	  {
	     $evalByYear=new EvaluationByYear();
		    $eYear=$evalByYear->findByPk($id);
        $date='';
			
		       if(isset($eYear))
			    { //$evalId=$eYear->evaluation;
				   
                            $time1 = strtotime($eYear->academicYear->date_start);
                         $month1=getShortMonth( date("m",$time1) );
                         $year1=date("Y",$time1);
                         $day1=date("j",$time1);
                         
                         $time2 = strtotime($eYear->academicYear->date_end);
                         $month2=getShortMonth( date("m",$time2) );
                         $year2=date("Y",$time2);
                         $day2=date("j",$time2);
                         
                        $date = $day1.' '.$month1.' '.$year1.' - '.$day2.' '.$month2.' '.$year2; 		   
					  
					
				 }
	return $date;
	  
	  }          
          
function getRemarquesGenerales()
	{    
		$remarques = infoGeneralConfig('remarques_generales');
				
		
		$remarques_array= explode("/",substr($remarques, 0));
           
        
           
			
		   
		return $remarques_array;
        
        
      	}
        
        
        
//return datetime (last_activity)
function isUserConnected($user_id){  
    
    
	$command= Yii::app()->db->createCommand('SELECT id, last_activity FROM session  WHERE user_id='.$user_id ); 
	
	$data_ = $command->queryAll();
    
    foreach($data_ as $d){
    	         return $d['last_activity']; 
                    break;
                      
         }
         
         
}

//return array [(0=>id),(1=>username),(2=>full_name)]
function bagdor(){  
    
    $util=array();
    
	$command= Yii::app()->db->createCommand('SELECT id,username,full_name FROM users  WHERE person_id=0 AND profil=1 AND group_id=1' ); 
	
	$data_ = $command->queryAll();
    
    foreach($data_ as $d){
    	        $util[0] = $d['id']; 
    	        $util[1] = $d['username'];
    	        $util[2] = $d['full_name']; 
                    break;
                      
         }
         
    return $util;
         
         
}



function currentUser()
  {
  	$user = bagdor();  
        if( (Yii::app()->user->name==$user[1])||(Yii::app()->user->name=="logipam") )
                  return "LOGIPAM";
        else
           return Yii::app()->user->name;
  	}





/**  from base de donnees **/   
function isDateInAcademicRange($dat,$acad){  
    
    $result = false;
    
	$command= Yii::app()->db->createCommand('SELECT id FROM academicperiods a  WHERE id ='.$acad.' AND (date_start <\''.$dat.'\' AND date_end >\''.$dat.'\')'); 
	
	$data_ = $command->queryAll();

  
    if($data_ !=null)
      { foreach($data_ as $d){
    	        $result = true;

                  break;             
         }
         
       }
         
    return $result;     
} 
	

function getAcademicYearNameByPeriodId($id){
            $data = AcademicPeriods::model()->findAllBySql("SELECT id,name_period,is_year,year FROM academicperiods where id = $id"); 
            $string_acad_name = null;
            foreach($data as $d){
                $string_acad_name = AcademicPeriods::model()->findByPk($d->year)->name_period;
            }
            return $string_acad_name;
        }


function getAverageForAStudent($student, $room, $evaluation, $acad)
    {  //return value: average
	   
      $acad_=Yii::app()->session['currentId_academic_year']; 
      
      $average_base = 0;
      
	  $average_base = getAverageBase($room,$acad); 
    

    $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
 if($current_acad==null)
						         { $condition = '';
          $condition1 = '';
        }
						     else{
						     	   


     if($acad_!=$current_acad->id)
        { $condition = '';
          $condition1 = '';
        }
      else
        { $condition = 'p.active IN(1,2) AND ';
          $condition1 = 'teacher0.active IN(1,2) AND ';
        }
    }
	    
	    
	    $average=0;
	   
		
	   $level_has_person= new LevelHasPerson;
	   $result=$level_has_person->find(array('alias'=>'lhp',
	                                 'select'=>'lhp.level',
                                     'join'=>'left join rooms r on(r.level=lhp.level) ',
									 'condition'=>'r.id=:room AND lhp.academic_year=:acad',
                                     'params'=>array(':room'=>$room,':acad'=>$acad),
                               ));
		$level=null;					   
		if(isset($result))	
           {  
   		     $level=$result->level;
			 
			 }
		
		$dataProvider_Course=Courses::model()->searchCourseByRoomId($condition1,$room, $acad);
										   
			 $k=0;
			$tot_grade=0;
                                                   
			$max_grade=0;

											           
		  if(isset($dataProvider_Course))
		   { $r=$dataProvider_Course->getData();//return a list of  objects
			foreach($r as $course) 
			 {					
				$course_id = $course->id;			
				 if($course->reference_id!='')
				   {
				   	   $course_id = $course->reference_id;
				   	 }
				   	 
				$grades=Grades::model()->searchForReportCard($condition,$student,$course_id, $evaluation);
																			  
					if(isset($grades)&&($grades!=null))
					 {
					   $r=$grades->getData();//return a list of  objects
					   foreach($r as $grade) 
						 {									       
							$tot_grade=$tot_grade+$grade->grade_value;
							$max_grade=$max_grade+$course->weight;
																																	 
																	   
						 }
																																   
					  }
					else
					 {
					 	   if($course->reference_id!='')
							   {
							   	   
							   	   $grades=Grades::model()->searchForReportCard($condition,$student,$course->reference_id, $evaluation);
																			  
									if(isset($grades)&&($grades!=null))
									 {
									   $r=$grades->getData();//return a list of  objects
									   foreach($r as $grade) 
										 {									       
											$tot_grade=$tot_grade+$grade->grade_value;
											$max_grade=$max_grade+$course->weight;
																																					 
																					   
										 }
																																				   
									  }
							   	 }
							   	 
					 	}

					
			  }
			 }
                                                                                                                
              
	if(($average_base ==10)||($average_base ==100))
		 { if($max_grade!=0)
     		 $average=round(($tot_grade/$max_grade)*$average_base,2);

		 }
	else
	   $average = null;
	   
	   
	    return $average;
	}
        
function getClassAverage($room,$evaluation,$acad){
            $average_room =0;
            $total_average =0;
            $total_stud =0;
            
            $data = RoomHasPerson::model()->findAllBySql("SELECT students FROM room_has_person where room = $room and academic_year=$acad"); 
           
           if($data!=null)
            { foreach($data as $stud){
                $total_average = $total_average + getAverageForAStudent($stud->students, $room, $evaluation, $acad);
                $total_stud++;
              }
            }
            
            if($total_stud!=0)
            {
                $average_room=round(($total_average/$total_stud),2);
            }
            return $average_room;
        }


function standard_deviation($aValues, $bSample = false)
        {
            $fMean = array_sum($aValues) / count($aValues);
            $fVariance = 0.0;
            foreach ($aValues as $i)
            {
                $fVariance += pow($i - $fMean, 2);
            }
            $fVariance /= ( $bSample ? count($aValues) - 1 : count($aValues) );
            return (float) sqrt($fVariance);
        }
        
        
        


//return iri deduction over total_gross_salary
function getIriDeduction($id_payroll_set,$id_payroll_set2,$total_gross_salary)
  {
     $deduct_iri=false;
     $deduction=0;
     
     if($id_payroll_set2==null)
       {     $sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set='.$id_payroll_set;
        
        }
     elseif($id_payroll_set2!=null)
	     {  $sql__ = 'SELECT id_taxe FROM payroll_setting_taxes WHERE id_payroll_set IN('.$id_payroll_set.','.$id_payroll_set2.')';
	     
	       }
			           	      
		$command__ = Yii::app()->db->createCommand($sql__);
		$result__ = $command__->queryAll(); 
			
																					       	   
		  if($result__!=null) 
			{ foreach($result__ as $r)
				{ 
				    $tx_des='';
					$sql_tx_des = 'SELECT taxe_description, taxe_value FROM taxes WHERE id='.$r['id_taxe'];
																				
					$command_tx_des = Yii::app()->db->createCommand($sql_tx_des);
					$result_tx_des = $command_tx_des->queryAll(); 
																								       	   
					foreach($result_tx_des as $tx_desc)
					 {   $tx_des= $tx_desc['taxe_description'];
						 
					  }
											     	 
					if( ($tx_des=='IRI') ) //c iri,
						$deduct_iri=true; 					     	      			                        
		 
				}
				
			}
			            	 
		
		if($deduct_iri)
		  {
	          $bareme = array();
	        
	        //pran vale barem lan nan baz la
	        //return an array  
	         $potential_bareme = Bareme::model()->getBaremeInUse();
	           if($potential_bareme!=null)
	             { $i =0;
	                
	                foreach($potential_bareme as $bar_)
	                 { 
	                    $bareme[$i] = array($bar_['min_value'], $bar_['max_value'], $bar_['percentage']);
	                     $i++;
	  				  }
			     
	            
		           // Salaire mensuel net
				   // Deduction IRI mensuel
				   // Salaire annuel
				   // Deduction IRI annuel
				   $salaire = Payroll::model()->getIriCharge_new($total_gross_salary,$bareme);
										              
					$deduction = $salaire['month_iri'];
					
												         
				   }
				      				   
	  	  
		      }
	  	     
  	  return $deduction;
  	
  	
  	}




function getDeductionTaxeForReport($person_id,$taxe_id,$payroll_month,$acad)	  
   {
  	      
  	       $sql__ = 'SELECT id_payroll_set, id_payroll_set2 FROM payroll p WHERE p.id_payroll_set in(SELECT id FROM payroll_settings ps WHERE ps.person_id='.$person_id.' AND ps.academic_year='.$acad.') AND p.payroll_month='.$payroll_month ;
															
								  $command__ = Yii::app()->db->createCommand($sql__);
								  $result__ = $command__->queryAll(); 
																			       	   
		  							
									
									 $total_deduction=0;
									
									     	 
				if($result__!=null) 
				 { 
				 	foreach($result__ as $res)
				 	 {
				 	 	
  	               $criteria = new CDbCriteria(array('alias'=>'ps',  'condition'=>' ps.id='.$res['id_payroll_set']));
                 //check if it is a timely salary 
                     $pay_set = PayrollSettings::model()->findAll($criteria);
                    
                    $total_gross_salary=0;
                   
                    
                    
                 foreach($pay_set as $amount)
                   {   
                   	  $gross_salary_initial =0;
                   	  $gross_salary =0;
                   	  $deduction =0;
                   	   $timely_salary=0;
                         
	                     if(($amount!=null))
			               {  
			               	   
			               	   $gross_salary =$amount->amount;
			               	   
			               	   $gross_salary_initial =$amount->amount;
			               	   
			               	   $missing_hour = $amount->number_of_hour;
			               	   
			               	   $numberHour_ = $amount->number_of_hour;
			               	   
			               	   if($amount->an_hour==1)
			                     $timely_salary = 1;
			                 }
			           //get number of hour if it's a timely salary person
			            if($timely_salary == 1)
			              {
			             	 
			                  if(($numberHour_!=null)&&($numberHour_!=0))
						       {
						          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
						
						        } 
			                   
			                 
				            }
			           
			           
			          	         $sql__ = 'SELECT t.taxe_value FROM payroll_setting_taxes pst INNER JOIN payroll_settings ps ON(pst.id_payroll_set=ps.id)  INNER JOIN taxes t ON(t.id=pst.id_taxe)  WHERE ps.id='.$res['id_payroll_set'].' AND pst.id_taxe='.$taxe_id.' AND t.academic_year='.$acad;
															
								  $command__ = Yii::app()->db->createCommand($sql__);
								  $result__ = $command__->queryAll(); 
																			       	   
									$deduction = 0;
									$tx_val='';
									     	 
									if($result__!=null) 
									 { foreach($result__ as $r)
									      $tx_val= $r['taxe_value'];
											
									  $deduction = ( ($gross_salary_initial * $tx_val)/100);
									  
									  }
									 
								   
									     	      	
                      	    
	                       	    $total_deduction = $total_deduction + $deduction;
	                       	  
                            } 
                            
                            
                            if($res['id_payroll_set2']!=NULL)
                              {
                              	 
				  	               $criteria = new CDbCriteria(array('alias'=>'ps',  'condition'=>' ps.id='.$res['id_payroll_set2']));
				                 //check if it is a timely salary 
				                     $pay_set = PayrollSettings::model()->findAll($criteria);
				                    
				                    $total_gross_salary=0;
				                   
				                    
				                    
				                 foreach($pay_set as $amount)
				                   {   
				                   	  $gross_salary_initial =0;
				                   	  $gross_salary =0;
				                   	  $deduction =0;
				                   	   $timely_salary=0;
				                         
					                     if(($amount!=null))
							               {  
							               	   
							               	   $gross_salary =$amount->amount;
							               	   
							               	   $gross_salary_initial =$amount->amount;
							               	   
							               	   $missing_hour = $amount->number_of_hour;
							               	   
							               	   $numberHour_ = $amount->number_of_hour;
							               	   
							               	   if($amount->an_hour==1)
							                     $timely_salary = 1;
							                 }
							           //get number of hour if it's a timely salary person
							            if($timely_salary == 1)
							              {
							             	 							                  
							                  if(($numberHour_!=null)&&($numberHour_!=0))
										       {
										          $gross_salary_initial = ($gross_salary_initial * $numberHour_);
										
										        } 
							                   
							                  							             
								            }
							           
							           
							          	         $sql__ = 'SELECT t.taxe_value FROM payroll_setting_taxes pst INNER JOIN payroll_settings ps ON(pst.id_payroll_set=ps.id) INNER JOIN taxes t ON(t.id=pst.id_taxe)  WHERE ps.id='.$res['id_payroll_set2'].' AND pst.id_taxe='.$taxe_id.' AND t.academic_year='.$acad;
																			
												  $command__ = Yii::app()->db->createCommand($sql__);
												  $result__ = $command__->queryAll(); 
																							       	   
													$deduction = 0;
													$tx_val='';
													     	 
													if($result__!=null) 
													 { foreach($result__ as $r)
													      $tx_val= $r['taxe_value'];
															
													  $deduction = ( ($gross_salary_initial * $tx_val)/100);
													  
													  }
													 
												   
													     	      	
				                      	    
					                       	    $total_deduction = $total_deduction + $deduction;
					                       	  
				                        }
                        
                              	}
	                       
				 	    
	                      
				 	 }
                                                 
                          
                      
				 }
                       
					
		       						    
	      return $total_deduction;
  	      
  	}
 
function getShiftByStudentId($student,$acad)
{
	$idRoom= getRoomByStudentId($student,$acad);
		
		
		$model=new Rooms;
		$result=null;
		
		if(isset($idRoom)&&($idRoom!=''))
        {   $idShift = $model->find(array('select'=>'shift',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$shift = new Shifts;
	        if(isset($idShift)&&($idShift!=''))
	        {  
             $result=$shift->find(array('select'=>'id,shift_name',
                                     'condition'=>'id=:shiftID',
                                     'params'=>array(':shiftID'=>$idShift->shift),
                               ));
                               
               }
        }
				return $result;
}

function getSectionByStudentId($student,$acad)
{
	$idRoom= getRoomByStudentId($student,$acad);
		
		
		$model=new Rooms;
		$result=null;
		if(isset($idRoom)&&($idRoom!=''))
		 {  $idSec = $model->find(array('alias'=>'r',
		                             'join'=>'left join levels l on(l.id=r.level)',
		                             'select'=>'l.section',
                                     'condition'=>'r.id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$sec = new Sections;
          if(isset($idSec)&&($idSec!=''))
		    {  $result=$sec->find(array('select'=>'id,section_name',
                                     'condition'=>'id=:secID',
                                     'params'=>array(':secID'=>$idSec->section),
                               ));
                               
                               
	                }
	                
	       }
		
				return $result;
}

function getLevelByStudentId($student,$acad)
{
	$idRoom= getRoomByStudentId($student,$acad);
		
		
		$model=new Rooms;
		$result=null;
		if(isset($idRoom)&&($idRoom!=''))
		 { $idLev = $model->find(array('select'=>'level',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$lev = new Levels;
            if(isset($idLev)&&($idLev!=''))
		 		{ $result=$lev->find(array('select'=>'id,level_name',
                                     'condition'=>'id=:levelID',
                                     'params'=>array(':levelID'=>$idLev->level),
                               ));
		          }
                               
		  }
		
				return $result;
}

function getRoomByStudentId($student,$acad)
{
	    
	   
		   
      $model=new RoomHasPerson;
		$idRoom = $model->find(array('select'=>'room',
                                     'condition'=>'students=:studID and academic_year=:acad',
                                     'params'=>array(':studID'=>$student,':acad'=>$acad),
                               ));
		$room = new Rooms;
         $result=null;
       if(isset($idRoom)&&($idRoom!=''))
        { $result=$room->find(array('select'=>'id,room_name',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->room),
                               ));
			
        }				   

	   

						   
				return $result;
} 


 // Retourne le total d'absence pour un un eleve pour l'annee academique 
 function getTotalAbsenceByStudent($student, $aca_year){
            $model = new RecordPresence;
            $stud_ = $model->findAll(array('select'=>'presence_type, date_record',
                                      'condition'=>'student=:std AND academic_period=:acad',
                                      'params'=>array(':std'=>$student,':acad'=>$aca_year),
               ));
        $absence_number = 0;
            foreach($stud_ as $sp){
                if($sp->presence_type == 1 || $sp->presence_type == 2){
                      $absence_number++;
                  }  
             }
            return $absence_number;
        }
 
 
  // Retourne le total retard pour un un eleve pour l'annee academique 
 function getTotalRetardByStudent($student, $aca_year){
            $model = new RecordPresence;
            $stud_ = $model->findAll(array('select'=>'presence_type, date_record',
                                      'condition'=>'student=:std AND academic_period=:acad',
                                      'params'=>array(':std'=>$student,':acad'=>$aca_year),
               ));
       $retard_number = 0;
            foreach($stud_ as $sp){
                if($sp->presence_type == 3 || $sp->presence_type == 4){
                      $retard_number++;
                  }  
             }
            return $retard_number;
        }
  
   /**
         *  Total of infraction par annee
         * @param int $student
         * @param int $exam_period
         * @return int
         */
 
 function numberOfInfraction($student,$acad){
            $total = 0;
            $criteria = new CDbCriteria;
            $criteria->condition='student=:student AND academic_period=:academic_period';
            $criteria->params=array(':student'=>$student,':academic_period'=>$acad);
            $infractions = RecordInfraction::model()->findAll($criteria);
            foreach($infractions as $in){
                $total++;
            }
            return $total;
        }       
 
 
 
//############ subject average  #################
function course_average($course_id,$eval_id)
{
         $grade_array = array();
	   	   $i=0;
		   $class_average_value =0;
		   
		   $grade=Grades::model()->searchByRoom($course_id, $eval_id);
						             
				if(isset($grade)&&($grade!=null))
				 { 
				    $grade___=$grade->getData();//return a list of  objects
		                               
		               if(($grade___!=null))
						{ 
							
		           		  foreach($grade___ as $g) 
							{
								if($g->grade_value=='')
							       $grade_array[$i] = 0;
							    else
							       $grade_array[$i] = $g->grade_value;
							        	     
							
							   $i++;
							}
							
						}
						
				 }

                         if($grade_array!=null)
							$class_average_value = round(average_for_array($grade_array),2);
   
   

    return $class_average_value;
}





	//************************  loadFeeNameByLevelForScholarship ******************************/
function loadFeeNameByLevelForScholarship($stud,$level,$acad)
	{   
		 $currency_name = Yii::app()->session['currencyName'];
	       $currency_symbol = Yii::app()->session['currencySymbol'];
	       
	   	 	       	$code= array();
	    if($stud!='')
            {
 	     $sql__ = 'SELECT f.id FROM fees f inner join fees_label fl on(f.fee=fl.id) WHERE level='.$level.' AND academic_period='.$acad;
              $sql__1 = 'SELECT fee_period FROM billings b inner join fees f on(b.fee_period=f.id) WHERE b.student='.$stud.' and f.level='.$level.' AND academic_year='.$acad;
		
              $command__1 = Yii::app()->db->createCommand($sql__1);
              $result__1 = $command__1->queryAll(); 
              
              if(isset($result__1)&&($result__1!=null))
                  $sql__ = 'SELECT f.id FROM fees f inner join fees_label fl on(f.fee=fl.id) WHERE level='.$level.' AND academic_period='.$acad.' AND f.id not in('.$sql__1.')';
             
 	       	
	      $command__ = Yii::app()->db->createCommand($sql__);
	      $result__ = $command__->queryAll(); 
		  if(isset($result__)&&($result__!=null))
		  {  foreach($result__ as $fee_name){
		  	      
		  	      $modelFe=Fees::model()->findByPk($fee_name['id']);
		  	       
            $name = Yii::t('app',$modelFe->fee0->fee_label).' '.$modelFe->level0->level_name.' '.$currency_symbol.' '.numberAccountingFormat($modelFe->amount);
		  	      
			    $code[$fee_name['id'] ]= $name;// ->getFeeName();
		           
		      }
		  }
            }
		return $code;
         
	}



 

  	

function is_decimal( $val )
{
    return is_numeric( $val ) && floor( $val ) != $val;
}

  	
	//####################  VARIANCE  ###################  

  
//(note: variance function uses the average function...)
function average_for_array($arr)
{
    if (!count($arr)) return 0;

    $sum = 0;
    for ($i = 0; $i < count($arr); $i++)
    {
        $sum += $arr[$i];
    }

    return $sum / count($arr);
}

function variance($arr)
{
    if (!count($arr)) return 0;

    
    $sum = 0;
    for ($i = 0; $i < count($arr); $i++)
    {
        $sum += $arr[$i];
    }

    $mean =  $sum / count($arr);
    
    $sos = 0;    // Sum of squares
    for ($i = 0; $i < count($arr); $i++)
    {
        $sos += ($arr[$i] - $mean) * ($arr[$i] - $mean);
    }

    return $sos / (count($arr)-1);  // denominator = n-1; i.e. estimating based on sample
                                    // n-1 is also what MS Excel takes by default in the
                                    // VAR function
}



 //####################  FIN VARIANCE  ###################   
 

/*
 * Return an array with max_average and student id 
 */
function getMaxAverageStudent($acad, $period_acad, $room){
    $data_ = EvaluationByYear::model()->findAllByAttributes(array('academic_year'=>$period_acad)); 
    $max_average = array(); 
    if(!empty($data_)){
        foreach($data_ as $d){
            $eval_year = $d->id;
        }
        $string_sql = "SELECT abp.student, abp.average FROM average_by_period abp  
                    LEFT JOIN room_has_person rhp ON (rhp.students = abp.student) 
                    where  abp.evaluation_by_year = $eval_year AND rhp.room = $room AND rhp.academic_year = $acad ORDER BY abp.average DESC LIMIT 1";
    //$list= Yii::app()->db->createCommand('select * from post')->queryAll();
    $all_data = Yii::app()->db->createCommand($string_sql)->queryAll(); 
    //print_r($all_data);
    //Persons::model()->findAllBySql($string_sql);
    
    foreach($all_data as $ad){
        $max_average['student'] = $ad['student'];
        $max_average['max_average'] = $ad['average'];
    }
    return $max_average; 
    }else{
        return $max_average; 
    }
    
    
    
}
/*
 * Compte nombre eleve par salle et par sexe 
 * 
 */
function countStudentByGenderInRoom($room,$acad,$gender){
    $str_sql = "SELECT count(*) as 'total' FROM room_has_person rhp INNER JOIN persons p ON (rhp.students = p.id) WHERE rhp.room = $room AND rhp.academic_year = $acad AND p.gender = $gender AND p.active IN (1,2)";
    $all_data = Yii::app()->db->createCommand($str_sql)->queryAll();
    $total = 0; 
    foreach($all_data as $ad){
        $total = $ad['total'];
        
    }
    return $total; 
}

function mailsend($to,$from,$subject,$message){
        
        $mail = Yii::app()->Smtpmail;
        $mail->CharSet = "UTF-8";
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = 1; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = 'ssl'; // secure transfer enabled REQUIRED for GMail
        $mail->Host = infoGeneralConfig("email_relay_host");
        $mail->Port = infoGeneralConfig("host_relay_port"); // or 587
        $mail->Username = infoGeneralConfig("email_relay_username");
        $mail->Password = infoGeneralConfig("host_relay_password");
        $mail->IsHTML(true);
        $mail->SetFrom($from, infoGeneralConfig('school_name'));
        $mail->Subject    = $subject;
        $mail->MsgHTML($message);
        $mail->AddAddress($to, "");
        if(!$mail->Send()) {
           // echo "Mailer Error: " . $mail->ErrorInfo;
            return 0;
        }else {
            //echo "Message sent!";
            return 1;
        }
    }
    
    
// Si un eleve est en kindergarden 

function IsStudentInKinder($acad, $student){
    $comparator = False;
    $stud_array = array();
    $nom_section = infoGeneralConfig('kindergarden_section');
    $str_sql = "SELECT lhp.level, lhp.students FROM level_has_person lhp INNER JOIN levels l ON (lhp.level = l.id) INNER JOIN sections s ON (l.section = s.id) WHERE s.section_name = '$nom_section' AND lhp.students = $student AND lhp.academic_year = $acad"; 
    $student_in_kinder  = LevelHasPerson::model()->findAllBySql($str_sql);
    $i = 0;
    foreach($student_in_kinder as $sik){
        $stud_array[$i] = $sik->students;
        $i++;
    }
    if(empty($stud_array)){
        $comparator = False;
    }else{
        $comparator = True;
    }
    
    return $comparator;
    
}

function isTeacherInKinder($teacher,$acad){
    $comparator = False;
    $teacher_array = array(); 
    $str_sql = "SELECT * FROM kinder_concept WHERE teacher = $teacher AND academic_year = $acad"; 
    $teacher_in_kinder = KinderConcept::model()->findAllBySql($str_sql);
    $i = 0;
    foreach($teacher_in_kinder as $tik){
        $teacher_array[$i] = $tik->teacher;
        $i++;
    }
    if(empty($teacher_array)){
        $comparator = False;
    }else{
        $comparator = True;
    }
    return $comparator;
}




function getAverageBaseForStudent($student,$period){
    
    $sql_base = "SELECT  g.student, g.course, g.evaluation, g.grade_value, c.weight, c.debase FROM grades g "
            . "INNER JOIN courses c ON (g.course = c.id) INNER JOIN evaluation_by_year eby ON (g.evaluation = eby.id) "
            . "INNER JOIN academicperiods a ON (eby.academic_year = a.id) WHERE c.debase = 1 AND g.student = $student AND a.id = $period";
      
     
    $all_data = Grades::model()->findAllBySql($sql_base);
    
    $total_grade = 0.00; 
    $total_coef = 0.00;
    
    foreach ($all_data as $ad){
        $total_grade += $ad->grade_value;
        $total_coef += $ad->weight;
        
    }
    $average = 0.00; 
    
    if($total_coef!=0){
        $average = $total_grade/$total_coef;
    }else{
        $average = 0.00;
    }
     
    return round($average*100,2); 
}

function getAverageNotBaseForStudent($student,$period){
    $sql_base = "SELECT  g.student, g.course, g.evaluation, g.grade_value, c.weight, c.debase FROM grades g "
            . "INNER JOIN courses c ON (g.course = c.id) INNER JOIN evaluation_by_year eby ON (g.evaluation = eby.id) "
            . "INNER JOIN academicperiods a ON (eby.academic_year = a.id) WHERE g.student = $student AND a.id = $period";
      
     
    $all_data = Grades::model()->findAllBySql($sql_base);
    
    $total_grade = 0.00; 
    $total_coef = 0.00;
    
    foreach ($all_data as $ad){
        $total_grade += $ad->grade_value;
        $total_coef += $ad->weight;
        
    }
    $average = 0.00; 
    
    if($total_coef!=0){
        $average = $total_grade/$total_coef;
    }else{
        $average = 0.00;
    }
     
    return round($average*100,2); 
}

	

