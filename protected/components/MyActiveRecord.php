<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

class MyActiveRecord extends CActiveRecord {
    
    private static $dbformalms = null;

    protected static function getFormalmsDbConnection()
    {
        if (self::$dbformalms !== null){
            return self::$dbformalms;
        }
        else
        {
            self::$dbformalms = Yii::app()->dbformalms;
            if (self::$dbformalms instanceof CDbConnection)
            {
                self::$dbformalms->setActive(true);
                return self::$dbformalms;
            }
            else{
                throw new CDbException(Yii::t('yii','Active Record requires a "db" CDbConnection application component.'));
            }
        }
    }
    
}