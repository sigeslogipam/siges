<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function isRoomMigrate($id){
    $one_elearning_room = CoreGroup::model()->findByAttributes(array('groupid'=>$id));
    if(isset($one_elearning_room->groupid)){
        if($id==$one_elearning_room->groupid){
            return true; 
        }
        else{
            return false; 
        }
    }else{
        return false;
    }
    
}

function isCourseMigrate($code){
    $one_elearning_course = LearningCourse::model()->findByAttributes(array('code'=>$code));
    if(isset($one_elearning_course->code)){
        if($code==$one_elearning_course->code){
            return true; 
        }
        else{
            return false; 
        }
    }else{
        return false;
    }
    
}

function isStudentMigrate($id){
    $one_student = CoreUser::model()->findByPk($id); 
    if(isset($one_student->idst)){
        if($id==$one_student->idst){
            return true; 
        }else{
            return false;
        }
    }else{
        return false; 
    }
            
}

function setLanguageCode(){
    $code_siges = Yii::app()->language; 
    switch($code_siges){
        case 'fr':
            return 'french';
            break;
        case 'en':
            return 'english';
            break;
        case 'ht':
            return 'kreyol';
            break;
        default :
            return 'french';
    }
}

function infoRoles($param_value){
        $item_value=null;
        $criteria = new CDbCriteria;
	$criteria->condition = 'roleid=:item_name';
	$criteria->params=array(':item_name'=>$param_value,);
	$item_name = CoreRole::model()->find($criteria);
        
        if(isset($item_name)&&($item_name!=null))
            $item_value = $item_name->idst;
            
            return $item_value;
               
   }
   
function infoGroup($param_value){
        $item_value=null;
        $criteria = new CDbCriteria;
	$criteria->condition = 'groupid=:item_name';
	$criteria->params=array(':item_name'=>$param_value,);
	$item_name = CoreGroup::model()->find($criteria);
        
        if(isset($item_name)&&($item_name!=null))
            $item_value = $item_name->idst;
            
            return $item_value;
               
   }  
   
 function isStudentGradesAlreadySave($student, $course, $period){
     $sql_g = "SELECT id FROM grades where student = $student AND course = $course AND evaluation = $period"; 
     $grades = Grades::model()->findAllBYSql($sql_g);
     $id_grade = 0; 
     foreach($grades as $g){
         if(isset($g->id)){
             $id_grade = $g->id; 
         }
     }
     
     return $id_grade; 
     
 }  
