<?php
/* @var $this KinderPeriodController */
/* @var $model KinderPeriod */

$this->breadcrumbs=array(
	'Kinder Periods'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KinderPeriod', 'url'=>array('index')),
	array('label'=>'Create KinderPeriod', 'url'=>array('create')),
	array('label'=>'View KinderPeriod', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage KinderPeriod', 'url'=>array('admin')),
);
?>

<h1>Update KinderPeriod <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>