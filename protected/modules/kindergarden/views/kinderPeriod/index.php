<div class="kindertest">
  <?php
    echo $this->renderPartial('//layouts/navBaseKinder',NULL,true);
?>

<div class="row-fluid kinder1">
    <div class="span2">
        <ul class="nav nav-pills nav-stacked">
            <li class="active" id="period"><a href="#period" data-toggle="tab"><?= Yii::t('app','Evaluation period'); ?></a></li>
            <li id="mention"><a href="#mention" data-toggle="tab" ><?= Yii::t('app','Mention'); ?></a></li>
            <li id="category"><a href="#category" data-toggle="tab" ><?= Yii::t('app','Concept category'); ?></a></li>
            <li id="concept"><a href="#concept" data-toggle="tab"><?= Yii::t('app','Concept') ?></a></li>
            <li id="teacher"><a href="#teacher" data-toggle="tab"><?= Yii::t('app','Add teacher') ?></a></li>
        </ul>
    </div>
    <div class="span10">
        <div id="plas-kontni">

        </div>
    </div>
</div>

<?php
$baseUrl = Yii::app()->baseUrl;
?>
<script>

    $(document).ready(function(){

         $.get('<?= $baseUrl?>/index.php/kindergarden/kinderPeriod/create',{},function(data){
                            $('#plas-kontni').html(data);
                        });

        $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
            var target = $(e.target).attr("href") // activated tab
            switch(target){
                case '#period':{

                        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderPeriod/create',{},function(data){
                            $('#plas-kontni').html(data);
                        });

                    }
                break;
                case '#mention' : {
                         $.get('<?= $baseUrl?>/index.php/kindergarden/kinderMention/create',{},function(data){
                            $('#plas-kontni').html(data);
                        });
                    }
                break;
                case '#category' : {
                    $.get('<?= $baseUrl?>/index.php/kindergarden/kinderCatConcept/create',{},function(data){
                            $('#plas-kontni').html(data);
                        });
                }
                break;
                case '#concept' : {
                    $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/create',{},function(data){
                            $('#plas-kontni').html(data);
                        });
                }
                break;
                case '#teacher' : {
                    $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/createteacher',{},function(data){
                            $('#plas-kontni').html(data);
                        });
                }
                break;
                


            }
            //alert(target);
        });
        /*
        $('#mention').click(function(){
            $('#period').removeClass('active');
            $('#mention').addClass('active');
        });
        */
    });
</script>
</div>
