<?php
/* @var $this KinderPeriodController */
/* @var $model KinderPeriod */
$acad=Yii::app()->session['currentId_academic_year']; 

?>
<div id="dash">
    <div class="span3"><h2><?= Yii::t('app','List of period'); ?></h2></div>
    <div class="span3">
        <div class="span4">
            <a href="#" id="add-period"><i class="fa fa-plus"> <?= Yii::t('app','Add'); ?></i></a>
        </div>
        <!-- <div class="span4"></div> -->
    </div>
</div>

<div id="list-period">
    ...
</div>



<!-- New Modal from scratch -->
<!-- Modal -->
  <div class="modal fade modal-lg" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add period'); ?><span id="en-tete"></span></h4>
        </div>
        <div class="modal-body">
            <div class="form">
                <div id="resp_form_siges">
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="period-name" class="required"><?= Yii::t('app','Period name'); ?></label>
                        <input size="60" maxlength="45" placeholder="<?= Yii::t('app','Period name'); ?>" name="period-name" id="period-name" type="text"></label>
                    </div>
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="date-start" class="required"><?= Yii::t('app','Date Start'); ?></label>
                        <input size="60" maxlength="45" placeholder="<?= Yii::t('app','Date Start'); ?>" name="date-start" id="date-start" type="text"></label>
                    </div>
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="date-end" class="required"><?= Yii::t('app','Date End'); ?></label>
                        <input size="60" maxlength="45" placeholder="<?= Yii::t('app','Date End'); ?>" name="date-end" id="date-end" type="text"></label>
                    </div>
                    <br/>
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="is-last-period"><?= Yii::t('app','Is last period ?'); ?></label>
                        <input name="is-last-period" id="is-last-period" type="checkbox"></label>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="save-period"><?= Yii::t('app','Save'); ?></button>  
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>




<?php 
$baseUrl = Yii::app()->baseUrl;
?>

<script>
    $(document).ready(function(){
        
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderPeriod/refreshperiod',{},function(data){
            $('#list-period').html(data);
        });
        
        $('#date-start').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
        
        $('#date-end').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
        
        $('#save-period').click(function(){
            var period_name = $('#period-name').val(); 
            var date_start = $('#date-start').val(); 
            var date_end = $('#date-end').val(); 
            var is_last_period = 0; 
            if($('#is-last-period').is(":checked")){
                is_last_period = 1;
            }else{
                is_last_period = 0; 
            }
            if(period_name === "" || date_start === "" || date_end ===""){
                alert("<?= Yii::t('app','Period name, date start and date end are mandatory field, please review your data entries !')?>");
            }else{
                $.get('<?= $baseUrl?>/index.php/kindergarden/kinderPeriod/saveperiod',{
                    period_name:period_name,
                    date_start:date_start,
                    date_end:date_end,
                    is_last_period:is_last_period

                },function(data){
                     $('#list-period').html(data);

                });
                 $('#myModal').modal('toggle');
            } 
          
        });
        
        $(document).on('click','.efase',function(){
             var result = confirm("<?= Yii::t('app','Do you realy want to delete this period ?')?>");
             if(result){
                 var id = $(this).data().id;
                 $.get('<?= $baseUrl?>/index.php/kindergarden/kinderPeriod/delete',{id:id},function(data){
                     if(data === '23000'){
                         alert('<?= Yii::t('app','Unable to delete this period!')?>');
                     }else{
                         $('#list-period').html(data);
                     }
                 });
             }else{
                 
             }
         });
        
        $('.modifye').editable({
          emptytext:'N/A',
          mode : 'inline',
          placement: function (context, source) {
            var popupWidth = 50;
            if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
         
         $('#add-period').click(function(){
             //alert('add period');
             $("#myModal").modal();
            $('#myModal').on('hidden.bs.modal', function () {
               
              });
         });
           
    });
    
   
</script>