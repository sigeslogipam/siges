<?php
     if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }
    $acad=Yii::app()->session['currentId_academic_year'];
    $data_period = KinderPeriod::model()->findAllByAttributes(array('academic_year'=>$acad));

    ?>
<div class="kinder-form1">
<form action="" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<table class="table-responsive table-striped table-condensed">
    <thead>

    <th><?= Yii::t('app','Period name'); ?></th>
    <th><?= Yii::t('app','Academic year');?></th>
    <th><?= Yii::t('app','Date start')?></th>
    <th><?= Yii::t('app','Date end') ?> </th>
    <th><?= Yii::t('app','Is last period')?></th>
    <th></th>
    </thead>
    <tbody>
    <?php

    foreach($data_period as $dp){
        ?>
        <tr>
            <td class="modifye" data-type="text" data-name="period_name" data-pk="<?= $dp->id; ?>" data-url="updateperiod"><?= $dp->period_name;?></td>
            <td><?= $dp->academicYear->name_period;  ?></td>
            <td class="modifye" data-type="date" data-name="date_start" data-pk="<?= $dp->id; ?>" data-url="updateperiod"><?= Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($dp->date_start, 'yyyy-MM-dd'),'medium',null); ?></td>
            <td class="modifye" data-type="date" data-name="date_end" data-pk="<?= $dp->id; ?>" data-url="updateperiod"><?= Yii::app()->dateFormatter->formatDateTime(CDateTimeParser::parse($dp->date_end, 'yyyy-MM-dd'),'medium',null); ?></td>
            <td class="modifye" data-type="select" data-name="is_last_period" data-source="yesno" data-pk="<?= $dp->id; ?>" data-url="updateperiod">
                <?php
                    if($dp->is_last_period == 1){
                        echo Yii::t('app','Yes');
                    }else{
                        echo Yii::t('app','No');
                    }
                ?>
            </td>
            <td class="efase" data-id="<?= $dp->id; ?>"><i class="fa fa-trash"></i></td>
        </tr>
    <?php

    }
    ?>
    </tbody>
</table>
</form>
</div>
<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });

    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'N/A',
          mode : 'inline',
          placement: function (context, source) {
            var popupWidth = 50;
            if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });



    });
</script>
