<?php
/* @var $this KinderPeriodController */
/* @var $model KinderPeriod */

$this->breadcrumbs=array(
	'Kinder Periods'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List KinderPeriod', 'url'=>array('index')),
	array('label'=>'Create KinderPeriod', 'url'=>array('create')),
	array('label'=>'Update KinderPeriod', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KinderPeriod', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KinderPeriod', 'url'=>array('admin')),
);
?>

<h1>View KinderPeriod #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'period_name',
		'academic_year',
		'date_start',
		'date_end',
		'is_last_period',
		'create_by',
		'update_by',
		'create_date',
		'update_date',
	),
)); ?>
