<?php
/* @var $this KinderCatConceptController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kinder Cat Concepts',
);

$this->menu=array(
	array('label'=>'Create KinderCatConcept', 'url'=>array('create')),
	array('label'=>'Manage KinderCatConcept', 'url'=>array('admin')),
);
?>

<h1>Kinder Cat Concepts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
