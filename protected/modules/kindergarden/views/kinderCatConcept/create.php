<?php
/* @var $this KinderCatConceptController */
/* @var $model KinderCatConcept */

$acad=Yii::app()->session['currentId_academic_year']; 
?>

<div id="dash">
    <div class="span3"><h2><?= Yii::t('app','List of category'); ?></h2></div>
    <div class="span3">
        <div class="span4">
            <a href="#" id="add-category"><i class="fa fa-plus"> <?= Yii::t('app','Add'); ?></i></a>
        </div>
        
    </div>
</div>

<div id="list-cat">
    ...
</div>

<div class="modal fade modal-lg" id="modal-category" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add category'); ?><span id="en-tete"></span></h4>
        </div>
        <div class="modal-body">
            <div class="form">
                <div id="resp_form_siges">
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="cat-name" class="required"><?= Yii::t('app','Category name'); ?></label>
                        <input size="60" maxlength="45" placeholder="<?= Yii::t('app','Category name'); ?>" name="cat-name" id="cat-name" type="text"></label>
                    </div>
                    <br/>
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="is-cat-special"><?= Yii::t('app','Is special category ?'); ?></label>
                        <input name="is-cat-special" id="is-cat-special" type="checkbox"></label>
                    </div>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="save-cat"><?= Yii::t('app','Save'); ?></button>  
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>

<?php 
$baseUrl = Yii::app()->baseUrl;
?>

<script>
    $(document).ready(function(){
        
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderCatConcept/refreshcat',{},function(data){
            $('#list-cat').html(data);
        });
        
        
        
        $('#save-cat').click(function(){
            var cat_name = $('#cat-name').val(); 
            var is_special_cat = 0; 
            if($('#is-cat-special').is(":checked")){
                is_special_cat = 1;
            }else{
                is_special_cat = 0; 
            }
            if(cat_name === ""){
                alert("<?= Yii::t('app','Category name is a mandatory field, please review your data entries !')?>");
            }else{
                $.get('<?= $baseUrl?>/index.php/kindergarden/kinderCatConcept/savecat',{
                    cat_name:cat_name,
                    is_special_cat:is_special_cat
                    },function(data){
                     $('#list-cat').html(data);

                });
                $('#cat-name').val(''); 
                 $('#modal-category').modal('toggle');
            } 
          
        });
        
        $(document).on('click','.efase-cat',function(){
             var result = confirm("<?= Yii::t('app','Do you realy want to delete this category ?')?>");
             if(result){
                 var id = $(this).data().id;
                 $.get('<?= $baseUrl?>/index.php/kindergarden/kinderCatConcept/delete',{id:id},function(data){
                     if(data === '23000'){
                         alert('<?= Yii::t('app','Unable to delete this category!')?>');
                     }else{
                         $('#list-cat').html(data);
                     }
                 });
             }else{
                 
             }
         });
        
        $('.modifye').editable({
          emptytext:'N/A',
          mode : 'inline',
          placement: function (context, source) {
            var popupWidth = 50;
            if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
         
         $('#add-category').click(function(){
             //alert('add period');
             $("#modal-category").modal();
            $('#modal-category').on('hidden.bs.modal', function () {
               $('#cat-name').val(''); 
              });
         });
           
    });
    
   
</script>