<?php
/* @var $this KinderCatConceptController */
/* @var $model KinderCatConcept */

$this->breadcrumbs=array(
	'Kinder Cat Concepts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List KinderCatConcept', 'url'=>array('index')),
	array('label'=>'Create KinderCatConcept', 'url'=>array('create')),
	array('label'=>'Update KinderCatConcept', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KinderCatConcept', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KinderCatConcept', 'url'=>array('admin')),
);
?>

<h1>View KinderCatConcept #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'cat_name',
		'cat_special',
		'create_by',
		'update_by',
		'create_date',
		'update_date',
	),
)); ?>
