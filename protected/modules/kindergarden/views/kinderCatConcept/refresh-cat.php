<?php
     if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }
    $acad=Yii::app()->session['currentId_academic_year']; 
    $data_cat = KinderCatConcept::model()->findAll();
    $baseUrl = Yii::app()->baseUrl;
    
    ?>
<form action="" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<table class="table-responsive table-striped table-condensed">
    <thead>
    
    <th><?= Yii::t('app','Category name'); ?></th>
    <th><?= Yii::t('app','Is special category');?></th>
    <th></th>
    </thead>
    <tbody>
    <?php 
    
    foreach($data_cat as $dc){
        ?>
        <tr>
            <td class="modifye" data-type="text" data-name="cat_name" data-pk="<?= $dc->id; ?>" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderCatConcept/updatecat"><?= $dc->cat_name; ?></td>
            <td class="modifye" data-type="select" data-name="cat_special" data-pk="<?= $dc->id; ?>" data-source="<?= $baseUrl ?>/index.php/kindergarden/kinderPeriod/yesno" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderCatConcept/updatecat">
                    
                <?php 
                    if($dc->cat_special == 1){
                        echo Yii::t('app','Yes');
                    }else{
                        echo Yii::t('app','No');
                    }
                ?>
            </td>
            <td class="efase-cat" data-id="<?= $dc->id; ?>"><i class="fa fa-trash"></i></td>
        </tr>
    <?php 
        
    }
    ?>
    </tbody>
</table>
</form>
<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'N/A',
          mode : 'inline',
          placement: function (context, source) {
            var popupWidth = 50;
            if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
         
         
         
    });
</script>