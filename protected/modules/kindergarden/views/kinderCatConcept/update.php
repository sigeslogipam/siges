<?php
/* @var $this KinderCatConceptController */
/* @var $model KinderCatConcept */

$this->breadcrumbs=array(
	'Kinder Cat Concepts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KinderCatConcept', 'url'=>array('index')),
	array('label'=>'Create KinderCatConcept', 'url'=>array('create')),
	array('label'=>'View KinderCatConcept', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage KinderCatConcept', 'url'=>array('admin')),
);
?>

<h1>Update KinderCatConcept <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>