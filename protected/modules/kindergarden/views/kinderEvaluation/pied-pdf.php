<br/><br/><br/>
<table width="100%">
    <tr>
        <td width="40%" align="center"><?= Yii::t('app','Teacher Signature'); ?></td>
        <td width="40%" align="center"><?= Yii::t('app','Direction Signature')?></td>
    </tr>
    <tr>
        <td width="40%" align="center">
            -------------------------------
        </td>
        <td width="40%" align="center">
            -------------------------------
        </td>
    </tr>
</table>
<br/><br/>
<div class="teks-pye-paj">
    <span><strong><?= Yii::t('app','Legend');?></strong></span>
    
        <?php 
        $all_mention = KinderMention::model()->findAll();
        foreach($all_mention as $am){
            ?>
        <span> <?= $am->mention_short_name; ?> : <?= $am->mention_name?> | </span>
        <?php 
        }
        ?>
    
</div>