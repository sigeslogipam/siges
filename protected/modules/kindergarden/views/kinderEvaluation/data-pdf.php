<?php 
// $array_stud = explode(",", $student);  
// $k =0;
//foreach($array_stud as $as){

?>

<table id="pdf-report-card">
    <tr>
        <td colspan="2" style="text-align: center; font-size: 14px;" border="0">
            <span>
                <strong>
                    <?= Yii::t('app','Student name'); ?> : <?= Persons::model()->findByPk($student)->fullName; ?>
                </strong>
            </span>
        </td>
    </tr>
    <?php 
    
    foreach($cat_data as $cd){
        $concept_data = $this->conceptByCategory($cd->id,$level);
        ?>
    <tr>
        <td><span class="kategori"><strong><?= $cd->cat_name; ?></strong></span></td>
        <td style="text-align: center;"><span class="kategori"><strong><?= Yii::t('app','Mention') ?></strong></span></td>
        
    </tr>
    <?php 
        $i = 1;
        foreach($concept_data as $concept){
            ?>
    <tr>
        <td style="padding-left: 15px;"><span class="konsep"><?= $concept->concept_name; ?></span></td>
        <td style="text-align: center; vertical-align: middle;">
            <span class="mention">
                
                <?php 
                     $eval_array =  $this->findMentionByStudent($student,$concept->id,$period);
                                    if(!empty($eval_array)){
                                        echo KinderMention::model()->findByPk($eval_array['mention'])->mention_short_name;
                                    }else{
                                        echo '';
                                    }
                ?>
            </span>
         </td>
    </tr>
    <?php
        $i++;
        }
    ?>
    <?php 
      
    }?>
    
    <?php foreach($cat_special_data as $csd){
        ?>
    <tr>
        <td>
            <span class="kategori">
                <strong><?= $csd->cat_name ?></strong>
            </span>
        </td>
        <td style="text-align: justify">
            <span class="mention">
                
                <?php 
                     $eval_special_array =  $this->findMentionByStudent($student,$csd->id,$period,1);
                                    if(!empty($eval_special_array)){
                                        echo $eval_special_array['mention'];
                                    }else{
                                        echo '';
                                    }
                ?>
                
            </span>
        </td>
    </tr>
    <?php 
    } 
    
    ?>
    
</table>

