<div class="kindertest">
  <?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');

    /*
     *
     *
     * <link href="js/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.3/bootstrap-wysihtml5-0.0.3.css" rel="stylesheet" type="text/css"></link>
<script src="js/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.3/wysihtml5-0.3.0.min.js"></script>
<script src="js/inputs-ext/wysihtml5/bootstrap-wysihtml5-0.0.3/bootstrap-wysihtml5-0.0.3.min.js"></script>
     * <script src="js/inputs-ext/wysihtml5/wysihtml5-0.0.3.js"></script>
     */

    $acad=Yii::app()->session['currentId_academic_year'];
    $defaut_section_kinder = infoGeneralConfig('kindergarden_section');
    $period_data = KinderPeriod::model()->findAllBySql("SELECT id, period_name FROM kinder_period WHERE academic_year = $acad ORDER BY date_start ASC");
    $room_data = Rooms::model()->findAllBySql("SELECT r.id, r.room_name, r.short_room_name  FROM levels  l  INNER JOIN rooms r ON (l.id = r.level) INNER JOIN sections s ON (l.section = s.id) WHERE s.section_name LIKE '$defaut_section_kinder' AND r.id IN (SELECT room FROM room_has_person WHERE academic_year = $acad) ORDER BY r.room_name");


    $period_data_current = KinderPeriod::model()->findAllBySql("SELECT id, period_name FROM kinder_period WHERE academic_year = $acad AND (NOW() BETWEEN date_start and date_end) ORDER BY date_start ASC");
    echo $this->renderPartial('//layouts/navBaseKinder',NULL,true);

    $default_room = 0;
    $default_period = 0;
    $default_student = 0;

    foreach($room_data as $rd){
        $default_room = $rd->id;
    }

    foreach($period_data_current as $pd){
        $default_period = $pd->id;
    }

    $student_in_romm = RoomHasPerson::model()->findAllBySql("SELECT * FROM room_has_person WHERE room = $default_room");
    foreach($student_in_romm as $sir){
        $default_student = $sir->students;
    }

    $student_name = Persons::model()->findByPk($default_student)->fullName;


?>

<div class="row-fluid kinder">
    <!--
    <div class="row-fluid">
        <h4><?= Yii::t('app','Create evaluation'); ?> <span id="espas-period"></span> <span id="espas-room"></span> <span id='espas-lib'>pour l'&eacute;l&egrave;ve : </span> <span id="espas-student" class='elev-vert'></span></h4>
    </div>
    -->
    <div class="form span12">
        <div id="resp_form_siges">
            <div class="col-4">
                <label id="resp_form">
               <!-- <label for="period-name" class="required"><?= Yii::t('app','Period name'); ?></label> -->
                <select name="period-name" id="period-name">
                    <option value=""><?= Yii::t('app','Choose period'); ?></option>
                    <?php
                        foreach($period_data as $pd){
                            ?>
                    <option value="<?= $pd->id; ?>"><?= $pd->period_name; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </label>
            </div>
            <div class="col-4">
                <label id="resp_form">
              <!--  <label for="room-name" class="required"><?= Yii::t('app','Room name'); ?></label> -->
                <select name="room-name" id="room-name">
                    <option value=""><?= Yii::t('app','Choose room'); ?></option>
                    <?php
                        foreach($room_data as $rd){
                            ?>
                    <option value="<?= $rd->id; ?>"><?= $rd->room_name; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </label>

            </div>

<div id="espas-student" class='elev-vert'></div>
      </div>

    </div>
</div>
<div class='row-fluid'>
    <div class='span6'>
        <div id='espas-eval'>

        </div>
    </div>
    <div class='span6' id='trombinoscope'>

    </div>
</div>


<script>
$(document).ready(function(){

    // Load a default page
    var default_room = <?= $default_room; ?>;
    var default_period = <?= $default_period; ?>;
    var default_student = <?= $default_student; ?>;
    $('#period-name').val(default_period);
    $('#room-name').val(default_room);
    // Charger une classe
    $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/trombinoscope',{room:default_room},function(data){
            $('#trombinoscope').html(data);
        });

    // Charger un eleve
    $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/kindereval',{room:default_room,period:default_period,student:default_student},function(data){
            $('#espas-eval').html(data);
        });

    $('#espas-student').html('<?= $student_name; ?>');


    $('.sub').accordion();

    var period_id = 0;
    var room_id = 0;
    var student_id = 0;

    $('select').select2({
             placeholder: 'This is my placeholder',
            allowClear: true
        });

    $('#student-combo').hide();
    $('#espas-lib').hide();
    $('#period-name').change(function(){
        var period_text = $('#period-name option:selected').text();
        period_id = $('#period-name').val();
        $('#espas-period').html(period_text);
    });

    $('#room-name').change(function(){
        var room_text = $('#room-name option:selected').text();
        room_id = $('#room-name').val();

        $('#espas-room').html(room_text);
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/combostud',{room:room_id},function(data){
            $('#student-combo').show();
            $('#student-combo').html(data);
        });
         $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/trombinoscope',{room:room_id},function(data){

            $('#trombinoscope').html(data);
        });

    });

    $(document).on('change','#student',function(){
        var student_text = $('#student option:selected').text();
        student_id = $('#student').val();
        $('#espas-lib').show();
        $('#espas-student').html(student_text);
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/kindereval',{room:room_id,period:period_id,student:student_id},function(data){
            $('#espas-eval').html(data);
        });
    });

    $(document).on('click','.espas-timoun',function(){

        var student_text = $(this).data().nom;
        var student_id = $(this).data().id;
        room_id = $('#room-name').val();
        period_id = $('#period-name').val();
        $('#espas-student').html(student_text);
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/kindereval',{room:room_id,period:period_id,student:student_id},function(data){
            $('#espas-eval').html(data);
        });

    });


});
</script>
</div>
