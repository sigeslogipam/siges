<?php
$baseUrl = Yii::app()->baseUrl;
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div id="persons-grid" class="form">
    <table class="table responsive table-striped table-hover" id="DataTables_Table_0">
        <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t('app','Last name')?></th>
                <th><?= Yii::t('app','First name') ?></th>
                <th class="checkbox-column" id="chk">
                    <input class="select-on-check-all" type="checkbox" value="1" name="chk_all" id="check-all">
                </th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $k = 1;
                foreach($person_data as $pd){
                    ?>
            <tr>
                <td><?= $k; ?></td>
                <td><?= $pd->last_name; ?></td>
                <td><?= $pd->first_name; ?></td>
                <td>
                    <input class="on-check" type="checkbox" value="<?= $pd->id; ?>" name="chk" id="<?= $pd->id ?>">
                </td>
            </tr>
            <?php
            $k++; 
                }
            ?>
        </tbody>
    </table>
    
</div>


<div class="row" style="text-align: center">
    <a href="#" id="print-report" class="btn btn-warning"><?= Yii::t('app','Print report card'); ?></a>
</div>

<script>
    var listid = null;
    var room_text = $('#room-name option:selected').text();
    var room_id = $('#room-name').val();
    var period_id = $('#period-name').val();
    $(document).ready(function(){
         $("#print-report").hide();
         
        function getValueUsingClass(){
            /* declare an checkbox array */
            var chkArray = [];

            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".on-check:checked").each(function() {
                    chkArray.push($(this).val());
            });

            /* we join the array separated by the comma */
            var selected;
            selected = chkArray.join(',') ;

            return selected;
        }
        
        $("#check-all").click(function(){
            
           if ($('#check-all').is(":checked"))
                {
                  $('.on-check').prop('checked',true);
                  listid = getValueUsingClass();
                  $("#print-report").show();
                 
                }else{
                    $('.on-check').prop('checked',false);
                    $("#print-report").hide();
                }
        
                
        });
        
        $('.on-check').click(function(){
            $("#print-report").show();
            listid = getValueUsingClass();
            if(!$(".on-check").is(":checked") && !$('#check-all').is(":checked")){
                $("#print-report").hide();
            }
        });
        
         $('#print-report').click(function(){
               
               window.open("<?= $baseUrl; ?>/index.php/kindergarden/kinderEvaluation/makepdf/room/"+room_id+"/period/"+period_id+"/student/"+listid);
        
            });
        
        
        
    
    
    
    });
</script>