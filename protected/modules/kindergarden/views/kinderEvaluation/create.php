<?php
/* @var $this KinderEvaluationController */
/* @var $model KinderEvaluation */

$this->breadcrumbs=array(
	'Kinder Evaluations'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List KinderEvaluation', 'url'=>array('index')),
	array('label'=>'Manage KinderEvaluation', 'url'=>array('admin')),
);
?>

<h1>Create KinderEvaluation</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>