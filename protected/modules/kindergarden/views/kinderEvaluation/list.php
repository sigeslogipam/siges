<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$baseUrl = Yii::app()->baseUrl;
$acad=Yii::app()->session['currentId_academic_year'];
$defaut_section_kinder = infoGeneralConfig('kindergarden_section');
 
  $room_data = Rooms::model()->findAllBySql("SELECT r.id, r.room_name, r.short_room_name  FROM levels  l  INNER JOIN rooms r ON (l.id = r.level) INNER JOIN sections s ON (l.section = s.id) WHERE s.section_name LIKE '$defaut_section_kinder' AND r.id IN (SELECT room FROM room_has_person WHERE academic_year = $acad) ORDER BY r.room_name");
  $default_room = 0;
 
 foreach($room_data as $rd){
        $default_room = $rd->id;
    }

 echo $this->renderPartial('//layouts/navBaseKinder',NULL,true);
 ?>


<div class="row-fluid kinder">
    
</div>

<br/>
<div class='row-fluid'>
    <div class='span12' id='list-student'>

    </div>
</div>



<script>
    $(document).ready(function(){
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/combostud',{room:1},function(data){
            $('#list-student').show();
            $('#list-student').html(data);
        });
        
    });
</script>