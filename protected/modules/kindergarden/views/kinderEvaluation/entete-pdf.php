
<div id='wrapper' >    
    <div style='float:left;width:10%'> 
      <img class="img-logo" src="<?= Yii::app()->baseUrl.'/css/images/school_logo.png' ; ?>"/>
    </div>    
    <div style='float:left;width:40%'>   
        <span class="non-lekol"><strong><?= infoGeneralConfig('school_name'); ?></strong></span><br/>
        <span class="adres"><?= infoGeneralConfig('school_address'); ?></span><br/>
        <span class="telefon"><?= infoGeneralConfig('school_phone_number'); ?></span>
    </div>
    <div style='float: left;width:50%'> 
        <span class="adres"><strong><?= Yii::t('app','Academic year')?> </strong>: <?= AcademicPeriods::model()->findByPk($acad)->name_period; ?></span><br/>
        <span class="adres"><strong><?php echo Yii::t('app','Level').' / '.Yii::t('app','Room'); ?> </strong>: <?= Levels::model()->findByPk(Rooms::model()->findByPk($room)->level)->level_name ?> / <?= Rooms::model()->findByPk($room)->room_name; ?></span><br/>
        <span class="adres"><strong><?= Yii::t('app','Report card') ?></strong> : <?= KinderPeriod::model()->findByPk($period)->period_name ?></span>
        
    </div>
    <hr size="30"/>
</div>

