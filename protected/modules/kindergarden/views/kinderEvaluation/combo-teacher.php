<?php 
$baseUrl = Yii::app()->baseUrl;
?>

<table class="table responsive table-striped table-hover" id="table-list-stud">
    <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t('app','Code') ?></th>
                <th><?= Yii::t('app','Last name')?></th>
                <th><?= Yii::t('app','First name') ?></th>
                <th><?= Yii::t('app','Gender') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php 
                $k = 1;
                foreach($person_data as $pd){
                    ?>
            <tr>
                <td><?= $k; ?></td>
                <td><?= $pd->id_number; ?></td>
                <td>
                    <a href="<?= $baseUrl?>/index.php/academic/persons/viewForReport/id/<?=$pd->id ?>/from/emp">
                    <?= $pd->last_name; ?>
                    </a>
                </td>
                <td>
                    <a href="<?= $baseUrl?>/index.php/academic/persons/viewForReport/id/<?=$pd->id ?>/from/emp">
                       <?= $pd->first_name; ?>
                    </a>
                </td>
                <td><?= $pd->sexe; ?></td>
            </tr>
            <?php
            $k++; 
                }
            ?>
        </tbody>
</table>

<script>
    $(document).ready(function(){
          $('#table-list-stud').DataTable({
                pageLength: 50,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  {extend: 'excel', title: "<?= Yii::t('app','list_postulant') ?>"},
                   
                ]

            });
    });
</script>

