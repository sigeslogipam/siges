<?php
/* @var $this KinderEvaluationController */
/* @var $data KinderEvaluation */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student')); ?>:</b>
	<?php echo CHtml::encode($data->student); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('concept')); ?>:</b>
	<?php echo CHtml::encode($data->concept); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('mention')); ?>:</b>
	<?php echo CHtml::encode($data->mention); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('free_comments')); ?>:</b>
	<?php echo CHtml::encode($data->free_comments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from_special_cat')); ?>:</b>
	<?php echo CHtml::encode($data->from_special_cat); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('academic_year')); ?>:</b>
	<?php echo CHtml::encode($data->academic_year); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('create_by')); ?>:</b>
	<?php echo CHtml::encode($data->create_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_by')); ?>:</b>
	<?php echo CHtml::encode($data->update_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('create_date')); ?>:</b>
	<?php echo CHtml::encode($data->create_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('update_date')); ?>:</b>
	<?php echo CHtml::encode($data->update_date); ?>
	<br />

	*/ ?>

</div>