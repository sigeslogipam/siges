<?php
    //$baseUrl = Yii::app()->baseUrl;

if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }
?>
<div class="kinder-form">
<form action="" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<ol class="sub">
    <?php
        $baseUrl = Yii::app()->baseUrl;

        foreach($cat_data as $cd){
            $concept_data = $this->conceptByCategory($cd->id,$level);
            ?>
    <li class="accordion">
        <h3><?= $cd->cat_name ?></h3>
        <div>
            <table class="table-responsive table-striped table-condensed">
                <thead>
                <th><?= Yii::t('app','Evaluation concept'); ?></th>
                <th><?= Yii::t('app','Mention'); ?></th>
                </thead>
                <tbody>
                    <?php
                    foreach($concept_data as $concept){
                        ?>
                    <tr>
                        <td>
                        <?= $concept->concept_name; ?>
                         </td>
                        <?php
                            $eval_array =  $this->findMentionByStudent($student,$concept->id,$period);
                            if(!empty($eval_array)){
                                ?>
                        <td class='modifye-mention' data-type="select" data-name="mention" data-pk="<?= $eval_array['id']; ?>" data-source="<?= $baseUrl ?>/index.php/kindergarden/kinderEvaluation/listmention" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderEvaluation/updateevaluation">
                            <?= KinderMention::model()->findByPk($eval_array['mention'])->mention_short_name; ?>
                        </td>
                        <?php
                            }else{
                         ?>
                        <td class='modifye-mention' data-type="select" data-name="create-mention" data-pk="<?= $concept->id; ?>" data-source="<?= $baseUrl ?>/index.php/kindergarden/kinderEvaluation/listmention" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderEvaluation/createevaluation/student/<?= $student; ?>/period/<?= $period;?>">
                        <i class="fa fa-plus"></i>
                        </td>
                            <?php } ?>
                        </tr>
                    <?php
                        }
                        ?>


                </tbody>
            </table>
        </div>
    </li>
    <?php
        }

        foreach($cat_special_data as $csd){
            $eval_special_array =  $this->findMentionByStudent($student,$csd->id,$period,1);

            ?>
    <li class='accordion'>
        <h3><?= $csd->cat_name; ?></h3>
        <div height="400px">
            <?php
            if(!empty($eval_special_array)){
            ?>
            <div class="modifye-mention" data-type="textarea" data-name="update-mention-special" data-pk="<?= $eval_special_array['id']; ?>" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderEvaluation/updateevaluationspecial">
                <?= $eval_special_array['mention']; ?>
            </div>
            <?php }else{
                ?>
            <div class="modifye-mention" data-type="textarea" data-name="create-mention-special" data-pk="<?= $csd->id; ?>"  data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderEvaluation/createevaluationspecial/student/<?= $student; ?>/period/<?= $period;?>">
            <?= Yii::t('app','Click bellow to add new text !')?>
            </div>
            <?php

            }
            ?>

        </div>
    </li>
    <?php
        }
    ?>


</ol>
</form>
</div>
<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    $(document).ready(function(){
        $('.sub').accordion();
        $('.modifye-mention').editable({
          emptytext:'<i class="fa fa-plus"></i>',
          mode : 'inline',

         });
    });
</script>

<!--

<ol>
                <li>Description</li>
                <li>This is text within section 1.</li>
            </ol>
-->
