<div class="kindertest">
  <?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
    $acad = Yii::app()->session['currentId_academic_year'];
    $defaut_section_kinder = infoGeneralConfig('kindergarden_section'); 
    $period_data = KinderPeriod::model()->findAllBySql("SELECT id, period_name FROM kinder_period WHERE academic_year = $acad ORDER BY date_start ASC");
    $room_data = Rooms::model()->findAllBySql("SELECT r.id, r.room_name, r.short_room_name  FROM levels  l  INNER JOIN rooms r ON (l.id = r.level) INNER JOIN sections s ON (l.section = s.id) WHERE s.section_name LIKE '$defaut_section_kinder' ORDER BY r.room_name");



echo $this->renderPartial('//layouts/navBaseKinder',NULL,true);
    ?>


<div class="row-fluid kinder-card">

   <div class="form span8">
        <div id="resp_form_siges">
            <div class="col-4">
                <label id="resp_form">
              <!--  <label for="period-name" class="required"><?= Yii::t('app','Period name'); ?></label> -->
                <select name="period-name" id="period-name">
                    <option value=""><?= Yii::t('app','Choose period'); ?></option>
                    <?php
                        foreach($period_data as $pd){
                            ?>
                    <option value="<?= $pd->id; ?>"><?= $pd->period_name; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </label>
            </div>
            <div class="col-4">
                <label id="resp_form">
                <!-- <label for="room-name" class="required"><?= Yii::t('app','Room name'); ?></label> -->
                <select name="room-name" id="room-name">
                    <option value=""><?= Yii::t('app','Choose room'); ?></option>
                    <?php
                        foreach($room_data as $rd){
                            ?>
                    <option value="<?= $rd->id; ?>"><?= $rd->room_name; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </label>
            </div>
            <!--
            <div class="col-4" id="student-combo">
                ..
            </div>
            -->
        </div>

    </div>

    
</div>
<br/>
<div class='row-fluid'>
    <div class='span12' id='list-student'>

    </div>
</div>


<script>
    $(document).ready(function(){

       $('#room-name').change(function(){
        var room_text = $('#room-name option:selected').text();
        room_id = $('#room-name').val();

       // $('#espas-room').html(room_text);
            $.get('<?= $baseUrl?>/index.php/kindergarden/kinderEvaluation/listreportcard',{room:room_id},function(data){

                $('#list-student').html(data);
            });

    });



    });
</script>
</div>
