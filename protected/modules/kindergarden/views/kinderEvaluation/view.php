<?php
/* @var $this KinderEvaluationController */
/* @var $model KinderEvaluation */

$this->breadcrumbs=array(
	'Kinder Evaluations'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List KinderEvaluation', 'url'=>array('index')),
	array('label'=>'Create KinderEvaluation', 'url'=>array('create')),
	array('label'=>'Update KinderEvaluation', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KinderEvaluation', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KinderEvaluation', 'url'=>array('admin')),
);
?>

<h1>View KinderEvaluation #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'student',
		'concept',
		'mention',
		'free_comments',
		'from_special_cat',
		'academic_year',
		'create_by',
		'update_by',
		'create_date',
		'update_date',
	),
)); ?>
