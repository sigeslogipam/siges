<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//require_once __DIR__ . '/vendor/autoload.php';
 $acad = Yii::app()->session['currentId_academic_year'];
 $array_stud = explode(",", $student); 
 // Define the size 
 $format_paper = array(); 
 $orientation_paper = "";
 
 $format_paper_ = infoGeneralConfig('size_paper_kinder');
 if($format_paper_ == "Letter"){
     $format_paper = array(216,356);
 }elseif($format_paper_=="Legal"){
     $format_paper = array(216,279); 
 }else{
     $format_paper = array(216,279);
 }
 
 $orientation_paper_ = infoGeneralConfig('orientation_paper_kinder');
 if($orientation_paper_ == "P"){
     $orientation_paper = "P";
 }elseif($orientation_paper_== "L"){
     $orientation_paper = "L";
 }else{
     $orientation_paper = "P";
 }
 
 
$mpdf = new \Mpdf\Mpdf(array(
        'margin_top' =>35,
	'margin_left' =>10 ,
	'margin_right' =>10,
        'margin_bottom'=>40,
	//'mirrorMargins' => true,
        'mode' => 'utf-8', 
        'format' =>$format_paper,
        'orientation'=>$orientation_paper
    ));
// 8.5 * 14 : array(216,356) -> Letter 
// 8.5 * 11 : array(216,279) -> Legal
// P : Portrait
// L : Landscape
//  'Legal-P'
//$mpdf->SetColumns(1);
$mpdf->SetHTMLHeader($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLHeader($this->renderPartial('entete-pdf',array(
     'cat_data'=>$cat_data,
     'cat_special_data'=>$cat_special_data,
     'period'=>$period,
     'student'=>$student,
     'room'=>$room,
     'acad'=>$acad
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);
//$mpdf->AddColumn();
//$mpdf->shrink_tables_to_fit = 1;
$mpdf->SetColumns(2,'J',5);
$mpdf->keepColumns = true;
//$mpdf->max_colH_correction = 5;
//$mpdf->WriteHTML('<columns column-count="2" vAlign="J" column-gap="7" />');
$k = 0;
foreach($array_stud as $as){ 
$mpdf->WriteHTML($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($this->renderPartial('data-pdf',array(
     'cat_data'=>$cat_data,
     'cat_special_data'=>$cat_special_data,
     'period'=>$period,
     'student'=>$array_stud[$k],
     'room'=>$room,
    'level'=>$level,
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);

//$mpdf->SetColumns(1);

$mpdf->SetHTMLFooter($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLFooter($this->renderPartial('pied-pdf',array(
     'cat_data'=>$cat_data,
     'cat_special_data'=>$cat_special_data,
     'period'=>$period,
     'student'=>$array_stud[$k],
     'room'=>$room,
     'acad'=>$acad
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);
//$mpdf->AddColumn();
    if($k < count($array_stud)-1){
        $mpdf->AddPage();
    }elseif($k==count($array_stud)-1){
        
    }

$k++;
}
//$mpdf->WriteHTML($kontni);
$room_name = Rooms::model()->findByPk($room)->short_room_name;
$period_name = KinderPeriod::model()->findByPk($period)->period_name;
$mpdf->Output("$room_name-$period_name.pdf",\Mpdf\Output\Destination::INLINE);
//echo 'Hello World !';