<?php
/* @var $this KinderEvaluationController */
/* @var $model KinderEvaluation */

$this->breadcrumbs=array(
	'Kinder Evaluations'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KinderEvaluation', 'url'=>array('index')),
	array('label'=>'Create KinderEvaluation', 'url'=>array('create')),
	array('label'=>'View KinderEvaluation', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage KinderEvaluation', 'url'=>array('admin')),
);
?>

<h1>Update KinderEvaluation <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>