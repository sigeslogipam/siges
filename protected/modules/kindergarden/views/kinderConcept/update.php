<?php
/* @var $this KinderConceptController */
/* @var $model KinderConcept */

$this->breadcrumbs=array(
	'Kinder Concepts'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KinderConcept', 'url'=>array('index')),
	array('label'=>'Create KinderConcept', 'url'=>array('create')),
	array('label'=>'View KinderConcept', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage KinderConcept', 'url'=>array('admin')),
);
?>

<h1>Update KinderConcept <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>