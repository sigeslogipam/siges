<?php
     if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }
    $acad = Yii::app()->session['currentId_academic_year']; 
    $data_concept = KinderConcept::model()->findAllByAttributes(array('academic_year'=>$acad));
    $baseUrl = Yii::app()->baseUrl;
    
    ?>
<form action="" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<table class="table-responsive table-striped table-condensed" id="table-concept">
    <thead>
    
    <th><?= Yii::t('app','Concept name'); ?></th>
    <th><?= Yii::t('app','Category');?></th>
    <th><?= Yii::t('app','Level'); ?></th>
    
    <th></th>
    </thead>
    <tbody>
    <?php 
    
    foreach($data_concept as $dc){
        ?>
        <tr>
            <td class="modifye" data-type="textarea" data-name="concept_name" data-pk="<?= $dc->id; ?>" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderConcept/updateconcept"><?= $dc->concept_name; ?></td>
            <td class="modifye" data-type="select" data-name="category" data-pk="<?= $dc->id; ?>" data-source="<?= $baseUrl ?>/index.php/kindergarden/kinderConcept/listcat" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderConcept/updateconcept">
                <?= $dc->category0->cat_name; ?>
            </td>
            <td class="modifye" data-type="select" data-name="level" data-pk="<?= $dc->id; ?>" data-source="<?= $baseUrl ?>/index.php/kindergarden/kinderConcept/listlevel" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderConcept/updateconcept"><?= $dc->level0->short_level_name; ?></td>
             <td class="efase-concept" data-id="<?= $dc->id; ?>"><i class="fa fa-trash"></i></td>
        </tr>
    <?php 
        
    }
    ?>
    </tbody>
</table>
</form>
<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'N/A',
          mode : 'inline',
          placement: function (context, source) {
            var popupWidth = 50;
            if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
         
         $('#table-concept').DataTable({
                pageLength: 50,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  
                    
                  
                 
                ]

            });
         
         
         
    });
</script>