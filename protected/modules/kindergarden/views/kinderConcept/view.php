<?php
/* @var $this KinderConceptController */
/* @var $model KinderConcept */

$this->breadcrumbs=array(
	'Kinder Concepts'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List KinderConcept', 'url'=>array('index')),
	array('label'=>'Create KinderConcept', 'url'=>array('create')),
	array('label'=>'Update KinderConcept', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KinderConcept', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KinderConcept', 'url'=>array('admin')),
);
?>

<h1>View KinderConcept #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'concept_name',
		'category',
		'level',
		'academic_year',
		'teacher',
		'create_by',
		'update_by',
		'create_date',
		'update_date',
	),
)); ?>
