<?php
/* @var $this KinderConceptController */
/* @var $model KinderConcept */

$baseUrl = Yii::app()->baseUrl;
$acad=Yii::app()->session['currentId_academic_year'];
$nom_section = infoGeneralConfig('kindergarden_section'); 
$room_data = Rooms::model()->findAllBySql("SELECT r.id, r.room_name FROM levels l INNER JOIN sections s ON (l.section = s.id) INNER JOIN rooms r ON (r.level = l.id) WHERE s.section_name LIKE '$nom_section'");
$teacher_data = Persons::model()->findAllBySql("SELECT id, first_name, last_name  FROM `persons` Where id NOT IN (SELECT teacher FROM courses WHERE academic_period = $acad) AND id NOT IN (SELECT teacher FROM kinder_teacher WHERE academic_year = $acad) AND is_student <> 1 AND active IN (1,2) ORDER BY last_name ASC");
$cat_data = KinderCatConcept::model()->findAllBySql("SELECT id, cat_name FROM kinder_cat_concept WHERE cat_special <> 1");
?>



<div class="row-fluid lot">
    <div class="row-fluid">
    <h4><?= Yii::t('app','Asign person to kindergarden room'); ?></h4>
    </div>
    <div class="form">
        <div id="resp_form_siges">
            <div class="span4">
                <label id="resp_form">
                <label for="room" class="required"><?= Yii::t('app','Room name'); ?></label>
                <select name="room" id="room">
                    <option value=""><?= Yii::t('app','Choose room'); ?></option>
                    <?php
                        foreach($room_data as $rd){
                            ?>
                    <option value="<?= $rd->id; ?>"><?= $rd->room_name; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </label>
            </div>
            
            <div class="span4">
            <label id="resp_form">
                <label for="prof" class="required"><?= Yii::t('app','Teacher'); ?></label>
                <select name="prf" id="prof">
                    <option value=""><?= Yii::t('app','Choose teacher'); ?></option>
                    <?php
                        foreach($teacher_data as $td){
                            ?>
                    <option value="<?= $td->id; ?>"><?= $td->first_name.' '.$td->last_name ?></option>
                    <?php
                        }
                    ?>
                </select>
            </label>
            </div>
            
            <div class="span4">
                <label id="resp_rrform">
                    <label for="btn-save-teacher">&nbsp;</label>
                <span id="btn-save-teacher" class="btn btn-lg btn-warning"><?= Yii::t('app','Add teacher')?></span>
            </label>
            </div>

        </div>
    </div>
</div>

<div id="list-concept">
    ...
</div>



<script>
    $(document).ready(function(){
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/refreshteacher',{},function(data){
            $('#list-concept').html(data);
        });
        
        $('select').select2({
            placeholder: 'This is my placeholder',
            allowClear: true
        });

       // var level = 0;
        //var teacher = 0;
        var academic_year = <?= $acad; ?>;

        $('#btn-add-concept').click(function(){
             var level_text = $('#level option:selected').text();
             var teacher_text = $('#teacher option:selected').text();
             level = $('#level').val();
            //teacher = $('#teacher').val();
             if(level === ""){
                 alert('<?= Yii::t('app','You must select level before continue !'); ?>');
             }else{
                $('#en-tete-level').html(level_text);
                $('#en-tete-teacher').html(teacher_text);
                $("#modal-concept").modal();
                $('#modal-concept').on('hidden.bs.modal', function () {
                $('#concept-name').val('');
              });
          }
         });

         $('#btn-save-teacher').click(function(){
            var room = $('#room').val();
            var teacher_ = $('#prof').val();
            if(room === "" || teacher_ === ""){
                alert("<?= Yii::t('app','Room and teacher are mandatories fields, please review your data entries !')?>");
            }else{
                $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/saveteacher',{
                    teacher:teacher_,
                    room: room,
                    academic_year: academic_year
                    },function(data){
                     if(data==='0'){
                         alert("<?= Yii::t('app','Teacher already teach in kindergarden for this yeart');?>");
                     }else{   
                         $('#list-concept').html(data);
                         $('#room').val('');
                         $('#prof').val('');
                     }

                });
               

            }

        });

        $(document).on('click','.efase-concept',function(){
             var result = confirm("<?= Yii::t('app','Do you realy want to delete this teacher ?')?>");
             if(result){
                 var id = $(this).data().id;
                 $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/deleteteacher',{id:id},function(data){
                     if(data === '23000'){
                         alert('<?= Yii::t('app','Unable to delete this concept!')?>');
                     }else{
                         $('#list-concept').html(data);
                     }
                 });
             }else{

             }
         });



    });
</script>
