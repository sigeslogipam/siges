<?php
/* @var $this KinderConceptController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kinder Concepts',
);

$this->menu=array(
	array('label'=>'Create KinderConcept', 'url'=>array('create')),
	array('label'=>'Manage KinderConcept', 'url'=>array('admin')),
);
?>

<h1>Kinder Concepts</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
