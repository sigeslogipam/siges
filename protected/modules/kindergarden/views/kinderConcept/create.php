<?php
/* @var $this KinderConceptController */
/* @var $model KinderConcept */

$baseUrl = Yii::app()->baseUrl;
$acad=Yii::app()->session['currentId_academic_year'];
$defaut_section_kinder = infoGeneralConfig('kindergarden_section'); 
$level_data = Levels::model()->findAllBySql("SELECT l.id, l.level_name, l.short_level_name  FROM levels l INNER JOIN sections s ON (l.section = s.id) WHERE s.section_name LIKE '$defaut_section_kinder'");
$teacher_data = Persons::model()->findAllBySql("SELECT id, first_name, last_name  FROM `persons` Where id NOT IN (SELECT teacher FROM courses WHERE academic_period = $acad) AND is_student <> 1 AND active IN (1,2) ORDER BY last_name ASC");
$cat_data = KinderCatConcept::model()->findAllBySql("SELECT id, cat_name FROM kinder_cat_concept WHERE cat_special <> 1");
?>



<div class="row-fluid lot">
    <div class="row-fluid">
    <h4><?= Yii::t('app','Define concept for evaluation'); ?></h4>
    </div>
    <div class="form">
        <div id="resp_form_siges">
            <div class="span4">
                <label id="resp_form">
                <label for="cat-name" class="required"><?= Yii::t('app','Level name'); ?></label>
                <select name="level" id="level">
                    <option value=""><?= Yii::t('app','Choose level'); ?></option>
                    <?php
                        foreach($level_data as $ld){
                            ?>
                    <option value="<?= $ld->id; ?>"><?= $ld->level_name; ?></option>
                    <?php
                        }
                    ?>
                </select>
                </label>
            </div>
        <!--    
            <div class="span4">
            <label id="resp_form">
                <label for="cat-name" class="required"><?= Yii::t('app','Teacher'); ?></label>
                <select name="teacher" id="teacher">
                    <option value=""><?= Yii::t('app','Choose teacher'); ?></option>
                    <?php
                        foreach($teacher_data as $td){
                            ?>
                    <option value="<?= $td->id; ?>"><?= $td->first_name.' '.$td->last_name ?></option>
                    <?php
                        }
                    ?>
                </select>
            </label>
            </div>
            -->
            <div class="span4">
                <label id="resp_rrform">
                    <label for="btn-add-concept">&nbsp;</label>
                <span id="btn-add-concept" class="btn btn-lg btn-warning"><?= Yii::t('app','Add new concept')?></span>
            </label>
            </div>

        </div>
    </div>
</div>

<div id="list-concept">
    ...
</div>

<div class="modal fade modal-lg" id="modal-concept" role="dialog">
    <div class="modal-dialog">

      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add concept for level '); ?> <span id="en-tete-level"></span> </h4>
        </div>
          <!-- <?= Yii::t('app',' and teacher ')?> <span id="en-tete-teacher"></span> -->
        <div class="modal-body">
            <div class="form">
                <div id="resp_form_siges">
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="concept-name" class="required"><?= Yii::t('app','Concept name'); ?></label>
                        <input size="100" maxlength="1024" placeholder="<?= Yii::t('app','Concept name'); ?>" name="concept-name" id="concept-name" type="text"></label>
                    </div>
                    <br/>
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="kategori" class="required"><?= Yii::t('app','Category'); ?></label>
                            <select name="kategori" id="kategori">
                                <option value=""><?= Yii::t('app','Choose Category'); ?></option>
                                    <?php
                                        foreach($cat_data as $cd){
                                    ?>
                                <option value="<?= $cd->id; ?>"><?= $cd->cat_name; ?></option>
                                    <?php
                                    }
                                    ?>
                            </select>
                    </label>

                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="save-concept"><?= Yii::t('app','Save'); ?></button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>

    </div>
  </div>


<script>
    $(document).ready(function(){
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/refreshconcept',{},function(data){
            $('#list-concept').html(data);
        });

        $('select').select2({
            placeholder: 'This is my placeholder',
            allowClear: true
        });

        var level = 0;
        var teacher = 0;
        var academic_year = <?= $acad; ?>;

        $('#btn-add-concept').click(function(){
             var level_text = $('#level option:selected').text();
             var teacher_text = $('#teacher option:selected').text();
             level = $('#level').val();
             teacher = $('#teacher').val();
             if(level === ""){
                 alert('<?= Yii::t('app','You must select level before continue !'); ?>');
             }else{
                $('#en-tete-level').html(level_text);
                $('#en-tete-teacher').html(teacher_text);
                $("#modal-concept").modal();
                $('#modal-concept').on('hidden.bs.modal', function () {
                $('#concept-name').val('');
              });
          }
         });

         $('#save-concept').click(function(){
            var concept_name = $('#concept-name').val();
            var kategori = $('#kategori').val();

            if(concept_name === "" || kategori === ""){
                alert("<?= Yii::t('app','Concept name  and category are mandatories fields, please review your data entries !')?>");
            }else{
                $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/saveconcept',{
                    concept_name:concept_name,
                    kategori: kategori,
                   // teacher: teacher,
                    level: level,
                    academic_year: academic_year
                    },function(data){
                     $('#list-concept').html(data);
                     $('#concept-name').val('');
                     $('#category').val('');

                });
                $('#modal-concept').modal('toggle');

            }

        });

        $(document).on('click','.efase-concept',function(){
             var result = confirm("<?= Yii::t('app','Do you realy want to delete this concept ?')?>");
             if(result){
                 var id = $(this).data().id;
                 $.get('<?= $baseUrl?>/index.php/kindergarden/kinderConcept/delete',{id:id},function(data){
                     if(data === '23000'){
                         alert('<?= Yii::t('app','Unable to delete this concept!')?>');
                     }else{
                         $('#list-concept').html(data);
                     }
                 });
             }else{

             }
         });



    });
</script>
