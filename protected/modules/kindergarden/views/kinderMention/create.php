<?php
/* @var $this KinderMentionController */
/* @var $model KinderMention */
$acad=Yii::app()->session['currentId_academic_year']; 


?>

<div id="dash">
    <div class="span3"><h2><?= Yii::t('app','List of mention'); ?></h2></div>
    <div class="span3">
        <div class="span4">
            <a href="#" id="add-mention"><i class="fa fa-plus"> <?= Yii::t('app','Add'); ?></i></a>
        </div>
        <!-- <div class="span4"></div> -->
    </div>
</div>

<div id="list-mention">
    ...
</div>

 <div class="modal fade modal-lg" id="modal-mention" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Add mention'); ?><span id="en-tete"></span></h4>
        </div>
        <div class="modal-body">
            <div class="form">
                <div id="resp_form_siges">
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="mention-name" class="required"><?= Yii::t('app','Mention name'); ?></label>
                        <input size="60" maxlength="45" placeholder="<?= Yii::t('app','Mention name'); ?>" name="mention-name" id="mention-name" type="text"></label>
                    </div>
                    <div class="col-12">
                    <label id="resp_form">
                        <label for="mention-short-name" class="required"><?= Yii::t('app','Mention short name'); ?></label>
                        <input size="60" maxlength="45" placeholder="<?= Yii::t('app','Mention short name'); ?>" name="mention-short-name" id="mention-short-name" type="text"></label>
                    </div>
                   
                </div>
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="save-mention"><?= Yii::t('app','Save'); ?></button>  
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>

<?php 
$baseUrl = Yii::app()->baseUrl;
?>

<script>
    $(document).ready(function(){
        
        $.get('<?= $baseUrl?>/index.php/kindergarden/kinderMention/refreshmention',{},function(data){
            $('#list-mention').html(data);
        });
        
        
        
        $('#save-mention').click(function(){
            var mention_name = $('#mention-name').val(); 
            var mention_short_name = $('#mention-short-name').val(); 
            if(mention_name === "" || mention_short_name === ""){
                alert("<?= Yii::t('app','Mention name and mention short name are mandatory fields, please review your data entries !')?>");
            }else{
                $.get('<?= $baseUrl?>/index.php/kindergarden/kinderMention/savemention',{
                    mention_name:mention_name,
                    mention_short_name:mention_short_name
                    
                },function(data){
                     $('#list-mention').html(data);

                });
                 $('#modal-mention').modal('toggle');
            } 
          
        });
        
        $(document).on('click','.efase-mention',function(){
             var result = confirm("<?= Yii::t('app','Do you realy want to delete this mention ?')?>");
             if(result){
                 var id = $(this).data().id;
                 $.get('<?= $baseUrl?>/index.php/kindergarden/kinderMention/delete',{id:id},function(data){
                     if(data === '23000'){
                         alert('<?= Yii::t('app','Unable to delete this Mention!')?>');
                     }else{
                         $('#list-mention').html(data);
                     }
                 });
             }else{
                 
             }
         });
        
        $('.modifye').editable({
          emptytext:'N/A',
          mode : 'inline',
          placement: function (context, source) {
            var popupWidth = 50;
            if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
         
         $('#add-mention').click(function(){
             //alert('add period');
             $("#modal-mention").modal();
            $('#modal-mention').on('hidden.bs.modal', function () {
               
              });
         });
           
    });
    
   
</script>