<?php
/* @var $this KinderMentionController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Kinder Mentions',
);

$this->menu=array(
	array('label'=>'Create KinderMention', 'url'=>array('create')),
	array('label'=>'Manage KinderMention', 'url'=>array('admin')),
);
?>

<h1>Kinder Mentions</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
