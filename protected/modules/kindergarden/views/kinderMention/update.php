<?php
/* @var $this KinderMentionController */
/* @var $model KinderMention */

$this->breadcrumbs=array(
	'Kinder Mentions'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List KinderMention', 'url'=>array('index')),
	array('label'=>'Create KinderMention', 'url'=>array('create')),
	array('label'=>'View KinderMention', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage KinderMention', 'url'=>array('admin')),
);
?>

<h1>Update KinderMention <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>