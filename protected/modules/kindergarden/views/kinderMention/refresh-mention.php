<?php
     if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }
    $acad=Yii::app()->session['currentId_academic_year']; 
    $data_mention = KinderMention::model()->findAll();
    $baseUrl = Yii::app()->baseUrl;
    
    ?>
<form action="" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<table class="table-responsive table-striped table-condensed">
    <thead>
    
    <th><?= Yii::t('app','Mention name'); ?></th>
    <th><?= Yii::t('app','Mention short name');?></th>
    <th></th>
    </thead>
    <tbody>
    <?php 
    
    foreach($data_mention as $dm){
        ?>
        <tr>
            <td class="modifye" data-type="text" data-name="mention_name" data-pk="<?= $dm->id; ?>" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderMention/updatemention"><?= $dm->mention_name; ?></td>
            <td class="modifye" data-type="text" data-name="mention_short_name" data-pk="<?= $dm->id; ?>" data-url="<?= $baseUrl ?>/index.php/kindergarden/kinderMention/updatemention"><?= $dm->mention_short_name; ?></td>
            <td class="efase-mention" data-id="<?= $dm->id; ?>"><i class="fa fa-trash"></i></td>
        </tr>
    <?php 
        
    }
    ?>
    </tbody>
</table>
</form>
<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    
    $(document).ready(function(){
        $('.modifye').editable({
          emptytext:'N/A',
          mode : 'inline',
          placement: function (context, source) {
            var popupWidth = 50;
            if(($(window).scrollLeft() + popupWidth) > $(source).offset().left){
                    return "right";
                } else {
                    return "left";
                }
                }
         });
         
         
         
    });
</script>