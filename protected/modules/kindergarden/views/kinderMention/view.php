<?php
/* @var $this KinderMentionController */
/* @var $model KinderMention */

$this->breadcrumbs=array(
	'Kinder Mentions'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List KinderMention', 'url'=>array('index')),
	array('label'=>'Create KinderMention', 'url'=>array('create')),
	array('label'=>'Update KinderMention', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete KinderMention', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage KinderMention', 'url'=>array('admin')),
);
?>

<h1>View KinderMention #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'mention_name',
		'mention_short_name',
		'create_by',
		'update_by',
		'create_date',
		'update_date',
	),
)); ?>
