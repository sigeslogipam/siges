<?php

/**
 * This is the model class for table "kinder_cat_concept".
 *
 * The followings are the available columns in table 'kinder_cat_concept':
 * @property integer $id
 * @property string $cat_name
 * @property integer $cat_special
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 *
 * The followings are the available model relations:
 * @property KinderConcept[] $kinderConcepts
 * @property KinderEvaluation[] $kinderEvaluations
 */
class KinderCatConcept extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KinderCatConcept the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kinder_cat_concept';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cat_name', 'required'),
			array('id, cat_special', 'numerical', 'integerOnly'=>true),
			array('cat_name', 'length', 'max'=>128),
			array('create_by, update_by', 'length', 'max'=>64),
			array('create_date, update_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('cat_name, cat_special, create_by, update_by, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'kinderConcepts' => array(self::HAS_MANY, 'KinderConcept', 'category'),
			'kinderEvaluations' => array(self::HAS_MANY, 'KinderEvaluation', 'from_special_cat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'cat_name' =>Yii::t('app','Cat Name'),
			'cat_special' =>Yii::t('app', 'Cat Special'),
			'create_by' =>Yii::t('app', 'Create By'),
			'update_by' =>Yii::t('app', 'Update By'),
			'create_date' =>Yii::t('app', 'Create Date'),
			'update_date' =>Yii::t('app', 'Update Date'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cat_name',$this->cat_name,true);
		$criteria->compare('cat_special',$this->cat_special);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}