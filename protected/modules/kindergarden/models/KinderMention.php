<?php

/**
 * This is the model class for table "kinder_mention".
 *
 * The followings are the available columns in table 'kinder_mention':
 * @property integer $id
 * @property string $mention_name
 * @property string $mention_short_name
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 */
class KinderMention extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KinderMention the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kinder_mention';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('mention_name, mention_short_name', 'required'),
			array('id', 'numerical', 'integerOnly'=>true),
			array('mention_name, create_by, update_by', 'length', 'max'=>64),
			array('mention_short_name', 'length', 'max'=>32),
			array('create_date, update_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('mention_name, mention_short_name, create_by, update_by, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'mention_name' => 'Mention Name',
			'mention_short_name' => 'Mention Short Name',
			'create_by' => 'Create By',
			'update_by' => 'Update By',
			'create_date' => 'Create Date',
			'update_date' => 'Update Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('mention_name',$this->mention_name,true);
		$criteria->compare('mention_short_name',$this->mention_short_name,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}