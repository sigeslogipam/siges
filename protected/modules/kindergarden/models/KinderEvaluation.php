<?php

/**
 * This is the model class for table "kinder_evaluation".
 *
 * The followings are the available columns in table 'kinder_evaluation':
 * @property integer $id
 * @property integer $student
 * @property integer $concept
 * @property integer $mention
 * @property string $free_comments
 * @property integer $from_special_cat
 * @property integer $academic_year
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 *
 * The followings are the available model relations:
 * @property Persons $student0
 * @property KinderConcept $concept0
 * @property Academicperiods $academicYear
 * @property KinderCatConcept $fromSpecialCat
 */
class KinderEvaluation extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KinderEvaluation the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kinder_evaluation';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student, academic_year', 'required'),
			array('student, concept, mention, from_special_cat, academic_year', 'numerical', 'integerOnly'=>true),
			array('create_by, update_by', 'length', 'max'=>64),
			array('free_comments, create_date, update_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, student, concept, mention, free_comments, from_special_cat, academic_year, create_by, update_by, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'student0' => array(self::BELONGS_TO, 'Persons', 'student'),
			'concept0' => array(self::BELONGS_TO, 'KinderConcept', 'concept'),
			'academicYear' => array(self::BELONGS_TO, 'Academicperiods', 'academic_year'),
			'fromSpecialCat' => array(self::BELONGS_TO, 'KinderCatConcept', 'from_special_cat'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student' => 'Student',
			'concept' => 'Concept',
			'mention' => 'Mention',
			'free_comments' => 'Free Comments',
			'from_special_cat' => 'From Special Cat',
			'academic_year' => 'Academic Year',
			'create_by' => 'Create By',
			'update_by' => 'Update By',
			'create_date' => 'Create Date',
			'update_date' => 'Update Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student',$this->student);
		$criteria->compare('concept',$this->concept);
		$criteria->compare('mention',$this->mention);
		$criteria->compare('free_comments',$this->free_comments,true);
		$criteria->compare('from_special_cat',$this->from_special_cat);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}