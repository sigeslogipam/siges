<?php

/**
 * This is the model class for table "kinder_concept".
 *
 * The followings are the available columns in table 'kinder_concept':
 * @property integer $id
 * @property string $concept_name
 * @property integer $category
 * @property integer $level
 * @property integer $academic_year
 * @property integer $teacher
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 *
 * The followings are the available model relations:
 * @property Academicperiods $academicYear
 * @property KinderCatConcept $category0
 * @property Persons $teacher0
 * @property KinderEvaluation[] $kinderEvaluations
 */
class KinderConcept extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KinderConcept the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kinder_concept';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('concept_name, category, level, academic_year', 'required'),
			array('category, level, academic_year', 'numerical', 'integerOnly'=>true),
			array('create_by, update_by', 'length', 'max'=>64),
			//array('create_date, update_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('concept_name, category, academic_year, create_by, update_by, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'academicYear' => array(self::BELONGS_TO, 'Academicperiods', 'academic_year'),
			'category0' => array(self::BELONGS_TO, 'KinderCatConcept', 'category'),
			'teacher0' => array(self::BELONGS_TO, 'Persons', 'teacher'),
			'kinderEvaluations' => array(self::HAS_MANY, 'KinderEvaluation', 'concept'),
                        'level0' => array(self::BELONGS_TO, 'Levels', 'level'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'concept_name' => 'Concept Name',
			'category' => 'Category',
			'level' => 'Level',
			'academic_year' => 'Academic Year',
			'teacher' => 'Teacher',
			'create_by' => 'Create By',
			'update_by' => 'Update By',
			'create_date' => 'Create Date',
			'update_date' => 'Update Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		
		$criteria->compare('concept_name',$this->concept_name,true);
		$criteria->compare('category',$this->category);
		$criteria->compare('level',$this->level);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('teacher',$this->teacher);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}