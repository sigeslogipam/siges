<?php

/**
 * This is the model class for table "kinder_period".
 *
 * The followings are the available columns in table 'kinder_period':
 * @property integer $id
 * @property string $period_name
 * @property integer $academic_year
 * @property string $date_start
 * @property string $date_end
 * @property integer $is_last_period
 * @property string $create_by
 * @property string $update_by
 * @property string $create_date
 * @property string $update_date
 *
 * The followings are the available model relations:
 * @property Academicperiods $academicYear
 */
class KinderPeriod extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return KinderPeriod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'kinder_period';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('period_name, academic_year, date_start, date_end', 'required'),
			array('academic_year, is_last_period', 'numerical', 'integerOnly'=>true),
			array('period_name', 'length', 'max'=>32),
			array('create_by, update_by', 'length', 'max'=>64),
			array('create_date, update_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, period_name, academic_year, date_start, date_end, is_last_period, create_by, update_by, create_date, update_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'academicYear' => array(self::BELONGS_TO, 'AcademicPeriods', 'academic_year'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'period_name' => 'Period Name',
			'academic_year' => 'Academic Year',
			'date_start' => 'Date Start',
			'date_end' => 'Date End',
			'is_last_period' => 'Is Last Period',
			'create_by' => 'Create By',
			'update_by' => 'Update By',
			'create_date' => 'Create Date',
			'update_date' => 'Update Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('period_name',$this->period_name,true);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('date_start',$this->date_start,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('is_last_period',$this->is_last_period);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('update_date',$this->update_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}