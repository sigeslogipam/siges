<?php

class KinderPeriodController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/l_dashboard';
        public $part = "";
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
        
        public function accessRules()
	{
		 $explode_url= explode("/",substr($_SERVER['REQUEST_URI'],1));
            $controller=$explode_url[3];
            
            $actions=$this->getRulesArray($this->module->name,$controller);
          
            if($this->getModuleName($this->module->name))
                {
		            if($actions!=null)
             			 {     
                                return array(
                                            array('allow',  

                                                    'actions'=>array_merge($actions,
                                                            array('create','update','saveperiod','refreshperiod','delete','updateperiod','yesno')
                                                            ),
		                                  'users'=>array(Yii::app()->user->name),
				                    	),
				              		  array('deny',  
					                 	'users'=>array('*'),
				                    ),
			                );
             			 }
             			 else
             			  return array(array('deny', 'users'=>array('*')),);
                }
                else
                {
                    return array(array('deny', 'users'=>array('*')),);
                }


         
	}
        /*
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view',),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','saveperiod','refreshperiod','delete','updateperiod','yesno'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
         * 
         */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new KinderPeriod;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KinderPeriod']))
		{
			$model->attributes=$_POST['KinderPeriod'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->renderPartial('create',array(
			'model'=>$model,
		));
	}
        
        public function actionSaveperiod($period_name, $date_start, $date_end, $is_last_period){
            
            $model = new KinderPeriod();
            $acad=Yii::app()->session['currentId_academic_year']; 
            $model->period_name = $period_name;
            $model->date_start = date('Y-m-d',strtotime($date_start));
            $model->date_end = date('Y-m-d',strtotime($date_end));
            $model->is_last_period = $is_last_period; 
            $model->academic_year = $acad; 
            $model->create_by = currentUser(); 
            $model->create_date = date('Y-m-d H:i:s');
            $model->save();
            
            return $this->renderPartial('refresh-period');
        }
        
        public function actionRefreshperiod(){
            return $this->renderPartial('refresh-period');
        }

        /**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KinderPeriod']))
		{
			$model->attributes=$_POST['KinderPeriod'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        public function actionUpdateperiod(){
            // if (isset($_POST['csrf_token'])) {
                $kote_pou_modifye = $_POST['name'];
                switch ($kote_pou_modifye){
                        case 'period_name':{
                            $model = $this->loadModel($_POST['pk']);
                            $model->period_name = $_POST['value']; 
                            $model->update_by = currentUser(); 
                            $model->update_date = date('Y-m-d H:i:s');
                            $model->save(); 
                        }
                        break;
                        case 'date_start':{
                            $model = $this->loadModel($_POST['pk']);
                            $model->date_start = date('Y-m-d',strtotime($_POST['value']));
                            $model->update_by = currentUser(); 
                            $model->update_date = date('Y-m-d H:i:s');
                            $model->save(); 
                        }
                        break;
                        case 'date_end':{
                            $model = $this->loadModel($_POST['pk']);
                            $model->date_end = date('Y-m-d',strtotime($_POST['value']));
                            $model->update_by = currentUser(); 
                            $model->update_date = date('Y-m-d H:i:s');
                            $model->save(); 
                        }
                        break; 
                        case 'is_last_period':{
                                $model = $this->loadModel($_POST['pk']);
                                $model->is_last_period = $_POST['value'];
                                $model->update_by = currentUser(); 
                                $model->update_date = date('Y-m-d H:i:s');
                                $model->save(); 
                            }
                }
             //}
        }
        
        public function actionYesno(){
            
        $all_yesno = array(
            array('value'=>0,'text'=>Yii::t('app','No')),
            array('value'=>1,'text'=>Yii::t('app','Yes'))    
            );
        $json_yesno =json_encode($all_yesno,false); 
                echo $json_yesno;
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
                try{
		$this->loadModel($id)->delete();
                    return $this->renderPartial('refresh-period');
                
                }catch(Exception $e){
                    echo $e->getCode(); 
                }
        }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                $this->part = 'conf';
		$dataProvider=new CActiveDataProvider('KinderPeriod');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KinderPeriod('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KinderPeriod']))
			$model->attributes=$_GET['KinderPeriod'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return KinderPeriod the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=KinderPeriod::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param KinderPeriod $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kinder-period-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function beforeAction($action) {
            //Yii::app()->params->request->enableCsrfvalidation = false;
            Yii::app()->request->enableCsrfValidation = true;
            //$this->enableCsrfValidation = false;
            return parent::beforeAction($action);
        }
}
