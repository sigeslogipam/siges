<?php

class KinderConceptController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','saveconcept','refreshconcept','listcat','listlevel','listperson','updateconcept','delete','createteacher','refreshteacher','saveteacher', 'deleteteacher'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new KinderConcept;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KinderConcept']))
		{
			$model->attributes=$_POST['KinderConcept'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->renderPartial('create',array(
			'model'=>$model,
		));
	}
        
        
        public function actionCreateteacher(){
            $model = new KinderTeacher(); 
            
            $this->renderPartial('create-teacher',array(
                'model'=>$model,
            ));
        }
        
        public function actionSaveconcept($concept_name, $kategori, $level, $academic_year){
            $model = new KinderConcept();
            $model->concept_name = $concept_name;
            $model->category = $kategori;
            $model->level = $level; 
           // $model->teacher = $teacher;
            $model->academic_year = $academic_year;
            $model->create_by = currentUser(); 
            $model->create_date = date('Y-m-d H:i:s');
            $model->save();
            
            return $this->renderPartial('refresh-concept');
        }
        
        public function actionSaveteacher($teacher, $room, $academic_year){
            $model = new KinderTeacher(); 
            $model->teacher = $teacher; 
            $model->room = $room; 
            $model->academic_year = $academic_year; 
            $model->create_by = currentUser();
            $model->create_date = date('Y-m-d H:i:s');
            if(!$this->isTeacherAlreadyThere($teacher, $academic_year)){
                $model->save(); 
                return $this->renderPartial('refresh-teacher');
            }else{
                return 0;
            }
            
        }
        
        public function isTeacherAlreadyThere($teacher, $academic_year){
            $data_teacher = KinderTeacher::model()->findAllByAttributes(array('teacher'=>$teacher,'academic_year'=>$academic_year));
            $teacher_array = array(); 
            foreach($data_teacher as $dt){
                $teacher_array = $dt->id;
            }
            $flag = False; 
            if(!empty($teacher_array)){
                $flag = False; 
            }else{
                $flag = True;
            }
        }
        
        public function actionRefreshconcept(){
            return $this->renderPartial('refresh-concept');
        }
        
        public function actionRefreshteacher(){
            return $this->renderPartial('refresh-teacher');
        }
        
        public function  actionListcat(){
         $all_list_cat = CHtml::listData(KinderCatConcept::model()->findAll(),'id','cat_name');
         $json_list_cat =json_encode($all_list_cat,false); 
         echo $json_list_cat; 
        }
        
        public function  actionListlevel(){
         $defaut_section_kinder = infoGeneralConfig('kindergarden_section');   
         $all_list_level = CHtml::listData(Levels::model()->findAllBySql("SELECT l.id, l.level_name, l.short_level_name  FROM levels l INNER JOIN sections s ON (l.section = s.id) WHERE s.section_name LIKE '$defaut_section_kinder'"),'id','short_level_name');
         $json_list_level =json_encode($all_list_level,false); 
         echo $json_list_level; 
        }
        
        public function  actionListperson(){
         $acad=Yii::app()->session['currentId_academic_year'];   
         $all_list_person = CHtml::listData(Persons::model()->findAllBySql("SELECT id, first_name, last_name  FROM `persons` Where id NOT IN (SELECT teacher FROM courses WHERE academic_period = $acad) AND is_student <> 1 AND active IN (1,2) ORDER BY last_name ASC"),'id','fullName');
         $json_list_person =json_encode($all_list_person,false); 
         echo $json_list_person; 
        }
        
        public function actionUpdateconcept(){
                $kote_pou_modifye = $_POST['name'];
                switch ($kote_pou_modifye){
                        case 'concept_name':{
                            $model = $this->loadModel($_POST['pk']);
                            $model->concept_name = $_POST['value']; 
                            $model->update_by = currentUser(); 
                            $model->update_date = date('Y-m-d H:i:s');
                            $model->save(); 
                        }
                        break;
                        case 'category':{
                            $model = $this->loadModel($_POST['pk']);
                            $model->category = $_POST['value'];
                            $model->update_by = currentUser(); 
                            $model->update_date = date('Y-m-d H:i:s');
                            $model->save(); 
                        }
                        break;
                        case 'level':{
                                $model = $this->loadModel($_POST['pk']);
                                $model->level = $_POST['value'];
                                $model->update_by = currentUser(); 
                                $model->update_date = date('Y-m-d H:i:s');
                                $model->save(); 
                            }
                        break;
                        case 'teacher':{
                                $model = $this->loadModel($_POST['pk']);
                                $model->teacher = $_POST['value'];
                                $model->update_by = currentUser(); 
                                $model->update_date = date('Y-m-d H:i:s');
                                $model->save(); 
                            }
                        
                }
        }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KinderConcept']))
		{
			$model->attributes=$_POST['KinderConcept'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		try{
                    $this->loadModel($id)->delete();
                    return $this->renderPartial('refresh-concept');
                
                }catch(Exception $e){
                    echo $e->getCode(); 
            }
	}
        
        public function actionDeleteteacher($id)
	{
		try{
                    //$this->loadModel($id)->delete();
                    KinderTeacher::model()->findByPk($id)->delete();
                    return $this->renderPartial('refresh-teacher');
                
                }catch(Exception $e){
                    echo $e->getCode(); 
            }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('KinderConcept');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KinderConcept('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KinderConcept']))
			$model->attributes=$_GET['KinderConcept'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return KinderConcept the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=KinderConcept::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param KinderConcept $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kinder-concept-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
