<?php

class KinderEvaluationController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/l_dashboard';
        public $part = '';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
        
        public function accessRules()
	{
		 $explode_url= explode("/",substr($_SERVER['REQUEST_URI'],1));
            $controller=$explode_url[3];
            
            $actions=$this->getRulesArray($this->module->name,$controller);
          
            if($this->getModuleName($this->module->name))
                {
		            if($actions!=null)
             			 {     
                                return array(
                                            array('allow',  

                                                    'actions'=>array_merge($actions,
                                                            array('combostud','comboteach','kindereval','listmention','updateevaluation','createevaluation','trombinoscope','makepdf','makehtml','createevaluationspecial','updateevaluationspecial','reportcard','listreportcard','listteacher')
                                                            ),
		                                  'users'=>array(Yii::app()->user->name),
				                    	),
				              		  array('deny',  
					                 	'users'=>array('*'),
				                    ),
			                );
             			 }
             			 else
             			  return array(array('deny', 'users'=>array('*')),);
                }
                else
                {
                    return array(array('deny', 'users'=>array('*')),);
                }


         
	}
         
        /*
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','combostud','kindereval','listmention','updateevaluation','createevaluation','trombinoscope','makepdf','makehtml','createevaluationspecial','updateevaluationspecial','reportcard','listreportcard'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}
         */

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                
		$model=new KinderEvaluation;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KinderEvaluation']))
		{
			$model->attributes=$_POST['KinderEvaluation'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        
        public function actionCombostud($room){
            $acad = Yii::app()->session['currentId_academic_year'];
            $defaut_section_kinder = infoGeneralConfig('kindergarden_section');
            $person_data = Persons::model()->findAllBySql("SELECT * FROM persons WHERE id IN (SELECT students FROM room_has_person  rhp  INNER JOIN rooms r ON (rhp.room = r.id) INNER JOIN levels l ON (r.level = l.id) INNER JOIN sections s ON (l.section = s.id) WHERE rhp.academic_year = $acad AND s.section_name = '$defaut_section_kinder') AND active IN (1,2) ORDER BY last_name ASC");
            $this->renderPartial('combo-stud',array('person_data'=>$person_data)); 
        }
        
        public function actionComboteach(){
            $acad = Yii::app()->session['currentId_academic_year'];
            $person_data = Persons::model()->findAllBySql("SELECT * FROM `persons` WHERE id IN (SELECT teacher FROM kinder_teacher WHERE academic_year = $acad) AND active IN (1,2) ORDER BY last_name ASC");
            $this->renderPartial('combo-teacher',array('person_data'=>$person_data)); 
        }
        
        public function actionKindereval($room,$period,$student){
            if(isset(Rooms::model()->findByPk($room)->level)){
                $level = Rooms::model()->findByPk($room)->level;
                $acad = Yii::app()->session['currentId_academic_year'];
                $cat_data = KinderCatConcept::model()->findAllBySql("SELECT  DISTINCT kcc.id, kcc.cat_name FROM `kinder_concept` kc INNER JOIN kinder_cat_concept kcc ON (kc.category = kcc.id) WHERE kc.academic_year = $acad AND kc.level = $level");
                
                $cat_special_data = KinderCatConcept::model()->findAllBySql("SELECT id, cat_name FROM kinder_cat_concept WHERE cat_special = 1");
                $this->renderPartial('refresh-eval',array(
                    'cat_data'=>$cat_data,
                    'cat_special_data'=>$cat_special_data,
                    'period'=>$period,'student'=>$student,
                    'room'=>$room,
                    'level'=>$level,
                        ));
            }else{
                echo 'Error';
            }
           
            
        }
        
        public function  actionListmention(){
             $all_list_mention = CHtml::listData(KinderMention::model()->findAll(),'id','mention_short_name');
             $aucun_a = array('0'=>Yii::t('app','None'));
             $json_list_mention =json_encode($all_list_mention+$aucun_a,false); 
             echo $json_list_mention; 
             //print_r($all_list_mention);
        }
        
        public function actionUpdateevaluation(){
            $evaluation = KinderEvaluation::model()->findByPk($_POST['pk']); 
            if($_POST['value']!=0){
            $evaluation->mention = $_POST['value']; 
            $evaluation->update_by = currentUser(); 
            $evaluation->update_date = date('Y-m-d H:i:s');
            $evaluation->save(); 
            }else{
                
            }
        }
        
        public function actionUpdateevaluationspecial(){
            $evaluation = KinderEvaluation::model()->findByPk($_POST['pk']); 
            
            $evaluation->free_comments = $_POST['value']; 
            $evaluation->update_by = currentUser(); 
            $evaluation->update_date = date('Y-m-d H:i:s');
            $evaluation->save(); 
           
        }
        
        
        
        public function actionCreateevaluation($student,$period){
            $acad = Yii::app()->session['currentId_academic_year'];
            if($_POST['value']!=0){
            $evaluation = new KinderEvaluation(); 
            $evaluation->student = $student; 
            $evaluation->concept = $_POST['pk'];
            $evaluation->mention = $_POST['value'];
            $evaluation->academic_year = $acad; 
            $evaluation->period = $period;
            $evaluation->create_by = currentUser(); 
            $evaluation->create_date = date('Y-m-d H:i:s');
            $evaluation->save();
            }else{
                
            }
            
        }
        
        public function actionCreateevaluationspecial($student,$period){
            $acad = Yii::app()->session['currentId_academic_year'];
            $evaluation = new KinderEvaluation(); 
            $evaluation->student = $student; 
            $evaluation->from_special_cat = $_POST['pk'];
            $evaluation->free_comments = $_POST['value'];
            $evaluation->academic_year = $acad; 
            $evaluation->period = $period;
            $evaluation->create_by = currentUser(); 
            $evaluation->create_date = date('Y-m-d H:i:s');
            $evaluation->save();
            
            
        }
        
        public function actionTrombinoscope($room){
            $acad = Yii::app()->session['currentId_academic_year'];
            $person_data = Persons::model()->findAllBySql("SELECT id, last_name, first_name, image FROM `persons` WHERE id IN (SELECT students FROM room_has_person WHERE room = $room AND academic_year = $acad) AND active IN (1,2) ORDER BY last_name ASC");
            $this->renderPartial('trombinoscope',array('person_data'=>$person_data)); 
        }
        
        public function actionListreportcard($room){
            $acad = Yii::app()->session['currentId_academic_year'];
            $person_data = Persons::model()->findAllBySql("SELECT id, last_name, first_name, image FROM persons WHERE id IN (SELECT students FROM room_has_person WHERE room = $room AND academic_year = $acad) AND active IN (1,2) ORDER BY last_name ASC");
            $this->renderPartial('list_reportcard',array('person_data'=>$person_data)); 
        }
        
public function actionMakepdf($room,$period,$student){
    if(isset(Rooms::model()->findByPk($room)->level)){
                $level = Rooms::model()->findByPk($room)->level;
                $acad = Yii::app()->session['currentId_academic_year'];
                $cat_data = KinderCatConcept::model()->findAllBySql("SELECT  DISTINCT kcc.id, kcc.cat_name FROM `kinder_concept` kc INNER JOIN kinder_cat_concept kcc ON (kc.category = kcc.id) WHERE kc.academic_year = $acad AND kc.level = $level");
                $cat_special_data = KinderCatConcept::model()->findAllBySql("SELECT id, cat_name FROM kinder_cat_concept WHERE cat_special = 1");
                $this->renderPartial('makepdf',array(
                    'cat_data'=>$cat_data,
                    'cat_special_data'=>$cat_special_data,
                    'period'=>$period,
                    'student'=>$student,
                    'room'=>$room,
                    'level'=>$level,
                        ));
            }else{
                echo 'Error';
            }
    
}

public function actionMakehtml($room,$period,$student){
    if(isset(Rooms::model()->findByPk($room)->level)){
                $level = Rooms::model()->findByPk($room)->level;
                $acad = Yii::app()->session['currentId_academic_year'];
                $cat_data = KinderCatConcept::model()->findAllBySql("SELECT  DISTINCT kcc.id, kcc.cat_name FROM `kinder_concept` kc INNER JOIN kinder_cat_concept kcc ON (kc.category = kcc.id) WHERE kc.academic_year = $acad AND kc.level = $level");
                $cat_special_data = KinderCatConcept::model()->findAllBySql("SELECT id, cat_name FROM kinder_cat_concept WHERE cat_special = 1");
                $this->renderPartial('data-pdf',array(
                    'cat_data'=>$cat_data,'cat_special_data'=>$cat_special_data,
                    'period'=>$period,'student'=>$student,'room'=>$room
                        ));
            }else{
                echo 'Error';
            }
    
}



	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['KinderEvaluation']))
		{
			$model->attributes=$_POST['KinderEvaluation'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                $acad = Yii::app()->session['currentId_academic_year'];
                $concept = KinderConcept::model()->findAllBySql("SELECT * FROM kinder_concept WHERE academic_year = $acad");
                $temoin_arr = array(); 
                $i = 0;
                foreach($concept as $c){
                     $temoin_arr[$i] = $c->id;
                     $i++;
                }
                if(!empty($temoin_arr)){
                $this->part = 'eval';
		$dataProvider=new CActiveDataProvider('KinderEvaluation');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
                }else{
                    $this->part = 'conf';
                    //$this->render('redirect');
                    return $this->redirect(array('kinderPeriod/index?part=conf'));
                }
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new KinderEvaluation('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['KinderEvaluation']))
			$model->attributes=$_GET['KinderEvaluation'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return KinderEvaluation the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=KinderEvaluation::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param KinderEvaluation $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='kinder-evaluation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        
        public function conceptByCategory($id_cat,$level){
            $acad = Yii::app()->session['currentId_academic_year'];
            $data_concept = KinderConcept::model()->findAllBySql("SELECT id, concept_name FROM kinder_concept WHERE category = $id_cat AND academic_year = $acad AND level = $level");
            return $data_concept;
        }
        
        public function findMentionByStudent($student, $concept,$period,$is_special = NULL){
            if(!isset($is_special)){
                $acad = Yii::app()->session['currentId_academic_year'];
                $eval_array = array();
                $data_eval = KinderEvaluation::model()->findAllBySql("SELECT * FROM kinder_evaluation WHERE academic_year = $acad AND student = $student AND period = $period AND concept = $concept");
                foreach($data_eval as $de){
                    $eval_array['id'] = $de->id;
                    $eval_array['mention'] = $de->mention;

                }
                if(!empty($eval_array)){
                    return $eval_array;
                }else{
                    return NULL;
                }
            }elseif($is_special==1){
                $acad = Yii::app()->session['currentId_academic_year'];
                $eval_array = array();
                $data_eval = KinderEvaluation::model()->findAllBySql("SELECT * FROM kinder_evaluation WHERE academic_year = $acad AND student = $student AND period = $period AND from_special_cat = $concept");
                foreach($data_eval as $de){
                    $eval_array['id'] = $de->id;
                    $eval_array['mention'] = $de->free_comments;

                }
                if(!empty($eval_array)){
                    return $eval_array;
                }else{
                    return NULL;
                }
            }
             
        }
        
        public function actionReportcard(){
            $this->part = 'rpt';
            return $this->render('reportcard');
        }
        
        public function actionList(){
             $this->part = 'lst';
            return $this->render('list');
        }
        
        public function actionListteacher(){
             $this->part = 'tea';
            return $this->render('listteacher');
        }
        
}
