<?php
/* @var $this ArticlelevelController */
/* @var $data articlelevel */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_article')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id_article), array('view', 'id'=>$data->id_article)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_product')); ?>:</b>
	<?php echo CHtml::encode($data->id_product); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_level')); ?>:</b>
	<?php echo CHtml::encode($data->id_level); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_academicyear')); ?>:</b>
	<?php echo CHtml::encode($data->id_academicyear); ?>	<br />


	<b><?php echo CHtml::encode($data->getAttributeLabel('article_quantite')); ?>:</b>
	<?php echo CHtml::encode($data->article_quantite); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prix_achat')); ?>:</b>
	<?php echo CHtml::encode($data->prix_achat); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('prix_vente')); ?>:</b>
	<?php echo CHtml::encode($data->prix_vente); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('alerte')); ?>:</b>
	<?php echo CHtml::encode($data->alerte); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('classe')); ?>:</b>
	<?php echo CHtml::encode($data->classe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('salle')); ?>:</b>
	<?php echo CHtml::encode($data->salle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nom_eleve')); ?>:</b>
	<?php echo CHtml::encode($data->nom_eleve); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('annee_academique')); ?>:</b>
	<?php echo CHtml::encode($data->annee_academique); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description_article')); ?>:</b>
	<?php echo CHtml::encode($data->description_article); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_date')); ?>:</b>
	<?php echo CHtml::encode($data->created_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_date')); ?>:</b>
	<?php echo CHtml::encode($data->updated_date); ?>
	<br />

	*/ ?>

</div>