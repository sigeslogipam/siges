<?php
/* @var $this ArticlelevelController */
/* @var $model articlelevel */

$this->breadcrumbs=array(
	'Articlelevels'=>array('index'),
	$model->id_article=>array('view','id'=>$model->id_article),
	'Update',
);

$this->menu=array(
	array('label'=>'List articlelevel', 'url'=>array('index')),
	array('label'=>'Create articlelevel', 'url'=>array('create')),
	array('label'=>'View articlelevel', 'url'=>array('view', 'id'=>$model->id_article)),
	array('label'=>'Manage articlelevel', 'url'=>array('admin')),
);
?>

<h1>Update articlelevel <?php echo $model->id_article; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>