<?php
/* @var $this ArticlelevelController */
/* @var $model articlelevel */
/* @var $form CActiveForm */

$acad_sess=acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'articlelevel-form',
	'enableAjaxValidation'=>false,
)); ?>
<div class="b_m">
	<p class="note">Fields with <span class="required">*</span> are required.</p>
        
        <br/>

	<?php echo $form->errorSummary($model); ?>
   <div id="resp_form_siges">  
       <form  id="resp_form">
           <div class="col-2">
                <label id="resp_form">
                    <?php echo $form->labelEx($model,'id_product'); ?>
                    <?php echo $form->textField($model,'id_product'); ?>
                    <?php echo $form->error($model,'id_product'); ?>
                </label>
           </div>

	
       <div class="col-2">
           <label id="resp_form">
                <?php echo $form->labelEx($model,'id_level'); ?>
                <?php echo $form->textField($model,'id_level'); ?>
                <?php echo $form->error($model,'id_level'); ?>   
           </label>                   
       </div>
        
           <div class="col-2">
               <label id="resp_form">
                   <?php echo $form->labelEx($model,'id_academicyear'); ?>
                   <?php echo $form->textField($model,'id_academicyear'); ?>
                   <?php echo $form->error($model,'id_academicyear'); ?>
               </label>
           </div>         
          
	
           <div class="col-2">
               <label>
                    <?php echo $form->labelEx($model,'article_quantite'); ?>
                    <?php echo $form->textField($model,'article_quantite'); ?>
                    <?php echo $form->error($model,'article_quantite'); ?>
               </label>
           </div>           
	
           <div class="col-2">
               <label>
                    <?php echo $form->labelEx($model,'prix_achat'); ?>
                    <?php echo $form->textField($model,'prix_achat'); ?>
                    <?php echo $form->error($model,'prix_achat'); ?>
               </label>
           </div>
           
           <div class="col-2">
               <label>
                    <?php echo $form->labelEx($model,'prix_vente'); ?>
                    <?php echo $form->textField($model,'prix_vente'); ?>
                    <?php echo $form->error($model,'prix_vente'); ?>
               </label>
           </div>

           <div class="col-2">
               <label>
                   <?php echo $form->labelEx($model,'alerte'); ?>
                    <?php echo $form->textField($model,'alerte'); ?>
                    <?php echo $form->error($model,'alerte'); ?>
               </label>
           </div>

           <div class="col-2">
               <label>
                   <?php echo $form->labelEx($model,'classe'); ?>
                   <?php echo $form->textField($model,'classe',array('size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'classe'); ?>
               </label>
           </div>

           <div class="col-2">
               <label>
                   <?php echo $form->labelEx($model,'salle'); ?>
                   <?php echo $form->textField($model,'salle',array('size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'salle'); ?>
               </label>
           </div>

           <div class="col-2">
               <label>
                   <?php echo $form->labelEx($model,'annee_academique'); ?>
                   <?php echo $form->textField($model,'annee_academique',array('size'=>60,'maxlength'=>255)); ?>
                   <?php echo $form->error($model,'annee_academique'); ?>
               </label>
           </div>

           <div class="col-2">
               <label>
                   <?php echo $form->labelEx($model,'description_article'); ?>
                   <?php echo $form->textArea($model,'description_article',array('rows'=>6, 'cols'=>50)); ?>
                   <?php echo $form->error($model,'description_article'); ?>
               </label>
           </div>
           
           <div class="col-2">
               <label>
                    <?php echo $form->labelEx($model,'created_by'); ?>
                    <?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'created_by'); ?>
               </label>
           </div>
	
           <div class="col-2">
               <label>
                    <?php echo $form->labelEx($model,'updated_by'); ?>
                    <?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>255)); ?>
                    <?php echo $form->error($model,'updated_by'); ?>
               </label>
           </div>
	
            <div class="col-2">
                <label>
                    <?php echo $form->labelEx($model,'created_date'); ?>
                    <?php echo $form->textField($model,'created_date'); ?>
                    <?php echo $form->error($model,'created_date'); ?>
                </label>
            </div>

            <div class="col-2">
                <label>
                    <?php echo $form->labelEx($model,'updated_date'); ?>
                    <?php echo $form->textField($model,'updated_date'); ?>
                    <?php echo $form->error($model,'updated_date'); ?>
                </label>
            </div>
           
           <div class="col-submit">
               <?php
                    if(!isset($_GET['id'])){
                         if(!isAchiveMode($acad_sess))
                             echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning'));
                         
                         echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                    }else{
                        if(!isAchiveMode($acad_sess))
                            echo CHtml::submitButton(Yii::t('app', 'Save'),array('name'=>'update','class'=>'btn btn-warning'));
                        
                        echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                    }
               ?>
           </div>

	<!--<div class="row buttons">
		
	</div>-->
       </form><!-- resp_form-->
    </div><!-- resp_form_siges -->

<?php $this->endWidget(); ?>
        </div><!-- b_m -->

</div><!-- form -->