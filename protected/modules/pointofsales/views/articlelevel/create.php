<?php
/* @var $this ArticlelevelController */
/* @var $model articlelevel */

$this->breadcrumbs=array(
	'Articlelevels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List articlelevel', 'url'=>array('index')),
	array('label'=>'Manage articlelevel', 'url'=>array('admin')),
);
?>

<h1>Create articlelevel</h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>