<?php
/* @var $this ArticlelevelController */
/* @var $model articlelevel */

$this->breadcrumbs=array(
	'Articlelevels'=>array('index'),
	$model->id_article,
);

$this->menu=array(
	array('label'=>'List articlelevel', 'url'=>array('index')),
	array('label'=>'Create articlelevel', 'url'=>array('create')),
	array('label'=>'Update articlelevel', 'url'=>array('update', 'id'=>$model->id_article)),
	array('label'=>'Delete articlelevel', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id_article),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage articlelevel', 'url'=>array('admin')),
);
?>

<h1>View articlelevel #<?php echo $model->id_article; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id_article',
		'id_product',
		'id_level',
		'id_academicyear',		
		'article_quantite',
		'prix_achat',
		'prix_vente',
		'alerte',
		'classe',
		'salle',
		'nom_eleve',
		'annee_academique',
		'description_article',
		'created_by',
		'updated_by',
		'created_date',
		'updated_date',
	),
)); ?>
