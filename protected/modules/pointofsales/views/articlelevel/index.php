<?php 
/*
 * © 2010 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>
<?php
/* @var $this ProductsController */
/* @var $model Products */

$acad_sess=acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];

$template = '';



$this->pageTitle = Yii::app()->name.' - '.Yii::t('app','Manage Stocks');

?>

/*Espace pour le menu CRUD*/


<div style="clear:both"></div>

<div >

</div>    

<div style="clear:both"></div>

/* ESPACE POUR LE FORMULAIRE SEARCH*/

<?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile($baseUrl."/css_pointofsales/style.css");
    $cs->registerScriptFile($baseUrl.'/js_pointofsales/all.js');
    $cs->registerScriptFile($baseUrl.'/js_pointofsales/iframeResizer.contentWindow.min.js');
    $cs->registerScriptFile($baseUrl.'/js_pointofsales/nr-1123.min.js');
?>


	<div class="col-lg-8 col-md-7 col-sm-12 col-xs-12 no-padding-right no-padding-left">
            <div class="register-box register-items-form">
                        <a tabindex="-1" href="#" class="dismissfullscreen hidden"><i class="ion-close-circled"></i></a>
			<div id="itemForm" class="item-form">
				<!-- Item adding form -->
				
				<form action="https://" id="add_item_form" class="form-inline" autocomplete="off" method="post" accept-charset="utf-8">
					
					<div class="input-group input-group-mobile contacts">
						<span class="input-group-addon">
							<a href="https:// class="none add-new-item" title="Nouvel article" id="new-item-mobile" tabindex="-1"><i class='icon ti-pencil-alt'></i> <span class='register-btn-text'>Nouvel article</span></a>						</span>		
						<div class="input-group-addon">
							<a href="http://localhost/siges_2/index.php/pointofsales/sellings/create" class="none active" tabindex="-1" title="Vente" id="select-mode-1" data-target="#"  aria-haspopup="true" role="button" aria-expanded="false"><i class='icon ti-shopping-cart'></i> <span class='register-btn-text'>Vente</span></a>					        
                                                           
						</div>
						
											</div>
					
					<div class="input-group contacts register-input-group">

						<!-- Css Loader  -->
						<div class="spinner" id="ajax-loader" style="display:none">
						  <div class="rect1"></div>
						  <div class="rect2"></div>
						  <div class="rect3"></div>
						</div>
						
                                                <!-- pour Ajouter des Articles-->
						<span class="input-group-addon">
							<a href="https://" class="none add-new-item" title="Nouvel article" id="new-item" tabindex="-1">
                                                            <!--<i class='icon ti-pencil-alt'></i>-->
                                                        <div class="span4">
                                                                <?php
                                                                   $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                                                                             // build the link in Yii standard
                                                                   echo CHtml::link($images,array('/pointofsales/articlelevel/create')); 
                                                                 ?>
                                                        </div>
                                                        </a>			
                                                </span>
						<input type="text" id="item" name="item"  class="add-item-input pull-left keyboardTop" placeholder="Entrez le nom ou code barre de l'article" data-title="Nom d'article">
		
						<div class="input-group-addon">
							<a href="http://localhost/siges_2/index.php/pointofsales/sellings/create" class="none active" tabindex="-1" title="Vente" id="select-mode-2" data-target="#" aria-haspopup="true" role="button" aria-expanded="false">
                                                            <i class='icon ti-shopping-cart'></i>Vente
                                                            
                                                        </a>					        
                                                            
                                                    
						</div>
						
						
					</div>
					
				</form>
			</div>
		</div>
				<!-- Register Items. @contains : Items table -->
		<div class="register-box register-items paper-cut">
			<div class="register-items-holder">
									
					<table id="register" class="table table-hover">

					
				
                                        <!--pour Afficher les articles enregistres-->
					<tbody class="register-item-content">
                                            
                                            <tr class="cart_content_area">
                                                <td colspan='6'>
                                                    <div class='text-center text-warning' > 
                                                       <?php
                                                       
                                                       $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'articlelevel-grid',
	'dataProvider'=>$model->search(),
	//'filter'=>$model,
	'columns'=>array(
	
		
		'article_quantite',
                'prix_vente',
                'classe',
                'description_article',
            
		/*
		'prix_achat',
		'id_article',
		'id_product',
		'id_level',
		'id_academicyear',
		'alerte',
		
		'salle',
		'nom_eleve',
		'annee_academique',
		
		'created_by',
		'updated_by',
		'created_date',
		'updated_date',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
));?>
                                                    </div>
                                                </td>
                                            </tr>
					</tbody>
                                    </table>
					
			</div>

			<!-- End of Sales or Return Mode -->
						<!-- End of Store Account Payment Mode -->

		</div>
		<!-- /.Register Items -->
			</div>






