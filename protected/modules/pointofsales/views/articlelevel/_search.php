<?php
/* @var $this ArticlelevelController */
/* @var $model articlelevel */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id_article'); ?>
		<?php echo $form->textField($model,'id_article'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_product'); ?>
		<?php echo $form->textField($model,'id_product'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_level'); ?>
		<?php echo $form->textField($model,'id_level'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_academicyear'); ?>
		<?php echo $form->textField($model,'id_academicyear'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'article_name'); ?>
		<?php echo $form->textField($model,'article_name',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'article_quantite'); ?>
		<?php echo $form->textField($model,'article_quantite'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prix_achat'); ?>
		<?php echo $form->textField($model,'prix_achat'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'prix_vente'); ?>
		<?php echo $form->textField($model,'prix_vente'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'alerte'); ?>
		<?php echo $form->textField($model,'alerte'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'classe'); ?>
		<?php echo $form->textField($model,'classe',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'salle'); ?>
		<?php echo $form->textField($model,'salle',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nom_eleve'); ?>
		<?php echo $form->textField($model,'nom_eleve'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'annee_academique'); ?>
		<?php echo $form->textField($model,'annee_academique',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description_article'); ?>
		<?php echo $form->textArea($model,'description_article',array('rows'=>6, 'cols'=>50)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_by'); ?>
		<?php echo $form->textField($model,'created_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_by'); ?>
		<?php echo $form->textField($model,'updated_by',array('size'=>60,'maxlength'=>255)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'created_date'); ?>
		<?php echo $form->textField($model,'created_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'updated_date'); ?>
		<?php echo $form->textField($model,'updated_date'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->