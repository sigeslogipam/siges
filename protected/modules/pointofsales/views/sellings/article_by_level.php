<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$student; 
$product_id;

$acad=Yii::app()->session['currentId_academic_year']; 
// Recupere la classe de l'eleve pour l'annee academique en cours 
$level_id =  $this->getLevelByStudentId($student,$acad); 
$str_sql = "SELECT abl.id_article, abl.id_product, p.product_name, s.selling_price FROM article_by_level abl INNER JOIN products p ON (abl.id_product = p.id) INNER JOIN stocks s ON (p.id = s.id_product) WHERE abl.id_academicyear = $acad AND id_level = $level_id";
$all_products = Products::model()->findAllBySql($str_sql);
$student_name = Persons::model()->findByPk($student)->fullName; 
$level_name = Levels::model()->findByPk($level_id)->level_name;

?>
<br/>
<div class="alert alert-success">
    <h3>
    <?= Yii::t('app','List of article for {student_name} in level {level_name}',array('{student_name}'=>$student_name, '{level_name}'=>$level_name)); ?>
    </h3>
</div>

<?php 
if(!empty($all_products)){
?>
<table class="table responsive">
    <thead>
        <tr>
            <th>#</th>
            <th><?= Yii::t('app','Article Name')?></th>
            <th><?= Yii::t('app','Quantity')?></th>
            <th><?= Yii::t('app','Price')?></th>
            <th></th>
        </tr>
       </thead>
       <tbody>
        <?php 
        $k = 1;
        foreach($all_products as $ap){
            ?>
        <tr>
            <td><?= $k;?></td>
            <td><?= $ap->product_name; ?></td>
            <td><input type="text" value="1" name="qty_<?=$k;?>" id="qty_<?=$k;?>"></td>
            <td><?= $ap->selling_price; ?></td>
            <td><input type="checkbox" value="1" checked="checked" name="qty_<?=$k;?>" id="qty_<?=$k;?>"></td>
        </tr>
        <?php 
        $k++;
        }
         
        ?>
       </tbody>
    
</table>
<?php  } else{
    ?>

<div class="alert alert-warning">
    <?= Yii::t('app','No article found for {level_name}, please contact your administrator or set articles for this level !',array('{level_name}'=>$level_name) )?>
</div>
<?php 

}?>
