<?php 
/*
 * © 2016 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


?>
<?php

$acad_sess=acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
$acad_name=Yii::app()->session['currentName_academic_year'];

$currency_name = Yii::app()->session['currencyName'];
$currency_symbol = Yii::app()->session['currencySymbol']; 
 $baseUrl = Yii::app()->baseUrl;

/* @var $this SellingsController */
/* @var $model Sellings */
/* @var $form CActiveForm */
?>
<div class="form">
<?php 

        $form=$this->beginWidget('CActiveForm', array(
            'id'=>'sellings-form',
            'enableAjaxValidation'=>true,
        )); 
        
     
        
        $dataProvider = Sellings::model()->searchByTransaction($this->id_transaction); 
        $data_sale = $dataProvider->getData();
        $count = count($data_sale);
       
        $total = 0.00;
        foreach($data_sale as $ds){
            $total += $ds->amount_selling;
            
        }
        $this->amount_selling = $total;
?>
   
 
<div class="b_m">

<div class="row">
    <div class="span8">
        <form  id="resp_form">
            
            <div class="col-2">
                <label id="resp_form">
                    <?php echo $form->labelEx($model,'client_name'); ?>
                    <?php echo $form->dropDownList($model, 'client_name',$this->getStudentname(), array('prompt'=> Yii::t('app', '-- Select Student --')))?>
                    <?php echo $form->error($model,'client_name'); ?>
               </label>
           </div>
            
            <div id="list-article">             
               
            </div>
            
             <div class="col-2">
                 <label id="resp_form">
                     
                    
               </label>
           </div>
            
            <div class="col-2">
               <label id="resp_form">
                    
               </label>
           </div>
        
            <div class="">
                <label id="resp_form">
                    
                </label>
            </div>
            
            <hr/>
            
            <div class="row-fluid">
                <div class="col-2" style="display:inline-block; vertical-align: middle; width: 45%; margin: 10px 0;">
                    <label id="resp_form">
                        <?php echo '<label>'.Yii::t('app', 'Total amount').'</label>'; //echo $form->labelEx($model,'amount_selling'); ?>
                        <?php
                        if($this->quantity>0){
                            $this->amount_selling = $model->getAmountSellingByTransaction($this->id_transaction)+$this->unit_price*$this->quantity;
                        }else{
                            $this->amount_selling = $this->amount_selling;
                        }
                        
                        $amount_selling=$this->amount_selling;
                        echo '<input name="amount_selling" id="amount_selling" type="text" readonly=true value="'.$amount_selling.'" />';
                        
                        ?>
                        <?php echo $form->error($model,'amount_selling'); ?>
                    </label>
               </div>
                
                <div class="col-2" style="display:inline-block; vertical-align: middle; width: 45%; margin: 10px 0;">
                <label id="resp_form">
                <?php echo $form->labelEx($model,'discount'); ?>
                <?php echo $form->textField($model,'discount',array('placeholder'=>Yii::t('app','Discount %'),'name'=>'discount','id'=>'discount','value'=>$this->discount,'onchange'=>"rabais('$this->amount_selling');")); ?>
                <?php echo $form->error($model,'discount'); ?>
                </label>
              </div>
                
                <div class="col-2" style="display:inline-block; vertical-align: middle; width: 45%; margin: 10px 0;">
                    <label id="resp_form" >
                        <?php echo $form->labelEx($model,'amount_receive'); ?>
                        <?php echo $form->textField($model,'amount_receive',array('placeholder'=>Yii::t('app','Amount receive'),'name'=>'amount_receive', 'id'=>'amount_receive','value'=>$this->amount_receive, 'onchange'=>'difference()')); ?>
                        <?php echo $form->error($model,'amount_receive'); ?>
                    </label>
               </div>
                
                <div class="col-2" style="display:inline-block; vertical-align: middle; margin: 10px 0; width: 45%">
                    <label id="resp_form">
                        <?php echo $form->labelEx($model,'amount_balance'); ?>
                        <?php echo $form->textField($model,'amount_balance',array('placeholder'=>Yii::t('app','Balance'),'name'=>'balance','id'=>'balance','value'=>$this->amount_balance,'readonly'=>true)); ?>
                        <?php echo $form->error($model,'amount_balance'); ?>
                    </label>
               </div>
                <input type="hidden" id="message_credit" value="<?php echo Yii::t('app','Amount received is less than amount sold!') ?>"/>
                <input type="hidden" name="real_sale" id="real_sale" value="<?php echo $total; ?>"></input>
              

  <?php 
     
     if(!isAchiveMode($acad_sess))
        {     
        
   ?>
            
             
                <div class="col-submit" style="margin: 10px auto;">
                    
                    <button class="btn btn-success" type="submit"  id="close_sale" name="close_sale" disabled="disabled" oncontextmenu="printDiv('print_receipt')">
                        <?php echo Yii::t('app','Close sale'); ?>
                    </button>
                      
                    <button class="fa fa-print btn btn-success" disabled="disabled" id="print_sale" name="print_sale" onclick="printDiv('print_receipt');"> <?php echo Yii::t('app','Print'); ?></button>
                    
                </div>
      <?php
        }
      
      ?>       
           
                
            </div>
    </form>
    </div>

    
    <div id="print_receipt" style="display: none">
        <?php
                $school_name = infoGeneralConfig('school_name');
                $school_address = infoGeneralConfig('school_address');
                $school_phone_number= infoGeneralConfig('school_phone_number');
                $school_email_address= infoGeneralConfig('school_email_address');
                
        ?>
        
             
               
               
         
           
           <div id="header" style="">
                 
                  <div class="info"><?php echo headerLogo().'<b>'.strtoupper(strtr($school_name, pa_daksan() )).'</b><br/>'.$school_address.' &nbsp; / &nbsp; '.Yii::t('app','Tel: ').$school_phone_number.' &nbsp; / &nbsp; '.Yii::t('app','Email: ').$school_email_address; ?><br/></div> 
                  
                  <br/> 
                  
                  <div class="info" style="text-align:center;  margin-top:-9px; margin-bottom:-15px; "> <b><?php echo strtoupper(strtr(Yii::t('app','Receipt of sale'), pa_daksan() )); ?></b></div>
                  <br/>
                  </div>
           
           
           
           
   <div class="span3"></div>
        <div class="span6" style="align-content: center; text-align: center" >
                  
           
            <div class="row-fluid" style="align-content: center; text-align: center">
                <div class="box">
                    <div>
                        <?php 
                              echo Yii::t('app','Transaction # {name}',array('{name}'=>$this->id_transaction)); 
                              echo '</br>';
                              echo date('Y-m-d H:m:s');
                            ?>
                    </div>
                    <div class="box-body" style="min-height: 300px">
                        <table class="table-condensed table-responsive">
                            <?php
                                foreach($data_sale as $ds){
                                
                                echo '<tr>';
                                echo '<td><font size="1">'.$ds->idProducts->product_name.'</font></td>';
                                echo '<td><font size="1">'.$ds->quantity.' X '.numberAccountingFormat($ds->unit_selling_price).'</font></td>';
                                 
                                echo '<td><font size="1">'.numberAccountingFormat($ds->amount_selling).'</font></td>';
                                
                                echo '</tr>';
                               
                            
                                 }
                            
                            ?>
                        </table>
                    </div>
                    <div class="box-footer">
                        <strong><?php   echo Yii::t('app', 'Total amount').' : '.$total; //echo Yii::t('app','Total : {name}',array('{name}'=>$total)); ?></strong>
                        <div><?php echo Yii::t('app','Discount: ').$currency_symbol; ?> <span id="discount_string1"></span></div>
                        <div><?php echo Yii::t('app','Total after discount: '); ?> <span id="total_after_discount1"></span></div>
                        <div><?php echo Yii::t('app','Receive: ').$currency_symbol; ?> <span id="total_receive1"></span></div>
                        <div><?php echo Yii::t('app','Change: ').$currency_symbol; ?> <span id="total_change1"></span></div>
                  </div>
                </div>
            </div>
        </div>
        
        <div class="span3"></div>
        
    </div>
    
</div>
</div>
</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
       

    
    $(document).ready(function (){
        $("#Sellings_client_name").click(function(){
            var student_id = $("#Sellings_client_name").val();         
            $.get('<?= $baseUrl;?>/index.php/pointofsales/sellings/articlebylevel',{student:student_id},function(data){
                    $('#list-article').html(data);
            });
        });
            
            validate1();
            
            $('#amount_selling, #amount_receive, #balance').change(validate1);
     });

        function validate1(){
            if (
                $('#amount_selling').val().length   >   0 &&
                $('#amount_receive').val().length   >   0 &&
                $('#balance').val().length   >   0 &&
                $('#balance').val() >= 0
                ) {
                $("button[name=close_sale]").prop("disabled", false);
                $("button[name=print_sale]").prop("disabled", false);
            }
           else {
               $("button[name=close_sale]").prop("disabled", true);
               $("button[name=print_sale]").prop("disabled", true);
            }
       
    }
    
    
    
    function difference(){
       
       var amountSelling = parseFloat(document.getElementById("amount_selling").value);
       
       var amountReceive = parseFloat( document.getElementById("amount_receive").value);
       
        var discount = parseFloat(document.getElementById("discount").value);
      
      
      
       var balance; 
       balance = amountReceive - amountSelling; 
       if(balance<0){
           var message = document.getElementById("message_credit").value;
           alert(message);
       } 
       document.getElementById("balance").value = balance.toFixed(2);
       
       document.getElementById('total_receive').innerHTML = amountReceive;
       document.getElementById('total_change').innerHTML = balance.toFixed(2);
       
       document.getElementById('discount_string').innerHTML = discount+"%";
       document.getElementById('total_after_discount').innerHTML = amountSelling.toFixed(2); 
       
    }
    
    function rabais(venteInitiale){
        
       
       var x = parseFloat(venteInitiale);
        var discount = parseFloat(document.getElementById("discount").value);
        var newAmountSelling;
        newAmountSelling = x - x*(discount/100); 
        var discountAbsolute = x*(discount/100);
        
        document.getElementById("amount_selling").value = newAmountSelling.toFixed(2);
        document.getElementById('discount_string').innerHTML = discountAbsolute.toFixed(2);
        document.getElementById('total_after_discount').innerHTML = newAmountSelling.toFixed(2);
        
        if(discount!==0){
            document.getElementById("amount_receive").value = "";
            document.getElementById("balance").value = "";
        }
        
        
    }
    
    function printDiv(divName) {
     document.getElementById(divName).style.display = "block"; 
     // Affichaj
     var amountSelling = parseFloat(document.getElementById("amount_selling").value);
       
     var amountReceive = parseFloat( document.getElementById("amount_receive").value);
       
     var discount = parseFloat(document.getElementById("discount").value);
     
     var realSale = parseFloat(document.getElementById("real_sale").value);
     var absoluteDiscount = realSale*(discount/100); 
      
       var balance; 
       balance = amountReceive - amountSelling; 
        
      
       
       document.getElementById('total_receive1').innerHTML = amountReceive;
       document.getElementById('total_change1').innerHTML = balance.toFixed(2);
       
       document.getElementById('discount_string1').innerHTML = absoluteDiscount.toFixed(2);
       document.getElementById('total_after_discount1').innerHTML = amountSelling.toFixed(2); 
       //Fin afichaj
     
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;
     
     
     
     
     document.body.innerHTML = printContents;
     
     window.print();
     
     document.body.innerHTML = originalContents;
     
     document.getElementById(divName).style.display = "none";
     
     
    }    
    
    
    
     </script>