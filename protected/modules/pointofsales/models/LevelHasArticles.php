<?php

/**
 * This is the model class for table "level_has_articles".
 *
 * The followings are the available columns in table 'level_has_articles':
 * @property integer $id_level_article
 * @property integer $id_level
 * @property integer $id_article
 * @property string $level_name
 * @property string $article_name
 *
 * The followings are the available model relations:
 * @property Levels $idLevel
 * @property ArticleByLevel $idArticle
 * @property ArticleByLevel $articleName
 */
class LevelHasArticles extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LevelHasArticles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'level_has_articles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_level, id_article, level_name, article_name', 'required'),
			array('id_level, id_article', 'numerical', 'integerOnly'=>true),
			array('level_name', 'length', 'max'=>50),
			array('article_name', 'length', 'max'=>80),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_level_article, id_level, id_article, level_name, article_name', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idLevel' => array(self::BELONGS_TO, 'Levels', 'id_level'),
			'idArticle' => array(self::BELONGS_TO, 'ArticleByLevel', 'id_article'),
			'articleName' => array(self::BELONGS_TO, 'ArticleByLevel', 'article_name'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_level_article' => 'Id Level Article',
			'id_level' => 'Id Level',
			'id_article' => 'Id Article',
			'level_name' => 'Level Name',
			'article_name' => 'Article Name',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_level_article',$this->id_level_article);
		$criteria->compare('id_level',$this->id_level);
		$criteria->compare('id_article',$this->id_article);
		$criteria->compare('level_name',$this->level_name,true);
		$criteria->compare('article_name',$this->article_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}