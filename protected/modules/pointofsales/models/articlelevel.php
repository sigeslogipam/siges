<?php

/**
 * This is the model class for table "article_by_level".
 *
 * The followings are the available columns in table 'article_by_level':
 * @property integer $id_article
 * @property integer $id_product
 * @property integer $id_level
 * @property integer $id_academicyear
 * @property integer $article_quantite
 * @property integer $prix_achat
 * @property integer $prix_vente
 * @property integer $alerte
 * @property string $classe
 * @property string $salle
 * @property integer $nom_eleve
 * @property string $annee_academique
 * @property string $description_article
 * @property string $created_by
 * @property string $updated_by
 * @property string $created_date
 * @property string $updated_date
 *
 * The followings are the available model relations:
 * @property Academicperiods $idAcademicyear
 * @property Levels $idLevel
 * @property Products $idProduct
 */
class articlelevel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return articlelevel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'article_by_level';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('id_product, id_level, id_academicyear, classe, salle, annee_academique, description_article', 'required'),
			array('id_product, id_level, id_academicyear, article_quantite, prix_achat, prix_vente, alerte, nom_eleve', 'numerical', 'integerOnly'=>true),
			array('classe, salle, annee_academique, created_by, updated_by', 'length', 'max'=>255),
			array('created_date, updated_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id_article, id_product, id_level, id_academicyear, article_quantite, prix_achat, prix_vente, alerte, classe, salle, annee_academique, description_article, created_by, updated_by, created_date, updated_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idAcademicyear' => array(self::BELONGS_TO, 'Academicperiods', 'id_academicyear'),
			'idLevel' => array(self::BELONGS_TO, 'Levels', 'id_level'),
			'idProduct' => array(self::BELONGS_TO, 'Products', 'id_product'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id_article' => 'Id Article',
			'id_product' => 'Id Product',
			'id_level' => 'Id Level',
			'id_academicyear' => 'Id Academicyear',			
			'article_quantite' => 'Article Quantite',
			'prix_achat' => 'Prix Achat',
			'prix_vente' => 'Prix Vente',
			'alerte' => 'Alerte',
			'classe' => 'Classe',
			'salle' => 'Salle',			
			'annee_academique' => 'Annee Academique',
			'description_article' => 'Description Article',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
			'created_date' => 'Created Date',
			'updated_date' => 'Updated Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id_article',$this->id_article);
		$criteria->compare('id_product',$this->id_product);
		$criteria->compare('id_level',$this->id_level);
		$criteria->compare('id_academicyear',$this->id_academicyear);		
		$criteria->compare('article_quantite',$this->article_quantite);
		$criteria->compare('prix_achat',$this->prix_achat);
		$criteria->compare('prix_vente',$this->prix_vente);
		$criteria->compare('alerte',$this->alerte);
		$criteria->compare('classe',$this->classe,true);
		$criteria->compare('salle',$this->salle,true);		
		$criteria->compare('annee_academique',$this->annee_academique,true);
		$criteria->compare('description_article',$this->description_article,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_by',$this->updated_by,true);
		$criteria->compare('created_date',$this->created_date,true);
		$criteria->compare('updated_date',$this->updated_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}