<?php 
//$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));
$skin = array('belle'=>'Belle','kira'=>'Kira','kitty'=>'Kitty','siges'=>'SIGES','yucca'=>'Yucca');
$person_full_name = null; 
if(isset(Yii::app()->user->userid))
    {
        $userid = Yii::app()->user->userid;
        $person_full_name = User::model()->findByPk($userid)->full_name; 
    }
    else 
    {
        $userid = null;
    }
    
    
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
$siges_structure = infoGeneralConfig('siges_structure_session');

$display_announcement_in_menu = infoGeneralConfig('display_announcement_in_menu');
      
     $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
     
 if($current_acad==''){
        $condition = '';
          $condition1 ='';
 }
    else
    {  if($acad!=$current_acad->id)
       {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
      else
        {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
    }


	
$modelAcad=new AcademicPeriods;


if(isset(Yii::app()->user->profil))
  {  
	if(Yii::app()->user->profil!='Guest')
	  {
			$lastAcadYear=AcademicPeriods::model()->denyeAneAkademikNanSistemLan();
			if(isset($lastAcadYear)&&($lastAcadYear!=null))
			 { 
				if($acad==$lastAcadYear['id'])
				  {
				  	
					$num_month = date_diff ( date_create(date('Y-m-d'))  , date_create($lastAcadYear['date_end']))->format('%R%a');
					
					$message = Yii::t('app','Please run "End Year Decision", {name} day(s) left to move to a new academic year.',array('{name}'=>substr($num_month,1) ));
			 	
					if(($num_month < 15))
					  Yii::app()->user->setFlash(Yii::t('app','Warning'), $message);
				  
				  	}
				
			  } 
	  
	  }
  }
	
	
	
    $school_name = infoGeneralConfig('school_name');
	
	//echo $school_name;
     $school_acronym = infoGeneralConfig('school_acronym');
     
      
      $path=null;
   
     $explode_basepath= explode("protected",substr(Yii::app()->basePath, 0)); 
     echo '<input type="hidden" id="basePath" value="'.$explode_basepath[0].'" />';
            
?>
<!--
<div id="dash">
<div class="span3">
    <h2> <?php //echo Yii::t('app','Preferences'); ?>  </h2> 
</div>
    
</div>
-->
<div style="clear:both"></div>

<div class="search-form">
    <?php
    echo $this->renderPartial('//layouts/navPreferences',NULL,true);	
    ?>
</div>

<!-- <div  id="resp_form_siges"> -->
    <!-- <form  id="resp_form"> -->
        <?php
            $preferences = UsersPreferences::model()->findAllBySql("SELECT * FROM users_preferences WHERE user_id = $userid"); 
            $pref = array(); 
            $id_pref = 0;
            $i = 0; 
            foreach($preferences as $p){
                $pref[$i] = $p->skin; 
                $id_pref = $p->id;
                $i++;
            }
           // print_r($pref);
            if(!isset($pref[0])){
               ?>
    <div class="span6">
        <table class="detail-view table table-striped table-condensed">
            <thead>
                    <th colspan="4">
                        <?= Yii::t('app','User preferences for {name}',array('{name}'=>$person_full_name));?>
                    </th>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <strong>
                                <?= Yii::t('app','Choose a Skin') ?>
                        </strong>
                    </td>
                    <td>
                        <select id="skin">
                            <option value="">
                                <?= Yii::t('app','Choose a Skin'); ?>
                            </option>
                            <?php 
                                foreach($skin as $key=>$value){
                                    ?>
                            <option value="<?= $key?>"><?= $value; ?></option>
                            <?php 
                                }
                            ?>
                        </select>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <strong>
                               <?= Yii::t('app','Choose language')?> 
                        </strong>
                    </td>
                    <td>
                        <?php 
                               $this->widget('application.components.widgets.LanguageSelector');
                                        
                        ?>
                    </td>
                    
                </tr>
                <?php 
						     
                        if(isset(Yii::app()->user->profil))
						      {  
                           if((Yii::app()->user->profil!='Guest')&&(Yii::app()->user->profil!='Teacher')) 
                                {
                               ?>
                           
                <tr>
                    <td>
                        <strong>
                            <?= Yii::t('app','Choose academic year');?>
                        </strong>
                    </td>
                    <td>
                        <?php 
						     
                        if(isset(Yii::app()->user->profil))
						      {  
                           if((Yii::app()->user->profil!='Guest')&&(Yii::app()->user->profil!='Teacher')) 
                                {

                                    $path='/schoolconfig/calendar/calendarEvents';
                                    $view='/schoolconfig/calendar/viewForIndex/id/';
                                    $model=new AcademicPeriods;
                                    $model1=new AcademicPeriods;  // for session

                                    $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'academic-year-form',
                                    'enableAjaxValidation'=>true,
                                            )); 					
                                            $criteria = new CDbCriteria(array('condition'=>'is_year=1','order'=>'date_end DESC',));

                                            echo $form->dropDownList($model, 'name_period',
                                            CHtml::listData(AcademicPeriods::model()->findAll($criteria),'id','name_period'),
                                            array('options' => array(($acad)=>array('selected'=>true)),'onchange'=> 'submit()' )
                                            );

                                            if($siges_structure==1)
                                              {			
                                                $criteria = new CDbCriteria(array('condition'=>'is_year=0 AND year='.$acad,'order'=>'date_end DESC',));

                                                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$form->dropDownList($model1, '[1]name_period',
                                                CHtml::listData(AcademicPeriods::model()->findAll($criteria),'id','name_period'),
                                                array('options' => array(($acad_sess)=>array('selected'=>true)),'onchange'=> 'submit()' )
                                                );	
                                                }

                                                $this->endWidget();				
                }
                      elseif((Yii::app()->user->profil=='Teacher')) 
                           {
                                 $path='/schoolconfig/calendar/calendarEvents';
                                 $view='/schoolconfig/calendar/viewForIndex/id/';
                             }
                              else
                                {
                                    $path='/guest/calendar/calendarEvents';
                                    $view='/guest/calendar/viewForIndex/id/';

                                    }
                   }
		?>  
                    </td>
                    
                </tr>
            <?php 
                                }
                                                      }
             ?>
                <tr>
                    <td colspan="4" style="text-align: center">
                        <div class="col-submit">
                        <span class="btn btn-warning" id="save">
                            <?= Yii::t('app','Save');?>
                        </span>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>      
       <?php
            
            }
            else{
                ?>
        <div class="span6">
           
            <table class="detail-view table table-striped table-condensed">
                
                <thead>
                    <th colspan="4">
                <?= Yii::t('app','User preferences for {name}',array('{name}'=>$person_full_name));?>
                    </th>
                </thead>
                <tbody>
                <tr>
                    <td>
                        <strong>
                                <?= Yii::t('app','Choose a Skin') ?>
                        </strong>
                    </td>
                    <td>
                        <select id="skin">
                            <option value="<?= $pref[0]?>" selected="true">
                                <?= $skin[$pref[0]]; ?>
                            </option>
                            <?php 
                                unset($skin[$pref[0]]);
                                foreach($skin as $key=>$value){
                                    ?>
                            <option value="<?= $key?>"><?= $value; ?></option>
                            <?php 
                                }
                            ?>
                        </select>
                    </td>
                    
                </tr>
                <tr>
                    <td>
                        <strong>
                               <?= Yii::t('app','Choose language')?> 
                        </strong>
                    </td>
                    <td>
                        <?php 
                               $this->widget('application.components.widgets.LanguageSelector');
                                        
                        ?>
                    </td>
                </tr>
                <tr>
                    <td>
                        <strong>
                            <?= Yii::t('app','Choose academic year');?>
                        </strong>
                    </td>
                    <td>
                    <?php 
						     
                        if(isset(Yii::app()->user->profil))
						      {  
                           if((Yii::app()->user->profil!='Guest')&&(Yii::app()->user->profil!='Teacher')) 
                                {

                                    $path='/schoolconfig/calendar/calendarEvents';
                                    $view='/schoolconfig/calendar/viewForIndex/id/';
                                    $model=new AcademicPeriods;
                                    $model1=new AcademicPeriods;  // for session

                                    $form=$this->beginWidget('CActiveForm', array(
                                    'id'=>'academic-year-form',
                                    'enableAjaxValidation'=>true,
                                            )); 					
                                            $criteria = new CDbCriteria(array('condition'=>'is_year=1','order'=>'date_end DESC',));

                                            echo $form->dropDownList($model, 'name_period',
                                            CHtml::listData(AcademicPeriods::model()->findAll($criteria),'id','name_period'),
                                            array('options' => array(($acad)=>array('selected'=>true)),'onchange'=> 'submit()' )
                                            );

                                            if($siges_structure==1)
                                              {			
                                                $criteria = new CDbCriteria(array('condition'=>'is_year=0 AND year='.$acad,'order'=>'date_end DESC',));

                                                echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$form->dropDownList($model1, '[1]name_period',
                                                CHtml::listData(AcademicPeriods::model()->findAll($criteria),'id','name_period'),
                                                array('options' => array(($acad_sess)=>array('selected'=>true)),'onchange'=> 'submit()' )
                                                );	
                                                }

                                                $this->endWidget();				
                }
                      elseif((Yii::app()->user->profil=='Teacher')) 
                           {
                                 $path='/schoolconfig/calendar/calendarEvents';
                                 $view='/schoolconfig/calendar/viewForIndex/id/';
                             }
                              else
                                {
                                    $path='/guest/calendar/calendarEvents';
                                    $view='/guest/calendar/viewForIndex/id/';

                                    }
                   }
		?>  
						 
			

          <!--Sidebar content-->
       <!-- </div> -->
                    </td>
                </tr>
</tbody>
            </table>
            
        </div>
        
        <div class="col-4">
                    <label id="resp_form">
        
                    </label>
        </div>
        <!--
        <div  id="language-selector" style="margin-left: 0px;">
           
                                    <?php 
                                   
                                        $this->widget('application.components.widgets.LanguageSelector');
                                        
                                    ?>
            
	</div> 
        -->
        <!--
        <br/>
        <div class="col-submit">
            <span class="btn btn-warning" id="update">
                <?= Yii::t('app','Update');?>
            </span>
        </div>
        -->
        <?php 
                 
            } 
               
        ?>
        
        
<!--    </form> -->
<!--</div> -->




<div id="message_pref">
    
</div>

<script>
    $('#skin').change(function(){
        var skin = $('#skin').val();
        var userid = <?= $userid?>;
        var id = <?= $id_pref; ?>;
        $.get('<?= Yii::app()->baseUrl ?>/index.php/users/user/updatePreferences',{id:id,userid: userid, skin:skin},function(data){
            $('#message_pref').html(data); 
            location.reload();
        });
    });
    
    $('#save').click(function(){
        var userid = <?= $userid?>;
        var skin = $('#skin').val();
        if(skin==""){
                alert("<?= Yii::t('app','No skin selected')?>");
        }else{
        $.get('<?= Yii::app()->baseUrl ?>/index.php/users/user/savePreferences',{userid: userid, skin:skin},function(data){
            $('#message_pref').html(data); 
            location.reload();
        });
    }
    }); 
    
    $('#update').click(function(){
        var userid = <?= $userid?>;
        var skin = $('#skin').val();
        var id = <?= $id_pref; ?>;
        if(skin==""){
                alert("<?= Yii::t('app','No skin selected')?>");
        }else{
        $.get('<?= Yii::app()->baseUrl ?>/index.php/users/user/updatePreferences',{id:id,userid: userid, skin:skin},function(data){
            $('#message_pref').html(data); 
            location.reload();
        });
    }
    }); 
</script>