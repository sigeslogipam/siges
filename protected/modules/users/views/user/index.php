<?php

/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */



/* @var $this UserController */
/* @var $model User */


$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));
$acad_sess=acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];

 $template='';

 if(!isAchiveMode($acad_sess))
     $template='{update}';


$tit =Yii::t('app','Manage Users');
?>


<!-- Menu of CRUD  -->
<div id="dash">
		<div class="span3"><h2> <?php echo $tit; ?>  </h2> </div>
    <div class="span3">

        <div class="span4">
                  <?php
                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                               // build the link in Yii standard
                     echo CHtml::link($images,array('../site/index'));

                   ?>
              </div>


        </div>
</div>

<div style="clear:both"></div>

<div class="search-form">
    <?php
    echo $this->renderPartial('//layouts/navBaseUser',NULL,true);
    ?>
</div>



<?php

?>

<div class="clear"></div>
<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	//'enableAjaxValidation'=>false,
));

?>
       <div class="checkbox_view span2" >
           <label>

                                <div class="tichek">  
	                            <?php

                                     echo $form->labelEx($model,'sortOption' );

		                              if($this->sortOption==1)
				                          { echo $form->checkBox($model,'sortOption',array('onchange'=> 'submit()','checked'=>'checked'));

				                           }
						                 else
							               echo $form->checkBox($model,'sortOption',array('onchange'=> 'submit()'));


                               ?>

                                </div>
             </label>
       </div>
<?php
    if($this->sortOption==1)
      {
?>
                 <div class="span2" >
                              <div class="left" style="padding-left:20px;">
                                    <?php

                                        echo $form->labelEx($model,Yii::t('app','Person'));

                                        if(isset($this->person_)&&($this->person_!=''))
							       echo $form->dropDownList($model,'person_',$this->loadPersons(), array('onchange'=> 'submit()','options' => array($this->person_=>array('selected'=>true))));
							    else
								  {
									echo $form->dropDownList($model,'person_',$this->loadPersons(), array('onchange'=> 'submit()'));
								  }

                                    ?>
                                </div>

                            </div>

                   <div class="span2" >
                              <div class="left" style="padding-left:20px;">
                                    <?php

                                        echo $form->labelEx($model,Yii::t('app','Room'));

                                        if(isset($this->room)&&($this->room!=''))
							       echo $form->dropDownList($model,'room',$this->loadRoomByPerson($this->person_), array('onchange'=> 'submit()','options' => array($this->room=>array('selected'=>true))));
							    else
								  {
									echo $form->dropDownList($model,'room',$this->loadRoomByPerson($this->person_), array('onchange'=> 'submit()'));
								  }

                                    ?>
                                </div>

                            </div>


<?php
      	}
?>
<br/>
<div class="clear"></div>


<?php $this->endWidget(); ?>


<div class="grid-view">

<div  class="search-form">
<?php


//error message

    		//error message
        if(isset($_GET['msguv'])&&($_GET['msguv']=='y'))
           $this->message_default_user=true;




			if(($this->message_default_user))
			      { echo '<div class="" style=" padding-left:0px;margin-right:250px; margin-bottom:-48px; ';//-20px; ';
				      echo '">';

				      	echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';

				     	echo '<span style="color:red;" >'.Yii::t('app','- '.bagdor()[1].' - cannot be either updated nor deleted.').'</span>';
					     $this->message_default_user=false;
					     echo'</td>
						    </tr>
							</table>';

				           echo '</div>
				           <div style="clear:both;"></div>';
			           }


?>
<?php

$col = array(
            
                array(
                    'name' => 'username',
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->username,Yii::app()->createUrl("users/user/view",array("id"=>$data->id)))',
                    'htmlOptions'=>array('width'=>'150px'),
                     ),



				array(
                    'name'=>'full_name',
                    'header'=>Yii::t('app','Full name'),
                    'value'=>'$data->full_name',
                    ),



                    array('name'=>'profil',
			'header'=>Yii::t('app','Profil User'),
			'value'=>'$data->profil0->profil_name'),

                        
                    array('name'=>'group_id',
			'header'=>Yii::t('app','Group user'),
			'value'=>'$data->group0->group_name'),
                        

		array(
			'class'=>'CButtonColumn',
			'template'=>$template,
			   'buttons'=>array (

         'update'=>array(
            'label'=>'<span class="fa fa-pencil-square-o"></span>',
            'imageUrl'=>false,
            'url'=>'Yii::app()->createUrl("users/user/update?id=$data->id")',
            'options'=>array( 'title'=>Yii::t('app','Update' )),
        ),

    ),


		),
	);

if( ($this->sortOption==1) )
 $col = array(
                    
                array(
                    'name' => 'username',
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->username,Yii::app()->createUrl("users/user/view",array("id"=>$data->id)))',
                    'htmlOptions'=>array('width'=>'150px'),
                     ),
                     

                     
				array(
                    'name'=>'full_name',
                    'header'=>Yii::t('app','Full name'),
                    'value'=>'$data->full_name',
                    ),
                    
                array(
                    'name'=>'person.fullName',
                    'header'=>Yii::t('app','Student name'),
                    'value'=>'$data->person->fullName',
                    ),
                    
                    array('name'=>'profil',
			'header'=>Yii::t('app','Profil User'),
			'value'=>'$data->profil0->profil_name'),

                        
                    array('name'=>'group_id',
			'header'=>Yii::t('app','Group user'),
			'value'=>'$data->group0->group_name'),
                        

		array(
			'class'=>'CButtonColumn',
			'template'=>$template,
			   'buttons'=>array (

         'update'=>array(
            'label'=>'<span class="fa fa-pencil-square-o"></span>',
            'imageUrl'=>false,
            'url'=>'Yii::app()->createUrl("users/user/update?id=$data->id")',
            'options'=>array( 'title'=>Yii::t('app','Update' )),
        ),

    ),


		),
	);

$gridWidget = $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'user-grid',
	'summaryText'=>'',
	'dataProvider'=>$dataProvider,

	'columns'=>$col,
));
 // $this->renderExportGridButton($gridWidget, '<span class="fa fa-arrow-right">'.Yii::t('app',' CSV').'</span>',array('class'=>'btn-info btn'));

?>

</div>

<script>
    $(document).ready(function(){

            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",

                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit ?>"},

                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });

            //var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>

</div>
