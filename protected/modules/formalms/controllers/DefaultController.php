<?php

class DefaultController extends Controller
{
    
    public $layout='//layouts/column2';
    public $title; 
    
        /**
         * 
         */
	public function actionIndex()
	{
           $this->render('index');
	}
        /**
         * 
         */
        public function actionMigrateRooms(){
            $this->render('migrate_rooms'); 
        }
        
        public function  actionMigrateCourses(){
            $this->render('migrate_courses');
        }
        
        public function  actionMigrateUsers(){
            $this->render('migrate_users');
        }
        
        public function actionShowStudent($level){
          if(isset($level)){  
              return  $this->renderPartial('ajax_student',array(
                  'level'=>$level
              )); 
          }else{
              echo "Error: 404";
          }
            
        }
        
        public function actionReziltaNote($id_course){
            if(isset($id_course)){
                $course_name = LearningCourse::model()->findByPk($id_course)->name;
                $course_code = LearningCourse::model()->findByPk($id_course)->code;
                return $this->renderPartial('rezilta_note_elearning',array(
                    'id_course'=>$id_course,
                    'course_name'=>$course_name,
                    'course_code'=>$course_code,
                ));
            }else{
              echo "Error: 404";
          }
            
        }
        
        public function actionShowLisEleve($id_test,$id_course){
            if(isset($id_test)){
                $course_code = LearningCourse::model()->findByPk($id_course)->code;
                return $this->renderPartial('lis_elev',array(
                    'id_test'=>$id_test,
                    'course_code'=>$course_code
                ));
            }else{
              echo "Error: 404";
          }
        }
        
         public function actionShowListCourse($id_room){
            if(isset($id_room)){
                
                return $this->renderPartial('liste_cours',array(
                    'id_room'=>$id_room
                ));
            }else{
              echo "Error: 404";
          }
        }
        
        public function actionPortailGrades(){
            if(isset($_POST['save_grades'])){
              $course_id = $_POST["course_id"];
              $period_exam = $_POST["eval_period"];
              $total_konte = $_POST["total_konte"];
              $coefficient = Courses::model()->findByPk($course_id)->weight; 
              for($i=1;$i<$total_konte; $i++){
                $student_id = $_POST['student_'.$i];
                $grade_value = $_POST["grade_".$i];
                 $is_already_have_grade = isStudentGradesAlreadySave($student_id, $course_id, $period_exam);
                 if($is_already_have_grade==0){
                     // on inserre les notes 
                     $grades_model = new Grades(); 
                     $grades_model->student = $student_id; 
                     $grades_model->course = $course_id; 
                     $grades_model->evaluation = $period_exam;
                     if($grade_value <= $coefficient){
                        $grades_model->grade_value = $grade_value; 
                     }else{
                         $grades_model->grade_value = NULL; 
                     }
                     $grades_model->validate = 0; 
                     $grades_model->publish = 0; 
                     $grades_model->date_created = date("Y-m-d H:i:s");
                     $grades_model->create_by = currentUser()."/Elearning"; 
                     $grades_model->save(); 
                 }else{
                     // On update la note de l'eleve
                     $grades_model = Grades::model()->findByPk($is_already_have_grade); 
                      if($grade_value <= $coefficient){
                        $grades_model->grade_value = $grade_value; 
                     }else{
                         $grades_model->grade_value = NULL; 
                     }
                     $grades_model->validate = 0; 
                     $grades_model->publish = 0; 
                     $grades_model->date_updated = date("Y-m-d H:i:s");
                     $grades_model->update_by = currentUser()."/Elearning"; 
                     $grades_model->save(); 
                 }
                 
              }
              $this->redirect(array("portailGrades","course"=>$course_id));
               
            }
            $this->render("portail_grades");
            
       }
        
        public function actionShowComboCourse($room_id){
            if(isset($room_id)){
                return $this->renderPartial("ajax_list_cours",array(
                    'room_id'=>$room_id
                ));
            }else{
                echo "Error: 404";
            }
        }
        
        /**
         * 
         * @param list of value separed by comma  $room
         * Migrate classroom from siges to formalms 
         * Return confirmation of rooms migrate
         * 
         */
        public function actionMakeRoomMigration($room){
            // Verify if location exist on formlms 
            /*
            $location = LearningClassLocation::model()->findAllBySql("SELECT * FROM learning_class_location LIMIT 1");
            // print_r($location);
             $location_id = null;
             foreach($location as $l){
                 if(isset($l->location_id)){
                    $location_id = $l->location_id;
                 }else{
                     $location_id = null;
                 }
             }
             * 
             */
            $array_room = explode(",", $room);
            $array_room_name = array(); 
            // print_r($array_room);
           
               // $location_id = $location->location_id;
               // echo $location_id; 
                $k = 0;
                foreach($array_room as $ar){
                   // print_r($ar);
                    $room_model = Rooms::model()->findByPk($ar); 
                    $core_group_model = new CoreGroup(); 
                    $core_group_model->idst = 100000+$ar; 
                    $core_group_model->groupid = "/".$room_model->short_room_name;
                    $core_group_model->description =  $room_model->room_name;
                    $core_group_model->hidden = "false"; 
                    $core_group_model->type = "free";
                    $core_group_model->show_on_platform = "framework,lms,";
                    $core_group_model->save();
                    $array_room_name[$k] = $room_model->room_name;
                    $k++; 
                    
                   
                }
                $string_confirmation = ""; 
                
                for($i=0;$i< count($array_room_name);$i++){
                    $string_confirmation .= $array_room_name[$i].'<br/>';  
                }
                
                echo Yii::t('app','The following rooms have been successfully migrate: <br/> {string_confirmation}',array('{string_confirmation}'=>$string_confirmation)); 
                  
           // echo $room; 
        }
        
        public function actionMakeCourseMigration($course){
            $array_course = explode(",", $course);
            $array_course_name = array(); 
            $acad = Yii::app()->session['currentId_academic_year'];
            
            $profile_role_id1 = infoRoles('/lms/course/public/profile/view');
            $profile_role_id2 = infoRoles('/lms/course/public/tprofile/view'); 
            $is_student_room_migrate = 0;
            
            $k=0;
            foreach($array_course as $ac){
                 
                 
                $course_model = Courses::model()->findByPk($ac); 
                $learning_course_model = new LearningCourse();
                
                $learning_course_model->code = $course_model->subject0->short_subject_name.'-'.$ac;
                $learning_course_model->name = $course_model->subject0->subject_name.' '.$course_model->room0->room_name;
                $learning_course_model->box_description = $course_model->subject0->subject_name.' '.$course_model->room0->room_name.' '.$course_model->teacher0->fullName;
                $learning_course_model->lang_code = setLanguageCode(); 
                $learning_course_model->status = 2;
                $learning_course_model->level_show_user = 0;
               $learning_course_model->linkSponsor = '';
               $learning_course_model->imgSponsor = '';
               $learning_course_model->img_course = '';
               $learning_course_model->img_material = '';
               $learning_course_model->img_othermaterial = '';
               $learning_course_model->course_demo = '';
               
               $learning_course_model->save();
               
               // Enregistrer dans la table learning_label_course
               $learning_label_course_model = new LearningLabelCourse(); 
               $learning_label_course_model->id_common_label = 0; 
               $learning_label_course_model->id_course = $learning_course_model->idCourse;
               $learning_label_course_model->save(); 
               
               // Enregistrer dans la table learning_menucourse_main
               $learning_menucourse_main = new LearningMenucourseMain(); 
               $learning_menucourse_main->idCourse = $learning_course_model->idCourse;
               $learning_menucourse_main->sequence = 1;
               $learning_menucourse_main->name = "Student Area";
               $learning_menucourse_main->image = "";
               $learning_menucourse_main->save();
               /*
               $learning_menucourse_main = new LearningMenucourseMain(); 
               $learning_menucourse_main->idCourse = $learning_course_model->idCourse;
               $learning_menucourse_main->sequence = 2;
               $learning_menucourse_main->name = "Stat Area";
               $learning_menucourse_main->image = "";
               $learning_menucourse_main->save();
                * 
                */
               $learning_menucourse_main = new LearningMenucourseMain(); 
               $learning_menucourse_main->idCourse = $learning_course_model->idCourse;
               $learning_menucourse_main->sequence = 3;
               $learning_menucourse_main->name = "Teacher area";
               $learning_menucourse_main->image = "";
               $learning_menucourse_main->save();
               /*
               $learning_menucourse_main = new LearningMenucourseMain(); 
               $learning_menucourse_main->idCourse = $learning_course_model->idCourse;
               $learning_menucourse_main->sequence = 4;
               $learning_menucourse_main->name = "Collaborative Area";
               $learning_menucourse_main->image = "";
               $learning_menucourse_main->save();
                * 
                */
               
               // rechercher pour student area 
               $model_menucourse_main_stud = LearningMenucourseMain::model()->findAllBySql("SELECT * FROM `learning_menucourse_main` WHERE name = 'Student Area' AND idCourse = $learning_course_model->idCourse"); 
               foreach($model_menucourse_main_stud as $m){
                   $id_menucourse_main_stud= $m->idMain; 
               }
               // IN ('_HTMLFRONT','_ORGANIZATION','_ADVICE','_CALENDAR','_GRADEBOOK')
               // WHERE default_name IN ('_HTMLFRONT','_ORGANIZATION','_ADVICE','_CALENDAR','_GRADEBOOK')
               $sql_module_stud = "SELECT * FROM `learning_module` WHERE default_name IN ('_HTMLFRONT','_ORGANIZATION','_ADVICE','_CALENDAR','_GRADEBOOK')";
               $model_learning_module_stud = LearningModule::model()->findAllBySql($sql_module_stud); 
               $sequence_menu_stud = 1;
               foreach($model_learning_module_stud as $mlm){
                   $learning_menucourse_under = new LearningMenucourseUnder(); 
                   $learning_menucourse_under->idCourse = $learning_course_model->idCourse;
                   $learning_menucourse_under->idMain = $id_menucourse_main_stud;
                   $learning_menucourse_under->idModule = $mlm->idModule; 
                   $learning_menucourse_under->sequence = $sequence_menu_stud; 
                   $learning_menucourse_under->save();
                   $sequence_menu_stud++;
               }
               
               
               // Rechercher pour le teacher area
               $model_menucourse_main_teach = LearningMenucourseMain::model()->findAllBySql("SELECT * FROM `learning_menucourse_main` WHERE name = 'Teacher area' AND idCourse = $learning_course_model->idCourse"); 
               foreach($model_menucourse_main_teach as $m){
                   $id_menucourse_main_teach = $m->idMain; 
               }
               // IN ('_STORAGE','_GROUPS','_COURSEREPORT','_NEWSLETTER','_MAN_MENU')
               //  WHERE default_name  IN ('_STORAGE','_GROUPS','_COURSEREPORT','_NEWSLETTER','_MAN_MENU')
               $sql_module_teach = "SELECT * FROM `learning_module`  WHERE default_name  IN ('_STORAGE','_GROUPS','_COURSEREPORT','_NEWSLETTER','_MAN_MENU')";
               $model_learning_module_teach = LearningModule::model()->findAllBySql($sql_module_teach); 
               $sequence_menu_teach = 1;
               foreach($model_learning_module_teach as $mlm){
                   $learning_menucourse_under = new LearningMenucourseUnder(); 
                   $learning_menucourse_under->idCourse = $learning_course_model->idCourse;
                   $learning_menucourse_under->idMain = $id_menucourse_main_teach;
                   $learning_menucourse_under->idModule = $mlm->idModule; 
                   $learning_menucourse_under->sequence = $sequence_menu_teach; 
                   $learning_menucourse_under->save();
                   $sequence_menu_teach++;
               }
               
               // Migrer les etudiants 
              /* 
               if(isset(Courses::model()->findByPk($ac)->room)){
                  
                  
                   $room_id = Courses::model()->findByPk($ac)->room; 
                    
                   $konte_migration = 0;
                   $migrate_formalms = MigrationFormalms::model()->findAllByAttributes(array('data_type'=>'room','data'=>$room_id));
                   
                  
                   foreach($migrate_formalms as $mf){
                       $konte_migration++;
                   }
                   
                   
                   
                   // Si la migration pour cette salle n'a pas ete efectue on fait la migration pour les eleves de la salle
                   if($konte_migration==0){
                       
                       
                       $sql_student_in_room = "SELECT p.id, p.last_name, p.first_name FROM  persons p INNER JOIN room_has_person rhp ON (rhp.students = p.id) where rhp.room = $room_id and rhp.academic_year = $acad and p.is_student IN (1,2)"; 
                     
                       
                       $all_student_in_room = Persons::model()->findAllBySql($sql_student_in_room);
                       //print_r($all_student_in_room);  
                       
                       foreach($all_student_in_room as $asir){
                        
                        
                           $user = User::model()->findAllBySql("SELECT * FROM users WHERE person_id = $asir->id AND is_parent = 0"); 
                           
                           foreach($user as $u){
                               $username = $u->username; 
                               $password = $u->password; 
                               $user_formalms = new CoreUser(); 
                               $user_formalms->idst = $u->id; 
                               $user_formalms->userid = '/'.$username;
                               $user_formalms->pass = $password;
                               $user_formalms->firstname = $asir->first_name; 
                               $user_formalms->lastname = $asir->last_name; 
                               $user_formalms->email = " "; 
                               $user_formalms->avatar = " ";
                               $user_formalms->level = 1;
                               if($user_formalms->save()){
                                 
                                 $core_role_members_model1 = new CoreRoleMembers(); 
                                 $core_role_members_model1->idst = $profile_role_id1; 
                                 $core_role_members_model1->idstMember = $u->id; 
                                 $core_role_members_model1->save();
                                 $core_role_members_model2 = new CoreRoleMembers(); 
                                 $core_role_members_model2->idst = $profile_role_id2; 
                                 $core_role_members_model2->idstMember = $u->id; 
                                 $core_role_members_model2->save();
                                 
                                 // put user and course together 
                                 
                                 $course_user_model = new LearningCourseuser(); 
                                 $course_user_model->idUser = $u->id;
                                 $course_user_model->idCourse = $learning_course_model->idCourse;
                                 $course_user_model->edition_id = 0; 
                                 $course_user_model->level = 3; // Pour un etudiant 
                                 $course_user_model->status = 1; 
                                 $course_user_model->imported_from_connection = "SIGES";
                                 $course_user_model->save();
                                  
                               }
                               
                           }
                           // Alimente les tables de profil 
                           
                           // alimente les relation entre le cours et l'utilisateur 
                         
                           
                       }
                       
                   }
               
                   
               }
               */
              /*
                $migrate_formalms_model = new  MigrationFormalms(); 
                $migrate_formalms_model->data_type = "room";
                $migrate_formalms_model->data = $room_id; 
                $migrate_formalms_model->is_migrate = 1;
                $migrate_formalms_model->date_migration = date('Y-m-d H:i:s'); 
                $migrate_formalms_model->migrate_by = "SIGES";
                 */
               $array_course_name[$k] = $course_model->subject0->subject_name;
               $k++;   
                
            }
            
            $string_confirmation = ""; 

            for($i=0;$i< count($array_course_name);$i++){
                $string_confirmation .= $array_course_name[$i].'<br/>';  
            }

            echo Yii::t('app','The following courses have been successfully migrate: <br/> {string_confirmation}',array('{string_confirmation}'=>$string_confirmation)); 
            
            
        }
        
        
        public function actionMakeStudentMigration($student){
            $array_student = explode(",", $student);
          
            
            $array_student_name = array(); 
              
           
            $acad = Yii::app()->session['currentId_academic_year'];
            
            $profile_role_id1 = infoRoles('/lms/course/public/profile/view');
            $profile_role_id2 = infoRoles('/lms/course/public/tprofile/view'); 
            $profile_role_id3 = infoRoles("/lms/course/public/profile/mod");
            
             
            
            $normal_group_user_id = infoGroup('/framework/level/user');
           // echo "sa w genyen";
            
            $is_student_migrate = 0;
            
            
            $k=0;
            foreach($array_student as $as){
                $last_name = Persons::model()->findByPk($as)->last_name; 
                $first_name = Persons::model()->findByPk($as)->first_name; 
                $user = User::model()->findAllBySql("SELECT * FROM users WHERE person_id = $as AND is_parent = 0");
                foreach($user as $u){
                    
                       $username = $u->username; 
                       $password = $u->password; 
                       $user_formalms = new CoreUser(); 
                       $user_formalms->idst = $as; 
                       $user_formalms->userid = '/'.$username;
                       $user_formalms->pass = $password;
                       $user_formalms->firstname = $first_name; 
                       $user_formalms->lastname = $last_name; 
                       $user_formalms->email = " "; 
                       $user_formalms->avatar = " ";
                       $user_formalms->level = 1;
                       $user_formalms->save();
                       
                       if($user_formalms->save()){
                                 // Attributes a role to the user 
                           
                                 $core_role_members_model1 = new CoreRoleMembers(); 
                                 $core_role_members_model1->idst = $profile_role_id1; 
                                 $core_role_members_model1->idstMember = $as; 
                                 $core_role_members_model1->save();
                                 $core_role_members_model2 = new CoreRoleMembers(); 
                                 $core_role_members_model2->idst = $profile_role_id2; 
                                 $core_role_members_model2->idstMember = $as; 
                                 $core_role_members_model2->save();
                                 $core_role_members_model3 = new CoreRoleMembers(); 
                                 $core_role_members_model3->idst = $profile_role_id3; 
                                 $core_role_members_model3->idstMember = $as; 
                                 $core_role_members_model3->save();
                                 
                                 // Attributes users to groups 
                                 $core_group_members_model = new CoreGroupMembers(); 
                                 $core_group_members_model->idstMember = $as; 
                                 $core_group_members_model->idst = $normal_group_user_id; 
                                 $core_group_members_model->save(); 
                                 
                                 //
                               //  $sql_stud = "SELECT room FROM `room_has_person` where students = 1005 and academic_year = 6";
                                 $room= getRoomByStudentId($as, $acad); 
                                 $room_name = Rooms::model()->findByPk($room->id)->short_room_name;
                                 $id_group_ = infoGroup("/".$room_name);
                                 $core_group_members_model_s = new CoreGroupMembers(); 
                                 $core_group_members_model_s->idstMember = $as; 
                                 $core_group_members_model_s->idst = $id_group_; 
                                 $core_group_members_model_s->save(); 
                                 
                       }
                        
                      //  $user_formalms->idst = $as; 
                      //  $user_formalms->save();
                    
                }
                 $array_student_name[$k] = $first_name.' '.$last_name;
                 $k++;   
            }
            
            $string_confirmation = ""; 

            for($i=0;$i< count($array_student_name);$i++){
                $string_confirmation .= $array_student_name[$i].'<br/>';  
            }

            echo Yii::t('app','The following students have been successfully migrate: <br/> {string_confirmation}',array('{string_confirmation}'=>$string_confirmation)); 
            
            
        }
        
        
        public function actionUpdateCourse(){
             $this->render("update_course");
        }
        
        public function actionMakeCourseUpdate($course){
             $array_course = explode(",", $course);
              foreach($array_course as $ac){
                  $one_course = Courses::model()->findByPk($ac); 
                  $code_course = $one_course->subject0->short_subject_name.'-'.$ac;
                  $one_course->code_cours = $code_course; 
                  $one_course->save(); 
              }
        }
        
        
        
        
}