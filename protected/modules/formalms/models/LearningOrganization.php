<?php

/**
 * This is the model class for table "learning_organization".
 *
 * The followings are the available columns in table 'learning_organization':
 * @property integer $idOrg
 * @property integer $idParent
 * @property string $path
 * @property integer $lev
 * @property string $title
 * @property string $objectType
 * @property integer $idResource
 * @property integer $idCategory
 * @property integer $idUser
 * @property integer $idAuthor
 * @property string $version
 * @property string $difficult
 * @property string $description
 * @property string $language
 * @property string $resource
 * @property string $objective
 * @property string $dateInsert
 * @property integer $idCourse
 * @property string $prerequisites
 * @property integer $isTerminator
 * @property integer $idParam
 * @property integer $visible
 * @property string $milestone
 * @property string $width
 * @property string $height
 * @property string $publish_from
 * @property string $publish_to
 * @property string $access
 * @property integer $publish_for
 */
class LearningOrganization extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningOrganization the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_organization';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description, objective', 'required'),
			array('idParent, lev, idResource, idCategory, idUser, idAuthor, idCourse, isTerminator, idParam, visible, publish_for', 'numerical', 'integerOnly'=>true),
			array('path, title, resource, prerequisites, access', 'length', 'max'=>255),
			array('objectType', 'length', 'max'=>20),
			array('version', 'length', 'max'=>8),
			array('difficult', 'length', 'max'=>14),
			array('language', 'length', 'max'=>50),
			array('milestone', 'length', 'max'=>5),
			array('width, height', 'length', 'max'=>4),
			array('dateInsert, publish_from, publish_to', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idOrg, idParent, path, lev, title, objectType, idResource, idCategory, idUser, idAuthor, version, difficult, description, language, resource, objective, dateInsert, idCourse, prerequisites, isTerminator, idParam, visible, milestone, width, height, publish_from, publish_to, access, publish_for', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idOrg' => 'Id Org',
			'idParent' => 'Id Parent',
			'path' => 'Path',
			'lev' => 'Lev',
			'title' => 'Title',
			'objectType' => 'Object Type',
			'idResource' => 'Id Resource',
			'idCategory' => 'Id Category',
			'idUser' => 'Id User',
			'idAuthor' => 'Id Author',
			'version' => 'Version',
			'difficult' => 'Difficult',
			'description' => 'Description',
			'language' => 'Language',
			'resource' => 'Resource',
			'objective' => 'Objective',
			'dateInsert' => 'Date Insert',
			'idCourse' => 'Id Course',
			'prerequisites' => 'Prerequisites',
			'isTerminator' => 'Is Terminator',
			'idParam' => 'Id Param',
			'visible' => 'Visible',
			'milestone' => 'Milestone',
			'width' => 'Width',
			'height' => 'Height',
			'publish_from' => 'Publish From',
			'publish_to' => 'Publish To',
			'access' => 'Access',
			'publish_for' => 'Publish For',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idOrg',$this->idOrg);
		$criteria->compare('idParent',$this->idParent);
		$criteria->compare('path',$this->path,true);
		$criteria->compare('lev',$this->lev);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('objectType',$this->objectType,true);
		$criteria->compare('idResource',$this->idResource);
		$criteria->compare('idCategory',$this->idCategory);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idAuthor',$this->idAuthor);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('difficult',$this->difficult,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('resource',$this->resource,true);
		$criteria->compare('objective',$this->objective,true);
		$criteria->compare('dateInsert',$this->dateInsert,true);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('prerequisites',$this->prerequisites,true);
		$criteria->compare('isTerminator',$this->isTerminator);
		$criteria->compare('idParam',$this->idParam);
		$criteria->compare('visible',$this->visible);
		$criteria->compare('milestone',$this->milestone,true);
		$criteria->compare('width',$this->width,true);
		$criteria->compare('height',$this->height,true);
		$criteria->compare('publish_from',$this->publish_from,true);
		$criteria->compare('publish_to',$this->publish_to,true);
		$criteria->compare('access',$this->access,true);
		$criteria->compare('publish_for',$this->publish_for);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}