<?php

/**
 * This is the model class for table "learning_courseuser".
 *
 * The followings are the available columns in table 'learning_courseuser':
 * @property integer $idUser
 * @property integer $idCourse
 * @property integer $edition_id
 * @property integer $level
 * @property string $date_inscr
 * @property string $date_first_access
 * @property string $date_complete
 * @property integer $status
 * @property integer $waiting
 * @property integer $subscribed_by
 * @property integer $rule_log
 * @property integer $score_given
 * @property string $imported_from_connection
 * @property integer $absent
 * @property integer $cancelled_by
 * @property integer $new_forum_post
 * @property string $date_begin_validity
 * @property string $date_expire_validity
 * @property integer $requesting_unsubscribe
 * @property string $requesting_unsubscribe_date
 */
class LearningCourseuser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCourseuser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_courseuser';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('idUser, idCourse, edition_id, level, status, waiting, subscribed_by, rule_log, score_given, absent, cancelled_by, new_forum_post, requesting_unsubscribe', 'numerical', 'integerOnly'=>true),
			array('imported_from_connection', 'length', 'max'=>255),
			// array('date_inscr, date_first_access, date_complete, date_begin_validity, date_expire_validity, requesting_unsubscribe_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idUser, idCourse, edition_id, level, date_inscr, date_first_access, date_complete, status, waiting, subscribed_by, rule_log, score_given, imported_from_connection, absent, cancelled_by, new_forum_post, date_begin_validity, date_expire_validity, requesting_unsubscribe, requesting_unsubscribe_date', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idUser' => 'Id User',
			'idCourse' => 'Id Course',
			'edition_id' => 'Edition',
			'level' => 'Level',
			'date_inscr' => 'Date Inscr',
			'date_first_access' => 'Date First Access',
			'date_complete' => 'Date Complete',
			'status' => 'Status',
			'waiting' => 'Waiting',
			'subscribed_by' => 'Subscribed By',
			'rule_log' => 'Rule Log',
			'score_given' => 'Score Given',
			'imported_from_connection' => 'Imported From Connection',
			'absent' => 'Absent',
			'cancelled_by' => 'Cancelled By',
			'new_forum_post' => 'New Forum Post',
			'date_begin_validity' => 'Date Begin Validity',
			'date_expire_validity' => 'Date Expire Validity',
			'requesting_unsubscribe' => 'Requesting Unsubscribe',
			'requesting_unsubscribe_date' => 'Requesting Unsubscribe Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('edition_id',$this->edition_id);
		$criteria->compare('level',$this->level);
		$criteria->compare('date_inscr',$this->date_inscr,true);
		$criteria->compare('date_first_access',$this->date_first_access,true);
		$criteria->compare('date_complete',$this->date_complete,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('waiting',$this->waiting);
		$criteria->compare('subscribed_by',$this->subscribed_by);
		$criteria->compare('rule_log',$this->rule_log);
		$criteria->compare('score_given',$this->score_given);
		$criteria->compare('imported_from_connection',$this->imported_from_connection,true);
		$criteria->compare('absent',$this->absent);
		$criteria->compare('cancelled_by',$this->cancelled_by);
		$criteria->compare('new_forum_post',$this->new_forum_post);
		$criteria->compare('date_begin_validity',$this->date_begin_validity,true);
		$criteria->compare('date_expire_validity',$this->date_expire_validity,true);
		$criteria->compare('requesting_unsubscribe',$this->requesting_unsubscribe);
		$criteria->compare('requesting_unsubscribe_date',$this->requesting_unsubscribe_date,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}