<?php

/**
 * This is the model class for table "core_group".
 *
 * The followings are the available columns in table 'core_group':
 * @property integer $idst
 * @property string $groupid
 * @property string $description
 * @property string $hidden
 * @property string $type
 * @property string $show_on_platform
 */
class CoreGroup extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreGroup the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_group';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description, show_on_platform', 'required'),
			array('idst', 'numerical', 'integerOnly'=>true),
			array('groupid', 'length', 'max'=>255),
			array('hidden', 'length', 'max'=>5),
			array('type', 'length', 'max'=>9),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idst, groupid, description, hidden, type, show_on_platform', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst' => 'Idst',
			'groupid' => 'Groupid',
			'description' => 'Description',
			'hidden' => 'Hidden',
			'type' => 'Type',
			'show_on_platform' => 'Show On Platform',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idst',$this->idst);
		$criteria->compare('groupid',$this->groupid,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('hidden',$this->hidden,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('show_on_platform',$this->show_on_platform,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}