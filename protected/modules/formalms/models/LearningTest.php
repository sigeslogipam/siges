<?php

/**
 * This is the model class for table "learning_test".
 *
 * The followings are the available columns in table 'learning_test':
 * @property integer $idTest
 * @property integer $author
 * @property string $title
 * @property string $description
 * @property integer $point_type
 * @property double $point_required
 * @property integer $display_type
 * @property integer $order_type
 * @property integer $shuffle_answer
 * @property integer $question_random_number
 * @property integer $save_keep
 * @property integer $mod_doanswer
 * @property integer $can_travel
 * @property integer $show_only_status
 * @property integer $show_score
 * @property integer $show_score_cat
 * @property integer $show_doanswer
 * @property integer $show_solution
 * @property integer $time_dependent
 * @property integer $time_assigned
 * @property integer $penality_test
 * @property double $penality_time_test
 * @property integer $penality_quest
 * @property double $penality_time_quest
 * @property integer $max_attempt
 * @property integer $hide_info
 * @property string $order_info
 * @property string $cf_info
 * @property integer $use_suspension
 * @property string $suspension_num_attempts
 * @property string $suspension_num_hours
 * @property integer $suspension_prerequisites
 * @property string $chart_options
 * @property integer $mandatory_answer
 * @property integer $score_max
 * @property string $obj_type
 * @property integer $retain_answers_history
 */
class LearningTest extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTest the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_test';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description, order_info, cf_info, chart_options', 'required'),
			array('author, point_type, display_type, order_type, shuffle_answer, question_random_number, save_keep, mod_doanswer, can_travel, show_only_status, show_score, show_score_cat, show_doanswer, show_solution, time_dependent, time_assigned, penality_test, penality_quest, max_attempt, hide_info, use_suspension, suspension_prerequisites, mandatory_answer, score_max, retain_answers_history', 'numerical', 'integerOnly'=>true),
			array('point_required, penality_time_test, penality_time_quest', 'numerical'),
			array('title', 'length', 'max'=>255),
			array('suspension_num_attempts, suspension_num_hours', 'length', 'max'=>10),
			array('obj_type', 'length', 'max'=>45),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTest, author, title, description, point_type, point_required, display_type, order_type, shuffle_answer, question_random_number, save_keep, mod_doanswer, can_travel, show_only_status, show_score, show_score_cat, show_doanswer, show_solution, time_dependent, time_assigned, penality_test, penality_time_test, penality_quest, penality_time_quest, max_attempt, hide_info, order_info, cf_info, use_suspension, suspension_num_attempts, suspension_num_hours, suspension_prerequisites, chart_options, mandatory_answer, score_max, obj_type, retain_answers_history', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTest' => 'Id Test',
			'author' => 'Author',
			'title' => 'Title',
			'description' => 'Description',
			'point_type' => 'Point Type',
			'point_required' => 'Point Required',
			'display_type' => 'Display Type',
			'order_type' => 'Order Type',
			'shuffle_answer' => 'Shuffle Answer',
			'question_random_number' => 'Question Random Number',
			'save_keep' => 'Save Keep',
			'mod_doanswer' => 'Mod Doanswer',
			'can_travel' => 'Can Travel',
			'show_only_status' => 'Show Only Status',
			'show_score' => 'Show Score',
			'show_score_cat' => 'Show Score Cat',
			'show_doanswer' => 'Show Doanswer',
			'show_solution' => 'Show Solution',
			'time_dependent' => 'Time Dependent',
			'time_assigned' => 'Time Assigned',
			'penality_test' => 'Penality Test',
			'penality_time_test' => 'Penality Time Test',
			'penality_quest' => 'Penality Quest',
			'penality_time_quest' => 'Penality Time Quest',
			'max_attempt' => 'Max Attempt',
			'hide_info' => 'Hide Info',
			'order_info' => 'Order Info',
			'cf_info' => 'Cf Info',
			'use_suspension' => 'Use Suspension',
			'suspension_num_attempts' => 'Suspension Num Attempts',
			'suspension_num_hours' => 'Suspension Num Hours',
			'suspension_prerequisites' => 'Suspension Prerequisites',
			'chart_options' => 'Chart Options',
			'mandatory_answer' => 'Mandatory Answer',
			'score_max' => 'Score Max',
			'obj_type' => 'Obj Type',
			'retain_answers_history' => 'Retain Answers History',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTest',$this->idTest);
		$criteria->compare('author',$this->author);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('point_type',$this->point_type);
		$criteria->compare('point_required',$this->point_required);
		$criteria->compare('display_type',$this->display_type);
		$criteria->compare('order_type',$this->order_type);
		$criteria->compare('shuffle_answer',$this->shuffle_answer);
		$criteria->compare('question_random_number',$this->question_random_number);
		$criteria->compare('save_keep',$this->save_keep);
		$criteria->compare('mod_doanswer',$this->mod_doanswer);
		$criteria->compare('can_travel',$this->can_travel);
		$criteria->compare('show_only_status',$this->show_only_status);
		$criteria->compare('show_score',$this->show_score);
		$criteria->compare('show_score_cat',$this->show_score_cat);
		$criteria->compare('show_doanswer',$this->show_doanswer);
		$criteria->compare('show_solution',$this->show_solution);
		$criteria->compare('time_dependent',$this->time_dependent);
		$criteria->compare('time_assigned',$this->time_assigned);
		$criteria->compare('penality_test',$this->penality_test);
		$criteria->compare('penality_time_test',$this->penality_time_test);
		$criteria->compare('penality_quest',$this->penality_quest);
		$criteria->compare('penality_time_quest',$this->penality_time_quest);
		$criteria->compare('max_attempt',$this->max_attempt);
		$criteria->compare('hide_info',$this->hide_info);
		$criteria->compare('order_info',$this->order_info,true);
		$criteria->compare('cf_info',$this->cf_info,true);
		$criteria->compare('use_suspension',$this->use_suspension);
		$criteria->compare('suspension_num_attempts',$this->suspension_num_attempts,true);
		$criteria->compare('suspension_num_hours',$this->suspension_num_hours,true);
		$criteria->compare('suspension_prerequisites',$this->suspension_prerequisites);
		$criteria->compare('chart_options',$this->chart_options,true);
		$criteria->compare('mandatory_answer',$this->mandatory_answer);
		$criteria->compare('score_max',$this->score_max);
		$criteria->compare('obj_type',$this->obj_type,true);
		$criteria->compare('retain_answers_history',$this->retain_answers_history);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}