<?php

/**
 * This is the model class for table "core_user".
 *
 * The followings are the available columns in table 'core_user':
 * @property integer $idst
 * @property string $userid
 * @property string $firstname
 * @property string $lastname
 * @property string $pass
 * @property string $email
 * @property string $avatar
 * @property string $signature
 * @property integer $level
 * @property string $lastenter
 * @property integer $valid
 * @property string $pwd_expire_at
 * @property integer $force_change
 * @property string $register_date
 * @property string $facebook_id
 * @property string $twitter_id
 * @property string $linkedin_id
 * @property string $google_id
 * @property integer $privacy_policy
 */
class CoreUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return CoreUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'core_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('signature', 'required'),
			array('idst, level, valid, force_change, privacy_policy', 'numerical', 'integerOnly'=>true),
			array('userid, firstname, lastname, pass, email, avatar, facebook_id, twitter_id, linkedin_id, google_id', 'length', 'max'=>255),
			//array('lastenter, pwd_expire_at, register_date', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
                    // idst,
			array('idst, userid, firstname, lastname, pass, email, avatar, signature, level, lastenter, valid, pwd_expire_at, force_change, register_date, facebook_id, twitter_id, linkedin_id, google_id, privacy_policy', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idst' => 'Idst',
			'userid' => 'Userid',
			'firstname' => 'Firstname',
			'lastname' => 'Lastname',
			'pass' => 'Pass',
			'email' => 'Email',
			'avatar' => 'Avatar',
			'signature' => 'Signature',
			'level' => 'Level',
			'lastenter' => 'Lastenter',
			'valid' => 'Valid',
			'pwd_expire_at' => 'Pwd Expire At',
			'force_change' => 'Force Change',
			'register_date' => 'Register Date',
			'facebook_id' => 'Facebook',
			'twitter_id' => 'Twitter',
			'linkedin_id' => 'Linkedin',
			'google_id' => 'Google',
			'privacy_policy' => 'Privacy Policy',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idst',$this->idst);
		$criteria->compare('userid',$this->userid,true);
		$criteria->compare('firstname',$this->firstname,true);
		$criteria->compare('lastname',$this->lastname,true);
		$criteria->compare('pass',$this->pass,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('signature',$this->signature,true);
		$criteria->compare('level',$this->level);
		$criteria->compare('lastenter',$this->lastenter,true);
		$criteria->compare('valid',$this->valid);
		$criteria->compare('pwd_expire_at',$this->pwd_expire_at,true);
		$criteria->compare('force_change',$this->force_change);
		$criteria->compare('register_date',$this->register_date,true);
		$criteria->compare('facebook_id',$this->facebook_id,true);
		$criteria->compare('twitter_id',$this->twitter_id,true);
		$criteria->compare('linkedin_id',$this->linkedin_id,true);
		$criteria->compare('google_id',$this->google_id,true);
		$criteria->compare('privacy_policy',$this->privacy_policy);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}