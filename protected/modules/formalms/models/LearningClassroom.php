<?php

/**
 * This is the model class for table "learning_classroom".
 *
 * The followings are the available columns in table 'learning_classroom':
 * @property integer $idClassroom
 * @property string $name
 * @property string $description
 * @property integer $location_id
 * @property string $room
 * @property string $street
 * @property string $city
 * @property string $state
 * @property string $zip_code
 * @property string $phone
 * @property string $fax
 * @property string $capacity
 * @property string $disposition
 * @property string $instrument
 * @property string $available_instrument
 * @property string $note
 * @property string $responsable
 */
class LearningClassroom extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningClassroom the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_classroom';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('description', 'required'), // disposition, instrument, available_instrument, note
			array('location_id', 'numerical', 'integerOnly'=>true),
			array('name, room, street, city, state, zip_code, phone, fax, capacity, responsable', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idClassroom, name, description, location_id, room, street, city, state, zip_code, phone, fax, capacity, disposition, instrument, available_instrument, note, responsable', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idClassroom' => 'Id Classroom',
			'name' => 'Name',
			'description' => 'Description',
			'location_id' => 'Location',
			'room' => 'Room',
			'street' => 'Street',
			'city' => 'City',
			'state' => 'State',
			'zip_code' => 'Zip Code',
			'phone' => 'Phone',
			'fax' => 'Fax',
			'capacity' => 'Capacity',
			'disposition' => 'Disposition',
			'instrument' => 'Instrument',
			'available_instrument' => 'Available Instrument',
			'note' => 'Note',
			'responsable' => 'Responsable',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idClassroom',$this->idClassroom);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('location_id',$this->location_id);
		$criteria->compare('room',$this->room,true);
		$criteria->compare('street',$this->street,true);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('state',$this->state,true);
		$criteria->compare('zip_code',$this->zip_code,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('fax',$this->fax,true);
		$criteria->compare('capacity',$this->capacity,true);
		$criteria->compare('disposition',$this->disposition,true);
		$criteria->compare('instrument',$this->instrument,true);
		$criteria->compare('available_instrument',$this->available_instrument,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('responsable',$this->responsable,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}