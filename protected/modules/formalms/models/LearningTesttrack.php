<?php

/**
 * This is the model class for table "learning_testtrack".
 *
 * The followings are the available columns in table 'learning_testtrack':
 * @property integer $idTrack
 * @property integer $idUser
 * @property integer $idReference
 * @property integer $idTest
 * @property string $date_attempt
 * @property string $date_attempt_mod
 * @property string $date_end_attempt
 * @property integer $last_page_seen
 * @property integer $last_page_saved
 * @property integer $number_of_save
 * @property integer $number_of_attempt
 * @property double $score
 * @property double $bonus_score
 * @property string $score_status
 * @property string $comment
 * @property string $attempts_for_suspension
 * @property string $suspended_until
 */
class LearningTesttrack extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningTesttrack the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_testtrack';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('comment', 'required'),
			array('idUser, idReference, idTest, last_page_seen, last_page_saved, number_of_save, number_of_attempt', 'numerical', 'integerOnly'=>true),
			array('score, bonus_score', 'numerical'),
			array('score_status', 'length', 'max'=>12),
			array('attempts_for_suspension', 'length', 'max'=>10),
			array('date_attempt, date_attempt_mod, date_end_attempt, suspended_until', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idTrack, idUser, idReference, idTest, date_attempt, date_attempt_mod, date_end_attempt, last_page_seen, last_page_saved, number_of_save, number_of_attempt, score, bonus_score, score_status, comment, attempts_for_suspension, suspended_until', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idTrack' => 'Id Track',
			'idUser' => 'Id User',
			'idReference' => 'Id Reference',
			'idTest' => 'Id Test',
			'date_attempt' => 'Date Attempt',
			'date_attempt_mod' => 'Date Attempt Mod',
			'date_end_attempt' => 'Date End Attempt',
			'last_page_seen' => 'Last Page Seen',
			'last_page_saved' => 'Last Page Saved',
			'number_of_save' => 'Number Of Save',
			'number_of_attempt' => 'Number Of Attempt',
			'score' => 'Score',
			'bonus_score' => 'Bonus Score',
			'score_status' => 'Score Status',
			'comment' => 'Comment',
			'attempts_for_suspension' => 'Attempts For Suspension',
			'suspended_until' => 'Suspended Until',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idTrack',$this->idTrack);
		$criteria->compare('idUser',$this->idUser);
		$criteria->compare('idReference',$this->idReference);
		$criteria->compare('idTest',$this->idTest);
		$criteria->compare('date_attempt',$this->date_attempt,true);
		$criteria->compare('date_attempt_mod',$this->date_attempt_mod,true);
		$criteria->compare('date_end_attempt',$this->date_end_attempt,true);
		$criteria->compare('last_page_seen',$this->last_page_seen);
		$criteria->compare('last_page_saved',$this->last_page_saved);
		$criteria->compare('number_of_save',$this->number_of_save);
		$criteria->compare('number_of_attempt',$this->number_of_attempt);
		$criteria->compare('score',$this->score);
		$criteria->compare('bonus_score',$this->bonus_score);
		$criteria->compare('score_status',$this->score_status,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('attempts_for_suspension',$this->attempts_for_suspension,true);
		$criteria->compare('suspended_until',$this->suspended_until,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}