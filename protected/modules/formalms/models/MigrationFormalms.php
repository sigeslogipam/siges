<?php

/**
 * This is the model class for table "migration_formalms".
 *
 * The followings are the available columns in table 'migration_formalms':
 * @property integer $id
 * @property string $data_type
 * @property string $data
 * @property integer $is_migrate
 * @property string $date_migration
 * @property string $migrate_by
 */
class MigrationFormalms extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return MigrationFormalms the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'migration_formalms';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('data_type, data, is_migrate, date_migration, migrate_by', 'required'),
			array('is_migrate', 'numerical', 'integerOnly'=>true),
			array('data_type, data, migrate_by', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, data_type, data, is_migrate, date_migration, migrate_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'data_type' => 'Data Type',
			'data' => 'Data',
			'is_migrate' => 'Is Migrate',
			'date_migration' => 'Date Migration',
			'migrate_by' => 'Migrate By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('data_type',$this->data_type,true);
		$criteria->compare('data',$this->data,true);
		$criteria->compare('is_migrate',$this->is_migrate);
		$criteria->compare('date_migration',$this->date_migration,true);
		$criteria->compare('migrate_by',$this->migrate_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}