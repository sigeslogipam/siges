<?php

/**
 * This is the model class for table "learning_course".
 *
 * The followings are the available columns in table 'learning_course':
 * @property integer $idCourse
 * @property integer $idCategory
 * @property string $code
 * @property string $name
 * @property string $box_description
 * @property string $description
 * @property string $lang_code
 * @property integer $status
 * @property integer $level_show_user
 * @property integer $subscribe_method
 * @property string $linkSponsor
 * @property string $imgSponsor
 * @property string $img_course
 * @property string $img_material
 * @property string $img_othermaterial
 * @property string $course_demo
 * @property string $mediumTime
 * @property integer $permCloseLO
 * @property integer $userStatusOp
 * @property string $difficult
 * @property integer $show_progress
 * @property integer $show_time
 * @property integer $show_who_online
 * @property integer $show_extra_info
 * @property integer $show_rules
 * @property string $date_begin
 * @property string $date_end
 * @property string $hour_begin
 * @property string $hour_end
 * @property integer $valid_time
 * @property integer $max_num_subscribe
 * @property integer $min_num_subscribe
 * @property double $max_sms_budget
 * @property integer $selling
 * @property string $prize
 * @property string $course_type
 * @property string $policy_point
 * @property integer $point_to_all
 * @property integer $course_edition
 * @property string $classrooms
 * @property string $certificates
 * @property string $create_date
 * @property string $security_code
 * @property string $imported_from_connection
 * @property string $course_quota
 * @property string $used_space
 * @property double $course_vote
 * @property integer $allow_overbooking
 * @property integer $can_subscribe
 * @property string $sub_start_date
 * @property string $sub_end_date
 * @property string $advance
 * @property string $autoregistration_code
 * @property integer $direct_play
 * @property integer $use_logo_in_courselist
 * @property integer $show_result
 * @property integer $credits
 * @property integer $auto_unsubscribe
 * @property string $unsubscribe_date_limit
 * @property integer $id_menucustom
 */
class LearningCourse extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LearningCourse the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return CDbConnection database connection
	 */
	public function getDbConnection()
	{
		return Yii::app()->dbformalms;
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'learning_course';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('box_description', 'required'),
			array('idCategory, status, level_show_user, subscribe_method, permCloseLO, userStatusOp, show_progress, show_time, show_who_online, show_extra_info, show_rules, valid_time, max_num_subscribe, min_num_subscribe, selling, point_to_all, course_edition, allow_overbooking, can_subscribe, direct_play, use_logo_in_courselist, show_result, credits, auto_unsubscribe, id_menucustom', 'numerical', 'integerOnly'=>true),
			array('max_sms_budget, course_vote', 'numerical'),
			array('code', 'length', 'max'=>50),
			array('name, linkSponsor, imgSponsor, img_course, img_material, img_othermaterial, course_demo, prize, course_type, policy_point, classrooms, certificates, security_code, imported_from_connection, course_quota, used_space, advance, autoregistration_code', 'length', 'max'=>255),
			array('lang_code', 'length', 'max'=>100),
			array('mediumTime', 'length', 'max'=>10),
			array('difficult', 'length', 'max'=>13),
			array('hour_begin, hour_end', 'length', 'max'=>5),
			array('date_begin, date_end, create_date, sub_start_date, sub_end_date, unsubscribe_date_limit', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('idCourse, idCategory, code, name, box_description, description, lang_code, status, level_show_user, subscribe_method, linkSponsor, imgSponsor, img_course, img_material, img_othermaterial, course_demo, mediumTime, permCloseLO, userStatusOp, difficult, show_progress, show_time, show_who_online, show_extra_info, show_rules, date_begin, date_end, hour_begin, hour_end, valid_time, max_num_subscribe, min_num_subscribe, max_sms_budget, selling, prize, course_type, policy_point, point_to_all, course_edition, classrooms, certificates, create_date, security_code, imported_from_connection, course_quota, used_space, course_vote, allow_overbooking, can_subscribe, sub_start_date, sub_end_date, advance, autoregistration_code, direct_play, use_logo_in_courselist, show_result, credits, auto_unsubscribe, unsubscribe_date_limit, id_menucustom', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'idCourse' => 'Id Course',
			'idCategory' => 'Id Category',
			'code' => 'Code',
			'name' => 'Name',
			'box_description' => 'Box Description',
			'description' => 'Description',
			'lang_code' => 'Lang Code',
			'status' => 'Status',
			'level_show_user' => 'Level Show User',
			'subscribe_method' => 'Subscribe Method',
			'linkSponsor' => 'Link Sponsor',
			'imgSponsor' => 'Img Sponsor',
			'img_course' => 'Img Course',
			'img_material' => 'Img Material',
			'img_othermaterial' => 'Img Othermaterial',
			'course_demo' => 'Course Demo',
			'mediumTime' => 'Medium Time',
			'permCloseLO' => 'Perm Close Lo',
			'userStatusOp' => 'User Status Op',
			'difficult' => 'Difficult',
			'show_progress' => 'Show Progress',
			'show_time' => 'Show Time',
			'show_who_online' => 'Show Who Online',
			'show_extra_info' => 'Show Extra Info',
			'show_rules' => 'Show Rules',
			'date_begin' => 'Date Begin',
			'date_end' => 'Date End',
			'hour_begin' => 'Hour Begin',
			'hour_end' => 'Hour End',
			'valid_time' => 'Valid Time',
			'max_num_subscribe' => 'Max Num Subscribe',
			'min_num_subscribe' => 'Min Num Subscribe',
			'max_sms_budget' => 'Max Sms Budget',
			'selling' => 'Selling',
			'prize' => 'Prize',
			'course_type' => 'Course Type',
			'policy_point' => 'Policy Point',
			'point_to_all' => 'Point To All',
			'course_edition' => 'Course Edition',
			'classrooms' => 'Classrooms',
			'certificates' => 'Certificates',
			'create_date' => 'Create Date',
			'security_code' => 'Security Code',
			'imported_from_connection' => 'Imported From Connection',
			'course_quota' => 'Course Quota',
			'used_space' => 'Used Space',
			'course_vote' => 'Course Vote',
			'allow_overbooking' => 'Allow Overbooking',
			'can_subscribe' => 'Can Subscribe',
			'sub_start_date' => 'Sub Start Date',
			'sub_end_date' => 'Sub End Date',
			'advance' => 'Advance',
			'autoregistration_code' => 'Autoregistration Code',
			'direct_play' => 'Direct Play',
			'use_logo_in_courselist' => 'Use Logo In Courselist',
			'show_result' => 'Show Result',
			'credits' => 'Credits',
			'auto_unsubscribe' => 'Auto Unsubscribe',
			'unsubscribe_date_limit' => 'Unsubscribe Date Limit',
			'id_menucustom' => 'Id Menucustom',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('idCourse',$this->idCourse);
		$criteria->compare('idCategory',$this->idCategory);
		$criteria->compare('code',$this->code,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('box_description',$this->box_description,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('lang_code',$this->lang_code,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('level_show_user',$this->level_show_user);
		$criteria->compare('subscribe_method',$this->subscribe_method);
		$criteria->compare('linkSponsor',$this->linkSponsor,true);
		$criteria->compare('imgSponsor',$this->imgSponsor,true);
		$criteria->compare('img_course',$this->img_course,true);
		$criteria->compare('img_material',$this->img_material,true);
		$criteria->compare('img_othermaterial',$this->img_othermaterial,true);
		$criteria->compare('course_demo',$this->course_demo,true);
		$criteria->compare('mediumTime',$this->mediumTime,true);
		$criteria->compare('permCloseLO',$this->permCloseLO);
		$criteria->compare('userStatusOp',$this->userStatusOp);
		$criteria->compare('difficult',$this->difficult,true);
		$criteria->compare('show_progress',$this->show_progress);
		$criteria->compare('show_time',$this->show_time);
		$criteria->compare('show_who_online',$this->show_who_online);
		$criteria->compare('show_extra_info',$this->show_extra_info);
		$criteria->compare('show_rules',$this->show_rules);
		$criteria->compare('date_begin',$this->date_begin,true);
		$criteria->compare('date_end',$this->date_end,true);
		$criteria->compare('hour_begin',$this->hour_begin,true);
		$criteria->compare('hour_end',$this->hour_end,true);
		$criteria->compare('valid_time',$this->valid_time);
		$criteria->compare('max_num_subscribe',$this->max_num_subscribe);
		$criteria->compare('min_num_subscribe',$this->min_num_subscribe);
		$criteria->compare('max_sms_budget',$this->max_sms_budget);
		$criteria->compare('selling',$this->selling);
		$criteria->compare('prize',$this->prize,true);
		$criteria->compare('course_type',$this->course_type,true);
		$criteria->compare('policy_point',$this->policy_point,true);
		$criteria->compare('point_to_all',$this->point_to_all);
		$criteria->compare('course_edition',$this->course_edition);
		$criteria->compare('classrooms',$this->classrooms,true);
		$criteria->compare('certificates',$this->certificates,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('security_code',$this->security_code,true);
		$criteria->compare('imported_from_connection',$this->imported_from_connection,true);
		$criteria->compare('course_quota',$this->course_quota,true);
		$criteria->compare('used_space',$this->used_space,true);
		$criteria->compare('course_vote',$this->course_vote);
		$criteria->compare('allow_overbooking',$this->allow_overbooking);
		$criteria->compare('can_subscribe',$this->can_subscribe);
		$criteria->compare('sub_start_date',$this->sub_start_date,true);
		$criteria->compare('sub_end_date',$this->sub_end_date,true);
		$criteria->compare('advance',$this->advance,true);
		$criteria->compare('autoregistration_code',$this->autoregistration_code,true);
		$criteria->compare('direct_play',$this->direct_play);
		$criteria->compare('use_logo_in_courselist',$this->use_logo_in_courselist);
		$criteria->compare('show_result',$this->show_result);
		$criteria->compare('credits',$this->credits);
		$criteria->compare('auto_unsubscribe',$this->auto_unsubscribe);
		$criteria->compare('unsubscribe_date_limit',$this->unsubscribe_date_limit,true);
		$criteria->compare('id_menucustom',$this->id_menucustom);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}