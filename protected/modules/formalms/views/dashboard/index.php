<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));


$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->baseUrl;

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/css/AdminLTE.min.css");
$cs->registerCssFile($baseUrl."/css/new-dashboard.css");
$cs->registerCssFile("https://use.fontawesome.com/releases/v5.5.0/css/all.css");

// echo "Dashboard FormaLMS";

// Retrieve online course

$learning_course = new LearningCourse();
$learning_exam = new LearningTest();
$learning_students =  new LearningCourseuser();
$all_learning_student = $learning_students::model()->findAllBySql("SELECT DISTINCT idUser FROM `learning_courseuser` where status = 1");
$count_learning_student = 0;

foreach($all_learning_student as $als){
    $count_learning_student++;
}



$count_learning_course = $learning_course::model()->count();
$count_learning_exam = $learning_exam::model()->count();


/*
$sql = "SELECT * from core_group";
$db1Rows = Yii::app()->dbformalms->createCommand($sql)->queryAll();
print_r($db1Rows);
*/

// print_r($all_core_group_data);
/*
foreach($all_learning_course_data as $alcd){
    echo $alcd->name.'<br/>';
}
 *
 */

?>

<div class="row-fluid flm">
    <div id="dash">
        <div class="span1">

        </div>
        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate courses to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('default/migrateCourses'));

                   ?>
         </div>
        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate students to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('default/migrateUsers'));

                   ?>
         </div>
        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate classrooms to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('default/migrateRooms'));

                   ?>
         </div>

    </div>
</div>

<div class="container-fluid" style="text-align:center;" >
    <div class="row-fluid">
        <div class="checkbox" style="text-align:justify;padding-left:30px;" id="index_dash">
                <!-- Re loocker le dashboard ici -->
                <div  class="row">
                   <!-- Element 1 -->
                   <div class="span4 ti-bwat">
                         <!-- small box -->
                      <div class="small-box bg-aqua" data-html="true" rel="tooltip">
                        <div class="inner">
                            <h3><span class="teks-blan"><?= Yii::t('app','Total online courses') ?></span></h3>

                            <p class="nom-piti teks-blan">
                                <?= Yii::t('app','{count_learning_course} cours',array('{count_learning_course}'=>$count_learning_course)); ?>
                            </p>

                            </div>

                       <div class="icon">
                           <i class="fas fa-chalkboard-teacher" style="padding-top: 20px"></i>
                        </div>
                      </div>
                    </div>

                   <!-- Element 2 -->
                   <div class="span4 ti-bwat">
                    <!-- small box -->
                      <div class="small-box bg-green" data-html="true" rel="tooltip">
                        <div class="inner">
                            <h3><span class="teks-blan"><?= Yii::t('app','Total online exam') ?></span></h3>

                            <p class="nom-piti teks-blan">
                                <?= Yii::t('app','{count_learning_exam} exams',array('{count_learning_exam}'=>$count_learning_exam)); ?>

                            </p>

                            </div>

                       <div class="icon">
                           <i class="fas fa-clipboard-list" style="padding-top: 20px"></i>
                       </div>
                      </div>
                    </div>

                   <!-- Element 3 -->
                   <div class="span4 ti-bwat">
                         <!-- small box -->
                      <div class="small-box bg-fuchsia" data-html="true" rel="tooltip">
                        <div class="inner">
                            <h3><span class="teks-blan"><?= Yii::t('app','Total e-learning students') ?></span></h3>

                            <p class="nom-piti teks-blan">
                                 <?= Yii::t('app','{count_learning_student} students',array('{count_learning_student}'=>$count_learning_student)); ?>

                            </p>

                            </div>

                       <div class="icon">
                           <i class="fas fa-user-graduate" style="padding-top: 20px"></i>
                        </div>
                      </div>
                    </div>

                </div>
        </div>
    </div>
</div>
