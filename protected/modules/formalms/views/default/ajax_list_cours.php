<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo $room_id;
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$acad = Yii::app()->session['currentId_academic_year'];

$room_name = Rooms::model()->findByPk($room_id)->room_name; 

$sql_1 = "SELECT students FROM room_has_person WHERE room = $room_id AND academic_year = $acad";
$data_stud_room = RoomHasPerson::model()->findAllBySql($sql_1); 
$list_student = array(); 
$k = 0;
foreach($data_stud_room as $dsr){
    $list_student[$k] = $dsr->students; 
    $k++;
}
$list_student_comma = implode(',', $list_student);

//echo $list_student_comma;
$sql_2 = "SELECT DISTINCT idCourse FROM learning_courseuser WHERE idUser IN ($list_student_comma)";
$course_for_student_data = LearningCourseuser::model()->findAllBYSql($sql_2); 


 //echo $list_student_comma; 
?>
<h5><?= Yii::t("app","List of course for {room_name}",array("{room_name}"=>$room_name)); ?> </h5>
<ul class="nav nav-pills nav-stacked">
  <?php
    foreach($course_for_student_data as $cfsd){
        ?>
    <li><a class="course-zone" href="#" data-idcourse="<?= $cfsd->idCourse; ?>"><?= LearningCourse::model()->findByPk($cfsd->idCourse)->name." (".LearningCourse::model()->findByPk($cfsd->idCourse)->code.")"; ?></a></li>
    <?php 
    }
  ?>  
  
</ul>


<script>
    $(document).ready(function(){
       
       $(".course-zone").click(function(){
           var course_id = $(this).attr("data-idcourse");
          
           $.get('<?= $baseUrl?>/index.php/formalms/default/ReziltaNote',{id_course:course_id},function(data){
                   $('#rezilta-note').html(data);
                 });
       });
        
    });
</script>