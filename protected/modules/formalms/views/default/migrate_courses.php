<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title =  Yii::t('app',"Migrate Courses");
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$acad = Yii::app()->session['currentId_academic_year'];

$sql_courses = "SELECT c.id, c.academic_period, c.teacher, c.subject, c.room, c.weight, s.subject_name, s.short_subject_name, p.first_name, p.last_name, r.room_name, r.short_room_name FROM courses c
INNER JOIN subjects s ON (c.subject = s.id)
INNER JOIN rooms r ON (c.room = r.id)
INNER JOIN persons p ON (c.teacher = p.id)
WHERE c.academic_period = $acad";

$all_siges_courses = Courses::model()->findAllBySql($sql_courses);

?>

<div class="row-fluid flm">
    <div id="dash">
        <div class="span1">

        </div>

        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate students to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('default/migrateUsers'));

                   ?>
         </div>
        <div class="span4">

                  <?php

                 $images = '<i class="fas fa-undo">&nbsp;'.Yii::t('app','Back').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('dashboard/index'));

                   ?>
         </div>

    </div>
</div>

<div class="row-fluid tit_h1">
    <h1>
        <?= Yii::t('app','Migrate siges courses to E-learning');?>
    </h1>
</div>

<div class="row">
    <div class="span1">

    </div>
    <div class="span10 plas-rezilta-migration" id="plas-rezilta-migration">
        <div class="alert alert-success alert-dismissible plas-rezilta-migration">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <div id="rezilta-migration" class="plas-rezilta-migration"></div>
        </div>
    </div>
    <div class="span1">

    </div>
</div>

<div class='row-fluid'>
    <div class='span12'>
        <div id="persons-grid" class="form">
            <table class="table responsive table-striped table-hover" id="DataTables_Table_0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= Yii::t('app','Subject name')?></th>
                        <th><?= Yii::t('app','Room') ?></th>
                        <th><?= Yii::t('app','Teacher') ?></th>

                        <th class="checkbox-column" id="chk">
                            <input class="select-on-check-all" type="checkbox" value="1" name="chk_all" id="check-all">
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $k = 1;
                        foreach($all_siges_courses as $asc){
                            ?>
                    <tr>
                        <td><?= $k; ?></td>
                        <td><?= $asc->subject0->subject_name; ?></td>
                        <td><?= $asc->room0->short_room_name; ?></td>
                        <td><?= $asc->teacher0->fullName; ?></td>
                        <td>
                            <?php
                            $code = $asc->subject0->short_subject_name.'-'.$asc->id;
                                if(!isCourseMigrate($code)){
                                    ?>
                            <input class="on-check" type="checkbox" value="<?= $asc->id; ?>" name="chk" id="<?= $asc->id ?>">
                                <?php } else {?>

                                <?php } ?>
                        </td>
                        <td>
                            <?php
                                if(isCourseMigrate($code)){
                                    ?>
                                    <i class="fas fa-toggle-on" style="color: green"></i>

                            <?php
                                }else{
                                    ?>
                                    <i class="fas fa-toggle-off" style="color: red"></i>
                        <?php
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                    $k++;
                        }
                    ?>
                </tbody>
            </table>

        </div>
    </div>
</div>

<div class="row" style="text-align: center">
    <a href="#" id="migrate-course" class="btn btn-warning"><?= Yii::t('app','Migrate course selection'); ?></a>
</div>

<script>
    var listid = null;

   $(document).ready(function(){
         $("#migrate-course").hide();
         $('.plas-rezilta-migration').hide();

        function getValueUsingClass(){
            /* declare an checkbox array */
            var chkArray = [];

            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".on-check:checked").each(function() {
                    chkArray.push($(this).val());
            });

            /* we join the array separated by the comma */
            var selected;
            selected = chkArray.join(',') ;

            return selected;
        }

        $("#check-all").click(function(){

           if ($('#check-all').is(":checked"))
                {
                  $('.on-check').prop('checked',true);
                  listid = getValueUsingClass();
                  $("#migrate-course").show();

                }else{
                    $('.on-check').prop('checked',false);
                    $("#migrate-course").hide();
                }


        });

        $('.on-check').click(function(){
            $("#migrate-course").show();
            listid = getValueUsingClass();
            if(!$(".on-check").is(":checked") && !$('#check-all').is(":checked")){
                $("#migrate-course").hide();
            }
        });

         $('#migrate-course').click(function(){
              $.get('<?= $baseUrl?>/index.php/formalms/default/makeCourseMigration',{course:listid},function(data){
                $('.plas-rezilta-migration').show();
                $('#rezilta-migration').html(data);
                //wait(5000);
                setTimeout(function(){  location.reload(); }, 5000);

            });

             //  window.open("<?= $baseUrl; ?>/index.php/kindergarden/kinderEvaluation/makepdf/room/"+listid);

            });



         $('#DataTables_Table_0').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [




                ]

            });






    });

</script>
