<?php

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$acad = Yii::app()->session['currentId_academic_year'];

$level_data = Rooms::model()->findAllBySql("SELECT DISTINCT r.id, r.room_name, r.short_room_name  FROM rooms r  INNER JOIN room_has_person rhp ON (rhp.room = r.id) WHERE rhp.academic_year = $acad order by r.room_name ASC");


?>
<div class="row-fluid flm">
    <div id="dash">
        <div class="span1">

        </div>

        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migration dashbord').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('dashboard/index'));

                   ?>
         </div>


    </div>
</div>

<div class="row-fluid tit_h1">
    <h1>
        <?= Yii::t('app','Update SIGES course to meet Elearning requirements');?>
    </h1>
</div>

<div class="row-fluid">
    <div class="span3">
        <label id="resp_form">
        <label for="cat-name" class="required"><?= Yii::t('app','Room name'); ?></label>
        <select name="room" id="room">
            <option value=""><?= Yii::t('app','Choose room'); ?></option>
            <?php
                foreach($level_data as $ld){
                    if(isRoomMigrate("/".$ld->short_room_name)){
                    ?>
            <option value="<?= $ld->id; ?>"><?= $ld->room_name; ?></option>
            <?php
                    }
                }
            ?>
        </select>
        </label>
    </div>

</div>

<div class="row-fluid">
    <div class="span9" id="list-cours">

    </div>

</div>

<script>
    $(document).ready(function(){
       $("#room").change(function(){
           var room_id = $("#room").val();
           $.get('<?= $baseUrl?>/index.php/formalms/default/ShowListCourse',{id_room:room_id},function(data){
                   $('#list-cours').html(data);
                 });
       });



    });
</script>
