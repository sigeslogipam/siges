<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$acad = Yii::app()->session['currentId_academic_year'];


$sql_student = "SELECT p.id, p.last_name, p.first_name, p.gender, p.active, rhp.room  FROM persons p INNER JOIN room_has_person rhp ON (rhp.students = p.id) WHERE rhp.academic_year = $acad AND rhp.room = $level and active IN (1,2) ORDER BY p.last_name"; 
$all_student_in_level = Persons::model()->findAllBySql($sql_student); 
$level_name = Rooms::model()->findByPk($level)->room_name;
$baseUrl = Yii::app()->baseUrl;

?>

<table class="table responsive table-striped table-hover" id="DataTables_Table_0">
                 <thead>
                     <tr>
                        <th>#</th>
                        <th><?= Yii::t('app','First name')?></th>
                        <th><?= Yii::t('app','Last name') ?></th>
                        <th><?= Yii::t('app','Class name') ?></th>
                        
                        <th class="checkbox-column" id="chk">
                            <input class="select-on-check-all" type="checkbox" value="1" name="chk_all" id="check-all">
                        </th>
                        <th></th>
                        
                    </tr>
                 </thead>
                     <tbody>
                            <!-- Place les utilisateurs ici --> 
                    <?php 
                        $k=1; 
                        foreach ($all_student_in_level as $asi){
                            ?>
                    <tr>         
                         <td><?= $k; ?></td>
                         <td><?= $asi->first_name; ?></td>
                         <td><?= $asi->last_name; ?></td>
                         <td><?= $level_name; ?></td>
                         <td>
                        <?php 
                            if(!isStudentMigrate($asi->id)){
                        ?>
                              <input class="on-check" type="checkbox" value="<?= $asi->id; ?>" name="chk" id="<?= $asi->id ?>">
                        <?php 
                            } 
                        ?>
                         </td>
                         <td>
                         <?php 
                                if(isStudentMigrate($asi->id)){
                                    ?>
                                    <i class="fas fa-toggle-on" style="color: green"></i>
                            
                            <?php
                                }else{
                                    ?>
                                    <i class="fas fa-toggle-off" style="color: red"></i>
                        <?php 
                                }
                            ?>
                         </td>
                    </tr>
                    <?php 
                        $k++;
                        }
                    ?>
                            
                     </tbody>
</table>

<div class="row" style="text-align: center">
    <a href="#" id="migrate-student" class="btn btn-warning"><?= Yii::t('app','Migrate students selection'); ?></a>
</div>

<script>
    $(document).ready(function(){
         $("#migrate-student").hide();
         $('.plas-rezilta-migration').hide(); 
        
        
        function getValueUsingClass(){
            /* declare an checkbox array */
            var chkArray = [];

            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".on-check:checked").each(function() {
                    chkArray.push($(this).val());
            });

            /* we join the array separated by the comma */
            var selected;
            selected = chkArray.join(',') ;

            return selected;
        }
        
        $("#check-all").click(function(){
            
           if ($('#check-all').is(":checked"))
                {
                  $('.on-check').prop('checked',true);
                  listid = getValueUsingClass();
                  $("#migrate-student").show();
                 
                }else{
                    $('.on-check').prop('checked',false);
                    $("#migrate-student").hide();
                }
        
                
        });
        
        $('.on-check').click(function(){
            $("#migrate-student").show();
            listid = getValueUsingClass();
            if(!$(".on-check").is(":checked") && !$('#check-all').is(":checked")){
                $("#migrate-student").hide();
            }
        });
        
        $('#migrate-student').click(function(){
              $.get('<?= $baseUrl?>/index.php/formalms/default/makeStudentMigration',{student:listid},function(data){
                $('.plas-rezilta-migration').show();   
                $('#rezilta-migration').html(data);
                //wait(5000);
                setTimeout(function(){  location.reload(); }, 5000);
                
            });
               
             //  window.open("<?= $baseUrl; ?>/index.php/kindergarden/kinderEvaluation/makepdf/room/"+listid);
        
            });
      
        $('#DataTables_Table_0').DataTable({
                pageLength: 50,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  
                    
                  
                 
                ]

            });
        
    });
</script>