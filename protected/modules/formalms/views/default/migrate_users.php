<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title =  Yii::t('app',"Migrate students and teacher");
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$acad = Yii::app()->session['currentId_academic_year'];

$level_data = Rooms::model()->findAllBySql("SELECT DISTINCT r.id, r.room_name, r.short_room_name  FROM rooms r  INNER JOIN room_has_person rhp ON (rhp.room = r.id) WHERE rhp.academic_year = $acad order by r.room_name ASC");

$sql_stud = "SELECT * FROM users";



$all_siges_users = User::model()->findAllBySql($sql_stud);
?>

<div class="row-fluid flm">
    <div id="dash">
        <div class="span1">

        </div>

        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate course to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('default/migrateCourses'));

                   ?>
         </div>
        <div class="span4">

                  <?php

                 $images = '<i class="fas fa-undo">&nbsp;'.Yii::t('app','Back').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('dashboard/index'));

                   ?>
         </div>

    </div>
</div>

<div class="row-fluid tit_h1">
    <h1>
        <?= Yii::t('app','Migrate siges students and teachers to E-learning');?>
    </h1>
</div>

<div class="row-fluid">
    <div class="span6">
        <label id="resp_form">
        <label for="cat-name" class="required"><?= Yii::t('app','Room name'); ?></label>
        <select name="level" id="level">
            <option value=""><?= Yii::t('app','Choose room'); ?></option>
            <?php
                foreach($level_data as $ld){
                    if(isRoomMigrate("/".$ld->short_room_name)){
                    ?>
            <option value="<?= $ld->id; ?>"><?= $ld->room_name; ?></option>
            <?php
                    }
                }
            ?>
        </select>
        </label>
    </div>
</div>

<div class="row-fluid">
    <p>
        &nbsp;
    </p>
</div>

<div class="row">
    <div class="span1">

    </div>
    <div class="span10 plas-rezilta-migration" id="plas-rezilta-migration">
        <div class="alert alert-success alert-dismissible plas-rezilta-migration">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <div id="rezilta-migration" class="plas-rezilta-migration"></div>
        </div>
    </div>
    <div class="span1">

    </div>
</div>

<div class='row-fluid'>
    <div class='span12'>
        <div id="persons-grid" class="form">
         </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        // alert("Test");
        $('.plas-rezilta-migration').hide();

        $("#level").change(function(){
            var id_level = $(this).val();
            //alert(id_level);

             $.get('<?= $baseUrl?>/index.php/formalms/default/showStudent',{level:id_level},function(data){
                   $('#persons-grid').html(data);
                 });

        });


    });
</script>
