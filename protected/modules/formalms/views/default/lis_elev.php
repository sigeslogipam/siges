<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$acad = Yii::app()->session['currentId_academic_year'];
$sql_2 = "SELECT * FROM learning_testtrack where idTest = $id_test AND score_status = 'valid'";
$all_test_track_data = LearningTesttrack::model()->findAllBySql($sql_2);

$sql_period = "SELECT eby.id, eby.evaluation_date, e.evaluation_name, a.name_period FROM evaluation_by_year eby "
        . "INNER JOIN academicperiods a ON (eby.academic_year = a.id) "
        . "INNER JOIN evaluations e ON (eby.evaluation = e.id) WHERE a.year = $acad";

$all_data_period = EvaluationByYear::model()->findAllBySql($sql_period);


preg_match_all('!\d+!', $course_code, $course_id_array);
$course_id = $course_id_array[0][0];

?>
 <form method="POST">

<div class="row-fluid anndan">
<div class="span8">
     <table class="table responsive table-striped table-hover" id="DataTables_Table_0"">
        <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t("app","First name")?></th>
                 <th><?= Yii::t("app","Last name")?></th>
                  <th><?= Yii::t("app","Grades")?></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $konte = 1;
                foreach($all_test_track_data as $ad){
                    ?>

            <tr>
                <td><?= $konte; ?></td>
                <td><?= Persons::model()->findByPk($ad->idUser)->first_name; ?></td>
                <td><?= Persons::model()->findByPk($ad->idUser)->last_name; ?></td>
                <td>
                    <input name="student_<?=$konte?>" type="hidden" value="<?= $ad->idUser?>">
                    <input id="<?= $ad->idUser?>" name="grade_<?= $konte; ?>" value="<?= $ad->score+$ad->bonus_score; ?>">
                </td>
            </tr>
            <?php
                $konte++;
                }
            ?>

        </tbody>
    </table>
</div>
    <div class="span3" id="cours_periode_siges">

            <label id="resp_form">
                <label for="eval_period" class="required"><?= Yii::t('app','Evaluation period'); ?></label>
                <select name="eval_period" id="eval_period">
                    <option value=""><?= Yii::t('app','Choose evaluation period'); ?></option>
                    <?php
                        foreach($all_data_period as $adp){

                            ?>
                    <option value="<?= $adp->id; ?>">
                        <?php
                            setlocale(LC_TIME, "fr_FR");
                           echo  $adp->name_period.' / '.$adp->evaluation_name.' ('.date("d/m/Y",strtotime($adp->evaluation_date)).')';
                            ?>
                    </option>
                    <?php

                        }
                    ?>
                </select>
        </label>
        <input name="course_id" value="<?= $course_id ?>" type="hidden">
        <input name="total_konte" value="<?= $konte; ?>" type="hidden">
        <button type="submit" class="btn btn-warning btnm" name="save_grades">
            <?= Yii::t("app","Transfert grades to SIGES"); ?>
        </button>
    </div>
</div>
 </form>
