<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// echo $id_course;
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$sql_1 = "SELECT title, idResource FROM learning_organization WHERE objectType = 'test' AND idCourse = $id_course";
$all_id_test_in_course = LearningOrganization::model()->findAllBySql($sql_1);


?>
<h5>
    <?= Yii::t('app',"Grades for exams {course_name}",array('{course_name}'=>$course_name))?>
</h5>
<div class="row-fluid">
    <div class="span3">
        <label id="resp_form">
        <label for="test-name" class="required"><?= Yii::t('app','E-Learning Test'); ?></label>
        <select name="test-name" id="test-name">
            <option value=""><?= Yii::t('app','Choose elearning test'); ?></option>
            <?php
                foreach($all_id_test_in_course as $ac){
                   
                    ?>
            <option value="<?= $ac->idResource; ?>"><?= $ac->title; ?></option>
            <?php
                   
                }
            ?>
        </select>
        </label>
    </div>
   
</div>

<div class="row-fluid" id="lis-elev">
    
</div>

<script>
    $(document).ready(function(){
       var id_course = <?= $id_course; ?>;
       
       $("#test-name").change(function(){
           var test_id = $("#test-name").val();
           $.get('<?= $baseUrl?>/index.php/formalms/default/ShowLisEleve',{id_test:test_id,id_course:id_course},function(data){
                   $('#lis-elev').html(data);
                 });
       });
        
    });
</script>