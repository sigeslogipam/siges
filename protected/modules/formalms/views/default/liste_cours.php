<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$acad = Yii::app()->session['currentId_academic_year'];

$sql_courses = "SELECT c.id, c.academic_period, c.teacher, c.subject, c.room, c.code_cours, c.weight, s.subject_name, s.short_subject_name, p.first_name, p.last_name, r.room_name, r.short_room_name FROM courses c 
INNER JOIN subjects s ON (c.subject = s.id) 
INNER JOIN rooms r ON (c.room = r.id)
INNER JOIN persons p ON (c.teacher = p.id)
WHERE c.academic_period = $acad AND r.id = $id_room"; 

$all_siges_courses = Courses::model()->findAllBySql($sql_courses); 

$room_name = Rooms::model()->findByPk($id_room)->short_room_name; 


?>
<p>
    &nbsp;
</p>
    <h5>
        <?= Yii::t("app","List of course for {room_name}",array('{room_name}'=>$room_name)); ?>
    </h5>

 <div id="persons-grid" class="form">
    <table class="table responsive table-striped table-hover" id="DataTables_Table_0">
        <thead>
            <tr>
                <th>#</th>
                <th><?= Yii::t('app','Subject name')?></th>
                <th><?= Yii::t('app','Room') ?></th>
                <th><?= Yii::t('app','Teacher') ?></th>
                <th><?= Yii::t("app","Code") ?></th>

                <th class="checkbox-column" id="chk">
                    <input class="select-on-check-all" type="checkbox" value="1" name="chk_all" id="check-all">
                </th>
                
            </tr>
        </thead>
        <tbody>
        <?php 
            $k = 1;
            foreach($all_siges_courses as $asc){
                        ?>
                    <tr>
                        <td><?= $k; ?></td>
                        <td><?= $asc->subject0->subject_name; ?></td>
                        <td><?= $asc->room0->short_room_name; ?></td>
                        <td><?= $asc->teacher0->fullName; ?></td>
                        <td><?= $asc->code_cours; ?></td>
                        <td>
                            
                            <input class="on-check" type="checkbox" value="<?= $asc->id; ?>" name="chk" id="<?= $asc->id ?>">
                               
                        </td>
                        
                    </tr>
                    <?php
                    $k++; 
                        }
                    ?>
        </tbody>
    </table>
 </div>

<div class="row" style="text-align: center">
    <a href="#" id="update-course" class="btn btn-warning"><?= Yii::t('app','Update course selection'); ?></a>
</div>

<script>
    var listid = null;
   
   $(document).ready(function(){
         $("#update-course").hide();
         $('.plas-rezilta-migration').hide(); 
         
        function getValueUsingClass(){
            /* declare an checkbox array */
            var chkArray = [];

            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".on-check:checked").each(function() {
                    chkArray.push($(this).val());
            });

            /* we join the array separated by the comma */
            var selected;
            selected = chkArray.join(',') ;

            return selected;
        }
        
        $("#check-all").click(function(){
            
           if ($('#check-all').is(":checked"))
                {
                  $('.on-check').prop('checked',true);
                  listid = getValueUsingClass();
                  $("#update-course").show();
                 
                }else{
                    $('.on-check').prop('checked',false);
                    $("#update-course").hide();
                }
        
                
        });
        
        $('.on-check').click(function(){
            $("#update-course").show();
            listid = getValueUsingClass();
            if(!$(".on-check").is(":checked") && !$('#check-all').is(":checked")){
                $("#update-course").hide();
            }
        });
        
         $('#update-course').click(function(){
              $.get('<?= $baseUrl?>/index.php/formalms/default/makeCourseUpdate',{course:listid},function(data){
               // $('.plas-rezilta-update').show();   
                //$('#rezilta-migration').html(data);
                //wait(5000);
                setTimeout(function(){  location.reload(); }, 5000);
                
            });
               
             //  window.open("<?= $baseUrl; ?>/index.php/kindergarden/kinderEvaluation/makepdf/room/"+listid);
        
            });
           
            
            
         $('#DataTables_Table_0').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  
                    
                  
                 
                ]

            });
        
        
        
    
    
    
    });
    
</script>