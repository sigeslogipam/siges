<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$this->title =  Yii::t('app',"Migrate Rooms");
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$acad = Yii::app()->session['currentId_academic_year'];

$all_siges_rooms = Rooms::model()->findAll(array('order'=>'room_name'));

?>

<div class="row-fluid flm">
    <div id="dash">
        <div class="span1">

        </div>
        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate courses to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('recordInfraction/create'));

                   ?>
         </div>
        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate students to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('recordInfraction/create'));

                   ?>
         </div>
        <div class="span4">

                  <?php

                 $images = '<i class="fas fa-undo">&nbsp;'.Yii::t('app','Back').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('dashboard/index'));

                   ?>
         </div>

    </div>
</div>

<div class="row-fluid tit_h1">
    <h1>
        <?= Yii::t('app','Migrate classroom to E-learning');?>
    </h1>
</div>

<div class="row">
    <div class="span1">

    </div>
    <div class="span10 plas-rezilta-migration" id="plas-rezilta-migration">
        <div class="alert alert-success alert-dismissible plas-rezilta-migration">
          <button type="button" class="close" data-dismiss="alert">&times;</button>
          <div id="rezilta-migration" class="plas-rezilta-migration"></div>
        </div>
    </div>
    <div class="span1">

    </div>
</div>

<div class='row-fluid'>
    <div class='span12'>
        <div id="persons-grid" class="form">
            <table class="table responsive table-striped table-hover" id="DataTables_Table_0">
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= Yii::t('app','Room Name')?></th>
                        <th><?= Yii::t('app','Short Room Name') ?></th>
                        <th class="checkbox-column" id="chk">
                            <input class="select-on-check-all" type="checkbox" value="1" name="chk_all" id="check-all">
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        $k = 1;
                        foreach($all_siges_rooms as $asr){
                            ?>
                    <tr>
                        <td><?= $k; ?></td>
                        <td><?= $asr->room_name; ?></td>
                        <td><?= $asr->short_room_name; ?></td>
                        <td>
                            <?php
                                if(!isRoomMigrate("/".$asr->short_room_name)){
                                    ?>
                            <input class="on-check" type="checkbox" value="<?= $asr->id; ?>" name="chk" id="<?= $asr->id ?>">
                                <?php } else {?>

                                <?php } ?>
                        </td>
                        <td>
                            <?php
                                if(isRoomMigrate("/".$asr->short_room_name)){
                                    ?>
                                    <i class="fas fa-toggle-on" style="color: green"></i>

                            <?php
                                }else{
                                    ?>
                                    <i class="fas fa-toggle-off" style="color: red"></i>
                        <?php
                                }
                            ?>
                        </td>
                    </tr>
                    <?php
                    $k++;
                        }
                    ?>
                </tbody>
            </table>

        </div>
    </div>
</div>

<div class="row" style="text-align: center">
    <a href="#" id="migrate-class-room" class="btn btn-warning"><?= Yii::t('app','Migrate classroom selection'); ?></a>
</div>

<script>
    var listid = null;

    $(document).ready(function(){
         $("#migrate-class-room").hide();
         $('.plas-rezilta-migration').hide();

        function getValueUsingClass(){
            /* declare an checkbox array */
            var chkArray = [];

            /* look for all checkboes that have a class 'chk' attached to it and check if it was checked */
            $(".on-check:checked").each(function() {
                    chkArray.push($(this).val());
            });

            /* we join the array separated by the comma */
            var selected;
            selected = chkArray.join(',') ;

            return selected;
        }

        $("#check-all").click(function(){

           if ($('#check-all').is(":checked"))
                {
                  $('.on-check').prop('checked',true);
                  listid = getValueUsingClass();
                  $("#migrate-class-room").show();

                }else{
                    $('.on-check').prop('checked',false);
                    $("#migrate-class-room").hide();
                }


        });

        $('.on-check').click(function(){
            $("#migrate-class-room").show();
            listid = getValueUsingClass();
            if(!$(".on-check").is(":checked") && !$('#check-all').is(":checked")){
                $("#migrate-class-room").hide();
            }
        });

         $('#migrate-class-room').click(function(){
              $.get('<?= $baseUrl?>/index.php/formalms/default/makeRoomMigration',{room:listid},function(data){
                $('.plas-rezilta-migration').show();
                $('#rezilta-migration').html(data);
                //wait(5000);
                setTimeout(function(){  location.reload(); }, 5000);

            });

             //  window.open("<?= $baseUrl; ?>/index.php/kindergarden/kinderEvaluation/makepdf/room/"+listid);

            });






    });
</script>
