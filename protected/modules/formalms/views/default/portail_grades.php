<?php
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$acad = Yii::app()->session['currentId_academic_year'];

$level_data = Rooms::model()->findAllBySql("SELECT DISTINCT r.id, r.room_name, r.short_room_name  FROM rooms r  INNER JOIN room_has_person rhp ON (rhp.room = r.id) WHERE rhp.academic_year = $acad order by r.room_name ASC");

?>
<div class="row-fluid flm">
    <div id="dash">
        <div class="span1">

        </div>

        <div class="span4">
                  <?php

                 $images = '<i class="fas fa-magic">&nbsp;'.Yii::t('app','Migrate course to E-learning').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('default/migrateCourses'));

                   ?>
         </div>
        <div class="span4">

                  <?php

                 $images = '<i class="fas fa-undo">&nbsp;'.Yii::t('app','Back').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('dashboard/index'));

                   ?>
         </div>

    </div>
</div>

<div class="row-fluid tit_h1">
    <h1>
        <?= Yii::t('app','Migrate grades from E-learning to SIGES');?>
    </h1>
</div>

<?php
    if(isset($_GET["course"])){
        $course_pk  = $_GET["course"];
        $course_ = Courses::model()->findByPk($course_pk);
        $course_name = $course_->subject0->subject_name;
        ?>
    <div class="row-fluid">
        <h2>
            <?= Yii::t("app","Elearning grades migrate with success for {course_name}",array("{course_name}"=>$course_name)); ?>
        </h2>
    </div>
<?php
    }
?>

<div class="row-fluid">
    <div class="span3 lis_la">
        <label id="resp_form">
        <label for="cat-name" class="required"><?= Yii::t('app','Room name'); ?></label>
        <select name="room" id="room">
            <option value=""><?= Yii::t('app','Choose room'); ?></option>
            <?php
                foreach($level_data as $ld){
                    if(isRoomMigrate("/".$ld->short_room_name)){
                    ?>
            <option value="<?= $ld->id; ?>"><?= $ld->room_name; ?></option>
            <?php
                    }
                }
            ?>
        </select>
        </label>
    </div>

</div>

<div class="row-fluid">
    <div class="span3 kou_elearning" id="list-cours-elearning">

    </div>
    <div class="span8 kou_rel_note"  id="rezilta-note">

    </div>
</div>

<script>
    $(document).ready(function(){
       $("#room").change(function(){
           var room_id = $("#room").val();
           $.get('<?= $baseUrl?>/index.php/formalms/default/ShowComboCourse',{room_id:room_id},function(data){
                   $('#list-cours-elearning').html(data);
                 });
       });



    });
</script>
