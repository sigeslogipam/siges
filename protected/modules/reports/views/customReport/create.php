    <div id="dash">
            <div class="span3">
                <h2><?php echo Yii::t('app','List custom report');  ?></h2>
            </div>
        
     <div class="span3">       
        <div class="span4">
                  <?php
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
                 $images = '<i class="fa fa-list">&nbsp;'.Yii::t('app','List report').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('customReport/list?from1=rpt&cat='.$cat));
                 
                   ?>
        </div>
    </div>
  
    </div>
<div style="clear:both"></div>

    
<div class="row-fluid">
    <div class="span12 well">
        <?php
            $form=$this->beginWidget('CActiveForm', array(
                'id'=>'custom-report-new',
                'enableAjaxValidation'=>true,
                )); 
            ?>
        
        
        <div class="row-fluid">
            <div class="span3">
                    <label id="resp_form">
                        <label><?php echo Yii::t('app','Report title'); ?></label>
                        <input id="report_title" type="text" name="report_title" placeholder="<?php echo Yii::t('app','Report title'); ?>"/>
                    </label>
            </div>
            <!--
            <div class="span3">
                    <label id="resp_form">
                        <label><?php echo Yii::t('app','Academic year'); ?></label>
                        
                        <?php
                        $criteria = new CDbCriteria(array('condition'=>'is_year=1'));
                       
                        echo CHtml::dropDownList('academic_year','', CHtml::listData(AcademicPeriods::model()->findAll($criteria),'id','name_period'),
                            array(
                            'prompt'=>Yii::t('app','-- Please select year --'),    
                            'ajax' => array(
                            'type'=>'POST', //request type
                            'url'=>CController::createUrl('customReport/loadAcademicPeriod'), //url to call.
                            
                            'update'=>'#period_id', //selector to update
                            
                            ))); 
                        ?>
                        
                    </label>
                </div>
            -->
            <div class="span3">
                    <label id="cmb_shift">
                        <label><?php echo Yii::t('app','Category'); ?></label>
                           
                        
                        <?php
                        $criteria = new CDbCriteria(array('order'=>'categorie_name'));
                       
                        echo CHtml::dropDownList('cat_id','', CHtml::listData(RptCustomCat::model()->findAll($criteria),'id','categorie_name'),
                            array(
                            'prompt'=>Yii::t('app','-- Please select categorie --'), 
                              
                            'ajax' => array(
                            'type'=>'POST', //request type
                            'url'=>CController::createUrl('customReport/loadRoomByShift'), //url to call.
                            
                            'update'=>'#room_name', //selector to update
                          
                            ))); 
                        ?>
                    </label>
            </div>
        </div>
        <div class="row-fluid">
            <div class="span12">
                    <label id="resp_form">
                        
                        <label><?php echo Yii::t('app','Code SQL'); ?></label>
                        <div class="alert alert-primary"><?= Yii::t('app','Type your SQL code in the box bellow, use "AS" to specify column labelling. <br/>If you want to make your SQL code parametrable please use this format "{%param1%}". <br/>If your parameter is two words use '
                                . '"_", example {%param_avec_plusieurs_mots%} !'); ?></div>
                        <textarea id="report_sql" style="width: 1300px; height: 200px;" name="report_sql">
                        </textarea>
                    </label>
                </div>
        </div>
        
        
        
        <div class="span2">
            
        </div>
        
        <div class="span2">
            <p></p>
            <button name="btnSave" class="btn btn-warning"><?= Yii::t('app','Save report'); ?></button>
            <?php
            /*
            echo CHtml::ajaxSubmitButton(
                    Yii::t('app','Save report'),
                    array('customReport/saveReport'),
                    array(
                       'update'=>'#list_zone',
                        ), 
                    array('class' => "btn btn-primary",'name'=>'btnPreview','id'=>'btnPreview')
                    );
             * 
             */
            ?>
           
        </div>
        <input type="hidden" name="parameters" id="parameters"/>
        <input type="hidden" name="parameter" id="parameter" />
        <input type="hidden" name="variable" id="variable"/>
       
        <div class="span2">
            
        </div>
        
    </div>
    </div>
    
    <?php $this->endWidget(); ?>
</div>

<div id="list_zone"></div>

<script>
    $(document).ready(function(){
        $("#btn-config-parameter").hide(); 
    }); 
    
    
   
    
    $("#report_sql").keyup(function(){
        var valeur = $('#report_sql').val();
        var result_param = getFromBetween.get(valeur,"{{","}}");
        var result_variable = getFromBetween.get(valeur,"{%","%}");
        if (result_variable == undefined || result_variable.length == 0) {
                 
            }else{
                // $("#btn-config-parameter").show();
                
                /*
                var div = document.createElement('div');

                div.className = 'param-config';
                
            for(var i = 0; i<result_variable.length; i++){
                 div.innerHTML += '<a href="#" class="btn btn-success param" data-paramvalue='+result_variable[i]+'><span class="fa fa-config">'+result_variable[i]+'</span></a>';
            }
            

                document.getElementById('param-config').appendChild(div);
                
                */
               
                $("#variable").val(result_variable);
                
            }
        
       
        
        $("#parameter").val(result_param); 
         
        
           /* 
            $.get('detect-parameter',{value : valeur},function(data){
                $('#param_zone').html(data);
           
            });
            */
    });
    
    var getFromBetween = {
    results:[],
    string:"",
    getFromBetween:function (sub1,sub2) {
        if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var SP = this.string.indexOf(sub1)+sub1.length;
        var string1 = this.string.substr(0,SP);
        var string2 = this.string.substr(SP);
        var TP = string1.length + string2.indexOf(sub2);
        return this.string.substring(SP,TP);
    },
    removeFromBetween:function (sub1,sub2) {
        if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return false;
        var removal = sub1+this.getFromBetween(sub1,sub2)+sub2;
        this.string = this.string.replace(removal,"");
    },
    getAllResults:function (sub1,sub2) {
        // first check to see if we do have both substrings
        if(this.string.indexOf(sub1) < 0 || this.string.indexOf(sub2) < 0) return;

        // find one result
        var result = this.getFromBetween(sub1,sub2);
        // push it to the results array
        this.results.push(result);
        // remove the most recently found one from the string
        this.removeFromBetween(sub1,sub2);

        // if there's more substrings
        if(this.string.indexOf(sub1) > -1 && this.string.indexOf(sub2) > -1) {
            this.getAllResults(sub1,sub2);
        }
        else return;
    },
    get:function (string,sub1,sub2) {
        this.results = [];
        this.string = string;
        this.getAllResults(sub1,sub2);
        return this.results;
    }
};

/*
window.onload = function() {
/*
  var mime = 'text/x-mariadb';
  // get mime type
  if (window.location.href.indexOf('mime=') > -1) {
    mime = window.location.href.substr(window.location.href.indexOf('mime=') + 5);
  }
 */
/*
  window.editor = CodeMirror.fromTextArea(document.getElementById('report_sql'), {
    mode: 'text/x-mariadb',
    indentWithTabs: true,
    smartIndent: true,
    lineNumbers: true,
    matchBrackets : true,
    autofocus: true
   extraKeys: {"Tab": "autocomplete"},
     
    hintOptions: {tables: {
      users: ["name", "score", "birthDate"],
      countries: ["name", "population", "size"]
    }}
   
  });
};
*/

/*
    $(function () {
        initSqlEditor();
        initAutoComplete();
    });

    // init sql editor
    function initSqlEditor() {

        var editor = CodeMirror.fromTextArea(document.getElementById('report_sql'), {
            autofocus: true,
            extraKeys: {
                "TAB": "autocomplete"
            },
            hint: CodeMirror.hint.sql,
            lineNumbers: true,
            mode: 'text/x-hive',
            lineWrapping: true,
            theme: 'eclipse',
        });

        editor.on('keyup', function(editor, event){
            // type code and show autocomplete hint in the meanwhile
            CodeMirror.commands.autocomplete(editor);
        });
    }

    /**
     * Init autocomplete for table name and column names in table.
     */
     /*
    function initAutoComplete() {

        CodeMirror.commands.autocomplete = function (cmeditor) {

            CodeMirror.showHint(cmeditor, CodeMirror.hint.sql, {

                // "completeSingle: false" prevents case when you are typing some word
                // and in the middle it is automatically completed and you continue typing by reflex.
                // So user will always need to select the intended string
                // from popup (even if it's single option). (copy from @Oleksandr Pshenychnyy)
                completeSingle: false,

                // there are 2 ways to autocomplete field name:
                // (1) table_name.field_name (2) field_name
                // Put field name in table names to autocomplete directly
                // no need to type table name first.
                tables: {
                    "table1": ["col_A", "col_B", "col_C"],
                    "table2": ["other_columns1", "other_columns2"],
                    "col_A": [],
                    "col_B": [],
                    "col_C": [],
                    "other_columns1": [],
                    "other_columns2": [],
                }
            });
        }
    }
*/


</script>