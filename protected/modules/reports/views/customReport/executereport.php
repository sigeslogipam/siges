<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$a_variables = explode(",", $rpt->variables);
$re_write_variable = array();
for($i=0;$i<count($a_variables);$i++){
    $re_write_variable[$i] = "{%".$a_variables[$i]."%}";
}

$str_sql =  $rpt->data;
$str_final = str_replace($re_write_variable, $a_variables, $str_sql);

?>

<?php
    if($rpt->variables == NULL || $rpt->variables == ""){
?>
<div class="row-fluid">
    <div class="row-fluid">
    <div id="dash">
        <div class="row-fluid">
    <div class="span10">

    <h2><?= Yii::t('app','{nameReport}',array('{nameReport}'=>$rpt->title));  ?></h2>
    </div>
    <div class="span2">


                  <?php
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
                 $images = '<i class="fa fa-list">&nbsp;'.Yii::t('app','List report').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('customReport/list?from1=rpt&cat='.$cat));

                   ?>


    </div>
        </div>
 </div>
</div>
    <?php
    // Pour afficher les dates au format francais
    $locale = "fr_FR";
    if(isset($_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']) && $_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']=="fr" ){
        $locale = "fr_FR";
    }else{
       $locale = 'fr_FR';
    }
    Yii::app()->getDb()->createCommand("SET lc_time_names = '$locale'")->execute();
    $name = Yii::app()->getDb()->createCommand($rpt->data)->queryAll();
    $clean_data = array();
    $i=0;
    foreach ($name as $n){
        $clean_data[$i] = $n;
        $i++;
    }

    ?>

    <?php if (count($clean_data) > 0): ?>

<table class='table table-striped table-bordered table-hover dataTables-exemple'>
  <thead>
    <tr>
      <th><?php echo implode('</th><th>', array_keys(current($clean_data))); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($clean_data as $row): array_map('htmlentities', $row); ?>

    <tr>
      <td><?php echo implode('</td><td>', $row); ?></td>
    </tr>

<?php endforeach; ?>
  </tbody>
</table>
<?php endif;

?>
</div>
<div class="row-fluid">
    <?php }else{
        ?>
    <div id="dash">
    <div class="span10">

    <h2><?= Yii::t('app','{nameReport}',array('{nameReport}'=>$rpt->title));  ?></h2>
    </div>
    <div class="span3">
        <div class="span4">
                  <?php
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
                 $images = '<i class="fa fa-list">&nbsp;'.Yii::t('app','List report').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('customReport/list?from1=rpt&cat='.$cat));

                   ?>
        </div>
    </div>
 </div>
<div class="run-re">
    <?php
       $form=$this->beginWidget('CActiveForm', array(
                'id'=>'run-report',
                'enableAjaxValidation'=>true,
        ));
        $decodedText = html_entity_decode($rpt->setup_variable);
        $myArray = json_decode($decodedText, true);
        $data_type = $myArray['data_type'];
        $param_value = $myArray['param_value'];
       ?>
    <table>
        <tr>

    <?php
        for($i = 0; $i<count($a_variables); $i++){
            ?>
        <td>
        <?php
            switch ($data_type[$i]){
                case "txt":
                {

                    ?>
                    <label><b><?= ucfirst(str_replace('_',' ',$a_variables[$i])); ?></b></label>
                    <input name="<?= 'field'.$i ?>" type="text"/>

    <?php

                }
                break;
                case "date":{
                    ?>
                    <label><b><?= ucfirst(str_replace('_',' ',$a_variables[$i])); ?></b></label>
                    <?php
                        $this->widget('zii.widgets.jui.CJuiDatePicker',
				array(
                                // 'model'=>'$modelStudentOtherInfo',
                                 'name'=>'field'.$i,
                                 'language'=>'fr',
                                 'value'=>'',
                                 'htmlOptions'=>array('size'=>30, 'style'=>'width:25% !important'),
                                         'options'=>array(
                                         'showButtonPanel'=>true,
                                         'changeYear'=>true,
                                         'dateFormat'=>'yy-mm-dd',
                                         'yearRange'=>'1900:2100',
                                         'changeMonth'=>true,
                                          'showButtonPane'=>true,
                                          'dateFormat'=>'yy-mm-dd',
                                         ),
                                 )
                         );
                    ?>

    <?php
                }
                break;
                case "static-combobox":{

                    ?>
                    <label><b>
                        <?php
                            if($param_value['static-combo'.$i]!=NULL){
                            echo ucfirst(str_replace('_',' ',$a_variables[$i]));
                    ?>
                    </b></label>

    <?php
                        $pair_index_value = explode(",",$param_value['static-combo'.$i]);
                        echo "<select name=field$i>";
                            for($j=0;$j<count($pair_index_value); $j++){
                                $list_final = explode(":",$pair_index_value[$j]);
                                echo "<option value='$list_final[0]'>$list_final[1]</option>";
                            }
                        echo "</select>";


                            }
                }
                break;
                case "dynamic-combobox":{
                    ?>
                    <label><b>
                        <?php
                            if($param_value['dynamic-combo'.$i]!=NULL){
                            echo ucfirst(str_replace('_',' ',$a_variables[$i]));
                    ?>
                    </b></label>

                <?php
                    $str_sql = $param_value['dynamic-combo'.$i];
                    preg_match('~T(.*?)F~', $str_sql, $output);
                    $title = explode(",",$output[1]);

                    $data = Yii::app()->getDb()->createCommand($str_sql)->queryAll();
                    $clean_ = array();
                    $k=0;

                    foreach($data as $n){
                        $clean_[$k] = $n;
                        $k++;

                    }
                   echo "<select name='field$i'>";
                        foreach($clean_ as $c){
                            $tag_id = str_replace(' ','',$title[0]);
                            $tag_value = str_replace(' ','',$title[1]);
                        echo "<option value='".$c[$tag_id]."'>".$c[$tag_value]."</option>";

                        }

                      echo "</select>";
                    }
                }
            }
            ?>
    </td>
            <?php
        }


        ?>

    <td>


                    <?php
                    echo "<br/>";
            echo CHtml::ajaxSubmitButton(
                    Yii::t('app','Run report'),
                    array('customReport/runreport?id='.$id),
                    array(
                       'update'=>'#roule-rapo',
                        ),
                    array('class' => "btn btn-primary",'name'=>'btnPreview','id'=>'btnPreview')
                    );
            ?>
    </td>
     </tr>
    </table>


    <?php $this->endWidget(); ?>

<?php

    } ?>



</div>
<div id="roule-rapo">

</div>
</div>
<script>
   $(document).ready(function(){

            $('.dataTables-exemple').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: 'Students List'},
                   // {extend: 'pdf', title: 'Students List'},

                  /*  {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });

            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>
