<div class="row-fluid">
    <div id="dash">
    <h2><?= Yii::t('app','{nameReport}',array('{nameReport}'=>$model->findByPk($id)->title));  ?></h2>
    </div>
</div>

<table>
    <thead>
        <tr>
            <th><?php echo Yii::t('app','Report title'); ?></th>
            <th><?php echo Yii::t('app','Category'); ?></th>
            <th><?php echo Yii::t('app','Create date'); ?></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <?php
            $rpt = $model->findByPk($id);
            $cat = new RptCustomCat(); 
        ?>
        <tr>
            <td><?= $rpt->title; ?></td>
            <td><?= $cat->findByPk($rpt->categorie)->categorie_name; ?></td>
            <td><?= $rpt->create_date; ?></td>
            <td>
                <?php if($rpt->variables != "" || $rpt->variables != NULL){ ?>
                <a href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/configparam?id='.$rpt->id.'&cat=stud&from1=rpt'; ?>" title="<?php echo Yii::t('app','Parameter configuration'); ?>"><i class="fa fa-gear"></i></a>
                <?php } ?>
            </td>
            <td><?php if(($rpt->setup_variable != NULL || $rpt->setup_variable != "") || ($rpt->variables==NULL || $rpt->variables=="")) {?>
                <a href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/executereport?id='.$rpt->id.'&cat=stud&from1=rpt'; ?>" title="<?php echo Yii::t('app','Execute Report'); ?>"><i class="fa fa-play"></i></a>
           <?php 
            } ?></td>
            <?php
                if(Yii::app()->user->profil=="Admin"){
                 ?> 
                <td><a Onclick="return ConfirmDelete()" href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/delete?id='.$rpt->id.'&cat=stud' ?>" title="<?php echo Yii::t('app','Delete report'); ?>"><i class="fa fa-trash-o"></i></a></td>
             <?php    
                }
                ?>
        </tr>
</table>

<script>
    function ConfirmDelete()
        {
          var x = confirm("<?php echo Yii::t('app','Are you sure you want to delete?'); ?>");
          if (x)
              return true;
          else
            return false;
        }
</script>