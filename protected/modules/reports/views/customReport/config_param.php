<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo $json_value; 

?>

<div class="row-fluid">
    <div id="dash">
    <h2><?= Yii::t('app','Setup parameter(s) for report << {nameReport} >>',array('{nameReport}'=>$model->findByPk($id)->title));  ?></h2>
    </div>
    <?php 
        
       //echo $json_value; 
       $param_value = explode(",",$model->findByPk($id)->variables); 
    
            $form=$this->beginWidget('CActiveForm', array(
                'id'=>'config-param',
                'enableAjaxValidation'=>true,
                )); 
           
            
          
   ?>
    <p><?= Yii::t('app','Choose data type for the parameter(s) below : '); ?></p>
   
    
    <table>
        
   
    
        <?php 
        for($i=0; $i < count($param_value); $i++){
            
        ?>
        <tr>
            <td>
        <?php  echo '<i class="config">'.$param_value[$i].'</i>'; ?> 
            </td>
       
            <td>
        <select name="<?php  echo $param_value[$i]; ?>" id="<?php  echo $param_value[$i]; ?>" class="konfig">
            <option value="txt"><?= Yii::t('app','Text');?></option>
            <option value="date"><?= Yii::t('app','Date'); ?></option>
            <option value="static-combobox"><?= Yii::t('app','Static combobox'); ?></option>
            <option value="dynamic-combobox"><?= Yii::t('app','Dynamic combobox');?></option>
        </select>
            </td>
            <td class="static">
                <label class="static-combo<?=$i?>"><?= Yii::t('app','Please build your dropdownlist by typing key:value separate by coma. Example : 1:Enable, 2:Disabled'); ?></label>
                <input id="static-combo<?=$i?>" name="static-combo<?=$i?>" type="text"/>
                <label class="dynamic-combo<?=$i?>"><?= Yii::t('app','Please build your dropdownlist by typing your SQL code.'); ?></label>
                <input id="dynamic-combo<?=$i?>" name="dynamic-combo<?=$i?>" type="text"/>
                
            </td>
        
    </tr>
        
        
        
    <?php         
            
        }
        ?>
     </table>    
        <div>
            <button hre="#" class="btn btn-primary" id="btn-save-paramter" name="btn-save-parameter"><?= Yii::t('app','Save Configuration parameter'); ?></button>
            
        </div>
    
   <?php $this->endWidget(); ?>  
    
    <div id="data-param">
        
    </div>
  
</div>

<script>
    <?php 
        for($i=0; $i < count($param_value); $i++){
            
    ?>
    $(document).ready(function(){
        $("#static-combo<?=$i?>").hide(); 
        $(".static-combo<?=$i?>").hide();
        $(".dynamic-combo<?=$i?>").hide();
        $("#dynamic-combo<?=$i?>").hide();
    });
    
    $("#<?php  echo $param_value[$i]; ?>").click(function(){
        var combo_value = $("#<?php  echo $param_value[$i]; ?>").val(); 
        if(combo_value === "static-combobox"){
            $("#static-combo<?= $i ?>").show();
            $(".static-combo<?= $i ?>").show();
            $(".dynamic-combo<?=$i?>").hide();
            $("#dynamic-combo<?=$i?>").hide();
        }else if(combo_value==="dynamic-combobox"){
            $("#static-combo<?= $i ?>").hide();
            $(".static-combo<?= $i ?>").hide();
            $(".dynamic-combo<?=$i?>").show();
            $("#dynamic-combo<?=$i?>").show();
        }else{
            $("#static-combo<?= $i ?>").hide();
            $("#dynamic-combo<?=$i?>").hide();
            $(".static-combo<?= $i ?>").hide();
            $(".dynamic-combo<?=$i?>").hide();
        }
    });
        <?php } ?>
</script>