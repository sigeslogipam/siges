<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$modelCat = new RptCustomCat(); 

?>


    <div id="dash">
            <div class="span3">
                <h2><?php echo Yii::t('app','List custom report');  ?></h2>
            </div>
        
     <div class="span3">    
         <?php if(Yii::app()->user->profil=="Admin") {?>
         <div class="span4">
                  <?php
                  if(isset($_GET['cat'])){
                      $cat = $_GET['cat'];
                  }
                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Create report').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('customReport/create?cat='.$cat.'&from1=rpt'));
                 
                   ?>
        </div> 
         <?php } ?>
     </div>
  
    </div>
<div style="clear:both"></div>

    
<div class="row-fluid">
<?php
    echo $this->renderPartial('//layouts/navCustomReport',NULL,true);	
 ?>
</div>

<?php 
    if(isset($_GET['cat'])){
        $cat = $_GET['cat'];
        $id_cat = RptCustomCat::model()->findBySql("SELECT id FROM rpt_custom_cat WHERE cat = '$cat'")->id; 
        $data_rpt = $model->findAllBySql("SELECT * FROM rpt_custom WHERE categorie = $id_cat ORDER BY title ASC"); 
    }
?>
<div class="row-fluid">
    
    <div class="span12">
        <table> 
            <thead>
                <tr>
                    <th><?php echo Yii::t('app','Report title'); ?></th>
                    <?php if(Yii::app()->user->name==bagdor()[1]) { ?>
                    <th><?php echo Yii::t('app','Create date'); ?></th>
                    <?php } ?>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
            </thead>
        <?php 
            foreach($data_rpt as $d){
                $arr_ = json_decode($d->data,true);
                $description = $arr_['report_description'];
                
                
                
        ?>
            <tr>    
                <td><a href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/executereport?id='.$d->id.'&cat='.$cat.'&from1=rpt'; ?>"> <?php echo $d->title; ?></a></td>
                <?php if(Yii::app()->user->name==bagdor()[1]) {?>
                <td><?php echo $d->create_date; ?></td>
                <?php } ?>
                <td>
                    <?php if($d->variables != NULL || $d->variables !="") {?>
                    <?php if(Yii::app()->user->name==bagdor()[1]) {?>
                    <a href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/configparam?id='.$d->id.'&part=stud&from1=rpt'; ?>" title="<?php echo Yii::t('app','Parameter configuration'); ?>"><i class="fa fa-gear"></i></a>
                    <?php } ?>
                    <?php } ?>
                </td>
                <td>
                    <?php if(($d->setup_variable != NULL || $d->setup_variable != "") || ($d->variables==NULL || $d->variables=="")) {?>
                    <a href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/executereport?id='.$d->id.'&cat='.$cat.'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Run report'); ?>"><i class="fa fa-play"></i></a></td>
                    <?php } ?>
                <td>
                    <?php
                      if(Yii::app()->user->name==bagdor()[1]){
                    ?> 
                    <a href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/updatereport?id='.$d->id.'&cat='.$cat.'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Update report'); ?>"><i class="fa fa-edit"></i></a>
                      <?php } ?>
                </td>
                <?php
                if(Yii::app()->user->name==bagdor()[1]){
                 ?> 
                <td><a Onclick="return ConfirmDelete()" href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/delete?id='.$d->id.'&cat='.$cat; ?>" title="<?php echo Yii::t('app','Delete report'); ?>"><i class="fa fa-trash-o"></i></a></td>
             <?php    
                }
                ?>
            </tr>
            <?php    
                 
             }
             
             if($_GET['cat']=='stud')
             {
                 
            ?>
               <tr>    
                    <td><a href="<?php echo Yii::app()->baseUrl.'/index.php/academic/persons/classSetup?cat='.$cat.'&from1=rpt'; ?>"> <?php echo Yii::t('app','Class Setup List'); ?></a></td>
                    
                    <td><?php  ?></td>
                    
                    <td>
                        
                    </td>
                    <td>
                        
                    <td>
                        
                    </td>
                    
              </tr>
            
            
            <?php
             }
        ?>
        </table>    
      
    </div>
    
</div>

<script>
    function ConfirmDelete()
        {
          var x = confirm("<?php echo Yii::t('app','Are you sure you want to delete?'); ?>");
          if (x)
              return true;
          else
            return false;
        }
</script>