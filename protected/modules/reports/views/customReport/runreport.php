<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo "TESTY";

//echo $post_json;

$a_variables = explode(",", $rpt->variables); 
$re_write_variable = array(); 
for($i=0;$i<count($a_variables);$i++){
    $re_write_variable[$i] = "{%".$a_variables[$i]."%}";  
}

$str_sql =  $rpt->data;
$str_final = str_replace($re_write_variable, $array_param, $str_sql);
?>


<?php
// Pour afficher les dates au format francais 
    $locale = "fr_FR";
    if(isset($_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']) && $_SESSION['74b4c8d8f9bb5e6f4886abddce09d0f7_lang']=="fr" ){
        $locale = "fr_FR";
    }else{
       $locale = 'fr_FR';
    }
    Yii::app()->getDb()->createCommand("SET lc_time_names = '$locale'")->execute();
$name = Yii::app()->getDb()->createCommand($str_final)->queryAll();
    $clean_data = array();
    $i=0;
    foreach ($name as $n){
        $clean_data[$i] = $n;
        $i++;
    }
    
   
    ?>
<?php if (count($clean_data) > 0): ?>
<?php if(Yii::app()->user->name == bagdor()[1]) { ?>
<span class="fa fa-database alert alert-info alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?= $str_final; ?>
</span>
<?php } ?>
<table class='table table-striped table-bordered table-hover dataTables-exemple'>
  <thead>
    <tr>
      <th><?php echo implode('</th><th>', array_keys(current($clean_data))); ?></th>
    </tr>
  </thead>
  <tbody>
<?php foreach ($clean_data as $row): array_map('htmlentities', $row); ?>
      
    <tr>
      <td><?php echo implode('</td><td>', $row); ?></td>
    </tr>
   
<?php endforeach; ?>
  </tbody>
</table>
<?php endif;

?>
   
<script>
    $(document).ready(function(){
      
            $('.dataTables-exemple').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: 'siges_custom_report'},
                  
                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>

