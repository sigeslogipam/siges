<table>
    <thead>
        <tr>
            <th><?php echo Yii::t('app','Report title'); ?></th>
            <th><?php echo Yii::t('app','Category'); ?></th>
            <th><?php echo Yii::t('app','Create date'); ?></th>
            <th></th>
            <th></th>
            <th></th>
        </tr>
        <?php
            $rpt = $model->findByPk($id);
            $cat = new RptCustomCat(); 
            if(isset($_GET['cat'])){
                $cate = $_GET['cat'];
            }
        ?>
        <tr>
            <td><?= $rpt->title; ?></td>
            <td><?= $cat->findByPk($rpt->categorie)->categorie_name; ?></td>
            <td><?= $rpt->create_date; ?></td>
            <td><a href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/configparam?id='.$rpt->id.'&cat='.$cate.'&from1=rpt'; ?>" title="<?php echo Yii::t('app','Parameter configuration'); ?>"><i class="fa fa-gear"></i></a></td>
            <?php
                if(Yii::app()->user->profil=="Admin"){
                 ?> 
                <td><a Onclick="return ConfirmDelete()" href="<?php echo Yii::app()->baseUrl.'/index.php/reports/customReport/delete?id='.$rpt->id; ?>" title="<?php echo Yii::t('app','Delete report'); ?>"><i class="fa fa-trash-o"></i></a></td>
             <?php    
                }
                ?>
        </tr>
</table>

<script>
    function ConfirmDelete()
        {
          var x = confirm("<?php echo Yii::t('app','Are you sure you want to delete?'); ?>");
          if (x)
              return true;
          else
            return false;
        }
</script>