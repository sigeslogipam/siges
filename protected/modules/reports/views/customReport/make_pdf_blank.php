<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$format_paper_ = infoGeneralConfig('size_paper_kinder');
 if($format_paper_ == "Letter"){
     $format_paper = array(216,356);
 }elseif($format_paper_=="Legal"){
     $format_paper = array(216,279); 
 }else{
     $format_paper = array(216,279);
 }
 
 $orientation_paper_ = infoGeneralConfig('orientation_paper_kinder');
 if($orientation_paper_ == "P"){
     $orientation_paper = "P";
 }elseif($orientation_paper_== "L"){
     $orientation_paper = "L";
 }else{
     $orientation_paper = "P";
 }
 
 $mpdf = new \Mpdf\Mpdf(array(
        'margin_top' =>35,
	'margin_left' =>10 ,
	'margin_right' =>10,
        'margin_bottom'=>10,
	//'mirrorMargins' => true,
        'mode' => 'utf-8', 
        'format' =>$format_paper,
        'orientation'=>$orientation_paper
    ));
 
$mpdf->SetHTMLHeader($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLHeader($this->renderPartial('entete-pdf',array(
     'room'=>$room,
     'acad'=>$acad
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);

$mpdf->SetColumns(1,'J',5);
$mpdf->keepColumns = true;

$mpdf->WriteHTML($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($this->renderPartial('data-pdf',array(
     'room'=>$room,
    'all_students'=>$all_students,
    'all_periods'=>$all_periods
    
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);

$room_name = Rooms::model()->findByPk($room)->short_room_name;

$mpdf->Output("$room_name.pdf",\Mpdf\Output\Destination::INLINE);
