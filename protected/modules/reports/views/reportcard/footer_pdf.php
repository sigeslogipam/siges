

<div style="font-size: 8px;">
    <span style="font-size: 8px; "><i> * <?= Yii::t('app','Cadre d\'interprétation de la performance de l\'élève')?></i></span>
    <table cellpadding="1" class="table table-striped table-bordered" style="font-size: 8px; width:20%; float: left; border-collapse: collapse; ">
        <tr>
            <td class="th_legende"><?= Yii::t('app','Range') ?></td><td class="th_legende"><?= Yii::t('app','Stage') ?></td><td class="th_legende"><?= Yii::t('app','Appreciation') ?></td><td class="td_space"></td><td class="th_legende"><?= Yii::t('app','Range') ?></td><td class="th_legende"><?= Yii::t('app','Stage') ?></td><td class="th_legende"><?= Yii::t('app','Appreciation') ?></td>
        </tr>
        <tr>
            <?php
                   $dataProvider =  AppreciationsGrid::model()->searchBySection($section_id,$acad);
                  if($dataProvider!=null)
                  {
                   $dataProvider = $dataProvider->getData();
                   $i=1;
                   foreach ($dataProvider as $data)
                   {
                       if($i>2)
                           $i=1;
                       
                       if($i==1)
                           echo '<tr>';
                           
                        echo '<td class="td_foot" >'.$data->getAppreciationRange().'</td><td class="td_foot" >'.$data->short_text.'</td><td class="td_foot" >'.$data->comment.'</td>';
                        
                       if($i==1)
                          echo '<td class="td_space"> </td>';
                       
                       if($i==2)  
                        echo '</tr>';
                       
                        $i++;
                   }
                  }
                   
                   
            ?>
        </tr>
    </table>
  
<?php

 //Extract display_created_date
    $display_created_date = infoGeneralConfig('display_created_date');

    if($display_created_date==1)
  echo ' <div style="float:right; font-weight:normal; font-size:10px; margin-top:-10px;">    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.ChangeDateFormat(date('Y-m-d')).'</div>    ';
?>
</div>

