<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<table class="table responsive table-striped table-hover  table-bordered table-condensed" id="DataTables_Table_0">
     <thead>
         <tr>
            <th>#</th>
            <th><?= Yii::t('app','First name')?></th>
            <th><?= Yii::t('app','Last name') ?></th>
            <?php
                foreach($all_period as $ap){
                    ?>
            <th colspan="2"><?= $ap->name_period;?></th>
           
            <?php
                }
            ?>
             <th colspan="2"><?= Yii::t('app','Average'); ?></th>
           
        </tr>
     </thead>
      <tbody>
                            
            <?php 
                $k=1; 
                if($k==1){
                    ?>
          <tr>
              <td></td>
              <td></td>
              <td></td>
              <?php
                    foreach($all_period as $ap){
                        ?>
                 <td>
                     <strong><?= Yii::t('app','Base')?></strong>
                 </td>
                  <td>
                      <strong><?= Yii::t('app','General')?></strong>
                 </td>
                <?php
                    }
                ?>
                 <td>
                     <strong><?= Yii::t('app','Base')?></strong>
                 </td>
                  <td>
                      <strong><?= Yii::t('app','General')?></strong>
                 </td>
          </tr>
          <?php 
                }
                foreach ($all_student as $as){
                    ?>
            <tr>         
                 <td><?= $k; ?></td>
                 <td><?= $as->first_name; ?></td>
                 <td><?= $as->last_name; ?></td>
                 <?php
                    $base_ = array();
                    $not_base_ = array();
                    $j = 0;
                    foreach($all_period as $ap){
                        $base_[$j] = getAverageBaseForStudent($as->id, $ap->id);
                        $not_base_[$j] = getAverageNotBaseForStudent($as->id, $ap->id);
                        ?>
                <td>
                     <?= getAverageBaseForStudent($as->id, $ap->id)?>
                 </td>
                  <td>
                    <?= getAverageNotBaseForStudent($as->id, $ap->id)?>
                 </td>
                <?php
                    $j++;
                    }
                ?>
                 <td>
                   <?=  round(array_sum($base_)/count($base_),2); ?>
                 </td>
                  <td>
                     <?=  round(array_sum($not_base_)/count($not_base_),2); ?>
                 </td>

            </tr>
            <?php 
                $k++;
                }
            ?>
                            
       </tbody>
</table>

<script>
    $(document).ready(function(){
        var room_name = $("#room option:selected").text();
        room_name = room_name.replace(/\s/g, '');
       $("#send_excel").click(function(){
            $("#DataTables_Table_0").table2excel({
                exclude: ".noExport",
                filename: "performances_"+room_name+".xls",
         });
       });
       
       
       
        
    });
</script>