

<div style="font-size: 8px;">
    <span style="font-size: 10px;"><strong><?= Yii::t('app','Legend')?></strong></span>
    <table cellpadding="0" class="table table-striped table-bordered" style="font-size: 8px; width:20%; float: left; border-collapse: collapse; ">
        <tr>
             <td class="th_legende"><?= Yii::t('app','Range') ?></td><td class="th_legende"><?= Yii::t('app','Stage') ?></td><td class="th_legende"><?= Yii::t('app','Appreciation') ?></td>
        </tr>
        <tr>
            <?php
                   $dataProvider =  AppreciationsGrid::model()->searchBySection($section_id,$acad);
                  if($dataProvider!=null)
                  {
                   $dataProvider = $dataProvider->getData();
                   
                   foreach ($dataProvider as $data)
                   {
                        echo '<tr><td >'.$data->getAppreciationRange().'</td><td >'.$data->short_text.'</td><td >'.$data->comment.'</td></tr>';
                   }
                  }
                   
                   
            ?>
        </tr>
    </table>
    
</div>

