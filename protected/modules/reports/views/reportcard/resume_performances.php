<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$sql_room = "SELECT * FROM rooms order by room_name ASC";
$room_data = Rooms::model()->findAllBySql($sql_room);
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');
$cs->registerScriptFile("//cdn.rawgit.com/rainabba/jquery-table2excel/1.1.0/dist/jquery.table2excel.min.js");
$cs->registerScriptFile("https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.4.1/jspdf.min.js");
$cs->registerScriptFile("https://cdnjs.cloudflare.com/ajax/libs/jspdf-autotable/2.3.5/jspdf.plugin.autotable.min.js");






?>

<div id="dash">
		<div class="span3"><h2>
		      
       <?php  	
	      echo Yii::t('app','R&eacute;sum&eacute; performances'); 
		?>
              
        </h2> </div>
              
              
      <div class="span3">
          <div class="span4">

                      <?php

                     $images = '<i class="fa fa-balance-scale"> &nbsp;'.Yii::t('app','End Year Decision').'</i>';

                               // build the link in Yii standard

                                echo CHtml::link($images,array('/reports/reportcard/endYearDecision/mn/std/from/stud')); 
                                //$this->back_url='/academic/persons/listforreport?isstud=1&from=stud';   
                     
                   ?>

           </div> 
          
             <div class="span4">

                      <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                               // build the link in Yii standard

                                echo CHtml::link($images,array('/academic/persons/listforreport?isstud=1&from=stud')); 
                                $this->back_url='/academic/persons/listforreport?isstud=1&from=stud';   
                     
                   ?>

                  </div> 
    
         </div>
 </div>

<div class="row-fluid">
    <div class="span3">
        <label id="resp_form">
        <label for="cat-name" class="required"><?= Yii::t('app','Room'); ?></label>
        <select name="room" id="room">
            <option value=""><?= Yii::t('app','Room'); ?></option>
            <?php
                foreach($room_data as $rd){
                    ?>
            <option value="<?= $rd->id; ?>"><?= $rd->room_name; ?></option>
            <?php
                }
            ?>
        </select>
        </label>
    </div>
    
</div>
<p>
    &nbsp;
</p>
<div class="row-fluid">
    
       <div class="span2">
            <button class="btn btn-warning" id="send_excel"><?= Yii::t('app','Send to Excel'); ?></button>
        </div>
    
</div>
<p>
    &nbsp;
</p>

<div id="persons-grid" class="form">
    
</div>


<script>
    
   
    $(document).ready(function(){
        // alert("Test");
        $("#room").change(function(){
           
            var id_room = $(this).val();
            
            
             $.get('<?= $baseUrl?>/index.php/reports/reportcard/loadStudentPerfo',{room:id_room},function(data){
                   $('#persons-grid').html(data);
                 });
            
        });
        
        
        
        
        
         
        
    });
</script>