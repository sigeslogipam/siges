<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//require_once __DIR__ . '/vendor/autoload.php';
 $acad = Yii::app()->session['currentId_academic_year'];
 
 $format_paper_ = infoGeneralConfig('size_paper_bulletin');
 if($format_paper_ == "Legal"){
     $format_paper = array(216,356);   
 }elseif($format_paper_=="Letter"){
     $format_paper = array(216,279); 
 }else{
     $format_paper = array(216,279);
 }
 
 if($ns_structure!='')
     $ns_reportcard = $ns_structure;
 else
 $ns_reportcard = infoGeneralConfig('ns_reportcard');
 //Extract display_student_stage
 $display_student_stage = infoGeneralConfig('display_student_stage');
    
 $body=null;
 $foot=null; 
 
 
 switch($ns_reportcard)
 {
     case 0:
        {
           $body = "body_pdf";
           
           if($display_student_stage==1)
             $foot= "footer_pdf";
           else
              $foot= "footer_pdf0"; 
            $orientation_paper = "P";
        }
        break;
   
     case 1: // hibiscus
        {
            $body = "body_pdf1";
            
            if($display_student_stage==1)
             $foot= "footer_pdf1";
            else
              $foot= "footer_pdf0"; 
             $orientation_paper = "L";
        }
        break;
    
    case 2: // saintjean
        {
            $body = "body_pdf2";
            
            if($display_student_stage==1)
             $foot= "footer_pdf";
            else
              $foot= "footer_pdf0"; 
            
             $orientation_paper = "P";
        }
        break;
    
    default:
        {
             $body = "body_pdf";
             
            if($display_student_stage==1)
             $foot= "footer_pdf";
            else
              $foot= "footer_pdf0"; 
            
              $orientation_paper = "P";
        }
        break;
 }
    
$mpdf = new \Mpdf\Mpdf(array( 
        'margin_top' =>25,
        'margin_header' =>5,
        'margin_footer' =>5,
	'margin_left' =>10 ,
	'margin_right' =>10,
        'margin_bottom'=>10,
	//'mirrorMargins' => true,
        'mode' => 'utf-8', 
        'format' =>$format_paper,
        'orientation'=>$orientation_paper
    ));
// 8.5 * 14 : array(216,356) -> Letter 
// 8.5 * 11 : array(216,279) -> Legal
// P : Portrait
// L : Landscape
//  'Legal-P'
//$mpdf->SetColumns(1);

$mpdf->keep_table_proportions=false;
$mpdf->SetHTMLHeader($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLHeader($this->renderPartial('header_pdf',array(), true),\Mpdf\HTMLParserMode::HTML_BODY);
//$mpdf->AddColumn();
//$mpdf->shrink_tables_to_fit = 1;
//$mpdf->SetColumns(2,'J',5);
//$mpdf->keepColumns = true;
//$mpdf->max_colH_correction = 5;
//$mpdf->WriteHTML('<columns column-count="2" vAlign="J" column-gap="7" />');

// To add watermark to the PDF Uncomment if you want to apply 
/*
$mpdf->SetWatermarkImage(Yii::app()->baseUrl.'/css/images/school_logo.png');
$mpdf->showWatermarkImage = true;
 * 
 */


$i=0;

foreach($array_stud as $student_id)
 { 
    $place ='';
    
    //check if all grades are validated for this stud		
     $all_validated=$this->if_all_grades_validated($student_id,$this->evaluation_id);
     if($all_validated)
      {
     
     $student=$this->getStudent($student_id);
 
     
     for($jj = 0; $jj<$tot; $jj++)
       {
          if($array_place[$jj][0]===$student_id)
           {
               $place = $array_place[$jj][1]; 

            }

        }
     
$mpdf->WriteHTML($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($this->renderPartial($body,array(
     'dataProvider'=>$dataProvider,
'class_average_'=>$class_average_,
'tot_grade'=>$tot_grade,
'tot_grade_debase'=>$tot_grade_debase,
'student'=>$student,
'student_id'=>$student_id, 
'pastp'=>$pastp,
'k'=>$k,
'l'=>$l,
'place'=>$place,
'max_grade'=>$max_grade,
'max_grade_debase'=>$max_grade_debase,
'acad'=>$acad,
'evaluationPeriod'=>$evaluationPeriod,
'period_exam_name'=>$period_exam_name,
'level'=>$level,
'room'=>$room,
'section'=>$section,
'section_id'=>$section_id,
'shift'=>$shift,
'name_acadPeriod_for_this_room'=>$name_acadPeriod_for_this_room,
'tot'=>$tot
 ), true),\Mpdf\HTMLParserMode::HTML_BODY);


 
  


 

//$mpdf->SetColumns(1);

$mpdf->SetHTMLFooter($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLFooter($this->renderPartial($foot,array(
  'acad'=>$acad,
'section_id'=>$section_id,
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);


if($i < count($array_stud)-1){
        $mpdf->AddPage();
    }elseif($i==count($array_stud)-1){
        
    }
    
    
 }

$i++;
} 
//$mpdf->AddColumn();
//$mpdf->WriteHTML($kontni);
$mpdf->Output("$room-$period_exam_name.pdf",\Mpdf\Output\Destination::DOWNLOAD);
