<?php
 /*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>

	
<?php

$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];

 $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
 $menfp = null;

$model_menfp=isLevelExamenMenfp($this->idLevel,$acad_sess);
			      
			      if($model_menfp !=null)
			        $menfp = $model_menfp['id'];

 $display_period_summary = infoGeneralConfig('display_period_summary');
 
 
  $use_mention_summary = infoGeneralConfig('use_mention_summary');
 
 $use_behavior_summary_inDecision = infoGeneralConfig('use_behavior_summary_inDecision');
 
 $lower_behaviorGrade_summary  = infoGeneralConfig('lower_behaviorGrade_summary');
 

 
 $decision_s = infoGeneralConfig('decision_finale_success');
		                      $decision_f = infoGeneralConfig('decision_finale_failure');		
		
								$decision_success_array= explode("/",substr($decision_s, 0));
						           
						        $decision_failure_array= explode("/",substr($decision_f, 0));	

 $lastEvaluation_done = false;

 
     if($current_acad==null)
		$condition = '';
	 else{
			$condition = 'p.active IN(1,2) AND ';
		  }

$average_base = getAverageBase($this->room_id,$acad_sess);

			   
$menfp_passing_grade = round( ($average_base/2),2);
		   
     $customReport = new CustomReport; 
	 
	// $this->room_id = Yii::app()->session['room_dash'];
	 $room_name = '';
	 
	 if(Rooms::model()->findByPk($this->room_id)!=null)
       { //$idLevel = Rooms::model()->findByPk($this->room_id)->level;
       
	     if(Rooms::model()->findByPk($this->room_id)->short_room_name!=null)
	     {
	         $room_name = Rooms::model()->findByPk($this->room_id)->short_room_name; 
	     }else{
	        $room_name = Rooms::model()->findByPk($this->room_id)->room_name;
	       }
	     
       }
       
     $passing_grade = $this->getPassingGrade($this->idLevel, $acad);
    
     $string_sql = "SELECT p.id, p.last_name, p.image, p.first_name, p.gender, r.room_name   FROM `persons` p
                    INNER JOIN room_has_person rh ON (rh.students = p.id)
                    INNER JOIN rooms  r ON  (rh.room = r.id )
                    WHERE rh.academic_year = $acad AND  r.id = $this->room_id AND p.active IN (1,2)
                    ORDER BY p.last_name";
    
     $data = Persons::model()->findAllBySql($string_sql);
     
     
 $string_sql_periode = "SELECT distinct g.evaluation, a.id as idantite_peryod, a.name_period FROM grades g INNER JOIN evaluation_by_year eby ON (g.evaluation = eby.id) INNER JOIN academicperiods a ON (eby.academic_year = a.id) INNER JOIN evaluations e ON (eby.evaluation = e.id) where e.academic_year = $acad ORDER BY a.name_period";
 
 // Construction des periodes d'examen    
 
 $data_periode = Grades::model()->findAllBySql($string_sql_periode);
 
$last_period = 0; 
 
 $array_period = array(); 
 $array_effectif = array();
 $array_success = array();
 $array_fail = array();
 $array_success_rate = array();
 $array_fail_rate = array();


 $j = 0;

 if($data_periode !=null)
 { 
 foreach($data_periode as $ap){
     $array_period[$j] = $ap->name_period;
     //$array_effectif[$j] = $this->getRoomSizeByExam($this->room_id, $ap->evaluation);
    // $array_success[$j] = $this->getSuccesSizeByExam($this->room_id, $ap->evaluation,$acad);
     //$array_fail[$j] = $array_effectif[$j]-$array_success[$j];
     $j++;
 }
 }
 
 //$count_tardy = $customReport->getAttendanceCountByRoom($this->room_id, $acad, array(3,4));
 //$count_tardy_f = $customReport->getAttendanceCountByRoomBySex($this->room_id, $acad, array(3,4),1);
 //$count_tardy_m = $customReport->getAttendanceCountByRoomBySex($this->room_id, $acad, array(3,4),0);
 //$count_absence = $customReport->getAttendanceCountByRoom($this->room_id, $acad, array(1,2)); 
 //$count_absence_f = $customReport->getAttendanceCountByRoomBySex($this->room_id, $acad, array(1,2),1);
 //$count_absence_m = $customReport->getAttendanceCountByRoomBySex($this->room_id, $acad, array(1,2),0);
 //$count_infraction = $customReport->getInfractionCountByRoom($this->room_id, $acad);
 
  ?>

<table>
    
    <thead id="anTetMwen" class="anTet">   
    <tr>
        <th style="width: 50px; background-color: #00E4C1;"></th>
        <th style="width: 250px; background-color: #00E4C1;"><?php echo Yii::t('app','First Name');?></th>
        <th style="width: 250px; background-color: #00E4C1;"><?php echo Yii::t('app','Last Name');?></th>
                
        <?php  if($data_periode !=null)
                {
                	
                   foreach($data_periode as $p)
                     {
                     	//gade si se denye evalyasyon
                     	$modelEvalByPeriod = EvaluationByYear::model()->findByPk($p->evaluation);
                     	if($modelEvalByPeriod->last_evaluation == 1)
                     	 {  $lastEvaluation_done= true;
                     	    $last_period = $p->evaluation;
                     	 }
					    
            ?>
					        <th style="width: 200px; background-color: #049cdb;"><?php  echo $p->name_period; ?></th>
        <?php 
                      }
                      
                   if($lastEvaluation_done== false) 
						  { 
					         Yii::app()->user->setFlash(Yii::t('app','Warning'), Yii::t('app','Please, specify your last evaluation in "Evaluation by period".'));

						  }
						  
				   if(($use_mention_summary ==1) && ($lastEvaluation_done == true) )
						{
						?>  
						   <th style="width: 200px; background-color: #049cdb;"><?php  echo Yii::t('app','Summary'); ?></th> 
					<?php	
						}
                }
                
            if($use_behavior_summary_inDecision==1)    
             {
             	  ?>
               <th style="width: 100px; background-color: #FABA3C;"><?php echo Yii::t('app','Discipline');?></th>
       <?php
                 }
        
		if($menfp!=null)
		  {
			?>
	          <th style="width: 100px; background-color: #049CDB;"><?php echo Yii::t('app','MENFP Average'); ?> </th>
		<?php
                 }
        
		?>
        
        
        <th style="width: 100px; background-color: #FABA3C;"><?php echo Yii::t('app','Mention');?></th>
        
        <th style="width: 100px; background-color: #FABA3C;"><?php echo Yii::t('app','Decision ');?></th>
    </tr>
    </thead>
    <tbody class="kontni">  
        <?php
            if($data !=null)
                {
            
            $j=0;
            foreach($data as $d){
                
         $j++;
        ?>
        <tr class="">
        <td style=""><?php echo $j;   ?></td>
        <?php
            // Affiche une photo de l'eleve dans SIGES
            if($d->image==NULL){
                $string_image_location = "<img src='".Yii::app()->baseUrl."/css/images/no_pic.png'>";
            }else{
                $string_image_location = "<img src='".Yii::app()->baseUrl."/documents/photo-Uploads/1/".$d->image."'>";
            }
            
            
			$string_stud_tool_tip_data = (Persons::model()->getIsScholarshipHolder($d->id,$acad)==1)? "<span><b>". Yii::t('app','Scholarship holder ')."</b></span><br/>" : "";
           
                        
        ?>
        <td style="width: 250px"><a data-toggle="tooltip" data-html="true" title="<div class='row-fluid'><div class='span4'><?php echo $string_image_location; ?></div><div class='span8' align='left'><?php echo $string_stud_tool_tip_data; ?></div></div>"   href="<?php echo Yii::app()->baseUrl.'/index.php/academic/persons/viewForReport/id/'.$d->id.'/pg/lr/pi/no/isstud/1/from/stud' ?>"><?php echo $d->first_name; ?></a>
        
        </td>
        <td style="width: 250px"><a data-toggle="tooltip" data-html="true" title="<div class='row-fluid'><div class='span4'><?php echo $string_image_location; ?></div><div class='span8' align='left'><?php echo $string_stud_tool_tip_data; ?></div></div>"   href="<?php echo Yii::app()->baseUrl.'/index.php/academic/persons/viewForReport/id/'.$d->id.'/pg/lr/pi/no/isstud/1/from/stud' ?>"><?php echo $d->last_name; ?></a>
        
        </td>
        <?php
      
       $summary_grade = 0;
       $summary_grade_count=0;
       $tot_summary_grade = 0;
       
        foreach($data_periode as $p)
         {
           ?>
        <td style="">
                <?php
                   $grade='';    
                $customRep = new CustomReport; 
                $grade=$customRep->calMoyStudent($d->id, $p->evaluation);
                if($grade >= $passing_grade)
                  {
                    echo  $grade;
                  }
                else
                  {
                    echo '<span id="koule_not" style="color: #F15C80">'.$grade.'</span>';
                  } 
                
                if(($use_mention_summary ==1) && ($lastEvaluation_done == true) )
				 { $summary_grade = $summary_grade + $grade;
				    $summary_grade_count++;
				 }
               //if($last_period ==$p->evaluation)  
                ?>
        </td>
        <?php
        
        } 
        
         if(($use_mention_summary ==1) && ($lastEvaluation_done == true) )
						{
						?>  <td style="">
				                <?php
				                       
												   
									if($summary_grade_count!= 0)
									  $tot_summary_grade = round( ($summary_grade / $summary_grade_count), 2); 
									
				                
				                if($tot_summary_grade >= $passing_grade){
				                    echo  $tot_summary_grade;
				                }else{
				                    echo '<span id="koule_not" style="color: #F15C80">'.$tot_summary_grade.'</span>';
				                }  
				                 
				                    echo '<input name="gen_average['.$d->id.']" type="hidden" value="'.$tot_summary_grade.'" />';  
				                ?>
                           </td> 
					<?php	
						}
						
			 if($use_behavior_summary_inDecision==1)
			 {
        ?>
       
      <td style="">
            <?php
               $disc = new RecordInfraction;
               $konte_peryod = 0;
               $mwayen_disiplin = 0;
               $som_peryod = 0.00;
               
               foreach($data_periode as $p){
                  $som_peryod += $disc->getDisciplineGradeByExamPeriod($d->id, $p->idantite_peryod);
                  $konte_peryod++;
               }
               
               if($konte_peryod!=0){
                   $mwayen_disiplin = round( ($som_peryod/$konte_peryod),2); 
               }
                echo $mwayen_disiplin; 
              
            ?>
        </td>
		
        <?php   
        
			 }
	if($menfp!=null)
		  {   $menfp_average=0;
		?>
        <td style="">
            <?php
			       $menfp_data = MenfpDecision::model()->checkDecisionMenfp($d->id, $acad_sess);
										 foreach($menfp_data as $menfp_d)
										   {
										   	    $menfp_average = $menfp_d['average'];
										   	}
			         if($menfp_average < $menfp_passing_grade)
				        echo '<span id="koule_not" style="color: #F15C80">'.$menfp_average.'</span>';
				      else
				        echo $menfp_average;
			 ?>
        </td>		
	
	<?php   
        
			 }		  
                   // 1: adm.; 2: adm_ail; 3: ref_ail; 4: ref_ca
                      //al gad nan tab decision_final pou pran mention an, sil vid pranl ak mwayenn lan
                       //check  record ID already stored in table decision_finale
						 $is_there = $this->checkDecisionFinale($d->id, $acad);                                                         
								   
				  if((isset($is_there))&&($is_there!=null))
					 {  //yes let's make an update
									     foreach($is_there as $result)
									        $mention_id = $result["report_mention"];
									     
					  }
                   else //pa genyen
                     {  
                      if($use_behavior_summary_inDecision==1)
                     	{ 
	                      if( ($tot_summary_grade >=$passing_grade)&&($mwayen_disiplin >= $lower_behaviorGrade_summary) )
	                          $mention_id = 1;
	                      elseif( ($tot_summary_grade >=$passing_grade)&&($mwayen_disiplin < $lower_behaviorGrade_summary) )
	                          $mention_id = 2;
	                         elseif( ($tot_summary_grade < $passing_grade)&&($tot_summary_grade >= ($average_base/2) ) )
	                              $mention_id = 2;//4;
	                          elseif( ($tot_summary_grade < ($average_base/2) ) )
	                                 $mention_id = 3;
	                                 
                     	  }
                     	elseif($use_behavior_summary_inDecision==0)
                     	{ 
	                      if( ($tot_summary_grade >=$passing_grade) )
	                          $mention_id = 1;
	                       elseif( ($tot_summary_grade < $passing_grade)&&($tot_summary_grade >= ($average_base/2) ) )
	                              $mention_id = 2;//4;
	                          elseif( ($tot_summary_grade < ($average_base/2) ) )
	                                 $mention_id = 3;
	                                 
                     	  }
                     	
	                           
                       }
                      
                     if($mention_id ==null)
                        {  
	                       if($use_behavior_summary_inDecision==1)
	                     	{ 
		                      if( ($tot_summary_grade >=$passing_grade)&&($mwayen_disiplin >= $lower_behaviorGrade_summary) )
		                          $mention_id = 1;
		                      elseif( ($tot_summary_grade >=$passing_grade)&&($mwayen_disiplin < $lower_behaviorGrade_summary) )
		                          $mention_id = 2;
		                         elseif( ($tot_summary_grade < $passing_grade)&&($tot_summary_grade >= ($average_base/2) ) )
		                              $mention_id = 2;//4;
		                          elseif( ($tot_summary_grade < ($average_base/2) ) )
		                                 $mention_id = 3;
		                                 
	                     	  }
	                     	elseif($use_behavior_summary_inDecision==0)
	                     	{ 
		                      if( ($tot_summary_grade >=$passing_grade) )
		                          $mention_id = 1;
		                       elseif( ($tot_summary_grade < $passing_grade)&&($tot_summary_grade >= ($average_base/2) ) )
		                              $mention_id = 2;//4;
		                          elseif( ($tot_summary_grade < ($average_base/2) ) )
		                                 $mention_id = 3;
		                                 
	                     	  }
	                           
                       }
 
                      
                if(($lastEvaluation_done == true) )  //if(($use_mention_summary ==1) && ($lastEvaluation_done == true) )
                  {   
                            $mention_str = getSuccessFailureDecision($mention_id);
					    	 
					    	 $style="";
					    	   	
					    	   	 if(in_array($mention_str,$decision_success_array) )
					    	   	   { 
					    	   	   	  $is_move =1; 
						    	   	   if($mention_id==1)
						    	   	     $style="background-color: #9CEB44;";
	                                   elseif($mention_id==2)
	                                      $style="background-color: #F6E77C;";
					    	   	   }
					    	   	 elseif(in_array($mention_str,$decision_failure_array) )
					    	   	   { 
					    	   	   	  $is_move =0;
					    	   	   	}
                    
                            echo '<td style="'.$style.'">';
                            echo $mention_str;
                      	         echo '</td>';
                      	         
                      	         $report_mention =$mention_id;
                            
                                 echo '<input name="reportMention['.$d->id.']" type="hidden" value="'.$report_mention.'" />';
                 
                  }
                 else
                  { echo '<td style="">';
                      	  echo Yii::t('app','N/A');
                    echo '</td>';
                      	           	  
                  }
                   
                    
                      
						
                  
              
              ?>
         
         
         
        <td style="">
             <?php  
                 /*   $null=Yii::t('app','Changer Décision');
	                $adm=Yii::t('app','Admis(e) en classe supérieure');
	                $adm_reserv=Yii::t('app','Admis(e) au Canado sous conditions');  //echo Yii::t('app','Admis(e) à l\'école sous conditions');
	                $adm_ail=Yii::t('app','Admis(e) ailleurs');
	                $ref_ail=Yii::t('app','À refaire ailleurs');
	                $ref_ca=Yii::t('app','À refaire au Canado');  //Yii::t('app','À refaire à l\'école');
                */
                
                 $decisions = loadSuccessFailureDecision();
                
                 echo '<select name="mention['.$d->id.']" > ';
                 
                 $i=0;
                foreach($decisions as $decis)
                { 
                	if($i==0)
                	  $selected = ' selected';
                	else
                	   $selected ='';
                	 
                	 echo '<option value='.$i.' '.$selected.'  >'.$decis.'</option>';
                	 
                	 $i++;
                }
               
               echo '</select>';
       
		 echo ' <input name="id_student['.$d->id.']" type="hidden" value="'.$d->id.'" />
		       ';
             
             ?>
        
         </td>
         
         
        </tr>
    
            <?php
            
            }
            } ?>
        
    </tbody>
</table>
			
		

	
<script>
   
// Change the selector if needed
/*
var $table = $('table.scroll'),
    $bodyCells = $table.find('tbody tr').children(),
    colWidth;

// Adjust the width of thead cells when window resizes

$(window).resize(function() {
    // Get the tbody columns width array
    colWidth = $bodyCells.map(function() {
        return $(this).width();
    }).get();
    
    // Set the width of thead columns
    $table.find('thead tr').children().each(function(i, v) {
        $(v).width(colWidth[i]);
    });    
}).resize(); // Trigger resize handler

*/
/*    
window.onscroll = function() {myFunction()};

var header = document.getElementById("anTetMwen");
var sticky = header.offsetTop;

function myFunction() {
  if (window.pageYOffset >= sticky) {
    header.classList.add("sticky");
  } else {
    header.classList.remove("sticky");
  }
}
*/
</script>	
	