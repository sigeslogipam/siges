<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 $current_acad_id= 0;
$current_year = currentAcad();
  if($current_year!=null)
	$current_acad_id = $current_year->id;
   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
$academic_period_name = AcademicPeriods::model()->findBYPk($acad)->name_period;


$current_acad = AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));


?>

 <!--Menu of CRUD  -->

 <?php 
   $siges_structure = infoGeneralConfig('siges_structure_session');
		
		$acad_=Yii::app()->session['currentId_academic_year']; 
		
		 $this->summary = false;
		 
		
		if((isset($_POST['summary']))&&($_POST['summary']==true)) 
		    $this->summary = true;
   
    $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
    if($current_acad==null)
						          $condition = '';
						     else{
						     	   if($acad_!=$current_acad->id)
							         $condition = '';
							      else
							         $condition = 'p.active IN(1,2) AND ';
						        }

      
								   								
	 $general_average_current_period =0;
     $max_grade_discipline=0;
     $include_discipline=0;
     $include_place = 1;
     $average_base =0;
     
 $temoin_has_note =false;
  
  //Extract max grade of discipline
  $max_grade_discipline = infoGeneralConfig('note_discipline_initiale') ;
                                    //Extract school Director
   $include_discipline = infoGeneralConfig('include_discipline_grade');
   //Extract school Director
   $tardy_absence_display = infoGeneralConfig('tardy_absence_display');
   //Extract include_place
   $include_place = infoGeneralConfig('include_place');

                                    //Extract observation line
    $show_observation_line = infoGeneralConfig('observation_line');

    //Extract student code
    $display_student_code = infoGeneralConfig('display_student_code');

     //Extract display_administration_signature
    $display_administration_signature = infoGeneralConfig('display_administration_signature');

    //Extract display_parent_signature
    $display_parent_signature = infoGeneralConfig('display_parent_signature');

    //Extract administration_signature_text
    $administration_signature_text = infoGeneralConfig('administration_signature_text');

    //Extract parent_signature_text
    $parent_signature_text = infoGeneralConfig('parent_signature_text');

     //Extract display_created_date
    $display_created_date = infoGeneralConfig('display_created_date');

    //Extract display_class_enroll
    $display_class_enroll = infoGeneralConfig('display_class_enroll');

   
    //Extract use_period_weight
    $use_period_weight = infoGeneralConfig('use_period_weight');

    //Extract use_period_weight
    $use_period_weight = infoGeneralConfig('use_period_weight');
    
    //Extract display_average_class
    $display_average_class = infoGeneralConfig('display_average_class');
    
    //Extract display_student_stage
    $display_student_stage = infoGeneralConfig('display_student_stage');
    
    //Extract display_total_in_subjectparent
    $display_total_in_subjectparent = infoGeneralConfig('display_total_in_subjectparent');
    
   $average_base = getAverageBase($this->room_id,$acad_);
		
     $lastEvaluation_done= false;
     
     $eval_date = null;
			                   $acad_year = null;
		//is last evaluation?
		$modelEvalByPeriod = EvaluationByYear::model()->findByPk($this->evaluation_id);
         if($modelEvalByPeriod->last_evaluation == 1)
           {  $lastEvaluation_done= true;
                     	    
            }	                    


                  //find date of the current evaluation
						$result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
							if(isset($result))
							 {  $result=$result->getData();//return a list of  objects
								 foreach($result as $r)
								   { $eval_date = $r->evaluation_date;
									  $acad_year = $r->academic_year;	   
									}
							   }
 
     
     
       $data_current_period =null;	      
	   $p_name_general_average = EvaluationByYear::model()-> getPeriodNameByEvaluationDATE($eval_date);
			           
		   foreach($p_name_general_average as $p_na)
			 $data_current_period = $p_na;


                 
                   
                   
?>

 <div class="grid-view" id="table-container" style="margin-top:0px;  margin-bottom: 20px; float:left;   width:100%;  overflow-x:scroll; overflow-y:hidden;">
    
     <center><div class="title" ><?= strtoupper(Yii::t("app","Report Card ")) ?> </div></center>
     <div class="info" > <div style="float:left; width: 59%;">  <b> <?= Yii::t('app','Name: ').'</b>'.$student ?> 
 <?php     
  
  

if($display_student_code==1)
{   $code=''; 
    //return id_number, gender,is_student,nif_cin,email,phone,adresse
$flashInfo= Persons::model()->getFlashInfoById($student_id);
foreach($flashInfo as $r)
{
$code = $r['id_number'];
}

     echo '<br/> <b>  '.Yii::t('app', 'Id Number').': </b>'.$code;
}
     

    echo '<br/> <b>  '.Yii::t('app','Level / Room: ').'</b> '.$level.' / '.$room.'<br/>  <b>  '.Yii::t('app','Section :').'</b> '.$section;
   ?>                  
   </div>
             
   <div style="float:right; ">
  <?php  
         $display_period_date='';
        $period_date= getEvaluationStartEndDateWithShortMonthById($this->evaluation_id);
        
        $display_period_date=' ('.$period_date.')';
        
            if($siges_structure==0)
              echo '<b>'.$evaluationPeriod.' : </b>'.$period_exam_name.$display_period_date;
      elseif($siges_structure==1)
             echo '<b>'.$period_exam_name.' : </b>'.$evaluationPeriod.$display_period_date;
      
      if($display_class_enroll==1)
        echo ' <br/> <b>'.Yii::t('app','Number of students: ').'</b> '.$tot.' / ';
    // On enleve la vacation pour le NCB (College Bird) 
    if($display_class_enroll==0)
         echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';



    echo ' <b>'.Yii::t('app','Academic period: ').'</b> '.$name_acadPeriod_for_this_room;


?>
     </div></div>
 
     <table class="table table-striped table-bordered" style="width:105%; float: left; margin-top: 10px; ">
                                                      <tr>
                                                        <td class="subjectheadnote"></td>
<?php  
// debut of cases depending on past period	   
			                    $eval_period='';
			                    $compter_p=2;///nou ajoute 1` pou kolonn max grade
                                            $colspan = 4;///nou ajoute 1` pou kolonn max grade
                                            
                                             if( ($display_average_class==1)&&($display_student_stage ==1) )
                                           {  $colspan = 4;///nou ajoute 1` pou kolonn max grade
                                             }
                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                            {    $colspan = 3; ///nou ajoute 1` pou kolonn max grade
                                                
                                            }
                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                {  $colspan = "2"; ///nou ajoute 1` pou kolonn max grade
                                                   
                                                }
                                                
                                           
                                              
                                              
			                     if($pastp!=null)
				                     {  
					                   foreach($pastp as $id_past_period)
					                      {
			 	                                $compter_p++;
			 	                                //$eval_period = $this->searchPeriodName($id_past_period);
			 	                                $period=ReportCard::searchPeriodNameForReportCard($id_past_period);
			 	                                
										       if($use_period_weight==1)                 
												 echo '<td colspan="'.$colspan.'" class="periodheadnote" style="text-align:center;"> <span style="font-size:8pt; "> <b>'.$period->name_period.'</b><br/>('.$period->weight.'%)  </span> </td>';
											   else
											     echo'<td colspan="'.$colspan.'" class="periodheadnote" style="text-align:center;"> <span style="font-size:8pt;"> <b>'.$period->name_period.'</b>  </span> </td>';
																		
											}
											//for the current period
											//$eval_period=$this->searchPeriodName($this->evaluation_id);
											$period=ReportCard::searchPeriodNameForReportCard($this->evaluation_id);
											
											if($use_period_weight==1)
											  echo '<td colspan="'.$colspan.'" class="periodheadnote" style="text-align:center;"> <span style="font-size:8pt;"> <b>'.$period->name_period.'</b><br/>('.$period->weight.'%) </span> </td>'; 
											else
											  echo '<td colspan="'.$colspan.'" class="periodheadnote" style="text-align:center;"> <span style="font-size:8pt;"> <b>'.$period->name_period.'</b> </span> </td>';
											 
											//echo '<td class="periodheadnote" > <span style="font-size:8pt;"> <b>'.$evaluationPeriod.'-'.$period_exam_name.'</b> </span> </td>';
											
								       } 
								    else  //for the current period
									{      $eval_period=$this->searchPeriodName($this->evaluation_id);
									
										echo '<td colspan="'.$colspan.'" class="periodheadnote" style="text-align:center;"> <span style="font-size:8pt;"> <b>'.$period_exam_name.'</b> </span> </td>'; 
										 //echo '<td colspan="3" class="periodheadnote" style="text-align:center;"> <span style="font-size:8pt;"> <b>'.$evaluationPeriod.'</b> </span> </td>';
								    }							      
									//fin ajout  
									$compter_p=$compter_p +3; 
									
									         
											
																	
											            $compter_p_debase=$compter_p; 
      
                                                                                                    $i=0;
                                                                                                    $j=0;
                                                                                                    $l=0;

                                                                                                      $old_parent='';
                                                                                                      $old_parent_debase='';

                                                                                                   $tot_grade_period_summary =0;
                                                                                                   $tot_grade_period_summary_final =0;

											           
?>                                                        
                                                           
   					             </tr>
                                                     
                                                         <tr class="">
                                                             <td class=""></td>
                                      <?php
                                                   $stud_grade_text = '';
                                                   if($display_average_class ==1)
                                                               $stud_grade_text = Yii::t('app','Stud.Grade');
                                                            
                                      
                                              if($pastp!=null)
				                 {  
                                                       foreach($pastp as $id_past_period)
                                                          {
                                                            echo '<td class="subtitleperiod">'.$stud_grade_text.'</td>';
                                                            
                                                            if($display_average_class==1) 
                                                            {  echo  '<td class="subtitleperiod">'.Yii::t('app','Class.Grade').'</td>';
                                                            
                                                               $compter_p=$compter_p ++;
                                                            }
                                                              
                                                           echo '<td class="subtitleperiod_side">'.Yii::t('app','MAX GRADE').'</td>';
                                                             
                                                            if($display_student_stage ==1)      
                                                              {  echo '<td class="subtitleperiod_side">'.Yii::t('app','Stage').'</td>';
                                                              
                                                              $compter_p=$compter_p ++;
                                                              }
                                                            
                                                                //$compter_p=$compter_p +2; 
                                                           }
                                                           
                                                           
                                                          echo '<td class="subtitleperiod">'.$stud_grade_text.'</td>';
                                                            
                                                          if($display_average_class==1) 
                                                                echo  '<td class="subtitleperiod">'.Yii::t('app','Class.Grade').'</td>';
                                                              
                                                           echo '<td class="subtitleperiod_side">'.Yii::t('app','MAX GRADE').'</td>';
                                                             
                                                            if($display_student_stage ==1)      
                                                                   echo '<td class="subtitleperiod_side">'.Yii::t('app','Stage').'</td>';
                                                 }
                                                else  
                                                    {      
                                                          echo '<td class="subtitleperiod">'.$stud_grade_text.'</td>';
                                                                 
                                                            if($display_average_class==1) 
                                                                echo  '<td class="subtitleperiod">'.Yii::t('app','Class.Grade').'</td>';
                                                             
                                                           echo '<td class="subtitleperiod_side">'.Yii::t('app','MAX GRADE').'</td>';
                                                             
                                                            if($display_student_stage ==1)      
                                                                   echo '<td class="subtitleperiod_side">'.Yii::t('app','Stage').'</td>';

                                                    }
                                                    
                                                
                                                  
                                      ?>
                                                             
                                                          
                                                             
                                                        </tr>
<?php
                                                                                                         
                                                        //[$k][1]=id; [$k][1]=subject_name; [$k][2]=weight; [$k][3]=subject_parent; [$k][4]=reference_id;
                                    
													   while(isset($dataProvider[$l][0]))
													     {
													     
                                                               $_grade=0;														  
														      if($j==2)
																 $j=0;
																if($j==0)
																	$class="color1";
																elseif($j==1)
																	$class="color2";
if($dataProvider[$l][5]==1)// si se kou debase
{
																			  
//$class_child="subject";																//$line=
																
if($dataProvider[$l][3]!=null)//[$k][1]=id; [$k][1]=subject_name; [$k][2]=weight; [$k][3]=subject_parent; [$k][4]=reference_id;
  {  
  	  $parent_name_debase ='';
  	  $grade_total_debase=0;
  	  $weight_total_debase=0;
 	  
  	  $subject_parent_name_debase = Subjects::model()->getSubjectNameBySubjectID($dataProvider[$l][3]);
  	  
          
          if( isset($subject_parent_name_debase)&&($subject_parent_name_debase!=null) )
              $subject_parent_name_debase = $subject_parent_name_debase->getData();
  	  
  	  $class_child="subject";
  	  
  	    foreach($subject_parent_name_debase as $subject_parent)
  	       $parent_name_debase = $subject_parent;
  	  
  	    if($old_parent_debase!= $dataProvider[$l][3])                     
  	     {  
  	     	if($parent_name_debase!=null)
  	     	   echo '<tr class=""  > <td class="subjectParent"  > '.strtoupper(strtr($parent_name_debase->subject_name, pa_daksan() )).'  </td> ';
  	     	else
  	     	   echo '<tr class=""  > <td class="subjectParent"  >  </td> ';
  	     	   
                  
                 $colspan = 2;///nou ajoute 1` pou kolonn max grade
                $td_app ='';
                $td_app1 ='';

                  if($pastp!=null)
		  {  
			foreach($pastp as $id_past_period)
			 {
			 	  $grade_total_debase=0;
  	              $weight_total_debase=0;
  	  
			$_total_debase=Grades::model()->getTotalGradeWeightByPeriodForSubjectParent($dataProvider[$l][3],$student_id, $id_past_period);
					
					if($_total_debase!=null)
					 { foreach($_total_debase as $_total_) 
					    {
						  $grade_total_debase= $_total_["grade_total"];
  	                      $weight_total_debase= $_total_["weight_total"];
						 }//fin foreach 
					  }
					         
                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                           {  $colspan = 3;///nou ajoute 1` pou kolonn max grade
                                              $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weight_total_debase),2),$acad).' </td>';
                                              $td_app1 ='<td class="periodParent_side" >  </td>';
                                           }
                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                            {    $colspan = 2; ///nou ajoute 1` pou kolonn max grade
                                                
                                                  if($display_student_stage ==1)
                                                  {     $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weigh_debaset_total),2),$acad).' </td>';
                                                      $td_app1 ='<td class="periodParent_side" > </td>';
                                                  }
                                                  
                                                 if($display_average_class ==1) 
                                                 { $td_app ='<td class="periodParent_side" > </td>';
                                                   $td_app1 ='<td class="periodParent_side" > </td>';
                                                 }
                                               
                                            }
                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                {  $colspan = "2"; ///nou ajoute 1` pou kolonn max grade
                                                    $td_app ='';
                                                    $td_app1 ='';
                                                }
                                                             
                                          if($display_total_in_subjectparent==1)
                                            {  $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weight_total_debase),2),$acad).' </td>';
                                              $td_total_debase =round($grade_total_debase,2).' /<b> '.$weight_total_debase.'</b>';
                                            }
                                          elseif($display_total_in_subjectparent==0)
                                           {    $td_app ='';
                                           $td_app1 ='';
                                               $td_total_debase ='';
                                               
                                                 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                    {  $colspan = 4;///nou ajoute 1` pou kolonn max grade
                                                      }
                                                   elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                     {    $colspan = 3; ///nou ajoute 1` pou kolonn max grade    
                                                     }
                                                   elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                         {  $colspan = 2; ///nou ajoute 1` pou kolonn max grade
                                                          }
                                           }
                                              
                                              
					//if($grade_total_debase!=0)																					                                                              
					//   echo ' <td class="periodParent" colspan="'.$colspan.'" style="text-align:left;"> '.$td_total_debase.'</td>'.$td_app;
					//else
					   echo ' <td class="periodParent" colspan="'.$colspan.'" > </td>'.$td_app1;
																			  
				
			 }//fin foreach past_period
				
				   //Grades for the current period
			$_total1_debase=Grades::model()->getTotalGradeWeightByPeriodForSubjectParent($dataProvider[$l][3],$student_id, $this->evaluation_id);
				 
					if($_total1_debase!=null)
					 { foreach($_total1_debase as $_total_) 
					    {
						  $grade_total_debase= $_total_["grade_total"];
  	                      $weight_total_debase= $_total_["weight_total"];
						 }//fin foreach 
					  }
					
                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                           {  $colspan = 3;///nou ajoute 1` pou kolonn max grade
                                              $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weight_total_debase),2),$acad).' </td>';
                                              $td_app1 ='<td class="periodParent_side" > </td>';
                                           }
                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                            {    $colspan = 2; ///nou ajoute 1` pou kolonn max grade
                                                
                                                  if($display_student_stage ==1)
                                                  {     $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                                      $td_app1 ='<td class="periodParent_side" > </td>';
                                                  }
                                                  
                                                 if($display_average_class ==1) 
                                                 { $td_app ='<td class="periodParent_side" > </td>';
                                                   $td_app1 ='<td class="periodParent_side" > </td>';
                                                 }
                                               
                                            }
                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                {  $colspan = "2"; ///nou ajoute 1` pou kolonn max grade
                                                    $td_app ='';
                                                    $td_app1 ='';
                                                }
                                         
                                      if($display_total_in_subjectparent==1)
                                            {  $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weight_total_debase),2),$acad).' </td>';
                                              $td_total_debase =round($grade_total_debase,2).' /<b> '.$weight_total_debase.'</b>';
                                            }
                                          elseif($display_total_in_subjectparent==0)
                                           {    $td_app ='';
                                               $td_total_debase ='';
                                               $td_app1 ='';
                                               
                                                 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                    {  $colspan = 4;///nou ajoute 1` pou kolonn max grade
                                                      }
                                                   elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                     {    $colspan = 3; ///nou ajoute 1` pou kolonn max grade    
                                                     }
                                                   elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                         {  $colspan = 2; ///nou ajoute 1` pou kolonn max grade
                                                          }
                                           }
                                           
					//if($grade_total_debase!=0)																					                                                              
					//   echo ' <td class="periodParent" colspan="'.$colspan.'" style="text-align:left;"> '.$td_total_debase.'</td>'.$td_app;
					//else
					   echo ' <td class="periodParent" colspan="'.$colspan.'" > </td>'.$td_app1;
							
																	  
               }//fin past !=null
             else //if past_period null, get grades for the current period
                {          
                     //Grades for the current period
			    $_total1_debase=Grades::model()->getTotalGradeWeightByPeriodForSubjectParent($dataProvider[$l][3],$student_id, $this->evaluation_id);
				 
					if($_total1_debase!=null)
					 { foreach($_total1_debase as $_total_) 
					    {
						  $grade_total_debase= $_total_["grade_total"];
  	                      $weight_total_debase= $_total_["weight_total"];
						 }//fin foreach 
					  }
					
                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                           {  $colspan = 3;///nou ajoute 1` pou kolonn max grade
                                              $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weight_total_debase),2),$acad).' </td>';
                                              $td_app1 ='<td class="periodParent_side" > </td>';
                                           }
                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                            {    $colspan = 2; ///nou ajoute 1` pou kolonn max grade
                                                
                                                  if($display_student_stage ==1)
                                                  {     $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weight_total_debase),2),$acad).' </td>';
                                                      $td_app1 ='<td class="periodParent_side" > </td>';
                                                  }
                                                  
                                                 if($display_average_class ==1) 
                                                 { $td_app ='<td class="periodParent_side" > </td>';
                                                   $td_app1 ='<td class="periodParent_side" > </td>';
                                                 }
                                               
                                            }
                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                {  $colspan = "2"; ///nou ajoute 1` pou kolonn max grade
                                                    $td_app ='';
                                                    $td_app1 ='';
                                                }
                                         
                                                if($display_total_in_subjectparent==1)
                                            {  $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total_debase*100)/$weight_total_debase),2),$acad).' </td>';
                                              $td_total_debase =round($grade_total_debase,2).' /<b> '.$weight_total_debase.'</b>';
                                            }
                                          elseif($display_total_in_subjectparent==0)
                                           {    $td_app ='';
                                           $td_app1 ='';
                                               $td_total_debase ='';
                                              
                                                 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                    {  $colspan = 4;///nou ajoute 1` pou kolonn max grade
                                                      }
                                                   elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                     {    $colspan = 3; ///nou ajoute 1` pou kolonn max grade    
                                                     }
                                                   elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                         {  $colspan = 2; ///nou ajoute 1` pou kolonn max grade
                                                          }
                                           }
                                           
					//if($grade_total_debase!=0)																					                                                              
					//   echo ' <td class="periodParent" colspan="'.$colspan.'" style="text-align:left;" > '.$td_total_debase.'</td>'.$td_app;
					//else
					   echo ' <td class="periodParent" colspan="'.$colspan.'" > </td>'.$td_app1;
							
													  
                  }//fin past_period ==null
                                                             
                                                  
					
					
            
  	       
  	       echo ' </tr>';
               
  	         $old_parent_debase = $dataProvider[$l][3];
  	       }
  	  
  	    
     }
  else
     {
     	   $class_child="subject_single";
     	   
        if($old_parent_debase!= $dataProvider[$l][3])
  	     { 
           $old_parent_debase = $dataProvider[$l][3];
           
  	     }
  	    	       	   
  	     
      }
                                             $summary_grade_debase = 0;
                                             $summary_grade_count_debase = 0;
                                              
                                               
											  echo '<tr class="'.$class.'"> <td class="'.$class_child.'"> '.$dataProvider[$l][1].'  </td>';	     
															
													 if($pastp!=null)
														   {  
															  if($dataProvider[$l][4]!=NULL)
															     $id_course_debase = $dataProvider[$l][4];
															  else
															    $id_course_debase = $dataProvider[$l][0];
															   
															  foreach($pastp as $id_past_period)
															    {
																		//gade si kou sa gen referans
																		$reference_id=0; 
																	    $useCourseReference=false;
																	    $modelCourse = Courses::model()->findByPk($id_course_debase);
																	        
																        $reference_id = $modelCourse->reference_id;
																        
																        if($reference_id==null)//sil pa genyen
	         															  { //si kou a evalye pou peryod sa
																			$old_subject_evaluated_debase=$this->isOldSubjectEvaluated($id_course_debase,$this->room_id,$id_past_period);         
														                   if($old_subject_evaluated_debase)
														                     { $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$id_course_debase,$id_past_period);
														                        $careAbout=$old_subject_evaluated_debase; 
														                     }
														                   else
														                     {  $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$k][0],$id_past_period);
														                          $careAbout=$this->isSubjectEvaluated($dataProvider[$l][0],$this->room_id,$id_past_period); 
														                       }
														                     
														                     
	         															   }
	       																elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
																          { 
																          	$dataProvider9=Grades::model()->searchByRoom($reference_id,$id_past_period);
																          	              
																          	//1) gad sil gen not pou evaluation sa
																            if($dataProvider9->getData()!=null)
																              { $useCourseReference=true;
																                //si kou a evalye pou peryod sa
																		         $old_subject_evaluated_debase=$this->isOldSubjectEvaluated($reference_id,$this->room_id,$id_past_period);
																		         if($old_subject_evaluated_debase)
																                    {  $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$reference_id,$id_past_period);
																                        $careAbout=$old_subject_evaluated_debase;
																                    }
																                 else
																                   { $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$id_course_debase,$id_past_period);
																                        $careAbout=$this->isSubjectEvaluated($id_course_debase,$this->room_id,$id_past_period);
																                     }

																              
																               }
																            elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
																              {
																                  $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$id_course_debase,$id_past_period);
																                  
																                  $careAbout=$this->isSubjectEvaluated($id_course_debase,$this->room_id,$id_past_period);
																               }
																               
																            
																          
																          
																          
																          }
																		
																		//si kou a evalye pou peryod sa
																	/*	$old_subject_evaluated=$this->isOldSubjectEvaluated($id_course_debase,$this->room_id,$id_past_period);         
													                   if($old_subject_evaluated)
													                      $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$id_course_debase,$id_past_period);
													                   else
													                       $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$l][0],$id_past_period);
													                       
													                  */
                                                                                                                                            
                                                                                                                                                    
                                                                                                                                             if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="period_side1" > --- </td>';

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='';
                                                                                                                                                    }
                                                                                                                                                    
																			if(isset($grades_debase)){
														                        $r=$grades_debase->getData();//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade) {
																			          // pr creer bulletin pr ceux ki ont au moins 1 note

                                                                                                                                            if($grade->grade_value!=null)																					                                            
                                                                                                                                                {  $temoin_has_note=true;	
									
									                                              
																					
                                                                                                                                                         $subject_average_debase = round(Grades::model()->getSubjectAverage($acad_,$id_past_period,$id_course_debase), 2);                          
																			
                                                                                                                                                         if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                            {  $td_app_av ='<td class="period" > '.$subject_average_debase.' </td>';
                                                                                                                                                               $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$l][2]), 2 ),$acad).' </td>';
                                                                                                                                                               $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1" > --- </td>'; ///ajoute 1td pou maxgrade
                                                                                                                                                            }
                                                                                                                                                           elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                             {    
                                                                                                                                                                   if($display_student_stage ==1)
                                                                                                                                                                   {      $td_app_av ='';
                                                                                                                                                                       $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$l][2]), 2 ),$acad).' </td>';
                                                                                                                                                                   }
                                                                                                                                                                   elseif($display_student_stage ==0)
                                                                                                                                                                   {   $td_app_av ='<td class="period_side1" > '.$subject_average_debase.' </td>';
                                                                                                                                                                      $td_app_ap ='';
                                                                                                                                                                   }

                                                                                                                                                                   $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1" > --- </td>';//ajoute 1td pou maxgrade

                                                                                                                                                                  //if($display_average_class ==1) 
                                                                                                                                                                   //$td_app_av ='<td class="period" > '.$subject_average_debase.' </td>';



                                                                                                                                                             }
                                                                                                                                                           elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                 {   $td_app_av ='';
                                                                                                                                                                     $td_app_ap ='';
                                                                                                                                                                     $td_app1 ='<td class="period_side1" > --- </td>';//1td pou maxgrade
                                                                                                                                                                 }
                                                                                                                                                         //les colonnes notes suivant le nbre d'etape anterieur
                                                                                                                                                                 
                                                                                                                                                                $grade_value = $grade->grade_value;
                                                                                                                                                                $passCours = getPassingGradeCourse($dataProvider[$l][0],$acad);
                                                                                                                                                                
                                                                                                                                                                if($grade_value<$passCours)
                                                                                                                                                                    $grade_value = '<b><>'.$grade->grade_value.'</b>';
                                                                                                                                                                
										                                   
																		           echo ' <td class="period" > '.$grade_value.' </td>'.$td_app_av.' <td class="period_side1" > <b>'.$dataProvider[$l][2].'</b> </td>'.$td_app_ap;
														//fin...			           
														                             
																			            }
																			           else
																			             echo ' <td class="period" > --- </td>'.$td_app1;
																			             
																			            // $max_grade=$max_grade+$dataProvider[$k][2];
																			             
   															                      }//fin foreach grades
																				}
																			  else
																			  echo ' <td class="period" > --- </td>'.$td_app1;
																			  
																			  
													  	                   } //fin isset grades
													  	                   
													  	                   
													  	                   
																	   }//fin foreach past_period
																	   //$careAbout=false;
																	   if($dataProvider[$l][4]!=NULL)
																	      {  
																	      	   $id_course_debase = $dataProvider[$l][4];
																	      	
																	         
																	         //si kou a evalye pou peryod sa
																				$old_subject_evaluated_debase=$this->isOldSubjectEvaluated($id_course_debase,$this->room_id,$this->evaluation_id);         
															                   if($old_subject_evaluated_debase)
															                     { $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$id_course_debase,$this->evaluation_id);
															                        $careAbout=$old_subject_evaluated_debase; 
															                     }
															                   else
															                     {  $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$l][0],$this->evaluation_id);
															                       $careAbout=$this->isSubjectEvaluated($dataProvider[$l][0],$this->room_id,$this->evaluation_id); 
															                     }
															                     
																	           
		
																	    }
																	  else
																	    {  $id_course = $dataProvider[$l][0];
																	        $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$l][0],$this->evaluation_id);
																	        $careAbout=$this->isSubjectEvaluated($id_course_debase,$this->room_id,$this->evaluation_id); 
																	    } 
																	    
																	            
													                    if(($careAbout))//if(($careAbout)||($pastp!=null) )
													                         $max_grade_debase=$max_grade_debase+$dataProvider[$l][2];
																	   
																	   //Grades for the current period
																	  
                                                                                                                                              if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1" > --- </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1" > --- </td>';//ajoute 1td pou maxgrade

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="period_side1" > --- </td>'; //1td pou maxgrade
                                                                                                                                                    }
																			if(isset($grades_debase)){
														                        $r=$grades_debase->getData();//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade) {
																			   
																			        if($grade->grade_value!=null)// pr creer bulletin pr ceux ki ont au moins 1 note
																					  { $temoin_has_note=true;	
									//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&                       
									                                              //les colonnes notes suivant le nbre d'etape anterieur
                                                                                                                                                                          
                                                                                                                                                                          $subject_average_debase = round(Grades::model()->getSubjectAverage($acad_,$this->evaluation_id,$id_course_debase), 2);
                                                                                                                                                                     
                                                                                                                                                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                                                  {  $td_app_av ='<td class="period" > '.$subject_average_debase.' </td>';
                                                                                                                                                                                     $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$l][2]), 2 ),$acad).' </td>';
                                                                                                                                                                                     $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1" > --- </td>';//ajoute 1td pou maxgrade
                                                                                                                                                                                  }
                                                                                                                                                                                 elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                                                   {    
                                                                                                                                                                                         if($display_student_stage ==1)
                                                                                                                                                                                         {      $td_app_av ='';
                                                                                                                                                                                             $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$l][2]), 2 ),$acad).' </td>';
                                                                                                                                                                                         }
                                                                                                                                                                                         elseif($display_student_stage ==0)
                                                                                                                                                                                         {   $td_app_av ='<td class="period_side1" > '.$subject_average_debase.' </td>';
                                                                                                                                                                                            $td_app_ap ='';
                                                                                                                                                                                         }

                                                                                                                                                                                         $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1" > --- </td>'; //ajoute 1td pou maxgrade

                                                                                                                                                                                        //if($display_average_class ==1) 
                                                                                                                                                                                         //$td_app_av ='<td class="period" > '.$subject_average_debase.' </td>';



                                                                                                                                                                                   }
                                                                                                                                                                                 elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                                       {   $td_app_av ='';
                                                                                                                                                                                           $td_app_ap ='';
                                                                                                                                                                                           $td_app1 ='<td class="period_side1" > --- </td>';//1td pou maxgrade
                                                                                                                                                                                       }

																		           if($careAbout)
																		              {  $tot_grade_debase = $tot_grade_debase + $grade->grade_value;
																		              
																		                  $grade_value = $grade->grade_value;
                                                                                                                                                                $passCours = getPassingGradeCourse($dataProvider[$l][0],$acad);
                                                                                                                                                                
                                                                                                                                                                if($grade_value<$passCours)
                                                                                                                                                                    $grade_value = '<u>'.$grade->grade_value.'</u>';
                                                                                                                                                                
                                                                                                                                                                echo ' <td class="period" > '.$grade_value.' </td>'.$td_app_av.' <td class="period_side1" > <b>'.$dataProvider[$l][2].'</b> </td>'.$td_app_ap;
																		                   
																		                   
							 											              
																		                }
														//fin...			        
                                                         
																				   
																					  
																					   }
																			           else
																			             echo ' <td class="period" > --- </td>'.$td_app1;
																			              
																			           
   															                      }//fin foreach grades
																				}
																			  else
																			  echo ' <td class="period" > --- </td>'.$td_app1;
													  	                   } //fin isset grades
																	  
                                                                   }//fin past !=null
                                                           else //if past_period null, get grades for the current period
                                                             {          
                                                               if($dataProvider[$k][4]!=NULL)
															      {  $id_course_debase = $dataProvider[$l][4];
															         //si kou a evalye pou peryod sa
																		$old_subject_evaluated_debase=$this->isOldSubjectEvaluated($id_course_debase,$this->room_id,$this->evaluation_id);         
													                   if($old_subject_evaluated_debase)
													                     { $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$id_course_debase,$this->evaluation_id);
													                        $careAbout=$old_subject_evaluated_debase; 
													                     }
													                   else
													                     {  $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$l][0],$this->evaluation_id);
													                       $careAbout=$this->isSubjectEvaluated($dataProvider[$l][0],$this->room_id,$this->evaluation_id); 
													                     }

															      }
															  else
															    {  $id_course_debase = $dataProvider[$l][0];
															        $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$l][0],$this->evaluation_id);
															        $careAbout=$this->isSubjectEvaluated($id_course_debase,$this->room_id,$this->evaluation_id); 
															    }
                                                             	
                                                              
                                                              
                                                             	                
													                    if(($careAbout) )//if( ($careAbout)||($pastp!=null) )
													                         $max_grade_debase=$max_grade_debase+$dataProvider[$l][2];

                                                             	//Grades for the current period                                       
                                                                                                                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1" > --- </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1" > --- </td>';  //ajoute 1td pou maxgrade

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="period_side1" > --- </td>'; ///1td pou maxgrade
                                                                                                                                                    }
																	  // $grades_debase=Grades::model()->searchForReportCard($condition,$student_id,$id_course_debase,$this->evaluation_id);
																			if(isset($grades_debase)){
														                        $r=$grades_debase->getData();//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade) {
																			   
																			        if($grade->grade_value!=null)// pr creer bulletin pr ceux ki ont au moins 1 note
																					  { $temoin_has_note=true;	
									//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
									                                              //les colonnes notes suivant le nbre d'etape anterieur
										                                                    
																		           $subject_average_debase = round(Grades::model()->getSubjectAverage($acad_,$this->evaluation_id,$id_course_debase), 2);
                                                                                                                                                           if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app_av ='<td class="period" > '.$subject_average_debase.' </td>';
                                                                                                                                                  $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$l][2]), 2 ),$acad).' </td>';
                                                                                                                                                  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1" > --- </td>';  //ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                      if($display_student_stage ==1)
                                                                                                                                                      {      $td_app_av ='';
                                                                                                                                                          $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$l][2]), 2 ),$acad).' </td>';
                                                                                                                                                      }
                                                                                                                                                      elseif($display_student_stage ==0)
                                                                                                                                                      {   $td_app_av ='<td class="period_side1" > '.$subject_average_debase.' </td>';
                                                                                                                                                         $td_app_ap ='';
                                                                                                                                                      }
                                                                                                                                                      
                                                                                                                                                      $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1" > --- </td>'; //ajoute 1td pou maxgrade

                                                                                                                                                     //if($display_average_class ==1) 
                                                                                                                                                      //$td_app_av ='<td class="period" > '.$subject_average_debase.' </td>';
                                                                                                                                                       
                                                                                                                                                     
                                                                                                                                                    
                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av ='';
                                                                                                                                                        $td_app_ap ='';
                                                                                                                                                        $td_app1 ='<td class="period_side1" > --- </td>';  //1td pou maxgrade
                                                                                                                                                    }
                                                                                                                                                           
																		               if($careAbout)
																		                 {  $tot_grade_debase = $tot_grade_debase + $grade->grade_value;
																		                     
                                                                                                                                                                  $grade_value = $grade->grade_value;
                                                                                                                                                                $passCours = getPassingGradeCourse($dataProvider[$l][0],$acad);
                                                                                                                                                                
                                                                                                                                                                if($grade_value<$passCours)
                                                                                                                                                                    $grade_value = '<u>'.$grade->grade_value.'</u>';
                                                                                                                                                                
                                                                                                                                                                 echo ' <td class="period" > '.$grade_value.' </td>'.$td_app_av.' <td class="period_side1" > <b>'.$dataProvider[$l][2].'</b> </td>'.$td_app_ap;
																		                 
																		                      
																		                    
																		                   }
														//fin...			
                                                       
                                                      																					   
																					   }
																			           else
																			             echo ' <td class="period" > --- </td>'.$td_app1;
																			             
																			             
   															                      }//fin foreach grades
																				}
																			  else
																			    echo ' <td class="period" > --- </td>'.$td_app1;
													  	                   } //fin isset grades
																	  

                                                             }//fin past_period ==null
                                                             
                                                  
													
											
											
											  
									 echo ' </tr>';
                                                                         
                                                                                    }

                                                                                                               $j++;

                                                               
														$l=$l+1;
										 }
    $average_debase=0;  	$general_average_debase=0; 

             if(($average_base==10)||($average_base==100)) 
               { if($max_grade_debase!=0)  
                   $average_debase=round(($tot_grade_debase/$max_grade_debase)*$average_base,2);
               }
              else			
                {	$average_debase =null;	
                    $this->messageWrongAverageBase= true;
                  }
												 
// pour le depassement dans BIRD $html .= '<tr class="subjectheadnote_white_tr"><td colspan="'.$compter_p.'"> </td></tr>';

          echo '<tr class="sommes"><td class="periodsommes"><b><i>'.Yii::t('app','Total').' '.Yii::t('app','Base course').'</i></b></td>';
      if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="periodsommes2_red" > </td><td class="periodsommes2_red_side1" ><b>'.$max_grade_debase.'</b> </td><td class="periodsommes2_red_side1" > </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="periodsommes2_red_side1" >  </td><td class="periodsommes2_red_side1" ><b>'.$max_grade_debase.'</b> </td>';

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="periodsommes2_red_side1" ><b>'.$max_grade_debase.'</b> </td>';
                                                                                                                                                    }
												           if($pastp!=null)
														     {  
															    foreach($pastp as $id_past_period)
															      {
																		$data_=Grades::model()->getDataAverageByPeriod($acad,$id_past_period,$student_id);
																			if(isset($data_))
																			{
														                        $rs=$data_->getData();//return a list of  objects
															                 if($rs!=null)
																			   { foreach($rs as $_data) 
																			       {
																			 
																				     if($_data->sum!=null)
																				         echo '<td class="periodsommes2_red_side"> <b>'.$_data->sum.'</b> </td>'.$td_app1;
																				     else
																				         echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
														                            }//fin foreach _data
																				}
																				else
																			          echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                     } //fin isset data_
																	   }//fin foreach past period
																	  
																	  //total for the current period 
																	
																			      if($temoin_has_note)
                                                                                                                                                                  echo '<td class="periodsommes2_red_side"> <b>'.$tot_grade_debase.'</b> </td>'.$td_app1;
																			       else
																			         echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                  
													  	                   
																	   
                                                                   }//fin past period!=null
										                         else
										                          {
										                           //total for the current period 
																
																				  if($temoin_has_note)
																			         echo '<td class="periodsommes2_red_side"> <b>'.$tot_grade_debase.'</b> </td>'.$td_app1;
																			      else
																			        echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                  
	
										                          }
										                          
										                           
										                          
												              //echo '<td class="periodsommes2_red"> <b>'.$max_grade.' </b></td><td class="period"> --- </td><td class="period"> --- </td>';
												echo '  </tr>
												  
												  <tr class="sommes"><td class="periodsommes"><b><i>'.Yii::t('app','Average').' '.Yii::t('app','Base course').'</i></b></td>';
														$general_average_debase=0;
                                                                                                                $compter_p_general_av_debase =1;
                                                                                                                
                                                                                                                
														
													 if($pastp!=null)
														     {  
															    foreach($pastp as $id_past_period)
															      {
																		$data_=Grades::model()->getDataAverageByPeriod($acad,$id_past_period,$student_id);
																			if(isset($data_)){
														                        $rs=$data_->getData();//return a list of  objects
															                 if($rs!=null)
																			   { foreach($rs as $_data) 
																			       {
                                                                                                                                                                  if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$_data->average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td></td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td></td><td class="period_side1"> </td>';  //ajoutew 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$_data->average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td></td><td class="period_side1"> </td>';  //ajoute 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='</td><td class="period_side1"> </td>';//1td pou maxgrade
                                                                                                                                                    }
																			         if($use_period_weight==1)
																			          {
																			          	 //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($id_past_period);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average_debase = round( ($general_average_debase + ( ($_data->average*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average_debase = round( ($general_average_debase + $_data->average), 2);
																			       
                                                                                                                                                              $compter_p_general_av_debase ++;
                                                                                                                                                              
																				     echo '<td class="period_side"> <b>'.$_data->average.'</b> </td>'.$td_app_av.'</td><td class="period_side1"> </td>'.$td_app_ap;
														                          }//fin foreach _data 
																				}
																				else
																			          echo '<td class="period_side"> --- </td>'.$td_app1;
													  	                   } //fin isset data_
																	   }//fin foreach past period
																	  
																	  //average for the current period 
																	 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average_debase,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average_debase,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='';
                                                                                                                                                    }
																			      	if($use_period_weight==1)
																			          {  
																						  //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($this->evaluation_id);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average_debase = round( ($general_average_debase + (($average_debase*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average_debase = round( ($general_average_debase + $average_debase), 2);
																			            
																			      	
																			      	if($average_debase!=0)
																			           {
																			           	 echo '<td class="period_side"> <b>'.$average_debase.'</b> </td>'.$td_app_av.'<td class="period_side1"> </td>'.$td_app_ap;
																			           }
																			         else 
																			           echo '<td class="period_side"> --- </td>'.$td_app1;
			 																 
													  	                    
                                                                   }//fin isset period
                                                                 else
                                                                   {
                                                                   	//average for the current period 
																	  
																	if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average_debase,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average_debase,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='';
                                                                                                                                                    }	      	 
                                                                                                                                                                if($use_period_weight==1)
																			          {  
																						  //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($this->evaluation_id);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average_debase = round( ($general_average_debase + (($average_debase*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average_debase = round( ($general_average_debase + $average_debase), 2);
																			            
																			            
																			         
																			         if($average_debase!=0)
																			           { 
																			               echo '<td class="period_side"> <b>'.$average_debase.'</b> </td>'.$td_app_av.'<td class="period_side1"> </td>'.$td_app_ap;
																			           }
																			         else
																			           echo '<td class="period_side"> --- </td>'.$td_app1;
																			     
                                                                   }
                                                                   
                                                                   
												     
												         	   	
												         	   						     
												echo '</tr>';                                                                                
                                                                                 
 			                                                                        
                                                                                 
                  //fen kou debaze   //########################################### 
                                                                                
                                                                                 
 //[$k][1]=id; [$k][1]=subject_name; [$k][2]=weight; [$k][3]=subject_parent; [$k][4]=reference_id;
                                    
													   while(isset($dataProvider[$k][0]))
													     {
													     
                                                               $_grade=0;														  
														      if($i==2)
																 $i=0;
																if($i==0)
																	$class="color1";
																elseif($i==1)
																	$class="color2";

if($dataProvider[$k][5]==0)// se pa kou debase
{																			  
//$class_child="subject";																//$line=
																
if($dataProvider[$k][3]!=null)//[$k][1]=id; [$k][1]=subject_name; [$k][2]=weight; [$k][3]=subject_parent; [$k][4]=reference_id;
  {  
  	  $parent_name ='';
  	  $grade_total=0;
  	  $weight_total=0;
 	  
  	  $subject_parent_name = Subjects::model()->getSubjectNameBySubjectID($dataProvider[$k][3]);
  	  
          
          if( isset($subject_parent_name)&&($subject_parent_name!=null) )
              $subject_parent_name = $subject_parent_name->getData();
  	  
  	  $class_child="subject";
  	  
  	    foreach($subject_parent_name as $subject_parent)
  	       $parent_name = $subject_parent;
  	  
  	    if($old_parent!= $dataProvider[$k][3])                     
  	     {  
  	     	if($parent_name!=null)
  	     	   echo '<tr class=""  > <td class="subjectParent"  > '.strtoupper(strtr($parent_name->subject_name, pa_daksan() )).'  </td> ';
  	     	else
  	     	   echo '<tr class=""  > <td class="subjectParent"  >  </td> ';
  	     	   
                  
                 $colspan = 2; //ajoute 1 pou maxgrade
                $td_app ='';
                $td_app1 ='';

                  if($pastp!=null)
		  {  
			foreach($pastp as $id_past_period)
			 {
			 	  $grade_total=0;
  	              $weight_total=0;
  	  
			$_total=Grades::model()->getTotalGradeWeightByPeriodForSubjectParent($dataProvider[$k][3],$student_id, $id_past_period);
					
					if($_total!=null)
					 { foreach($_total as $_total_) 
					    {
						  $grade_total= $_total_["grade_total"];
  	                      $weight_total= $_total_["weight_total"];
						 }//fin foreach 
					  }
					         
                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                           {  $colspan = 3;//ajoute 1 pou maxgrade
                                              $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                              $td_app1 ='<td class="periodParent_side" >  </td>';
                                           }
                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                            {    $colspan = 2;//ajoute 1 pou maxgrade 
                                                
                                                  if($display_student_stage ==1)
                                                  {     $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                                      $td_app1 ='<td class="periodParent_side" > </td>';
                                                  }
                                                  
                                                 if($display_average_class ==1) 
                                                 { $td_app ='<td class="periodParent_side" > </td>';
                                                   $td_app1 ='<td class="periodParent_side" > </td>';
                                                 }
                                               
                                            }
                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                {  $colspan = "2"; //ajoute 1 pou maxgrade
                                                    $td_app ='';
                                                    $td_app1 ='';
                                                }
                                                             
                                          if($display_total_in_subjectparent==1)
                                            {  $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                              $td_total =round($grade_total,2).' /<b> '.$weight_total.'</b>';
                                            }
                                          elseif($display_total_in_subjectparent==0)
                                           {    $td_app ='';
                                           $td_app1 ='';
                                               $td_total ='';
                                               
                                                 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                    {  $colspan = 4;//ajoute 1 pou maxgrade
                                                    }
                                                   elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                     {    $colspan = 3;//ajoute 1 pou maxgrade     
                                                     }
                                                   elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                         {  $colspan = 2; //ajoute 1 pou maxgrade
                                                          }
                                           }
                                              
                                              
					//if($grade_total!=0)																					                                                              
					//   echo ' <td class="periodParent" colspan="'.$colspan.'" style="text-align:left;"> '.$td_total.'</td>'.$td_app;
					//else
					   echo ' <td class="periodParent" colspan="'.$colspan.'" > </td>'.$td_app1;
																			  
				
			 }//fin foreach past_period
				
				   //Grades for the current period
			$_total1=Grades::model()->getTotalGradeWeightByPeriodForSubjectParent($dataProvider[$k][3],$student_id, $this->evaluation_id);
				 
					if($_total1!=null)
					 { foreach($_total1 as $_total_) 
					    {
						  $grade_total= $_total_["grade_total"];
  	                      $weight_total= $_total_["weight_total"];
						 }//fin foreach 
					  }
					
                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                           {  $colspan = 3;//ajoute 1 pou maxgrade
                                              $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                              $td_app1 ='<td class="periodParent_side" > </td>';
                                           }
                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                            {    $colspan = 2; //ajoute 1 pou maxgrade 
                                                
                                                  if($display_student_stage ==1)
                                                  {     $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                                      $td_app1 ='<td class="periodParent_side" > </td>';
                                                  }
                                                  
                                                 if($display_average_class ==1) 
                                                 { $td_app ='<td class="periodParent_side" > </td>';
                                                   $td_app1 ='<td class="periodParent_side" > </td>';
                                                 }
                                               
                                            }
                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                {  $colspan = "2"; //ajoute 1 pou maxgrade
                                                    $td_app ='';
                                                    $td_app1 ='';
                                                }
                                         
                                      if($display_total_in_subjectparent==1)
                                            {  $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                              $td_total =round($grade_total,2).' /<b> '.$weight_total.'</b>';
                                            }
                                          elseif($display_total_in_subjectparent==0)
                                           {    $td_app ='';
                                               $td_total ='';
                                               $td_app1 ='';
                                               
                                                 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                    {  $colspan = 4;//ajoute 1 pou maxgrade
                                                      }
                                                   elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                     {    $colspan = 3;  //ajoute 1 pou maxgrade   
                                                     }
                                                   elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                         {  $colspan = 2; //ajoute 1 pou maxgrade
                                                          }
                                           }
                                           
					//if($grade_total!=0)																					                                                              
					//   echo ' <td class="periodParent" colspan="'.$colspan.'" style="text-align:left;"> '.$td_total.'</td>'.$td_app;
					//else
					   echo ' <td class="periodParent" colspan="'.$colspan.'" > </td>'.$td_app1;
							
																	  
               }//fin past !=null
             else //if past_period null, get grades for the current period
                {          
                     //Grades for the current period
			    $_total1=Grades::model()->getTotalGradeWeightByPeriodForSubjectParent($dataProvider[$k][3],$student_id, $this->evaluation_id);
				 
					if($_total1!=null)
					 { foreach($_total1 as $_total_) 
					    {
						  $grade_total= $_total_["grade_total"];
  	                      $weight_total= $_total_["weight_total"];
						 }//fin foreach 
					  }
					
                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                           {  $colspan = 3;//ajoute 1 pou maxgrade
                                              $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                              $td_app1 ='<td class="periodParent_side" > </td>';
                                           }
                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                            {    $colspan = 2; //ajoute 1 pou maxgrade
                                                
                                                  if($display_student_stage ==1)
                                                  {     $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                                      $td_app1 ='<td class="periodParent_side" > </td>';
                                                  }
                                                  
                                                 if($display_average_class ==1) 
                                                 { $td_app ='<td class="periodParent_side" > </td>';
                                                   $td_app1 ='<td class="periodParent_side" > </td>';
                                                 }
                                               
                                            }
                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                {  $colspan = "2"; //ajoute 1 pou maxgrade 
                                                    $td_app ='';
                                                    $td_app1 ='';
                                                }
                                         
                                                if($display_total_in_subjectparent==1)
                                            {  $td_app ='<td class="periodParent_side"  >'.appreciationAverageShortText($section_id,round( ( ($grade_total*100)/$weight_total),2),$acad).' </td>';
                                              $td_total =round($grade_total,2).' /<b> '.$weight_total.'</b>';
                                            }
                                          elseif($display_total_in_subjectparent==0)
                                           {    $td_app ='';
                                           $td_app1 ='';
                                               $td_total ='';
                                              
                                                 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                    {  $colspan = 4;  //ajoute 1 pou maxgrade
                                                      }
                                                   elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                     {    $colspan = 3; //ajoute 1 pou max grade    
                                                     }
                                                   elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                         {  $colspan = 2; //ajoute 1 pou maxgrade
                                                          }
                                           }
                                           
					//if($grade_total!=0)																					                                                              
					//   echo ' <td class="periodParent" colspan="'.$colspan.'" style="text-align:left;" > '.$td_total.'</td>'.$td_app;
					//else
					   echo ' <td class="periodParent" colspan="'.$colspan.'" > </td>'.$td_app1;
							
													  
                  }//fin past_period ==null
                                                             
                                                  
					
					
            
  	       
  	       echo ' </tr>';
               
  	         $old_parent = $dataProvider[$k][3];
  	       }
  	  
  	    
     }
  else
     {
     	   $class_child="subject_single";
     	   
        if($old_parent!= $dataProvider[$k][3])
  	     { 
           $old_parent = $dataProvider[$k][3];
           
  	     }
  	    	       	   
  	     
      }
                                             $summary_grade = 0;
                                             $summary_grade_count = 0;
                                              
                                               
											  echo '<tr class="'.$class.'"> <td class="'.$class_child.'"> '.$dataProvider[$k][1].' </td>';	     
															
													 if($pastp!=null)
														   {  
															  if($dataProvider[$k][4]!=NULL)
															     $id_course = $dataProvider[$k][4];
															  else
															    $id_course = $dataProvider[$k][0];
															   
															  foreach($pastp as $id_past_period)
															    {
																		//gade si kou sa gen referans
																		$reference_id=0; 
																	    $useCourseReference=false;
																	    $modelCourse = Courses::model()->findByPk($id_course);
																	        
																        $reference_id = $modelCourse->reference_id;
																        
																        if($reference_id==null)//sil pa genyen
	         															  { //si kou a evalye pou peryod sa
																			$old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$this->room_id,$id_past_period);         
														                   if($old_subject_evaluated)
														                     { $grades=Grades::model()->searchForReportCard($condition,$student_id,$id_course,$id_past_period);
														                        $careAbout=$old_subject_evaluated; 
														                     }
														                   else
														                     {  $grades=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$k][0],$id_past_period);
														                          $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$this->room_id,$id_past_period); 
														                       }
														                     
														                     
	         															   }
	       																elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
																          { 
																          	$dataProvider9=Grades::model()->searchByRoom($reference_id,$id_past_period);
																          	              
																          	//1) gad sil gen not pou evaluation sa
																            if($dataProvider9->getData()!=null)
																              { $useCourseReference=true;
																                //si kou a evalye pou peryod sa
																		         $old_subject_evaluated=$this->isOldSubjectEvaluated($reference_id,$this->room_id,$id_past_period);
																		         if($old_subject_evaluated)
																                    {  $grades=Grades::model()->searchForReportCard($condition,$student_id,$reference_id,$id_past_period);
																                        $careAbout=$old_subject_evaluated;
																                    }
																                 else
																                   { $grades=Grades::model()->searchForReportCard($condition,$student_id,$id_course,$id_past_period);
																                        $careAbout=$this->isSubjectEvaluated($id_course,$this->room_id,$id_past_period);
																                     }

																              
																               }
																            elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
																              {
																                  $grades=Grades::model()->searchForReportCard($condition,$student_id,$id_course,$id_past_period);
																                  
																                  $careAbout=$this->isSubjectEvaluated($id_course,$this->room_id,$id_past_period);
																               }
																               
																            
																          
																          
																          
																          }
																		
																		//si kou a evalye pou peryod sa
																	/*	$old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$this->room_id,$id_past_period);         
													                   if($old_subject_evaluated)
													                      $grades=Grades::model()->searchForReportCard($condition,$student_id,$id_course,$id_past_period);
													                   else
													                       $grades=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$k][0],$id_past_period);
													                       
													                  */
                                                                                                                                            
                                                                                                                                                    
                                                                                                                                             if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="period_side1"> </td>';//1td pou maxgrade
                                                                                                                                                    }
                                                                                                                                                    
																			if(isset($grades)){
														                        $r=$grades->getData();//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade) {
																			          // pr creer bulletin pr ceux ki ont au moins 1 note

                                                                                                                                            if($grade->grade_value!=null)																					                                            
                                                                                                                                                {  $temoin_has_note=true;	
									
									                                              
																					
                                                                                                                                                         $subject_average = round(Grades::model()->getSubjectAverage($acad_,$id_past_period,$id_course), 2);                          
																			
                                                                                                                                                         if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                            {  $td_app_av ='<td class="period" > '.$subject_average.' </td>';
                                                                                                                                                               $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$k][2]), 2 ),$acad).' </td>';
                                                                                                                                                               $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                                            }
                                                                                                                                                           elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                             {    
                                                                                                                                                                   if($display_student_stage ==1)
                                                                                                                                                                   {      $td_app_av ='';
                                                                                                                                                                       $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$k][2]), 2 ),$acad).' </td>';
                                                                                                                                                                   }
                                                                                                                                                                   elseif($display_student_stage ==0)
                                                                                                                                                                   {   $td_app_av ='<td class="period_side1" > '.$subject_average.' </td>';
                                                                                                                                                                      $td_app_ap ='';
                                                                                                                                                                   }

                                                                                                                                                                   $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade

                                                                                                                                                                  //if($display_average_class ==1) 
                                                                                                                                                                   //$td_app_av ='<td class="period" > '.$subject_average.' </td>';



                                                                                                                                                             }
                                                                                                                                                           elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                 {   $td_app_av ='';
                                                                                                                                                                     $td_app_ap ='';
                                                                                                                                                                     $td_app1 ='<td class="period_side1"> </td>';//1td pou maxgrade
                                                                                                                                                                 }
                                                                                                                                                         //les colonnes notes suivant le nbre d'etape anterieur
                                                                                                                                                                     $grade_value = $grade->grade_value;
                                                                                                                                                                $passCours = getPassingGradeCourse($dataProvider[$k][0],$acad);
                                                                                                                                                                
                                                                                                                                                                if($grade_value<$passCours)
                                                                                                                                                                    $grade_value = '<u>'.$grade->grade_value.'</u>';
                                                                                                                                                                
																		           echo ' <td class="period" > '.$grade_value.' </td>'.$td_app_av.' <td class="period_side1" > <b>'.$dataProvider[$k][2].'</b> </td>'.$td_app_ap;
														//fin...			           
														                             
																			            }
																			           else
																			             echo ' <td class="period" > --- </td>'.$td_app1;
																			             
																			            // $max_grade=$max_grade+$dataProvider[$k][2];
																			             
   															                      }//fin foreach grades
																				}
																			  else
																			  echo ' <td class="period" > --- </td>'.$td_app1;
																			  
																			  
													  	                   } //fin isset grades
													  	                   
													  	                   
													  	                   
																	   }//fin foreach past_period
																	   //$careAbout=false;
																	   if($dataProvider[$k][4]!=NULL)
																	      {  
																	      	   $id_course = $dataProvider[$k][4];
																	      	
																	         
																	         //si kou a evalye pou peryod sa
																				$old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$this->room_id,$this->evaluation_id);         
															                   if($old_subject_evaluated)
															                     { $grades=Grades::model()->searchForReportCard($condition,$student_id,$id_course,$this->evaluation_id);
															                        $careAbout=$old_subject_evaluated; 
															                     }
															                   else
															                     {  $grades=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$k][0],$this->evaluation_id);
															                       $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$this->room_id,$this->evaluation_id); 
															                     }
															                     
																	           
		
																	    }
																	  else
																	    {  $id_course = $dataProvider[$k][0];
																	        $grades=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$k][0],$this->evaluation_id);
																	        $careAbout=$this->isSubjectEvaluated($id_course,$this->room_id,$this->evaluation_id); 
																	    } 
																	    
																	            
													                    if(($careAbout))//if(($careAbout)||($pastp!=null) )
													                         $max_grade=$max_grade+$dataProvider[$k][2];
																	   
																	   //Grades for the current period
																	  
                                                                                                                                              if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="period_side1"> </td>';//1td pou maxgrade
                                                                                                                                                    }
																			if(isset($grades)){
														                        $r=$grades->getData();//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade) {
																			   
																			        if($grade->grade_value!=null)// pr creer bulletin pr ceux ki ont au moins 1 note
																					  { $temoin_has_note=true;	
									//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&                       
									                                              //les colonnes notes suivant le nbre d'etape anterieur
                                                                                                                                                                          
                                                                                                                                                                          $subject_average = round(Grades::model()->getSubjectAverage($acad_,$this->evaluation_id,$id_course), 2);
                                                                                                                                                                     
                                                                                                                                                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                                                  {  $td_app_av ='<td class="period" > '.$subject_average.' </td>';
                                                                                                                                                                                     $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$k][2]), 2 ),$acad).' </td>';
                                                                                                                                                                                     $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                                                                  }
                                                                                                                                                                                 elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                                                   {    
                                                                                                                                                                                         if($display_student_stage ==1)
                                                                                                                                                                                         {      $td_app_av ='';
                                                                                                                                                                                             $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$k][2]), 2 ),$acad).' </td>';
                                                                                                                                                                                         }
                                                                                                                                                                                         elseif($display_student_stage ==0)
                                                                                                                                                                                         {   $td_app_av ='<td class="period_side1" > '.$subject_average.' </td>';
                                                                                                                                                                                            $td_app_ap ='';
                                                                                                                                                                                         }

                                                                                                                                                                                         $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgradfe

                                                                                                                                                                                        //if($display_average_class ==1) 
                                                                                                                                                                                         //$td_app_av ='<td class="period" > '.$subject_average.' </td>';



                                                                                                                                                                                   }
                                                                                                                                                                                 elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                                       {   $td_app_av ='';
                                                                                                                                                                                           $td_app_ap ='';
                                                                                                                                                                                           $td_app1 ='<td class="period_side1"> </td>';//1td pou maxgrade
                                                                                                                                                                                       }

																		           if($careAbout)
																		              {  $tot_grade = $tot_grade + $grade->grade_value;
																		              
																		                  $grade_value = $grade->grade_value;
                                                                                                                                                                $passCours = getPassingGradeCourse($dataProvider[$k][0],$acad);
                                                                                                                                                                
                                                                                                                                                                if($grade_value<$passCours)
                                                                                                                                                                    $grade_value = '<u>'.$grade->grade_value.'</u>';
                                                                                                                                                                
                                                                                                                                                                echo ' <td class="period" > '.$grade_value.' </td>'.$td_app_av.' <td class="period_side1" > <b>'.$dataProvider[$k][2].'</b> </td>'.$td_app_ap;
																		                   
																		                   
							 											              
																		                }
														//fin...			        
                                                         
																				   
																					  
																					   }
																			           else
																			             echo ' <td class="period" > --- </td>'.$td_app1;
																			              
																			           
   															                      }//fin foreach grades
																				}
																			  else
																			  echo ' <td class="period" > --- </td>'.$td_app1;
													  	                   } //fin isset grades
																	  
                                                                   }//fin past !=null
                                                           else //if past_period null, get grades for the current period
                                                             {          
                                                               if($dataProvider[$k][4]!=NULL)
															      {  $id_course = $dataProvider[$k][4];
															         //si kou a evalye pou peryod sa
																		$old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$this->room_id,$this->evaluation_id);         
													                   if($old_subject_evaluated)
													                     { $grades=Grades::model()->searchForReportCard($condition,$student_id,$id_course,$this->evaluation_id);
													                        $careAbout=$old_subject_evaluated; 
													                     }
													                   else
													                     {  $grades=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$k][0],$this->evaluation_id);
													                       $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$this->room_id,$this->evaluation_id); 
													                     }

															      }
															  else
															    {  $id_course = $dataProvider[$k][0];
															        $grades=Grades::model()->searchForReportCard($condition,$student_id,$dataProvider[$k][0],$this->evaluation_id);
															        $careAbout=$this->isSubjectEvaluated($id_course,$this->room_id,$this->evaluation_id); 
															    }
                                                             	
                                                              
                                                              
                                                             	                
													                    if(($careAbout) )//if( ($careAbout)||($pastp!=null) )
													                         $max_grade=$max_grade+$dataProvider[$k][2];

                                                             	//Grades for the current period                                       
                                                                                                                                          if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="period_side1"> </td>';//1td pou maxgrade
                                                                                                                                                    }
																	  // $grades=Grades::model()->searchForReportCard($condition,$student_id,$id_course,$this->evaluation_id);
																			if(isset($grades)){
														                        $r=$grades->getData();//return a list of  objects
															                 if($r!=null)
																			   { foreach($r as $grade) {
																			   
																			        if($grade->grade_value!=null)// pr creer bulletin pr ceux ki ont au moins 1 note
																					  { $temoin_has_note=true;	
									//&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&
									                                              //les colonnes notes suivant le nbre d'etape anterieur
										                                                    
																		           $subject_average = round(Grades::model()->getSubjectAverage($acad_,$this->evaluation_id,$id_course), 2);
                                                                                                                                                           if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app_av ='<td class="period" > '.$subject_average.' </td>';
                                                                                                                                                  $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$k][2]), 2 ),$acad).' </td>';
                                                                                                                                                  $td_app1 ='<td class="period" > --- </td><td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                      if($display_student_stage ==1)
                                                                                                                                                      {      $td_app_av ='';
                                                                                                                                                          $td_app_ap ='<td class="period_side1" > '.appreciationAverageShortText($section_id,round( (($grade->grade_value * 100)/$dataProvider[$k][2]), 2 ),$acad).' </td>';
                                                                                                                                                      }
                                                                                                                                                      elseif($display_student_stage ==0)
                                                                                                                                                      {   $td_app_av ='<td class="period_side1" > '.$subject_average.' </td>';
                                                                                                                                                         $td_app_ap ='';
                                                                                                                                                      }
                                                                                                                                                      
                                                                                                                                                      $td_app1 ='<td class="period_side1" > --- </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade

                                                                                                                                                     //if($display_average_class ==1) 
                                                                                                                                                      //$td_app_av ='<td class="period" > '.$subject_average.' </td>';
                                                                                                                                                       
                                                                                                                                                     
                                                                                                                                                    
                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av ='';
                                                                                                                                                        $td_app_ap ='';
                                                                                                                                                        $td_app1 ='<td class="period_side1"> </td>'; //1td pou maxgrade
                                                                                                                                                    }
                                                                                                                                                           
																		               if($careAbout)
																		                 {  $tot_grade = $tot_grade + $grade->grade_value;
																		                      $grade_value = $grade->grade_value;
                                                                                                                                                                $passCours = getPassingGradeCourse($dataProvider[$k][0],$acad);
                                                                                                                                                                
                                                                                                                                                                if($grade_value<$passCours)
                                                                                                                                                                    $grade_value = '<u>'.$grade->grade_value.'</u>';
                                                                                                                                                                
                                                                                                                                                                echo ' <td class="period" > '.$grade_value.' </td>'.$td_app_av.' <td class="period_side1" > <b>'.$dataProvider[$k][2].'</b> </td>'.$td_app_ap;
																		                 
																		                      
																		                    
																		                   }
														//fin...			
                                                       
                                                      																					   
																					   }
																			           else
																			             echo ' <td class="period" > --- </td>'.$td_app1;
																			             
																			             
   															                      }//fin foreach grades
																				}
																			  else
																			    echo ' <td class="period" > --- </td>'.$td_app1;
													  	                   } //fin isset grades
																	  

                                                             }//fin past_period ==null
                                                             
                                                  
													
											
											
											  
									 echo ' </tr>';   
                                                                         
                                                                    }
                                                                                                             
                                                                                                             $i++;
														       						                                        
                                                               
														$k=$k+1;
										 }
                                                                                 
                                                                                 
                                                                                 
                                  	//check to include discipline grade
//check to include discipline grade

if($include_discipline==1)
  {    												
  	     echo '<tr class="'.$class.'" > <td class="subject_single"> '.Yii::t('app','Discipline').' /<b>'.$max_grade_discipline.'</b>  </td>';                                
  	                                     
  	                                     $summary_grade_discipline_count=0;
  	                                     $summary_grade_discipline=0;
                                             $colspan=3;
                                             
                                             if( ($display_average_class==1)&&($display_student_stage ==1) )
                                               {  $colspan=3;
                                               }
                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                {    
                                                  $colspan=2;

                                                }
                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                    {  $colspan=1;
                                                    }
  	                                     
  	                                      if($pastp!=null)
														   {  
															  foreach($pastp as $id_past_period)
															    {   // To find period name in in evaluation by year 
                                                                    $period_acad_id =null;
			                                                        //$result=EvaluationByYear::model()->searchPeriodName($id_past_period);
			                                                        $period_acad_id=ReportCard::searchPeriodNameForReportCard($id_past_period)->id;
																		
			                                                               // end of code 
																															  	                   
													  	               		$grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($student_id, $period_acad_id);
																		  	 
																		  	 $summary_grade_discipline_count++;
																		  	 
																		  	 $summary_grade_discipline=$grade_discipline;
																		  	 
																		//  	if(($grade_discipline==0))
																	  	//   echo '<td class="period" > --- </td>';
																	  	// else
																	  	    echo '<td class="period_side1" colspan="'.$colspan.'"> '.$grade_discipline.' </td>';	     
																	
																		  
													  	                   
																 }//fin foreach past_period
																
																 //current period
																 // To find period name in in evaluation by year 
                                                                 $period_acad_id =null;
			                                                       // $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
			                                                       $period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
																		
			                                                               // end of code 
																	 $grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($student_id, $period_acad_id);
																	  	 $max_grade = $max_grade + $max_grade_discipline;
																	  	 $tot_grade = $tot_grade + $grade_discipline;
																	  	
																	     	$summary_grade_discipline_count++;
																		  	 
																		  	 $summary_grade_discipline=$grade_discipline;
																		  	 
																	  	//if(($grade_discipline==0))
																	  	//   echo '<td class="period" > --- </td>';
																	  	// else
																	  	    echo '<td class="period_side1" colspan="'.$colspan.'"> '.$grade_discipline.' </td>';	     
																
																	  						  	                   
																	  
                                                              }//fin past !=null
                                                           else //if past_period null, get grades for the current period
                                                             {          
                                                                 //current period
																	// To find period name in in evaluation by year 
                                                                 $period_acad_id =null;
			                                                       // $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
			                                                       $period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
																		
			                                                               // end of code 
																	 $grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($student_id, $period_acad_id);
																	  	 $max_grade = $max_grade + $max_grade_discipline;
																	  	 $tot_grade = $tot_grade + $grade_discipline;
																	  	
																	  	 $summary_grade_discipline_count++;
																		  	 
																		  	 $summary_grade_discipline=$grade_discipline;
																	  	 
																	  	 //if(($grade_discipline==0))
																	  	 //  echo '<td class="period" colspan="3"> --- </td>';
																	  	 //else
																	  	    echo'<td class="period_side1" colspan="'.$colspan.'"> '.$grade_discipline.' </td>';	     
																
																	  		
                                                               }
                                                               
                                                               
                                                                  
                                /*                               
                                  if(($max_grade_discipline==null)||($max_grade_discipline==0))
                                     echo ' <td class="period" colspan="3"><b> --- </b></td></tr>';
                                  else
                                     echo ' <td class="period" colspan="3"><b> '.$max_grade_discipline.'</b></td></tr>';     
                                 *
                                 */                     	 
	}
  
   $average=0;  	$general_average=0; 
						  
						  if(($average_base==10)||($average_base==100)) 
							   { if($max_grade!=0)  
							       $average=round(($tot_grade/$max_grade)*$average_base,2);
							   }
							  else			
                                                            {	$average =null;	
                                                                $this->messageWrongAverageBase= true;
                                                              }
 
          echo '<tr class="sommes"><td class="periodsommes"><b><i>'.Yii::t('app','Total').' '.Yii::t('app','Other courses').'</i></b></td>';
      if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="periodsommes2_red" > </td><td class="periodsommes2_red_side1" ><b>'.$max_grade.'</b> </td><td class="periodsommes2_red_side1" > </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="periodsommes2_red_side1" >  </td><td class="periodsommes2_red_side1" ><b>'.$max_grade.'</b> </td>';

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="periodsommes2_red_side1" ><b>'.$max_grade.'</b> </td>';
                                                                                                                                                    }
												           if($pastp!=null)
														     {  
															    foreach($pastp as $id_past_period)
															      {
																		$data_=Grades::model()->getDataAverageByPeriod($acad,$id_past_period,$student_id);
																			if(isset($data_))
																			{
														                        $rs=$data_->getData();//return a list of  objects
															                 if($rs!=null)
																			   { foreach($rs as $_data) 
																			       {
																			 
																				     if($_data->sum!=null)
																				         echo '<td class="periodsommes2_red_side"> <b>'.$_data->sum.'</b> </td>'.$td_app1;
																				     else
																				         echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
														                            }//fin foreach _data
																				}
																				else
																			          echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                     } //fin isset data_
																	   }//fin foreach past period
																	  
																	  //total for the current period 
																	
																			      if($temoin_has_note)
                                                                                                                                                                  echo '<td class="periodsommes2_red_side"> <b>'.$tot_grade.'</b> </td>'.$td_app1;
																			       else
																			         echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                  
													  	                   
																	   
                                                                   }//fin past period!=null
										                         else
										                          {
										                           //total for the current period 
																
																				  if($temoin_has_note)
																			         echo '<td class="periodsommes2_red_side"> <b>'.$tot_grade.'</b> </td>'.$td_app1;
																			      else
																			        echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                  
	
										                          }
										                          
										                           
										                          
												              //echo '<td class="periodsommes2_red"> <b>'.$max_grade.' </b></td><td class="period"> --- </td><td class="period"> --- </td>';
												echo '  </tr>
												  
												  <tr class="sommes"><td class="periodsommes"><b><i>'.Yii::t('app','Average').' '.Yii::t('app','Other courses').'</i></b></td>';
														$general_average_debase=0;
                                                                                                                $compter_p_general_av_debase =1;
                                                                                                                
                                                                                                                
														
													 if($pastp!=null)
														     {  
															    foreach($pastp as $id_past_period)
															      {
																		$data_=Grades::model()->getDataAverageByPeriod($acad,$id_past_period,$student_id);
																			if(isset($data_)){
														                        $rs=$data_->getData();//return a list of  objects
															                 if($rs!=null)
																			   { foreach($rs as $_data) 
																			       {
                                                                                                                                                                  if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$_data->average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td></td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td></td><td class="period_side1"> </td>';  //ajoutew 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$_data->average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td></td><td class="period_side1"> </td>';  //ajoute 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='</td><td class="period_side1"> </td>';//1td pou maxgrade
                                                                                                                                                    }
																			         if($use_period_weight==1)
																			          {
																			          	 //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($id_past_period);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average = round( ($general_average + ( ($_data->average*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average = round( ($general_average + $_data->average), 2);
																			       
                                                                                                                                                              $compter_p_general_av_debase ++;
                                                                                                                                                              
																				     echo '<td class="period_side"> <b>'.$_data->average.'</b> </td>'.$td_app_av.'</td><td class="period_side1"> </td>'.$td_app_ap;
														                          }//fin foreach _data 
																				}
																				else
																			          echo '<td class="period_side"> --- </td>'.$td_app1;
													  	                   } //fin isset data_
																	   }//fin foreach past period
																	  
																	  //average for the current period 
																	 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='';
                                                                                                                                                    }
																			      	if($use_period_weight==1)
																			          {  
																						  //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($this->evaluation_id);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average = round( ($general_average + (($average*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average = round( ($general_average + $average), 2);
																			            
																			      	
																			      	if($average!=0)
																			           {
																			           	 echo '<td class="period_side"> <b>'.$average.'</b> </td>'.$td_app_av.'<td class="period_side1"> </td>'.$td_app_ap;
																			           }
																			         else 
																			           echo '<td class="period_side"> --- </td>'.$td_app1;
			 																 
													  	                    
                                                                   }//fin isset period
                                                                 else
                                                                   {
                                                                   	//average for the current period 
																	  
																	if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='';
                                                                                                                                                    }	      	 
                                                                                                                                                                if($use_period_weight==1)
																			          {  
																						  //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($this->evaluation_id);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average = round( ($general_average + (($average*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average = round( ($general_average + $average), 2);
																			            
																			            
																			         
																			         if($average!=0)
																			           { 
																			               echo '<td class="period_side"> <b>'.$average.'</b> </td>'.$td_app_av.'<td class="period_side1"> </td>'.$td_app_ap;
																			           }
																			         else
																			           echo '<td class="period_side"> --- </td>'.$td_app1;
																			     
                                                                   }
                                                                   
                                                                   
												     
												         	   	
												         	   						     
												echo '</tr>';                                                                                
                                                                                 
                                                
   $place_text=null;                     
if($place===1)		
 $place_text=$place.'<span class="place">'.Yii::t('app','st').'</span>';
                                                                                                                                        elseif($place===2)
                                                                                                                                            $place_text=$place.'<span class="place">'.Yii::t('app','nd').'</span>';
																																			elseif($place===3)
                                                                                                                                            $place_text=$place.'<span class="place">'.Yii::t('app','rd').'</span>';
                                                                                                                                               else
  //ajoute total not debase sou total lot not yo
      $tot_grade = $tot_grade + $tot_grade_debase;
      $max_grade = $max_grade + $max_grade_debase;                                                                                                                                               $place_text=$place.'<span class="place">'.Yii::t('app','th').'</span>'; 	          
                                                                                     
      $average=0;  	$general_average=0; 
						  
						  if(($average_base==10)||($average_base==100)) 
							   { if($max_grade!=0)  
							       $average=round(($tot_grade/$max_grade)*$average_base,2);
							   }
							  else			
                                                            {	$average =null;	
                                                                $this->messageWrongAverageBase= true;
                                                              }                                        
												 
										// pour le depassement dans BIRD echo '<tr class="subjectheadnote_white_tr"><td colspan="'.$compter_p.'"> </td></tr>';
										
										          echo'<tr class="sommes2"><td class="periodsommes2"><b>'.Yii::t('app','Total: ').'  </td>';
											                    
                                                                                                         if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  $td_app1 ='<td class="periodsommes2_red" > </td><td class="periodsommes2_red_side1" ><b>'.$max_grade.'</b> </td><td class="periodsommes2_red_side1" > </td>';
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  $td_app1 ='<td class="periodsommes2_red_side1" >  </td><td class="periodsommes2_red_side1" ><b>'.$max_grade.'</b> </td>';

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app1 ='<td class="periodsommes2_red_side1" ><b>'.$max_grade.'</b> </td>';
                                                                                                                                                    }
												           if($pastp!=null)
														     {  
															    foreach($pastp as $id_past_period)
															      {
																		$data_=Grades::model()->getDataAverageByPeriod($acad,$id_past_period,$student_id);
																			if(isset($data_))
																			{
														                        $rs=$data_->getData();//return a list of  objects
															                 if($rs!=null)
																			   { foreach($rs as $_data) 
																			       {
																			 
																				     if($_data->sum!=null)
																				         echo '<td class="periodsommes2_red_side"> <b>'.$_data->sum.'</b> </td>'.$td_app1;
																				     else
																				         echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
														                            }//fin foreach _data
																				}
																				else
																			          echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                     } //fin isset data_
																	   }//fin foreach past period
																	  
																	  //total for the current period 
																	
																			      if($temoin_has_note)
                                                                                                                                                                  echo '<td class="periodsommes2_red_side"> <b>'.$tot_grade.'</b> </td>'.$td_app1;
																			       else
																			         echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                  
													  	                   
																	   
                                                                   }//fin past period!=null
										                         else
										                          {
										                           //total for the current period 
																
																				  if($temoin_has_note)
																			         echo '<td class="periodsommes2_red_side"> <b>'.$tot_grade.'</b> </td>'.$td_app1;
																			      else
																			        echo '<td class="periodsommes2_red_side"> --- </td>'.$td_app1;
													  	                  
	
										                          }
										                          
										                           
										                          
												              //echo '<td class="periodsommes2_red"> <b>'.$max_grade.' </b></td><td class="period"> --- </td><td class="period"> --- </td>';
												echo '  </tr>
												  
												  <tr class="sommes"><td class="periodsommes"><b>'.Yii::t('app','Average: ').'</b></td>';
														$general_average=0;
                                                                                                                $compter_p_general_av =1;
                                                                                                                
                                                                                                                
														
													 if($pastp!=null)
														     {  
															    foreach($pastp as $id_past_period)
															      {
																		$data_=Grades::model()->getDataAverageByPeriod($acad,$id_past_period,$student_id);
																			if(isset($data_)){
														                        $rs=$data_->getData();//return a list of  objects
															                 if($rs!=null)
																			   { foreach($rs as $_data) 
																			       {
                                                                                                                                                                  if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$_data->average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td><td class="period_side1"> </td>';//ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td><td class="period_side1"> </td>';  //ajoute 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$_data->average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td><td class="period_side1"> </td>'; //ajoute 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='';
                                                                                                                                                    }
																			         if($use_period_weight==1)
																			          {
																			          	 //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($id_past_period);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average = round( ($general_average + ( ($_data->average*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average = round( ($general_average + $_data->average), 2);
																			       
                                                                                                                                                              $compter_p_general_av ++;
                                                                                                                                                              
																				     echo '<td class="period_side"> <b>'.$_data->average.'</b> </td>'.$td_app_av.'<td class="period_side1"> </td>'.$td_app_ap;
														                          }//fin foreach _data 
																				}
																				else
																			          echo '<td class="period_side"> --- </td>'.$td_app1;
													  	                   } //fin isset data_
																	   }//fin foreach past period
																	  
																	  //average for the current period 
																	 if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td><td class="period_side1"> </td>';  //ajute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td><td class="period_side1"> </td>';  //ajoute 1td poou maxgrade
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td><td class="period_side1"> </td>';  //ajoute 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='';
                                                                                                                                                    }
																			      	if($use_period_weight==1)
																			          {  
																						  //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($this->evaluation_id);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average = round( ($general_average + (($average*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average = round( ($general_average + $average), 2);
																			            
																			      	
																			      	if($average!=0)
																			           {
																			           	 echo '<td class="period_side"> <b>'.$average.'</b> </td>'.$td_app_av.'<td class="period_side1"> </td>'.$td_app_ap;
																			           }
																			         else 
																			           echo '<td class="period_side"> --- </td>'.$td_app1;
			 																 
													  	                    
                                                                   }//fin isset period
                                                                 else
                                                                   {
                                                                   	//average for the current period 
																	  
																	if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                               {  
                                                                                                                                                    $td_app_av= '<td class="period"> </td>';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td><td class="period_side1"> </td>';  //ajoute 1td pou maxgrade
                                                                                                                                               }
                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                {    
                                                                                                                                                  if($display_average_class==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                                                   $td_app_ap='';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td><td class="period_side1"> </td>';  //ajoute 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                 elseif($display_student_stage==1)
                                                                                                                                                  {
                                                                                                                                                      $td_app_av= '';
                                                                                                                                                   $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$average,$acad).'</b> </td>';
                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td><td class="period_side1"> </td>';  //ajoute 1td pou maxgrade
                                                                                                                                                  }
                                                                                                                                                  

                                                                                                                                                }
                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                    {   $td_app_av= '';
                                                                                                                                                        $td_app_ap='';
                                                                                                                                                        $td_app1 ='';
                                                                                                                                                    }	      	 
                                                                                                                                                                if($use_period_weight==1)
																			          {  
																						  //jwenn peryod eval sa ye
																		  	    	   	  $p_acad = EvaluationByYear::model()->getPeriodNameByEvaluationID($this->evaluation_id);
																		  	    	   	  
																		  	    	   	   foreach($p_acad as $p_weight)
																		  	    	   	     {  
																		  	    	   	     	if($p_weight['weight']!=null)
																		  	    	   	           $general_average = round( ($general_average + (($average*$p_weight['weight'])/100) ), 2);
																		  	    	   	              
																		  	    	   	      }
																		  	    	   	     
																			           }
																			          else
																			            $general_average = round( ($general_average + $average), 2);
																			            
																			            
																			         
																			         if($average!=0)
																			           { 
																			               echo '<td class="period_side"> <b>'.$average.'</b> </td>'.$td_app_av.'<td class="period_side1"> </td>'.$td_app_ap;
																			           }
																			         else
																			           echo '<td class="period_side"> --- </td>'.$td_app1;
																			     
                                                                   }
                                                                   
                                                                   
												     
												         	   	
												         	   						     
												echo '</tr>';                                                                                
                                                                                 
 			
if($include_place==1)
  { 												  
				echo '	  <tr class="sommes1"><td class="periodsommes"><b>'.Yii::t('app','Place:').' </b></td>';
												     if($pastp!=null)
														     {  
															    foreach($pastp as $id_past_period)
															      {
																		$data_=Grades::model()->getDataAverageByPeriod($acad,$id_past_period,$student_id);
																			if(isset($data_)){
														                        $rs=$data_->getData();//return a list of  objects
															                 if($rs!=null)
																			   { foreach($rs as $_data) {
																			   
																			        $place_text1="";
																					    if($_data->place==1)								        
                                                                                          $place_text1=$_data->place.'<span class="place">'.Yii::t('app','st').'</span>';
                                                                                        elseif($_data->place==2)
                                                                                              $place_text1=$_data->place.'<span class="place">'.Yii::t('app','nd').'</span>';
																				            elseif($_data->place==3)
                                                                                                  $place_text1=$_data->place.'<span class="place">'.Yii::t('app','rd').'</span>';
                                                                                                 else
                                                                                                    $place_text1=$_data->place.'<span class="place">'.Yii::t('app','th').'</span>'; 
                                                                                                 
                                                                                                                                                                                    if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                                                       {  
                                                                                                                                                                                           $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                                                                       }
                                                                                                                                                                                      elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                                                        {    
                                                                                                                                                                                           $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                                                         }
                                                                                                                                                                                      elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                                            {   $td_app1 ='';
                                                                                                                                                                                            }
																									
																					   echo '<td class="period_side" > '.$place_text1.' </td>'.$td_app1;
																					  
														                          }//fin foreach _data
																				}
																				else
                                                                                                                                                                {   if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                                                       {  
                                                                                                                                                                                           $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                                                                       }
                                                                                                                                                                                      elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                                                        {    
                                                                                                                                                                                           $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                                                         }
                                                                                                                                                                                      elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                                            {   $td_app1 ='';
                                                                                                                                                                                            }
                                                                                                                                                                                    
                                                                                                                                                                              echo '<td class="period_side" > --- </td>'.$td_app1;
                                                                                                                                                                }
													  	                   } //fin isset data_
																	   }//fin foreach period
																	   
																	                       if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                               {  
                                                                                                                                                                   $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                                               }
                                                                                                                                                              elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                                {    
                                                                                                                                                                   $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                                 }
                                                                                                                                                              elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                    {   $td_app1 ='';
                                                                                                                                                                    }

																				if($place_text!=null)
																			           echo '<td class="period_side" > '.$place_text.' </td>'.$td_app1;
																			      else
																			           echo '<td class="period_side" > --- </td>'.$td_app1;
													  	                   
																	   
                                                                   }//fin past period!=null
                                                                else
                                                                  {   //place for the current period 
                                                                                                                                                        if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                                                           {  
                                                                                                                                                               $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                                                           }
                                                                                                                                                          elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                                                            {    
                                                                                                                                                               $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                                                             }
                                                                                                                                                          elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                                                                {   $td_app1 ='';
                                                                                                                                                                }

																			     if($place_text!=null)
																			          echo '<td class="period_side" > '.$place_text.' </td>'.$td_app1;
																			      else
																			           echo '<td class="period_side" > --- </td>'.$td_app1;
													  	                  
													  	                 
                                                                        } 
												        
                                                                                                      
                                                                                                                
												              echo '</tr>';
				 
				 
  }			 
                                                                                
 
		if( ((isset($_POST['calculate_g_average']))&&($_POST['calculate_g_average']==true)) || ($use_period_weight==1) ) // make G-average								  
		   { 
					if($pastp!=null)
						{  		
							//calculate the general average
							if($use_period_weight==1)
							  $general_average=round($general_average, 2);
							else
							    $general_average=round(($general_average/($compter_p_general_av)),2);
							  
							if( ($display_average_class==1)&&($display_student_stage ==1) )
                                                                                                                       {  
                                                                                                                            $td_app_av= '<td class="period"> </td>';
                                                                                                                           $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$general_average,$acad).'</b> </td>';
                                                                                                                           $td_app1 ='<td class="period"> </td><td class="period_side1"> </td>';
                                                                                                                       
                                                                                                                           $colspan = $compter_p;
                                                                                                                       }
                                                                                                                      elseif( ( ($display_average_class==1)&&($display_student_stage ==0) )||( ($display_average_class==0)&&($display_student_stage ==1) ) )
                                                                                                                        {    
                                                                                                                          if($display_average_class==1)
                                                                                                                          {
                                                                                                                              $td_app_av= '<td class="period_side1"> </td>';
                                                                                                                           $td_app_ap='';
                                                                                                                           $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                          }
                                                                                                                         elseif($display_student_stage==1)
                                                                                                                          {
                                                                                                                              $td_app_av= '';
                                                                                                                           $td_app_ap='<td class="period_side1"> <b>'.appreciationAverageShortText($section_id,$general_average,$acad).'</b> </td>';
                                                                                                                           $td_app1 ='<td class="period_side1"> </td>';
                                                                                                                          }
                                                                                                                          
                                                                                                                          $colspan = $compter_p-1;


                                                                                                                        }
                                                                                                                      elseif( ($display_average_class==0)&&($display_student_stage ==0) ) 
                                                                                                                            {   $td_app_av= '';
                                                                                                                                $td_app_ap='';
                                                                                                                                $td_app1 ='';
                                                                                                                                $colspan = $compter_p-2;
                                                                                                                            }
                                                                                                                            
							echo '	<tr class="subjectheadnote_white_tr"><td colspan="'.$colspan.'"></td></tr>
							        				  
												  
												    <tr class="sommes1"><td class="periodsommes"><b>'.Yii::t('app','General Average:').' </b></td>'; 
												     
                                                                                                                     
                                                                                                                            
                                                                                                                            
                                                                                                                        if($general_average!=null)
															      echo '<td class="period" > <b>'.$general_average.'</b></td>'.$td_app_av.$td_app_ap;
															 else
																  echo '<td class="period"> --- </td>'.$td_app1;
																					 
														                       
																	  												            					  
												              echo' </tr>			  
												  ';
						}

		     }
	    
		     
?>

     </table>
    </div>
 
<?php
if(($tardy_absence_display== 1)||($include_discipline==0))
  {
   echo '<table  class="discipline" >
          <tr ><td style="border:0px thin #F9F9F9 ;"></td>';
		   //on ajoute les colonnes suivant le nbre d'etape anterieur
			 if($pastp!=null)
				{ $compter_p=1; 
				  foreach($pastp as $id_past_period)
					{
					  echo '<td class="td_discipline"> <span style="font-size:8px;"> <b>'.$this->searchPeriodName($id_past_period).'</b> </span> </td>';
					 
					  
					  		$compter_p++;										
		             }
		            
		            echo '<td class="td_discipline"> <span style="font-size:8px;"> <b>'.$this->searchPeriodName($this->evaluation_id).'</b> </span> </td>'; 
		            
		              
		             
				 }
			else
			  {
			     	echo '<td class="td_discipline"> <span style="font-size:8px;"> <b>'.$period_exam_name.'</b> </span> </td>'; 

			  }
	
	      
	          
	
          
         echo ' </tr>';
	}	                                                                                
                                                                                 
            
if($tardy_absence_display== 1)
     {          
          echo '<tr><td style="border:1px solid #E4E4E4;"> <span style="font-size:8px;"><b>'.Yii::t('app','RETARD').'</b></span></td>';//on ajoute les colonnes suivant le nbre d'etape anterieur
			
			$period_acad_id = '';
			
			  if($pastp!=null)
				{  
				  foreach($pastp as $id_past_period)
					{
						 //$result=EvaluationByYear::model()->searchPeriodName($id_past_period);
						 
						 $period_acad_id=ReportCard::searchPeriodNameForReportCard($id_past_period)->id;
															
					  
						$retard = RecordPresence::model()->getTotalRetardByExam($period_acad_id, $student_id, $acad);
						echo '<td class="td_discipline"> '.$retard.' </td>';
																		
		             }
		             
		                // $result=EvaluationByYear::model()->searchPeriodName($id_past_period);
																				  
		               $period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
		               
		               $retard = RecordPresence::model()->getTotalRetardByExam($period_acad_id, $student_id, $acad);
		               echo '<td class="td_discipline"> </td>';
		               
				 }
			  else
			    {
			         //  $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
						
						$period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
						
			          $retard = RecordPresence::model()->getTotalRetardByExam($period_acad_id, $student_id, $acad);
			          
			          echo '<td class="td_discipline"> '.$retard.' </td>';	
			     }
			     
			   
			     
		echo ' </tr>
		   <tr><td style="border:1px solid #E4E4E4;"><span style="font-size:8px;"><b>'.Yii::t('app','ABSENCE').'</b></span></td>';//on ajoute les colonnes suivant le nbre d'etape anterieur
			 if($pastp!=null)
				{  
				  foreach($pastp as $id_past_period)
					{
						 //$result=EvaluationByYear::model()->searchPeriodName($id_past_period);
															
						$period_acad_id=ReportCard::searchPeriodNameForReportCard($id_past_period)->id;									
					 
						$absc = RecordPresence::model()->getTotalPresenceByExam($period_acad_id, $student_id, $acad);
						echo '<td class="td_discipline"> '.$absc.' </td>';
																		
		                           }
		             
		                //  $result=EvaluationByYear::model()->searchPeriodName($id_past_period);
						
						$period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
						
						$absc = RecordPresence::model()->getTotalPresenceByExam($period_acad_id, $student_id, $acad);
		               echo '<td class="td_discipline"> '.$absc.' </td>';

				 }
			   else
			     {
			         
			         //  $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
						
						$period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
						
					  $absc = RecordPresence::model()->getTotalPresenceByExam($period_acad_id, $student_id, $acad);
			         echo '<td class="td_discipline"> '.$absc.' </td>';
	
			     }
			   
			   
			     
				 echo '</tr>';
 
  }
  
  //check to include discipline grade
if(($include_discipline!=2)&&($include_discipline!=1))
{
if($include_discipline==0)
  {    												
  	   echo ' <tr><td style="border:1px solid #E4E4E4;"><span style="font-size:8px;"><b>'.Yii::t('app','Discipline').'</b></span></td>';//on ajoute les colonnes suivant le nbre d'etape anterieur
                               
                               $konte_peryod = 0;
				               $mwayen_disiplin = 0;
				               $som_peryod = 0.00;
				               
  	                                      if($pastp!=null)
				                            {  
				                              foreach($pastp as $id_past_period)
					                            {    $period_acad_id = null;
														  //$result=EvaluationByYear::model()->searchPeriodName($id_past_period);
															
															$period_acad_id=ReportCard::searchPeriodNameForReportCard($id_past_period)->id;																	  	                   
													  	                 //check to include discipline grade
																		
																		  	 $grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($student_id, $period_acad_id);
																		  	 
																		  	 $konte_peryod++;
																               
																               $som_peryod +=$grade_discipline;
																		  	 
																		  																			  	 
																		  	//if(($grade_discipline==0))
																	  	   //echo '<td style="border:1px solid #E4E4E4;" > --- </td>';
																	  	 //else
                                                                                                                                                    /** Pour le nouveau college Bird **/
                                                                                                                                                   if(infoGeneralConfig('discipline_ncb')!=NULL || infoGeneralConfig('discipline_ncb') == 1){
                                                                                                                                                       
                                                                                                                                                       $disc_break_point = DiscPeriodBreak::model()->findAllBySql("SELECT * FROM disc_period_break WHERE id_period = $period_acad_id");
                                                                                                                                                       foreach($disc_break_point as $dbp){
                                                                                                                                                           $max_grade_discipline = $dbp->break_point;
                                                                                                                                                       }
                                                                                                                                                   }else{
                                                                                                                                                       $max_grade_discipline = infoGeneralConfig('note_discipline_initiale');
                                                                                                                                                   }  
																	  	    echo '<td class="td_discipline" > '.$grade_discipline.' / '.$max_grade_discipline.' </td>';	     
																	
																		  
													  	                   
																 }//fin foreach past_period
																
																 //current period
																   $period_acad_id = null;
																  // $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
															
															$period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
															
																	 $grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($student_id, $period_acad_id);
																	  	
																	  	$konte_peryod++;
																               
																         $som_peryod +=$grade_discipline;                                            
																	  	
																	  	//if(($grade_discipline==0))
																	  	   //echo '<td style="border:1px solid #E4E4E4;"> --- </td>';
																	  	// else
                                                                                                                                          /** Pour le nouveau college Bird **/
                                                                                                                                                   if(infoGeneralConfig('discipline_ncb')!=NULL || infoGeneralConfig('discipline_ncb') == 1){
                                                                                                                                                       
                                                                                                                                                       $disc_break_point = DiscPeriodBreak::model()->findAllBySql("SELECT * FROM disc_period_break WHERE id_period = $period_acad_id");
                                                                                                                                                       foreach($disc_break_point as $dbp){
                                                                                                                                                           $max_grade_discipline = $dbp->break_point;
                                                                                                                                                       }
                                                                                                                                                   }else{
                                                                                                                                                       $max_grade_discipline = infoGeneralConfig('note_discipline_initiale');
                                                                                                                                                   } 
																	  	    echo '<td class="td_discipline"> '.$grade_discipline.' / '.$max_grade_discipline.' </td>';	     
																
																	  						  	                   
																	  
                                                              }//fin past !=null
                                                           else //if past_period null, get grades for the current period
                                                             {          
                                                                 //current period
                                                                 //current period
																   $period_acad_id = null;
																   //$result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
															
															$period_acad_id=ReportCard::searchPeriodNameForReportCard($this->evaluation_id)->id;
															                                                                
																$grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($student_id,  $period_acad_id);
																       $konte_peryod++;
																         
																        $som_peryod +=$grade_discipline;
								 
									  	
																	  	//if(($grade_discipline==0))
																	  	//   echo'<td style="border:1px solid #E4E4E4;" > --- </td>';
																	  	// else
																	  	    echo '<td class="td_discipline" > '.$grade_discipline.' / '.$max_grade_discipline.' </td>';	     
																
																	  		
                                                               }
                                                               
                                   
           
            echo '</tr>';                          
                                                               	 
	}

}			

if(($tardy_absence_display== 1)||($include_discipline==0))
  {				
  	echo '</table>';
  }

    
echo '																	 
<div class="clear" style="font-size:9px; font-weight:bold; float: right; width: 100%">   
<br/>';

 $section_ = $this->getSectionByStudentId($student_id);
 
 $observation = '( '.$period_exam_name.' ): '.observationReportcard($section_->id,$average,$acad_); //sou tout ane a

$use_mention_summary = infoGeneralConfig('use_mention_summary');	
  if( ($lastEvaluation_done==true) ) //( ($use_mention_summary==1)&&($lastEvaluation_done==true) ) 
	   {
	   	  $summary_mention = '';
	   	  //check  record ID already stored in table decision_finale
		   $is_there = $this->checkDecisionFinale($student_id, $acad);                                                         
								   
				  if((isset($is_there))&&($is_there!=null))
					 {  //yes let's make an update
									     foreach($is_there as $result)
									        $summary_mention = $result["mention"];
									     
					  }
                   
	   	  
	   	  $observation = $observation.' &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.Yii::t('app','Mention').': '.$summary_mention; 
	   	}

	echo strtoupper(Yii::t('app','Notes') ).$observation;	
	 
	 	     

//if($show_observation_line== 1)    
   //provisoirement pour un cas dans BIRD -->  echo '<br/>....................................................................................................................................................................................................................................................................';

echo '</div>  <br/>          
<table class="signature" ><tr>';

 if(($section=="Primaire")||($section=="Fondamental"))
    { 
    	
    	if(($display_administration_signature==1) &&($display_parent_signature!=1))
		{
			 echo '<td class="signature1" style="width:70%;">  </td><td class="signature1" style=" width:30%;">  <hr style="width:100%;" /><div style="text-align: center;">'.$administration_signature_text.'</div> </td>';
			
		 }	
		elseif($display_administration_signature==1)
    	  { echo '<td class="signature1" style=" width:30%;">  <hr style="width:93%;" /> &nbsp;&nbsp;&nbsp;'.$administration_signature_text.' </td>
                      <td class="signature1" style="width:2%;">  </td>';
                      
    	    }
			

		if($display_parent_signature==1)
    	  {  echo '   <td class="signature1" style=" width:30%;"> <hr style="width:93%;" /> &nbsp;&nbsp;&nbsp;'.$parent_signature_text.' </td>';
						
    	   }
    	
    }
  elseif($section=='Secondaire')
    {  
    	
    	
    	if(($display_administration_signature==1) &&($display_parent_signature!=1))
		{
			 echo '<td class="signature1" style="width:70%;">  </td><td class="signature1" style=" width:30%;">  <hr style="width:100%;" /><div style="text-align: center;">'.$administration_signature_text.'</div> </td>';
			
		 }	
		elseif($display_administration_signature==1)
    	  {   echo '<td class="signature1" style=" width:30%;">  <hr style="width:93%;" /> &nbsp;&nbsp;&nbsp;'.$administration_signature_text.' </td>
                      <td class="signature1" style="width:2%;">  </td>';
                      
    	    }
    	    
		
		if($display_parent_signature==1)
    	  { echo '  <td class="signature1" style=" width:30%;"> <hr style="width:93%;" /> &nbsp;&nbsp;&nbsp;'.$parent_signature_text.' </td>';
						
    	  }
    	  
    	  
    	  
    }
   else
    {  
    	
    	
    	if($display_administration_signature==1)
		{
			 echo '<td class="signature1" style="width:70%;">  </td><td class="signature1" style=" width:30%;">  <hr style="width:100%;" /><div style="text-align: center;">'.$administration_signature_text.'</div> </td>';
			
		 }	
		/*if($display_administration_signature==1)
    	  {   echo '<td class="signature1" style=" width:30%;">  <hr style="width:93%;" /> &nbsp;&nbsp;&nbsp;'.$administration_signature_text.' </td>
                      <td class="signature1" style="width:2%;">  </td>';
                      
    	    }
    	    */
		
		
    	  
    	  
    }

     
 echo '</tr></table>';

//if($display_created_date==1)
 // echo ' <div style="float:right; font-weight:normal; font-size:10px; margin-top:-10px;">    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.ChangeDateFormat(date('Y-m-d')).'</div>  </div>   ';

  	




		                         		                         
/* AVERAGE BY PERIOD */
 
				//save average for this period			  
						  $command = Yii::app()->db->createCommand();
						   if($temoin_has_note)//
							{//check if already exit
							  $data =  Grades::model()->checkDataAverageByPeriod($acad,$this->evaluation_id,$student_id);
							  $is_present=false;
							       if($data)
								     $is_present=true;
								  
                                                               if($average!=null)
                                                                 {
                                                                   
                                if($is_present){// yes, update
								
								    $command->update('average_by_period', array(
											'sum'=>$tot_grade,'average'=>$average,'place'=>$place,'date_updated'=>date('Y-m-d'),
										), 'academic_year=:year AND evaluation_by_year=:period AND student=:stud', array(':year'=>$acad, ':period'=>$this->evaluation_id, ':stud'=>$student_id));
                                   }
								 else{// no, insert
								   $command->insert('average_by_period', array(
										'academic_year'=>$acad,
										'evaluation_by_year'=>$this->evaluation_id,
										'student'=>$student_id,
										'sum'=>$tot_grade,
										'average'=>$average,
										'place'=>$place,
										'date_created'=>date('Y-m-d'),
									));
									
								     
								     }
                                                                     
                                                                }
								 
								 
							       }
							
	/* GENERAL AVERAGE (END YEAR DECISION)  */
 
 //save general average for end year decision				  
	        if($this->final_period==true) // this is the last period
	          {     					 
					 $command2 = Yii::app()->db->createCommand();
					 
			   if($pastp!=null) //past evaluation period was included
				 {  
				  if( ((isset($_POST['calculate_g_average']))&&($_POST['calculate_g_average']==true))  ) // make G-average
					  {
                                      
                                             if($general_average!=null)
                                               {
                                                        //check if already exit
                                                       $result=$this->checkDataGeneralAverage($acad,$student_id,$general_average,$this->idLevel);
							  
							if((isset($result))&&($result!=null))
							  {  // yes, update
							       
									  $date_updated=date('Y-m-d');
							     foreach($result as $data)
				                   { 
								      $command2->update('decision_finale', array(
												'general_average'=>$general_average,'date_updated'=>$date_updated,
											), 'id=:ID', array(':ID'=>$data["id"] ));
	                                  
								     }
								   
							  }
							 else
							  { // no, insert
							        $date_created= date('Y-m-d');
							        
									$command2->insert('decision_finale', array(
									'student'=>$student_id,
									'academic_year'=>$acad,
									'general_average'=>$general_average,
									'current_level'=>$this->idLevel,
									'date_created'=>$date_created,
											
										));
							   }
							   
							   
							   //general_average_by_period
							    //save general average for this period
							    $period_acad_id ='';			  
						        $command3 = Yii::app()->db->createCommand();
						       //check if already exit
						       $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
															if(isset($result))
															 {  $result=$result->getData();//return a list of  objects
																foreach($result as $r)
																  {
																	$period_exam_name= $r->name_period;
																   $period_acad_id = $r->id;
																   }
															 }
								  
								  $data___=Grades::model()->checkDataGeralAverageByPeriodForReport($acad,$student_id,$period_acad_id);
                                                                 
								  if((isset($data___))&&($data___!=null))
								  {  // yes, update
						
								    $command3->update('general_average_by_period', array(
											'general_average'=>$general_average,'date_updated'=>date('Y-m-d'),
										), 'academic_year=:year AND academic_period=:period AND student=:stud', array(':year'=>$acad, ':period'=>$period_acad_id, ':stud'=>$student_id));
                                   }
								 else{// no, insert
								   $command3->insert('general_average_by_period', array(
										'academic_year'=>$acad,
										'academic_period'=>$period_acad_id,
										'student'=>$student_id,
										'general_average'=>$general_average,
										'date_created'=>date('Y-m-d'),
									));
									
								     
								     }
							
                                                             }
								     
						      }//end making G-average
						   else  //the last period average as average of final decision
						     
						     
						     {	
                                                    
                                                       if($average!=null)
                                                        {   
                                             //check if already exit
				             $result=$this->checkDataGeneralAverage($acad,$student_id,$average,$this->idLevel);
							  //$is_present=false;
							if((isset($result))&&($result!=null))
							  {  // yes, update
							       
									  $date_updated=date('Y-m-d');
							     foreach($result as $data)
				                   { 
								      $command2->update('decision_finale', array(
												'general_average'=>$average,'date_updated'=>$date_updated,
											), 'id=:ID', array(':ID'=>$data["id"] ));
	                                  
								     }
								   
							  }
							 else
							  { // no, insert
							        $date_created= date('Y-m-d');
							        
									$command2->insert('decision_finale', array(
									'student'=>$student_id,
									'academic_year'=>$acad,
									'general_average'=>$average,
									'current_level'=>$this->idLevel,
									'date_created'=>$date_created,
											
										));
							   }
							   
							   
							   //general_average_by_period
							    //save general average for this period
							    $period_acad_id ='';			  
						        $command3 = Yii::app()->db->createCommand();
						       //check if already exit
						       $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
															if(isset($result))
															 {  $result=$result->getData();//return a list of  objects
																foreach($result as $r)
																  {
																	$period_exam_name= $r->name_period;
																   $period_acad_id = $r->id;
																   }
															 }
								  
								  $data___=Grades::model()->checkDataGeralAverageByPeriodForReport($acad,$student_id,$period_acad_id);
								  if((isset($data___))&&($data___!=null))
								  {  // yes, update
						
								    $command3->update('general_average_by_period', array(
											'general_average'=>$average,'date_updated'=>date('Y-m-d'),
										), 'academic_year=:year AND academic_period=:period AND student=:stud', array(':year'=>$acad, ':period'=>$period_acad_id, ':stud'=>$student_id));
                                   }
								 else{// no, insert
								   $command3->insert('general_average_by_period', array(
										'academic_year'=>$acad,
										'academic_period'=>$period_acad_id,
										'student'=>$student_id,
										'general_average'=>$average,
										'date_created'=>date('Y-m-d'),
									));
									
								     
								     }
                                                             }     								     
								     
						      }
						      
					      }
					   else  //no past evaluation period was included
					     {
					     	if($average!=null)
                                                    {
								//check if already exit
				             $result=$this->checkDataGeneralAverage($acad,$student_id,$average,$this->idLevel);
							
							if((isset($result))&&($result!=null))
							  {  // yes, update
							       
									  $date_updated=date('Y-m-d');
							     foreach($result as $data)
				                   { 
								      $command2->update('decision_finale', array(
												'general_average'=>$average,'date_updated'=>$date_updated,
											), 'id=:ID', array(':ID'=>$data["id"] ));
	                                  
								     }
								   
							  }
							 else
							  { // no, insert
							        $date_created= date('Y-m-d');
							        
									$command2->insert('decision_finale', array(
									'student'=>$student_id,
									'academic_year'=>$acad,
									'general_average'=>$average,
									'current_level'=>$this->idLevel,
									'date_created'=>$date_created,
											
										));
							   }
							   
							   //general_average_by_period
							    //save general average for this period
							    $period_acad_id ='';			  
						        $command3 = Yii::app()->db->createCommand();
						       //check if already exit
						       $result=EvaluationByYear::model()->searchPeriodName($this->evaluation_id);
															if(isset($result))
															 {  $result=$result->getData();//return a list of  objects
																foreach($result as $r)
																  {
																	$period_exam_name= $r->name_period;
																   $period_acad_id = $r->id;
																   }
															 }
								 
								  $data___=Grades::model()->checkDataGeralAverageByPeriodForReport($acad,$student_id,$period_acad_id);
								  if((isset($data___))&&($data___!=null))
								  {  // yes, update
						
								    $command3->update('general_average_by_period', array(
											'general_average'=>$average,'date_updated'=>date('Y-m-d'),
										), 'academic_year=:year AND academic_period=:period AND student=:stud', array(':year'=>$acad, ':period'=>$period_acad_id, ':stud'=>$student_id));
                                   }
								 else{// no, insert
								   $command3->insert('general_average_by_period', array(
										'academic_year'=>$acad,
										'academic_period'=>$period_acad_id,
										'student'=>$student_id,
										'general_average'=>$average,
										'date_created'=>date('Y-m-d'),
									));
									
								     
								     }
								     
                                                      }   
							   
					     }	//end of no past evaluation   
							  
							  
	          }				 
					  
	      
                                                                                 
                                   ?>
                                                        
                                                        
                                                        

     
 </div>
 