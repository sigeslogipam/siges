<?php
 /*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));

    
$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
	
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
 


    $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
     //if($current_acad==null)
						      //    $condition = '';
						    // else{
						     	   $condition = 'p.active IN(1,2) AND ';
						   //     }


      



                  if(isset($_GET['shi'])) $this->idShift=$_GET['shi'];
				  else{$idShift = Yii::app()->session['ShiftsAdmit'];
				  $this->idShift=$idShift;}
				  
			      if(isset($_GET['sec'])) $this->section_id=$_GET['sec'];
				  else{$section = Yii::app()->session['SectionsAdmit'];
				  $this->section_id=$section;}
				  
				  if(isset($_GET['lev'])) $this->idLevel=$_GET['lev'];
				  else{$level = Yii::app()->session['LevelHasPersonAdmit'];
				  $this->idLevel=$level;}
				  
				 
				  if(isset($_GET['stud'])) $this->student_id=$_GET['stud'];
				  
 
      
     $validated_by = NULL;
     $date_validation = NULL;
     
     

     	
?>



<div class="box box-info">
         <div class="box-body">
                  <div class="table-responsive">
                    <table class="table no-margin">
                      
                      <tbody>
                        
                        <tr>
                          <td colspan="4">
<div style="padding:0px;">			

				 
		    <!--evaluation-->
			<div class="left" style="margin-left:10px;">
			<label for="Evaluation_name"></label>
			           
				</div>
			
			
			
			<!--Shift(vacation)-->
        <div class="left" style="margin-left:10px;">
		
			<?php $modelShift = new Shifts;
			echo $form->labelEx($modelShift,Yii::t('app','Shift'));?>
					 <?php 
							 $default_vacation=null;
					         $default_vacation_name = infoGeneralConfig('default_vacation');
						   		
						   		$criteria2 = new CDbCriteria;
						   								$criteria2->condition='shift_name=:item_name';
						   								$criteria2->params=array(':item_name'=>$default_vacation_name,);
						   		$default_vacation = Shifts::model()->find($criteria2);
						   		
				   		

						
						  if((isset($this->idShift))&&($this->idShift!=''))
						        {   
					               echo $form->dropDownList($modelShift,'shift_name',$this->loadShift(), array('options' => array($this->idShift=>array('selected'=>true)),'onchange'=> 'submit()' )); 
					             }
							  else
								{ $this->idLevel=0;
								     if($default_vacation!=null)
								       { echo $form->dropDownList($modelShift,'shift_name',$this->loadShift(), array('options' => array(($default_vacation->id)=>array('selected'=>true)),'onchange'=> 'submit()' )); 
								              $this->idShift=$default_vacation->id;
								       }
								    else
								      echo $form->dropDownList($modelShift,'shift_name',$this->loadShift(), array('onchange'=> 'submit()' ));
																		
								}
							
						echo $form->error($modelShift,'shift_name'); 
						
					
					  ?>
				</div>
			 
		    <!--section(liee au Shift choisi)-->
			<div class="left" style="margin-left:10px;">
			<?php $modelSection = new Sections;
			                   echo $form->labelEx($modelSection,Yii::t('app','Section')); ?>
			           <?php 
					
					    
							    if((isset($this->section_id))&&($this->section_id!=0))
							       echo $form->dropDownList($modelSection,'section_name',$this->loadSectionByIdShift($this->idShift), array('options' => array($this->section_id=>array('selected'=>true)),'onchange'=> 'submit()')); 
							    else
								  { $this->idLevel=0;
									echo $form->dropDownList($modelSection,'section_name',$this->loadSectionByIdShift($this->idShift), array('onchange'=> 'submit()' )); 
						           }					      
						  
						echo $form->error($modelSection,'section_name'); 
						
															
					   ?>
				</div>
			
			<!--level-->
			<div class="left" style="margin-left:10px;">
			<?php $modelLevelPerson = new LevelHasPerson;
                        
			                       echo $form->labelEx($modelLevelPerson,Yii::t('app','Level'));?> 
					   <?php 
					 
					   
						if((isset($this->idLevel))&&($this->idLevel!=0))
							    echo $form->dropDownList($modelLevelPerson,'level',$this->loadLevelByIdShiftSectionId($this->idShift,$this->section_id,$acad_sess), array('options' => array($this->idLevel=>array('selected'=>true)),'onchange'=> 'submit()', )); 
							 else
								{ $this->idLevel=0;
								  echo $form->dropDownList($modelLevelPerson,'level',$this->loadLevelByIdShiftSectionId($this->idShift,$this->section_id,$acad_sess), array('onchange'=> 'submit()' )); 
					              $this->room_id=0;
							      }
						echo $form->error($modelLevelPerson,'level'); 
					 
					  ?>
				</div>
			
			<!--room-->
			<div class="left" style="margin-left:10px;">
			     <?php  $modelRoom = new Rooms;
			      
			                     echo $form->labelEx($modelRoom,Yii::t('app','Room')); ?>
			          <?php 
					
					
						    
							  
							  if((isset($this->room_id))&&($this->room_id!=0))
							   { 
						          echo $form->dropDownList($modelRoom,'room_name',$this->loadRoomByIdShiftSectionLevel($this->idShift,$this->section_id,$this->idLevel), array('onchange'=> 'submit()','options' => array($this->room_id=>array('selected'=>true)) )); 
					             }
							   else
							      { $this->room_id=0;
							      	echo $form->dropDownList($modelRoom,'room_name',$this->loadRoomByIdShiftSectionLevel($this->idShift,$this->section_id,$this->idLevel), array('onchange'=> 'submit()')); 
							          
									 
							      }
						echo $form->error($modelRoom,'room_name'); 
						
					   ?>
				</div>
		 	
		
													   
    </div>

						 </td>
	       
					       
					    </tr>
					    
					    <tr>
					       <td colspan="4"> 

			<?php	 
			
$command= Yii::app()->db->createCommand("SELECT df.create_by,df.update_by,df.date_created,df.date_updated FROM decision_finale df INNER JOIN room_has_person rhp ON(rhp.students=df.student) WHERE room=:room AND rhp.academic_year=:acad AND df.academic_year=:acad");
			$command->bindValue(':room', $this->room_id);
			$command->bindValue(':acad', $acad);	
			
			$sql_result = $command->queryAll();
		if($sql_result!=null)
		  {
		  	 foreach($sql_result as $result)
			   { 
				 if($result["update_by"]==NULL)
				   $validated_by = $result["create_by"];
				 else
                    $validated_by = $result["update_by"];
                  
                  if($result["date_updated"]==NULL)
				   $date_validation = $result["date_created"];
				 else
                    $date_validation = $result["date_updated"];
                  
                  break;
			   }
			   
		  }	
			
			   			
$display_name=false;
	                                                                                                                                             
 $display_period_summary = infoGeneralConfig('display_period_summary');

$use_mention_summary = infoGeneralConfig('use_mention_summary');
 //Extract use automatic mention
 $use_automatic_mention = infoGeneralConfig('use_automatic_mention');
  //si classe la nan ekzamen ofisyel pa bay mention otomatik
	   	  $menfp = '';
	   	   
 $lastEvaluation_done = false;
$string_sql_periode = "SELECT distinct g.evaluation, a.id as idantite_peryod, a.name_period FROM grades g INNER JOIN evaluation_by_year eby ON (g.evaluation = eby.id) INNER JOIN academicperiods a ON (eby.academic_year = a.id) INNER JOIN evaluations e ON (eby.evaluation = e.id) where e.academic_year = $acad ORDER BY a.name_period";
 
 // Construction des periodes d'examen    
 $data_periode = Grades::model()->findAllBySql($string_sql_periode);
if($data_periode !=null)
 { 
 foreach($data_periode as $p)
  {
     //gade si se denye evalyasyon
     $modelEvalByPeriod = EvaluationByYear::model()->findByPk($p->evaluation);
        if($modelEvalByPeriod->last_evaluation == 1)
          {  $lastEvaluation_done= true;
           }
 }
 }			         
	   	   
			   
if((isset($this->room_id))&&($this->room_id!=0))
   {     
	   	$model_menfp=isLevelExamenMenfp($this->idLevel,$acad_sess);
			      
			      if($model_menfp !=null)
			        $menfp = $model_menfp['id'];
			      
	 if($use_mention_summary==0)
	   {  	 
	   	 if( ($use_automatic_mention==0) ) //mansyon an pa otomatik
	   	   echo $this->renderPartial('_endYD1', array('model'=>$model, 'form' =>$form));
	   	 elseif( ($use_automatic_mention==1)&&($menfp!='') ) //mansyon an pa otomatik
	   	    echo $this->renderPartial('_endYD1', array('model'=>$model, 'form' =>$form));
	   	    else //mansyon an otomatik
	   	     echo $this->renderPartial('_endYD2', array('model'=>$model, 'form' =>$form));
	   	     
	    }
	  else
	    {
	    	
	    	echo $this->renderPartial('_endYD_S', array('model'=>$model, 'form' =>$form));
	      }
   	
   	
   	}//end room_id !=''		
			
			
			?> 
			
						        	  </td>
                                 </tr>
                                
                                
		<?php	
			//Submit button
		 if((isset($this->room_id))&&($this->room_id!=""))//if((isset($this->idLevel))&&($this->idLevel!=""))//
		   { 
			   	if((!$this->lastReportcardNotSet)&&(!$this->messageNoStud)&&(!$this->messageNoPassingGradeSet)&&(!$this->messageDecisionDone) )
		  		 { 
	  		 	
	  		 ?>
                        <tr>
                                 <td colspan="4">
          <?php
	if($acad == $current_acad_id)
	  {		 
            if((!$display_name))  //((!$this->messageDecisionDone)&&(!$display_name))
             {
             if( ($lastEvaluation_done == true) )  //(($display_period_summary ==1) && ($lastEvaluation_done == true) )
				{
                 
	  		 	//if(!isAchiveMode($acad))
	  		 	    if($use_mention_summary==0)
	  		 	       echo CHtml::submitButton(Yii::t('app', 'Execute Decision '),array('name'=>'execute','class'=>'btn btn-warning')); 
	  		 	    elseif($use_mention_summary==1)
	  		 	       echo CHtml::submitButton(Yii::t('app', 'Validate Decision'),array('name'=>'execute','class'=>'btn btn-warning')); 
	            
	           
			   echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary')); //'Cancel ' avec espace a la fin POUR AVOIR UNE TRADUCTION "Annuler"/ sans espace=>'Retour'
                                         
                                           //back button   
                                              $url=Yii::app()->request->urlReferrer;
                                              $explode_url= explode("php",substr($url,0));
				             
                                              echo ' <a href="'.$explode_url[0].'php'.$this->back_url.'" class="btn btn-secondary">'.Yii::t('app', 'Back').'</a>';
                                              
			    	}
				
              }
         /*  else
            {
            	 if($use_mention_summary==0)
                   echo CHtml::submitButton(Yii::t('app', 'Update Decision Of Student In This Room'),array('name'=>'update','class'=>'btn btn-warning'));
                   
            }
           */   
                // if($use_mention_summary==0)                     
               //  echo CHtml::submitButton(Yii::t('app', 'View Decision For This Level'),array('name'=>'view','class'=>'btn btn-warning'));
          
          }
              ?>
                                      </td>
                        </tr>
          <?php
			        }	
			 
		      }																	
																	
	  ?>
	  
	          
							       
 
 
                       </tbody>
                    </table>
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
 
   <?php
      if($this->room_id!='')
        {
          if($acad == $current_acad_id)
		      {
   ?>
           <div>
               <?=  Yii::t('app', 'Validated by').': '.$validated_by.',  '.Yii::t('app', 'Date/Time').': '.Yii::app()->dateFormatter->formatDateTime($date_validation, 'medium') ?>
           
            </div>
  <?php
        }
        
        }
  ?>                     
              </div>

  	
           	
     
	  
 
 						

  