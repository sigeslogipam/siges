<script>

h2 {
    font-size : 12px;
    color : green;
}
.mention {
    color: black;
    font-size:10px;
    text-align: center;
    vertical-align: middle;
}

.kategori {
    font-size : 12px;
    color : black;
    background-color: #F8F8F8;
}



.impair{
  background-color : #F8F8F8;
}



.img-logo{
        
        text-align: center;
        height: auto; 
        width: auto; 
        max-width: 75px; 
        max-height:75px;
}

.enfo-lekol {
     padding-left : 0px;
}

#pdf-report-card td {
  border: 0.5px dotted #000;
  padding: 2px;
}

hr {
    border: none;
    height: 2px;
    /* Set the hr color */
    color: black; /* old IE */
    background-color: #black; /* Modern Browsers */
}
.non-lekol {
  font-size: 16px;
}

.non-lekol-hibis {
  font-size: 16px;
  font-family:Bravo,Stencil, Helvetica, sans-serif;
}

.adres{
  font-size: 14px;
}

.telefon{
  font-size: 12px;
}

.email{
  font-size: 12px;
}

#plas-logo{
   display: inline-block;
}

#plas-info {
    display: inline-block;
    
}

.signature {
    text-align : center;
}

.teks-pye-paj{
font-size: 9px;
}



.title {
        width:100%;
        font-weight:bold;
	font-family:Helvetica, sans-serif;
	font-size: 16px;
	text-align: center;
	}
	
	
.info{   font-size:10px;
         background-color: #F5F6F7;
	  border-bottom: 1px solid #ECEDF2;
	 }
		
 .corps{
	width:100%;
	background-color: #F5F6F7;
	}

		
table.signature {
		width:70%;
		float:right;
		font-size:10px;
		margin-top:15px;
		margin-bottom:5px;
		
	}
	
.place{
	  font-size:6pt;
	}
	
	
	
	td.space {
		width:30%;
	}

.th_legende{
            text-align:left;
            font-weight:bold;
            padding-left: 10px; 
            background-color:lightgrey;
	}
.td_foot{
	 padding-left: 10px; 
        }

.tb {
     width:100%;
     font-size:10px;
    }
	
 .discipline {
		width:65%; 
		margin-top:0px;
		font-size:8px;
                border-collapse: collapse;
	}
.td_discipline {
		text-align:center;
                font-size:8px;
                border:1px solid #E4E4E4;
	}

.td_space {
		width:60px;
                text-color:#ffffff;
	}

		
 .subjectheadnote {
                    color:#000; 
                    height:15px;
                    font-size:10px; 
                    text-align:left;
                    width:30%;
                    }
			
			

.subjectheadnote_white_tr{
			 background-color:#FFFFFF; 
	               }
						
						
 .subject{
        color:#000; 
        font-size:10px; 
        height:15px;
        font-weight: normal;  
        text-align:left;
        border-bottom: 1px solid #ecedf4;
        }
		
 .color1{
        color:#000; 
        font-size:10px; 
        text-align:left;
        background-color: #F5F6F7;
        border-bottom: 1px solid #ecedf4;
        }


 .color2{
            color:#000; 
            font-size:10px; 
            text-align:left;
            background-color: #F5F6F7;
            border-bottom: 1px solid #ecedf4;
		}
	
 .sommes{
            font-size:10px; 
            text-align:left;
            background-color:  #F5F6F7;
            border-bottom: 1px solid #ecedf4;
		}
 .sommes1{
			font-size:10px; 
			text-align:left;
			background-color:  #F5F6F7;
			border-bottom: 1px solid #ecedf4;
		}
 .sommes2{
            font-size:10px; 
            text-align:left;
            border-top: 1px solid #EE6539;
            border-radius: 5px;
            background-color:  #EFEFEF;
            border-bottom: 1px solid #ecedf4;
		}

 .border{
        border-bottom: 1px solid #ecedf4;
        }
			

	
	
.periodsommes2{
		height:15px; 
                font-size:10px; 
		text-align: right;
			border-top: 1px solid #EE6539;
	border-bottom: 1px solid #ecedf4;
	}
.periodsommes{
	height:15px;
        font-size:10px; 
	text-align: right; 	
	border-bottom: 1px solid #ecedf4;
	}
 .period {
		width:5%;
                height:15px;
                font-size:10px;
		text-align: center;
	border-bottom: 1px solid #ecedf4;
	}
        
.period_side {
		width:5%;
                height:15px;
                font-size:10px;
		text-align: center;
	border-bottom: 1px solid #ecedf4;
        border-left: 1px solid #c6c6c6;
	}

.period_side1 {
		width:5%;
                height:15px;
                font-size:10px;
		text-align: center;
	border-bottom: 1px solid #ecedf4;
        border-right: 1px solid #c6c6c6;
	}
.period_side2 {
		width:5%;
                height:15px;
                font-size:10px;
		text-align: center;
	border-bottom: 1px solid #ecedf4;
        border-right: 1px solid #c6c6c6;
        border-left: 1px solid #c6c6c6;
	}

 .subtitleperiod {
		width:5%;
                height:15px;
		text-align: center;
                font-size:8px; 
	border-bottom: 1px solid #ecedf4;
	}

.subtitleperiod_side {
		width:5%;
                height:15px;
		text-align: center;
                font-size:8px; 
	border-bottom: 1px solid #ecedf4;
         border-right: 1px solid #c6c6c6;  
	}

.periodParent {
		width:5%;
                height:15px;
                font-size:10px; 
		text-align: center;
		font-weight:bold;
	font-style: italic;
	background-color: #F1F1F1
	border-bottom: 1px solid #ecedf4;
	}
        
.periodParent_side {
		width:5%;
                height:15px;
                font-size:10px; 
		text-align: center;
		font-weight:bold;
	font-style: italic;
	background-color: #F1F1F1;
	border-bottom: 1px solid #ecedf4;
        border-right: 1px solid #c6c6c6;
	}
	
.periodsommes2_red{
		width:5%;
                height:15px;
                font-size:10px; 
		text-align: center;
		border-top: 1px solid #EE6539;
	border-bottom: 1px solid #ecedf4;
	}	

.periodsommes2_red_side{
		width:5%;
                height:15px;
                font-size:10px; 
		text-align: center;
		border-top: 1px solid #EE6539;
	border-bottom: 1px solid #ecedf4;
        border-left: 1px solid #c6c6c6;
	}
        
.periodsommes2_red_side1{
		width:5%;
                height:15px;
                font-size:10px; 
		text-align: center;
		border-top: 1px solid #EE6539;
	border-bottom: 1px solid #ecedf4;
        border-right: 1px solid #c6c6c6;
	}
	
.periodsommes2_red_side2{
		width:5%;
                height:15px;
                font-size:10px; 
		text-align: center;
		border-top: 1px solid #EE6539;
	border-bottom: 1px solid #ecedf4;
        border-right: 1px solid #c6c6c6;
        border-left: 1px solid #c6c6c6;
	}
	

.periodheadnote {
		width:5%;
                height:15px;
		font-size:9px;
                background-color: #F1F1F1;
	        border-right: 1px solid #c6c6c6;
	}


.subjectParent{
	height:15px;
	font-weight:bold;
	font-style: italic;
        font-size:10px;
	background-color: #F1F1F1;
	}
	

			
div > .subject {
		width:30%;
                height:15px;
             text-indent: 10px;
             font-weight: normal;  
	}
	
.subject_single{
        color:#000; 
        font-size:10px; 
        height:15px;
        font-weight:bold;
        text-indent: 0px; 
        text-align:left;
        border-bottom: 1px solid #ecedf4;	
	}	


</script>