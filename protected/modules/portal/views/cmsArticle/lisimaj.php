<table class="table table-responsive">
    <thead>
        <tr>
    <th><?= Yii::t('app','Label image')?></th>
    <th></th>
        </tr>
    
    </thead>
    <tbody>
        <?php 
            foreach($cms_image as $ci){
                 $string_image_location = "<img src='".Yii::app()->baseUrl."/cms_files/images/banners/".$ci->nom_image."'>";
                ?>
        <tr>
            <td class="modifye" data-label="<?= $ci->label_image; ?>" data-id="<?= $ci->id; ?>"><a data-toggle="tooltip" data-html="true" title="<?= $string_image_location; ?>"><?= $ci->label_image; ?></a></td>
            
            <td><a href="<?= Yii::app()->baseUrl?>/index.php/portal/cmsArticle/deleteCarrousel/id/<?= $ci->id ?>" title='<?= Yii::t('app','Delete')?>' data-type='POST' onclick='return confirm("<?= Yii::t('app','Do you really want to the delete this picture ?'); ?>")'><i class="fa fa-trash"></i></a></td>
            
        </tr>
        <?php 
               
            }   
        ?>
    </tbody>
    
</table>

<!-- Modal pour modifier les labels des iamges -->
 
 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Update image label'); ?></h4>
        </div>
        <div class="modal-body">
            <input type="text" name="label_value" id="label_value" />
            <input type="hidden" name="image_id" id="image_id"/>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update"><?= Yii::t('app','Update'); ?></button>  
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        </div>
      </div>
      
    </div>
  </div>

 <script>
     $(document).ready(function(){
         $('a[data-toggle=tooltip]').tooltip();
          
         $(".modifye").click(function(){
                var image_id = $(this).attr("data-id"); 
                var label = $(this).attr("data-label"); 
               $("#label_value").val(label); 
                $("#image_id").val(image_id); 
                 
                
                $("#myModal").modal();
                 $('#myModal').on('hidden.bs.modal', function () {
                        $("#label_value").val(""); 
                        $("#image_id").val("");
                    });
           });
           
           $('#update').click(function(){
               var label = $('#label_value').val(); 
               var image_id = $('#image_id').val(); 
               $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsArticle/updateCarrousel',{id:image_id,label:label},function(data){
                    $('#myModal').modal('toggle');
               // $('#lis_imaj').html(data);
            }); 
           });
     });
 </script>
