<?php
/*
 * © 2019 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cms-menu-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="dash">
   <div class="span3">
       <h2>
        <?php echo Yii::t('app','Manage album categories');?>
        
    </h2> 
   </div>
    
</div>



<?php
    echo $this->renderPartial('//layouts/navBasePortal',NULL,true);	
    ?>


<?php 



$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'cms-album-cat-grid',
        'itemsCssClass' => 'table-bordered items',
	'dataProvider'=>$model->search(),
	
	'columns'=>array(
		
            array(
                'class' => 'editable.EditableColumn',
                'name'=>'album_name',
                'headerHtmlOptions' => array('style' => 'width: 400px'),
                'editable' => array(    //editable section
                 //'apply'      => '$data->is_home != 1',
                  'url'        => $this->createUrl('cmsArticle/updateAlbum'),
                  'placement'  => 'right',
              )  
            ),
            
            'update_by',
		
		
	),
));



?>
