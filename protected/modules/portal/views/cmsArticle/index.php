<?php
 /*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 *//* @var $this CmsArticleController */
/* @var $model CmsArticle */




$tit = Yii::t('app','Manage school web site');

$this->pageTitle = Yii::app()->name.' - '.Yii::t('app','Manage school web site');
?>

<div id="dash">
   <div class="span3"><h2>
        <?php echo Yii::t('app','Manage school web site');?>
        
   </h2> </div>
    
    <div class="span3">
        <div class="span4">
            <?php
                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                               // build the link in Yii standard
                     echo CHtml::link($images,array('cmsArticle/create')); 
            ?>
        </div>
        
        <div class="span4">
            <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                     echo CHtml::link($images,array('/portal/cmsArticle/index')); 

                    ?>
        </div>
    </div>
</div>

<div class="clear"></div>

<?php
    echo $this->renderPartial('//layouts/navBasePortal',NULL,true);	
?>





<?php 
$order = array();
if(infoGeneralConfig('number_article_per_page')!=null){
        $messagesParPage = infoGeneralConfig('number_article_per_page');
        
        for($i=0; $i < $messagesParPage; $i++){
            $j = $i+1;
            $order[$i] = Yii::t('app','Rank {name}',array('{name}'=>$j));
       
        }
}
 
 
    
   // print_r($order);

$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'cms-article-grid',
	'summaryText'=>'',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table-bordered items',
	'columns'=>array(
		
		
            array(
                    'name' => 'article_title',
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->article_title,Yii::app()->createUrl("portal/cmsArticle/update",array("id"=>$data->id)))',
                   
                     ),
            'articleMenu.menu_label',
		
		'create_by',
            array(
                'class' => 'editable.EditableColumn',
                'header'=>Yii::t('app','Article rank'),
                'name'=>'rank_article',
                'headerHtmlOptions' => array('style' => 'width: 200px'),
                'editable'=>array(
                  'type'     => 'select',
                 // 'apply'      => '$data->is_rand != null',
                  'url'      => $this->createUrl('cmsArticle/updateArticle'),
                  
                 'source'=>Editable::source($order),
                  'options'  => array(    //custom display 
                     
                  ),
                 //onsave event handler 
                 'onSave' => 'js: function(e, params) {
                      console && console.log("saved value: "+params.newValue);
                 }',
                ),
            ),
            
             array(
                'class' => 'editable.EditableColumn',
                'name'=>'is_publish',
                'headerHtmlOptions' => array('style' => 'width: 200px'),
                'editable'=>array(
                  'type'     => 'select',
                 // 'apply'      => '$data->is_rand != null',
                  'url'      => $this->createUrl('cmsArticle/updateArticle'),
                  
                 'source'=>Editable::source(array(0 =>Yii::t('app','No'),  1=>Yii::t('app','Yes'))),
                  'options'  => array(    //custom display 
                     'display' => 'js: function(value, sourceData) {
                          var selected = $.grep(sourceData, function(o){ return value == o.value; }),
                              colors = {1: "green", 0: "red"};
                          $(this).text(selected[0].text).css("color", colors[value]);    
                      }'
                  ),
                 //onsave event handler 
                 'onSave' => 'js: function(e, params) {
                      console && console.log("saved value: "+params.newValue);
                 }',
                ),
            ),
            
		
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
                        'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                        
                        
		),
	),
)); 

$this->beginWidget('zii.widgets.jui.CJuiDialog',array(
                'id'=>'logoUploadDialog',
                'options'=>array(
                    'title'=>Yii::t('app','Upload image for carrousel'),
                    'autoOpen'=>false,
					'modal'=>'true',
                    'width'=>'34%',
                                   ),
                ));
	
 
 
 echo "<div id='logo_upload'></div>";
 
  $this->endWidget('zii.widgets.jui.CJuiDialog');



?>

            
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit ?>"},
                  
                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
            //var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>

