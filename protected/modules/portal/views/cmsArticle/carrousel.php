<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 
$this->pageTitle = Yii::app()->name.' - '.Yii::t('app','Manage school web site');
?>


<div id="dash">
   <div class="span3"><h2>
        <?php echo Yii::t('app','Manage Carrousel');?>
        
   </h2> </div>
    
    <div class="span3">
       <div class="span4">
            <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                     echo CHtml::link($images,array('/portal/cmsArticle/index')); 

                    ?>
        </div>
    </div>
</div>

<div class="clear"></div>

<?php
    echo $this->renderPartial('//layouts/navBasePortal',NULL,true);	
?>

<div class="clear"></div>
<div class="row">
   
    <?php 
$dict = array(
	'dictDefaultMessage'=>'<i class="fa fa-up"></i> '.Yii::t('app','Drag and Drop your image here to upload'),
	'dictFallbackMessage'=>Yii::t('app','The browser do not support the process. :('),
	'dictFallbackText'=>Yii::t('app','Please use an alternative upload process!'),
	'dictInvalidFileType'=>Yii::t('app','Type of files incorrect !'),
	'dictFileTooBig'=>Yii::t('app','File too big!'),
	'dictResponseError'=>Yii::t('app','Oops ! Something goes wrong!'),
	'dictCancelUpload'=>Yii::t('app','Delete'),
	'dictCancelUploadConfirmation'=>Yii::t('app','Do you want to cancel the upload?'),
	'dictRemoveFile'=>Yii::t('app','Delete'),
	'dictMaxFilesExceeded'=>Yii::t('app','File size exceeded'),
);

$options = array(
    'addRemoveLinks'=>true,
);

$events = array(
    'success' => 'successUp(this, param, param2, param3)',
    'totaluploadprogress'=>'incProgress(this, param, param2, param3)',
    'queuecomplete'=>'complete()'
);?>
    <div class="span2">
        
    </div> 
  
    <div class="span8">
         
        <div id="dropZon">
        <?php

        $this->widget('ext.dropzone.EDropzone', array(
            //'model' => $model,
            //'attribute' => 'file',
            'name'=>'upload',
            'url' => $this->createUrl('uploadLogo'),
            'mimeTypes' => array('image/jpeg', 'image/png'),
            'events' => $events,
            'options' => CMap::mergeArray($options, $dict ),
            'htmlOptions' => array('style'=>'height:95%; overflow: hidden;'),
            //'customStyle'=> $this->module->assetsPath.'/css/customdropzone.css'
        ));
        ?>
        </div>
    </div>
    <div class="span2">
        
    </div>
    

</div>
<div class="row">
    <div class="span1">
        
    </div>
    <div class="span10">
         <br/>  <br/>
        <div id="lis_imaj">
        
        </div>
    </div>
    
    <div class="span1">
        
    </div>
</div>

<script>
    $(document).ready(function(){
        $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsArticle/lisimaj',{},function(data){
                $('#lis_imaj').html(data);
            });
        $('#dropZon').mouseover(function(){
            $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsArticle/lisimaj',{},function(data){
                $('#lis_imaj').html(data);
            });
            //alert('Test li sou li');
        });
    });
</script>