<?php
setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');
$model_article = new CmsArticle();
?>
<div class="container-fluid masthead bg-one text-white text-center">
    <section>
        <div class="container-fluid-top text-center">
            <div class="row content">
                <div class="col-sm-9 text-left">

<!-- ========================== fen atik  ============================ -->


<!-- ========================== komansman mo direktè a  ============================ -->

              <div class="pawol">

                     <div class="row">
                         <h3 style="text-align: left"><?= $article->article_title; ?></h3>
                       <div class="col-lg-12">
                               <img class="img_article" alt="siges" src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/3.jpg" />
                       </div>
                       <div class="col-lg-12">

                            <p>
                                <?= $article->article_description; ?>
                            </p>
                            <p style="text-align: left">
                                <?= Yii::t('app','Publish at: ')?> <?= strftime('%d %B %Y %H:%M:%S',strtotime($article->date_create)); ?>
                            </p>
                       </div>
                     </div>

                     <hr class="espas ">

                  </div>

                </div>

<!-- ========================== fen atik  ============================ -->



<!-- ========================== A kote sit entenet la ============================ -->

              <div class="col-sm-3 sidenav">
                  <!-- Album PHOTOS ICI -->
                <div class="row">
                    <div class="col-lg-12">
                         <h5><?= Yii::t('app','Photo gallerie'); ?></h5>
                        <?php
                            $sql_image = "SELECT * FROM `cms_image` WHERE album IS NOT NULL AND type_image = 'album' ORDER BY id DESC LIMIT 1";
                            $cover_album = CmsImage::model()->findAllBySql($sql_image);
                            $image_name = null;
                            foreach($cover_album as $cv){
                                ?>
                        <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie">
                            <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $cv->nom_image; ?>" />
                        </a>
                          <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie"><?= $cv->album0->album_name; ?></a>
                        <?php
                            }
                        ?>

                    </div>

                </div>
                  <hr>
                  <div class="well">
                      <!-- Liste des sous menu lie a l'article ici -->
                        <div class="row">

                          <div class="col-lg-12">

                             <?php

                              if(isset(CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->menu_label)){

                                  ?>
                                  <h5>
                               <?php
                                  echo CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->menu_label;
                                ?>
                                </h5>
                             <?php
                                  $menu_parent = CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->menu_label;
                                   $menu_parent_id = CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->id;
                                  $all_sub_in_parent = CmsMenu::model()->findAllByAttributes(array('parent_menu'=>$menu_parent_id));

                                  ?>
                              <ul class="nav flex-column">
                                  <?php
                                    foreach($all_sub_in_parent as $asp){

                                        ?>
                                  <?php if($model_article->getLastArticleIdByMenu($asp->id)!=null){

                                      ?>

                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($asp->id); ?>&menu=<?= $asp->id; ?>"><?= $asp->menu_label; ?></a>
                                  </li>
                                  <?php

                                    }
                                  ?>
                                  <?php

                                    }
                                  ?>
                              </ul>
                              <?php

                                }

                                ?>


                          </div>
                      </div>
                      </div>
              </div>
        </div>
    </section>
</div>
