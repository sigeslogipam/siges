<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Debut du processus de pagination des articles du Portal
 *
 */


        $doc_ = CmsDoc::model()->findAllBySql("SELECT * from cms_doc ORDER BY id DESC");
?>

<body>
       <div class="container-fluid masthead bg-one text-white text-center">
                     <section>
                         <div class="container-fluid-top text-center">
                             <div class="row content">
                                 <div class="col-sm-9 text-left group">

                                   <div class="bottom-color"><span class="title clr">  Documents à télécharger </span></div>
                                   <div class="list-group">




             <?php foreach($doc_ as $d) { ?>

                   <a href="<?php echo Yii::app()->baseUrl.'/cms_files/files/'.$d->document_name; ?>" class="list-group-item">
                       <h5 class="list-group-item-heading"><i class="glyphicon glyphicon-paperclip"></i>  <?php echo $d->document_title; ?></h5>
                       <p class="list-group-item-text">
                           <?php
                            echo $d->document_description;
                           ?>
                       </p>

                   </a>



                <?php }?>

               </div>

                </div>

                <div class="col-lg-3 sidenav" id="map_contact">
                    <!--    PHOTOS ICI -->
                <div class="row album">
                    <div class="col-lg-12">

                        <?php
                            $sql_image = "SELECT * FROM `cms_image` WHERE album IS NOT NULL AND type_image = 'album' ORDER BY id DESC LIMIT 1";
                            $cover_album = CmsImage::model()->findAllBySql($sql_image);
                            $image_name = null;
                            foreach($cover_album as $cv){
                                ?>

                        <div id="title" >
                            <h4><a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie">
                                    <i class="fa fa-expand" aria-hidden="true"></i>&nbsp;
                                        <?= $cv->album0->album_name; ?>
                                </a>
                            </h4>
                        </div>
                        <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie">
                            <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $cv->nom_image; ?>" />
                        </a>
                                 <!--
                                               <div id="desc-gallerie" >  <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie"><?= $cv->album0->album_name; ?></a> </div>
                                 -->
                        <?php
                            }
                        ?>

                    </div>

                </div>
                    
                 

                 <div class="well">
                      <!-- Liste des autres articles ici! -->
                        <div class="row">

                          <div class="col-lg-12">
                              <div id="title" ><span> 
                             <i class="fa fa-pen-alt" aria-hidden="true"></i>
                            <?= Yii::t('app','Other articles')?> </span> </div>
                              <ul class="nav flex-column">
                              <?php

                              $all_related_articles = CmsArticle::model()->findAllBySql("SELECT * FROM cms_article WHERE is_publish = 1 ORDER BY id DESC LIMIT 5");

                              if(!empty($all_related_articles)){

                              foreach($all_related_articles as $ara){

                                  ?>
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $ara->id; ?>&menu=<?= $ara->article_menu; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= $ara->article_title; ?>
                                      </a>
                                  </li>
                              <?php

                              }
                              }


                              ?>
                              </ul>
                          </div>
                      </div>
                </div>


            </div>
            </div>
          </div>




<p class="pull-right" ><a href="#"> Retour <i class="fa fa-chevron-up"> </i> </a></p>



</div>



</body>
