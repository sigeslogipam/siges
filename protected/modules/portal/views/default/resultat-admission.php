<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo "Les resultats seront affiche ici !";


$acad = currentAcad()->id;
$model_article = new CmsArticle(); 
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/css/admission.css");
$all_level_admis = Levels::model()->findAllBySql("SELECT DISTINCT l.level_name, l.id  FROM postulant p INNER JOIN levels l ON (p.apply_for_level = l.id) WHERE p.status IN (1,2) AND academic_year = $acad");

?>


<div class="container-fluid masthead bg-one text-white text-center">
    <section>
        <div class="container-fluid-top text-center">
            <div class="row content">
                <div class="col-lg-9 text-left">
                    <!-- Les resultats seront places ici -->
                        <div class="col-lg-9">
                    <div class="ibox float-e-margins">
                        <div class="ibox-title">
                            <h5><?= Yii::t('app',"Liste des admis, cliquez sur le nom d'une classe pour afficher la liste des admis !")?></h5>
                           
                        </div>
                        <div class="ibox-content">
                            <div class="panel-body">
                                <div class="panel-group" id="accordion">
                                    <?php 
                                    $k = 0;
                                    foreach($all_level_admis as $ala){
                                        $all_postulants = Postulant::model()->findAllBySql("SELECT * FROM postulant WHERE status IN (1,2) and academic_year = $acad AND apply_for_level = $ala->id ORDER BY status"); 
                                    ?>
                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h5 class="panel-title">
                                                <a data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $k;?>" aria-expanded="false" class="collapsed">
                                                    <?= $ala->level_name; ?>
                                                </a>
                                            </h5>
                                        </div>
                                        <div id="collapse<?= $k; ?>" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">
                                            <div class="panel-body">
                                                <table class="table responsive table-striped table-hover non-validate">
                                                    <thead>
                                                        <th><?= Yii::t('app','Num&eacute;ro inscription')?></th>
                                                        <th><?= Yii::t('app','Mention')?></th>
                                                    </thead>
                                                    <tbody>
                                                        <?php foreach($all_postulants as $ap){?>
                                                        <tr>
                                                            <td><?= $ap->id; ?></td>
                                                            <td><?= $ap->statut; ?></td>
                                                        </tr>
                                                        <?php } ?>
                                                    </tbody>
                                                </table>
                                               <?php 
                                                
                                               ?>
                                            </div>
                                        </div>
                                    </div>
                                   <?php
                                   $k++;
                                        }
                                   ?>
                                    
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                    <!-- Fin des resultats  -->
                </div>
                
                <div class="col-lg-3 sidenav">
                    <!-- Si la periode d'admission est active --> 
                 <?php if(infoGeneralConfig('is_admission_open')==1) {
                     $critere = "Condition d\'admission";
                     $condition_admission = CmsMenu::model()->findAllBySql("SELECT id FROM cms_menu WHERE menu_label = '$critere'");
                     $id_adm = null;
                     foreach($condition_admission as $ca){
                         $id_adm = $ca->id;
                     }
                     ?>
                 <div class="well">
                     <div class="row">
                         <div class="col-lg-12">
                             <div id="title" ><span> <i class="fas fa-user-graduate" aria-hidden="true"></i> <?= Yii::t('app','Admission')?> </span> </div>
                             <ul class="nav flex-column">
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($id_adm); ?>&menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app',"Condition d'admission") ?>
                                      </a>
                                  </li>
                                  
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/resultatadmission?menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app','R&eacute;sultat admission'); ?>
                                      </a>
                                  </li>
                             </ul>
                         </div>
                     </div>
                 </div>
                 <?php } ?>
                 <!-- Si la periode d'admission est active --> 
                </div>
                
            </div>
        </div>
    </section>
</div>