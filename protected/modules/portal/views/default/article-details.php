<?php
setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');
$model_article = new CmsArticle();
?>
<div class="container-fluid masthead bg-one text-white text-center">
<section>
  <div class="container-fluid-top text-center">
    <div class="row content">
        <div class="col-lg-9 col-md-12 text-left">

<!-- ========================== fen atik  ============================ -->


<!-- ========================== komansman mo direktè a  ============================ -->

              <div class="pawol">

                     <div class="row">

                       <div class="col-lg-12">
                           <?php
                            if(isset($article->featured_image)){
                                ?>
                           <img class="img_article" alt="siges" src="<?= Yii::app()->baseUrl ?>/portal_assets/ckfinder/core/connector/php/images/images/<?= $article->featured_image?>" />

                           <?php
                            }else{
                           ?>
                               <img class="img_article" alt="siges" src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/3.jpg" />
                            <?php } ?>
                       </div>
                       <div class="col-lg-12">

                <!-- ================== Article title  ================== -->

                           <div class="bottom-color"><span class="title clr">  <?= $article->article_title; ?>  </span></div>


                            <p class="description">
                                <?= $article->article_description; ?>
                            </p>
                            <p style="text-align: left; font-size: 10px">
                                <?= Yii::t('app','Publish at: ')?> <?= strftime('%d %B %Y %H:%M:%S',strtotime($article->date_create)); ?>
                            </p>
                       </div>
                     </div>

                     <hr class="espas ">

                  </div>

</div>







<!-- ========================== fen atik  ============================ -->



<!-- ========================== A kote sit entenet la ============================ -->

                <div class="col-lg-3 col-md-12 sidenav">

                 <!--    PHOTOS ICI -->
                <div class="row album">
                    <div class="col-lg-12">

                        <?php
                            $sql_image = "SELECT * FROM `cms_image` WHERE album IS NOT NULL AND type_image = 'album' ORDER BY id DESC LIMIT 1";
                            $cover_album = CmsImage::model()->findAllBySql($sql_image);
                            $image_name = null;
                            foreach($cover_album as $cv){
                                ?>

                        <div id="title"><span>

                                    <i class="fa fa-expand" aria-hidden="true"></i>&nbsp;
                                        <?= $cv->album0->album_name; ?>


                        </div>
                        <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie">
                            <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $cv->nom_image; ?>" />
                        </a>
                                 <!--
                                               <div id="desc-gallerie" >  <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie"><?= $cv->album0->album_name; ?></a> </div>
                                 -->
                        <?php
                            }
                        ?>

                    </div>

                </div>
                 
                 
                 <!-- Si la periode d'admission est active --> 
                 <?php if(infoGeneralConfig('is_admission_open')==1) {
                     $critere = "Condition d\'admission";
                     $condition_admission = CmsMenu::model()->findAllBySql("SELECT id FROM cms_menu WHERE menu_label = '$critere'");
                     $id_adm = null;
                     foreach($condition_admission as $ca){
                         $id_adm = $ca->id;
                     }
                     ?>
                 <div class="well">
                     <div class="row">
                         <div class="col-lg-12">
                             <div id="title" ><span> <i class="fas fa-user-graduate" aria-hidden="true"></i> <?= Yii::t('app','Admission')?> </span> </div>
                             <ul class="nav flex-column">
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($id_adm); ?>&menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app',"Condition d'admission") ?>
                                      </a>
                                  </li>
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/admission?menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app','Formulaire admission'); ?>
                                      </a>
                                  </li>
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/resultatadmission?menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app','R&eacute;sultat admission'); ?>
                                      </a>
                                  </li>
                             </ul>
                         </div>
                     </div>
                 </div>
                 <?php } ?>
                 <!-- Si la periode d'admission est active --> 
                 
                    <div class="well">
                      <!-- Liste des sous menu lie a l'article ici -->
                        <div class="row">

                          <div class="col-lg-12">

                             <?php

                              if(isset(CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->menu_label)){
                                  ?>
                                  <div id="title" ><span>
                                          <i class="fa fa-pen-alt" aria-hidden="true"></i>
                               <?php
                                  echo CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->menu_label;
                                  ?>
                                </span></div>
                             <?php
                                  $menu_parent = CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->menu_label;
                                   $menu_parent_id = CmsMenu::model()->findByPk($model_article->getMenuBySubMenu($_GET['menu']))->id;
                                  $all_sub_in_parent = CmsMenu::model()->findAllByAttributes(array('parent_menu'=>$menu_parent_id));

                                  ?>
                              <ul class="nav flex-column">
                                  <?php
                                    foreach($all_sub_in_parent as $asp){
                                        ?>
                                  <?php if($model_article->getLastArticleIdByMenu($asp->id)!=null){ ?>
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($asp->id); ?>&menu=<?= $asp->id; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= $asp->menu_label; ?>
                                      </a>
                                  </li>
                                  <?php } ?>
                                  <?php
                                    }
                                  ?>
                              </ul>
                              <?php

                                }

                                ?>


                          </div>
                      </div>
                      </div>


                    <div class="well">
                      <!-- Liste des autres articles ici! -->
                        <div class="row">

                          <div class="col-lg-12">
                              <div id="title" ><span> <i class="fa fa-pen-alt" aria-hidden="true"></i> <?= Yii::t('app','Related articles')?> </span> </div>
                              <ul class="nav flex-column">
                              <?php
                              $menu_val = $_GET['menu'];
                              $id_a = $_GET['id'];
                              $all_related_articles = CmsArticle::model()->findAllBySql("SELECT * FROM cms_article WHERE article_menu IN (SELECT id FROM cms_menu WHERE parent_menu = (SELECT parent_menu  FROM cms_menu WHERE id = $menu_val)) AND is_publish = 1 ORDER BY id DESC LIMIT 5");

                              if(!empty($all_related_articles)){

                              foreach($all_related_articles as $ara){
                                  if($ara->id != $id_a){
                                  ?>
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $ara->id; ?>&menu=<?= $ara->article_menu; ?>">
                                          <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= $ara->article_title; ?>
                                      </a>
                                  </li>
                              <?php
                                  }
                              }
                              }else{
                                  $same_menu_article = CmsArticle::model()->findAllByAttributes(array('is_publish'=>1,'article_menu'=>$_GET['menu']));
                                  foreach($same_menu_article as $sma){
                                      if($sma->id != $id_a){

                                  ?>
                                  <li class="nav-item">

                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $sma->id; ?>&menu=<?= $sma->article_menu; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= $sma->article_title; ?>
                                      </a>
                                  </li>
                              <?php
                                      }
                              }
                              }


                              ?>
                              </ul>
                          </div>
                      </div>
                      </div>


                </div></div></div>
              </section>
</div>
