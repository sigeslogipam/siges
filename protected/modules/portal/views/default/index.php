<?php
$cms_image = CmsImage::model()->findAllBySql("SELECT * FROM cms_image WHERE type_image = 'carrousel' AND is_publish = 1 ORDER BY id DESC LIMIT 10");
$last_n_article = CmsArticle::model()->findAllBySql("SELECT * FROM cms_article WHERE set_position = 'main' AND is_publish = 1 ORDER BY id DESC LIMIT 2");
$home_article = CmsArticle::model()->findAllBySql("SELECT ca.id, ca.article_title, ca.article_description, ca.article_menu FROM cms_article ca INNER JOIN cms_menu cm ON (ca.article_menu = cm.id) WHERE cm.is_home = 1 AND ca.is_publish = 1 AND cm.is_special is not NULL ORDER BY id DESC LIMIT 1 ");
$last_announce = Announcements::model()->findAllBySql("SELECT * FROM announcements ORDER BY id DESC LIMIT 1");
$last_events = Calendar::model()->findAllBySql("SELECT * FROM scalendar WHERE start_date >= NOW() ORDER BY start_date ASC LIMIT 3");
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
//$cs->registerCssFile("//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
$cs->registerCssFile($baseUrl."/portal_assets/css/calendar-index.css");
setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');


?>
<div class="fotoyo">
  <header class="container-fluid masthead bg-one text-white text-center">
    <div id="demo" class="carousel slide" data-ride="carousel">

        <!-- Indicators -->
        <ul class="carousel-indicators">
          <?php
          if(isset($cms_image)){
          $k = 0;
          foreach($cms_image as $cm){
              if($k==0){
              ?>
          <li data-target="#demo" data-slide-to="<?= $k; ?>" class="active"></li>
            <?php
              }else{
                  ?>
          <li data-target="#demo" data-slide-to="<?= $k; ?>"></li>
          <?php
              }
            $k++;
              }
          }else{
              ?>
          <li data-target="#demo" data-slide-to="0" class="active"></li> -->
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
          <li data-target="#demo" data-slide-to="3"></li>
          <li data-target="#demo" data-slide-to="4"></li>
          <li data-target="#demo" data-slide-to="5"></li>
          <li data-target="#demo" data-slide-to="6"></li>

          <?php
          }
          ?>
         <!-- <li data-target="#demo" data-slide-to="0" class="active"></li> -->
          <!--
          <li data-target="#demo" data-slide-to="1"></li>
          <li data-target="#demo" data-slide-to="2"></li>
          <li data-target="#demo" data-slide-to="3"></li>
          <li data-target="#demo" data-slide-to="4"></li>
          <li data-target="#demo" data-slide-to="5"></li>
          <li data-target="#demo" data-slide-to="6"></li>
          -->
        </ul>

        <!-- The slideshow -->
        <div class="carousel-inner">
          <?php
          $data_carou = array();
          $konte_carou = 0;
          foreach($cms_image as $ci){
              $data_carou['nom_image'][$konte_carou] = $ci->nom_image;
              $data_carou['label_image'][$konte_carou] = $ci->label_image;
              $konte_carou++;
          }
          if(isset($data_carou['nom_image'])){
              for($i=0; $i < count($data_carou['nom_image']);$i++){
                  if($i==0){
                  ?>
          <div class="carousel-item active">
            <img src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $data_carou['nom_image'][$i]; ?>" alt="<?= $data_carou['label_image'][$i];?>">
          </div>

            <?php
                  }else{
                      ?>
            <div class="carousel-item">
            <img src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $data_carou['nom_image'][$i]; ?>" alt="<?= $data_carou['label_image'][$i];?>">
          </div>
            <?php
                  }
              }

          }else{
              ?>

          <div class="carousel-item active">
            <img src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/5.jpg" alt="Logipam/Siges">
          </div>
          <div class="carousel-item">
            <img src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/3.jpg" alt="Logipam/Siges">
          </div>
          <div class="carousel-item">
            <img src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/2.jpg" alt="Logipam/Siges">
          </div>
          <div class="carousel-item">
            <img src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/1.jpg" alt="Logipam/Siges">
          </div>
          <div class="carousel-item">
            <img src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/2.jpg" alt="Logipam/Siges">
          </div>
          <div class="carousel-item">
            <img src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/3.jpg" alt="Logipam/Siges">
          </div>
          <div class="carousel-item">
            <img src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/4.jpg" alt="Logipam/Siges">
          </div>
          <?php

          }
          ?>

        </div>

        <!-- Left and right controls -->
        <a class="carousel-control-prev" href="#demo" data-slide="prev">
          <span class="carousel-control-prev-icon"></span>
        </a>
        <a class="carousel-control-next" href="#demo" data-slide="next">
          <span class="carousel-control-next-icon"></span>
        </a>
      </div>


    </header>
</div>
<?php
 
$display_communication = infoGeneralConfig('display_communication');
$display_portal = infoGeneralConfig('display_portal');

if( ($display_communication==1) && ($display_portal==1) )
  {
 
 ?>
<section>
  <div class="container-fluid-top text-center">
    <div class="row content">


      <div class="col-lg-9 col-md-12 text-left">
        <div class="heading">
          <div class="row pt-md">
              <?php
              foreach($last_n_article as $lna){
                  ?>
              <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 team">

                      <div class="row">
                        <div class="col-lg-5 col-md-5">
                            <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $lna->id; ?>&menu=<?= $lna->article_menu?>">
                                <?php
                                if(isset($lna->featured_image)){
                                    ?>
                                 <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/portal_assets/ckfinder/core/connector/php/images/images/<?= $lna->featured_image; ?>" />
                                <?php
                                }else{
                                    ?>
                                <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/3.jpg" />
                                <?php
                                }
                                ?>

                            </a>
                        </div>
                        <div class="col-lg-7 col-md-7">
                            <h3>
                                <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $lna->id; ?>&menu=<?= $lna->article_menu?>">
                                    <?= $lna->article_title; ?>
                                </a>
                            </h3>
                            <p>
                                <?= substr(strip_tags($lna->article_description),0,400);?>

                            </p>

                        </div>
                        </div>
                 </div>
              <?php
              }
              ?>

                    </div>
        </div>

<!-- ========================== fen atik  ============================ -->


<!-- ========================== komansman mo direktè a  ============================ -->

            <div class="pawol">
                <?php
                    foreach($home_article as $ha){
                ?>
                <div class="bottom-color"><span class="title clr"> <?= $ha->article_title; ?> </span></div>
                <?= $ha->article_description; ?>
                <?php
                    }
                  
                ?>

            </div>





    </div>

<!-- ========================== fen atik  ============================ -->



<!-- ========================== A kote sit entenet la ============================ -->

  <div class="col-lg-3 col-md-12 sidenav">
        <div id="title" ><i class="fa fa-bell"></i>&nbsp;<span> <?= Yii::t('app','Annonces')?></span></div>
                  <?php
                  // Affichage des annonces de l'ecole
                    foreach ($last_announce as $la){
                  ?>
                  <div class="row">
                  <div class="col-lg-12 col-sm-12">
                  <!--  <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/annonces.jpg" /> -->

                        <h3><?= $la->title; ?></h3>
                          </div>
                    <div class="col-lg-12 col-sm-12">
                       <p style="text-align:justify">
                            <?= $la->description; ?>
                        </p>
                        <div style="text-align: right; font-size: 10px;">
                            <?= Yii::t('app','Publish at: ').$la->publishDate;?>
                        </div>
                        <div class="row row-striped">
                             <!-- Need to be in the center -->
                            <a href="<?= $baseUrl;?>/index.php/portal/default/allanouncements"><?= Yii::t('app','All Anouncemnts')?></a>
                        </div>
                   </div>
                </div>
                    <?php
                       }
                    ?>
                <hr>
    <div class="well col-lg-12">
                  <div id="title" ><i class="fas fa fa-calendar"></i>&nbsp;<span> <?= Yii::t('app','Incoming events')?></span></div>
       <div class="container">
            <?php
            // Afichage du calendrier de l'ecole
            foreach($last_events as $le){

            ?>
		<div class="row row-striped">
			<div class="col-3 text-right">
                             <h4 class="display-5"><span class="badge badge-secondary"><?= date("d",strtotime($le->start_date));?></span></h4>
                             <h6 class="display-5"><?= strtoupper(strftime('%b',strtotime($le->start_date))); ?></h6>
			</div>
			<div class="col-9">
                            <h6 class="text-uppercase" style="text-align: left"><strong><?= $le->c_title; ?></strong></h6>
				<ul class="list-inline ti-karakte">
				   <li class="list-inline-item"><i class="fa fa-calendar" aria-hidden="true"></i> <?= strftime('%A',strtotime($le->start_date)); ?></li>
                                    <li class="list-inline-item"><i class="fa fa-clock" aria-hidden="true"></i> <?= date("h:i a",strtotime($le->start_time)); ?> - <?= date("h:i a",strtotime($le->end_time)); ?></li>
                                 </ul>
                            <p style="text-align: left">
                                <?= $le->description; ?>
                            </p>
			</div>
		</div>


            <?php } ?>

            <div class="row row-striped">
                <!-- Need to be in the center -->
                <a href="<?= $baseUrl;?>/index.php/portal/default/allevents"><?= Yii::t('app','All Events')?></a>
            </div>


	</div>


    </div>



                </div></div></div>
  </section>

<?php

  }
?>