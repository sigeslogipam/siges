<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerCssFile("https://use.fontawesome.com/releases/v5.0.6/css/all.css");
$cs->registerCssFile("https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css");
$cs->registerCssFile($baseUrl."/portal_assets/fullcalendar/core/main.css");
$cs->registerCssFile($baseUrl."/portal_assets/fullcalendar/daygrid/main.css");
$cs->registerCssFile($baseUrl."/portal_assets/css/custom-calendar.css");
$cs->registerCssFile($baseUrl."/portal_assets/fullcalendar/bootstrap/main.css");
$cs->registerCssFile($baseUrl."/portal_assets/fullcalendar/timegrid/main.css");
$cs->registerCssFile($baseUrl."/portal_assets/fullcalendar/list/main.css");

$cs->registerCssFile($baseUrl."/portal_assets/css/calendar-index.css");


$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/core/main.js");
$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/daygrid/main.js");
$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/core/main.js");
$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/interaction/main.js");
$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/bootstrap/main.js");
$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/timegrid/main.js");
$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/list/main.js");
$cs->registerScriptFile($baseUrl."/portal_assets/fullcalendar/core/locales-all.min.js");

setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');

$last_events = Calendar::model()->findAllBySql("SELECT * FROM scalendar WHERE start_date >= NOW() ORDER BY start_date ASC LIMIT 5");

$all_events = Calendar::model()->findAllBySql("SELECT * FROM scalendar");
$all_events_arr = array();
$k = 0;
foreach($all_events as $ae){

    $all_events_arr[$k]['title'] = $ae->c_title;
    $all_events_arr[$k]['start'] = $ae->start_date.' '.$ae->start_time;
    $all_events_arr[$k]['end'] = $ae->end_date.' '.$ae->end_time;
    $k++;
}

        $str_json = "";
        foreach($all_events_arr as $e){
         $str_json = $str_json.json_encode($e,JSON_FORCE_OBJECT).",";
        }

$last_announce = Announcements::model()->findAllBySql("SELECT * FROM announcements ORDER BY id DESC LIMIT 1");
?>

<div class="container-fluid masthead bg-one text-white text-center">
    <section>
          <div class="container-fluid-top text-center">
            <div class="row content">
                <div class="col-lg-9 col-md-12 text-left">
                    <div  class="bottom-color"><span class="title clr"> <?= Yii::t('app','Incoming events')?> </span></div>
                    <div id='calendar'></div>

                </div>
                <div class="col-lg-3 col-md-12 sidenav">
                   <div class="container">
                        <h4><i class="fas fa fa-calendar"></i> <?= Yii::t('app','Ev&egrave;nement &agrave; venir')?></h4>
                        <?php
                        // Afichage du calendrier de l'ecole
                        foreach($last_events as $le){

                        ?>
                            <div class="row row-striped">
                                    <div class="col-3 text-right">
                                         <h4 class="display-5"><span class="badge badge-secondary"><?= date("d",strtotime($le->start_date));?></span></h4>
                                         <h6 class="display-5"><?= strtoupper(strftime('%b',strtotime($le->start_date))); ?></h6>
                                    </div>
                                    <div class="col-9">
                                        <h6 class="text-uppercase" style="text-align: left"><strong><?= $le->c_title; ?></strong></h6>
                                            <ul class="list-inline ti-karakte">
                                               <li class="list-inline-item"><i class="fa fa-calendar" aria-hidden="true"></i> <?= strftime('%A',strtotime($le->start_date)); ?></li>
                                                <li class="list-inline-item"><i class="fa fa-clock" aria-hidden="true"></i> <?= date("h:i a",strtotime($le->start_time)); ?> - <?= date("h:i a",strtotime($le->end_time)); ?></li>
                                             </ul>
                                        <p style="text-align: left">
                                            <?= $le->description; ?>
                                        </p>
                                    </div>
                            </div>


                        <?php } ?>

                        <?php
                  // Affichage des annonces de l'ecole
                    foreach ($last_announce as $la){
                  ?>
                  <div class="row">
                  <div class="col-lg-12">
                    <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/portal_assets/images/banner/annonces.jpg" />

                        <h3><?= $la->title; ?></h3>
                          </div>
                    <div class="col-lg-12">
                       <p style="text-align:justify">
                            <?= $la->description; ?>
                        </p>
                        <div style="text-align: right; font-size: 10px;">
                            <?= Yii::t('app','Publish at: ').$la->publishDate;?>
                        </div>
                   </div>
                </div>
                    <?php
                       }
                    ?>
                </div>
                </div>
            </div>
          </div>
    </section>


</div>


<script>

    document.addEventListener('DOMContentLoaded', function() {
      var calendarEl = document.getElementById('calendar');

      var calendar = new FullCalendar.Calendar(calendarEl, {
        plugins: [ 'bootstrap', 'interaction', 'dayGrid', 'timeGrid', 'list' ],
        themeSystem: 'bootstrap',
        defaultView: 'dayGridMonth',
        //defaultDate: '2019-05-07',
        weekNumbers: true,
        locale: 'fr',
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'dayGridMonth,timeGridWeek,listMonth'
          //right: 'dayGridMonth,timeGridWeek,timeGridDay,listMonth'
        },
        events: [
            <?= $str_json ?>
                            
        ]
      });
      calendar.setOption('themeSystem', 'bootstrap');
      calendar.render();

    });




</script>
