<div>
    <h3 style="text-align: center; font-size: 14px;"><?= Yii::t('app',"Formulaire d'inscription");?></h3>
     <h3 style="text-align: center; font-size: 14px;"><?= Yii::t('app',"Num&eacute;ro inscription : {num}",array('{num}'=>$id));?></h3>
</div>

<?php
$postulant = Postulant::model()->findByPk($id);
setlocale(LC_TIME, 'fr','fr_FR','fr_FR.utf8');
$school_name = infoGeneralConfig('school_name'); 
$school_address = infoGeneralConfig('school_address');
$date_limit = date("d",strtotime("7 weekdays",strtotime($postulant->date_created))).' '.strftime('%B',strtotime("7 weekdays",strtotime($postulant->date_created))).' '.strftime('%Y',strtotime("7 weekdays",strtotime($postulant->date_created)));
$instruction_admission = infoGeneralConfig('instruction_admission'); 

// Les champs personalisables 
$all_custom_field_data = CustomFieldData::model()->findAllBySql("SELECT * FROM custom_field_data WHERE object_id = $id AND object_type = 'post'"); 
$admission_piece = infoGeneralConfig('admission_pieces');

?>
<table width="100%" class="bodi">
    <tr style="background: #f5f0e5">
        <td colspan="4" class="bodi"><strong>INFORMATION POSTULANT</strong></td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Pr&eacute;nom </strong></td>
        <td width="25%" class="bodi"><?= $postulant->first_name ?></td>
        <td width="25%" class="bodi"><strong>Nom </strong></td>
        <td width="25%" class="bodi"><?= $postulant->last_name ?></td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Sexe</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->sexe; ?></td>
        <td width="25%" class="bodi"><strong>Date de naissance</strong></td>
        <td width="25%" class="bodi">
           
            <?php 
                   echo date("d",strtotime($postulant->birthday)).' '.strftime('%B',strtotime($postulant->birthday)).' '.strftime('%Y',strtotime($postulant->birthday));
            ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Groupe sanguin</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->blood; ?></td>
        <td width="25%" class="bodi"><strong>Lieu de naissance</strong></td>
        <td width="25%" class="bodi">
          <?= $postulant->city;?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>T&eacute;l&eacute;phone</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->phone; ?></td>
        <td width="25%" class="bodi"><strong>Email</strong></td>
        <td width="25%" class="bodi">
          <?= $postulant->email;?>
        </td>
    </tr>
    
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Adresse</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->adresse; ?></td>
        <td width="25%" class="bodi"><strong>Nationalit&eacute;</strong></td>
        <td width="25%" class="bodi">
          <?= $postulant->citizenship; ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Personne responsable</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->person_liable; ?></td>
        <td width="25%" class="bodi"><strong></strong></td>
        <td width="25%" class="bodi">
          
        </td>
    </tr>
    <tr style="background: #f5f0e5">
        <td colspan="4" class="bodi"><strong>INFORMATION ACADEMIQUE</strong></td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Classe d'admission</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->classe; ?></td>
        <td width="25%" class="bodi"><strong>Derni&egrave;re &eacute;cole fr&eacute;quent&eacute;e</strong></td>
        <td width="25%" class="bodi">
          <?= $postulant->previous_school; ?>
        </td>
    </tr>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong>Date inscription</strong></td>
        <td width="25%" class="bodi"> <?php echo date("d",strtotime($postulant->date_created)).' '.strftime('%B',strtotime($postulant->date_created)).' '.strftime('%Y',strtotime($postulant->date_created)); ?></td>
        <td width="25%" class="bodi"><strong></strong></td>
        <td width="25%" class="bodi">
         
        </td>
    </tr>
    <tr style="background: #f5f0e5">
        <td colspan="4" class="bodi"><strong>AUTRES INFORMATIONS</strong> </td>
    </tr>
    
    <!-- Here place the thing bellow -->
    
    <tr style="background: #f5f0e5">
        <td colspan="4" class="bodi"><strong>INSTRUCTIONS</strong> </td>
    </tr>
    <tr>
        <td colspan="4" class="bodi">
                <?php 
                echo Yii::t('app',$instruction_admission,array('{school_name}'=>$school_name,'{school_address}'=>$school_address,'{date_limite}'=>$date_limit));
                ?>
                   <br/>
                   <?= $admission_piece ?>
                   <br/>
                   Il est important de conserver le num&eacute;ro d'inscription : (<strong><?= $id ?></strong>) car il sera utilis&eacute; pour afficher les r&eacute;sultats sur le site web de l'&eacute;cole. 
        </td>
    </tr>
</table>
<?php 
/*
 * <?php
        if(isset($all_custom_field_data)){
            foreach($all_custom_field_data as $acfd){
                ?>
    <tr class="bodi">
        <td width="25%" class="bodi"><strong><?= $acfd->fieldLink->field_label; ?></strong></td>
        <td width="25%" class="bodi">
                <?php 
                if(isset($acfd->field_data)){
                    echo $acfd->field_data;
                } 
                ?>
        </td>
    </tr>
    <?php 
            }
        }
    ?>
 */
?>