<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//require_once __DIR__ . '/vendor/autoload.php';
// $acad = Yii::app()->session['currentId_academic_year'];
$mpdf = new \Mpdf\Mpdf(array(
        'margin_top' =>35,
	'margin_left' =>10 ,
	'margin_right' =>10,
        'margin_bottom'=>5,
	//'mirrorMargins' => true,
        'mode' => 'utf-8', 
        'format' => 'A4-P'));
//$mpdf->SetColumns(1);
$mpdf->SetHTMLHeader($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLHeader($this->renderPartial('entete-pdf',array(), true),\Mpdf\HTMLParserMode::HTML_BODY);
//$mpdf->AddColumn();
//$mpdf->shrink_tables_to_fit = 1;
$mpdf->SetColumns(1,'J',5);
$mpdf->keepColumns = true;
//$mpdf->max_colH_correction = 5;
//$mpdf->WriteHTML('<columns column-count="2" vAlign="J" column-gap="7" />');
$mpdf->WriteHTML($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($this->renderPartial('data-admission-pdf',array('id'=>$id), true),\Mpdf\HTMLParserMode::HTML_BODY);
//$mpdf->AddColumn();
//$mpdf->AddPage();
//$mpdf->WriteHTML($kontni);
$postulant = Postulant::model()->findByPk($id);
$first_name = $postulant->first_name; 
$last_name = $postulant->last_name; 
$mpdf->Output("Formulaire-inscription-$first_name-$last_name.pdf",\Mpdf\Output\Destination::INLINE);
//echo 'Hello World !';