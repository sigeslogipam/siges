<?php

/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

        $criteria2 = new CDbCriteria;
	$criteria2->condition='item_name=:item_name';
	$criteria2->params=array(':item_name'=>'school_address',);
	$school_address = GeneralConfig::model()->find($criteria2)->item_value;




?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">





    <title><?php echo infoGeneralConfig('school_name').' - Nous contacter'; ?></title>




  </head>
<!-- NAVBAR
================================================== -->
<body onload="validateHuman();">



    <!-- Marketing messaging and featurettes
    ================================================== -->
    <!-- Wrap the rest of the page in another container to center all the content. -->

<div class="container-fluid contact">

  <h1 class="title">
  <?php echo Yii::t('app','Nous contacter'); ?>
</h1>
  </br>
        <!-- style=" width: 100%; margin: 0 auto;" class="featurette-heading" -->
        <div class="row">
            <div class="col-lg-8" id="contact">


          <?php $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'contact-form',
                        'enableAjaxValidation' => false,
                        'enableClientValidation'=>true,
                        'clientOptions'=>array(
                                'validateOnSubmit'=>true,
                        ),
                )); ?>



                        <div class="form-group ">
                            <label for="name">Nom *</label>

                                <?php echo $form->textField($model,'name',array('class'=>'form-control')); ?>
                            <span class="alert-danger"><?php echo $form->error($model,'name'); ?></span>
                        </div>

                         <div class="form-group ">
                             <label for="email">Email *</label>

                                <?php echo $form->textField($model,'email',array('class'=>'form-control')); ?>
                               <span class="alert-danger"> <?php echo $form->error($model,'email'); ?></span>
                        </div>

                       <div class="form-group ">
                           <label for="subject">Sujet *</label>

                                <?php echo $form->textField($model,'subject',array('size'=>60,'maxlength'=>128,'class'=>'form-control')); ?>
                           <span class="alert-danger">   <?php echo $form->error($model,'subject'); ?></span>
                        </div>

                       <div class="form-group">
                           <label for="body">Message * </label>

                                <?php echo $form->textArea($model,'body',array('rows'=>4, 'cols'=>50,'class'=>'form-control')); ?>
                           <span class="alert-danger">  <?php echo $form->error($model,'body'); ?></span>
                        </div>

                        <div class="form-group">
                            <label><?php echo Yii::t('app','Quel est le résultat de : '); ?><span id="human1"></span> + <span id="human2"></span> ?</label>

                            <input type="hidden" id="result_hide" name="result_hide"/>
                            <input class="form-control" id="result" name="result" type="number" required="required"/>
                        </div>


                        <div class="form-group">
                                <?php echo CHtml::submitButton(Yii::t('app','Envoyer message'),array('class'=>'btn btn-success')); ?>
                        </div>

                <?php $this->endWidget(); ?>



        	</div>


             <div class="col-lg-3 sidenav" id="map_contact">
                    <!--    PHOTOS ICI -->
                <div class="row album">
                    <div class="col-lg-12">

                        <?php
                            $sql_image = "SELECT * FROM `cms_image` WHERE album IS NOT NULL AND type_image = 'album' ORDER BY id DESC LIMIT 1";
                            $cover_album = CmsImage::model()->findAllBySql($sql_image);
                            $image_name = null;
                            foreach($cover_album as $cv){
                                ?>

                        <div id="title" >
                            <h4><a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie">
                                    <i class="fa fa-expand" aria-hidden="true"></i>&nbsp;
                                        <?= $cv->album0->album_name; ?>
                                </a>
                            </h4>
                        </div>
                        <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie">
                            <img class="img_1" alt="siges" src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $cv->nom_image; ?>" />
                        </a>
                                 <!--
                                               <div id="desc-gallerie" >  <a href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/gallerie"><?= $cv->album0->album_name; ?></a> </div>
                                 -->
                        <?php
                            }
                        ?>

                    </div>

                </div>
                    
                 

                 <div class="well">
                      <!-- Liste des autres articles ici! -->
                        <div class="row">

                          <div class="col-lg-12">
                              <div id="title" ><span> 
                             <i class="fa fa-pen-alt" aria-hidden="true"></i>
                            <?= Yii::t('app','Other articles')?> </span> </div>
                              <ul class="nav flex-column">
                              <?php

                              $all_related_articles = CmsArticle::model()->findAllBySql("SELECT * FROM cms_article WHERE is_publish = 1 ORDER BY id DESC LIMIT 5");

                              if(!empty($all_related_articles)){

                              foreach($all_related_articles as $ara){

                                  ?>
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $ara->id; ?>&menu=<?= $ara->article_menu; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= $ara->article_title; ?>
                                      </a>
                                  </li>
                              <?php

                              }
                              }


                              ?>
                              </ul>
                          </div>
                      </div>
                </div>


            </div>
        </div>
        

</div>



</body>

<script type="text/javascript">


function validateHuman(){
    var d_number = ["Zero","Un","Deux","Trois","Quatre","Cinq","Six","Sept","Huit","Neuf"];

    var rand1 = Math.round(Math.random()*10);
    var rand2 = Math.round(Math.random()*10);
    var result_addition = 0;


    if((rand1>=0 && rand1<=9)&&(rand2>=0 && rand2<=9)){
        document.getElementById("human1").innerHTML = d_number[rand1];
        document.getElementById("human2").innerHTML = d_number[rand2];
        result_addition = rand1+rand2;
        document.getElementById("result_hide").value = result_addition;

    }
    else{
        document.getElementById("human1").innerHTML = 4;
        document.getElementById("human2").innerHTML = 7;
        result_addition = 11;
        document.getElementById("result_hide").value = result_addition;
    }


  }

</script>
