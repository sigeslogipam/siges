<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 

setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');

//mailsend("jcpoulard@gmail.com", "scanado@gmail.com", "test brital", "Nap fout teste email en force brital!");

$model_article = new CmsArticle(); 
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/css/admission.css");

$is_admission_publish = infoGeneralConfig('publish_admission_result');

?>
<div class="container-fluid masthead bg-one text-white text-center">
    <section>
        <div class="container-fluid-top text-center">
            <div class="row content">
                <div class="col-sm-12 col-md-9 text-left">
                    <div class="bottom-color"><span class="title clr">  Formulaire d'admission</span></div>
                    <?php 
                      $form=$this->beginWidget('CActiveForm', array(
                        'id'=>'products-form',
                        'enableAjaxValidation'=>false,
                        )); 
                    ?>
                    <div class='row'>
                        <div class="form-group col-md-6"> 
                            <?php echo $form->labelEx($model,'last_name',array('for'=>'last_name')); ?>
                            <?php echo $form->textField($model,'last_name',array('size'=>60,'maxlength'=>128,'placeholder'=>Yii::t('app','Last name'),'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'last_name'); ?>
                        </div>
                        <div class="form-group col-md-6"> 
                            <?php echo $form->labelEx($model,'first_name',array('for'=>'first_name')); ?>
                            <?php echo $form->textField($model,'first_name',array('size'=>60,'maxlength'=>128,'placeholder'=>Yii::t('app','First name'),'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'first_name'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model,'gender',array('for'=>'gender')); ?>
                            <?php echo $form->dropDownList($model,'gender',$model->getGenders(),array('prompt'=>Yii::t('app','-- Select gender --'),'class'=>'form-control')); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="blood_group"><?php echo Yii::t('app','Blood Group');?></label>
                            <?php echo $form->dropDownList($model,'blood_group',$model->getBlood_groupValue(),array('prompt'=>Yii::t('app','-- Select blood group --'),'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'blood_group'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model,'birthday',array('for'=>'birthday')); ?>
                            <?php $this->widget('zii.widgets.jui.CJuiDatePicker',
                                 array(
                                 'model'=>'$model',
                                 'name'=>'Persons[birthday]',
                                 'language'=>'fr',
                                 'value'=>$model->birthday,
                                 'htmlOptions'=>array('size'=>60, 'style'=>'width:100% !important','placeholder'=>Yii::t('app','Birthday'),'class'=>'form-control'),
                                         'options'=>array(
                                         'showButtonPanel'=>true,
                                         'changeYear'=>true,                                      
                                         'dateFormat'=>'yy-mm-dd',
                                         'yearRange'=>'1900:2100',
                                         'changeMonth'=>true,
                                         'showButtonPane'=>true,

                                          'dateFormat'=>'yy-mm-dd',   
                                         ),
                                 )
                                );
                             ?>
                             <?php echo $form->error($model,'birthday'); ?>
                            
                         </div>
                        <div class="form-group col-md-6">
                                <label for="cities">
                                    <?php echo Yii::t('app','Birth place'); ?> 
                                </label>
                                <?php 
                                    $criteria = new CDbCriteria(array('order'=>'city_name'));

                                    echo $form->dropDownList($model, 'cities',
                                    CHtml::listData(Cities::model()->findAll($criteria),'id','city_name'),
                                    array('prompt'=>Yii::t('app','-- Please select city --'),'class'=>'form-control')
                                    );
                                    ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model,'phone',array('for'=>'phone')); ?>
                            <?php echo $form->textField($model,'phone',array('size'=>60,'maxlength'=>45,'placeholder'=>Yii::t('app','Phone'),'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'phone'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model,'email',array('for'=>'email')); ?>

                            <?php echo $form->textField($model,'email',array('size'=>60,'maxlength'=>45,'placeholder'=>Yii::t('app','Email'),'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'email'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model,'confirm_email',array('for'=>'confirm_email')); ?>

                            <?php echo $form->textField($model,'confirm_email',array('size'=>60,'maxlength'=>45,'placeholder'=>Yii::t('app','Confirmer Email'),'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'confirm_email'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model,'adresse',array('for'=>'adresse')); ?>
                            <?php echo $form->textField($model,'adresse',array('size'=>60,'maxlength'=>255,'placeholder'=>Yii::t('app','Adresse'),'class'=>'form-control')); ?>
                            <?php echo $form->error($model,'adresse'); ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($model,'citizenship',array('for'=>'citizenship')); ?>
                            <?php echo $form->textField($model,'citizenship',array('size'=>60,'maxlength'=>45,'placeholder'=>Yii::t('app','Citizenship'),'class'=>'form-control')); 
                                echo $form->error($model,'citizenship'); 
                            ?>
                        </div>
                        <div class="form-group col-md-6">
                          <?php 
                                echo $form->labelEx($modelStudentOtherInfo,'person_liable',array('for'=>'person_liable'));                        
                                echo $form->textField($modelStudentOtherInfo,'person_liable',array('size'=>60,'maxlength'=>100,'placeholder'=>Yii::t('app','Person liable'),'class'=>'form-control'));  
                                echo $form->error($modelStudentOtherInfo,'person_liable'); 
                          ?>
                        </div>
                        <div class="form-group col-md-6">
                            <?php echo $form->labelEx($modelStudentOtherInfo,'person_liable_phone',array('for'=>'person_liable_phone')); 
                                   echo $form->textField($modelStudentOtherInfo,'person_liable_phone',array('size'=>60,'maxlength'=>65,'placeholder'=>Yii::t('app','Person liable phone'),'class'=>'form-control'));
                                   echo $form->error($modelStudentOtherInfo,'person_liable_phone'); 
                            ?>
                         </div>
                        <div class="form-group col-md-6">
                            <?php  
                                echo $form->labelEx($modelStudentOtherInfo,'previous_school',array('for'=>'previous_school'));
                                echo $form->textField($modelStudentOtherInfo,'previous_school',array('size'=>60,'maxlength'=>255,'placeholder'=>Yii::t('app','Previous School'),'class'=>'form-control'));
                                echo $form->error($modelStudentOtherInfo,'previous_school'); 
                             ?>
                        </div>
                        <?php 
                        $list_level = infoGeneralConfig('classe_inscription'); 
                        if(!isset($list_level)) { ?>
                        <div class="form-group col-md-6">
                            <?php 
                                echo $form->labelEx($model,'admission_en',array('for'=>'admission_en')); 
                                echo $form->dropDownList($model,'admission_en',$this->loadLevel(),array('class'=>'form-control')); 
                                echo $form->error($model,'admission_en'); 
                            ?>
                        </div>
                        <?php }else {
                            
                            ?>
                            
                        <div class="form-group col-md-6">
                            <?php 
                                echo $form->labelEx($model,'admission_en',array('for'=>'admission_en')); 
                                $list_id_level = infoGeneralConfig('classe_inscription'); 
                                $level_inscription =  CHtml::listData(Levels::model()->findAllBySql("SELECT id, level_name FROM levels WHERE id IN ($list_id_level)"),'id','level_name'); 
                                echo $form->dropDownList($model,'admission_en',$level_inscription,array('class'=>'form-control', 'prompt'=>Yii::t('app',''))); 
                                echo $form->error($model,'admission_en'); 
                            ?>
                        </div>
                        
                        <?php 
                        } ?>
                        
                        <!-- chan pesonalize -->
                        <!-- Champs personalisable ICI  -->    
       
                        <?php
                    $criteria = new CDbCriteria; 
                    $criteria->condition = "field_related_to='stud'";
                    if(!isset($_GET['id'])){  // Si on est en mode creation des champs personalisables 
                    
                            $mCustomField = CustomField::model()->findAllBySql("SELECT * FROM custom_field WHERE field_related_to = 'stud' ORDER BY order_field "); //CustomField::model()->findAll($criteria); 
                            $i=0;
                            foreach($mCustomField as $mc){
                                switch ($mc->field_type){
                                    case "txt" : {


                    ?> 

                                <div class="form-group col-md-6">
                                    <label for='<?php echo $mc->field_name; ?>'><?php echo $mc->field_label; ?></label>
                                    <input size="60" name="<?php echo $mc->field_name; ?>" placeholder="<?php echo $mc->field_label; ?>" class='form-control'>
                                 </div>


                    <?php 
                    }
                    break;
                            case "date" : {

                    ?>
                    <div class="form-group col-md-6">
                        
                        <label for='<?php echo $mc->field_name; ?>'><?php echo $mc->field_label; ?></label>
                            <input size="60" class="custom_date form-control" name="<?php echo $mc->field_name; ?>" placeholder="<?php echo $mc->field_label; ?>" type="date">
                    
                    </div>

                    <?php 
                    }
                    break;
                            case "combo" :{

                    ?>
                        <div class="form-group col-md-6">
                            <label for="<?= $mc->field_name; ?>"><?= $mc->field_label; ?></label>
                            
                            <?php
                                echo '<select name="'.$mc->field_name.'" class="form-control">';
                                $field_value = explode(",", $mc->field_option);
                                foreach($field_value as $fv){
                                   echo '<option value="'.$fv.'">'.$fv.'</option>';
                                }
                                echo '</select>';
                            ?>
                        
                        </div>
                    <?php 

                    }
                        }
                    }
        }?>
        
                  </div>
                    <div class='row'>
<div class="col-sm-12 col-md-6" style="margin: 20px 0px">
                                <button type='submit' name="btn_save" class="glyphicon glyphicon-save btn btn-success"> Confirmer inscription & Imprimer le formulaire</button>
                             </div>


                            <div class="col-sm-12 col-md-6" style="margin: 20px 0px">
                                <button type="submit" name="btn_cancel" class="glyphicon glyphicon-trash btn btn-danger"> Annuler l'inscription</button> 
                            </div>
                        </div>
                    <?php $this->endWidget(); ?>
                </div>
                <div class="col-sm-12 col-md-3 sidenav">
                    <!-- Si la periode d'admission est active --> 
                 <?php if(infoGeneralConfig('is_admission_open')==1) {
                     $critere = "Condition d\'admission";
                     $condition_admission = CmsMenu::model()->findAllBySql("SELECT id FROM cms_menu WHERE menu_label = '$critere'");
                     $id_adm = null;
                     foreach($condition_admission as $ca){
                         $id_adm = $ca->id;
                     }
                     ?>
                 <div class="well">
                     <div class="row">
                         <div class="col-lg-12">
                             <div id="title" ><span> <i class="fas fa-user-graduate" aria-hidden="true"></i> <?= Yii::t('app','Admission')?> </span> </div>
                             <ul class="nav flex-column">
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($id_adm); ?>&menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app',"Condition d'admission") ?>
                                      </a>
                                  </li>
                                  <?php if($is_admission_publish==1){?>
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/resultatadmission?menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app','R&eacute;sultat admission'); ?>
                                      </a>
                                  </li>
                                  <?php }?>
                             </ul>
                         </div>
                     </div>
                 </div>
                 <?php } ?>
                 <!-- Si la periode d'admission est active --> 
                </div>
            </div>
        </div>
    </section>
</div>



