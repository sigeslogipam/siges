<?php
$all_gallerie = CmsImage::model()->findAllByAttributes(array('type_image'=>'album'));
$all_album = CmsAlbumCat::model()->findAll();
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/portal_assets/css/album-style.css");
$this->title = "Gallerie";
?>
<div class="container-fluid masthead bg-one text-white text-center">
    <section>
        <div class="container-fluid-top text-center">
            <div class="row content gallerie">
              <div class="gallerie">
                <div class="col-sm-12">
                   <div class="row back"><div class="album_name"><span class="title clr"> Galerie Photo </span></div></div>
                    <div class="row">
                    <?php

                    foreach($all_album as $aa){
                        $all_cover_image = CmsImage::model()->findAllBySql("SELECT * FROM cms_image WHERE album = $aa->id ORDER BY id DESC LIMIT 1");
                        $yon_grenn_image = null;
                        if(isset($all_cover_image)){
                        foreach($all_cover_image as $ac){
                            $yon_grenn_image = $ac->nom_image;
                            }
                        }
                       ?>
                        <div class="col-sm-3">
                            <div class="item">
                                <a href='<?= Yii::app()->baseUrl ?>/index.php/portal/default/galleriedetails?album=<?= $aa->id; ?>'>
                                <img src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $yon_grenn_image; ?>" class="img-thumbnail">
                                </a>
                                <h5>
                                    <a href='<?= Yii::app()->baseUrl ?>/index.php/portal/default/galleriedetails?album=<?= $aa->id; ?>'><?= $aa->album_name;?> </a>
                                </h5>
                            </div>

                        </div>

                        <?php

                    }
                    ?>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
</div>


<!--
<div class="col-sm-6">
                            <div class="album-cover">
                                <a href="gallery-album.html"><img class="img-fluid" src="<?= Yii::app()->baseUrl ?>/cms_files/images/banners/<?= $yon_grenn_image; ?>" alt=""></a>
                                <div class="desc">
                                    <h4><small><a href="#"><?= $aa->album_name;?></a></small></h4>
                                    <p>Integer ornare nisl tortor, sed condimentum metus pulvinar ut. Etiam ac pretium nunc. Donec porttitor erat non nibh pellentesque vehicula. Vestibulum tincidunt</p>
                                </div>
                            </div>

                        </div>
-->
