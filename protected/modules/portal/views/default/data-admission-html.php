

<?php
$postulant = Postulant::model()->findByPk($id);
setlocale(LC_TIME, 'fr','fr_FR','fr_FR@euro','fr_FR.utf8','fr-FR','fra');
$school_name = infoGeneralConfig('school_name'); 
$school_address = infoGeneralConfig('school_address');
$date_limit = date("d",strtotime("7 weekdays",strtotime($postulant->date_created))).' '.strftime('%B',strtotime("7 weekdays",strtotime($postulant->date_created))).' '.strftime('%Y',strtotime("7 weekdays",strtotime($postulant->date_created)));
$instruction_admission = infoGeneralConfig('instruction_admission'); 

// Les champs personalisables 
$all_custom_field_data = CustomFieldData::model()->findAllBySql("SELECT * FROM custom_field_data WHERE object_id = $id"); 
$admission_piece = infoGeneralConfig('admission_pieces');

?>

<div class="container-fluid masthead bg-one text-white text-center">
    <section>
  <div class="container-fluid-top text-center">
    <div class="row content">
         <div class="col-sm-9 text-left">
<div class='row'>
    <div>
         <h2 style="text-align: center; font-size: 24px;"><?= Yii::t('app',"Formulaire d'inscription");?></h2>
         <h3 style="text-align: center"><?= Yii::t('app',"Num&eacute;ro inscription : {num}",array('{num}'=>$id));?></h3>
    </div>
    <div>
        <a class="btn btn-lg  btn-success" href='<?= Yii::app()->baseUrl."/index.php/portal/default/admissionform?id=".$postulant->id; ?>'><i class="fa fa-print"></i>  <?= Yii::t('app','Print admission form') ?></a>
    </div>
    <div>
        <a class="btn btn-lg  btn-danger" href='<?= Yii::app()->baseUrl."/index.php/portal/default/admissioncancel?id=".$postulant->id; ?>' onclick="return confirm('<?= Yii::t('app','Do you really want to cancel this application form ?');?>')"><i class="fa fa-trash"> <?= Yii::t('app','Cancel admission') ?></i></a>
    </div>
</div>
    
<table width="100%" class='table table-striped'>
    <tr>
        <td colspan="4"><strong>INFORMATION POSTULANT</strong></td>
    </tr>
    <tr class="bodi">
        <td width="25%" style='text-align: left;'><strong>Pr&eacute;nom </strong></td>
        <td width="25%" style='text-align: left;'><?= $postulant->first_name ?></td>
        <td width="25%" style='text-align: left;'><strong>Nom </strong></td>
        <td width="25%" style='text-align: left;'><?= $postulant->last_name ?></td>
    </tr>
    <tr class="bodi">
        <td width="25%" style='text-align: left;'><strong>Sexe</strong></td>
        <td width="25%" style='text-align: left;'> <?= $postulant->sexe; ?></td>
        <td width="25%" style='text-align: left;'><strong>Date de naissance</strong></td>
        <td width="25%" style='text-align: left;'>
           
            <?php 
                   echo date("d",strtotime($postulant->birthday)).' '.strftime('%B',strtotime($postulant->birthday)).' '.strftime('%Y',strtotime($postulant->birthday));
            ?>
        </td>
    </tr>
    <tr class="bodi" style='text-align: left;'>
        <td width="25%"><strong>Groupe sanguin</strong></td>
        <td width="25%"> <?= $postulant->blood; ?></td>
        <td width="25%"><strong>Lieu de naissance</strong></td>
        <td width="25%">
          <?= $postulant->city;?>
        </td>
    </tr>
    <tr class="bodi" style='text-align: left;'>
        <td width="25%"><strong>T&eacute;l&eacute;phone</strong></td>
        <td width="25%"> <?= $postulant->phone; ?></td>
        <td width="25%"><strong>Email</strong></td>
        <td width="25%">
          <?= $postulant->email;?>
        </td>
    </tr>
    
    <tr class="bodi" style='text-align: left;'>
        <td width="25%"><strong>Adresse</strong></td>
        <td width="25%"> <?= $postulant->adresse; ?></td>
        <td width="25%"><strong>Nationalit&eacute;</strong></td>
        <td width="25%">
          <?= $postulant->citizenship; ?>
        </td>
    </tr>
    <tr class="bodi" style='text-align: left;'>
        <td width="25%"><strong>Personne responsable</strong></td>
        <td width="25%"> <?= $postulant->person_liable; ?></td>
        <td width="25%"><strong></strong></td>
        <td width="25%">
          
        </td>
    </tr>
    <tr>
        <td colspan="4"><strong>INFORMATION ACADEMIQUE</strong></td>
    </tr>
    <tr class="bodi" style='text-align: left;'>
        <td width="25%" class="bodi"><strong>Classe d'admission</strong></td>
        <td width="25%" class="bodi"> <?= $postulant->classe; ?></td>
        <td width="25%" class="bodi"><strong>Derni&egrave;re &eacute;cole fr&eacute;quent&eacute;e</strong></td>
        <td width="25%" class="bodi">
          <?= $postulant->previous_school; ?>
        </td>
    </tr>
    <tr class="bodi" style='text-align: left;'>
        <td width="25%"><strong>Date inscription</strong></td>
        <td width="25%"> <?php echo date("d",strtotime($postulant->date_created)).' '.strftime('%B',strtotime($postulant->date_created)).' '.strftime('%Y',strtotime($postulant->date_created)); ?></td>
        <td width="25%"><strong></strong></td>
        <td width="25%">
         
        </td>
    </tr>
    <tr>
        <td colspan="4"><strong>AUTRES INFORMATIONS</strong> </td>
    </tr>
    
    <?php
        if(isset($all_custom_field_data)){
            foreach($all_custom_field_data as $acfd){
                ?>
    <tr class="bodi" style='text-align: left;'>
        <td width="25%"><strong><?= $acfd->fieldLink->field_label; ?></strong></td>
        <td width="25%"><?= $acfd->field_data; ?></td>
    </tr>
    <?php 
            }
        }
    ?>
    
    <tr>
        <td colspan="4"><strong>INSTRUCTIONS</strong> </td>
    </tr>
    <tr style='text-align: left;'>
        <td colspan="4">
                <?php 
                echo Yii::t('app',$instruction_admission,array('{school_name}'=>$school_name,'{school_address}'=>$school_address,'{date_limite}'=>$date_limit));
                ?>
                   <br/>
                   <?= $admission_piece ?>
                   <br/>
                   Il est important de conserver le num&eacute;ro d'inscription : (<strong><?= $id ?></strong>) car il est sera utilis&eacute; pour afficher les r&eacute;sultats sur le site web de l'&eacute;cle. 
        </td>
    </tr>
</table>
    </div>
        
        <div class="col-sm-3 sidenav">
                    <!-- Si la periode d'admission est active --> 
                 <?php 
                 $model_article = new CmsArticle();
                 if(infoGeneralConfig('is_admission_open')==1) {
                     $critere = "Condition d\'admission";
                     $condition_admission = CmsMenu::model()->findAllBySql("SELECT id FROM cms_menu WHERE menu_label = '$critere'");
                     $id_adm = null;
                     foreach($condition_admission as $ca){
                         $id_adm = $ca->id;
                     }
                     ?>
                 <div class="well">
                     <div class="row">
                         <div class="col-lg-12">
                             <div id="title" ><span> <i class="fas fa-user-graduate" aria-hidden="true"></i> <?= Yii::t('app','Admission')?> </span> </div>
                             <ul class="nav flex-column">
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/articledetails?id=<?= $model_article->getLastArticleIdByMenu($id_adm); ?>&menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app',"Condition d'admission") ?>
                                      </a>
                                  </li>
                                  
                                  <li class="nav-item">
                                      <a class="nav-link" href="<?= Yii::app()->baseUrl ?>/index.php/portal/default/resultatadmission?menu=<?= $id_adm; ?>">
                                      <i class="fas fa-angle-right"></i>&nbsp;
                                      <?= Yii::t('app','R&eacute;sultat admission'); ?>
                                      </a>
                                  </li>
                             </ul>
                         </div>
                     </div>
                 </div>
                 <?php } ?>
                 <!-- Si la periode d'admission est active --> 
                </div>
        
        </div>
  </div>
        </section>

</div>