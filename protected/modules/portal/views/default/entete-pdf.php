<table>
    <tr class='koule'>
        <th width="20%">
            <img src="<?= Yii::app()->baseUrl;?>/css/images/school_logo.png" width="100px" height="100px">
            
        </th>
        <th width="60%">
            <span style="text-align: center;"><?= infoGeneralConfig('school_name');?></span>
            <br/>
            <span style="font-size: 12px;"><?= infoGeneralConfig('school_address'); ?></span>
            <br/>
            <span style="text-align: center; font-size: 12px;"><?= infoGeneralConfig('school_phone_number'); ?></span>
        </th>
        <th width="30%" style="text-align: right">
            <img src="<?= Yii::app()->baseUrl;?>/css/images/no_pic.png" width="100px" height="100px">
        </th>
    </tr>
</table>

