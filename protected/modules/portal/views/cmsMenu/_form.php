<?php
/* @var $this CmsMenuController */
/* @var $model CmsMenu */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cms-menu-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        
        <div  id="resp_form_siges">
            <form  id="resp_form" >
                <div class="col-3">
                    <label id="resp_form">
                        <?php echo $form->labelEx($model,'menu_label'); ?>
                        <?php echo $form->textField($model,'menu_label',array('size'=>60,'maxlength'=>64)); ?>
                        <?php echo $form->error($model,'menu_label'); ?>
                    </label>
                </div>
                <div class="col-3">
                    <label id="resp_form">
                        <?php echo $form->label($model, 'parent_menu') ?>
                        <?php
                            $criteria = new CDbCriteria(array('condition'=>'is_publish=1 AND is_parent_menu = 1', 'order'=>'menu_label',));

                            echo $form->dropDownList($model, 'parent_menu',
                            CHtml::listData(CmsMenu::model()->findAll($criteria),'id','menu_label'),
                            array('prompt'=>Yii::t('app','Select a menu'))
                            );
                        ?>
                    </label>
                </div>
                <div class="col-submit">
                                
                    <?php 
                    if(!isset($_GET['id'])){
                             echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning'));
                             echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                            }
                             else
                               {  echo CHtml::submitButton(Yii::t('app', 'Save'),array('name'=>'update','class'=>'btn btn-warning'));

                                  echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));

                                 } 
                               //back button   
                                  $url=Yii::app()->request->urlReferrer;
                                  $explode_url= explode("php",substr($url,0));

                                 if(!isset($_GET['from']))
                                    echo '<a href="'.$explode_url[0].'php'.$this->back_url.'" class="btn btn-secondary">'.Yii::t('app', 'Back').'</a>';

                        ?>
                 </div>     
            </form>
        </div>
  
<?php $this->endWidget(); ?>

</div><!-- form -->