<div class="form">
    


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cms-menu-form',
	'enableAjaxValidation'=>false,
)); ?>
    <div  id="resp_form_siges">
        <form  id="resp_form">
            
            <div class="col-12">
                <label id="resp_form">
                    <?php echo $form->labelEx($model,'menu_label'); ?>
                    <?php echo $form->textField($model,'menu_label',array('size'=>60,'maxlength'=>64)); ?>
                    <?php echo $form->error($model,'menu_label'); ?>
                </label>
            </div>
           
           
            <div class="col-12">
                    <label id="resp_form">
                        <?php echo $form->label($model, 'is_publish') ?>
                        <?php
                        $list_yes_no = array(0=>Yii::t('app','No'),1=>Yii::t('app','Yes'));
                           // $criteria = new CDbCriteria(array('condition'=>'is_publish=1 AND is_parent_menu = 1', 'order'=>'menu_label',));

                            echo $form->dropDownList($model, 'is_publish',
                                    $list_yes_no,
                            ///CHtml::listData(CmsMenu::model()->findAll($criteria),'id','menu_label'),
                            array('prompt'=>Yii::t('app','Is Publish'))
                            );
                        ?>
                    </label>
                
            </div>
            
            <input type="hidden" id="id_menu" name="id_menu" value="<?= $model->id ?>">
        </form>
    </div>

<?php $this->endWidget(); ?>

</div>