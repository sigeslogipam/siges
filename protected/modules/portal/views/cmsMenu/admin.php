<?php
/*
 * © 2016 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#cms-menu-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");



$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/css/bootstrap/css/bootstrap.min.css");

$cs->registerScriptFile($baseUrl."/css/bootstrap/js/bootstrap.min.js");
?>



<div id="dash">
   <div class="span3">
       <h2>
        <?php echo Yii::t('app','Manage menu');?>
        
    </h2> 
   </div>
    
    <div class="span3">
        <div class="span4">
            <?php
                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                               // build the link in Yii standard
                     echo CHtml::link($images,array('cmsMenu/create')); 
            ?>
        </div>
        
        
        
        <div class="span4">
            <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                     echo CHtml::link($images,array('/portal/cmsMenu/admin')); 

                    ?>
        </div>
    </div>
    
    
</div>

<div class="clear"></div>

<?php
    echo $this->renderPartial('//layouts/navBasePortal',NULL,true);	
    ?>
<?php 
    $all_menu_principal = CmsMenu::model()->findAllByAttributes(array('is_parent_menu'=>1));
    $all_ordinary_menu = CmsMenu::model()->findAllByAttributes(array('is_parent_menu'=>null));
?>

<ul id="menu" class="meni-akodeyon">
    <?php 
    foreach($all_menu_principal as $amp){
        ?>
    <li>
        <?php 
        if($amp->is_publish == 1){
        ?>
        <a href="#" class="menu-principal"><?= $amp->menu_label; ?>
            <i class="fa fa-check" style="color: green"></i> 
        </a>
        <i class="fa fa-edit modifye-main-menu" data-idmenuprincipal="<?= $amp->id; ?>"></i>
        <?php }else{ ?>
        <a href="#" data-idmenuprincipal="<?= $amp->id; ?>" class="menu-principal"><?= $amp->menu_label; ?>
            <i class="fa fa-times" style="color: red"></i> 
        </a>
        <i class="fa fa-edit modifye-main-menu" data-idmenuprincipal="<?= $amp->id; ?>"></i>
        <?php }  ?>
        
        <ul>
            <?php 
                $all_sub_menu = CmsMenu::model()->findAllByAttributes(array('parent_menu'=>$amp->id));
                foreach($all_sub_menu as $asm){
                    ?>
            
            <li>
                <?php if($asm->is_publish == 1) {?>
                <a href="#" data-idsubmenu="<?= $asm->id; ?>" class="sub-menu"><?= $asm->menu_label; ?></a>
                <i class="fa fa-check" style="color: green"></i>
                <?php if($asm->isMenuHaveArticle==0) {?>
                <i class="fa fa-trash efase-menu" data-idmenuefase="<?= $asm->id; ?>"></i>
                <?php } ?>
                <?php  } else {?>
                <a href="#" data-idsubmenu="<?= $asm->id; ?>" class="sub-menu"><?= $asm->menu_label; ?></a>
                <i class="fa fa-times" style="color: red"></i>
                <?php if($asm->isMenuHaveArticle==0) {?>
                <i class="fa fa-trash efase-menu" data-idmenuefase="<?= $asm->id; ?>"></i>
                <?php } ?>
                <?php } ?>
            </li>
            <?php 
                }
            ?>
        </ul>
    </li>
    <?php 
    }
    ?>
</ul>
<ul class="meni-akodeyon">
   <?php 
   foreach($all_ordinary_menu as $aom){
       ?>
    <li>
        <?php if($aom->is_publish == 1) {?>
        <a href="#" data-idsubmenu="<?= $aom->id; ?>" class="<?php if($aom->is_special == null){ echo "sub-menu";}else{echo ""; }?>"><?= $aom->menu_label; ?></a>
        <i class="fa fa-check" style="color: green"></i>
        <?php  } else {?>
        <a href="#" data-idsubmenu="<?= $aom->id; ?>" class="<?php if($aom->is_special == null){ echo "sub-menu";}else{echo ""; }?>"><?= $aom->menu_label; ?></a>
        <i class="fa fa-times" style="color: red"></i>
        <?php } ?>
    </li>
   <?php 
   }
   ?>
</ul>


<!-- Modal Main menu -->
  <div class="modal fade" id="modal-main-menu" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Update main menu'); ?></h4>
        </div>
        <div class="modal-body">
            <div id="plas-main-menu">
                
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update-main-menu"><?= Yii::t('app','Update'); ?></button>  
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close');?></button>
        </div>
      </div>
      
    </div>
  </div>

<!-- Modal Sub menu-->
  <div class="modal fade" id="modal-sub-menu" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Update sub menu'); ?></h4>
        </div>
        <div class="modal-body">
            <div id="plas-sub-menu">
                
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update-sub-menu"><?= Yii::t('app','Update'); ?></button>
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close');?></button>
        </div>
      </div>
      
    </div>
  </div>



<?php 
/*
$criteria = new CDbCriteria(array('condition'=>'is_publish=1 AND is_parent_menu = 1', 'order'=>'menu_label',));
$data_list = CHtml::listData(CmsMenu::model()->findAll($criteria),'id','menu_label');

$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'cms-menu-grid',
        'itemsCssClass' => 'table-bordered items',
	'dataProvider'=>$model->search(),
        'pager'=>array(

            'class'=>'CLinkPager',

            'pageSize' => 50,

            'firstPageLabel'=>'Erste',

            'lastPageLabel'=>'Letzte',

            'nextPageLabel'=>'Nächste',

            'prevPageLabel'=>'Vorherige',

            'header'=>'',


        ),
	
	'columns'=>array(
		
            array(
                'class' => 'editable.EditableColumn',
                'name'=>'menu_label',
                'headerHtmlOptions' => array('style' => 'width: 200px'),
                'editable' => array(    //editable section
                 'apply'      => '$data->is_home != 1',
                  'url'        => $this->createUrl('cmsMenu/updateMenu'),
                  'placement'  => 'right',
              )  
            ),
            
            array(
                'class' => 'editable.EditableColumn',
                'name'=>'is_publish',
                'headerHtmlOptions' => array('style' => 'width: 200px'),
                'editable'=>array(
                  'type'     => 'select',
                  'apply'      => '$data->is_home != 1',
                  'url'      => $this->createUrl('cmsMenu/updateMenu'),
                  
                 'source'=>Editable::source(array(0 =>Yii::t('app','No'),  1=>Yii::t('app','Yes'))),
                  'options'  => array(    //custom display 
                     'display' => 'js: function(value, sourceData) {
                          var selected = $.grep(sourceData, function(o){ return value == o.value; }),
                              colors = {1: "green", 0: "red"};
                          $(this).text(selected[0].text).css("color", colors[value]);    
                      }'
                  ),
                 //onsave event handler 
                 'onSave' => 'js: function(e, params) {
                      console && console.log("saved value: "+params.newValue);
                 }',
                ),
            ),
            
            array(
                'class' => 'editable.EditableColumn',
                'name'=>'menu_position',
                'headerHtmlOptions' => array('style' => 'width: 200px'),
                'editable'=>array(
                    'type'     => 'select',
                  'apply'      => '$data->is_home != 1',
                  'url'      => $this->createUrl('cmsMenu/updateMenu'),
                 
                 'source'=>Editable::source(array(0 =>Yii::t('app','1st'),  1=>Yii::t('app','2nd'),2=>Yii::t('app','3rd'),3=>Yii::t('app','4th'),4=>Yii::t('app','5th'),5=>Yii::t('app','6th'))),
               
                 //onsave event handler 
                 'onSave' => 'js: function(e, params) {
                      console && console.log("saved value: "+params.newValue);
                 }',
                ),
            ),
            array(
                'class' => 'editable.EditableColumn',
                'name'=>'is_parent_menu',
                'headerHtmlOptions' => array('style' => 'width: 200px'),
                'editable' => array(    //editable section
                  'type'     => 'select',   
                 'apply'      => '$data->is_home != 1',
                  'url'        => $this->createUrl('cmsMenu/updateMenu'),
                 'source'=>Editable::source(array(0 =>Yii::t('app','No'),  1=>Yii::t('app','Yes'))),
                  //onsave event handler 
                 'onSave' => 'js: function(e, params) {
                      console && console.log("saved value: "+params.newValue);
                      document.location.reload();
                 }',   
                  
              )  
            ),
            
           array(
                'class' => 'editable.EditableColumn',
                'name'=>'parent_menu',
                'headerHtmlOptions' => array('style' => 'width: 200px'),
                'editable' => array(    //editable section
                  'type'     => 'select',   
                 'apply'      => '$data->is_parent_menu != 1',
                  'url'        => $this->createUrl('cmsMenu/updateMenu'),
                 'source'=>Editable::source($data_list),
                  //onsave event handler 
                 'onSave' => 'js: function(e, params) {
                      console && console.log("saved value: "+params.newValue);
                 }',   
                  
              )  
            ),
		//'menuParent',
		'update_by',
            array(
			'class'=>'CButtonColumn',
                        'template'=>'{delete}',
                         'buttons'=>array(
                         'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                            
                       
		),
		
		
	),
));
*/

$message_supr = Yii::t('app','Do you really want to delete this menu ?');

?>


<script>
    // Pour les main menu
     $(".modifye-main-menu").click(function(){
                var menu_id = $(this).attr("data-idmenuprincipal"); 
                $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsMenu/modifyemenu',{id:menu_id},function(data){
                    $("#plas-main-menu").html(data);
                    
                });
                
                $("#modal-main-menu").modal();
                 $('#modal-main-menu').on('hidden.bs.modal', function () {
                        
                  });
           });
       // Pour les sous menu     
     $(".sub-menu").click(function(){
                var menu_id = $(this).attr("data-idsubmenu"); 
                $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsMenu/modifyesubmenu',{id:menu_id},function(data){
                    $("#plas-sub-menu").html(data);
                    
                });
                
                $("#modal-sub-menu").modal();
                $('#modal-sub-menu').on('hidden.bs.modal', function () {
                        
                  });
           });      
           
           
      // Pour les main menu      
     $("#update-main-menu").click(function(){
         var menu_label = $("#CmsMenu_menu_label").val();
         var is_publish = $("#CmsMenu_is_publish").val(); 
         var id_menu = $("#id_menu").val(); 
         $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsMenu/miseajourmainmenu',{id:id_menu,menu_label:menu_label,is_publish:is_publish},function(data){
             
                 $('#modal-main-menu').on('hidden.bs.modal', function () {
                        
                  });
                 location.reload();
             
         });
        // alert(id_menu);
     });      
    
    
    // Pour les sub menu 
    $("#update-sub-menu").click(function(){
        var menu_label = null;
        var is_publish = null;
        var parent_menu = null;
         menu_label = $("#CmsMenu_menu_label").val();
         is_publish = $("#CmsMenu_is_publish").val();
         var id_menu = $("#id_menu").val();
         
         
         if($("#CmsMenu_parent_menu").length == 0) {
                        parent_menu = null;
            }else{
                parent_menu = $("#CmsMenu_parent_menu").val();
            }
         
         $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsMenu/miseajoursubmenu',{id:id_menu,menu_label:menu_label,is_publish:is_publish,parent_menu:parent_menu},function(data){
             
                 $('#modal-sub-menu').on('hidden.bs.modal', function () {
                        
                  });
                 location.reload();
             
         });
        // alert(id_menu);
     });
     
     $('.efase-menu').click(function(){
         var menu_id = $(this).attr("data-idmenuefase");
         
         if(confirm("<?= $message_supr?>")){
             $.get('<?= Yii::app()->baseUrl ?>/index.php/portal/cmsMenu/deletemenu',{id:menu_id},function(data){
                location.reload();
             });
         }
     });
    
    $.fn.extend({
    treed: function (o) {

      var openedClass = 'fa fa-minus';//'glyphicon-minus-sign';
      var closedClass = 'fa fa-plus';//'glyphicon-plus-sign';

      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class=' " + closedClass + "'></i> ");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree1').treed();
$('#menu').treed();

$('#tree2').treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

$('#tree3').treed({openedClass:'glyphicon-chevron-right', closedClass:'glyphicon-chevron-down'});

</script>