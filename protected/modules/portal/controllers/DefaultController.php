<?php

/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

Yii::import('ext.tcpdf.*');



class DefaultController extends PersonsController
{
   public $layout = '//layouts/column_portal';
   public $back_url;
   
   public $message = false;
   public $portal_position;
   public $title = "Portal";
   
   
   
   
   
   
    

  public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','contact','admission','about','preadmission','article','archives','download','articlesdetails'),
				'users'=>array('@'),
			),
			
		);
	}
        
   public function actionAbout(){
       $this->render('about');
   } 
   
   public function actionArticle(){
       $this->render('article');
   } 
   
   public function actionArchives(){
       $this->render('archives');
   } 
   
   public function actionDownload(){
       $this->render('download');
   }
   
   public function actionGallerie(){
       $this->render('gallerie');
   }
   
    public function actionIndex()
	{
                $this->layout='//layouts/column_portal';
                //$this->portal_position = "index";
		$this->render('index');
	}
        
        
 public function  actionAdmission(){
        
       // mailsend("jcpoulard@gmail.com", "sigessystem@slogipam.com", "test brital depi hostmonster epi", "Nap fout teste email en force brital depi hostmonster a!  nap pase");
        $model = new Admission;
        $modelStudentOtherInfo = new StudentOtherInfo;
        $postulant = new Postulant(); 
        $email_school = infoGeneralConfig('school_email_address');
        $email_body = "";
        $email_title = "";
        $school_name = infoGeneralConfig('school_name');
        $school_address = infoGeneralConfig('school_address');
        
        $this->layout='//layouts/column_portal';
        
        if(isset($_POST['btn_save'])){
            if(isset($_POST['Admission'])){
        $model->attributes=$_POST['Admission'];
        if($model->validate()){
            if(isset($_POST['Persons']['birthday'])){
                $date_naissance = $_POST['Persons']['birthday'];
            }else{
                $date_naissance = "N/A";
            }
            
            if(isset($_POST['Admission']['cities'])){
                $city = $_POST['Admission']['cities'];
            }else{
                $city = 0;
            }
            
            if(isset($_POST['Admission']['phone'])){
                $phone = $_POST['Admission']['phone'];
            }else{
                $phone = 'N/A';
            }
            
            if(isset($_POST['Admission']['email'])){
                $email = $_POST['Admission']['email'];
            }else{
                $email = 'N/A';
            }
            
            if(isset($_POST['Admission']['adresse'])){
                $adresse = $_POST['Admission']['adresse'];
            }else{
                $adresse = 'N/A';
            }
            
            if(isset($_POST['Admission']['citizenship'])){
                $citizenship = $_POST['Admission']['citizenship'];
            }else{
                $citizenship = 'N/A';
            }
            
            if(isset($_POST['Admission']['father_full_name'])){
                $father_full_name = $_POST['Admission']['father_full_name'];
            }else{
                $father_full_name = 'N/A';
            }
            
            if(isset($_POST['Admission']['mother_full_name'])){
                $mother_full_name = $_POST['Admission']['mother_full_name'];
            }else{
                $mother_full_name = 'N/A';
            }
            
            if(isset($_POST['StudentOtherInfo']['person_liable'])){
                $person_liable = $_POST['StudentOtherInfo']['person_liable'];
            }else{
                $person_liable = 'N/A';
            }
            
            if(isset($_POST['StudentOtherInfo']['person_liable_phone'])){
                $person_liable_phone = $_POST['StudentOtherInfo']['person_liable_phone'];
            }else{
                $person_liable_phone = 'N/A';
            }
            
            if(isset($_POST['StudentOtherInfo']['previous_school'])){
                $previous_school = $_POST['StudentOtherInfo']['previous_school'];
            }else{
                $previous_school = 'N/A';
            }
            
            if(isset($_POST['Admission']['admission_en'])){
                $admission_en = $_POST['Admission']['admission_en'];
            }else{
                $admission_en = 0;
            }
            
            if(isset($_POST['Admission']['comments'])){
                $comments = $_POST['Admission']['comments'];
            }else{
                $comments = 'N/A';
            }
            
            if(isset($_POST['Admission']['blood_group'])){
                $blood_group = $_POST['Admission']['blood_group'];
            }else{
                $blood_group = 'N/A';
            }
            
            
             // Fais les operations d'enregistrement des postulants 
        $postulant->last_name = $model->last_name; 
        $postulant->first_name  = $model->first_name; 
        $postulant->gender = $model->gender; 
        $postulant->birthday = $date_naissance;
        $postulant->blood_group = $blood_group;
        $postulant->cities = $city; 
        $postulant->phone = $phone; 
        $postulant->email = $email; 
        $postulant->adresse = $adresse; 
        $postulant->citizenship = $citizenship; 
        $postulant->person_liable = $person_liable; 
        $postulant->person_liable_phone = $person_liable_phone; 
        $postulant->previous_school = $previous_school; 
        $postulant->apply_for_level = $admission_en; 
        $curr_acad = currentAcad(); 
        $postulant->academic_year = $curr_acad->id; 
        $postulant->create_by = currentUser(); 
        $postulant->date_created = date('Y-m-d H:i:s');
        $postulant->status = 0; 
        $postulant->is_online = 1;
        $postulant->is_validate = 0;
        // on enregistre dans la table postulant
        if($postulant->save()){
            // Si les elements sont enregistres dans la table postulant il faut enregistrer les champs personalisables 
            // Demarrer enregistrement champs personalisables 
            $modelCustomFieldData = new CustomFieldData();
            $criteria_cf = new CDbCriteria; 
            $criteria_cf->condition = "field_related_to = 'stud'";
            $cfData = CustomField::model()->findAll($criteria_cf);
            foreach($cfData as $cd){
                if(isset($_POST[$cd->field_name])){
                    $customFieldValue = $_POST[$cd->field_name];
                    $modelCustomFieldData->setAttribute('field_link', $cd->id);
                    $modelCustomFieldData->setAttribute('field_data', $customFieldValue);
                    $modelCustomFieldData->setAttribute('object_id', $postulant->id);
                    $modelCustomFieldData->setAttribute('object_type', 'post');
                    $modelCustomFieldData->save();
                    $modelCustomFieldData->unsetAttributes();
                    $modelCustomFieldData = new CustomFieldData;
                }
            }
            
            // envoi un email a l'adresse email de la personne inscrite 
            // L'email envoye sera succinte !
            setlocale(LC_TIME, 'fr_FR.utf8');
            $email_title =   Yii::t('app','Subscription receive for {name}: Do not reply',array('{name}'=>$postulant->fullName));  
            $email_body = infoGeneralConfig('template_email_admission');
            $date_inscription = date("d",strtotime($postulant->date_created)).' '.strftime('%B',strtotime($postulant->date_created)).' '.strftime('%Y',strtotime($postulant->date_created));
            $link_form = "https://slogipam.com".Yii::app()->baseUrl."/index.php/portal/default/admissionform?id=$postulant->id";
            $email_body_final = Yii::t('app',$email_body,
                    array(
                        '{personne_responsable}'=>$postulant->person_liable,
                        '{nom_postulant}'=>$postulant->fullName,
                        '{classe_admission}'=>$postulant->classe,
                        '{date_inscription}'=>$date_inscription,
                        '{school_name}'=>$school_name,
                        '{school_address}'=>$school_address,
                        '{link_form}'=>$link_form,
                        '{admission_number}'=>$postulant->id,
                        
                    ));
            
            
            
        }
           // create new PDF document
	// $pdf = new tcpdf('P', PDF_UNIT, 'legal', true, 'UTF-8', false);
        //Take the school name
        $criteria = new CDbCriteria;
        $criteria->condition='item_name=:item_name';
        $criteria->params=array(':item_name'=>'school_name',);
        $school_name = GeneralConfig::model()->find($criteria)->item_value;
        $criteria->params=array(':item_name'=>'school_email_address',);
        $school_email = GeneralConfig::model()->find($criteria)->item_value;
        $criteria->params=array(':item_name'=>'school_address',);
        $school_address = GeneralConfig::model()->find($criteria)->item_value;
        $criteria->params=array(':item_name'=>'school_phone_number',);
        $school_phone_number = GeneralConfig::model()->find($criteria)->item_value;
     
              
        Yii::app()->user->setFlash(Yii::t('app','Subscription'), Yii::t('app','Thank you for submitting your online subscription.'));
        mailsend($email, $email_school, $email_title, $email_body_final);
        $this->redirect("admissionview?id=$postulant->id");    
            
        
        }
        
            
        } // Fin set formulaire
        
       
            }
            
            if(isset($_POST['btn_cancel'])){
            	$this->redirect(Yii::app()->baseUrl.'/index.php/portal/default/index');
            	}
        
        $this->render('admission',
                array('model'=>$model,
                    'modelStudentOtherInfo'=>$modelStudentOtherInfo,
                    ));
    }
    
    
 public function actionAdmissionform($id){
      $this->renderPartial('make-admission-pdf',array(
                    'id'=>$id,
                ));
 }
 
 public function actionAdmissionview($id){
      $this->render('data-admission-html',array(
                    'id'=>$id,
                ));
 }

   public function actionContact()
  {
          $model = new Contact; 
            //$this->layout = "//layouts/column3";
           $this->layout='//layouts/column_portal';
            $this->portal_position = "contact";
                 $gc = GeneralConfig::model()->findAll();
                $email = null;
                $school_name = null;
                foreach($gc as $c)
                  {
                    if($c->item_name == "school_email_address")
                      {
                        $email = $c->item_value;
                       }
                       
                    if($c->item_name == "school_name")
                       {
                        $school_name = $c->item_value;
                       }
                    
                   }
                   
		if(isset($_POST['Contact']))
		{
			
				 $model->attributes = $_POST['Contact'];
                             if(isset($_POST['result']) && $_POST['result']==$_POST['result_hide']){    
                                 
				 if($model->validate())
					{
						$name='=?UTF-8?B?'.base64_encode($model->name).'?=';
						$subject='=?UTF-8?B?'.base64_encode($model->subject).'?=';
						$headers="From: $name <{$model->email}>\r\n".
							"Reply-To: {$model->email}\r\n".
							"MIME-Version: 1.0\r\n".
							"Content-type: text/plain; charset=UTF-8";
		
						mail($email,$subject,$model->body,$headers);
						Yii::app()->user->setFlash('contact','Merci de nous avoir contacté. Nous vous répondra dès que possible !');
						
		                               
		                   $this->redirect(array('../portal'));
					  }
                             }else{
                                 Yii::app()->user->setFlash('error','SVP valider que tu es un être humain !');
                             }	  
			 // }
		}
	
		$this->render('contact',array('model'=>$model,));
	    
	 
	}
	
	
	        
    public function actionPreadmission(){
        
        $this->render('preadmission');
    }

    public function actionArticledetails($id){
      if(isset($id) && $id!=''){  
       $article = CmsArticle::model()->findByPk($id); 
       if(isset($article)){
            return $this->render('article-details',array('id'=>$id,'article'=>$article));
       }else{
           return $this->render('cms-error'); 
       }
      }else{
          return $this->render('cms-error'); 
      }
    }
    
    public function actionGalleriedetails($album){
        $gallerie = CmsImage::model()->findAllByAttributes(array('album'=>$album)); 
        return $this->render('gallerie-details',array('album'=>$album,'gallerie'=>$gallerie));
    }
    
    public function actionAllevents(){
        return $this->render('all-events');
    }
    
    public function actionAllanouncements(){
        return $this->render('all-anouncements'); 
    }
    
    public function actionBiblio(){
        return $this->render('biblio-opac');
    }
    
    public function actionResultatadmission(){
        return $this->render('resultat-admission'); 
    }
    
    public function actionAdmissioncancel($id){
        $postulant = Postulant::model()->findbyPk($id); 
        if($postulant->delete()){
            Yii::app()->user->setFlash(Yii::t('app','Subscription'), Yii::t('app','Admission form delete with success !'));
            $this->redirect('admission?menu=adm');
        }else{
            
        }
    }
        
        
}