<?php
 /*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
class CmsArticleController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $back_url;
        public $position_id = "main";
        public $part = "pot";
        public $article_menu; 
        

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
            $explode_url= explode("/",substr($_SERVER['REQUEST_URI'],1));
            $controller=$explode_url[3];
           
            $actions=$this->getRulesArray($this->module->name,$controller);
            
            if($this->getModuleName($this->module->name))
                {
		            if($actions!=null)
             			 {     return array(
				              	  	array('allow',  
					                	
					                	'actions'=> array_merge($actions,array('listgallerie','uploadAlbum','listphoto','album','updateAlbum','updateAlbumImage','gallerie')),
		                                  'users'=>array(Yii::app()->user->name),
				                    	),
				              		  array('deny',  
					                 	'users'=>array('*'),
				                    ),
			                );
             			 }
             			 else
             			  return array(array('deny', 'users'=>array('*')),);
                }
                else
                {
                    return array(array('deny', 'users'=>array('*')),);
                }
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CmsArticle;

		// Uncomment the following line if AJAX validation is needed
		
                $_SESSION['KCFINDER']['disabled'] = false; // enables the file browser in the admin
                $_SESSION['KCFINDER']['uploadURL'] = Yii::app()->baseUrl."/cms_files/"; // URL for the uploads folder
                $_SESSION['KCFINDER']['uploadDir'] = Yii::app()->basePath."/../cms_files/"; // path to the uploads folder
               
                    $this->position_id = 'main';//$_POST['CmsArticle']['set_position'];
               
                
                 if(isset($_POST['CmsArticle']['article_menu'])){
                    $this->article_menu = $_POST['CmsArticle']['article_menu'];
                    $featured_image = $_POST['CmsArticle']['featured_image'];
                }
                
                
		if(isset($_POST['CmsArticle']))
		{
			$model->attributes=$_POST['CmsArticle'];
                        $model->setAttribute('is_publish', 1);
                        $model->setAttribute('date_create',date('Y-m-d H:m:s'));
                        $model->setAttribute('create_by', currentUser());
                        $model->setAttribute('article_menu', $this->article_menu);
                        $model->setAttribute('set_position','main');
                        $model->setAttribute('featured_image',$featured_image);
                        if(isset($_POST['create']))
                            if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		  $this->position_id = 'main';//_POST['CmsArticle']['set_position'];

                     if(isset($_POST['CmsArticle']['article_menu'])){
                    $this->article_menu = $_POST['CmsArticle']['article_menu'];
                    $featured_image = $_POST['CmsArticle']['featured_image'];
                }
                

		if(isset($_POST['CmsArticle']))
		{
                   
			$model->setAttribute('last_update', date('Y-m-d'));
                        $model->setAttribute('set_position','main');
                        $model->attributes=$_POST['CmsArticle'];
                        $model->setAttribute('article_menu', $this->article_menu);
                        $model->setAttribute('featured_image',$featured_image);
                         if(isset($_POST['update'])){
                            if($model->save())
				$this->redirect(array('index'));
                         }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	
        }

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}
        
        public function actionDeleteCarrousel($id){
            $model = CmsImage::model()->findByPk($id); 
            $filename = str_replace('protected/','',Yii::app()->basePath.'/cms_files/images/banners/'.$model->nom_image);
            if(is_file($filename)){
                if(unlink($filename)){
                    $model->delete(); 
                    if(isset($_GET['part']) && $_GET['part']=="gal"){
                      $this->redirect(array('gallerie','part'=>'gal'));  
                    }else{
                     $this->redirect(array('carrousel','part'=>'car'));
                    }
                }
            }else {
                $model->delete();
                if(isset($_GET['part']) && $_GET['part']=="gal"){
                    $this->redirect(array('gallerie','part'=>'gal'));
                }else{
                    $this->redirect(array('carrousel','part'=>'car'));
                }
            }
           
        }

	
	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
            if (isset($_GET['pageSize'])) {
                        Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
                        unset($_GET['pageSize']);
                        }
		$model=new CmsArticle('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CmsArticle']))
			$model->attributes=$_GET['CmsArticle'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CmsArticle the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CmsArticle::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CmsArticle $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cms-article-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionCarrousel(){
            $this->part = "car";
            return $this->render('carrousel'); 
        }
        
        public function actionGallerie(){
            $this->part = "gal";
            return $this->render('gallerie'); 
        }
        
        public function actionAlbum(){
            $this->part = "alb";
            $model=new CmsAlbumCat('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CmsAlbumCat']))
			$model->attributes=$_GET['CmsAlbumCat'];

		$this->render('album',array(
			'model'=>$model,
		));
        }
        
         public function actionUpdateAlbum()
		{
                        $es = new EditableSaver('CmsAlbumCat');
                        $es->onBeforeUpdate = function($event) {
                        $event->sender->setAttribute('update_date', date('Y-m-d H:i:s'));
                        $event->sender->setAttribute('update_by', currentUser());
                        };
                        
			$es->update();
		}
        
        
        
        
        
        public function actionLisimaj(){
            $model = new CmsImage('search'); 
            $cms_image = CmsImage::model()->findAllBySql("SELECT * FROM cms_image WHERE type_image = 'carrousel' AND is_publish = 1 ORDER BY id DESC LIMIT 10");
            return $this->renderPartial('lisimaj',array('model'=>$model,'cms_image'=>$cms_image)); 
        }
        
        public function actionListphoto(){
            $model = new CmsImage('search'); 
            $cms_image = CmsImage::model()->findAllBySql("SELECT * FROM cms_image WHERE type_image <> 'carrousel' AND is_publish = 1 ORDER BY id DESC");
           // $all_gallerie = CmsImage::model()->findAllBySql("SELECT DISTINCT album_name FROM cms_image WHERE type_image <> 'carrousel' AND is_publish = 1 ORDER BY album_name ASC");
            $all_album = CmsAlbumCat::model()->findAllBySql("SELECT id, album_name FROM cms_album_cat ORDER BY album_name DESC");
            return $this->renderPartial('listphoto',array('model'=>$model,'cms_image'=>$cms_image,'all_album'=>$all_album)); 
        }
        
        public function actionUpdateCarrousel($id, $label){
            $model = CmsImage::model()->findByPk($id); 
            $model->label_image = $label; 
            $model->save(); 
           }
           
         public function actionUpdateAlbumImage($id, $album_id){
            $model = CmsImage::model()->findByPk($id); 
            $model->album = $album_id; 
            $model->save(); 
           }  
        
        
        //for ajaxrequest
    public function actionUploadLogo(){
                $model = new CmsImage();
                
                 if (!empty($_FILES)) {
                     $filename = Yii::app()->basePath.'/../cms_files/images/banners/'.str_replace(' ','_',strtolower($_FILES['upload']['name']));
                     $temp_name = $_FILES['upload']['tmp_name'];
                     if(move_uploaded_file($temp_name, $filename)){
                         $extension = strtolower($this->getExtension($_FILES['upload']['name']));
                         $model->label_image = str_replace($extension,'',strtolower($_FILES['upload']['name']));// json_encode($_FILES['upload']); //
                         $model->type_image = 'carrousel';
                         $model->nom_image = str_replace(' ','_',strtolower($_FILES['upload']['name'])); 
                         $model->is_publish = 1;
                         $model->save(); 
                     }
                 }
                
        }
        
  public function actionUploadAlbum(){
                $model = new CmsImage();
                
                 if (!empty($_FILES)) {
                     $filename = Yii::app()->basePath.'/../cms_files/images/banners/'.str_replace(' ','_',strtolower($_FILES['upload']['name']));
                     $temp_name = $_FILES['upload']['tmp_name'];
                     if(move_uploaded_file($temp_name, $filename)){
                         $extension = strtolower($this->getExtension($_FILES['upload']['name']));
                         $model->label_image = str_replace($extension,'',strtolower($_FILES['upload']['name']));// json_encode($_FILES['upload']); //
                         $model->type_image = 'album';
                         //$model->album_name = "Premier album";
                         $model->nom_image = str_replace(' ','_',strtolower($_FILES['upload']['name'])); 
                         $model->is_publish = 1;
                         $model->save(); 
                     }
                 }
                
        }      
        
    public function actionListgallerie(){
        
        return $this->renderPartial('list_gallerie');
    }    
        
        
        public function getExtension($str) {
            $i = strrpos($str,".");
            if (!$i) { return ""; }
            $l = strlen($str) - $i;
            $ext = substr($str,$i+1,$l);
            return $ext;
        }
        
    
    public function actionUpdateArticle()
		{
			$es = new EditableSaver('CmsArticle');
                        $es->onBeforeUpdate = function($event) {
                        $event->sender->setAttribute('last_update', date('Y-m-d H:i:s'));
                        
                        };
                        
			$es->update();
		}
        
        
}
