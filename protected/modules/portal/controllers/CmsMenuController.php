<?php

class CmsMenuController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $part = "men";
        public $back_url;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		 $explode_url= explode("/",substr($_SERVER['REQUEST_URI'], 1));
            $controller=$explode_url[3];
            
            $actions=$this->getRulesArray($this->module->name,$controller);
              
            if($this->getModuleName($this->module->name))
                {
		            if($actions!=null)
             			 {     return array(
				              	  	array('allow',  
					                	
					                	'actions'=>array_merge($actions,array('create','delete','modifyemenu','miseajourmainmenu','modifyesubmenu','miseajoursubmenu','deletemenu')),
		                                  'users'=>array(Yii::app()->user->name),
				                    	),
				              		  array('deny',  
					                 	'users'=>array('*'),
				                    ),
			                );
             			 }
             			 else
             			  return array(array('deny', 'users'=>array('*')),);
                }
                else
                {
                    return array(array('deny', 'users'=>array('*')),);
                }



	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CmsMenu;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CmsMenu']))
		{
			$model->attributes= $_POST['CmsMenu'];
                        $parent_menu = $_POST['CmsMenu']['parent_menu'];
                        $model->setAttribute('is_publish', 1);
                        $model->setAttribute('is_parent_menu', 0);
                        $model->setAttribute('parent_menu', $parent_menu);
			if($model->save())
				$this->redirect(array('admin'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CmsMenu']))
		{
			$model->attributes=$_POST['CmsMenu'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
        
        public function actionModifyemenu($id){
            $model=$this->loadModel($id);
            return $this->renderPartial('update-menu',array('model'=>$model));
        }
        
        public function actionModifyesubmenu($id){
            $model=$this->loadModel($id);
            return $this->renderPartial('update-sub-menu',array('model'=>$model));
        }
        
        public function actionMiseajourmainmenu($id,$menu_label, $is_publish){
            $model = $this->loadModel($id);
            $model->menu_label = $menu_label; 
            $model->is_publish = $is_publish; 
            
            if($model->save()){
                return 1; 
            }else{
                return 0; 
            } 
        }
        
        public function actionMiseajoursubmenu($id,$menu_label, $is_publish, $parent_menu){
            $model = $this->loadModel($id);
            $model->menu_label = $menu_label; 
            $model->is_publish = $is_publish; 
            $model->parent_menu = $parent_menu;
           // $model->is_parent_menu = 0;
            if($model->save()){
                return 1; 
            }else{
                return 0; 
            } 
        }
         
	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
               $at_least_one_article = CmsArticle::model()->findAllByAttributes(array('article_menu'=>$id));
               $article_1 = array(); 
               $k = 0; 
               foreach($at_least_one_article as $a){
                  $article_1[$k] = $a->article_menu;  
               }
               if(empty($article_1)){
		$this->loadModel($id)->delete();
                if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin','part'=>'men'));
               }else{
                   if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin','part'=>'men'));
               }

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin','part'=>'men'));
	}
        
        public function actionDeletemenu($id){
            $this->loadModel($id)->delete();
            
        }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$dataProvider=new CActiveDataProvider('CmsMenu');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
            if (isset($_GET['pageSize'])) {
		    Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
		    unset($_GET['pageSize']);
			}
		$model = new CmsMenu('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['CmsMenu']))
			$model->attributes=$_GET['CmsMenu'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CmsMenu the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=CmsMenu::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CmsMenu $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cms-menu-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
        
        public function actionUpdateMenu()
		{
			$es = new EditableSaver('CmsMenu');
                        $es->onBeforeUpdate = function($event) {
                        $event->sender->setAttribute('date_update', date('Y-m-d H:i:s'));
                        $event->sender->setAttribute('update_by', currentUser());
                        };
                        
			$es->update();
		}
                
      public function actionGetPublish(){
          
          return array(
              0=>Yii::t('app','Yes'),
              1=>Yii::t('app','No'),
          );
          
      }
      
      
      
}
