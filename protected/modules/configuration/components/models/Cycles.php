<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


	
/** 
 * This is the model class for table "cycles".
 *
 * The followings are the available columns in table 'cycles':
 * @property integer $id
 * @property string $cycle_description
 * @property integer $average_base
 * @property integer $section
 * @property integer $academic_year
 *
 * The followings are the available model relations:
 * @property SectionHasCycle[] $sectionHasCycles
 */
class Cycles extends BaseCycles
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Cycles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	
	/*	*/ 
  public function rules()
	{
              
              return array_merge(
		    	parent::rules(), array(
                            array('cycle_description', 'unique'),
							array('average_base,academic_year', 'numerical', 'integerOnly'=>true),
                           ));
	}



	
	
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('app','ID'),
			'cycle_description' => Yii::t('app','Cycle Description'),
			'average_base'=>Yii::t('app','Average Base'),
			//'section'=>Yii::t('app','Section'),
			'academic_year'=>Yii::t('app','Academic Year'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cycle_description',$this->cycle_description,true);
		$criteria->compare('average_base',$this->average_base,true);
		//$criteria->compare('section',$this->section,true);
		$criteria->compare('academic_year',$this->academic_year,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			
			'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                        ),
		));
	}
	
	
	public function searchByAcad($acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->condition = 'academic_year=:acad';
		$criteria->params = array( ':acad'=>$acad);
		
		$criteria->compare('id',$this->id);
		$criteria->compare('cycle_description',$this->cycle_description,true);
		$criteria->compare('average_base',$this->average_base,true);
		//$criteria->compare('section',$this->section,true);
		$criteria->compare('academic_year',$this->academic_year,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			
			'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                        ),
		));
	}
	
/*	
    public function getSection(){
               $criteria = new CDbCriteria;
               $criteria->condition='id=:item_name';
               $criteria->params=array(':item_name'=>$this->section);
               $method = Sections::model()->find($criteria);
               if($method!= NULL)
                 return $method->section_name;
               else 
                 {
                     return null;
                 }
           }	
	
 * 
 */
	
}