<?php 
/*
 * © 2019 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */





/* @var $this AppreciationsGridController */
/* @var $model AppreciationsGrid */

$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
	

 $acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

 


   $template = '';


$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#reportcard-observation-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<div id="dash">
          
          <div class="span3"><h2>
              <?php echo Yii::t('app','Appreciations grid'); ?>
              
          </h2> </div>
     
		   <div class="span3">
             
<?php 
     
     if(!isAchiveMode($acad_sess))
        { 
         if($acad == $current_acad_id)
	  {
             $template = '{delete}';  
        
   ?>


 <div class="span4">
              <?php

                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/configuration/appreciationsGrid/create')); 
               ?>
   </div>

 <?php
        
        if($model->search($acad_sess)->getData()!=null)
          {
   ?>
              <div class="span4">
              <?php

                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Update').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/configuration/appreciationsGrid/update')); 
               ?>
              </div>
        
   <?php    }   
        
        }
        
        }
      
      ?>       
   
   <div class="span4">
                  <?php

                 $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/configuration/appreciationsGrid/index')); 
               ?>
  </div>  


  </div>

</div>



<div style="clear:both"></div>


<br/>


    <?php
    echo $this->renderPartial('//layouts/navBaseConfiguration',NULL,true);	
    ?>



<div class="b_m">


<div class="grid-view">


<?php 

$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);


$gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'appreciations-grid-grid',
	'dataProvider'=>$model->search($acad_sess),
	//'filter'=>$model,
	'showTableOnEmpty'=>true,
	 'mergeColumns'=>array('section0.section_name'),
	
	'columns'=>array(
		//'id',
		'section0.section_name',
		'start_range',
		'end_range',
		'comment',
                'short_text',
		//'academic_year',
		//'create_by',
		/*
		'update_by',
		'create_date',
		'update_date',
		*/
		array(
			'class'=>'CButtonColumn',
			  'template'=>$template,
                         'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/configuration/appreciationsGrid/update?id=$data->id")',
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                      'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100,100000=>Yii::t('app','all')),array(
                             'onchange'=>"$.fn.yiiGridView.update('appreciations-grid-grid',{ data:{pageSize: $(this).val() }})",
                             )),
		),
	),
)); 



    // Export to CSV 
  $content=$model->search($acad_sess)->getData();
 if((isset($content))&&($content!=null))
   $this->renderExportGridButton($gridWidget, '<i class="fa fa-arrow-right"></i>'.Yii::t('app',' CSV'),array('class'=>'btn-info btn'));




?>



