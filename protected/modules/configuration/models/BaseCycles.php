<?php

/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */



/** 
 * This is the model class for table "cycles".
 *
 * The followings are the available columns in table 'cycles':
 * @property integer $id
 * @property string $cycle_description
 * @property integer $average_base
 * @property integer $section
 * @property integer $academic_year
 *
 * The followings are the available model relations:
 * @property SectionHasCycle[] $sectionHasCycles
 */
class BaseCycles extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseCycles the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'cycles';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('cycle_description', 'required'),
			array('cycle_description', 'length', 'max'=>100),
			array('average_base,academic_year', 'numerical', 'integerOnly'=>true),
			
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, cycle_description,average_base,academic_year', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'section0' => array(self::BELONGS_TO, 'Sections', 'section'),
			//'sectionHasCycles' => array(self::HAS_MANY, 'SectionHasCycle', 'cycle'),
		);
	}

	

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('cycle_description',$this->cycle_description,true);
		$criteria->compare('average_base',$this->average_base,true);
		//$criteria->compare('section',$this->section,true);
        $criteria->compare('academic_year',$this->academic_year,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			
			'pagination'=>array(
                        'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
                        ),
		));
	}
}