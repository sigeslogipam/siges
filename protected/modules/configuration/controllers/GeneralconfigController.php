<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */



class GeneralconfigController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	private $_model;
	
	public $back_url='';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
               
		  $explode_url= explode("/",substr($_SERVER['REQUEST_URI'], 1));
                  $controller=$explode_url[3];
            
                  $actions=$this->getRulesArray($this->module->name,$controller);
              
            if($this->getModuleName($this->module->name))
                {
		            if($actions!=null)
             			 {     return array(
				              	  	array('allow',  
					                	
					                	'actions'=> $actions,
		                                  'users'=>array(Yii::app()->user->name),
				                    	),
				              		  array('deny',  
					                 	'users'=>array('*'),
				                    ),
			                );
             			 }
             			 else
             			  return array(array('deny', 'users'=>array('*')),);
                }
                else
                {
                    return array(array('deny', 'users'=>array('*')),);
                }


            
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	

	public function actionView()
	{
		$this->render('view',array(
			'model'=>$this->loadModel(),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new GeneralConfig;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['GeneralConfig']))
		{
			$model->attributes=$_POST['GeneralConfig'];
			
			 if(isset($_POST['create']))
              {       
				$model->setAttribute('date_create',date('Y-m-d'));
				if($model->save())
					$this->redirect(array('view','id'=>$model->id));
					
              }
              
               if(isset($_POST['cancel']))
                          {
                              $this->redirect(Yii::app()->request->urlReferrer);
                          }
                          
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */

	
	
	public function actionUpdate()
	{   
             if (isset($_GET['pageSize'])) {
                    Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
                    unset($_GET['pageSize']);
                }
          
            
         if(!isset($_GET['all']))
		  {  
                $model=$this->loadModel();
              $this->performAjaxValidation($model);
		  
		       if(isset($_POST['update']))
				{   //on vient de presser le bouton
								 //reccuperer le ligne modifiee
									 
										   if(isset($_POST['GeneralConfig'])){
											
											$model->attributes=$_POST['GeneralConfig'];
												
										   $model->setAttribute('date_update',date('Y-m-d'));
                                                                                                                                                                                                                                                                		}                   
                                                                                                                                                                                                                                                                		               $pass=true;                  
                                                                                                                                                                                                                                                                 
                          //si sommaire=1,anpeche period_weight!=1   visevesa                                                                                                                                                                                                               
                                if($model->item_name=='display_period_summary')//sommaire
                                  {
                                  	   //Extract use_period_weight
						                $use_period_weight = infoGeneralConfig('use_period_weight');
						                if(($model->item_value==1)&&($use_period_weight==1))
				                           { $pass=false;
				                             Yii::app()->user->setFlash(Yii::t('app','Warning'), Yii::t('app','Sorry, period weight is set to 1. Set it to 0 if you really want to display period summary.') );
				                           }
                                  	}
                                 elseif($model->item_name=='use_period_weight')//period_weight
                                    {
                                    	//Extract display_period_summary
						                $display_period_summary = infoGeneralConfig('display_period_summary');
						                if(($model->item_value==1)&&($display_period_summary==1))
				                           { $pass=false;
				                             Yii::app()->user->setFlash(Yii::t('app','Warning'), Yii::t('app','Sorry, period summary is set to 1. Set it to 0 if you really want to use period weight.') );
				                           }
						                
				                
                                      }
                                                                                                                                                                                                                                                     
								
						if($pass==true)
						  {				  
						  	            if($model->save())
											 { 											   
											   
											   //CURRENCY
											 //Extract devise name and symbol 
											  $criteria_ = new CDbCriteria;
										   		$criteria_->condition='item_name=:item_name';
										   		$criteria_->params=array(':item_name'=>'currency_name_symbol',);
										   		$currency_name_symbol = GeneralConfig::model()->find($criteria_)->item_value;
											    
											    $explode_currency_name_symbol= explode("/",substr($currency_name_symbol, 0));
											    
											    $currency_name = $explode_currency_name_symbol[0];
											    $currency_symbol = $explode_currency_name_symbol[1];
											    
											     unset(Yii::app()->session['currencyName']);
											      unset(Yii::app()->session['currencySymbol']);
											      
											    Yii::app()->session['currencyName']=$currency_name;
											    Yii::app()->session['currencySymbol']=$currency_symbol;           
					        

											   
											   
											   $this->redirect(array('index'));
											 }
											 
						     }
								 
								
					
					
						
				}
				
			if(isset($_POST['cancel']))
                          {
                              $this->redirect(Yii::app()->request->urlReferrer);
                          }
                          
                          
		  }
		elseif($_GET['all']==1)
		 {	   $model=new GeneralConfig;
			
			
			
					 
				if(isset($_POST['update']))
					{   //on vient de presser le bouton
									 //reccuperer le ligne modifiee
										 $grade=0;
								   foreach($_POST['id_config'] as $id)
									{   	   
											   if(isset($_POST['generalconfig'][$id]))
													$grade=$_POST['generalconfig'][$id];
												
											   
												$model=GeneralConfig::model()->findbyPk($id);
											   
											   $model->setAttribute('date_update',date('Y-m-d'));
											   $model->setAttribute('item_value',$grade);
											   
											   $pass=true;                  
                                                                                                                                                                                                                                                                 
                               //si sommaire=1,anpeche period_weight!=1   visevesa                                                                                                                                                                                                               
                                if($model->item_name=='display_period_summary')//sommaire
                                  {
                                  	   //Extract use_period_weight
						                $use_period_weight = infoGeneralConfig('use_period_weight');
						                if(($model->item_value==1)&&($use_period_weight==1))
				                           { $pass=false;
				                             Yii::app()->user->setFlash(Yii::t('app','Warning'), Yii::t('app','Sorry, period weight is set to 1. Set it to 0 if you really want to display period summary.') );
				                           }
                                  	}
                                 elseif($model->item_name=='use_period_weight')//period_weight
                                    {
                                    	//Extract display_period_summary
						                $display_period_summary = infoGeneralConfig('display_period_summary');
						                if(($model->item_value==1)&&($display_period_summary==1))
				                           { $pass=false;
				                             Yii::app()->user->setFlash(Yii::t('app','Warning'), Yii::t('app','Sorry, period summary is set to 1. Set it to 0 if you really want to use period weight.') );
				                           }
						                
				                
                                      }

										if($pass==true)
										  {
										  	  if($model->save())
												 {  
												   $model->unSetAttributes();
												   $model= new GeneralConfig;
												   
												   
												 }
												 
										  }
									 
									}
						
						 //CURRENCY
											 //Extract devise name and symbol 
											  $criteria_ = new CDbCriteria;
										   		$criteria_->condition='item_name=:item_name';
										   		$criteria_->params=array(':item_name'=>'currency_name_symbol',);
										   		$currency_name_symbol = GeneralConfig::model()->find($criteria_)->item_value;
											    
											    $explode_currency_name_symbol= explode("/",substr($currency_name_symbol, 0));
											    
											    $currency_name = $explode_currency_name_symbol[0];
											    $currency_symbol = $explode_currency_name_symbol[1];
											    
											     unset(Yii::app()->session['currencyName']);
											      unset(Yii::app()->session['currencySymbol']);
											      
											    Yii::app()->session['currencyName']=$currency_name;
											    Yii::app()->session['currencySymbol']=$currency_symbol;           
					        

						
						 $this->redirect (array('index'));
							
					}
					
					
				if(isset($_POST['cancel']))
                          {
                              $this->redirect(Yii::app()->request->urlReferrer);
                          }
                          
                                       
		  }
		

		$this->render('update',array(
			'model'=>$model,
		));
	   
	}
	

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	
	
	public function actionDelete()
	{
		
		
		try {
   			 $this->loadModel()->delete();
   			 // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			  if(!isset($_GET['ajax']))
				$this->redirect( array('index'));
			
			} catch (CDbException $e) {
			    if($e->errorInfo[1] == 1451) {
			        
			        header($_SERVER["SERVER_PROTOCOL"]." 500 Relation Restriction");
			        echo Yii::t('app',"\n\n There are dependant elements, you have to delete them first.\n\n");
			    } else {
			        throw $e;
			    }
			}


	}

//for ajaxrequest
        /*
 public function actionUploadLogo(){
     $model = new GeneralConfig; 
     
     $path = Yii::app()->basePath.'/../css/images/';  //"../userfiles/orig/";
     $userpath="../".$_REQUEST['userpath']."/";



$valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");

if(isset($_POST['logoUpload'])){
  $name = $_FILES['image']['name'];
  $size = $_FILES['image']['size'];

  if(strlen($name)){
    $ext = $this->getExtension($name);

    if(in_array($ext,$valid_formats)){

      if($size<(2048*2048)){
        $time1=time();
        $actual_image_name =$time1."_".str_replace(" ", "_", $name);
        $tmp = $_FILES['image']['tmp_name'];

        if(move_uploaded_file($tmp, $path.$actual_image_name)){
          $image=$actual_image_name;
         // --------resize image-----------
          $size = 320; // the imageheight
          $filedir = Yii::app()->basePath.'/../css/images/'; //'../userfiles/orig/'; // the directory for the original image
          $thumbdir = $userpath; // the directory for the resized image
          $prefix = $time1.'_'; // the prefix to be added to the original name
          $maxfile = '20000000';
          $mode = '0666';
          $userfile_name =str_replace(" ", "", $_FILES['image']['name']);
          $userfile_tmp = str_replace(" ", "", $_FILES['image']['tmp_name']);
          $userfile_size =$_FILES['image']['size'];
          $userfile_type = $_FILES['image']['type'];

          if (isset($_FILES['image']['name'])){
            $prod_img = $filedir.$actual_image_name;
            $prod_img_thumb = $thumbdir.$prefix.$userfile_name;
            move_uploaded_file($userfile_tmp, $prod_img);
            chmod ($prod_img, octdec($mode));
            $sizes = getimagesize($prod_img);
            $aspect_ratio = $sizes[1]/$sizes[0];

            if ($sizes[1] <= $size){
              $new_width = $sizes[0];
              $new_height = $sizes[1];
            }else{
              $new_height = $size;
              $new_width = abs($new_height/$aspect_ratio);
            }
            $destimg=ImageCreateTrueColor($new_width,$new_height)
            or die('Problem In Creating image');

            switch($ext){
              case "jpg":
              case "jpeg":
              case "JPG":
              case "JPEG":
                $srcimg=ImageCreateFromJPEG($prod_img)or die('Problem In opening Source Image');
                break;
              case "PNG":
              case "png":
                $srcimg = imageCreateFromPng($prod_img)or die('Problem In opening Source Image');
                imagealphablending($destimg, false);
                $colorTransparent = imagecolorallocatealpha($destimg, 0, 0, 0, 0x7fff0000);
                imagefill($destimg, 0, 0, $colorTransparent);
                imagesavealpha($destimg, true);

                break;
              case "BMP":
              case "bmp":
                $srcimg = imageCreateFromBmp($prod_img)or die('Problem In opening Source Image');
                break;
              case "GIF":
              case "gif":
                $srcimg = imageCreateFromGif($prod_img)or die('Problem In opening Source Image');
                break;
              default:
                $srcimg=ImageCreateFromJPEG($prod_img)or die('Problem In opening Source Image');
            }

            if(function_exists('imagecopyresampled')){
              imagecopyresampled($destimg,$srcimg,0,0,0,0,$new_width,$new_height,imagesX($srcimg),imagesY($srcimg))
              or die('Problem In resizing');
            }else{
              Imagecopyresized($destimg,$srcimg,0,0,0,0,$new_width,$new_height,imagesX($srcimg),imagesY($srcimg))
              or die('Problem In resizing');
            }



            // Saving an image
            switch(strtolower($ext)){
              case "jpg":
              case "jpeg":
                ImageJPEG($destimg,$prod_img_thumb,90) or die('Problem In saving');
                break;

              case "png":
                imagepng($destimg,$prod_img_thumb) or die('Problem In saving');
                break;

              case "bmp":
                imagewbmp($destimg, $prod_img_thumb)or die('Problem In saving');
                break;

              case "gif":
                imagegif($destimg,$prod_img_thumb) or die('Problem In saving');
                break;

              default:
                // if image format is unknown, and you whant save it as jpeg, maybe you should change file extension
                imagejpeg($destimg,$prod_img_thumb,90) or die('Problem In saving');
            }



          }
          unlink($prod_img);
          echo "<img src='".$prod_img_thumb."'>";
        }else{
          echo "Fail upload folder with read access.";
        }
      }else{
        echo "Image file size max 3 MB";
      }

    }else{
      echo "Invalid file format..";
    }
  }else{
    echo "Please select image..!";
  }

  exit;
}
     
     
     $mesage=Yii::t('app','Logo upload successfully');
     Yii::app()->user->setFlash(Yii::t('app','UploadLogo'), $mesage);
     
     if(Yii::app()->request->isAjaxRequest) 
            { 
                   $this->renderPartial('uploadLogo',array(
                    'model'=>$model),false,true);
                }
			else
			  { 
			   					
                            $this->render('index',array(
                                              'model'=>$model,
                                                    ));

			 }
 }       
   */     
        
        
    public function actionUploadLogo(){
                $model = new GeneralConfig; 
                 if(isset($_POST['logoUpload']))
		    {
		    									
		    	  $fileName =$_FILES["image"]["name"];
	                  $uploadedfile = $_FILES['image']['tmp_name'];
		   	

		   	     
			         //saving logo
							if ($fileName)  // check if uploaded file is set or not
							  {   
							      
								  //check image extension
								    
									$filename = stripslashes($_FILES['image']['name']);
 	
									$extension = $this->getExtension($filename);
									$extension = strtolower($extension);
									
									//only .png allowed
									if ($extension != "png")
									 {
		
										$mesage=Yii::t('app','Load a png logo extension.');
										Yii::app()->user->setFlash(Yii::t('app','UploadLogo'), $mesage);
									 }
									else
									 {   //checking the size
									       $size=filesize($_FILES['image']['tmp_name']);


											if ($size > 400*1024)
											{
												$mesage=Yii::t('app','You have exceeded the size limit.');
												Yii::app()->user->setFlash(Yii::t('app','UploadLogo'), $mesage);
											}
											
											 if($extension=="png")
												{
												$uploadedfile = $_FILES['image']['tmp_name'];
												$src = imagecreatefrompng($uploadedfile);

												}
												
												list($width,$height)=getimagesize($uploadedfile);


												$newwidth=140; //105;//60;
												$newheight=($height/$width)*$newwidth;
												$tmp=imagecreatetruecolor($newwidth,$newheight);


												$newwidth1=34;//25;
												$newheight1=($height/$width)*$newwidth1;
												$tmp1=imagecreatetruecolor($newwidth1,$newheight1);

												imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

												imagecopyresampled($tmp1,$src,0,0,0,0,$newwidth1,$newheight1,$width,$height);


												$filename = Yii::app()->basePath.'/../css/images/school_logo.png';

												// Pour conserver la transparence de l'image en PNG
                                                                                                  imagealphablending($src,false);
                                                                                                  imagesavealpha($src,true);
                                                                                                  imagecolortransparent($src);
												imagepng($src,$filename,0);
                                                                                                
												

												imagedestroy($src);
												imagedestroy($tmp);
												imagedestroy($tmp1);
                                        
                                        $mesage=Yii::t('app','Logo upload successfully');
                                        Yii::app()->user->setFlash(Yii::t('app','UploadLogo'), $mesage);
									 
									 }
							      
								    
							  }
							else
							  {
							  	
							  	$mesage=Yii::t('app','No file to upload, retry later.');
							  	Yii::app()->user->setFlash(Yii::t('app','UploadLogo'), $mesage);
							  	}      
			                
			                
		             }
                
                
	    	
                    
          
           if(Yii::app()->request->isAjaxRequest) 
		     { 
			   $this->renderPartial('uploadLogo',array(
					'model'=>$model),false,true);
			 }
			else
			  { 
			   					
					$this->render('index',array(
									'model'=>$model,
								));

			 }

            
        }
  
  
  	
    public function getExtension($str) {
         $i = strrpos($str,".");
         if (!$i) { return ""; }
         $l = strlen($str) - $i;
         $ext = substr($str,$i+1,$l);
         return $ext;
    }
	
	      
	/**
	 * Lists all models.
	 */

	
	public function actionIndex()
	{
		
            if (isset($_GET['pageSize'])) {
            Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
            unset($_GET['pageSize']);
            }
            
            
            
            $model=new GeneralConfig('newsearch');
            $model->unsetAttributes();
            
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GeneralConfig']))
		  $model->attributes=$_GET['GeneralConfig'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		
             
                $model=new GeneralConfig('search');
             
                
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['GeneralConfig']))
			$model->attributes=$_GET['GeneralConfig'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return GeneralConfig the loaded model
	 * @throws CHttpException
	 */
	
	
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=GeneralConfig::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
		}
		return $this->_model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param GeneralConfig $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='general-config-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
