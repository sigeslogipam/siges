<?php 
/*
 * © 2018 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * This is the model class for table "idcard".
 *
 * The followings are the available columns in table 'idcard':
 * @property integer $id
 * @property integer $person_id
 * @property string $prenom
 * @property string $nom
 * @property string $sexe
 * @property string $image_name
 * @property string $date_ajout
 * @property integer $is_print
 * @property string $date_print
 *
 * The followings are the available model relations:
 * @property Persons $person
 */
class Idcard extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Idcard the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'idcard';
	}
        
        public $category_;

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('person_id, prenom, nom, sexe, image_name, date_ajout', 'required'),
			array('person_id, is_print', 'numerical', 'integerOnly'=>true),
			array('prenom, nom, sexe, image_name', 'length', 'max'=>64),
			array('date_print', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, person_id, prenom, nom, sexe, image_name, date_ajout, is_print, date_print', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'person' => array(self::BELONGS_TO, 'Persons', 'person_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'person_id' => 'Person',
			'prenom' => 'Prenom',
			'nom' => 'Nom',
			'sexe' => 'Sexe',
			'image_name' => 'Image Name',
			'date_ajout' => 'Date Ajout',
			'is_print' => 'Is Print',
			'date_print' => 'Date Print',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('prenom',$this->prenom,true);
		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('sexe',$this->sexe,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('date_ajout',$this->date_ajout,true);
		$criteria->compare('is_print',$this->is_print);
		$criteria->compare('date_print',$this->date_print,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
    			'pageSize'=> 10000000,
			),
			
			'criteria'=>$criteria,
		));
	}
	

 public function searchAllStudents()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
                
                $criteria->alias='i';
                
                $criteria->join='inner join persons p on(p.id = i.person_id) ';
                
                $criteria->condition = 'p.is_student=1';

		$criteria->compare('i.id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('prenom',$this->prenom,true);
		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('sexe',$this->sexe,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('date_ajout',$this->date_ajout,true);
		$criteria->compare('is_print',$this->is_print);
		$criteria->compare('date_print',$this->date_print,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
    			'pageSize'=> 10000000,
			),
			
			'criteria'=>$criteria,
		));
	}
	
  
        
  public function searchAllTeachers()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                
             $acad_sess = acad_sess();
             $acad=Yii::app()->session['currentId_academic_year']; 

		$criteria=new CDbCriteria;
                
                $criteria->alias='i';
                
                $criteria->join='inner join persons p on(p.id = i.person_id) ';
                
                $criteria->condition = 'p.is_student=0 AND ( p.id in(select teacher from courses where academic_period='.$acad_sess.') AND p.id not in(select persons_id from persons_has_titles where academic_year='.$acad_sess.') )';

		$criteria->compare('i.id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('prenom',$this->prenom,true);
		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('sexe',$this->sexe,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('date_ajout',$this->date_ajout,true);
		$criteria->compare('is_print',$this->is_print);
		$criteria->compare('date_print',$this->date_print,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
    			'pageSize'=> 10000000,
			),
			
			'criteria'=>$criteria,
		));
	}
	
        
        
  public function searchAllEmployees()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
                
             $acad_sess = acad_sess();
             $acad=Yii::app()->session['currentId_academic_year']; 

		$criteria=new CDbCriteria;
                
                $criteria->alias='i';
                
                $criteria->join='inner join persons p on(p.id = i.person_id) ';
                
                $criteria->condition = 'p.is_student=0 AND ( p.id in(select persons_id from persons_has_titles where academic_year='.$acad_sess.') )';

		$criteria->compare('i.id',$this->id);
		$criteria->compare('person_id',$this->person_id);
		$criteria->compare('prenom',$this->prenom,true);
		$criteria->compare('nom',$this->nom,true);
		$criteria->compare('sexe',$this->sexe,true);
		$criteria->compare('image_name',$this->image_name,true);
		$criteria->compare('date_ajout',$this->date_ajout,true);
		$criteria->compare('is_print',$this->is_print);
		$criteria->compare('date_print',$this->date_print,true);

		return new CActiveDataProvider($this, array(
			'pagination'=>array(
    			'pageSize'=> 10000000,
			),
			
			'criteria'=>$criteria,
		));
	}
	
            
        
public function searchByShiftSectionLevelRoom($sh,$se,$le,$ro,$acad)
{
	$criteria = new CDbCriteria;
	
	$criteria->alias = 'i';
	
	if( ($sh!=null)&&($se==null)&&($le==null)&&($ro==null) )
	{
		 $criteria->join = 'inner join persons p on(p.id = i.person_id) inner join room_has_person h on (i.person_id = h.students) inner join rooms r on (h.room=r.id)';//
		$criteria->condition = 'h.academic_year=:acad1 AND r.shift=:idShift AND p.is_student=1'; // 
		$criteria->params = array(':acad1' => $acad,':idShift'=>$sh);
		
		
	}
	elseif( ($sh!=null)&&($se!=null)&&($le==null)&&($ro==null) )
	{
		 $criteria->join = 'inner join persons p on(p.id = i.person_id) inner join room_has_person h on (i.person_id = h.students) inner join rooms r on (h.room=r.id) inner join levels l on (r.level=l.id)';//
		$criteria->condition = 'h.academic_year=:acad1 AND r.shift=:idShift AND l.section=:idSection AND p.is_student=1 '; //
		$criteria->params = array(':acad1' => $acad,':idShift'=>$sh,':idSection'=>$se);
		
	}
	elseif( ($sh!=null)&&($se!=null)&&($le!=null)&&($ro==null) )
	{
		$criteria->join = 'inner join persons p on(p.id = i.person_id) inner join room_has_person h on (i.person_id = h.students) inner join rooms r on (h.room=r.id) inner join levels l on (r.level=l.id)';//
		$criteria->condition = 'h.academic_year=:acad1 AND r.shift=:idShift AND l.section=:idSection AND r.level=:idLevel AND p.is_student=1 '; // 
		$criteria->params = array(':acad1' => $acad,':idShift'=>$sh,':idSection'=>$se,':idLevel'=>$le);
	}
	elseif( ($sh!=null)&&($se!=null)&&($le!=null)&&($ro!=null) )
	{
		$criteria->join = 'inner join persons p on(p.id = i.person_id) inner join room_has_person h on (i.person_id = h.students) inner join rooms r on (h.room=r.id) inner join levels l on (r.level=l.id)';//
		$criteria->condition = 'h.academic_year=:acad1 AND r.shift=:idShift AND l.section=:idSection AND r.level=:idLevel AND r.id=:roomId AND p.is_student=1'; 
		$criteria->params = array(':acad1' => $acad,':idShift'=>$sh,':idSection'=>$se,':idLevel'=>$le,':roomId'=>$ro);
	}
	
	
	
	return new CActiveDataProvider($this, array(
                    
			'pagination'=>array(
    			'pageSize'=> 10000000,
			),
			
			'criteria'=>$criteria, 
			
        ));
	
}



public function getIdNumber()
			{
			
				$modelPerson = Persons::model()->findByPk($this->person_id);
				
				return $modelPerson->id_number;
					
					
			}
	

public function getIdNumberForList($person_id)
			{
				$modelPerson = Persons::model()->findByPk($person_id);
				
				return $modelPerson->id_number;
					
			}

			
public function getGender()
			{
			
				switch($this->sexe)
				{
					case 0:
						return Yii::t('app','Male');
				
					case 1:
						return Yii::t('app','Female');
					
					}
			}	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}