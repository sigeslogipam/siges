<?php
/* @var $this IdcardController */
/* @var $data Idcard */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('person_id')); ?>:</b>
	<?php echo CHtml::encode($data->person_id); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('prenom')); ?>:</b>
	<?php echo CHtml::encode($data->prenom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nom')); ?>:</b>
	<?php echo CHtml::encode($data->nom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sexe')); ?>:</b>
	<?php echo CHtml::encode($data->sexe); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('image_name')); ?>:</b>
	<?php echo CHtml::encode($data->image_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_ajout')); ?>:</b>
	<?php echo CHtml::encode($data->date_ajout); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('is_print')); ?>:</b>
	<?php echo CHtml::encode($data->is_print); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_print')); ?>:</b>
	<?php echo CHtml::encode($data->date_print); ?>
	<br />

	*/ ?>

</div>