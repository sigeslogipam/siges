<?php
/* @var $this IdcardController */
/* @var $model Idcard */

$this->breadcrumbs=array(
	'Idcards'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Idcard', 'url'=>array('index')),
	array('label'=>'Create Idcard', 'url'=>array('create')),
	array('label'=>'View Idcard', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Idcard', 'url'=>array('admin')),
);
?>

<h1>Update Idcard <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>