<?php 
/*
 * © 2018 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/* @var $this IdcardController */
/* @var $model Idcard */


$part = 'cre';
if(isset($_GET['part'])&&($_GET['part']!=''))
    $part = $_GET['part'];
?>

<div id="dash">
   <div class="span3"><h2>
        <?php echo Yii::t('app','Create Idcard');?>
        
   </h2> </div>
    
    <div class="span3">
       <div class="span4">
            <?php

                     $images = '<i class="fa fa-print"> &nbsp;'.Yii::t('app','Print').'</i>';

                     echo CHtml::link($images,array('/idcards/idcard/index')); 

                    ?>
        </div>
    </div>
</div>


<div class="clear"></div>
<br/>


    <?php
    echo $this->renderPartial('//layouts/navBaseIDcard',NULL,true);	
    ?>

<div class="row">
   
    <?php 
$dict = array(
	'dictDefaultMessage'=>'<i class="fa fa-up"></i> '.Yii::t('app','Drag and Drop your image here to upload'),
	'dictFallbackMessage'=>Yii::t('app','The browser do not support the process. :('),
	'dictFallbackText'=>Yii::t('app','Please use an alternative upload process!'),
	'dictInvalidFileType'=>Yii::t('app','Type of files incorrect !'),
	'dictFileTooBig'=>Yii::t('app','File too big!'),
	'dictResponseError'=>Yii::t('app','Oops ! Something goes wrong!'),
	'dictCancelUpload'=>Yii::t('app','Delete'),
	'dictCancelUploadConfirmation'=>Yii::t('app','Do you want to cancel the upload?'),
	'dictRemoveFile'=>Yii::t('app','Delete'),
	'dictMaxFilesExceeded'=>Yii::t('app','File size exceeded'),
);

$options = array(
    'addRemoveLinks'=>true,
);

$events = array(
    'success' => 'successUp(this, param, param2, param3)',
    'totaluploadprogress'=>'incProgress(this, param, param2, param3)',
    'queuecomplete'=>'complete()'
);?>
    <div class="span2">
        
    </div> 
  
    <div class="span8">
         
        <div id="dropZon">
        <?php

        $this->widget('ext.dropzone.EDropzone', array(
            //'model' => $model,
            //'attribute' => 'file',
            'name'=>'upload',
            'url' => $this->createUrl('uploadPhoto'),
            'mimeTypes' => array('image/jpeg','image/jpg', 'image/png'),
            'events' => $events,
            'options' => CMap::mergeArray($options, $dict ),
            'htmlOptions' => array('style'=>'height:95%; overflow: hidden;'),
            //'customStyle'=> $this->module->assetsPath.'/css/customdropzone.css'
        ));
        ?>
        </div>
    </div>
    <div class="span2">
        
    </div>
    

</div>
<div class="row">
    <div class="span1">
        
    </div>
    <div class="span10">
         <br/>  <br/>
        <div id="lis_foto">
        
        </div>
    </div>
    
    <div class="span1">
        
    </div>
</div>

<script>
    $(document).ready(function(){
       
            
        $.get('<?= Yii::app()->baseUrl ?>/index.php/idcards/idcard/listephotos',{},function(data){
                $('#lis_foto').html(data);
            });
        $('#dropZon').mouseover(function(){
            $.get('<?= Yii::app()->baseUrl ?>/index.php/idcards/idcard/listephotos',{},function(data){
                $('#lis_foto').html(data);
            });
           //alert('Test li sou li');
        });
	/*	*/
       
       
    });
</script>

