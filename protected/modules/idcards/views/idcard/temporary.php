<?php 
/*
 * © 2018 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/* @var $this IdcardController */
/* @var $dataProvider CActiveDataProvider */

$part = 'cre';
if(isset($_GET['part'])&&($_GET['part']!=''))
    $part = $_GET['part'];


$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));
 

$template='{print}';

$condition = 'p.active IN(1, 2) AND ';
?>


<!-- Menu of CRUD  -->

<div id="dash">
          
          <div class="span3"><h2>      
       <?php  echo Yii::t('app','Print temporary card');
		?>
              </h2> </div>
              
      <div class="span3">
             
         <?php

                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';

                    echo '<div class="span4">';
         							 	echo CHtml::link($images,array('idcard/create'));
         							 	
         							 	echo '  </div>'; 
         ?>

    </div> 

 </div>



<div style="clear:both"></div>	
<br/>


    <?php
    echo $this->renderPartial('//layouts/navBaseIDcard',NULL,true);	
    ?>

<?php 
 
 $form=$this->beginWidget('CActiveForm', array(
	'id'=>'idcard-form',
	'enableAjaxValidation'=>true,
));

?>				



        <div class="box-body">
             
                  <div class="table-responsive">
                 


<!--category_-->
		<!--	<div class="left" style="margin-left:10px;">
			<label for="category_"> <?php 
					// echo Yii::t('app','Category'); 
					 ?>
				</label>
					 <?php 
					 
					 
					/*	
						if(isset($this->category))
                                                            echo $form->dropDownList($model,'category_',array(null=>Yii::t('app','All'),'0'=>Yii::t('app','Students'),'1'=>Yii::t('app','Teachers'),'2'=>Yii::t('app','Employees') ), array('options' => array($this->category=>array('selected'=>true)),'onchange'=> 'submit()', )); 
							 else
								{
								  echo $form->dropDownList($model,'category_',array(null=>Yii::t('app','All'),'0'=>Yii::t('app','Students'),'1'=>Yii::t('app','Teachers'),'2'=>Yii::t('app','Employees') ), array('onchange'=> 'submit()' )); 
					                 }
						
						*/
					 
					  ?>
				</div>
                     -->
<?php
 // if( ($this->category== 0) && ($this->category!=null) )
  //{
?>
   	
   
   <div  style="padding:0px;">			
			<!--Shift(vacation)-->
        <div class="left" style="margin-left:10px;" >
		
			<label for="Shifts"> <?php 
					 echo Yii::t('app','Shift'); 
					 ?>
				</label>
					 <?php 
					   
					 
						$modelShift = new Shifts;
						
						$default_vacation=null;
			            
			            $default_vacation_name = infoGeneralConfig('default_vacation');
			            
			         /*   $criteria2 = new CDbCriteria;
				   		$criteria2->condition='shift_name=:item_name';
				   		$criteria2->params=array(':item_name'=>$default_vacation_name,);
				   		$default_vacation = Shifts::model()->find($criteria2);
						*/
			            
			            
						    
						
			

						      if(isset($this->idShift)&&($this->idShift!=""))
						        {   
					               echo $form->dropDownList($modelShift,'shift_name',$this->loadShift(), array('options' => array($this->idShift=>array('selected'=>true)),'onchange'=> 'submit()' )); 
					             }
							  else
								{ $this->idLevel=0;
								     if($default_vacation!=null)
								       { echo $form->dropDownList($modelShift,'shift_name',$this->loadShift(), array('options' => array(($default_vacation->id)=>array('selected'=>true)),'onchange'=> 'submit()' )); 
								            $this->idShift=$default_vacation->id;
								       }
								    else
								       echo $form->dropDownList($modelShift,'shift_name',$this->loadShift(), array('onchange'=> 'submit()' )); 
								}
							
						echo $form->error($modelShift,'shift_name'); 
						
					
					  ?>
				</div>
			 
		    <!--section(liee au Shift choisi)-->
			<div class="left" style="margin-left:10px;" >
			<label for="Sections"> <?php 
					echo Yii::t('app','Section'); 
					?></label><?php 
					
					
											$modelSection = new Sections;
							    if(isset($this->section_id))
							       echo $form->dropDownList($modelSection,'section_name',$this->loadSectionByIdShift($this->idShift), array('options' => array($this->section_id=>array('selected'=>true)),'onchange'=> 'submit()')); 
							    else
								  { $this->section_id=0; $this->idLevel=0; $this->room_id=0;
									echo $form->dropDownList($modelSection,'section_name',$this->loadSectionByIdShift($this->idShift), array('onchange'=> 'submit()' )); 
						           }					      
						  
						echo $form->error($modelSection,'section_name'); 
						
					
											
					   ?>
				</div>
			
			<!--level-->
			<div class="left" style="margin-left:10px;">
			<label for="Levels"> <?php 
					 echo Yii::t('app','Level'); 
					 ?>
				</label>
					 <?php 
					 
					 
						$modelLevelPerson = new LevelHasPerson;
						if(isset($this->idLevel))
							    echo $form->dropDownList($modelLevelPerson,'level',$this->loadLevelByIdShiftSectionId($this->idShift,$this->section_id), array('options' => array($this->idLevel=>array('selected'=>true)),'onchange'=> 'submit()', )); 
							 else
								{ $this->idLevel=0;
								  echo $form->dropDownList($modelLevelPerson,'level',$this->loadLevelByIdShiftSectionId($this->idShift,$this->section_id), array('onchange'=> 'submit()' )); 
					             }
						echo $form->error($modelLevelPerson,'level'); 
						
					 
					  ?>
				</div>
			
			<!--room / title-->
			<div class="left" style="margin-left:10px;">
			     <label for="Titles"> <?php 
					 echo Yii::t('app','Room'); 
					 ?></label><?php 
					
					 
						$modelRoom = new RoomHasPerson;
						    
							  
							  if(isset($this->room_id))
							   {
						          echo $form->dropDownList($modelRoom,'room',$this->loadRoomByIdShiftSectionLevel($this->idShift,$this->section_id,$this->idLevel), array('onchange'=> 'submit()','options' => array($this->room_id=>array('selected'=>true)) )); 
					             }
							   else
							      echo $form->dropDownList($modelRoom,'room',$this->loadRoomByIdShiftSectionLevel($this->idShift,$this->section_id,$this->idLevel), array('onchange'=> 'submit()')); 
						echo $form->error($modelRoom,'room'); 
						
								   
					   ?>
				</div>
			  
     </div>
  <?php
 // }
?>   
   

 
                            

<div class="grid-view">

<div class="search-form" >

<?php 		
     
//if( ($this->category== 0) && ($this->category!=null) ) //elev
//{

	 if( ($this->idShift==0)||($this->idShift=='') ) //tout elev
	  {
              $condition = 'p.active IN(1,2) AND ';
              $dataProvider= Persons::model()->searchStudents_($condition,$acad_sess);//$dataProvider=$model->searchAllStudents(); 
		 
		 $tmwen=false;
		 
		 if($dataProvider->getItemCount()==0)
			{ 
				 $tmwen=false;
				
		    }
         else
		   {    $tmwen=true;
			}
	 
  $gridWidget=$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'persons-grid',
			'summaryText'=>'',
			'dataProvider'=>$dataProvider,
			 
			 
			'columns'=>array(
				
                                array(
                                        'name'=>Yii::t('app','Code student'),
                                        'type' => 'raw',
                                        'value'=>'$data->id_number',
                                        'htmlOptions'=>array('width'=>'100px'),
				),
                                
                                array(
                                'name' => 'first_name',
                                'type' => 'raw',
                                'value'=>'$data->first_name',
                                'htmlOptions'=>array('width'=>'150px'),
                                ),
                                
                                array(
                                'name' => 'last_name',
                                'type' => 'raw',
                                'value'=>'$data->last_name',
                                'htmlOptions'=>array('width'=>'150px'),
                                ),
                                
                                
				array(
                                    'name'=>'gender',
                                    'value'=>'$data->getGenders1()',
                                    'htmlOptions'=>array('width'=>'50px'),
						),
				
				
			),
		   ));

	  }
	elseif( ($this->idShift!='')&&($this->idShift!=0) )
	 {
		 $dataProvider= Rooms::model()->getInfoStudentsEnrolled($condition,$this->room_id, $acad_sess); 
		 $tmwen=false;
		 
		 if($dataProvider->getItemCount()==0)
			{ 
				 $tmwen=false;
				
		    }
         else
		   {    $tmwen=true;
			}
	    
		$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'idcard-grid',
	'summaryText'=>'',
	'dataProvider'=>$dataProvider,
	'showTableOnEmpty'=>'true',
	'selectableRows' =>2,
	
    'columns'=>array(
	 
	
        array(
							'name'=>Yii::t('app','Id Number' ),
							'value'=>'$data->id_number',
						),
						
		array(
							'name'=>Yii::t('app','First Name' ),
							'value'=>'$data->first_name',
						),
		array(
							'name'=>Yii::t('app','Last Name' ),
							'value'=>'$data->last_name',
						),
		array(
							'name'=>'sexe',
							'value'=>'$data->getGenders1()',
						),
		
		
		
     
       array(             'class'=>'CCheckBoxColumn',   
                           'id'=>'chk',
                 ),           
		
    ),
));

?>

                                                   

<?php
	 }
   
?>
                                                   
                                                   
</div>
    
</div>


</br></br>

<div id="resp_form_siges">

    <form  id="resp_form">
        <div class="row-fluid">
   	
<center>
<div class="col-submit">
		                
	 <?php 
	    
                if( ($this->category== 0))// && ($this->category!=null) ) //elev
                   {
                      if($dataProvider->getItemCount()!=0)
	               {
                          if(($this->room_id!=null)||($this->idLevel!=null) ) //||($this->section_id!=null)|($this->idShift!=null))		
			      {
				      if($tmwen==true)
						{              
		                         echo CHtml::submitButton(Yii::t('app', 'Print temporary IDcards'),array('name'=>'print','class'=>'btn btn-warning'));
								       
								  
				 
			                   }
                              }
                       }
                              
                    }
                
                        
                        
	                                                              

	?>
                
      </div>
</center>

   </div>

 </form>
 
 </br>
 
</div  >



                                        
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                
<?php  $this->endWidget();   ?>   

            
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  
                   // {extend: 'excel', title: "<?= Yii::t('app','Ready to print'); ?>"},
                  
                 
                ]

            });
            

        });
</script>






