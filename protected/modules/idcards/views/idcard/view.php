<?php
/* @var $this IdcardController */
/* @var $model Idcard */

$this->breadcrumbs=array(
	'Idcards'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Idcard', 'url'=>array('index')),
	array('label'=>'Create Idcard', 'url'=>array('create')),
	array('label'=>'Update Idcard', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Idcard', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Idcard', 'url'=>array('admin')),
);
?>

<h1>View Idcard #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'person_id',
		'prenom',
		'nom',
		'sexe',
		'image_name',
		'date_ajout',
		'is_print',
		'date_print',
	),
)); ?>
