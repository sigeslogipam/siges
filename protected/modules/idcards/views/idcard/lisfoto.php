<?php 
/*
 * © 2018 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>

<div class="grid-view" id="table-container" >
 <center> <table id="maintable" class="items dataTable table-striped table-bordered table-hover table-condensed " style="width:60%;" >
    <thead>
        <tr >
    <th colspan="6"><?= Yii::t('app','Liste photo')?></th>
    
        </tr>
    
    </thead>
    <tbody>
        <?php 
		       $i =1;
            foreach($id_card as $ci){ 
                 $string_image_location = "<img src=".Yii::app()->request->baseUrl."/documents/photo_Uploads/1/".$ci->image_name.">";
                ?>
        <tr>
            <td  style="width:8%;" ><?= $i; ?></td>
			<td  style="width:25%;" ><?= $ci->prenom; ?></td>
                        <td  style="width:23%;" ><?= $ci->nom; ?></td>
			<td><div class="avatar " style="width:23%;">

					        <?php

					            $path = Yii::app()->request->baseUrl.'/documents/photo-Uploads/1/'.$ci->image_name;
                                
                             ?>
					<img src="<?= $path ?>" class="img img-rounded img-responsive avatar" alt="" > 

				</div>
			</td>
			<td  style="width:12%" ><?= $model->getIdNumberForList($ci->person_id); ?></td>
			
            <td><a href="<?= Yii::app()->baseUrl?>/index.php/idcards/idcard/deletecard?id=<?= $ci->id ?>" title='<?= Yii::t('app','Delete')?>' data-type='POST' onclick='return confirm("<?= Yii::t('app','Do you really want to the delete this picture ?'); ?>")'><i class="fa fa-trash"></i></a></td>
            
        </tr>
        <?php 
               $i++;
            }   
        ?>
    </tbody>
    
  </table>
  </center>
</div>


<script>
 $(document).ready(function(){

 $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                   // lengthMenu:    "Afficher _MENU_ &eacute;l&eacute;ments",
                    info:           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
                    infoEmpty:      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
                    infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "Aucune donnée disponible dans le tableau",

                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                  //  {extend: 'excel', title: ""},
                  
                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
 });
            
</script>


