<?php
/* @var $this IdcardController */
/* @var $model Idcard */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'idcard-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'person_id'); ?>
		<?php echo $form->textField($model,'person_id'); ?>
		<?php echo $form->error($model,'person_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'prenom'); ?>
		<?php echo $form->textField($model,'prenom',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'prenom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nom'); ?>
		<?php echo $form->textField($model,'nom',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'nom'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sexe'); ?>
		<?php echo $form->textField($model,'sexe',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'sexe'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'image_name'); ?>
		<?php echo $form->textField($model,'image_name',array('size'=>60,'maxlength'=>64)); ?>
		<?php echo $form->error($model,'image_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_ajout'); ?>
		<?php echo $form->textField($model,'date_ajout'); ?>
		<?php echo $form->error($model,'date_ajout'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'is_print'); ?>
		<?php echo $form->textField($model,'is_print'); ?>
		<?php echo $form->error($model,'is_print'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_print'); ?>
		<?php echo $form->textField($model,'date_print'); ?>
		<?php echo $form->error($model,'date_print'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->