<?php 
/*
 * © 2018 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


Yii::import('ext.tcpdf.*');
Yii::import('ext.tcpdf.SIGESPDF');


class IdcardController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';

	public $idShift;
	public $section_id;
	public $idLevel;
	public $room_id;
        
        public $part;
        
        public $category;
        public $printing_date=false;
	
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
	     $explode_url= explode("/",substr($_SERVER['REQUEST_URI'],1));
            $controller=$explode_url[3];
            
            $actions=$this->getRulesArray($this->module->name,$controller);
          
            if($this->getModuleName($this->module->name))
                {
		            if($actions!=null)
             			 {     return array(
				              	  	array('allow',  
					                	
					                	'actions'=> array_merge($actions,array('uploadPhoto','listephotos')),
		                                  'users'=>array(Yii::app()->user->name),
				                    	),
				              		  array('deny',  
					                 	'users'=>array('*'),
				                    ),
			                );
             			 }
             			 else
             			  return array(array('deny', 'users'=>array('*')),);
                }
                else
                {
                    return array(array('deny', 'users'=>array('*')),);
                }

             /*   return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','deletecard','index','uploadPhoto','listephotos','printidcard'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
              
              */
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{   
            $this->part = 'cre';
		
            $model=new Idcard;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Idcard']))
		{
			$model->attributes=$_POST['Idcard'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Idcard']))
		{
			$model->attributes=$_POST['Idcard'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->id));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}
	
	
	public function actionPrintidcard($id)
	{
		$acad_sess = acad_sess();
		$acad=Yii::app()->session['currentId_academic_year']; 

		$model=$this->loadModel($id);
		
		

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		 //Extract school name 
							$school_name = infoGeneralConfig('school_name');
                        //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
					
			//Extract idcard orientation 
                            $orientation = infoGeneralConfig('idcard_orientation');
							//Extract idcard template 
                            $idcard_template = infoGeneralConfig('idcard_template');
							
							
							
				   			
							     //on retourne l'ID de l'eleve
								// Ne pas eliminer les commentaires dans la generation des fichiers PDF, posible utilisation ulterieure par d'autres programmeurs		
							    
									$idcard=$id;
                                                                        $shift =null;
                                                                        $room = null;
								
								// Set some content to print
								$person_id = $model->person_id;
                                                                
                                                               			
								
                                                                $modelPerson=Persons::model()->findByPk($person_id);
								if($modelPerson->is_student==1)
                                                                {   $shift = $this->getShiftByStudentId($person_id, $acad_sess) ->id;
								
								   $room = $this->getRoomByStudentId($person_id, $acad_sess) ->id;
                                                                   //$room = $this->getLevelByStudentId($person_id, $acad_sess) ->id;
                                                                
                                                                }
								
								// create new PDF document
								//$pdf = new SIGESPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
								$pdf = new SIGESPDF($orientation, PDF_UNIT, array('88','57'), true, 'UTF-8', false); // letter: 216x279 mm ; legal: 216x356;  612.000, 1008.00 ; 11.00x17.00 :279x432 mm
                                                                $barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
								
								// set document information
								$pdf->SetCreator(PDF_CREATOR);
								 		 
								$pdf->SetAuthor($school_name);
								$pdf->SetTitle(Yii::t('app',"ID Card"));
								$pdf->SetSubject(Yii::t('app',"ID Card"));
							
								// set default header data
								//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
								
								//$pdf->SetHeaderData(PDF_HEADER_LOGO_REPORTCARD, PDF_HEADER_LOGO_REPORTCARD_WIDTH, "", ""); //CNR
								//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $school_name, "$school_address \nTel: $school_phone_number\nE-mail: $school_email_address", pdfHeaderTextColor(), pdfHeaderLineColor());
								//$pdf->setFooterData(array(0,64,0), array(0,64,128));

								// set header and footer fonts
								$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
								//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

								// set default monospaced font
								$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

								// set margins
								$pdf->SetMargins(0.5,0,0.5);      
								//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
								//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
								$pdf->SetHeaderMargin(0);
								//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
								$pdf->SetFooterMargin(0);
                                
								
								// set auto page breaks
								$pdf->SetAutoPageBreak(false, 0); // PDF_MARGIN_BOTTOM

								// set image scale factor
								$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

								// set some language-dependent strings (optional)
								if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
									require_once(dirname(__FILE__).'/lang/eng.php');
									$pdf->setLanguageArray($l);
								}

								// ---------------------------------------------------------

								// set default font subsetting mode
								$pdf->setFontSubsetting(true);

								// Set font
								// dejavusans is a UTF-8 Unicode font, if you only need to
								// print standard ASCII chars, you can use core fonts like
								// helvetica or times to reduce file size.
								$pdf->SetFont('helvetica', '', 12, '', true);
				   
								 // Add a page
								// This method has several options, check the source code documentation for more information.
								$pdf->AddPage();

								// set text shadow effect
								//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

								// Set some content to print
								
							     if( ($orientation=='P')||($orientation=='p') )
							     {
									switch($idcard_template)
									  { 
									     case 1:
                                                                             {	//$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_p1($idcard,$shift,$room,$barcod), true, false, true, false, '');
									 
                                                                             }
                                                                             break;
											
									    case 2:
                                                                            {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_p2($idcard,$shift,$room,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    default :
                                                                               {      //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_p1($idcard,$shift,$room,$barcod), true, false, true, false, '');
									 
                                                                               }
                                                                               break;
											
									    
									 }
								 }
							   elseif( ($orientation=='L')||($orientation=='l') )
							    {
									 switch($idcard_template)
									  { 
									     case 1:
                                                                             {	//$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l1($idcard,$shift,$room,$barcod), true, false, true, false, '');
									 
                                                                             }
                                                                             break;
											
									    case 2:
                                                                               {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l2($idcard,$shift,$room,$barcod), true, false, true, false, '');
									 
                                                                               }
                                                                               break;
											
									     case 3:
                                                                               {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l3($idcard,$shift,$room,$barcod), true, false, true, false, '');
									 
                                                                               }
                                                                               break;
											
									    default :
                                                                                {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l1($idcard,$shift,$room,$barcod), true, false, true, false, '');
									 
                                                                                }
                                                                                break;
											
									    
									 }  
									
							   }                                  	
					
                                                           
                                          //update date_print
                                                if($this->printing_date == true)
                                                  { 
                                                    $model->is_print=1;
                                                    $model->date_print=date('Y-m-d');
                                                    $model->save();
                                                    
                                                    

                                                  }
                                                  
				         $pdf->Output('idcard'.$person_id.'.pdf', 'D');            
									
		                
							                                      	
							     	 
								
                                  /*Parameters
    $name	(string) The name of the file when saved. Note that special characters are removed and blanks characters are replaced with the underscore character.
    $dest	(string) Destination where to send the document. It can take one of the following values:

        I: send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
        D: send to the browser and force a file download with the name given by name.
        F: save to a local server file with the name given by name.
        S: return the document as a string (name is ignored).
        FI: equivalent to F + I option
        FD: equivalent to F + D option
        E: return the document as base64 mime multi-part email attachment (RFC 2045)
*/

								//============================================================+
								// END OF FILE
								//============================================================+		
		
			          

		$this->redirect(array('index'));
		
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDeletecard($id)
	{
		     $model = $this->loadModel($id); 
            $filename = Yii::app()->request->baseUrl.'/documents/photo-Uploads/1/'.$model->image_name; 
            if(is_file($filename)){
                if(unlink($filename)){
                    $model->delete();
					//retire non photo a nan tab persons
					$modelPersons=new Persons();
                     $pers = $modelPersons->findByPk($model->person_id);
							 $pers->image = '';
							 $pers->save();					
                     $this->redirect(array('create'));
                }
            }else {
              $model = $this->loadModel($id); 
			  $model->delete();
                $this->redirect(array('create'));
            }
		   
	}
	

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
            
            $this->printing_date = false;
            
            $this->part = 'prt';
            
            $model=new Idcard('searchByShiftSectionLevelRoom');
		
		$modelShift= new Shifts;
		$modelSection= new Sections;
		$modelLevel= new LevelHasPerson;
		$modelRoomPerson=new RoomHasPerson;
		
		 $this->idShift=null;
		$this->section_id=null;
		$this->idLevel=null;
		$this->room_id=null;
                $this->category = null;
		
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
$acad_name=Yii::app()->session['currentName_academic_year']; 

		
  if(isset($_POST['Idcard']['category_']))
      $this->category = $_POST['Idcard']['category_'];
  
  
  //$this->category= 0: elev, 1: prof, 2:emp  

  if( ($this->category== 0) && ($this->category!=null) ) 
  {
      
                $shift_id = $this->idShift;
		$room_id = $this->room_id;
                
                
		
        if(isset($_POST['Shifts']))
			               	  {  //on n'a pas presser le bouton, il fo load apropriate rooms
								      $modelShift->attributes=$_POST['Shifts'];
						              $this->idShift=$modelShift->shift_name;
										
									   $modelSection->attributes=$_POST['Sections'];
									   $this->section_id=$modelSection->section_name;
								     	
									   $modelLevel->attributes=$_POST['LevelHasPerson'];
									   $this->idLevel=$modelLevel->level;
									   
									   $modelRoomPerson->attributes=$_POST['RoomHasPerson'];
									   $this->room_id=$modelRoomPerson->room;
									  
				                 } 
			                   else
			                     {
			                         $this->idShift=null;
										$this->section_id=null;
										$this->idLevel=null;
										$this->room_id=null;
								 }
                                                                 
    }
								 
		
		   if(isset($_POST['print']))
			{ //on vient de presser le bouton create
				 	
						 //Extract school name 
							$school_name = infoGeneralConfig('school_name');
                        //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
				   		
                        //Extract idcard orientation 
                            $orientation = infoGeneralConfig('idcard_orientation');
							//Extract idcard template 
                            $idcard_template = infoGeneralConfig('idcard_template');						
	                
					   
						
							     //on retourne l'ID de l'eleve
								// Ne pas eliminer les commentaires dans la generation des fichiers PDF, posible utilisation ulterieure par d'autres programmeurs		
							    
								// create new PDF document
								//$pdf = new SIGESPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
								$pdf = new SIGESPDF($orientation, PDF_UNIT, array('88','57'), true, 'UTF-8', false); // letter: 216x279 mm ; legal: 216x356;  612.000, 1008.00 ; 11.00x17.00 :279x432 mm

								// set document information
								$pdf->SetCreator(PDF_CREATOR);
								 		 
								$pdf->SetAuthor($school_name);
								$pdf->SetTitle(Yii::t('app',"ID Card"));
								$pdf->SetSubject(Yii::t('app',"ID Card"));
							
								// set default header data
								//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
								
								//$pdf->SetHeaderData(PDF_HEADER_LOGO_REPORTCARD, PDF_HEADER_LOGO_REPORTCARD_WIDTH, "", ""); //CNR
								//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $school_name, "$school_address \nTel: $school_phone_number\nE-mail: $school_email_address", pdfHeaderTextColor(), pdfHeaderLineColor());
								//$pdf->setFooterData(array(0,64,0), array(0,64,128));

								// set header and footer fonts
								$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
								//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

								// set default monospaced font
								$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

								// set margins
								$pdf->SetMargins(0.5,0,0.5);      
								//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
								//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
								$pdf->SetHeaderMargin(0);
								//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
								$pdf->SetFooterMargin(0);
                                
								
								// set auto page breaks
								$pdf->SetAutoPageBreak(false, 0); // PDF_MARGIN_BOTTOM

								// set image scale factor
								$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

								// set some language-dependent strings (optional)
								if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
									require_once(dirname(__FILE__).'/lang/eng.php');
									$pdf->setLanguageArray($l);
								}

								// ---------------------------------------------------------

								// set default font subsetting mode
								$pdf->setFontSubsetting(true);

								// Set font
								// dejavusans is a UTF-8 Unicode font, if you only need to
								// print standard ASCII chars, you can use core fonts like
								// helvetica or times to reduce file size.
								$pdf->SetFont('helvetica', '', 12, '', true);

			         if( isset($_POST['chk']) )  
						{
                            foreach($_POST['chk'] as $val) 
							  {
								  $shift_id = null;
                                                                  $room_id = null;
                                                                  $level_id=null;
                                                                  
									$idcard=$val;
									$modelIdcard=Idcard::model()->findByPk($idcard);
									
		                        $person_id = $modelIdcard->person_id;
                                        
                                        $barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
										
                                                                             
                                                                             $modelPerson=Persons::model()->findByPk($person_id);
								
						     if($modelPerson->is_student)
                                                        {
                                                              if($this->idShift!='')
									 $shift_id = $this->idShift;
								 else 
								 {
									  $shift_id = $this->getShiftByStudentId($person_id,$acad_sess)->id;
								 }
								 if($this->room_id!='')
									 $room_id = $this->room_id;
								 else 
								 {
									 $room_id = $this->getRoomByStudentId($person_id,$acad_sess)->id;
								 }
                                                                 
                                                                 if($this->idShift!='')
									 $level_id=  $this->idLevel;
								 else 
								 {
									  $level_id= $this->getLevelByStudentId($person_id,$acad_sess)->id;
								 } 
                                                                 
                                                        }
									   
								 // Add a page
								// This method has several options, check the source code documentation for more information.
								$pdf->AddPage();

								// set text shadow effect
								//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

								// Set some content to print 
								
							   if( ($orientation=='P')||($orientation=='p') )
							     {
									switch($idcard_template)
									  { 
									     case 1:
                                                                             {	//$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_p1($idcard,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                             }
                                                                             break;
											
									    case 2:
                                                                            {  //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_p2($idcard,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    default :
                                                                            {    //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_p1($idcard,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    
									 }
								 }
							   elseif( ($orientation=='L')||($orientation=='l') )
							    {
									 switch($idcard_template)
									  { 
									     case 1:
                                                                             {	//$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l1($idcard,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                             }
                                                                             break;
											
									    case 2:
                                                                            {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l2($idcard,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    case 3:
                                                                            {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l3($idcard,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    default :
                                                                                {  //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_l1($idcard,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                                }
                                                                                break;
											
									    
									 }  
									
							   }
									
									
								 
						//update date_print
                                                if($this->printing_date == true)
                                                  { 
                                                    $modelIdcard->is_print=1;
                                                    $modelIdcard->date_print=date('Y-m-d');
                                                    $modelIdcard->save();
                                                    
                                                   // $modelIdcard =new Idcard();

                                                  }
							  
                                              
                                              
                                              }
                                                          
                                            
                                                        //$this->category= 0: elev, 1: prof, 2:emp  

                                                       if( ($this->category== 0) && ($this->category!=null) ) 
                                                          { 
								if($this->idShift!='')
								{	$modelshift=Shifts::model()->findByPk($this->idShift);
								  $shift_name = $modelshift->shift_name;
								}
								  
								 if($this->room_id!='')
								{ $modelroom=Rooms::model()->findByPk($this->room_id);
								  $room_name = $modelroom->room_name;
								}
								  
								 if($this->idLevel!='')
								{  $modellevel=Levels::model()->findByPk($this->idLevel);
								  $level_name = $modellevel->level_name;
								}
								  
								 if($this->section_id!='')
								{  $modelsection=Sections::model()->findByPk($this->section_id);
								  $section_name = $modelsection->section_name;
								}
								  
						       if($this->room_id!=null)
								 {
									 $pdf->Output($room_name.'_'.$acad_name.'.pdf', 'D'); 
								 }	 
								elseif($this->idLevel!=null)
								{
									$pdf->Output($level_name.'_'.$acad_name.'.pdf', 'D'); 
								}
								elseif($this->section_id!=null)
								{
									$pdf->Output($section_name.'_'.$acad_name.'.pdf', 'D'); 
								}
								elseif($this->idShift!=null)
								{
									$pdf->Output($shift_name.'_'.$acad_name.'.pdf', 'D'); 
								}
								else
								{
									$pdf->Output($student_name.'_'.$acad_name.'.pdf', 'D'); 
								}
                             								 
							    }
                                                         elseif($this->category== 1)
                                                           {
                                                             $pdf->Output(Yii::t('app','Teachers').'_'.$acad_name.'.pdf', 'D');
                                                             
                                                           }
                                                           elseif($this->category== 2)
                                                           {
                                                             $pdf->Output(Yii::t('app','Employee').'_'.$acad_name.'.pdf', 'D');
                                                             
                                                           }  	  
  
							                                      	
						} 	 
								
                                  /*Parameters
    $name	(string) The name of the file when saved. Note that special characters are removed and blanks characters are replaced with the underscore character.
    $dest	(string) Destination where to send the document. It can take one of the following values:

        I: send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
        D: send to the browser and force a file download with the name given by name.
        F: save to a local server file with the name given by name.
        S: return the document as a string (name is ignored).
        FI: equivalent to F + I option
        FD: equivalent to F + D option
        E: return the document as base64 mime multi-part email attachment (RFC 2045)
*/

								//============================================================+
								// END OF FILE
								//============================================================+		
		
			          
		     }
			
		    
		$this->render('index',array(
			'model'=>$model,
		));
	}

        
 /**
	 * Lists all models.
	 */
	public function actionTemporary()
	{
            $condition = 'p.active IN(1, 2) AND ';
            $this->printing_date = false;
            
            $this->part = 'prtt';
            
            $model=new Idcard('searchByShiftSectionLevelRoom');
		
		$modelShift= new Shifts;
		$modelSection= new Sections;
		$modelLevel= new LevelHasPerson;
		$modelRoomPerson=new RoomHasPerson;
		
		 $this->idShift=null;
		$this->section_id=null;
		$this->idLevel=null;
		$this->room_id=null;
                //$this->category = null;
		
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
$acad_name=Yii::app()->session['currentName_academic_year']; 

		
  //if(isset($_POST['Idcard']['category_']))
      $this->category = 0;    //$_POST['Idcard']['category_'];
  
   //$this->category= 0: elev, 1: prof, 2:emp  

  if( ($this->category== 0))// && ($this->category!=null) ) 
  {
     
                $shift_id = $this->idShift;
		$room_id = $this->room_id;
                
                
                            if(isset($_POST['Shifts']['shift_name']))
                                    { 
                              
						   $modelShift->attributes=$_POST['Shifts']['shift_name'];
			              $this->idShift=$_POST['Shifts']['shift_name'];//$modelShift->shift_name;
						  	                     
				          $modelSection->attributes=$_POST['Sections']['section_name'];
						   $this->section_id=$_POST['Sections']['section_name'];  //$modelSection->section_name;
						   						  						
						   $modelLevel->attributes=$_POST['LevelHasPerson']['level'];
						   $this->idLevel=$_POST['LevelHasPerson']['level'];  //$modelLevel->level;
						   
						    $modelRoomPerson->attributes=$_POST['RoomHasPerson']['room'];
									   $this->room_id=$_POST['RoomHasPerson']['room'];  //$modelRoomPerson->room;
					
									  
									  
				                 } 
			                   else
			                     {
			                         $this->idShift=null;
										$this->section_id=null;
										$this->idLevel=null;
										$this->room_id=null;
								 }
                                                                 
    }
								 
		
		   if(isset($_POST['print']))
			{ //on vient de presser le bouton create
				 	
						 //Extract school name 
							$school_name = infoGeneralConfig('school_name');
                        //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
				   		
                        //Extract idcard orientation 
                            $orientation = infoGeneralConfig('idcard_orientation');
							//Extract idcard template 
                            $idcard_template = infoGeneralConfig('idcard_template');						
	                
					   
						
							     //on retourne l'ID de l'eleve
								// Ne pas eliminer les commentaires dans la generation des fichiers PDF, posible utilisation ulterieure par d'autres programmeurs		
							    
								// create new PDF document
								//$pdf = new SIGESPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
								$pdf = new SIGESPDF($orientation, PDF_UNIT, array('88','57'), true, 'UTF-8', false); // letter: 216x279 mm ; legal: 216x356;  612.000, 1008.00 ; 11.00x17.00 :279x432 mm

								// set document information
								$pdf->SetCreator(PDF_CREATOR);
								 		 
								$pdf->SetAuthor($school_name);
								$pdf->SetTitle(Yii::t('app',"ID Card"));
								$pdf->SetSubject(Yii::t('app',"ID Card"));
							
								// set default header data
								//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE.' 001', PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
								
								//$pdf->SetHeaderData(PDF_HEADER_LOGO_REPORTCARD, PDF_HEADER_LOGO_REPORTCARD_WIDTH, "", ""); //CNR
								//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, $school_name, "$school_address \nTel: $school_phone_number\nE-mail: $school_email_address", pdfHeaderTextColor(), pdfHeaderLineColor());
								//$pdf->setFooterData(array(0,64,0), array(0,64,128));

								// set header and footer fonts
								$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
								//$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

								// set default monospaced font
								$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

								// set margins
								$pdf->SetMargins(0.5,0,0.5);      
								//$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
								//$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
								$pdf->SetHeaderMargin(0);
								//$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
								$pdf->SetFooterMargin(0);
                                
								
								// set auto page breaks
								$pdf->SetAutoPageBreak(false, 0); // PDF_MARGIN_BOTTOM

								// set image scale factor
								$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

								// set some language-dependent strings (optional)
								if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
									require_once(dirname(__FILE__).'/lang/eng.php');
									$pdf->setLanguageArray($l);
								}

								// ---------------------------------------------------------

								// set default font subsetting mode
								$pdf->setFontSubsetting(true);

								// Set font
								// dejavusans is a UTF-8 Unicode font, if you only need to
								// print standard ASCII chars, you can use core fonts like
								// helvetica or times to reduce file size.
								$pdf->SetFont('helvetica', '', 12, '', true);

			         if( isset($_POST['chk']) )  
						{
                            foreach($_POST['chk'] as $val) 
							  {
								  $shift_id = null;
                                                                  $room_id = null;
                                                                  $level_id=null;
                                                                 
									
		                        $person_id = $val;
                                        
                                        $barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
										
                                                                             
                                                    $modelPerson=Persons::model()->findByPk($person_id);
								
						     if($modelPerson->is_student)
                                                        {
                                                              if($this->idShift!='')
									 $shift_id = $this->idShift;
								 else 
								 {
									  $shift_id = $this->getShiftByStudentId($person_id,$acad_sess)->id;
								 }
								 if($this->room_id!='')
									 $room_id = $this->room_id;
								 else 
								 {
									 $room_id = $this->getRoomByStudentId($person_id,$acad_sess)->id;
								 }
                                                                 
                                                                 if($this->idShift!='')
									 $level_id=  $this->idLevel;
								 else 
								 {
									  $level_id= $this->getLevelByStudentId($person_id,$acad_sess)->id;
								 } 
                                                                 
                                                        }
									   
								 // Add a page
								// This method has several options, check the source code documentation for more information.
								$pdf->AddPage();

								// set text shadow effect
								//$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

								// Set some content to print 
								
							   if( ($orientation=='P')||($orientation=='p') )
							     {
									switch($idcard_template)
									  { 
									     case 1:
                                                                             {	//$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_temporary_p1($person_id,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                             }
                                                                             break;
											
									    case 2:
                                                                            {  //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_temporary_p2($person_id,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    default :
                                                                            {    //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_temporary_p1($person_id,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    
									 }
								 }
							   elseif( ($orientation=='L')||($orientation=='l') )
							    {
									 switch($idcard_template)
									  { 
									     case 1:
                                                                             {	//$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_temporary_l1($person_id,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                             }
                                                                             break;
											
									    case 2:
                                                                            {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_temporary_l2($person_id,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    case 3:
                                                                            {   //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_temporary_l3($person_id,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                            }
                                                                            break;
											
									    default :
                                                                                {  //$barcod = $pdf->serializeTCPDFtagParameters(array($person_id, 'C39', '', '', 33, 7, 0.04,array('position'=>'T', 'align' => 'C','stretch' => true,'fitwidth' => false,'cellfitalign' => 'true','border'=>false, 'padding'=>0, 'hpadding' => 0,'vpadding' => 0,'fgcolor'=>array(0,0,0), 'bgcolor'=>array(255,255,255), 'text'=>false, 'font'=>'helvetica', 'fontsize'=>8, 'stretchtext'=>8), 'N'));
											
											$pdf->writeHTML($this->htmlIdcard_temporary_l1($person_id,$shift_id,$room_id,$barcod), true, false, true, false, '');
									 
                                                                                }
                                                                                break;
											
									    
									 }  
									
							   }
									
									
								 
									  
                                              
                                              
                                              }
                                                          
                                            
                                                        //$this->category= 0: elev, 1: prof, 2:emp  

                                                       if( ($this->category== 0)) // && ($this->category!=null) ) 
                                                          { 
								if($this->idShift!='')
								{	$modelshift=Shifts::model()->findByPk($this->idShift);
								  $shift_name = $modelshift->shift_name;
								}
								  
								 if($this->room_id!='')
								{ $modelroom=Rooms::model()->findByPk($this->room_id);
								  $room_name = $modelroom->room_name;
								}
								  
								 if($this->idLevel!='')
								{  $modellevel=Levels::model()->findByPk($this->idLevel);
								  $level_name = $modellevel->level_name;
								}
								  
								 if($this->section_id!='')
								{  $modelsection=Sections::model()->findByPk($this->section_id);
								  $section_name = $modelsection->section_name;
								}
								  
						       if($this->room_id!=null)
								 {
									 $pdf->Output($room_name.'_'.$acad_name.'.pdf', 'D'); 
								 }	 
								elseif($this->idLevel!=null)
								{
									$pdf->Output($level_name.'_'.$acad_name.'.pdf', 'D'); 
								}
								elseif($this->section_id!=null)
								{
									$pdf->Output($section_name.'_'.$acad_name.'.pdf', 'D'); 
								}
								elseif($this->idShift!=null)
								{
									$pdf->Output($shift_name.'_'.$acad_name.'.pdf', 'D'); 
								}
								else
								{
									$pdf->Output($student_name.'_'.$acad_name.'.pdf', 'D'); 
								}
                             								 
							    }
                                                          	  
  
							                                      	
						} 	 
								
                                  /*Parameters
    $name	(string) The name of the file when saved. Note that special characters are removed and blanks characters are replaced with the underscore character.
    $dest	(string) Destination where to send the document. It can take one of the following values:

        I: send the file inline to the browser (default). The plug-in is used if available. The name given by name is used when one selects the "Save as" option on the link generating the PDF.
        D: send to the browser and force a file download with the name given by name.
        F: save to a local server file with the name given by name.
        S: return the document as a string (name is ignored).
        FI: equivalent to F + I option
        FD: equivalent to F + D option
        E: return the document as base64 mime multi-part email attachment (RFC 2045)
*/

								//============================================================+
								// END OF FILE
								//============================================================+		
		
			          
		     }
			
		    
		$this->render('temporary',array(
			'model'=>$model,
		));
	}

	 public function actionUpdatePhoto($id, $label){
            $model = Idcard::model()->findByPk($id); 
            $model->image_name = $label; 
            $model->save(); 
            
			
		}
	     public function actionListephotos(){
            $model = new Idcard('search'); 
            $id_card = Idcard::model()->findAllBySql("SELECT * FROM idcard ORDER BY nom ASC, prenom ASC   ");
            return $this->renderPartial('lisfoto',array('model'=>$model,'id_card'=>$id_card)); 
        }

        //for ajaxrequest
    public function actionUploadPhoto(){
                $model = new Idcard();
				$modelIdcard = new Idcard(); 
				 $modelPersons = new Persons();
				 $is_update=0;
				 $image_name ='';
       
        	   if (!empty($_FILES)) 
				 {
                     $image_name = $_FILES['upload']['name'];
					 
					 $filename = Yii::app()->basePath.'/../documents/photo-Uploads/1/'.str_replace(' ','_',strtolower($image_name));
					  $temp_name = $_FILES['upload']['tmp_name'];
					  
					  $pers_id  = $_FILES['upload']['name'];
					//delete sil la deja
					$file_to_delete = Yii::app()->request->baseUrl.'/documents/photo-Uploads/1/'.$image_name; // str_replace('protected/','',Yii::app()->basePath.'/documents/photo-Uploads/1/'.$image_name);
                      if(file_exists($file_to_delete))
					   { unlink($file_to_delete);
				         
						 $model = $modelIdcard->findByAttributes(array('person_id'=>$pers_id));
						 if($model ==null)
							 $model = new Idcard();
						 else
						   $is_update=1;
					    }
				 /*  */
				   
                    
                    if(move_uploaded_file($temp_name, $filename))
					 { 
                       if($is_update==0) //se pa foto wap ranplase
					   {						   
					   //cheche moun lan nan persons
						$pers = $modelPersons->findByPk($pers_id);
					
						$model->person_id = $pers->id;
						$model->prenom = $pers->first_name;
						$model->nom = $pers->last_name;
						$model->sexe = $pers->gender;
						
						$model->date_ajout = date('Y-m-d h:m:s');
                        // $extension = strtolower($this->getExtension($_FILES['upload']['name']));
                         //$model->label_image = str_replace($extension,'',strtolower($_FILES['upload']['name']));// json_encode($_FILES['upload']); //
                         $model->image_name = $_FILES['upload']['name']; 
                         
						 if($model->save())
						 {
							 //delete fiche ak ansyen non
							 if( ($pers->image!=null)&&($pers->image!=$model->image_name) )
							 {
							  $file_to_delete = Yii::app()->request->baseUrl.'/documents/photo-Uploads/1/'.$pers->image; // $file_to_delete =  str_replace('protected/','',Yii::app()->basePath.'/documents/photo-Uploads/1/'.$pers->image);
							 if(file_exists($file_to_delete))
							    unlink($file_to_delete);
							  
							 } 
							 
							 $pers->image = strtolower($model->image_name);
							 $pers->save();
						     
						 } 
					   }
					  elseif($is_update==1)  //ranplase foto
					  {
						  //cheche moun lan nan persons
						$pers = $modelPersons->findByPk($pers_id);
					
						$model->prenom = $pers->first_name;
						$model->nom = $pers->last_name;
						$model->sexe = $pers->gender;
						
						// $extension = strtolower($this->getExtension($_FILES['upload']['name']));
                         //$model->label_image = str_replace($extension,'',strtolower($_FILES['upload']['name']));// json_encode($_FILES['upload']); //
                         
						 $model->save();
						 
					  }
						 
                      }
                 }

        }
		


       public function getExtension($str) {
            $i = strrpos($str,".");
            if (!$i) { return ""; }
            $l = strlen($str) - $i;
            $ext = substr($str,$i+1,$l);
            return $ext;
        }
    		
	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Idcard('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Idcard']))
			$model->attributes=$_GET['Idcard'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Idcard the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Idcard::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Idcard $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='idcard-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
	
	
	 //xxxxxxxxxxxxxxx  SHIFT xxxxxxxxxxxxxxxxxxx
	//************************  loadShift ******************************/
	public function loadShift()
	{    $modelShift= new Shifts();
	
	     
           $code= array();
		   
		  $modelPersonShift=$modelShift->findAll();
            $code[null]= Yii::t('app','-- Select --');
		    foreach($modelPersonShift as $shift){
			    $code[$shift->id]= $shift->shift_name;
		           
		      }
		   
		return $code;
         
	}
   //************************  getShift($id) ******************************/
   public function getShift($id)
	{
		 
		 //$shift = new Shifts;
		$shift=Shifts::model()->findByPk($id);
        
			
		      if(isset($shift))
				return $shift->shift_name;
		
	}
	
	//************************  getShiftByStudentId($id, $acad) ******************************/
	public function getShiftByStudentId($id, $acad)
	{
		 
		 
		$idRoom= $this->getRoomByStudentId($id, $acad);
	 if(isset($idRoom)){ 
		
		$modelRoom=new Rooms;
		$idShift = $modelRoom->find(array('select'=>'shift',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$shift = new Shifts;
         if(isset($idShift)){ 
				$result=$shift->find(array('select'=>'id,shift_name',
											 'condition'=>'id=:shiftID',
											 'params'=>array(':shiftID'=>$idShift->shift),
									   ));
				   if(isset($result))
						return $result;
					else
					   return null;
			}
			else
		       return null;	
		}
		else
		  return null;
		
	}
	        //xxxxxxxxxxxxxxx  SECTION  xxxxxxxxxxxxxxxxxxx
	
	//************************  loadSectionByIdShift ******************************/
	public function loadSectionByIdShift($idShift)
	{     
	     
		 
		  $code= array();
          $code[null]= Yii::t('app','-- Select --');
	      $modelRoom= new Rooms();
	      $section_id=$modelRoom->findAll(array('alias'=>'r',
	                                 'join'=>'left join levels l on(l.id=r.level)',
	                                 'select'=>'l.section',
                                     'condition'=>'r.shift=:shiftID',
                                     'params'=>array(':shiftID'=>$idShift),
                               ));
			if(isset($section_id))
			 {  
			    foreach($section_id as $i){			   
					  $modelSection= new Sections();
					   
					  $section=$modelSection->findAll(array('select'=>'id,section_name',
												 'condition'=>'id=:sectionID',
												 'params'=>array(':sectionID'=>$i->section),
										   ));
						
						 if(isset($section)){
						     foreach($section as $s)
							    $code[$s->id]= $s->section_name;
						   }	   
							   }						 }
		   
		return $code;
		$this->section_id=null;
         
	}
	
	//************************  loadSection ******************************/
	public function loadSection()
	{    $modelSection= new Sections();
	
	     
		 
           $code= array();
		   
		  $modelPersonSection=$modelSection->findAll();
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonSection))
			 {  foreach($modelPersonSection as $section){
			        $code[$section->id]= $section->section_name;
		           
		           }
			 }
		   
		return $code;
         
	}
	
//************************  loadSectionByLevel ******************************/
	public function loadSectionByLevel($levelID)
	{    $modelSection= new Sections();
	
	    
		 
           $code= array();
		   
		  $modelPersonSection=$modelSection->findAll(array('alias'=>'s',
		  										 'select'=>'s.id, s.section_name',
												 'join'=>'left join levels l on(l.section=s.id) ',
												 'condition'=>'l.id=:levelID',
												 'params'=>array(':levelID'=>$levelID),
										   ) );
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonSection))
			 {  foreach($modelPersonSection as $section){
			        $code[$section->id]= $section->section_name;
		           
		           }
			 }
		   
		return $code;
         
	}

   //************************  getSection($id) ******************************/
   public function getSection($id)
	{
		 
		 //$section = new Sections;
		$section=Sections::model()->findByPk($id);
        
			
		       if(isset($section))
				return $section->section_name;
		
	}
	
	//************************  getSectionByStudentId($id, $acad) ******************************/
	public function getSectionByStudentId($id, $acad)
	{
		
		 
		$idRoom= $this->getRoomByStudentId($id, $acad);
		
	  if(isset($idRoom)){	
		$modelRoom=new Rooms;
		$idSection = $modelRoom->find(array('alias'=>'r',
		                             'join'=>'left join levels l on(l.id=r.level)',
		                             'select'=>'l.section',
                                     'condition'=>'r.id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$section = new Sections;
        if(isset($idSection)){
		     $result=$section->find(array('select'=>'id,section_name',
                                     'condition'=>'id=:sectionID',
                                     'params'=>array(':sectionID'=>$idSection->section),
                               ));
		      if(isset($result))
				 return $result;
			  else
			     return null;
				 
	  	 }
		else
		   return null;
				
				
		}
	else
      return null;	
		
	}
             //xxxxxxxxxxxxxxx  LEVEL xxxxxxxxxxxxxxxxxxx
		//************************  loadLevelByIdShiftSectionId  ******************************/
	public function loadLevelByIdShiftSectionId($idShift,$section_id)
	{    
       	  
		  $code= array();
          $code[null]= Yii::t('app','-- Select --');
	      $modelRoom= new Rooms();
	      $level_id=$modelRoom->findAll(array('alias'=>'r',
	                                 'join'=>'left join levels l on(l.id=r.level)',
	                                 'select'=>'r.level',
                                     'condition'=>'r.shift=:shiftID AND l.section=:sectionID',
                                     'params'=>array(':shiftID'=>$idShift, ':sectionID'=>$section_id),
                               ));
			if(isset($level_id))
			 {  
			    foreach($level_id as $i){			   
					  $modelLevel= new Levels();
					   
					  $level=$modelLevel->findAll(array('select'=>'id,level_name',
												 'condition'=>'id=:levelID',
												 'params'=>array(':levelID'=>$i->level),
										   ));
						
					 if(isset($level)){
						  foreach($level as $l)
						       $code[$l->id]= $l->level_name;
					    }  
							   }						 
		    
						  }	
			
		return $code;
         
	}
	
	//************************  loadLevel ******************************/
	public function loadLevel()
	{    
	    $modelLevel= new Levels();
		
		 
           $code= array();
		   
		  $modelPersonLevel=$modelLevel->findAll();
            $code[null]= Yii::t('app','-- Select --');
		    foreach($modelPersonLevel as $level){
			    $code[$level->id]= $level->level_name;
		           
		      }
		   
		return $code;
         
	}
	
	//************************  loadLevelBySection ******************************/
	public function loadLevelBySection($idSection)
	{    
	    $modelLevel= new Levels();
		
		 
           $code= array();
		   
		  $modelPersonLevel=$modelLevel->findAll(array('alias'=>'l',
		  										 'select'=>'l.id, l.level_name',
												 'condition'=>'l.section=:sectionID',
												 'params'=>array(':sectionID'=>$idSection),
										   ) );
            $code[null]= Yii::t('app','-- Select --');
		    foreach($modelPersonLevel as $level){
			    $code[$level->id]= $level->level_name;
		           
		      }
		   
		return $code;
         
	}

   //************************  getLevel($id) ******************************/
   public function getLevel($id)
	{
		$level = new Levels;
		
		 
		$level=Levels::model()->findByPk($id);
        
			
		    if(isset($level))
				return $level->level_name;
		
	}
	
	//************************  getLevelByStudentId($id,$acad) ******************************/
	public function getLevelByStudentId($id, $acad)
	{
		
		 
		$idRoom= $this->getRoomByStudentId($id, $acad);
		
	  if(isset($idRoom)){	
		$modelRoom=new Rooms;
		$idLevel = $modelRoom->find(array('select'=>'level',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$level = new Levels;
        if(isset($idLevel)){
		   $result=$level->find(array('select'=>'id,level_name',
                                     'condition'=>'id=:levelID',
                                     'params'=>array(':levelID'=>$idLevel->level),
                               ));
		       if(isset($result))
				    return $result;
			    else
				    return null;
				
		   }
		  else
		     return null;
				
		}
	  else
	      return null;
		
	}
	
//xxxxxxxxxxxxxxx  ROOM xxxxxxxxxxxxxxxxxxx

	//************************  loadRoomByIdShiftSectionLevel ******************************/
	public function loadRoomByIdShiftSectionLevel($shift,$section,$idLevel)
	{    
	      $modelRoom= new Rooms();
		  
		 
           $code= array();
		   
		  $modelPersonRoom=$modelRoom->findAll(array('alias'=>'r',
		                             'join'=>'left join levels l on(l.id=r.level)',
		                             'select'=>'r.id,r.room_name',
                                     'condition'=>'r.shift=:idShift AND l.section=:idSection AND r.level=:levelID ',
                                     'params'=>array(':idShift'=>$shift,':idSection'=>$section,':levelID'=>$idLevel),
                               ));
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonRoom))
			 {  
			    foreach($modelPersonRoom as $room){
			        $code[$room->id]= $room->room_name;
		           
		           }
			 }
		   
		return $code;
         
	}
	
	
	//************************  loadRoom ******************************/
	public function loadRoom()
	{    $modelRoom= new Rooms();
	     
		 
		 
           $code= array();
		   
		  $modelPersonRoom=$modelRoom->findAll();
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonRoom))
			 {  foreach($modelPersonRoom as $room){
			        $code[$room->id]= $room->room_name;
		           
		           }
			 }
		   
		return $code;
         
	}
   //************************  getRoom($id) ******************************/
   public function getRoom($id)
	{
		$room = new Rooms;
		
		
		 
		 
		$room=Rooms::model()->findByPk($id);
        
			
		
		    if(isset($room))
				return $room->room_name;
		
	}
	
	//************************  getRoomByStudentId($id,$acad) ******************************/
	public function getRoomByStudentId($id,$acad)
	{
		$modelRoomH=new RoomHasPerson;
		
		
		 
		$idRoom = $modelRoomH->find(array('select'=>'room',
                                     'condition'=>'students=:studID AND academic_year=:acad',
                                     'params'=>array(':studID'=>$id,':acad'=>$acad),
                               ));
		$room = new Rooms;
      if(isset($idRoom)){           
		   $result=$room->find(array('select'=>'id,room_name',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->room),
                               ));
			
						   

				if(isset($result))			   
					return $result;
				else
				return null;	
				
		  }
		  else
		    return null;
		
	}
		
	
	
//PORTRAIT MODEL 1	
public function htmlIdcard_p1($idcard,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                   //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                            //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          $modelIdcard=Idcard::model()->findByPk($idcard);
		  $student = $modelIdcard->person_id;
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  $pers_foto  = $modelStudent->image;
		  $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
                   
		  
	if($modelStudent->is_student==1)
	  {		
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
                     
		   }
		   
	  }
         else
         {
            
                                                                             
                //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher

                $employee_or_teacher = Persons::model()->isEmployeeOrTeacher($student, $acad_sess);

                if( ($employee_or_teacher==0)||($employee_or_teacher==2) ) //employee
                  {
                    $title=PersonsHasTitles::model()->findByAttributes(array('persons_id'=>$student,'academic_year'=>$acad_sess));
                    if($title!=null)
                      {
                        $modelTitle = Titles::model()->findByPk($title->titles_id);
                        
                        $title_name= $modelTitle->title_name;
                      }

                  }
                elseif($employee_or_teacher==1)//teacher
                  {
                    $title_name=Yii::t('app','Teacher');
                  }
                elseif($employee_or_teacher==-1) //none
                  {
                    $title_name= Yii::t('app','N/A');
                  }
                  
         }
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:164px;
		
		 
	   }

.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 23px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {   cellpadding:0px;
	          border-right:1px solid $line_color; 
			  width:166px;
		      font-size: 10px;
	
        }
		
.idcHeader	{ 
	           font-size: 10px;
			   width:203px;
	
          }   
		  
.logo {
	    
		padding:0px;
		height:50px; 
		width:60px;
		
	  }

.schoolName {
	         text-align:center;
			 font-size:12px; 
			 font-weight:bold;
			 text-transform: capitalize; 
			 
         }	  
.schoolDevise {
	            text-align:center;
			     width:80%;
				 font-style:italic;
				 font-size: 7px;
				
           }

.foto {
	      border:1px solid #e2e0e0;   
		  width:110px; 
		  height:110px;
		 border-radius: 20px;
                object-fit: contain;
          
	}		   
.tablefoto {
	
	width:340px; 
	height:100px;
}

.tocenterfoto {
	width:27px;
	height:50px; 
}

.tablebarcod {
	
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:center; 
		 font-size:10px; 
       }	   
.barc {
	 
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	   
	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
	 
	$html .='<span class="shspace"></span><table >
			  <tr>
			    <td class="firstLayer">
                </td><td class="secondLayer">
				<table class="content">
				<tr>
						<td class="td"><span class="shspace"></span><table class="idcHeader">
								  <tr>
									<td class="logo"><img src="'.$path_logo.'" /></td> 
								     <td class="schoolName">';
                                                                            
                               if($school_name=='Collège Canado-Haitien')
                                   $school_name= 'Collège <br/> Canado-Haitien';
                                                    
                            $html .= $school_name.'<p class="schoolDevise">'.$devise_school.'</p></td>
								  </tr>
								 </table>
						</td>
					  </tr>
					  
					  <tr>
						<td ><table class="tablefoto">
								  <tr>
									<td class="tocenterfoto">
                </td><td ><img src="'.$path_foto.'"  class="foto"></td><td class="tocenterfoto"></td> 
								   </tr>
								 </table>
						 </td>
					 </tr>
					 
					 <tr>
						<td>
							<br/><span class="text_s">'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
							           if($modelStudent->is_student==1)
	                                    {	
                                                $html .='<span class="text_s">'.$room_name.'</span>';
                                            if($display_code_card==1)
                                                $html .='<br/><span class="text_s">'.$pers_cod.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='<span class="text_s">'.$title_name.'</span><br/><span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
               $html .='</td>
					 </tr> 
					
					  <tr>
						<td ><span class="shspace"></span><table class="tablebarcod">
								  <tr>
									<td class="tocenterbarcod">
                                    </td><td class="signature"><tcpdf method="write1DBarcode" params="'.$bc.'"/></td><td class="tocenterbarcod"></td>
								   </tr>
								 </table><span class="text_s">[ '.$acad_name.' ]</span> 
								 </td>
					 </tr>
					 <tr>
						<td><span class="foot"><br/>'.$school_site_web.'<br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
					 </tr>

					  
					</table>
                </td>
				
                <td class="firstLayer">
                </td>				
		      </tr>
	          </table>
			  <div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}public function htmlIdcard_temporary_p1($student,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                   //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                            //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          	  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  //$pers_foto  = $modelStudent->image;
		  $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
                   
		  
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
                     
		   }
		   
	
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:164px;
		
		 
	   }

.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 23px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {   cellpadding:0px;
	          border-right:1px solid $line_color; 
			  width:166px;
		      font-size: 10px;
	
        }
		
.idcHeader	{ 
	           font-size: 10px;
			   width:203px;
	
          }   
		  
.logo {
	    
		padding:0px;
		height:50px; 
		width:60px;
		
	  }

.schoolName {
	         text-align:center;
			 font-size:12px; 
			 font-weight:bold;
			 text-transform: capitalize; 
			 
         }	  
.schoolDevise {
	            text-align:center;
			     width:80%;
				 font-style:italic;
				 font-size: 7px;
				
           }

.foto {
	      border:1px solid #e2e0e0;   
		  width:110px; 
		  height:110px;
		 border-radius: 20px;
                object-fit: contain;
          
	}		   
.tablefoto {
	
	width:340px; 
	height:100px;
}

.tocenterfoto {
	width:27px;
	height:50px; 
}

.tablebarcod {
	
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:center; 
		 font-size:10px; 
       }	   
.barc {
	 
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	   
	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/css/images/no_pic.png";  //$path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
	 
	$html .='<span class="shspace"></span><table >
			  <tr>
			    <td class="firstLayer">
                </td><td class="secondLayer">
				<table class="content">
				<tr>
						<td class="td"><span class="shspace"></span><table class="idcHeader">
								  <tr>
									<td class="logo"><img src="'.$path_logo.'" /></td> 
								     <td class="schoolName">';
                                                                            
                               if($school_name=='Collège Canado-Haitien')
                                   $school_name= 'Collège <br/> Canado-Haitien';
                                                    
                            $html .= $school_name.'<p class="schoolDevise">'.$devise_school.'</p></td>
								  </tr>
								 </table>
						</td>
					  </tr>
					  
					  <tr>
						<td ><table class="tablefoto">
								  <tr>
									<td class="tocenterfoto">
                </td><td ><img src="'.$path_foto.'"  class="foto"></td><td class="tocenterfoto"></td> 
								   </tr>
								 </table>
						 </td>
					 </tr>
					 
					 <tr>
						<td>
							<br/><span class="text_s">'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
					if($modelStudent->is_student==1)
	                                    {	
                                                $html .='<span class="text_s">'.$room_name.'</span>';
                                            if($display_code_card==1)
                                                $html .='<br/><span class="text_s">'.$pers_cod.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='<span class="text_s">'.$title_name.'</span><br/><span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
               $html .='</td>
					 </tr> 
					
					  <tr>
						<td ><span class="shspace"></span><table class="tablebarcod">
								  <tr>
									<td class="tocenterbarcod">
                                    </td><td class="signature"><tcpdf method="write1DBarcode" params="'.$bc.'"/></td><td class="tocenterbarcod"></td>
								   </tr>
								 </table><span class="text_s">[ '.$acad_name.' ]</span> 
								 </td>
					 </tr>
					 <tr>
						<td><span class="foot"><br/>'.$school_site_web.'<br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
					 </tr>

					  
					</table>
                </td>
				
                <td class="firstLayer">
                </td>				
		      </tr>
	          </table>
			  <div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}			
	
//PORTRAIT MODEL 2
public function htmlIdcard_p2($idcard,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                   //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                             //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          $modelIdcard=Idcard::model()->findByPk($idcard);
		  $student = $modelIdcard->person_id;
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  $pers_foto  = $modelStudent->image;
		  $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
                   
		  
	if($modelStudent->is_student==1)
	  {		
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
                     
		   }
		   
	  }
         else
         {
            
                                                                             
                //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher

                $employee_or_teacher = Persons::model()->isEmployeeOrTeacher($student, $acad_sess);

                if( ($employee_or_teacher==0)||($employee_or_teacher==2) ) //employee
                  {
                    $title=PersonsHasTitles::model()->findByAttributes(array('persons_id'=>$student,'academic_year'=>$acad_sess));
                    if($title!=null)
                      {
                        $modelTitle = Titles::model()->findByPk($title->titles_id);
                        
                        $title_name= $modelTitle->title_name;
                      }

                  }
                elseif($employee_or_teacher==1)//teacher
                  {
                    $title_name=Yii::t('app','Teacher');
                  }
                elseif($employee_or_teacher==-1) //none
                  {
                    $title_name= Yii::t('app','N/A');
                  }
                  
         }
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:164px;
		
		 
	   }

.td {
	   font-size: 10px;
	
    }
                
.bar {
    background-color:$main_color;
            width:14px;
   }

.hbar {
    background-color:$main_color;
            width:100%;
   }
.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 21px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {   cellpadding:0px;
                padding:0px;
	          border:1px solid $main_color; 
			  width:199px;
		      font-size: 10px;
	
        }
		
.idcHeader	{ 
	           font-size: 10px;
			   width:203px;
	
          }   
		  
.logo {
	    
		height:45px; 
		width:55px;
		
	  }

.schoolName {
	         text-align:left;
                width:67%;
                padding-left:10px; 
			 font-size:12px; 
			 font-weight:bold;
			 text-transform: capitalize; 
			 
         }	  
.schoolDevise {
	            text-align:left;
			     width:75%;
				 font-style:italic;
				 font-size: 7px;
				
           }

.foto {
	      border:1px solid #e2e0e0;   
		  width:110px; 
		  height:110px;
		 border-radius: 20px;
                object-fit: contain;
          
	}		   
.tablefoto {
	
	width:340px; 
	height:100px;
}

.tocenterfoto {
	display: flex;
        //align-items: center;
        justify-content: center; 
}

.tablebarcod {
	
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:center; 
		 font-size:10px; 
       }
 .code {
	     text-align:center; 
		 font-size:8px; 
       } 
 .acad {
	     text-align:center; 
		 font-size:8px; 
       }
.barc {
	 
	   height:5px; 
	   font-size:0px;
    }	   
.foot_web {
	    color:#FFF;
                text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	
                
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
	 
	$html .='<span class="shspace"></span><table class="content">
			 <tr>
                              <td class="hbar" >
                              </td>
                              
                          </tr>
                          <tr>
                            <td style="background-color:white;">
			       <table cellpadding="0" >
                                  <tr><td style=" width:65px;"><img class="logo" src="'.$path_logo.'" /></td><td class="schoolName">';
                                                                            
                               if($school_name=='Collège Canado-Haitien')
                                   $school_name= 'Collège<br/>Canado-Haitien';
                                                    
                            $html .= $school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span></td>  
                                  </tr>
                               </table>
                            </td>
			  </tr>
			  <tr>
			    <td style=" text-align:center;"><table cellpadding="1" >
								  <tr>
									<td class="tocenterfoto"><img src="'.$path_foto.'"  class="foto"></td> 
								   </tr>
                                                                   <tr>
									<td style="text-align:center;"><span class="text_s">'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
							           if($modelStudent->is_student==1)
	                                    {	$html .='<span class="text_s">'.$room_name.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='<span class="text_s">'.$title_name.'</span><br/><span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
					          $html .='</td> 
								   </tr>
                                                                   <tr>
									<td style="text-align:center;">'; if($display_code_card==1)
                                                                                                               $html .='<span class="code">'.$pers_cod.'</span>';
                                                                               $html .=' <span class="acad">[ '.$acad_name.' ]</span></td>
								   </tr>
                                                                   <tr>
									<td style="text-align:center; "><span class="signature"><tcpdf method="write1DBarcode" params="'.$bc.'"/></span> </td>
								   </tr>
				   </table>
                                  <table cellpadding="0" >
				             <tr>
						<td class="foot" style="width:100%; text-align:center;" >'.$school_address.'</td>
                                             </tr>          
				   </table>
                            </td>
			 </tr>
                         <tr><td ><table style="width:100%;" ><tr><td class="bar"></td>
				      <td colspan="2" style="width:80%;"><span class="foot">'.$school_phone_number.'</span></td>
                                      </tr>
                                </table >
                             </td>
                          </tr>
  			  <tr>
			    <td >&nbsp;&nbsp;&nbsp;&nbsp;         <table ><tr><td class="bar"></td>
				      <td ></td>
                                      </tr>
                                </table >
                            </td>
  			  </tr>
                          <tr>
                              <td class="hbar" ><span class="foot_web">'.$school_site_web.'</span>
                              </td>
                              
                          </tr>
</table>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}


public function htmlIdcard_temporary_p2($student,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                   //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                             //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  //$pers_foto  = $modelStudent->image;
		  $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
                   
		  
			
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
                     
		   }
		   
	 
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:164px;
		
		 
	   }

.td {
	   font-size: 10px;
	
    }
                
.bar {
    background-color:$main_color;
            width:14px;
   }

.hbar {
    background-color:$main_color;
            width:100%;
   }
.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 21px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {   cellpadding:0px;
                padding:0px;
	          border:1px solid $main_color; 
			  width:199px;
		      font-size: 10px;
	
        }
		
.idcHeader	{ 
	           font-size: 10px;
			   width:203px;
	
          }   
		  
.logo {
	    
		height:45px; 
		width:55px;
		
	  }

.schoolName {
	         text-align:left;
                width:67%;
                padding-left:10px; 
			 font-size:12px; 
			 font-weight:bold;
			 text-transform: capitalize; 
			 
         }	  
.schoolDevise {
	            text-align:left;
			     width:75%;
				 font-style:italic;
				 font-size: 7px;
				
           }

.foto {
	      border:1px solid #e2e0e0;   
		  width:110px; 
		  height:110px;
		 border-radius: 20px;
                object-fit: contain;
          
	}		   
.tablefoto {
	
	width:340px; 
	height:100px;
}

.tocenterfoto {
	display: flex;
        //align-items: center;
        justify-content: center; 
}

.tablebarcod {
	
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:center; 
		 font-size:10px; 
       }
 .code {
	     text-align:center; 
		 font-size:8px; 
       } 
 .acad {
	     text-align:center; 
		 font-size:8px; 
       }
.barc {
	 
	   height:5px; 
	   font-size:0px;
    }	   
.foot_web {
	    color:#FFF;
                text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	
                
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/css/images/no_pic.png";  //$path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
	 
	$html .='<span class="shspace"></span><table class="content">
			 <tr>
                              <td class="hbar" >
                              </td>
                              
                          </tr>
                          <tr>
                            <td style="background-color:white;">
			       <table cellpadding="0" >
                                  <tr><td style=" width:65px;"><img class="logo" src="'.$path_logo.'" /></td><td class="schoolName">';
                                                                            
                               if($school_name=='Collège Canado-Haitien')
                                   $school_name= 'Collège<br/>Canado-Haitien';
                                                    
                            $html .= $school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span></td>  
                                  </tr>
                               </table>
                            </td>
			  </tr>
			  <tr>
			    <td style=" text-align:center;"><table cellpadding="1" >
								  <tr>
									<td class="tocenterfoto"><img src="'.$path_foto.'"  class="foto"></td> 
								   </tr>
                                                                   <tr>
									<td style="text-align:center;"><span class="text_s">'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
							           if($modelStudent->is_student==1)
	                                    {	$html .='<span class="text_s">'.$room_name.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='<span class="text_s">'.$title_name.'</span><br/><span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
					          $html .='</td> 
								   </tr>
                                                                   <tr>
									<td style="text-align:center;">'; if($display_code_card==1)
                                                                                                               $html .='<span class="code">'.$pers_cod.'</span>';
                                                                               $html .=' <span class="acad">[ '.$acad_name.' ]</span></td>
								   </tr>
                                                                   <tr>
									<td style="text-align:center; "><span class="signature"><tcpdf method="write1DBarcode" params="'.$bc.'"/></span> </td>
								   </tr>
				   </table>
                                  <table cellpadding="0" >
				             <tr>
						<td class="foot" style="width:100%; text-align:center;" >'.$school_address.'</td>
                                             </tr>          
				   </table>
                            </td>
			 </tr>
                         <tr><td ><table style="width:100%;" ><tr><td class="bar"></td>
				      <td colspan="2" style="width:80%;"><span class="foot">'.$school_phone_number.'</span></td>
                                      </tr>
                                </table >
                             </td>
                          </tr>
  			  <tr>
			    <td >&nbsp;&nbsp;&nbsp;&nbsp;         <table ><tr><td class="bar"></td>
				      <td ></td>
                                      </tr>
                                </table >
                            </td>
  			  </tr>
                          <tr>
                              <td class="hbar" ><span class="foot_web">'.$school_site_web.'</span>
                              </td>
                              
                          </tr>
</table>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}



//LANDSCAPE MODEL 1
public function htmlIdcard_l1($idcard,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                           //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                            
                             //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
                                                        $signature_au = infoGeneralConfig('signature_au');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          $modelIdcard=Idcard::model()->findByPk($idcard);
		  $student = $modelIdcard->person_id;
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  $pers_foto  = $modelStudent->image;
                   $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
		  
		  
	if($modelStudent->is_student==1)
	  {		
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
		   }
		   
	  }
         else
         {
            
                                                                             
                //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher

                $employee_or_teacher = Persons::model()->isEmployeeOrTeacher($student, $acad_sess);

                if( ($employee_or_teacher==0)||($employee_or_teacher==2) ) //employee
                  {
                    $title=PersonsHasTitles::model()->findByAttributes(array('persons_id'=>$student,'academic_year'=>$acad_sess));
                    if($title!=null)
                      { $modelTitle = Titles::model()->findByPk($title->titles_id);
                        
                        $title_name= $modelTitle->title_name;
                      
                      }

                  }
                elseif($employee_or_teacher==1)//teacher
                  {
                    $title_name=Yii::t('app','Teacher');
                  }
                elseif($employee_or_teacher==-1) //none
                  {
                    $title_name= Yii::t('app','N/A');
                  }
                  
         }
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:274px;
		
		 
	   }

.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 24px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {      cellpadding:0px;
	          border-right:1px solid $line_color; 
                width:276px;
		      font-size: 10px;
	
        }
		
.idcHeader	{ 
	           font-size: 10px;
			   width:428px;
	
          }   
		  
.logo {
	    
		padding:0px;
		height:50px; 
		width:60px;
		
	  }

.schoolName {
	         text-align:center;
			 font-size:14px; 
			 font-weight:bold;
			 text-transform: capitalize; //uppercase; //|uppercase|lowercase|initial|inherit|none;
			 
         }

.info_school {
	    text-align:center; 
		font-size: 7px;
		 font-weight:normal;
		font-style: italic;
      }	
	  
.schoolDevise {
	            text-align:center;
			    
				 font-style:italic;
				 font-size: 8px;
				
           }

.foto {
	      border:1px solid #e2e0e0; //#AAA9A9;   
		  width:105px; 
		  height:105px;
		 border-radius: 20px;
                object-fit: contain;
          
	}

.tdbofoto {
	      width:171px; 
		  
          
	}

	
.tablefoto {
	 
	width:202px; 
	height:70px;
}

.tocenterfoto {
	width:27px;
	height:50px; 
}

.tablebarcod {
	
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:left; 
		 font-size:10px; 
       }	   
.barc {
	 
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	  
  
.signature_au{
	    text-align:left; 
		font-size: 6px;
		font-style: italic;
      }	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
         $path_siyati=Yii::app()->baseUrl."/css/images/signature_au.png"; 
	 
	$html .='<span class="shspace"></span><table >
			  <tr>
			    <td class="firstLayer">
                </td><td class="secondLayer">
				<table class="content">
				<tr>
						<td class="td"><span class="shspace"></span><table class="idcHeader">
								  <tr>
									<td class="logo"><img src="'.$path_logo.'" /></td> 
								        <td class="schoolName"><!-- Collège<br/>Canado-Haitien -->'.$school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span>
									 <span class="info_school"><br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
								  </tr>
								 </table>
						</td>
					  </tr>
					  <tr>
						<td ><table class="tablefoto">
								  <tr>
									<td class="tdbofoto" ><span class="text_s">';
                                                                      if($signature_au==0)
                                                                          $html .='<br/>';
                                                                      
                                                                          $html .='&nbsp;&nbsp;'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
                                      if($modelStudent->is_student==1)
	                                    {	$html .='&nbsp;&nbsp;<span class="text_s">'.$room_name.'</span>';
                                                          if($display_code_card==1)
                                                              $html .='<br/>&nbsp;&nbsp;<span class="text_s">'.$pers_cod.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='&nbsp;&nbsp;<span class="text_s">'.$title_name.'</span><br/>&nbsp;&nbsp;<span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
               $html .='<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<tcpdf method="write1DBarcode" params="'.$bc.'"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text_s">[ '.$acad_name.' ]</span>';
               if($signature_au==1)
                   $html .='<br/>&nbsp;&nbsp;<span class="signature_au">Signature autorisée:</span><img src="'.$path_siyati.'" />';
               $html .='</td><td ><img src="'.$path_foto.'"  class="foto"></td> 
								   </tr>
								 </table>
                                                                 
						 </td>
					 </tr>
					   <tr>
						<td style="font-size:10px;" ><span class="foot">'.$school_site_web.'</span></td>
					 </tr>
                  </table>
                </td>
				
                <td class="firstLayer">
                </td>				
		      </tr>
	          </table>
			  <div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}			
		


public function htmlIdcard_temporary_l1($student,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                           //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                            
                             //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
                                                        $signature_au = infoGeneralConfig('signature_au');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
         
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  //$pers_foto  = $modelStudent->image;
                   $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
		  
		  
			
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
		   }
		   
	 
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:274px;
		
		 
	   }

.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 24px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {      cellpadding:0px;
	          border-right:1px solid $line_color; 
                width:276px;
		      font-size: 10px;
	
        }
		
.idcHeader	{ 
	           font-size: 10px;
			   width:428px;
	
          }   
		  
.logo {
	    
		padding:0px;
		height:50px; 
		width:60px;
		
	  }

.schoolName {
	         text-align:center;
			 font-size:14px; 
			 font-weight:bold;
			 text-transform: capitalize; //uppercase; //|uppercase|lowercase|initial|inherit|none;
			 
         }

.info_school {
	    text-align:center; 
		font-size: 7px;
		 font-weight:normal;
		font-style: italic;
      }	
	  
.schoolDevise {
	            text-align:center;
			    
				 font-style:italic;
				 font-size: 8px;
				
           }

.foto {
	      border:1px solid #e2e0e0; //#AAA9A9;   
		  width:105px; 
		  height:105px;
		 border-radius: 20px;
                object-fit: contain;
          
	}

.tdbofoto {
	      width:171px; 
		  
          
	}

	
.tablefoto {
	 
	width:202px; 
	height:70px;
}

.tocenterfoto {
	width:27px;
	height:50px; 
}

.tablebarcod {
	
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:left; 
		 font-size:10px; 
       }	   
.barc {
	 
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	  
  
.signature_au{
	    text-align:left; 
		font-size: 6px;
		font-style: italic;
      }	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/css/images/no_pic.png";   //$path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
         $path_siyati=Yii::app()->baseUrl."/css/images/signature_au.png"; 
	 
	$html .='<span class="shspace"></span><table >
			  <tr>
			    <td class="firstLayer">
                </td><td class="secondLayer">
				<table class="content">
				<tr>
						<td class="td"><span class="shspace"></span><table class="idcHeader">
								  <tr>
									<td class="logo"><img src="'.$path_logo.'" /></td> 
								        <td class="schoolName"><!-- Collège<br/>Canado-Haitien -->'.$school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span>
									 <span class="info_school"><br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
								  </tr>
								 </table>
						</td>
					  </tr>
					  <tr>
						<td ><table class="tablefoto">
								  <tr>
									<td class="tdbofoto" ><span class="text_s">';
                                                                      if($signature_au==0)
                                                                          $html .='<br/>';
                                                                      
                                                                          $html .='&nbsp;&nbsp;'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
                                      if($modelStudent->is_student==1)
	                                    {	$html .='&nbsp;&nbsp;<span class="text_s">'.$room_name.'</span>';
                                                          if($display_code_card==1)
                                                              $html .='<br/>&nbsp;&nbsp;<span class="text_s">'.$pers_cod.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='&nbsp;&nbsp;<span class="text_s">'.$title_name.'</span><br/>&nbsp;&nbsp;<span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
               $html .='<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<tcpdf method="write1DBarcode" params="'.$bc.'"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text_s">[ '.$acad_name.' ]</span>';
               if($signature_au==1)
                   $html .='<br/>&nbsp;&nbsp;<span class="signature_au">Signature autorisée:</span><img src="'.$path_siyati.'" />';
               $html .='</td><td ><img src="'.$path_foto.'"  class="foto"></td> 
								   </tr>
								 </table>
                                                                 
						 </td>
					 </tr>
					   <tr>
						<td style="font-size:10px;" ><span class="foot">'.$school_site_web.'</span></td>
					 </tr>
                  </table>
                </td>
				
                <td class="firstLayer">
                </td>				
		      </tr>
	          </table>
			  <div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}			
		


//LANDSCAPE MODEL 2	
public function htmlIdcard_l2($idcard,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                         //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                             //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          $modelIdcard=Idcard::model()->findByPk($idcard);
		  $student = $modelIdcard->person_id;
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  $pers_foto  = $modelStudent->image;
                   $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
		  
		  
	if($modelStudent->is_student==1)
	  {		
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
		   }
		   
	  }
         else
         {
            
                                                                             
                //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher

                $employee_or_teacher = Persons::model()->isEmployeeOrTeacher($student, $acad_sess);

                if( ($employee_or_teacher==0)||($employee_or_teacher==2) ) //employee
                  {
                    $title=PersonsHasTitles::model()->findByAttributes(array('persons_id'=>$student,'academic_year'=>$acad_sess));
                    if($title!=null)
                      {  $modelTitle = Titles::model()->findByPk($title->titles_id);
                        
                        $title_name= $modelTitle->title_name;
                      }

                  }
                elseif($employee_or_teacher==1)//teacher
                  {
                    $title_name=Yii::t('app','Teacher');
                  }
                elseif($employee_or_teacher==-1) //none
                  {
                    $title_name= Yii::t('app','N/A');
                  }
                  
         }
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:274px;
		
		 
	   }
                
.logo_img {
		background-color:#FFF; 
		 
	   }
                
.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 24px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {
	          border-right:1px solid $line_color; 
			  width:309px;
		      font-size: 10px;
	
        }
		
.idcHeader	{  
	           font-size: 10px;
			   width:428px;
	
          }   
		  
.logo {
	    /* border:1px solid green; */
		padding:0px;
		height:50px; 
		width:60px;
                
		
	  }

.schoolName {
	         background-color:white;
			 border-right:2px solid $main_color;
			 text-align:center;
			 font-size:14px; 
			 font-weight:bold;
			 text-transform: capitalize; //uppercase; //|uppercase|lowercase|initial|inherit|none;
			 border-top:5px solid white;
			 border-bottom:14px solid white;
			 
         }

.vbar {
	         background-color:$main_color; 
		     width:4px;
		  
			
			 
         }

.info_school {
	    text-align:center; 
		font-size: 7px;
		 font-weight:normal;
		font-style: italic;
      }	
	  
.schoolDevise {
	            text-align:center;
			    /* font-weight:bold; */
				 font-style:italic;
				 font-size: 8px;
				/* border:1px solid green; */
           }

.foto {
	      border:1px solid #e2e0e0; //#AAA9A9;   
		  width:105px; 
		  height:105px;
		 border-radius: 20px;
                object-fit: contain;
          
	}

.tdbofoto {
	      width:200px; 
		  
          
	}

	
.tablefoto {
	/* border:1px solid blue;   */
	width:280px; 
	height:70px;
}

.tocenterfoto {
	width:31px;
	height:50px; 
}

.tablebarcod {
	/* border:1px solid blue;   */
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:left; 
		 font-size:10px; 
       }	   
.barc {
	 /*  border:1px solid brown; */
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	     width:100%;
		 background-color:$main_color;
		 color: white;
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	  
  
	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
	 
	$html .='<span class="shspace"></span><table >
			  <tr>
			    <td class="firstLayer"><span class="shspace"></span><table class="idcHeader">
								  <tr>
									<td class="logo"><img src="'.$path_logo.'" /></td> 
								     <td class="schoolName"><br/>'.$school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span>
									 <span class="info_school"><br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
								  </tr>
								 </table>
                </td>
				</tr>
				<tr><td><table class="content">
					  <tr>
						<td ><table class="tablefoto">
								  <tr>
									<td class="tdbofoto" ><span class="text_s"><br/>&nbsp;&nbsp;'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
                                      if($modelStudent->is_student==1)
	                                    {	$html .='&nbsp;&nbsp;<span class="text_s">'.$room_name.'</span>';
                                                 if($display_code_card==1)
                                                     $html .='<br/>&nbsp;&nbsp;<span class="text_s">'.$pers_cod.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='&nbsp;&nbsp;<span class="text_s">'.$title_name.'</span><br/>&nbsp;&nbsp;<span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
               $html .='<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<tcpdf method="write1DBarcode" params="'.$bc.'"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text_s">[ '.$acad_name.' ]</span></td><td><img src="'.$path_foto.'"  class="foto"></td> 
								   </tr>
								 </table>
						 </td>
					 </tr>
					   <tr>
						<td style="font-size:8px"><br/><div class="foot">'.$school_site_web.'</div></td>
					 </tr>
                  </table>
                </td>
				
                				
		      </tr>
	          </table>
			  <div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}		



public function htmlIdcard_temporary_l2($student,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                         //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
                            
                             //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  //$pers_foto  = $modelStudent->image;
                   $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
		  
			
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
		   }
		   
	 
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$main_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:274px;
		
		 
	   }
                
.logo_img {
		background-color:#FFF; 
		 
	   }
                
.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 24px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {
	          border-right:1px solid $line_color; 
			  width:309px;
		      font-size: 10px;
	
        }
		
.idcHeader	{  
	           font-size: 10px;
			   width:428px;
	
          }   
		  
.logo {
	    /* border:1px solid green; */
		padding:0px;
		height:50px; 
		width:60px;
                
		
	  }

.schoolName {
	         background-color:white;
			 border-right:2px solid $main_color;
			 text-align:center;
			 font-size:14px; 
			 font-weight:bold;
			 text-transform: capitalize; //uppercase; //|uppercase|lowercase|initial|inherit|none;
			 border-top:5px solid white;
			 border-bottom:14px solid white;
			 
         }

.vbar {
	         background-color:$main_color; 
		     width:4px;
		  
			
			 
         }

.info_school {
	    text-align:center; 
		font-size: 7px;
		 font-weight:normal;
		font-style: italic;
      }	
	  
.schoolDevise {
	            text-align:center;
			    /* font-weight:bold; */
				 font-style:italic;
				 font-size: 8px;
				/* border:1px solid green; */
           }

.foto {
	      border:1px solid #e2e0e0; //#AAA9A9;   
		  width:105px; 
		  height:105px;
		 border-radius: 20px;
                object-fit: contain;
          
	}

.tdbofoto {
	      width:200px; 
		  
          
	}

	
.tablefoto {
	/* border:1px solid blue;   */
	width:280px; 
	height:70px;
}

.tocenterfoto {
	width:31px;
	height:50px; 
}

.tablebarcod {
	/* border:1px solid blue;   */
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:left; 
		 font-size:10px; 
       }	   
.barc {
	 /*  border:1px solid brown; */
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	     width:100%;
		 background-color:$main_color;
		 color: white;
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	  
  
	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/css/images/no_pic.png";  //$path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
	 
	$html .='<span class="shspace"></span><table >
			  <tr>
			    <td class="firstLayer"><span class="shspace"></span><table class="idcHeader">
								  <tr>
									<td class="logo"><img src="'.$path_logo.'" /></td> 
								     <td class="schoolName"><br/>'.$school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span>
									 <span class="info_school"><br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
								  </tr>
								 </table>
                </td>
				</tr>
				<tr><td><table class="content">
					  <tr>
						<td ><table class="tablefoto">
								  <tr>
									<td class="tdbofoto" ><span class="text_s"><br/>&nbsp;&nbsp;'.$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
                                      if($modelStudent->is_student==1)
	                                    {	$html .='&nbsp;&nbsp;<span class="text_s">'.$room_name.'</span>';
                                                 if($display_code_card==1)
                                                     $html .='<br/>&nbsp;&nbsp;<span class="text_s">'.$pers_cod.'</span>'; 
									
									    }
                                                                        else
                                                                        {
                                                                            $html .='&nbsp;&nbsp;<span class="text_s">'.$title_name.'</span><br/>&nbsp;&nbsp;<span class="text_s">'.$pers_nif_cin.'</span>';     
                                                                              
                                                                        }
               $html .='<br/><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<tcpdf method="write1DBarcode" params="'.$bc.'"/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="text_s">[ '.$acad_name.' ]</span></td><td><img src="'.$path_foto.'"  class="foto"></td> 
								   </tr>
								 </table>
						 </td>
					 </tr>
					   <tr>
						<td style="font-size:8px"><br/><div class="foot">'.$school_site_web.'</div></td>
					 </tr>
                  </table>
                </td>
				
                				
		      </tr>
	          </table>
			  <div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}		



//LANDSCAPE MODEL 3
public function htmlIdcard_l3($idcard,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                           //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
                                                        
                                                        $top_content_color = infoGeneralConfig('top_content_color');
                                                        $top_top_content_color = infoGeneralConfig('top_top_content_color');
                                                        $bottom_content_color = infoGeneralConfig('bottom_content_color');
                                                        $bottom_bottom_content_color = infoGeneralConfig('bottom_bottom_content_color');
                                                        $left_right_color  = infoGeneralConfig('left_right_color');
                                                        $main_corner_color  = infoGeneralConfig('main_corner_color');
                                                        $inside_corner_color  = infoGeneralConfig('inside_corner_color');
                                                        
                                                        $inside_vertical_corner_color  = infoGeneralConfig('inside_vertical_corner_color');
                                                        $left_right_center_color  = infoGeneralConfig('left_right_center_color');
                                                                
                                                        $signature_au = infoGeneralConfig('signature_au');
                                                        
                                                         //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          $modelIdcard=Idcard::model()->findByPk($idcard);
		  $student = $modelIdcard->person_id;
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  $pers_foto  = $modelStudent->image;
                   $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
		  
		  
	if($modelStudent->is_student==1)
	  {		
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
		   }
		   
	  }
         else
         {
            
                                                                             
                //return 0 when employee, 1 when teacher; return 2 when employee-teacher; return -1 when either employee nor teacher

                $employee_or_teacher = Persons::model()->isEmployeeOrTeacher($student, $acad_sess);

                if( ($employee_or_teacher==0)||($employee_or_teacher==2) ) //employee
                  {
                    $title=PersonsHasTitles::model()->findByAttributes(array('persons_id'=>$student,'academic_year'=>$acad_sess));
                    if($title!=null)
                      { $modelTitle = Titles::model()->findByPk($title->titles_id);
                        
                        $title_name= $modelTitle->title_name;
                      
                      }

                  }
                elseif($employee_or_teacher==1)//teacher
                  {
                    $title_name=Yii::t('app','Teacher');
                  }
                elseif($employee_or_teacher==-1) //none
                  {
                    $title_name= Yii::t('app','N/A');
                  }
                  
         }
			   			
		
		
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$left_right_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:274px;
		
		 
	   }

.td_corner {
	   font-size: 4px;
           width:18px;
	   background-color:$main_corner_color;
    }

.td_corner1 {
	   font-size: 4px;
           width:11px;
	   background-color:$inside_corner_color;
    }
         
.td_top_content {
	   font-size: 4px;
           width:274px;
	   background-color:$top_content_color; 
    }
                
.td_top_content1 {
	   font-size: 4px;
           width:252px;
	   background-color:$top_top_content_color;
    }

.td_bottom_content {
	   font-size: 4px;
           width:274px;
	   background-color:$bottom_content_color; 
    }
 
.td_bottom_content1 {
	   font-size: 4px;
           width:252px;
	   background-color:$bottom_bottom_content_color;
    } 
                
.td_side_l2 {
	   font-size: 4px;
           width:13px;
	   background-color:$inside_vertical_corner_color; 
    }
    
 .td_side_lc2 {
	   font-size: 4px;
           width:13px;
	   background-color:$left_right_color; 
    }
   
 .td_side_l1 {
	   height: 11px;
                font-size: 4px;
           width:5px;
	   background-color:$inside_vertical_corner_color; 
    }

.td_side_lc1 {
	   height: 157px;
                font-size: 4px;
           width:5px;
	   background-color:$left_right_center_color; 
    }

.td_side_r2 {
	   font-size: 4px;
           width:13px;
	   background-color:$inside_vertical_corner_color; 
    }
    
.td_side_rc2 {
	   font-size: 4px;
           width:13px;
	   background-color:$left_right_color; 
    }
    
.td_side_r1 {
	   height: 11px;
                font-size: 4px;
           width:5px;
	   background-color:$inside_vertical_corner_color; 
    }
 
 .td_side_rc1 {
	   height: 157px;
                font-size: 4px;
           width:5px;
	   background-color:$left_right_center_color; 
    }
    
  
                
.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 22px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {      cellpadding:0px;
	          border-right:1px solid $line_color; 
                width:276px;
		      font-size: 10px;
	
        }
		
.idcHeader	{  /* border:1px solid blue; */
	           font-size: 10px;
			   width:428px;
	
          }   
		  
.logo {
	    /* border:1px solid green; */
		padding:0px;
		height:50px; 
		width:60px;
		
	  }

.schoolName {
	         text-align:center;
			 font-size:14px; 
			 font-weight:bold;
			 text-transform: capitalize; //uppercase; //|uppercase|lowercase|initial|inherit|none;
			 
         }

.info_school {
	    text-align:center; 
		font-size: 7px;
		 font-weight:normal;
		font-style: italic;
      }	
	  
.schoolDevise {
	            text-align:center;
			    /* font-weight:bold; */
				 font-style:italic;
				 font-size: 8px;
				/* border:1px solid green; */
           }

.foto {
	      border:1px solid #e2e0e0; //#AAA9A9;   
		  width:105px; 
		  height:105px;
		 border-radius: 20px;
                object-fit: contain;
          
	}

.tdbofoto {
	      width:171px; 
		  
          
	}

	
.tablefoto {
	/* border:1px solid blue;  */ 
	width:192px; 
	height:70px;
}

.tocenterfoto {
	width:27px;
	height:50px; 
}

.tablebarcod {
	/* border:1px solid blue;   */
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:left; 
		 font-size:10px; 
       }	   
.barc {
	 /*  border:1px solid brown; */
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	  
  
.signature_au{
	    text-align:left; 
		font-size: 6px;
		font-style: italic;
      }	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
         $path_siyati=Yii::app()->baseUrl."/css/images/signature_au.png"; 
	 
	$html .='<span class="shspace"></span><table cellpadding="0" style="border:none">
	<tr>
	     <td class="td_corner" rowspan="2"> </td><td class="td_corner1" > </td><td class="td_top_content1"></td><td class="td_corner1" > </td><td class="td_corner" rowspan="2"> </td>
    </tr>
    <tr>
	     <td colspan="3" class="td_top_content"></td>
    </tr>
    <tr>
             <td class="td_side_l1"></td>
             <td class="td_side_lc2" rowspan="3"></td>
             <td class="secondLayer" rowspan="3">
<table class="content" cellpadding="0" >
<tr>
 <td class="td">
  <table class="idcHeader">
   <tr>
   <td class="logo"><img src="'.$path_logo.'" /></td>
       <td class="schoolName"><!-- Collège<br/>Canado-Haitien -->'.$school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span>
                                 <span class="info_school"><br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
   </tr>
   </table>
 </td>
</tr>
<tr>
 <td >
  <table class="tablefoto" >
   <tr>
    <td class="tdbofoto" ><span class="text_s">';
                                                     if($signature_au==0)
                                                         $html .='<br/>';

                                                         $html .=$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
                     if($modelStudent->is_student==1)
                        {	$html .='<span class="text_s">'.$room_name.'</span>';
                                if($display_code_card==1)
                                     $html .='<br/><span class="text_s">'.$pers_cod.'</span>'; 

                                                        }
                                                       else
                                                       {
                                                           $html .='<span class="text_s">'.$title_name.'</span><br/><span class="text_s">'.$pers_nif_cin.'</span>';     

                                                       }
$html .='<br/><br/>&nbsp;&nbsp;&nbsp;<tcpdf method="write1DBarcode" params="'.$bc.'"/>&nbsp;&nbsp;&nbsp;<span class="text_s">[ '.$acad_name.' ]</span>';
if($signature_au==1)
    $html .='<br/><span class="signature_au">Signature autorisée:</span><img src="'.$path_siyati.'" />';
$html .='</td>
    <td ><img src="'.$path_foto.'"  class="foto"></td>
   </tr>
     </table>
    </td>
   </tr>
   <tr>
   <td style="font-size:10px;" ><span class="foot">'.$school_site_web.'</span></td>
   </tr>
  </table>
</td>
<td class="td_side_rc2" rowspan="3"></td>
<td class="td_side_r1"></td>
</tr>
<tr>
   <td class="td_side_lc1" rowspan=""></td>
   <td class="td_side_rc1" rowspan=""></td>
</tr>
<tr>
   <td class="td_side_l1"></td>
   <td class="td_side_r1"></td>
</tr>
<tr>
    <td class="td_corner" rowspan="2"> </td><td class="td_bottom_content"></td><td class="td_corner" rowspan="2"> </td>
</tr>
<tr>
    <td class="td_corner1" > </td><td class="td_bottom_content1"></td><td class="td_corner1" > </td>
</tr>
</table>
<div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}

	
		
public function htmlIdcard_temporary_l3($student,$shift,$room,$bc)
	{
		$this->printing_date = false;
                
                           //Extract school address
				   			$school_name = infoGeneralConfig('school_name');
						//Extract devise school
				   		$devise_school = infoGeneralConfig('devise_school');
							
					    //Extract school address
				   			$school_address = infoGeneralConfig('school_address');
                        //Extract  email address 
                           $school_email_address = infoGeneralConfig('school_email_address');
						//Extract site_web 
                           $school_site_web = infoGeneralConfig('school_site_web');   
						   
                        //Extract Phone Number
                            $school_phone_number = infoGeneralConfig('school_phone_number');
							
							$main_color = infoGeneralConfig('main_color');
							$secondary_color = infoGeneralConfig('secondary_color');
							$balance_color = infoGeneralConfig('balance_color');
							$line_color = infoGeneralConfig('line_color');
                                                        
                                                        $top_content_color = infoGeneralConfig('top_content_color');
                                                        $top_top_content_color = infoGeneralConfig('top_top_content_color');
                                                        $bottom_content_color = infoGeneralConfig('bottom_content_color');
                                                        $bottom_bottom_content_color = infoGeneralConfig('bottom_bottom_content_color');
                                                        $left_right_color  = infoGeneralConfig('left_right_color');
                                                        $main_corner_color  = infoGeneralConfig('main_corner_color');
                                                        $inside_corner_color  = infoGeneralConfig('inside_corner_color');
                                                        
                                                        $inside_vertical_corner_color  = infoGeneralConfig('inside_vertical_corner_color');
                                                        $left_right_center_color  = infoGeneralConfig('left_right_center_color');
                                                                
                                                        $signature_au = infoGeneralConfig('signature_au');
                                                        
                                                         //Extract display_code_card
                            $display_code_card = infoGeneralConfig('display_code_card');
							
					  if($school_site_web=='')
                        $school_site_web =$school_email_address;	

$acad_sess = acad_sess();			
$modelAcademicPeriods=AcademicPeriods::model()->findByPk($acad_sess);
		$acad_name = $modelAcademicPeriods->name_period ;
		
		
          
		  
		  $modelStudent=Persons::model()->findByPk($student);
          $first_name = $modelStudent->first_name ;
		  $last_name = $modelStudent->last_name;
		  $pers_cod  = $modelStudent->id_number;
		  //$pers_foto  = $modelStudent->image;
                   $pers_nif_cin = $modelStudent->nif_cin;
                  
                   $title_name="";
		  
		  
			
		  if(($shift!='')&&($shift!=0))
		   {  $modelshift=Shifts::model()->findByPk($shift);
		      $shift_name = $modelshift->shift_name;
		    }
			
		  if(($room!='')&&($room!=0))
		   {  $modelroom=Rooms::model()->findByPk($room);
                      $room_name = $modelroom->room_name;
		   }
		   
	 	
 							
	$html = <<<EOD
 <style>
	
	
.firstLayer {
		background-color:$left_right_color; 
		width:18px;
	    text-align: center;
		align: center;
		
		 
	   }
	
.secondLayer {
		background-color:$balance_color; 
		width:274px;
		
		 
	   }

.td_corner {
	   font-size: 4px;
           width:18px;
	   background-color:$main_corner_color;
    }

.td_corner1 {
	   font-size: 4px;
           width:11px;
	   background-color:$inside_corner_color;
    }
         
.td_top_content {
	   font-size: 4px;
           width:274px;
	   background-color:$top_content_color; 
    }
                
.td_top_content1 {
	   font-size: 4px;
           width:252px;
	   background-color:$top_top_content_color;
    }

.td_bottom_content {
	   font-size: 4px;
           width:274px;
	   background-color:$bottom_content_color; 
    }
 
.td_bottom_content1 {
	   font-size: 4px;
           width:252px;
	   background-color:$bottom_bottom_content_color;
    } 
                
.td_side_l2 {
	   font-size: 4px;
           width:13px;
	   background-color:$inside_vertical_corner_color; 
    }
    
 .td_side_lc2 {
	   font-size: 4px;
           width:13px;
	   background-color:$left_right_color; 
    }
   
 .td_side_l1 {
	   height: 11px;
                font-size: 4px;
           width:5px;
	   background-color:$inside_vertical_corner_color; 
    }

.td_side_lc1 {
	   height: 157px;
                font-size: 4px;
           width:5px;
	   background-color:$left_right_center_color; 
    }

.td_side_r2 {
	   font-size: 4px;
           width:13px;
	   background-color:$inside_vertical_corner_color; 
    }
    
.td_side_rc2 {
	   font-size: 4px;
           width:13px;
	   background-color:$left_right_color; 
    }
    
.td_side_r1 {
	   height: 11px;
                font-size: 4px;
           width:5px;
	   background-color:$inside_vertical_corner_color; 
    }
 
 .td_side_rc1 {
	   height: 157px;
                font-size: 4px;
           width:5px;
	   background-color:$left_right_center_color; 
    }
    
  
                
.td {
	   font-size: 10px;
	
    }

.hspace {
	      font-size: 5px;
		   text-align:center; 
        }	

.shspace {
	      font-size: 22px;
		  background-color:white; 
		  border:2px solid white;
		   text-align:center; 
        }	

.content {      cellpadding:0px;
	          border-right:1px solid $line_color; 
                width:276px;
		      font-size: 10px;
	
        }
		
.idcHeader	{  /* border:1px solid blue; */
	           font-size: 10px;
			   width:428px;
	
          }   
		  
.logo {
	    /* border:1px solid green; */
		padding:0px;
		height:50px; 
		width:60px;
		
	  }

.schoolName {
	         text-align:center;
			 font-size:14px; 
			 font-weight:bold;
			 text-transform: capitalize; //uppercase; //|uppercase|lowercase|initial|inherit|none;
			 
         }

.info_school {
	    text-align:center; 
		font-size: 7px;
		 font-weight:normal;
		font-style: italic;
      }	
	  
.schoolDevise {
	            text-align:center;
			    /* font-weight:bold; */
				 font-style:italic;
				 font-size: 8px;
				/* border:1px solid green; */
           }

.foto {
	      border:1px solid #e2e0e0; //#AAA9A9;   
		  width:105px; 
		  height:105px;
		 border-radius: 20px;
                object-fit: contain;
          
	}

.tdbofoto {
	      width:171px; 
		  
          
	}

	
.tablefoto {
	/* border:1px solid blue;  */ 
	width:192px; 
	height:70px;
}

.tocenterfoto {
	width:27px;
	height:50px; 
}

.tablebarcod {
	/* border:1px solid blue;   */
	width:244px; 
	font-size:0px;
	
}

.tocenterbarcod {
	width:25px;
	font-size:0px;
	
}
	
.signature {
		width:115px; 
		height:8px;
		valign:midle;
       }
.text_s {
	     text-align:left; 
		 font-size:10px; 
       }	   
.barc {
	 /*  border:1px solid brown; */
	   height:5px; 
	   font-size:0px;
    }	   
.foot {
	    text-align:center; 
		font-size: 8px;
		font-style: italic;
      }	  
  
.signature_au{
	    text-align:left; 
		font-size: 6px;
		font-style: italic;
      }	   
.tfoot {
	    width:85%; 
      }	   

	  
</style>
                                       
										
EOD;
	 $path_logo=Yii::app()->baseUrl."/css/images/school_logo.png";
	 $path_foto=Yii::app()->baseUrl."/css/images/no_pic.png";   //$path_foto=Yii::app()->baseUrl."/documents/photo-Uploads/1/".$pers_foto; 
         $path_siyati=Yii::app()->baseUrl."/css/images/signature_au.png"; 
	 
	$html .='<span class="shspace"></span><table cellpadding="0" style="border:none">
	<tr>
	     <td class="td_corner" rowspan="2"> </td><td class="td_corner1" > </td><td class="td_top_content1"></td><td class="td_corner1" > </td><td class="td_corner" rowspan="2"> </td>
    </tr>
    <tr>
	     <td colspan="3" class="td_top_content"></td>
    </tr>
    <tr>
             <td class="td_side_l1"></td>
             <td class="td_side_lc2" rowspan="3"></td>
             <td class="secondLayer" rowspan="3">
<table class="content" cellpadding="0" >
<tr>
 <td class="td">
  <table class="idcHeader">
   <tr>
   <td class="logo"><img src="'.$path_logo.'" /></td>
       <td class="schoolName"><!-- Collège<br/>Canado-Haitien -->'.$school_name.'<br/><span class="schoolDevise">'.$devise_school.'</span>
                                 <span class="info_school"><br/>'.$school_address.'<br/>'.$school_phone_number.'</span></td>
   </tr>
   </table>
 </td>
</tr>
<tr>
 <td >
  <table class="tablefoto" >
   <tr>
    <td class="tdbofoto" ><span class="text_s">';
                                                     if($signature_au==0)
                                                         $html .='<br/>';

                                                         $html .=$first_name.' <span style="font-weight: bold;">'.$last_name.'</span></span><br/>';
                     if($modelStudent->is_student==1)
                        {	$html .='<span class="text_s">'.$room_name.'</span>';
                                if($display_code_card==1)
                                     $html .='<br/><span class="text_s">'.$pers_cod.'</span>'; 

                                                        }
                                                       else
                                                       {
                                                           $html .='<span class="text_s">'.$title_name.'</span><br/><span class="text_s">'.$pers_nif_cin.'</span>';     

                                                       }
$html .='<br/><br/>&nbsp;&nbsp;&nbsp;<tcpdf method="write1DBarcode" params="'.$bc.'"/>&nbsp;&nbsp;&nbsp;<span class="text_s">[ '.$acad_name.' ]</span>';
if($signature_au==1)
    $html .='<br/><span class="signature_au">Signature autorisée:</span><img src="'.$path_siyati.'" />';
$html .='</td>
    <td ><img src="'.$path_foto.'"  class="foto"></td>
   </tr>
     </table>
    </td>
   </tr>
   <tr>
   <td style="font-size:10px;" ><span class="foot">'.$school_site_web.'</span></td>
   </tr>
  </table>
</td>
<td class="td_side_rc2" rowspan="3"></td>
<td class="td_side_r1"></td>
</tr>
<tr>
   <td class="td_side_lc1" rowspan=""></td>
   <td class="td_side_rc1" rowspan=""></td>
</tr>
<tr>
   <td class="td_side_l1"></td>
   <td class="td_side_r1"></td>
</tr>
<tr>
    <td class="td_corner" rowspan="2"> </td><td class="td_bottom_content"></td><td class="td_corner" rowspan="2"> </td>
</tr>
<tr>
    <td class="td_corner1" > </td><td class="td_bottom_content1"></td><td class="td_corner1" > </td>
</tr>
</table>
<div class="hspace"> </div>';
               
               $this->printing_date = true;
		                         		                         
		return $html;
		
		
}
	
		
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
