<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


/* @var $this SigespaymentController */
/* @var $model SigesPayment */
/* @var $form CActiveForm */

$acad=Yii::app()->session['currentId_academic_year']; 
$acad_name=Yii::app()->session['currentName_academic_year'];

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 

?>



<div  id="resp_form_siges">

        <form  id="resp_form">
        
            
         <div class="col-3">
            <label id="resp_form">              

				<?php
				//cheche balance lan ave l
				//$model->id_siges_payment_set
				
				 echo $form->labelEx($model,'balance'); ?>
				<?php echo $form->textField($model,'balance',array('size'=>60,'placeholder'=>Yii::t('app','Balance'),'name'=>'balance','id'=>'balance','value'=>$this->balance,'readonly'=>true)); ?>
				<input type="hidden" name="balance_init" id="balance_init" value="<?php echo $this->balance; ?>"></input>
				<input type="hidden" name="balance_hide" id="balance_hide" value=""></input>
				<?php echo $form->error($model,'balance'); ?>
			</label>
        </div>

                          
        <div class="col-3">
            <label id="resp_form">              

				<?php echo $form->labelEx($model,'amount_pay'); ?>
				<?php echo $form->textField($model,'amount_pay',array('size'=>60,'placeholder'=>Yii::t('app','Amount Pay'),'name'=>'amount_pay', 'id'=>'amount_pay','onchange'=>'difference()')); ?>
				<input type="hidden" name="amount_pay_hide" id="amount_pay_hide" value=""></input>
				<input type="hidden" name="old_pay" id="old_pay" value="<?php echo $this->old_pay; ?>"></input>
				<?php echo $form->error($model,'amount_pay'); ?>
			</label>
        </div>
                          
                                 
        <div class="col-3">
            <label id="resp_form">              
          
				<?php 
				        echo $form->labelEx($model,'payment_method'); 
				        
				$criteria = new CDbCriteria(array('order'=>'method_name'));
									
									echo $form->dropDownList($model, 'payment_method',
									CHtml::listData(PaymentMethod::model()->findAll($criteria),'id','method_name'),
									array('prompt'=>Yii::t('app','-- Please select a payment method --') ));
								
									
									?>
				<?php echo $form->error($model,'payment_method'); ?>
			</label>
        </div>
                          
        <div class="col-3">
            <label id="resp_form">              

				<?php echo $form->labelEx($model,'payment_date'); 
				
				    if((isset($_GET['id'])&&($_GET['id']!=''))  )
	                                   $date__ = $model->payment_date;
	                                else
	                                   $date__ = date('Y-m-d'); 
	                              
	                              
					 	   		 $this->widget('zii.widgets.jui.CJuiDatePicker',
							      array(
									 'model'=>'$model',
									 'name'=>'SigesPayment[payment_date]',
									 'language'=>'fr',
									 'value'=>$date__,
									 'htmlOptions'=>array('size'=>60, 'style'=>'width:100% !important','placeholder'=>Yii::t('app','Payment Date')),
										 'options'=>array(
										 'showButtonPanel'=>true,
										 'changeYear'=>true,                                      
										 'changeYear'=>true,
	                                     'dateFormat'=>'yy-mm-dd',   
										 ),
									 )
								 );
     
				?>
				<?php echo $form->error($model,'payment_date'); ?>
			</label>
        </div>
                          
        <div class="col-2">
            <label id="resp_form">              

				<?php echo $form->labelEx($model,'comment'); ?>
				<?php echo $form->textArea($model,'comment',array('rows'=>3, 'cols'=>160)); ?>
				<?php echo $form->error($model,'comment'); ?>
		   </label>
        </div>
                          
                          
         <div class="col-submit">
                                
                                <?php if(!isset($_GET['id'])){
                                         echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning'));
                                        
                                         echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                                        }
                                         else
                                           {  echo CHtml::submitButton(Yii::t('app', 'Save'),array('name'=>'update','class'=>'btn btn-warning'));
                                            
                                              echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                                              
                                             } 
                                           //back button   
                                              $url=Yii::app()->request->urlReferrer;//$_SERVER['REQUEST_URI'];
                                              $explode_url= explode("php",substr($url,0));
				             
                                              echo '<a href="'.$explode_url[0].'php'.$this->back_url.'" class="btn btn-secondary">'.Yii::t('app', 'Back').'</a>';
                                         
                                ?>
            </div>
        </form>
</div>



<script type="text/javascript">
       
    
    
    function difference(){
       
       var amountPay = parseFloat(document.getElementById("amount_pay").value);
       var oldPay = parseFloat(document.getElementById("old_pay").value);
       
       var balance_init = parseFloat( document.getElementById("balance_init").value);
       
        
      if(document.getElementById("amount_pay").value=='')
         amountPay=0;
      
      if(document.getElementById("old_pay").value=='')
         oldPay=0;
      
       var balance; 
       balance = balance_init - (amountPay - oldPay); 
        
       document.getElementById("balance").value = balance.toFixed(2);
       document.getElementById("balance_hide").value = balance.toFixed(2);
       document.getElementById("amount_pay_hide").value = amountPay.toFixed(2);
       
       
    }
    
      
    
     </script>