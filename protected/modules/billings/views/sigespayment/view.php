<?php
/* @var $this SigespaymentController */
/* @var $model SigesPayment */


?>

<h1>View SigesPayment #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'id_siges_payment_set',
		'amount_pay',
		'balance',
		'payment_method',
		'payment_date',
		'comment',
	),
)); ?>
