<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


/* @var $this SigespaymentController */
/* @var $model SigesPayment */

    
$acad=Yii::app()->session['currentId_academic_year']; 

$template = '';


$acad_name=Yii::app()->session['currentName_academic_year'];

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 

$tit =Yii::t('app','Manage Siges Payments');

 if(!isset($_GET['sps_id']) )
    $dataProvider=$model->searchByAcad($_GET['year']);
 elseif(isset($_GET['sps_id']) )
    $dataProvider=$model->searchByPaymentSet($_GET['sps_id']);
?>

<div id="dash">
   <div class="span3"><h2>
         <?php echo Yii::t('app','Manage Siges Payments'); ?>
         
   </h2> </div>
   
   
     <div class="span3">

<?php 
     
         $template = '{update}{delete}';  
      
      if($_GET['year']== $acad)
        {  
   ?>

         <div class="span4">
                  <?php

                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('sigespayment/create/part/sig_p/year/'.$_GET['year'].'/'));
               ?>
  </div> 

<?php
        }
      
      ?>       
           
               <div class="span4">
              <?php

                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/billings/sigespaymentset/index/part/sig_pset/from/stud')); 
                 
                  
               ?>
         </div>
   </div>

</div>


<div style="clear:both"></div>



<div class="b_m">
  <?php
    echo $this->renderPartial('//layouts/navBaseSIGESBilling',NULL,true);	
    ?>

<div class="grid-view">


<?php 
        $gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'siges-payment-grid',
	'dataProvider'=>$dataProvider,
	'showTableOnEmpty'=>true,
	'summaryText'=>'',
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		//'id_siges_payment_set',
		
		array('name'=>Yii::t('app','Amount Pay'), 
			'header'=>Yii::t('app','Amount Pay'), 
			 //'type' => 'raw',
			'value'=>'$data->AmountPay',
			),
		
		array('name'=>Yii::t('app','Balance'), 
			'header'=>Yii::t('app','Balance'), 
			 //'type' => 'raw',
			'value'=>'$data->Balance',
			),
		array('name'=>Yii::t('app','Payment Method'), 
			'header'=>Yii::t('app','Payment Method'), 
			 //'type' => 'raw',
			'value'=>'$data->paymentMethod->method_name',
			),
		array('name'=>Yii::t('app','Payment Date'), 
			'header'=>Yii::t('app','Payment Date'), 
			 //'type' => 'raw',
			'value'=>'$data->PaymentDate',
			),
		
		'comment',
		
		array(
			'class'=>'CButtonColumn',
                        'template'=>$template,
                        'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/billings/sigespayment/update?id=$data->id&year='.$_GET['year'].'&part='.$_GET['part'].'&from=stud")',
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/billings/sigespayment/delete?id=$data->id&year='.$_GET['year'].'&part='.$_GET['part'].'&from=stud")',
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                        
		),
	),
)); ?>


</div>


</div>


