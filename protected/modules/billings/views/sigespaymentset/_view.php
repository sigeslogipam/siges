<?php
/* @var $this SigespaymentController */
/* @var $data SigesPayment */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('old_balance')); ?>:</b>
	<?php echo CHtml::encode($data->old_balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amount_to_pay')); ?>:</b>
	<?php echo CHtml::encode($data->amount_to_pay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('amount_pay')); ?>:</b>
	<?php echo CHtml::encode($data->amount_pay); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('balance')); ?>:</b>
	<?php echo CHtml::encode($data->balance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_method')); ?>:</b>
	<?php echo CHtml::encode($data->payment_method); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('devise')); ?>:</b>
	<?php echo CHtml::encode($data->devise); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('payment_date')); ?>:</b>
	<?php echo CHtml::encode($data->payment_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comment')); ?>:</b>
	<?php echo CHtml::encode($data->comment); ?>
	<br />

	*/ ?>

</div>