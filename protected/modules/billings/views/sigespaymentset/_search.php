<?php
/* @var $this SigespaymentController */
/* @var $model SigesPayment */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'old_balance'); ?>
		<?php echo $form->textField($model,'old_balance'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'amount_to_pay'); ?>
		<?php echo $form->textField($model,'amount_to_pay'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'devise'); ?>
		<?php echo $form->textField($model,'devise'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->