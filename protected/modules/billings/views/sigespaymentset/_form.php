<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/* @var $this SigespaymentsetController */
/* @var $model SigesPaymentSet */
/* @var $form CActiveForm */


$acad=Yii::app()->session['currentId_academic_year']; 
$acad_name=Yii::app()->session['currentName_academic_year'];

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 


?>

	

<div  id="resp_form_siges">

        <form  id="resp_form">
        
            
        <div class="col-3">
            <label id="resp_form"> 
            
				    <?php echo $form->labelEx($model,'old_balance'); ?>
					<?php echo $form->textField($model,'old_balance',array('size'=>60,'placeholder'=>Yii::t('app','Old Balance'))); ?>
					<?php echo $form->error($model,'old_balance'); ?>
			</label>
        </div>
                          
        <div class="col-3">
            <label id="resp_form">              

					<?php echo $form->labelEx($model,'amount_to_pay'); ?>
					<?php echo $form->textField($model,'amount_to_pay',array('size'=>60,'placeholder'=>Yii::t('app','Amount To Pay'))); ?>
					<?php echo $form->error($model,'amount_to_pay'); ?>
		    </label>
        </div>
         
        <div class="col-3">
            <label id="resp_form">              

					<?php echo $form->labelEx($model,'display_on'); 
					if((isset($_GET['id'])&&($_GET['id']!=''))  )
	                                   $date__ = $model->display_on;
	                                else
	                                   $date__ = date('Y-m-d'); 
	                              
	                              
					 	   		 $this->widget('zii.widgets.jui.CJuiDatePicker',
							      array(
									 'model'=>'$model',
									 'name'=>'SigesPaymentSet[display_on]',
									 'language'=>'fr',
									 'value'=>$date__,
									 'htmlOptions'=>array('size'=>60, 'style'=>'width:100% !important','placeholder'=>Yii::t('app','Dispalay On')),
										 'options'=>array(
										 'showButtonPanel'=>true,
										 'changeYear'=>true,                                      
										 'changeYear'=>true,
	                                     'dateFormat'=>'yy-mm-dd',   
										 ),
									 )
								 );
							?>
					<?php echo $form->error($model,'display_on'); ?>
		    </label>
        </div>
       
                          
  <!--      <div class="col-2">
            <label id="resp_form">              

					<?php echo $form->labelEx($model,'devise'); ?>
					<?php echo $form->textField($model,'devise',array('size'=>60,'placeholder'=>Yii::t('app','Devise'))); ?>
					<?php echo $form->error($model,'devise'); ?>
			    </label>
        </div>
    -->
        <div class="col-1">
            <label id="resp_form">              

				<?php echo $form->labelEx($model,'comment'); ?>
				<?php echo $form->textArea($model,'comment',array('rows'=>1, 'cols'=>160)); ?>
				<?php echo $form->error($model,'comment'); ?>
		   </label>
        </div>
                           
         <div class="col-submit">
                                
                                <?php if(!isset($_GET['id'])){
                                         echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning'));
                                        
                                         echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                                        }
                                         else
                                           {  echo CHtml::submitButton(Yii::t('app', 'Save'),array('name'=>'update','class'=>'btn btn-warning'));
                                            
                                              echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                                              
                                             } 
                                           //back button   
                                              $url=Yii::app()->request->urlReferrer;//$_SERVER['REQUEST_URI'];
                                              $explode_url= explode("php",substr($url,0));
				             
                                              echo '<a href="'.$explode_url[0].'php'.$this->back_url.'" class="btn btn-secondary">'.Yii::t('app', 'Back').'</a>';
                                         
                                ?>
            </div>
        </form>
</div>
