<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/* @var $this SigespaymentController */
/* @var $model SigesPayment */

    
$acad=Yii::app()->session['currentId_academic_year']; 

$template = '';


$acad_name=Yii::app()->session['currentName_academic_year'];

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 

$tit =Yii::t('app','Manage Siges Payment Set');

?>

<div id="dash">
   <div class="span3"><h2>
         <?php echo Yii::t('app','Manage Siges Payment Set'); ?>
         
   </h2> </div>
   
   
     <div class="span3">

<?php 
     
         $template = '{update}{delete}';  
        
   ?>

         <div class="span4">
                  <?php

                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('sigespaymentset/create'));
               ?>
  </div> 

<?php
        
      
      ?>       
           
               <div class="span4">
              <?php

                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/billings/sigespaymentset/index/part/sig_pset/from/stud')); 
                 
                  
               ?>
         </div>
   </div>

</div>


<div style="clear:both"></div>


<div class="b_m">
  <?php
    echo $this->renderPartial('//layouts/navBaseSIGESBilling',NULL,true);	
    ?>

<div class="grid-view">


<?php 
        $gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'siges-payment-set-grid',
	'dataProvider'=>$model->search(),
	'showTableOnEmpty'=>true,
	'summaryText'=>'',
	//'filter'=>$model,
	'columns'=>array(
		//'id',
	
		array('name'=>Yii::t('app','Old Balance'), 
			'header'=>Yii::t('app','Old Balance'), 
			 'type' => 'raw',
			'value'=>'CHtml::link($data->oldBalance,Yii::app()->createUrl("/billings/sigespayment/index",array("part"=>"sig_p","sps_id"=>$data->id,"year"=>$data->academic_year)))'),
			
	    array('name'=>Yii::t('app','Amount To Pay'), 
			'header'=>Yii::t('app','Amount To Pay'), 
			 'type' => 'raw',
			'value'=>'CHtml::link($data->amountToPay,Yii::app()->createUrl("/billings/sigespayment/index",array("part"=>"sig_p","sps_id"=>$data->id,"year"=>$data->academic_year)))'), 

		
			array('name'=>Yii::t('app','Academic Year'), 
			'header'=>Yii::t('app','Academic Year'), 
			 'type' => 'raw',
			'value'=>'CHtml::link($data->academicYear->getAcademicPeriodName(),Yii::app()->createUrl("/billings/sigespayment/index",array("part"=>"sig_p","sps_id"=>$data->id,"year"=>$data->academic_year)))'), 
			
			array('name'=>Yii::t('app','Display On'), 
			'header'=>Yii::t('app','Display On'), 
			 //'type' => 'raw',
			'value'=>'$data->DisplayOn',
			),
		
		   array('header'=>Yii::t('app','Com. '),'name'=>'comment',
                    'type' => 'raw','value'=>'($data->comment == null) ? " " : "<a href=\"#\"><span data-toggle=\"tooltip\" title=\"$data->comment\"><i class=\"fa fa-comment-o\"></i> </span></a>"',
                ),
                      
            
			
		/*
		'devise',
		*/
		array(
			'class'=>'CButtonColumn',
                        'template'=>$template,
                        'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                        
		),
	),
)); ?>


</div>


</div>
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit?>"},
                  
                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>



