<?php 
/*
 * © 2010 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>
<?php
/* @var $this EnrollmentIncomeController */
/* @var $model EnrollmentIncome */

 $current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

 
$tit='';
$template = '';
$from='';
if(isset($_GET['from']))
    $from = $_GET['from'];
    
$part='bill';

if(isset($_GET['part']))
{
	$part=$_GET['part'];
	}


$currency_symbol = Yii::app()->session['currencySymbol'];
$acad_name=Yii::app()->session['currentName_academic_year'];

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 



Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#enrollment-income-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>



<div id="dash">
   <div class="span3"><h2>
         <?php echo Yii::t('app','Manage Postulants'); ?>
         
   </h2> </div>
   
   
     <div class="span3">
   
<?php 
     $template='';
     if(!isAchiveMode($acad_sess))
        {  
          if($acad == $current_acad_id)
		  {
        	$template = '{update}{delete}';  
        
   ?>

   
         <div class="span4">
                  <?php

                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('enrollmentIncome/create/part/bill/from/'.$from));
               ?>
        </div>
         
           
  <?php
          }
		
		}
      
      ?>       
     
      
        <div class="span4">
              <?php

                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/academic/postulant/viewListAdmission/part/enrlis/from/'.$from)); 
                 
                  
               ?>
         </div>
   </div>

</div>

<div style="clear:both"></div>


<br/>


    <?php
    echo $this->renderPartial('//layouts/navBaseEnrollment',NULL,true);	
    ?>





<div class="grid-view">



<div class="search-form" >




<?php 
     $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
       $gridWidget = $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'enrollment-income-grid',
           'summaryText'=>'',
	'showTableOnEmpty'=>true,
	'dataProvider'=>$model->search($acad_sess),
	//'filter'=>$model,
	'columns'=>array(
		//'id',
		
array('name'=>'postulant',
			'header'=>Yii::t('app','Postulant'), 
			'type' => 'raw',
                    'value'=>'CHtml::link($data->FullName,Yii::app()->createUrl("/academic/postulant/viewAdmissionDetail",array("id"=>$data->postulant,"part"=>"bill","pg"=>"")))',
                    'htmlOptions'=>array('width'=>'150px'),
                     ),	
                     		
		array('name'=>'apply_level',
			'header'=>Yii::t('app','Apply for level'), 
			'type' => 'raw',
                    'value'=>'CHtml::link($data->applyLevel->level_name,Yii::app()->createUrl("/academic/postulant/viewAdmissionDetail",array("id"=>$data->postulant,"part"=>"bill","pg"=>"")))',
                    
                     ),
			
		array('name'=>'amount',
			'header'=>Yii::t('app','Amount Pay'), 
			'value'=>'$data->Amount'),
			
		array('name'=>'payment_date',
			'header'=>Yii::t('app','Payment date'), 
			'value'=>'$data->PaymentDate'),
			
		
		
		
		array(
			'class'=>'CButtonColumn',
                        'template'=>$template,
                        'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/billings/enrollmentIncome/update?id=$data->id&part=bill&from='.$from.'")',
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                
		),
	),
)); 

       
?>

</div>

<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit;?>" },
                  
                    /*{extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                    
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>
</div>