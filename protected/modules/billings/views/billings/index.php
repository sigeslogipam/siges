<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>
<?php
/* @var $this BillingsController */
/* @var $model Billings */

   
	
 $current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
$tit='';

$template = '{view}';


$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 

	
?>


<div id="dash">
          
          <div class="span3"><h2>
              <?php if($this->status_==1)
                      { echo Yii::t('app','Manage Billings'); $tit=Yii::t('app','Manage Billings');
                      }
                    elseif($this->status_==0)
                      { echo Yii::t('app','Manage other fees'); $tit=Yii::t('app','Manage  other fees');
                      }
                    
                  ?>
              
          </h2> </div>
     
<div class="span3">
             
<?php 
     
     if(!isAchiveMode($acad_sess))
        {  
          if($acad == $current_acad_id)
		  {
         	$template = '{update}{delete}'; // '{view}{update}{delete}';  
      if($this->recettesItems!=0) 
       {
   ?>

     <div class="span4">
              <?php

                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 
                 //if($this->recettesItems==0)
                 //    echo CHtml::link($images,array('/billings/billings/create/part/rec/ri/0/from/stud'));
                 // else
                   if($this->recettesItems==1)
                     echo CHtml::link($images,array('/billings/billings/create/part/rec/ri/0/from/stud'));
                   elseif($this->recettesItems==2)
                     echo CHtml::link($images,array('/billings/otherIncomes/create/part/rec/ri/2/from/stud')); 
               ?>
   </div>
   
  <?php
        }
       else
       {
           ?>
       
          <div class="span3">
               <?php
                $form = $this->beginWidget('CActiveForm', array(
                'id'=>'billings-form',
                //'enableAjaxValidation'=>true,
        ));
        ?>
           <form  id="resp_form1">
               <div class="span12" style="left">
                    <label id="resp_form" style="margin-top:-18px;">
                          <?php
                $model_new  = new Billings;
                $criteria = new CDbCriteria(array('alias'=>'p', 'order'=>'last_name ASC','join'=>'left join room_has_person rh on(rh.students = p.id)', 'condition'=>'is_student=1 AND active IN(1,2) AND rh.academic_year ='.$acad_sess));

                echo $form->dropDownList($model_new,'student',$this->loadStudentByCriteria($criteria), array('onchange'=> 'submit()', 'prompt'=>Yii::t('app','Cashing') ));

         ?>
                    </label>
               </div>


           </form>   
          <?php $this->endWidget(); ?>
            </div>
    <?php
       }
                    }
		
		}
      
      ?>       

 
   <div class="span4">
                  <?php

                 $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 
                 if($this->recettesItems==0)
                     echo CHtml::link($images,array('../site/index')); 
                  elseif($this->recettesItems==1)
                     echo CHtml::link($images,array('/billings/billings/index/part/rec/ri/0/from/stud'));
                   
               ?>
  </div>  


  </div>

</div>



<div style="clear:both"></div>






<div class="b_m">


<div class="row" style="padding: 5px 10px;"> 
     
      <div class="span9" >                        

<?php
$form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'billings-form',
	'enableAjaxValidation'=>false,
));

?>
                           
     
      						<div class="span2" >
                                
                                <?php echo $form->errorSummary($model); ?>
                                <div class="left" style="padding-left:20px;">
                                    <?php 
                                        
                                        echo $form->labelEx($model,Yii::t('app','Recettes Items'));
                                        
                                        if(isset($this->recettesItems)&&($this->recettesItems!=''))
							       echo $form->dropDownList($model,'recettesItems',$this->loadRecettesItems(), array('onchange'=> 'submit()','options' => array($this->recettesItems=>array('selected'=>true)))); 
							    else
								  { 
									echo $form->dropDownList($model,'recettesItems',$this->loadRecettesItems(), array('onchange'=> 'submit()')); 
								  }

                                    ?>
                                </div>
                                
                            </div>
  
<?php $this->endWidget(); ?>
                   
        </div>
        
    </div>
    


<br/>



<ul class="nav nav-tabs nav-justified">  

</ul>




<div class="grid-view">

          
           <div  class="search-form">


<?php 
      
      $condition ='fl.status='.$this->status_.' AND ';  
            
function evenOdd($num)
{
($num % 2==0) ? $class = 'odd' : $class = 'even';
return $class;
}


$gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'billings-grid',
	'summaryText'=>'',
	'dataProvider'=>$model->search($condition, $acad_sess),
	'showTableOnEmpty'=>true,
	//'emptyText'=>Yii::t('app','No academic period found'),
			//'summaryText'=>Yii::t('app','View academic period from {start} to {end} (total of {count})'),
              //'mergeColumns'=>array('date_pay','student0.fullName', 'fee_fname' ),//'student0.first_name', 'student0.last_name','fee_fname'),
    
        'rowCssClassExpression'=>'($data->balance>0)?"balance":evenOdd($row)',
	
	'columns'=>array(
		//'id',
		
	    array('name'=>'date_pay',
			//'header'=>Yii::t('app','Fee name'), 
			'value'=>'$data->DatePay'),
			
             
       array(
		    'name'=>Yii::t('app','First Name'),//'student0.fullName',
			'header'=>Yii::t('app','First Name'),//Yii::t('app','Student'),
                        'type' => 'raw',
			'value'=>'CHtml::link("<span data-toggle=\"tooltip\" title=\"".$data->student0->id_number."\">".$data->student0->first_name."</span>",Yii::app()->createUrl("/billings/billings/view",array("stud"=>$data->student,"ri"=>0,"part"=>"rec","from"=>"stud")))',
                      //  'htmlOptions'=>array('width'=>'125px'),
                    ),
          array(
		    'name'=>Yii::t('app','Last Name'),//'student0.fullName',
			'header'=>Yii::t('app','Last Name'),//Yii::t('app','Student'),
                        'type' => 'raw',
			'value'=>'CHtml::link("<span data-toggle=\"tooltip\" title=\"".$data->student0->id_number."\">".$data->student0->last_name."</span>",Yii::app()->createUrl("/billings/billings/view",array("stud"=>$data->student,"ri"=>0,"part"=>"rec","from"=>"stud")))',
                        //'htmlOptions'=>array('width'=>'125px'),
                    ),
            	
   
             			//'student',
       		array('name'=>'fee_fname',
			'header'=>Yii::t('app','Fee name'), 
			'value'=>'$data->feePeriod->simpleFeeName'),
	
            array('header'=>Yii::t('app','Amount To Pay'),'value'=>'$data->amountToPay'),
            array('header'=>Yii::t('app', 'Amount Pay'),'value'=>'$data->amountPay'),
		
            array('header'=>Yii::t('app', 'Balance'),'value'=>'$data->balanceCurrency'),
		
		array(
			'class'=>'CButtonColumn',
                        'template'=>$template,
                         'buttons'=>array(
                         
                         'view'=>array(
				            'label'=>'<i class="fa fa-eye"></i>',
				            'imageUrl'=>false,
				            'url'=>'Yii::app()->createUrl("/billings/billings/view?stud=$data->student&id=$data->id&ri='.$this->recettesItems.'&part=rec&from=stud")',
				            'options'=>array( 'title'=>Yii::t('app','View') ),
				        ),
                         'update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/billings/billings/view?stud=$data->student&id=$data->id&ri='.$this->recettesItems.'&part=rec&from=stud")',
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                            
                       
		),
	 
	),
)); 

   
  
  

?>


</div>



<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit?>"},
                  
                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>



</div>
