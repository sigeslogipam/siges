<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

/*
 * Billings
 */

/* @var $this BillingsController */
$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));


$this->pageTitle=Yii::app()->name;
$baseUrl = Yii::app()->baseUrl;

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/css/AdminLTE.min.css");
$cs->registerCssFile($baseUrl."/css/new-dashboard.css");
$cs->registerCssFile("https://use.fontawesome.com/releases/v5.5.0/css/all.css");
?>
<?php
/* @var $this BillingsController */
// Set the page title at the index (dashboard) with the profil name and the user name of the login user.
$this->pageTitle=Yii::app()->name;//.' - '.Yii::app()->user->profil. ' '.Yii::app()->user->name;
?>

<?php // echo Yii::t('app','Welcome to {name}', array('{name}'=>Yii::app()->name));?>

<?php
$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
$siges_structure = infoGeneralConfig('siges_structure_session');


     $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));

 if($current_acad==''){
        $condition = '';
          $condition1 ='';
 }
    else
    {  if($acad!=$current_acad->id)
       {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
      else
        {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
    }



$modelAcad=new AcademicPeriods;





	$school_name = infoGeneralConfig('school_name');

	//echo $school_name;
     $school_acronym = infoGeneralConfig('school_acronym');


      $path=null;

     $explode_basepath= explode("protected",substr(Yii::app()->basePath, 0));
     echo '<input type="hidden" id="basePath" value="'.$explode_basepath[0].'" />';

?>

<div class="discv">
<div id="dash">
   <div class="span1">
      
  </div>

  <?php 
     
     if(!isAchiveMode($acad_sess))
        {   
          if($acad == $current_acad_id)
		  {
        	//$template = '{print}{update}{delete}';  
        
   ?>   

     <div class="span3">
       <?php
        $form = $this->beginWidget('CActiveForm', array(
	'id'=>'billings-form',
	//'enableAjaxValidation'=>true,
));
?>
   <form  id="resp_form1">
       <div class="span12" style="left">
            <label id="resp_form" style="margin-top:-18px;">
                  <?php
        $model_new  = new Billings;
        $criteria = new CDbCriteria(array('alias'=>'p', 'order'=>'last_name ASC','join'=>'left join room_has_person rh on(rh.students = p.id)', 'condition'=>'is_student=1 AND active IN(1,2) AND rh.academic_year ='.$acad_sess));

        echo $form->dropDownList($model_new,'student',$this->loadStudentByCriteria($criteria), array('onchange'=> 'submit()', 'prompt'=>Yii::t('app','Cashing') ));

 ?>
            </label>
       </div>
       
   </form>   
  <?php $this->endWidget(); ?>
    </div>
  <?php
                  }
                  
        }
    ?>        
          

   </div>

</div>


<div class="container-fluid" style="text-align:center;" >




<div class="row-fluid">

<div class="checkbox" style="text-align:justify;padding-left:30px;" id="index_dash">

				<!-- ================================================== -->

	 <?php

		 $currency_name = Yii::app()->session['currencyName'];
	         $currency_symbol = Yii::app()->session['currencySymbol'];

                      $tot_tuition_fee = 0;
                      $sql="SELECT SUM(amount_pay) as montant FROM billings b INNER JOIN fees f ON(f.id=b.fee_period AND f.academic_period=b.academic_year) INNER JOIN fees_label fl ON(fl.id=f.fee) WHERE b.academic_year=$acad_sess AND fl.status=1";
                      $bill_tuition_fee = Billings::model()->findAllBySql($sql);

                      if($bill_tuition_fee!=null)
                      { foreach ($bill_tuition_fee as $r)
                        $tot_tuition_fee = $r['montant'];
                          if($tot_tuition_fee ==null)
                            $tot_tuition_fee =0;
                      }


                      $tot_other_fee = 0;
                      $sql="SELECT SUM(amount_pay) as montant FROM billings b INNER JOIN fees f ON(f.id=b.fee_period AND f.academic_period=b.academic_year) INNER JOIN fees_label fl ON(fl.id=f.fee) WHERE b.academic_year=$acad_sess AND fl.status=0";
                      $bill_other_fee = Billings::model()->findAllBySql($sql);

                      if($bill_other_fee!=null)
                      { foreach ($bill_other_fee as $r)
                        $tot_other_fee = $r['montant'];
                        if($tot_other_fee ==null)
                            $tot_other_fee =0;
                      }


                      $tot_late_payment = 0;
                      //$sql="SELECT COUNT(student) as total FROM balance b INNER JOIN room_has_person rhp ON(b.student=rhp.students) WHERE  balance>0 and rhp.academic_year=$acad_sess";
                      $sql="SELECT COUNT(rhp.students) AS total FROM persons p INNER JOIN billings bi ON (bi.student = p.id) INNER JOIN level_has_person lhp ON (lhp.students = p.id) INNER JOIN room_has_person rhp ON (rhp.students = p.id AND rhp.academic_year=lhp.academic_year) WHERE bi.balance > 0 AND bi.fee_totally_paid=0 AND p.active in(1,2) AND rhp.academic_year =  $acad_sess  GROUP BY p.id ";    //"SELECT COUNT(students) as total FROM room_has_person rhp WHERE academic_year=$acad_sess and students in (SELECT student FROM billings b inner join persons p on(b.student=p.id) WHERE fee_totally_paid =0 AND balance>0 and active in(1,2) and b.academic_year=$acad_sess)";
                      
                      $bill_late_payment = Balance::model()->findAllBySql($sql);

                      if($bill_late_payment!=null)
                      { foreach ($bill_late_payment as $r)
                        $tot_late_payment = $r['total'];
                        if($tot_late_payment ==null)
                            $tot_late_payment =0;
                      }




                      $tot_bousye = 0;
                      $sql="SELECT COUNT(DISTINCT student) as total FROM scholarship_holder WHERE comment IS NULL AND academic_year=$acad_sess";
                      $bill_bousye = ScholarshipHolder::model()->findAllBySql($sql);

                      if($bill_bousye!=null)
                      { foreach ($bill_bousye as $r)
                        $tot_bousye = $r['total'];
                        if($tot_bousye ==null)
                            $tot_bousye =0;
                      }





   ?>



			 <!--===========================================-->



		<?php
		       $span="span12";
		?>

   <!-- Re loocker le dashboard ici -->
   <div  class="row">
       <!-- Frais scolaire -->
       <div class="span3 ti-bwat">
             <!-- small box -->
          <div class="small-box bg-aqua" data-html="true" rel="tooltip">
            <div class="inner">
                <h3><span class="teks-blan"><?= Yii::t('app','Tuition fees') ?></span></h3>

                <p class="nom-piti teks-blan">
                    <?= $currency_symbol.' '.numberAccountingFormat($tot_tuition_fee); ?>
                </p>

                </div>

           <div class="icon">
                    <i class="fas fa-dollar-sign"  style="padding-top: 20px"></i>
           </div>
          </div>
        </div>
       <!-- Autres entrees -->
       <div class="span3 ti-bwat">
             <!-- small box -->
          <div class="small-box bg-green" data-html="true" rel="tooltip">
            <div class="inner">
                <h3><span class="teks-blan"><?= Yii::t('app','Other incomes') ?></span></h3>

                <p class="nom-piti teks-blan">
                    <?= $currency_symbol.' '.numberAccountingFormat($tot_other_fee); ?>
                </p>

                </div>

           <div class="icon">
                    <i class="fas fa-search-dollar"  style="padding-top: 20px"></i>
           </div>
          </div>
        </div>
       <!-- Retard de paiement -->
       <div class="span3 ti-bwat">
             <!-- small box -->
          <div class="small-box bg-fuchsia" data-html="true" rel="tooltip">
            <div class="inner">
                <h3><span class="teks-blan"><?= Yii::t('app','Late payment') ?></span></h3>

                <p class="nom-piti teks-blan">
                    <?= $tot_late_payment.' '.Yii::t('app','Student(s)');?>
                </p>

                </div>

           <div class="icon">
                    <i class="fas fa-hand-holding-usd"  style="padding-top: 20px"></i>
           </div>
          </div>
        </div>
       <!-- Boursiers -->
       <div class="span3 ti-bwat">
             <!-- small box -->
          <div class="small-box bg-purple-gradient" data-html="true" rel="tooltip">
            <div class="inner">
                <h3><span class="teks-blan"><?= Yii::t('app','Scholarship holder') ?></span></h3>

                <p class="nom-piti teks-blan">
                    <?= $tot_bousye.' '.Yii::t('app','Student(s)');?>
                </p>

                </div>

           <div class="icon">
                    <i class="fas fa-funnel-dollar"  style="padding-top: 20px"></i>
           </div>
          </div>
        </div>

   </div>
   <!-- Fin du relookage des ti kare -->


   <!-- Relocakage du perimetre des graphes -->
   <div class="row">
       <div class="" >


		<div class="span12 gwobwat">
                       <?php

                          $last_acad=AcademicPeriods::model()->searchLastAcademicPeriod();

                             $month = getMonth(date('Y-m-d'));
                             $month_name = getLongMonth($month);

                             $year = getYear(date('Y-m-d'));


                             ?>
                     <div class="inner estaf">
                            <h4><span class='percent teks-blan'><?= Yii::t('app','Income by level').': '.$year ?></span></h4>
                     </div>

                     <div id="container" class='' style="float: left; width: 100%; height: 320px;  "></div>

                </div>
	<br/>

                  <div style=" width: 100%;">
                       <?php
                              $condition1 ='fl.status='.$this->status_.' AND ';
                             $dataProvider_ = $model->search2($condition1, $acad_sess);
                             $datalast_billing = $dataProvider_->getData();

                             ?>
                      <div class="inner estaf">
                            <h4><span class='percent teks-blan'><?php echo Yii::t('app','The last fee income'); ?></span></h4>
                         </div>
<?php
        $form=$this->beginWidget('CActiveForm', array(
	'id'=>'billings-form',
	//'enableAjaxValidation'=>true,
));
?>


            <label id="resp_form">
 <?php
        $criteria = new CDbCriteria(array('alias'=>'p', 'order'=>'last_name ASC','join'=>'left join room_has_person rh on(rh.students = p.id)', 'condition'=>'is_student=1 AND active IN(1,2) AND rh.academic_year ='.$acad_sess));

        echo $form->dropDownList($model,'student',$this->loadStudentByCriteria($criteria), array('onchange'=> 'submit()', 'prompt'=>Yii::t('app','Manage Billings') ));

 ?>
            </label>


<?php $this->endWidget(); ?>

              <div class="grid-view">
                      <div class="search-form">


				        <table class='table table-striped table-bordered table-hover dataTables-example items'>
				         <thead>
				            <tr>
				            <th></th>
                                            <th><?= Yii::t('app','Date Pay'); ?></th>
				            <th><?= Yii::t('app','First Name'); ?></th>
				            <th><?= Yii::t('app','Last Name'); ?></th>
				            <th><?= Yii::t('app','Fee name'); ?></th>
				            <th><?= Yii::t('app','Amount To Pay'); ?></th>
				            <th><?= Yii::t('app','Amount Pay'); ?></th>
				            <th><?= Yii::t('app','Balance'); ?></th>



				           <!--      <th></th> -->

				            </tr>
				        </thead>
				        <tbody>
<?php
         $compt=0;
          foreach($datalast_billing as $last_billing)
           {
              $compt++;
           	    $date_pay = '';
                    $prenom = '';
                    $nom = '';
                    $fee = '';
                    $amount_2_pay = 0;
                    $amount_pay = 0;
                    $bal = 0;
                 $modelBilling = Billings::model()->findAllByPk($last_billing["id"]);

                foreach ($modelBilling as $bill)
                  {
                    $date_pay = $bill->DatePay;
                    $prenom = $bill->student0->first_name;
                    $nom = $bill->student0->last_name;
                    $fee = $bill->feePeriod->simpleFeeName;
                    $amount_2_pay = $bill->AmountToPay;
                    $amount_pay = $bill->AmountPay;
                    $bal = $bill->BalanceCurrency;

                  }

              echo '  <tr >
                              <td >'.$compt.'</td>
                              <td >'.$date_pay.'</td>
                              <td >'.CHtml::link($prenom,Yii::app()->createUrl("/billings/billings/view",array("stud"=>$last_billing["student"],"ri"=>0,"part"=>"rec","from"=>"stud"))).'</td>
                              <td >'.CHtml::link($nom,Yii::app()->createUrl("/billings/billings/view",array("stud"=>$last_billing["student"],"ri"=>0,"part"=>"rec","from"=>"stud"))).'</td>
                              <td >'.$fee.'</td>
                              <td >'.$amount_2_pay.'</td>
                              <td >'.$amount_pay.'</td>
                              <td >'.$bal.'</td>
                          </tr>';
           }
  ?>

                                        </tbody>
                                     </table>



                          </div>


<?php


  $sql="SELECT SUM(amount_pay) as montant, level_name FROM billings b INNER JOIN fees f ON(f.id=b.fee_period AND f.academic_period=b.academic_year) INNER JOIN fees_label fl ON(fl.id=f.fee) INNER JOIN levels l ON(l.id=f.level)  WHERE b.academic_year=$acad_sess AND fl.status=1 GROUP BY l.level_name ORDER BY l.id ";
  $data1 = Billings::model()->findAllBySql($sql);

  $category_level ='';
  $montant_category_level ='';

     $s = 0;

 if($data1 !=null)
 {
 foreach($data1 as $ap){

     if($s==0)
       {   $category_level = $ap->level_name;
         $montant_category_level = $ap->montant;
           $s=1;
        }
     else
        {
          $category_level = $category_level.','.$ap->level_name;
          $montant_category_level = $montant_category_level.','.$ap->montant;
        }


 }
 }


 echo '<input type="hidden" id="data_category_level" value="'.$category_level.'" />';
 echo '<input type="hidden" id="montant_category_level" value="'.$montant_category_level.'" />';
 echo '<input type="hidden" id="devise_name" value="'.$currency_name.' ('.$currency_symbol.')" />';
 echo '<input type="hidden" id="devise_symbol" value="'.$currency_symbol.'" />';


 echo '<input type="hidden" id="title_inf" value="'.Yii::t('app',' ').'" />';
 echo '<input type="hidden" id="subtitle_inf" value=" " />';




?>




<script>



$(document).ready( function() {

     $('.items').DataTable({
                pageLength: 10,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: " "},

                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });

var title_inf = document.getElementById("title_inf").value;
 var subtitle_inf = document.getElementById("subtitle_inf").value;
var data_category_level=document.getElementById("data_category_level").value;
	var tab_level = data_category_level.split(",");
 var data_montant_category_level=document.getElementById("montant_category_level").value;
	var tab_montant_level = data_montant_category_level.split(",");


 var devise_name = document.getElementById("devise_name").value;
 var devise_symbol = document.getElementById("devise_symbol").value;

  var tab_montant=[];
   var i;

   //table value for male
   for(i=0;i<tab_montant_level.length; i++)
    {
    	tab_montant.push(parseFloat(tab_montant_level[i]));
    	}

    $('#container').highcharts({


    title: {
        text: title_inf
    },

    subtitle: {
        text: subtitle_inf
    },
       xAxis: {
        categories: tab_level
    },
       yAxis: {
        title: {
            text: devise_name
        }
    },

    series: [{
        type: 'column',
        colorByPoint: true,
        name: 'Total ('+devise_symbol+')',
        data: tab_montant,
        dataLabels: {
            enabled: true,
            rotation: -90,
            color: '#FFFFFF',
            align: 'right',
            format: '{point.y:.1f}', // one decimal
            y: 5, // 10 pixels down from the top
            style: {
                fontSize: '9px',
                fontFamily: 'Verdana, sans-serif'
            }
        },
        showInLegend: false
    }]



    });















});






</script>

                </div>

         </div>


       </div>
   </div>

   <!-- Fin du relockage du perimetre des graphes -->



            </div>







	</div>




</div>

<!-- ================================================== -->
