<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

?>

<?php
 $exam_period;

 $current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;

$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];


 $acad_name=Yii::app()->session['currentName_academic_year'];

 $previous_year= AcademicPeriods::model()->getPreviousAcademicYear($acad_sess);

 $pending_balance=null;
 
 $summary_extended_text = '';



 $student_id = 0;
      	 $birthday = '0000-00-00';
     $image='';
  $modelStud ='';

 /* if(isset($model->student))
      {
      	 $student_id = $model->student0->id;
      	 $birthday = $model->student0->birthday;
      	 $image= $model->student0->image;
      	 $full_name = $model->student0->getFullName();
      }
   else
      {
     */   if(isset($_GET['stud']))
      	    {  $student_id = $_GET['stud'];

      	       $modelStud =Persons::model()->findByPk($student_id);

      	       $birthday = $modelStud->birthday;

      	       $image= $modelStud->image;
      	       $full_name = $modelStud->fullName;

      	    }
    //  }
        
 
//si $this->is_pending_balance =true met summary pou ane pase epi retire afe pending balance lan
 if($this->is_pending_balance==true)
  { $acad_sess = $previous_year;
  
      $summary_extended_text = Yii::t('app','Pending balance');
      
      
   }
 else {
      
     $this->is_pending_balance =false;
    $acad_sess = acad_sess();//Yii::$app->session['currentId_academic_year'];
                      
}
				
$condition = '';
$ri= 1;

 if(isset($_GET['ri']))
   {
       if($_GET['ri']==0)
         {  $ri= 0;

          }
        elseif($_GET['ri']==1)
          { $ri= 1;

          	 }




    }

 function evenOdd($num)
            {
                ($num % 2==0) ? $class = 'odd' : $class = 'even';
                return $class;
            }
?>

<?php
/* @var $this BillingsController */
/* @var $model Billings */


 
?>

<div id="dash">

          <div class="span3"><h2>
               <?php
                     echo CHtml::link($full_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$student_id,"pg"=>"lr","pi"=>"no","isstud"=>1,"from"=>"stud")));

                ?>

          </h2> </div>

		   <div class="span6">

 <?php
 
/* if(!isAchiveMode($acad_sess))
        {   
          if($acad == $current_acad_id)
		  {
        */	//$template = '{print}{update}{delete}';
              
              
        $form = $this->beginWidget('CActiveForm', array(
	'id'=>'billings-form',
	//'enableAjaxValidation'=>true,
));
?>
   <form  id="resp_form1">
     <div class="col-3" style="left">
            <label id="resp_form" style="margin-top:-14px;">
 <?php
        $model_new  = new Billings;
        $criteria = new CDbCriteria(array('alias'=>'p', 'order'=>'last_name ASC','join'=>'left join room_has_person rh on(rh.students = p.id)', 'condition'=>'is_student=1 AND active IN(1,2) AND rh.academic_year ='.$acad_sess));

        echo $form->dropDownList($model_new,'student',$this->loadStudentByCriteria($criteria), array('onchange'=> 'submit()', 'prompt'=>Yii::t('app','Cashing') ));

 ?>
            </label>
     </div>
   </form>
<?php $this->endWidget(); 

      //    }

      // }


?>


              <div class="span4">

                  <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                               // build the link in Yii standard

                       if(isset($_GET['from1']))
                       { if($_GET['from1']=='vfr')
                               echo CHtml::link($images,array('/academic/persons/viewForReport/id/'.$_GET['stud'].'/pg/lr/pi/no/isstud/1/from/stud'));
                         elseif($_GET['from1']=='sho')
                             echo CHtml::link($images,array('/billings/scholarshipholder/index/from/bil'));
                            elseif($_GET['from1']=='exem')
                             echo CHtml::link($images,array('/billings/scholarshipholder/index_exempt/from/bil'));

                       }
                      else
                          {
                          	 echo CHtml::link($images,array('/billings/billings/dashboard/part/rec/from/stud'));
                          }

                   ?>

            </div>

        </div>

  </div>



<div style="clear:both"></div>

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'billings-form',
	//'enableAjaxValidation'=>true,
));


?>



<div style="clear:both"></div>

<!-- La ligne superiure  -->


<div class="row-fluid">



<div class="span3 grid-view gbwat" style=" padding:15px; height: auto;">

<div style=" color:#F0652E;">
        <?php
        /*
        $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'view-billings-form',
	'enableAjaxValidation'=>true,
));
       */
        ?>
           <label for="period_name" style="font-weight:bold;"><?php echo Yii::t('app','Summary').' '.$summary_extended_text; //echo Yii::t('app','-- Select --') ?></label>

 <?php


	//echo $form->dropDownList($model,'recettesItems',$this->loadRecettesItemsSummary(), array('onchange'=> 'submit()','options' => array($this->recettesItems=>array('selected'=>true))));
        ?>

        <?php //$this->endWidget(); ?>
    </div>





    <table class="detail-view table table-striped table-condensed">

        <tr class="odd"><th><?php echo Yii::t('app','Exonerate Fee(s) '); ?></th></tr>
        <tr class="odd">
            <td>

         <?php //

                if($this->recettesItems ==  0)
					{ $this->status_ = 1;
					  $ri=0;
					}
				elseif($this->recettesItems ==  1)
					{   $this->status_ = 0;
					    $ri=1;
					}
/*
 //gad si elev la gen balans ane pase ki poko peye
$modelPendingBal=PendingBalance::model()->findAll(array('select'=>'id, balance',
	'condition'=>'student=:stud AND is_paid=0 AND academic_year=:acad',
	'params'=>array(':stud'=>$student_id,':acad'=>$previous_year),
	));
//si gen pending, ajoutel nan lis apeye a
if( (isset($modelPendingBal))&&($modelPendingBal!=null) )
 {
   foreach($modelPendingBal as $bal)
	 {
	 	$pending_balance = $bal->balance;
	 }
 }
 
 */

              $condition ='fl.status ='.$this->status_.' AND ';
              $condition_receipt = '';

              $full_paid_fees_array = array();
					$fees_paid_array = array();
					$paid_fees_array = array();
			  $arraya_size=0;


					 //tcheke si elev sa se yon bousye
					 $is_scholarshipHolder = Persons::model()->getIsScholarshipHolder($student_id,$acad_sess);

				     if($is_scholarshipHolder == 1) //se yon bousye
					   {  //tcheke tout fee ki peye net yo    // pou ajoute l/yo sou $fees_paid
					       $full_paid_fee = Billings::model()->searchFullPaidFeeByStudentId($student_id, $this->status_, $acad_sess);

					       if($full_paid_fee!=null)
                                                    {  $i=0;
                                                          foreach($full_paid_fee as $full_paid)
                                                            {
                                                                $full_paid_fees_array[0][$i] = Yii::t('app',$full_paid["fee_label"]);
                                                                $full_paid_fees_array[1][$i] = $full_paid["id_fee"];

                                                                //ajoute full paid fee
                                                                $paid_fees_array[] = $full_paid["id_fee"];
                                                                $arraya_size++;
                                                                $i++;
                                                             }
                                                         }
                                                  else
                                                  {
                                                     
                                                        
                                                        
                                                          
                                                            //tcheke tout fee ki pa null nan bous la
                                                           $notNullFee = ScholarshipHolder::model()->feeNotNullByStudentId($student_id,$acad_sess);
                                                            $notNullFee = $notNullFee->getData();
                                                                 if($notNullFee!=NULL)
                                                                      {  $j=0;
                                                                        foreach($notNullFee as $scholarship)
                                                                         {
                                                                            if($scholarship->percentage_pay==100)  
                                                                            {
                                                                                $data_fee = Fees::model()->findByPk($scholarship->fee);
                                                                               if($data_fee!=null)
                                                                                 { 
                                                                                 
                                                                                   //foreach($data_fee as $fee)
                                                                                   //{     
                                                                                       $full_paid_fees_array[0][$j] =Yii::t('app',$data_fee->fee0->fee_label);
                                                                                           $full_paid_fees_array[1][$j] = $data_fee->id;

                                                                                           $paid_fees_array[] = $data_fee->id;
                                                                                           $arraya_size++;
                                                                                           $j++;
                                                                                   //}

                                                                                   }
                                                                            }

                                                                                    
                                                                         }
                                                                         
                                                                      }
                                                                    else 
                                                                        {
                                                                           
                                                                                    $modelLevel=$this->getLevelByStudentId($student_id,$acad_sess);

                                                                                    if($modelLevel!=null)
                                                                                         $level1=$modelLevel->id;


                                                                                $criteria = new CDbCriteria(array('alias'=>'f', 'join'=>'inner join fees_label fl on(fl.id=f.fee)', 'order'=>'fee','condition'=>'fl.status='.$this->status_.' AND level='.$level1.' AND academic_period='.$acad_sess));
                                                                             $data_fee = Fees::model()->findAll($criteria);
                                                                            if($data_fee!=null)
                                                                              {  $s=0;
                                                                                foreach($data_fee as $fee)
                                                                                {     $full_paid_fees_array[0][$s] = Yii::t('app',$fee->fee0->fee_label);
                                                                                        $full_paid_fees_array[1][$s] = $fee->id;

                                                                                        $paid_fees_array[] = $fee->id;
                                                                                        $arraya_size++;
                                                                                        $s++;
                                                                                }

                                                                                }
                                                                                
                                                                        }
                                                        
                                                        
                                                        
                                                        
                                                  }

					    }

				        if($full_paid_fees_array!=null)
				                     {  
					                   if(sizeof($full_paid_fees_array)==0)
					                     {
					                     	echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][0].'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$full_paid_fees_array[1][0].'" id="'.$full_paid_fees_array[1][0].'" checked="checked" value="'.$full_paid_fees_array[1][0].'" disabled="disabled" ></div></div></div>';

					                    }
					                 else
					                    {


					                   for($k=0; $k<$arraya_size; $k++)
					                      {
 
		 echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l" >'.$full_paid_fees_array[0][$k].'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$full_paid_fees_array[1][$k].'" id="'.$full_paid_fees_array[1][$k].'" checked="checked" value="'.$full_paid_fees_array[1][$k].'" disabled="disabled" ></div></div></div>';

											}

					                    }

						 }
	                     ?>

            </td>
        </tr>

                <tr class="odd"><th><?php echo Yii::t('app','Paid Fee(s) '); ?></th></tr>
        <tr class="odd">
            <td>

         <?php //

                if($this->recettesItems ==  0)
					{ $this->status_ = 1;
					  $ri=0;
					}
				elseif($this->recettesItems ==  1)
					{   $this->status_ = 0;
					    $ri=1;
					}


              $condition ='fl.status ='.$this->status_.' AND ';
              $condition_receipt = '';



					$fees_desc = array();

			    //return id_fee, fee_name
					 $fees_paid = Billings::model()->searchPaidFeesByStudentId($student_id, $this->status_, $acad_sess);

                 		      if($fees_paid!=null)
				                     {
					                   foreach($fees_paid as $id_fees_paid)
					                      {

		 echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$id_fees_paid["fee_label"]).'</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$id_fees_paid["id_fee"].'" id="'.$id_fees_paid["id_fee"].'" checked="checked" value="'.$id_fees_paid["id_fee"].'" disabled="disabled" ></div></div></div>';

											}

								       }


	                     ?>

            </td>
        </tr>

        <tr class="odd"><th><?php echo Yii::t('app','Pending Fee(s) '); ?></th></tr>
        <tr class="odd">
        <td>

           <?php //

               if($this->status_==1)
                 {
					 $fees_desc = array();



                       if($fees_paid!=null)
                         { foreach($fees_paid as $fee)
                              $paid_fees_array[] = $fee["id_fee"];
                           }



					 $level=$this->getLevelByStudentId($student_id,$acad_sess)->id;
					                              //return id_fee, fee_name
					$fees_pending = Billings::model()->searchPendingFeesByStudentId($level, $acad_sess);

                 		      if($fees_pending!=null)
				                     {
					                   foreach($fees_pending as $fees_pen)
					                      {
			 	                              if (!in_array($fees_pen["id_fee"], $paid_fees_array))
					                          {
					                          	if($pending_balance==null)
					                          	 {
					                          	  if($fees_pen["fee_label"] !="Pending balance")
					                          	    {

		echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen["fee_label"]).'('.numberAccountingFormat($fees_pen["amount"]).')</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$fees_pen["id_fee"].'" id="'.$fees_pen["id_fee"].'"  value="'.$fees_pen["id_fee"].'" disabled="disabled" ></div></div></div>';
					                          	    }

					                          	 }
					                            else
					                              {
					                              	    if($fees_pen["fee_label"] !="Pending balance")
					                          	            $pending_balance = $fees_pen["amount"];

					                          	         echo '<div class="rmodal"> <div class=""  style="float:left; margin-left:5px; width:auto;"> <div class="l">'.Yii::t('app',$fees_pen["fee_label"]).'('.numberAccountingFormat($pending_balance).')</div><div class="r checkbox_view" style="margin-left:5px; margin-top:-3px;"><input type="checkbox" name="'.$fees_pen["id_fee"].'" id="'.$fees_pen["id_fee"].'"  value="'.$fees_pen["id_fee"].'" disabled="disabled" ></div></div></div>';
					                              	}

					                           }

											}

								       }

                    }

	                     ?>

        </td>
        </tr>




    </table>

</div>


<?php

   if(isset($student_id))
      {
?>

    <div class="span7 grid-view gbwat" style="color:#F0652E;  padding:15px; margin-left:5px; height: auto; " >

    <?php



   	  $this->message_fullScholarship=false;
   	  $this->message_scholarship=false;
   	  //check if student is a full scholarship holder
  if($this->student_id!='')
   	{
   	   //check if student is scholarship holder
	   $model_scholarship = Persons::model()->getIsScholarshipHolder($this->student_id,$acad_sess);
   	 $modelFee = Fees::model()->findByPk($this->fee_period);


																		$percentage_pay = 0;
																		 $this->internal=0;
                                                                       $partner_repondant = NULL;																		                                                           if($modelFee!=NULL)
																			{
																			          if($model_scholarship == 1) //se yon bousye
																			           {  //tcheke tout fee ki pa null nan bous la
																							   $notNullFee = ScholarshipHolder::model()->feeNotNullByStudentId($this->student_id,$acad_sess);
																			           	      $notNullFee = $notNullFee->getData();
																						   if($notNullFee!=NULL)
																							{
																							  foreach($notNullFee as $scholarship)
																			           	    	{

																				           	    	if(isset($scholarship->fee))
																				           	    	 {
																				           	    	   if($scholarship->fee == $modelFee->id)
																				           	    	    {
																				           	    	    	$percentage_pay = $scholarship->percentage_pay;
																			           	                    $this->full_scholarship = true;
																												   $this->message_fullScholarship = true;

																										   if(($partner_repondant==NULL))
																										    {
																												 if(($percentage_pay==100))
																												  { 																										                                              $this->internal=1;
																													 $partner_repondant = $scholarship->partner;
																												  }
																												 else
																												    $this->internal=0;

																											  }

																				           	    	     }
																				           	    	  }


																			           	    	 }


																							}
																						   else
																						     {
																								 //fee ka NULL tou
																								  $check_partner=ScholarshipHolder::model()->getScholarshipPartnerByStudentIdFee($this->student_id,NULL,$acad_sess);

																								  if($check_partner!=NULL)
																								   {  $check_partner = $check_partner->getData();
																							             foreach($check_partner as $cp)
																										   {   $partner_repondant = $cp->partner;
																										      $percentage_pay = $cp->percentage_pay;
																										         break;
																										   }
																								   }

																								       if(($percentage_pay==100))
																										 {
																										 	 if(($partner_repondant==NULL))
																									          {
																										         $this->internal=1;
																									          }
																									          $this->full_scholarship = true;
																								      	          $this->message_fullScholarship = true;
																									     }
																								       else
																								       	{

																									   if(($partner_repondant==NULL))
																									      {
																									      	$this->internal=0;
																										  }
																										  $this->full_scholarship = true;
																								      	          $this->message_fullScholarship = true;
																								       	}

																							 }


																			           	 }

   	                                                                           }




   	  }



?>

    	<?php

	//get level for this student
								$modelLevel = null;;
								$level=0;
								if($model->student=='')
								  {
								  	if(isset($_GET['stud'])&&($_GET['stud']!=''))
								  	  $modelLevel=$this->getLevelByStudentId($_GET['stud'],$acad_sess);
								  	}
								else
								{
								   $modelLevel=$this->getLevelByStudentId($model->student,$acad_sess);
								   }

								if($modelLevel!=null)
								 {
								 	$level=$modelLevel->id;

								   }


	

 
             //if( (isset($_GET['bil'])&&($_GET['bil']!='')) )
             //     $title = Yii::t('app', 'Update Billings'); 
             //   else
                   $title = Yii::t('app', 'Create Billings'); 
            
                                
            if( ($this->is_pending_balance==true) &&($model->exempt_fees==1) )
                 $title = Yii::t('app', 'Record exemption'); 
            else
                $model->exempt_fees=0;
	 ?>

 

 <div class="row-fluid">
    
    <div class="col-lg-11" style="margin-top: -10px;">
                         <h4><?= '[ '.$title.' ]'; ?></h4>
                    </div>


 </div>

<div class="form" id="resp_form_siges1" >
    
        <form  id="resp_form1">
<?php  
 if(($model->exempt_fees==0) ) 
  {
     
       $form->errorSummary($model);


	 //error message
	        	if( (($this->message_paymentAllow)&&($this->special_payment) ) ||(!$this->message_paymentAllow) ||($this->message_positiveBalance) ||($this->message_2paymentFeeDatepay) || ($this->message_fullScholarship)|| ($this->message_scholarship) )
				    {   echo '<div class="" style=" padding-left:0px;margin-right:0px; margin-bottom:-15px; ';//-20px; ';
				      echo '">';

				       echo '<table class="responstable" style="width:100%; background-color:#F8F8c9;  ">
					   <tr>
					    <td style="text-align:center;">';
				      }


				  if((!$this->message_paymentAllow)&&(!$this->message_positiveBalance) )
				     {
				     	echo '<span style="color:red;" >'.Yii::t('app','Fee is totaly paid.').'</span>'.'<br/>';


				     }
                                     else{
                                         if($this->special_payment)
				     	   echo '<span style="color:red;" >'.Yii::t('app','Press the SAVE button to confirm this payment.').'</span>'.'<br/>';
				     	 
                                     }

				   if($this->message_positiveBalance)
				     { echo '<span style="color:red;" >'.Yii::t('app','{name} has an old fee with a positive balance.', array('{name}'=>$model->student0->fullName)).'</span>'.'<br/>';


				     }

				     if($this->message_2paymentFeeDatepay)
				     {
				     	$fee_name='';
				     	$criteria = new CDbCriteria(array('condition'=>'id='.$model->fee_period.' AND level='.$level.' AND academic_period='.$acad_sess));

								$data_fee = Fees::model()->findAll($criteria);
	                            if($data_fee!=null)
	                              {
	                              	foreach($data_fee as $fee)
	                              	  $fee_name = $fee->fee_name;

	                              	}

				     	echo '<span style="color:red;" >'.Yii::t('app','{name} already has a record for {name_fee} on {name_datePay}. Please, update it.', array('{name}'=>$model->student0->fullName, '{name_fee}'=>$fee_name, '{name_datePay}'=>$model->date_pay )).'</span>'.'<br/>';


				     }



				   if(($this->message_fullScholarship)||($this->message_scholarship))
				     {
				     	 echo '<span style="color:red;" >'.Yii::t('app','{name} is subsidized up to {percentage}%.', array('{name}'=>$model->student0->fullName,'{percentage}'=>$percentage_pay)).'</span>'.'<br/>';


				     }


			if(  (($this->message_paymentAllow)&&($this->special_payment) ) ||(!$this->message_paymentAllow) ||($this->message_positiveBalance) ||($this->message_2paymentFeeDatepay) || ($this->message_fullScholarship)|| ($this->message_scholarship) )
			  {
			  	         echo'</td>
					       </tr>
						  </table>';
						  echo '</div>';
			  	}
        ?>
            
     <div class="row" >
        <div class="col-3 left">
            <label id="resp_form">


                   <label for="fee_name" style="color:red;"><?php echo Yii::t('app','Fee name').' *'; ?></label>

								<?php


							$criteria = new CDbCriteria(array('alias'=>'f', 'join'=>'inner join fees_label fl on(fl.id=f.fee)', 'order'=>'fee','condition'=>'fl.status='.$this->status_.' AND level='.$level.' AND academic_period='.$acad_sess));
						if(isset($_GET['id']))
							 {


							 	//if(isset($_GET['stud']))
								//  {

									 /*	$data_fees = Fees::model()->findAll($criteria);
										echo $form->dropDownList($model, 'fee_period',
										CHtml::listData($data_fees,'id','feeName'),
										array('onchange'=>'submit()','options' => array($this->fee_period=>array('selected'=>true)) ));
                                                                          */
								//		echo $form->dropDownList($model,'fee_period',loadFeeName($this->status_,$level,$acad_sess,$this->student_id), array('onchange'=>'submit()','options' => array($this->fee_period=>array('selected'=>true)) ));
								//	}
								//  else
								//     {

								     	/*$data_fees = Fees::model()->findAll($criteria);
										echo $form->dropDownList($model, 'fee_period',
										CHtml::listData($data_fees,'id','feeName'),
							array('onchange'=>'submit()'  ,'options' => array($this->fee_period=>array('selected'=>true)), 'disabled'=>'disabled'));
							*/
							echo $form->dropDownList($model,'fee_period',loadFeeName($this->status_,$level,$acad_sess,$this->student_id), array('onchange'=>'submit()'  ,'options' => array($this->fee_period=>array('selected'=>true)), 'disabled'=>'disabled'));

								    //   }



							 }
							else
							  {
								/* $data_fees = Fees::model()->findAll($criteria);
								echo $form->dropDownList($model, 'fee_period',
								CHtml::listData($data_fees,'id','feeName'),
								array('prompt'=>Yii::t('app','-- Please select fee name ---'),'onchange'=>'submit()'));
								*/
								echo $form->dropDownList($model,'fee_period',loadFeeName($this->status_,$level,$acad_sess,$this->student_id), array('prompt'=>Yii::t('app','-- Please select fee name ---'), 'onchange'=>'submit()',   ));

								}


							?>

								<?php echo $form->error($model,'fee_period'); ?>
						 </label>
        </div>


        <div class="col-3 left">
            <label id="resp_form">
                               <?php echo $form->labelEx($model,'amount_to_pay'); ?>


							<?php


							          echo $form->textField($model,'amount_to_pay',array('readonly' => true,'disabled'=>'disabled'));

							       ?>
         				 </label>
        </div>


        <div class="col-3 ">
            <label id="resp_form">

<?php echo $form->labelEx($model,'amount_pay'); ?>

<?php if(  (($model->amount_pay!='')&&(!isset($_GET['id']))&&(!$this->message_2paymentFeeDatepay))||((((!$this->message_paymentAllow)||($this->message_positiveBalance))||($this->fee_period==''))&&(!$this->special_payment))   )
          echo $form->textField($model,'amount_pay',array('readonly' => true,'disabled'=>'disabled'));
      else
          echo $form->textField($model,'amount_pay', array('size'=>60,'placeholder'=>Yii::t('app','Amount Pay'))); ?>

								<?php echo $form->error($model,'amount_pay'); ?>
                           </label>
        </div>

     </div>
            
 <?php

      if(((($this->message_paymentAllow)&&(!$this->message_positiveBalance))&&($this->fee_period!=''))||($this->special_payment))
        {

 ?>
     <div style="clear:both"></div>
   <div class="row" > 
       <div class="col-3 left">
            <label id="resp_form">

<?php echo $form->labelEx($model,'payment_method'); ?>

<?php             $default_method = defaultPaymentMethod();
             if($model->payment_method==null)
             {   if($default_method!=null)
                      $model->payment_method = $default_method;

             }

									$criteria = new CDbCriteria(array('order'=>'method_name'));

									echo $form->dropDownList($model, 'payment_method',
									CHtml::listData(PaymentMethod::model()->findAll($criteria),'id','method_name'),
									array('prompt'=>Yii::t('app','-- Please select a payment method --')));


									?>
									<?php echo $form->error($model,'payment_method');
									     if($this->message_paymentMethod)
									       { echo '<br/><span class="required">'.Yii::t('app','Please fill payment method Field.').'</span>';
									           $this->message_paymentMethod=false;
									       }
									?>
						 </label>
        </div>






     <div class="col-3 left" style="width: 30%;">
            <label id="resp_form" >

<?php echo $form->labelEx($model,'date_pay'); ?>
							<?php


	                                if((isset($_GET['id'])&&($_GET['id']!='')) || (($this->reservation==true)&&($this->remain_balance==0)) )
	                                   $date__ = $model->date_pay;
	                                else
	                                   $date__ = date('Y-m-d');


					 	   		 $this->widget('zii.widgets.jui.CJuiDatePicker',
							      array(
									 'model'=>'$model',
									 'name'=>'Billings[date_pay]',
									 'language'=>'fr',
									 'value'=>$date__,
									 'htmlOptions'=>array('size'=>60, 'style'=>'width:100% !important;','placeholder'=>Yii::t('app','Date Pay')),
										 'options'=>array(
										 'showButtonPanel'=>true,
										 'changeYear'=>true,
										 'changeYear'=>true,
	                                     'dateFormat'=>'yy-mm-dd',
										 ),
									 )
								 );


								?>
								<?php echo $form->error($model,'date_pay');
								     if($this->message_datepay)
								       { echo '<br/><span class="required">'.Yii::t('app','You must have a date for this payment.').'</span>';
								           $this->message_datepay=false;
								       }

								?>
                           </label>
        </div>

         <div class="col-3 left">
            <label id="resp_form">

					<?php echo $form->labelEx($model,'comments'); ?>
							<?php

                                echo $form->textField($model,'comments', array('size'=>60,'placeholder'=>Yii::t('app','Comments'))); 
                               // echo  $form->textArea($model,'comments',array('rows'=>1, 'cols'=>160));
                                echo $form->error($model,'comments');

								?>
                           </label>
        </div>

   </div>





<?php

  if(!isAchiveMode($acad_sess))
        {   
          if($acad == $current_acad_id)
		  {
        	//$template = '{print}{update}{delete}';

   if((!$this->message_fullScholarship)||( $this->internal==0))
     {
?>
     <center>
                            <div class="col-submit" style="margin-top: 10px; ">


                                <?php //if(!isset($_GET['id'])){
                                         if(!isAchiveMode($acad_sess))
                                             echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning')); //'Create ' avec espace a la fin POUR AVOIR UNE TRADUCTION "ENREGISTRER"

                                          $url=Yii::app()->request->urlReferrer;//$_SERVER['REQUEST_URI'];
                                              $explode_url= explode("php",substr($url,0));

                                              echo ' <a href="view?stud='.$_GET['stud'].'&ri=0&part=rec&from=stud" class="btn btn-secondary">'.Yii::t('app', 'Cancel ').'</a>';


                                ?>
     

                  </div><!-- /.table-responsive -->
</center>
     
     
<?php
                 }
       
             
              }
        }


    } //end fee !=null


    
     }  //########################## DEBU EXEMPTION #################################### 
elseif(($model->exempt_fees==1) )
{
    
         //$dataProvider=$modelFee->searchPastFeesToExempt($_GET['id'],$program,$level,$previous_year); 
  ?>
    
<br/>          

<div class="grid-view" style="width:95%; margin-top:-20px; margin-left:10px;">
 <label id="resp_form"> 

  											
   <?php 				  //And you can get values like this:
        						 
        $modelExempt = new ScholarshipHolder;
        
		 $dataProvider=Fees::model()->searchFeesToExempt($this->student_id,$level,$acad_sess); 
	     
	    		
$this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'exempt',
	'dataProvider'=>$dataProvider,
	'summaryText'=> '', 
	'showTableOnEmpty'=>'true',
	'selectableRows' =>2,
	
    'columns'=>array(
	 'id',
	// 'level',
	array('name'=>Yii::t('app','Fees'),'type' => 'raw','value'=>'Yii::t(\'app\',$data->fee_label)." (".numberAccountingFormat($data->getFeeAmount('.$this->student_id.','.$acad_sess.',$data->amount)).")"','htmlOptions'=>array('width'=>'200px')),
	
	array('name'=>Yii::t('app','Deadline'),'type' => 'raw','value'=>'ChangeDateFormat($data->date_limit_payment)','htmlOptions'=>array('width'=>'100px')),
		
	array('name'=>Yii::t('app','Amount Pay'),'type' => 'raw','value'=>'$data->getAmountPayOnFee('.$this->student_id.',$data->id)','htmlOptions'=>array('width'=>'100px')),
	
	
       array(             'class'=>'CCheckBoxColumn',   
                           'id'=>'chk',
                           'htmlOptions'=>array('width'=>'10px')
                 ),           
		
    ),
));
				
				
		
		   
			 ?>

     </label>
 </div>       
       
     
   
       
       
       
 <?php
 
   if( $dataProvider->getData()!=NULL)
      {
 ?>      
       
       
       
         <div class="col-2">
            <label id="resp_form">

					<?php echo $form->labelEx($modelExempt,'comment'); ?>                              
							<?php 
							        
                                echo  $form->textArea($modelExempt,'comment',array('rows'=>3, 'cols'=>160,'style'=>'height:26px; width:95%; display:inline;')); 
                                echo $form->error($modelExempt,'comment');
                          								
								?>
                           </label>
        </div>

<br/>

             <center>
                            <div class="col-submit">
 
                                
                                <?php 
                                                	 if(!isAchiveMode($acad_sess))
                                                	     echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'exempt','class'=>'btn btn-warning')); //'Create ' avec espace a la fin POUR AVOIR UNE TRADUCTION "ENREGISTRER"
                                         
                                         echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary')); //'Cancel ' avec espace a la fin POUR AVOIR UNE TRADUCTION "Annuler"/ sans espace=>'Retour'
                                           

                                ?>
     
                            
                  </div><!-- /.table-responsive -->
             </center>
                  
     <?php
     
          }
     
         
                
    ?>
   
  <?php                        
}  //########################## FEN EXEMPTION #################################### 
 
     
            
     if( ($this->is_pending_balance==true) )
        {
  ?>
        
             <div class="col-3-4">
            <?php echo $form->label($model,'exempt_fees'); 
		                              if($this->exempt_fees_==1)
				                          { echo $form->checkBox($model,'exempt_fees',array('onchange'=> 'submit()','checked'=>'checked'));
				                              
				                           }
						                 else
							               echo $form->checkBox($model,'exempt_fees',array('onchange'=> 'submit()'));
                 ?>
	      </div>
     <?php
     
        }   
 
 
     ?>         

                </form>
    
  
              </div>





</div>


   <?php

      } //fen isset student_id
?>




<div class="span2 photo_view gbwat" style=" padding:15px; margin-left:5px; height: auto;">
  <div style="text-align: center; padding-top:-5xp; color: #F0652E;">
    <?php
    if($modelStud->ageCalculator($birthday)!=null)
            echo '<strong>'.$modelStud->ageCalculator($birthday).Yii::t('app',' yr old').' / '.$modelStud->getRooms($student_id, $acad_sess).'</strong>';
          else
            echo $modelStud->getRooms($student_id, $acad_sess).' ';
            ?>
  </div>
    <?php

                               if($image!=null)
                                    echo CHtml::image(Yii::app()->request->baseUrl.'/documents/photo-Uploads/1/'.$image);
                                else
                                    echo CHtml::image(Yii::app()->request->baseUrl.'/css/images/no_pic.png');



    if($modelStud->active == 0)
      echo $modelStud->status.' [ '.getInactiveReason($modelStud->inactive_reason).' ]'
    ?>



</div>
<div style="clear:both"></div>
 <!-- Seconde ligne -->


<?php

   if(isset($modelStud->id))
      {
?>
<div class="row-fluid">

    <div>
        <ul class="nav nav-tabs">
    <li class="active"><a data-toggle="tab" href="#transaction_list">  <?php echo Yii::t('app','Transactions list'); ?></a></li>
     <?php
             if($this->is_pending_balance==true)
             {
      ?>
      <li class=""><a data-toggle="tab" href="#lastyear_transaction_list" ><?= Yii::t('app', 'Last year transactions list') ?> </a></li>

    <?php           
             }
    ?>
    <li ><a data-toggle="tab" href="#payment_receipt">  <?php echo Yii::t('app','Payment receipt'); ?></a></li>



        </ul>


   <div class="tab-content">

    <!--  ************************** Transactions list *************************    -->

<div id="transaction_list" class="tab-pane fade in active">


<?php

      $condition ='';
      $current_acad = Yii::app()->session['currentId_academic_year'];

$gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'billings-grid',
	'dataProvider'=>Billings::model()->searchForView($condition,$modelStud->id,$current_acad),
	'showTableOnEmpty'=>true,
	//'emptyText'=>Yii::t('app','No academic period found'),
	'summaryText'=>'',
               'mergeColumns'=>array('fee_fname'),

        'rowCssClassExpression'=>'($data->balance>0)?"balance":evenOdd($row)',

	'columns'=>array(


		array('name'=>'date_pay',
			//'header'=>Yii::t('app','Fee name'),
			'value'=>'$data->DatePay'),

        array(
		    'name'=>'Transaction ID',
			'header'=>Yii::t('app','Transaction ID'),
                        'type' => 'raw',
			'value'=>'$data->id',
                        //'htmlOptions'=>array('width'=>'125px'),
                    ),


		array('name'=>'fee_fname',
			'header'=>Yii::t('app','Fee name'),
			'type' => 'raw',
			'value'=>'$data->feePeriod->simpleFeeName." (".$data->feePeriod->academicPeriod->name_period.")"',
                        'htmlOptions'=>array('width'=>'325px'),   ),


            array('header'=>Yii::t('app', 'Expected amount'),
                  'type' => 'raw',
                  'value'=>'$data->amountToPay',
                          ),


             array('header'=>Yii::t('app', 'Amount Pay'),
                  'type' => 'raw',
                  'value'=>'$data->amountPay',
                          ),

            array('header'=>Yii::t('app', 'Balance'),'value'=>'$data->balanceCurrency'),

            array(
			'class'=>'CButtonColumn',
                        'template'=>'{update}{delete}',
                         'buttons'=>array(

                         'view'=>array(
				            'label'=>'<i class="fa fa-eye"></i>',
				            'imageUrl'=>false,
				            'url'=>'Yii::app()->createUrl("/billings/billings/view?stud=$data->student&id=$data->id&ri='.$this->recettesItems.'&part=rec&from=stud")',
				            'options'=>array( 'title'=>Yii::t('app','View') ),
				        ),
                         'update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/billings/billings/view?stud=$data->student&id=$data->id&ri='.$this->recettesItems.'&part=rec&from=stud")',
                            'options'=>array('title'=>Yii::t('app','Update')),

                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),

                            ),
                            ),


		),

	  ),
));

?>

   </div>

 <?php
         if($this->is_pending_balance==true)
         {
     ?>     
             <div id="lastyear_transaction_list" class="tab-pane">

    <?php    
                $condition ='';
              
 $gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'billings-grid',
	'dataProvider'=>Billings::model()->searchLastYearForView($condition,$modelStud->id,$acad_sess),
	'showTableOnEmpty'=>true,
	//'emptyText'=>Yii::t('app','No academic period found'),
	'summaryText'=>'',
               'mergeColumns'=>array('fee_fname'),

        'rowCssClassExpression'=>'($data->balance>0)?"balance":evenOdd($row)',

	'columns'=>array(


		array('name'=>'date_pay',
			//'header'=>Yii::t('app','Fee name'),
			'value'=>'$data->DatePay'),

        array(
		    'name'=>'Transaction ID',
			'header'=>Yii::t('app','Transaction ID'),
                        'type' => 'raw',
			'value'=>'$data->id',
                        //'htmlOptions'=>array('width'=>'125px'),
                    ),


		array('name'=>'fee_fname',
			'header'=>Yii::t('app','Fee name'),
			'type' => 'raw',
			'value'=>'$data->feePeriod->simpleFeeName." (".$data->feePeriod->academicPeriod->name_period.")"',
                        'htmlOptions'=>array('width'=>'325px'),   ),


            array('header'=>Yii::t('app', 'Expected amount'),
                  'type' => 'raw',
                  'value'=>'$data->amountToPay',
                          ),


             array('header'=>Yii::t('app', 'Amount Pay'),
                  'type' => 'raw',
                  'value'=>'$data->amountPay',
                          ),

            array('header'=>Yii::t('app', 'Balance'),'value'=>'$data->balanceCurrency'),

            array(
			'class'=>'CButtonColumn',
                        'template'=>'{update}{delete}',
                         'buttons'=>array(

                         'view'=>array(
				            'label'=>'<i class="fa fa-eye"></i>',
				            'imageUrl'=>false,
				            'url'=>'Yii::app()->createUrl("/billings/billings/view?stud=$data->student&id=$data->id&ri='.$this->recettesItems.'&part=rec&from=stud")',
				            'options'=>array( 'title'=>Yii::t('app','View') ),
				        ),
                         'update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/billings/billings/view?stud=$data->student&id=$data->id&ri='.$this->recettesItems.'&part=rec&from=stud")',
                            'options'=>array('title'=>Yii::t('app','Update')),

                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),

                            ),
                            ),


		),

	  ),
));
      
 ?>

   </div>
         
   <?php         
         }
   
   ?>      
   
   <!--  ************************** payment receipt *************************    -->

<div id="payment_receipt" class="tab-pane fade">


<div class="grid-view">

	<?php

		//Extract school name
								               $school_name = infoGeneralConfig('school_name');
                                                                                                //Extract school address
				   								$school_address = infoGeneralConfig('school_address');
                                                                                                //Extract  email address
                                               $school_email_address = infoGeneralConfig('school_email_address');
                                                                                                //Extract Phone Number
                                                $school_phone_number = infoGeneralConfig('school_phone_number');


	//get level for this student
								$level=0;
								$modelLevel=$this->getLevelByStudentId($modelStud->id,$acad_sess);
								if($modelLevel!=null)
								 {
								 	$level=$modelLevel->id;

								   }


?>


<div  id="resp_form_siges1">

        <form  id="resp_form1">


<?php

$levelName='';
         if($modelStud->id!='')
           $levelName=$this->getLevelByStudentId($modelStud->id,$acad_sess)->level_name;

         $student=$this->getStudent($modelStud->id);



?>

 <div style="width:90%;">
            <label id="resp_form" style="width:100%;">



      <?php     $pa_daksan = pa_daksan();

           echo '<div id="rpaie" style="float:left; padding:10px; border:1px solid #EDF1F6; width:98%; ">

                  <div id="header" style="display:none; ">
                     <div class="info">'.headerLogo().'<b>'.strtoupper(strtr($school_name, pa_daksan() )).'</b><br/>'.$school_address.' &nbsp; / &nbsp; '.Yii::t('app','Tel: ').$school_phone_number.' &nbsp; / &nbsp; '.Yii::t('app','Email: ').$school_email_address.'<br/></div>

                  <br/>

                  <div class="info" style="text-align:center;  margin-top:-9px; margin-bottom:-15px; "> <b>'.strtoupper(strtr(Yii::t('app','Payment receipt'), $pa_daksan )).'</b></div> ';

			echo '<br/>  </div>

			    <div class="info" >
			      <div id="stud_name" style="display:none; float:left;">'.Yii::t('app','Name').':  '.$full_name.' </div>

			           <div id="level" style="display:none; margin-left:62%;">'.Yii::t('app','Level').': '.$levelName.'</div>


			         <br/><div id="acad" style="display:none; margin-left:62%;">'.Yii::t('app','Academic year').' '.$acad_name.' </div>   ';




				if($modelStud->id!='')
				   $dataPro = Billings::model()->searchByStudentId($condition_receipt,$modelStud->id,$current_acad);
				else
				    $dataPro = Billings::model()->searchByStudentId($condition_receipt,0,$current_acad);


         echo '<br/><div style="margin-left:1%;">
			         				<table class="" style="width:99%; background-color: #E5F1F4; color: #1E65A4; -webkit-border-top-left-radius: 5px;-webkit-border-top-right-radius: 5px;-moz-border-radius-topleft: 5px;-moz-border-radius-topright: 5px;border-top-left-radius: 5px;border-top-right-radius: 5px;">
									   <tr>

									       <td style="text-align:center; font-weight: bold; "> '.Yii::t('app','Date Pay').'</td>
									       <td style="text-align:center; font-weight: bold;"> '.Yii::t('app','No.').' </td>
									       <td style="text-align:center; font-weight: bold;">'.Yii::t('app','Rubric').' </td>
									       <td style="text-align:center; font-weight: bold; "> '.Yii::t('app','Expected amount').'</td>
									       <td style="text-align:center; font-weight: bold; "> '.Yii::t('app', 'Received amount').'</td>
									       <td style="text-align:center; font-weight: bold; "> '.Yii::t('app', 'Remain to pay').'</td>
                                                                               <td style="text-align:center; font-weight: bold; "> '.Yii::t('app', 'Comments').'</td>



									    </tr>';


						           	  if($dataPro->getData()!= null)
						           	    {
						           	    	$result = $dataPro->getData();
						           	    	$i=0;
						           	    	foreach($result as $r)
											     {

											     	echo '<tr>
												           <td style="text-align:center; "> '.$r->DatePay.'</td>
												           <td style="text-align:center;"> '.$r->id.'  </td>
														   <td style="text-align:center; ">'.$r->feePeriod->simpleFeeName.' ('.$r->feePeriod->academicPeriod->name_period.')'.' </td>
														   <td style="text-align:center; "> '.$r->amountToPay.'</td>
														   <td style="text-align:center;"> '.$r->amountPay.'  </td>
														   <td style="text-align:center; ">'.$r->balanceCurrency.' </td>
                                                                                                                       <td style="text-align:center; ">'.$r->comments.' </td>

														  </tr>';



												    }



							           	      }

							             echo '  </table>
						                    </div>';




		echo '	    <br/>
			      <div style="text-indent: 200px; font-weight: bold; font-style: italic;"> &nbsp;&nbsp;&nbsp;'.Yii::t('app','Authorized signature').'</div><br/><br/>

			       </div>';

	   ?>
	   </label>
    </div>

<br/>


                            <div class="col-submit">


                                <?php  echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                                       if($dataPro->getData()!=null)
                                         {

									       echo '<a  class="btn btn-success col-4" style="margin-left:40px;margin-top:10px; " onclick="printDiv(\'rpaie\')">'. Yii::t('app','Print').'</a>';


									     echo '';

                                         }


                                              //back button
                                              $url=Yii::app()->request->urlReferrer;//$_SERVER['REQUEST_URI'];
                                              $explode_url= explode("php",substr($url,0));

                                              echo '  <a href="index/part/rec/from/stud" style="margin-left:10px;margin-top:10px;" class="btn btn-secondary">'.Yii::t('app', 'Back').'</a>';


                                ?>


                  </div><!-- /.table-responsive -->

<?php

?>


                </form>
              </div>



</div>



</div>

   </div>
        
<?php $this->endWidget(); ?>


 <!-- Modal Commentaire -->
  <div class="modal fade" id="comments" role="dialog">
    <div class="modal-dialog modal-lg">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?php echo Yii::t('app','Comments'); ?></h4>
        </div>
        <div class="modal-body">
        <?php //echo $model->comments; ?>

        <!-- Fin contenu modal -->

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app','Close'); ?></button>
        </div>
      </div>
    </div>
  </div>


<script type="text/javascript">

function printDiv(divName) {
     document.getElementById("header").style.display = "block";
     document.getElementById("stud_name").style.display = "block";
     document.getElementById("level").style.display = "block";
     document.getElementById("acad").style.display = "block";
     document.getElementById(divName).style.display = "block";
     var printContents = document.getElementById(divName).innerHTML;
     var originalContents = document.body.innerHTML;


     document.body.innerHTML = printContents;

     window.print();

     document.body.innerHTML = originalContents;
     document.getElementById("header").style.display = "none";
     document.getElementById("stud_name").style.display = "none";
     document.getElementById("level").style.display = "none";
     document.getElementById("acad").style.display = "none";

}


</script>


<?php
      }

?>
