<?php
 /*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


class SigespaymentController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	public $part='sig_p';
	public $back_url;
	
	public $balance;
	public $amount_pay;
	public $old_pay=0;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','create','update','delete','view'),
				'users'=>array(bagdor()[1]),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new SigesPayment;
		$result = null;
		$year='';
		
		if(!isset($_GET['sps_id']) ) //Al cheche id siges_payment_set pou ane a
		 {
		 	 $year= $_GET['year'];
		 	 $dataProvider=$model->searchByAcad($_GET['year']);
		 	 $data=$dataProvider->getData();
		 	 if($data!=null)
		 	  {
		 	  	  
		 	  	  foreach($data as $d)
		 	  	   {
		 	  	   	  $result =  $d->id_siges_payment_set;
		 	  	   	  $this->balance = $d->balance;
		 	  	   	   break;
		 	  	   	}
		 	  	   	
		 	  	}
		 	 else //al cheche nan siges_payment_set
		 	   {    $modelPaySet = new SigesPaymentSet;
		 	          $new_dataprov= $modelPaySet->searchByAcad($_GET['year']);
		 	          
		 	          $new_data=$new_dataprov->getData();
		 	          
		 	          if($new_data!=null)
					 	  {
					 	  	  
					 	  	  foreach($new_data as $new_d)
					 	  	   {
					 	  	   	 
					 	  	   	  $result =  $new_d->id;
					 	  	   	  $this->balance = $new_d->amount_to_pay;
					 	  	   	   break;
					 	  	   	}
					 	  	}
		 	   	
		 	   	 }
		 	
		 	$model->setAttribute('id_siges_payment_set',$result );
		  }
		else
		  {
		   	   $dataProvider=$model->searchByPaymentSet($_GET['sps_id']);
		   	  
		   	  $data=$dataProvider->getData();
		   	  
		   	   if($data!=null)
		 	    {
		 	  	  foreach($data as $d)
		 	  	   {
		 	  	   	  $this->balance = $d->balance;
		 	  	   	  $year= $d->sigesPaymentSet->academic_year;
		 	  	   	   break;
		 	  	   	}
		 	  	}
		 	 else //al cheche nan siges_payment_set
		 	   {    $modelPaySet = new SigesPaymentSet;
		 	          $new_dataprov= SigesPaymentSet::model()->findByPk($_GET['sps_id']);
		 	          
		 	           $this->balance = $new_dataprov->amount_to_pay;
		 	           
		 	           $year= $new_dataprov->academic_year;
					 	  	   	  
					 	
		 	   	
		 	   	 }
		   	   
		   	  $model->setAttribute('id_siges_payment_set',$_GET['sps_id'] );
		   }
         
         
           
          if($this->balance == null)
           {
           	    Yii::app()->user->setFlash(Yii::t('app','Warning'),'Please, set payment for current year.');
           	    $this->redirect(array('/billings/sigespaymentset/create','part'=>'sig_pset'));
           	 }
           	 
		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SigesPayment']))
		 {
			$model->attributes=$_POST['SigesPayment'];
			
			$model->setAttribute('amount_pay', $_POST['amount_pay_hide'] );
			
			$model->setAttribute('balance', $_POST['balance_hide'] );
			
			if($model->save())
				$this->redirect(array('index','year'=>$year,'part'=>'sig_p'));
		   }

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
		
		$year = $model->sigesPaymentSet->academic_year;
		
		$dataProv = $model->searchNextRecordByPaymentDate($model->payment_date);
		
		$data = $dataProv->getData();
		
		if($data!=null)
		 {
		 	Yii::app()->user->setFlash(Yii::t('app','Warning'),'Update is not allowed. Next record(s) is(are) detected.');
           	    $this->redirect(array('/billings/sigespayment/index','year'=>$year,'part'=>'sig_p'));
		   }
		else
		  {    $this->balance = $model->balance;
		
				$this->old_pay = $model->amount_pay;
				
				
		
				// Uncomment the following line if AJAX validation is needed
				// $this->performAjaxValidation($model);
		
				if(isset($_POST['SigesPayment']))
				{
					$model->attributes=$_POST['SigesPayment'];
					
					$model->setAttribute('amount_pay', $_POST['amount_pay_hide'] );
					
					$model->setAttribute('balance', $_POST['balance_hide'] );
					
					if($model->save())
						$this->redirect(array('index','year'=>$year,'part'=>'sig_p'));
				}
		
				$this->render('update',array(
					'model'=>$model,
				));
				
		  }
		  
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model=$this->loadModel($id);
		
		$year = $model->sigesPaymentSet->academic_year;
		
		$dataProv = $model->searchNextRecordByPaymentDate($model->payment_date);
		
		$data = $dataProv->getData();
		
		if($data!=null)
		 {
		 	
		 	Yii::app()->user->setFlash(Yii::t('app','Warning'),'You have to delete all records that follow this one first.');
           	    $this->redirect(array('/billings/sigespayment/index','year'=>$year,'part'=>'sig_pset'));
		   }
		else
		  {
		  	   $model->delete();
		  	
		     // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
				if(!isset($_GET['ajax']))
					$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
					
			}
			
			
	}

	
	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		
		$model=new SigesPayment('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SigesPayment']))
			$model->attributes=$_GET['SigesPayment'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SigesPayment the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SigesPayment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SigesPayment $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='siges-payment-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
