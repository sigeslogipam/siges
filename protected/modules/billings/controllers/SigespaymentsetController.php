<?php
 /*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


class SigespaymentsetController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	
	public $part='sig_pset';
	public $back_url;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('index','create','update','delete','view'),
				'users'=>array(bagdor()[1]),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{ 
		$acad=Yii::app()->session['currentId_academic_year']; 

		$model=new SigesPaymentSet;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['SigesPaymentSet']))
		{
			$model->attributes=$_POST['SigesPaymentSet'];
			
			$model->setAttribute('display_on',$_POST['SigesPaymentSet']['display_on'] );
			$model->setAttribute('comment',$_POST['SigesPaymentSet']['comment'] );
			$model->setAttribute('academic_year',$acad );
			$model->setAttribute('devise',2 );
			
			if($model->save())
				$this->redirect(array('/billings/sigespayment/create','sps_id'=>$model->id,'part'=>'sig_pset'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		//si nou gentan komanse touche, pa dako update payment_set
		$modelPaySet = new SigesPayment;
		$dataProvider = $modelPaySet->searchByPaymentSet($id);
		
		$data = $dataProvider->getData();
		if($data!=null)
		  {
		  	 Yii::app()->user->setFlash(Yii::t('app','Warning'),'Update is not allowed. This record is already in use.');
           	    $this->redirect(array('/billings/sigespaymentset/index','part'=>'sig_pset'));
		  	}
		 else
		  {
				$model=$this->loadModel($id);
		    
				// Uncomment the following line if AJAX validation is needed
				// $this->performAjaxValidation($model);
		
				if(isset($_POST['SigesPaymentSet']))
				{
					$model->attributes=$_POST['SigesPaymentSet'];
					//$model->setAttribute('display_on',$_POST['SigesPaymentSet']['display_on'] );
			        //$model->setAttribute('comment',$_POST['SigesPaymentSet']['comment'] );
			
					if($model->save())
						$this->redirect(array('index','part'=>'sig_pset'));
				}
		
				$this->render('update',array(
					'model'=>$model,
				));
		  }
		  
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	
	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		
		$model=new SigesPaymentSet('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['SigesPaymentSet']))
			$model->attributes=$_GET['SigesPaymentSet'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return SigesPayment the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=SigesPaymentSet::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param SigesPayment $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='siges-payment-set-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
