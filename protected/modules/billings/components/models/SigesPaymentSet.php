<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * This is the model class for table "siges_payment_set".
 *
  *
 * The followings are the available model relations:
 * @property AcademicPeriods $academicYear
 */
class SigesPaymentSet extends BaseSigesPaymentSet
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SigesPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		 return array_merge(
                parent::rules(), array(
                   // array(),
                   array('old_balance, amount_to_pay,academic_year', 'required'),
                   array('devise, academic_year', 'numerical', 'integerOnly'=>true),
					array('old_balance, amount_to_pay', 'numerical'),
					  array('comment', 'length', 'max'=>255),
			       array('academic_year', 'application.extensions.uniqueMultiColumnValidator'),
                   
				 array('id, old_balance, amount_to_pay, devise, academic_year,display_on,comment', 'safe', 'on'=>'search'),
				 				
		));
	}
 

public function attributeLabels()
	{
		
            return array_merge(
                    parent::attributeLabels(), 
                    array(
                        'id' => Yii::t('app','ID'),
			'old_balance' => Yii::t('app','Old Balance'),
			'amount_to_pay' => Yii::t('app','Amount To Pay'),
			'devise' => Yii::t('app','Devise'),
			'display_on'=> Yii::t('app','Display On'),
			'comment' => Yii::t('app','Comment'),
			'academic_year'=> Yii::t('app','Academic Year'),
                                 
                        )
                    );
           
	}

	
	
		/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('old_balance',$this->old_balance);
		$criteria->compare('amount_to_pay',$this->amount_to_pay);
		$criteria->compare('devise',$this->devise);
		$criteria->compare('display_on',$this->display_on,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('academic_year',$this->academic_year);
		
		$criteria->order='academic_year DESC';

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function searchByAcad($acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('old_balance',$this->old_balance);
		$criteria->compare('amount_to_pay',$this->amount_to_pay);
		$criteria->compare('devise',$this->devise);
		$criteria->compare('display_on',$this->display_on,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('academic_year',$this->academic_year);
		
		$criteria->condition='academic_year ='.$acad;

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
public function getOldBalance(){
            return $this->devise0->devise_symbol.' '.$this->old_balance; 
        }
	
   public function getAmountToPay(){
            return $this->devise0->devise_symbol.' '.$this->amount_to_pay; 
        }
		
  public function getDisplayOn(){
            return ChangeDateFormat($this->display_on); 
        }
	
	
	
	
	
}