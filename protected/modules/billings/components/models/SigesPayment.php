<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * This is the model class for table "siges_payment".
 *
 * The followings are the available columns in table 'siges_payment':
 * @property integer $id
 * @property double $id_siges_payment_set
 * @property double $amount_pay
 * @property double $balance
 * @property integer $payment_method
 * @property string $payment_date
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property PaymentMethod $paymentMethod
 * @property SigesPaymentSet $sigesPaymentSet
 */
class SigesPayment extends BaseSigesPayment
{
	
	public $total_amount;
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SigesPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rules()
	{
		 return array_merge(
                parent::rules(), array(
                   // array(),
                   array('id_siges_payment_set, amount_pay, payment_method, payment_date', 'required'),
                   array('id_siges_payment_set, payment_method', 'numerical', 'integerOnly'=>true),
					array('amount_pay, balance', 'numerical'),
					array('comment', 'length', 'max'=>255),
                  //  array('amount_pay+payment_method+payment_date', 'application.extensions.uniqueMultiColumnValidator'),
                   
				 array('id, id_siges_payment_set, amount_pay, total_amount, balance, payment_method, payment_date, comment', 'safe', 'on'=>'search'),
				 				
		));
	}
 

public function attributeLabels()
	{
		
            return array_merge(
                    parent::attributeLabels(), 
                    array(
                        'id' => Yii::t('app','ID'),
			'id_siges_payment_set' => Yii::t('app','Siges Payment Set'),
			'amount_pay' => Yii::t('app','Amount Pay'),
			'balance' => Yii::t('app','Balance'),
			'payment_method' => Yii::t('app','Payment Method'),
			'payment_date' => Yii::t('app','Payment Date'),
			'comment' => Yii::t('app','Comment'),
			                     
                        )
                    );
           
	}

	
	
		/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_siges_payment_set',$this->id_siges_payment_set);
		$criteria->compare('amount_pay',$this->amount_pay);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('payment_method',$this->payment_method);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('comment',$this->comment,true);
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	

	public function searchByAcad($acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->alias='sp';
		
		$criteria->join='inner join siges_payment_set sps on(sps.id=sp.id_siges_payment_set)';
		
		$criteria->condition='sps.academic_year='.$acad;
		
		$criteria->order='payment_date DESC';

		$criteria->compare('sp.id',$this->id);
		$criteria->compare('id_siges_payment_set',$this->id_siges_payment_set);
		$criteria->compare('amount_pay',$this->amount_pay);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('payment_method',$this->payment_method);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('comment',$this->comment,true);
		
		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	
	public function searchByPaymentSet($id)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->condition='id_siges_payment_set='.$id;
		
		$criteria->order='payment_date DESC';

		$criteria->compare('id',$this->id);
		$criteria->compare('id_siges_payment_set',$this->id_siges_payment_set);
		$criteria->compare('amount_pay',$this->amount_pay);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('payment_method',$this->payment_method);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('comment',$this->comment,true);
		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}


public function searchNextRecordByPaymentDate($date)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;
		
		$criteria->condition='payment_date  >\''.$date.'\'';
		
		$criteria->order='payment_date ASC';		
		
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
 public function getBalance(){
            return $this->sigesPaymentSet->devise0->devise_symbol.' '.$this->balance; 
        }
	
   public function getAmountPay(){
            return $this->sigesPaymentSet->devise0->devise_symbol.' '.$this->amount_pay; 
        }

  public function getPaymentDate(){
            return ChangeDateFormat($this->payment_date); 
        }

        public function getTotalPayByPaymentSet($sps_id){
            $sum = 0;
            $sql_string = "SELECT sum(amount_pay)  as total_amount FROM siges_payment WHERE id_siges_payment_set = $sps_id GROUP BY id_siges_payment_set";
            $data = SigesPayment::model()->findAllBySql($sql_string);
            foreach($data as $d){
                $sum = $d->total_amount; 
               
            }
            
          return $sum;
            
            
        }	
	
}