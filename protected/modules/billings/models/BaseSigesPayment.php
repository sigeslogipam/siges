<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * This is the model class for table "siges_payment".
 *
 * The followings are the available columns in table 'siges_payment':
 * @property integer $id
 * @property double $id_siges_payment_set
 * @property double $amount_pay
 * @property double $balance
 * @property integer $payment_method
 * @property string $payment_date
 * @property string $comment
 *
 * The followings are the available model relations:
 * @property PaymentMethod $paymentMethod
 * @property AcademicPeriods $academicYear
 */
class BaseSigesPayment extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SigesPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'siges_payment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('old_balance, amount_to_pay, amount_pay, balance, payment_method, payment_date', 'required'),
			array('id_siges_payment_set, payment_method', 'numerical', 'integerOnly'=>true),
			array('amount_pay, balance', 'numerical'),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, id_siges_payment_set, amount_pay, balance, payment_method, payment_date, comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'paymentMethod' => array(self::BELONGS_TO, 'PaymentMethod', 'payment_method'),
			'sigesPaymentSet' => array(self::BELONGS_TO, 'SigesPaymentSet', 'id_siges_payment_set'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'id_siges_payment_set' => 'Siges Payment Set',
			'amount_pay' => 'Amount Pay',
			'balance' => 'Balance',
			'payment_method' => 'Payment Method',
			'payment_date' => 'Payment Date',
			'comment' => 'Comment',
			
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('id_siges_payment_set',$this->id_siges_payment_set);
		$criteria->compare('amount_pay',$this->amount_pay);
		$criteria->compare('balance',$this->balance);
		$criteria->compare('payment_method',$this->payment_method);
		$criteria->compare('payment_date',$this->payment_date,true);
		$criteria->compare('comment',$this->comment,true);
		

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
	
	
	
}