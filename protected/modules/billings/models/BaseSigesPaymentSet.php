<?php
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

/**
 * This is the model class for table "siges_payment_set".
 *
 * The followings are the available columns in table 'siges_payment_set':
 * @property integer $id
 * @property double $old_balance
 * @property double $amount_to_pay
 * @property integer $devise
 * @property integer $academic_year
 *
 * The followings are the available model relations:
 * @property AcademicPeriods $academicYear
 * @property AcademicPeriods $devise
 */
class BaseSigesPaymentSet extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return SigesPayment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'siges_payment_set';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			//array('old_balance, amount_to_pay ', 'required'),
			array('devise, academic_year', 'numerical', 'integerOnly'=>true),
			array('old_balance, amount_to_pay', 'numerical'),
			array('comment', 'length', 'max'=>255),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, old_balance, amount_to_pay, devise, academic_year,display_on,comment', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'academicYear' => array(self::BELONGS_TO, 'AcademicPeriods', 'academic_year'),
			'devise0' => array(self::BELONGS_TO, 'Devises', 'devise'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'old_balance' => 'Old Balance',
			'amount_to_pay' => 'Amount To Pay',
			'devise' => 'Devise',
			'display_on'=>'Display On',
			'comment' => 'Comment',
			'academic_year'=>'Academic Year',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('old_balance',$this->old_balance);
		$criteria->compare('amount_to_pay',$this->amount_to_pay);
		$criteria->compare('devise',$this->devise);
		$criteria->compare('display_on',$this->display_on,true);
		$criteria->compare('comment',$this->comment,true);
		$criteria->compare('academic_year',$this->academic_year);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}