<?php
/* @var $this RecordCaseController */
/* @var $model BaseRecordCase 

$this->breadcrumbs=array(
	'Base Record Cases'=>array('index'),
	'Create',
);*/

?>
<?php
    $this->pageTitle = Yii::app()->name. ' - '.Yii::t('app','Create case Incident');
?>

<div id="dash">
    <div class="span3">
        <h2><?php echo Yii::t('app','Ajouter Visite'); ?></h2>
    </div>
    <div class="span3">
        <div class="span4">
            <?php
                $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';                       
                echo CHtml::link($images,array('/Infirmerie/recordCase/index')); 
                $this->back_url='/Infirmerie/recordCase/index';
            ?>
        </div>
    </div>
</div>

<div style="clear:both"></div>

<div class="b_mail">
    <div class="form">
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>

