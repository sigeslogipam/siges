<?php
/* @var $this RecordCaseController */
/* @var $model BaseRecordCase 

$this->breadcrumbs=array(
	'Base Record Cases'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BaseRecordCase', 'url'=>array('index')),
	array('label'=>'Create BaseRecordCase', 'url'=>array('create')),
	array('label'=>'View BaseRecordCase', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BaseRecordCase', 'url'=>array('admin')),
);*/
$acad_sess=acad_sess();
?>
<div id="dash">
    <div class="span3">
        <h2>
            <?php echo Yii::t('app','Modifier Cas Pour {name}',array('{name}'=>$model->student0->fullName)); ?>		    
        </h2> 
    </div>
    <div class="span3">
        <?php 
             if(!isAchiveMode($acad_sess))
                {               
         ?>
        <div class="span4">
            <?php
                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';                           
                echo CHtml::link($images,array('recordCase/create')); 
            ?>
        </div>
        <?php } ?>
        <div class="span4">
            <?php
                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';                            
                  if(isset($_GET['from'])){
                     if($_GET['from']=='view')
                     {
                        echo CHtml::link($images,array('/Infirmerie/recordCase/view','id'=>$_GET['id']));
                        $this->back_url='/Infirmerie/recordCase/view?id='.$_GET['id'];
                     }
                   }
                   else
                     { echo CHtml::link($images,array('/Infirmerie/recordCase/index')); 
                        $this->back_url='/Infirmerie/recordCase/index';
                     }
            ?>
      </div>
    </div>
</div>

<div style="clear:both"></div>
<div style="clear:both"></div>

</br>

<div class="b_mail">
    <div class="form">
        <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
    </div>
</div>  
