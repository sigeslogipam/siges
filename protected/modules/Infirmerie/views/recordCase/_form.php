<div class="form">
<?php
/* @var $this RecordCaseController */
/* @var $model BaseRecordCase */
/* @var $form CActiveForm */

$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
?>



<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'base-record-case-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
        <div id="resp_form_siges">
          <form class="resp_form">
                <div class="col-2">
                    <label id="resp_form">
                        <?php echo $form->labelEx($model,'student'); ?>
                         <?php echo $form->dropDownList($model, 'student',$this->getStudentname(), array('prompt'=> Yii::t('app', '-- Choisir Élève --')))?>
                        <?php echo $form->error($model,'student'); ?>
                     </label>
                </div>

	<div class="col-2">
            <label id="resp_form">
		 <?php echo $form->labelEx($model,'case_type'); ?>
                    <?php 
                      $criteria = new CDbCriteria(array('order'=>'name'));
                      echo $form->dropDownList($model, 'case_type',
                      CHtml::listData(BaseCaseType::model()->findAll($criteria),'id','name'),
                      array('prompt'=>Yii::t('app','-- Choisir Type de Cas --'),'disabled'=>false)
                      );
                    ?>
            </label>
	</div>

	<div class="col-2">
            <label id="resp_form">
                <?php echo $form->labelEx($model,'record_by'); ?>                            
                <?php                                
                    $criteria = new CDbCriteria(array('order'=>'last_name','condition'=>'is_student<>1 AND active IN (1,2)'));
                    echo $form->dropDownList($model, 'record_by', $this->getStaffname(), array('prompt'=>Yii::t('app','-- Choisir Infirmier(ère) --'),'disabled'=>false));                              
                ?>
                <?php echo $form->error($model,'record_by'); ?>
            </label>
	</div>

	<div class="col-3">
            <label id="resp_form">
		<?php echo $form->labelEx($model,'incident_date'); ?>
                 <?php                             
                    $this->widget('zii.widgets.jui.CJuiDatePicker',
                        array(
                            'model'=>$model,
                            'name'=>'BaseRecordCase[incident_date]',
                            'language' => 'fr',  
                            // 'value'=>$model->incident_date,
                            //'value'=>date('Y-m-d'),
                            'htmlOptions'=>array('size'=>60, 'style'=>'width:100% !important'),
                                                'options'=>array(
                                                'showButtonPanel'=>true,
                                                'yearRange'=>'2012:2100',    
                                                'changeYear'=>true,                                      
                                                //'changeYear'=>true,
                                               'dateFormat'=>'yy-mm-dd',   
                                                ),
                                        )
                                );?>                
		<?php echo $form->error($model,'incident_date'); ?>
            </label>
	</div>	

	<div class="col-2">
            <label id="resp_form">
		<?php echo $form->labelEx($model,'case_description'); ?>
		<?php echo $form->textArea($model,'case_description',array('rows'=>6, 'cols'=>50)); ?>
		<?php echo $form->error($model,'case_description'); ?>
            </label>
	</div>

	<div class="col-2">
            <label id="resp_form">
		<?php echo $form->labelEx($model,'final_decision'); ?>
		<?php echo $form->textField($model,'final_decision',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'final_decision'); ?>
            </label>
	</div>


	<div class="col-submit">
                  <?php if(!isset($_GET['id'])){
                        if(!isAchiveMode($acad_sess))
                            echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning'));

                        echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                       }
                        else
                          {  if(!isAchiveMode($acad_sess))
                             echo CHtml::submitButton(Yii::t('app', 'Save'),array('name'=>'update','class'=>'btn btn-warning'));

                             echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));

                            } 
                          //back button   
                            $url=Yii::app()->request->urlReferrer;
                            $explode_url= explode("php",substr($url,0));

                           if(!isset($_GET['from']))
                              echo '<a href="'.$explode_url[0].'php'.$this->back_url.'" class="btn btn-secondary">'.Yii::t('app', 'Back').'</a>';

                            

               ?>
            </div>



</form>
</div><!-- form -->

</div>
<?php $this->endWidget(); ?>