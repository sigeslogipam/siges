<?php
/* @var $this RecordCaseController */
/* @var $data BaseRecordCase */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('student')); ?>:</b>
	<?php echo CHtml::encode($data->student); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('case_type')); ?>:</b>
	<?php echo CHtml::encode($data->case_type); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('record_by')); ?>:</b>
	<?php echo CHtml::encode($data->record_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('incident_date')); ?>:</b>
	<?php echo CHtml::encode($data->incident_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('academic_period')); ?>:</b>
	<?php echo CHtml::encode($data->academic_period); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('exam_period')); ?>:</b>
	<?php echo CHtml::encode($data->exam_period); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('case_description')); ?>:</b>
	<?php echo CHtml::encode($data->case_description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('final_decision')); ?>:</b>
	<?php echo CHtml::encode($data->final_decision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_created')); ?>:</b>
	<?php echo CHtml::encode($data->date_created); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_updated')); ?>:</b>
	<?php echo CHtml::encode($data->date_updated); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('created_by')); ?>:</b>
	<?php echo CHtml::encode($data->created_by); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('updated_by')); ?>:</b>
	<?php echo CHtml::encode($data->updated_by); ?>
	<br />

	*/ ?>

</div>