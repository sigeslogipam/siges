<?php
    $exam_period = '';
    $current_acad_id = 0;
    $all_case = '';
    $current_year= currentAcad();
    $ct = new BaseCaseType();
    
    if($current_year!=null)
        $current_acad_id = $current_year->id;
    
    $acad_sess = acad_sess();
    $acad = Yii::app()->session['CurrentId_academic_year'];
    
    function evenOdd($num)
    {
        ($num % 2==0) ? $class = 'odd' : $class = 'even';
        return $class;
    }
?>
<div id="dash">
    <div class="span3">
        <h2>
            <?php
                echo CHtml::link($model->student0->fullname, Yii::app()->createUrl("/academic/persons/viewForReport", array("id"=>$model->student0->id, "pg"=>"lr", "pi"=>"no", "isstud"=>1, "from"=>"stud")));
            ?>
        </h2>
    </div>
    <div class="span3">
        <?php
            if(!isAchiveMode($acad_sess))
            {                
              if($acad == $current_acad_id){       
        ?>
               
        <?php } }?>
        <div class="span4">
            <?php
                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app', 'Add').'</i>';
                echo CHtml::link($images, array('recordCase/create'));
            ?>
        </div>
    <!--     <div class="span4">
            <?php
              // $images = '<i class="fa fa-edit">&nbsp;'.Yii::t('app','Update').'</i>';                              
              // echo CHtml::link($images,array('recordCase/update/','id'=>$model->id,'from'=>'view'));
            ?>
        </div>-->
         <div class="span4">
            <?php
                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';              
                  echo CHtml::link($images,array('recordCase/index'));
             ?>
        </div>
    </div>
</div>
<div class="row-fluid">
   
    <div class="span6 grid-view">
        <?php
            $this->widget('bootstrap.widgets.TbDetailView', array(
                'data'=>$model,
                'attributes'=>array(
                    //'BaseCaseType.name',
                    //'record_by',
                    'incident_date',
                ),
            ));
        ?>
        <table class="detail-view table table-striped">
            <tr class="odd">
                <th>
                    <?php echo Yii::t('app', 'Description');?>
                </th>
                <td>
                    <?php
                        echo '<span data-toggle="modal" data-toggle="tooltip"> '.$model->case_description.'</span>';
                    ?>
                </td>
            </tr>
            <tr class="even">
                <th>
                    <?php echo Yii::t('app', 'Décision');?>
                </th>
                <td>
                    <?php
                        if($model->final_decision != ''){
                            echo '<span data-toggle="modal" data-toggle="tooltip"> '.$model->final_decision.'</span>';
                        }
                        else echo '';
                    ?>
                </td>
            </tr>
        </table>
    </div>
    <div class="span2 photo_view">
        <?php
            $modelStud = new Persons;
            if($modelStud->ageCalculator($model->student0->birthday)!= null)
                echo '<strong>'.$modelStud->ageCalculator($model->student0->birthday).Yii::t('app', ' yr old').' /'
                    .$modelStud->getRooms($model->student0->id, $acad_sess).'</strong>';
            else
                echo $modelStud->getRooms($model->student0->id, $acad_sess).' ';
            if($model->student0->image!=null)
                echo CHtml::image(Yii::app()->request->baseUrl.'/documents/photo-Uploads/1/'.$model->student0->image);
            else
                echo CHtml::image(Yii::app()->request->baseUrl.'/css/images/no_pic.png');
        ?>
    </div>
</div>
<div  class="row-fluid">
    <div>
        <ul class="nav nav-tabs">
            <li class="active"><a data-toggle="tab"><?php echo Yii::t('app', 'Historique de Cas')?></a></li>
        </ul>
        <?php
            //$modelCase = new BaseRecordCase;
            //$ac = $modelCase->searchStudent($acad_sess, $model->student0->id)->getData();
            
            $sql  = "SELECT * FROM `record_case` WHERE `student` = ".$model->student0->id;           
            $studData = BaseRecordCase::model()->findAllBySql($sql);
            $rcrdData = null;
            $i = 0;
        ?>
        <div class="tab-content">
            <div id="home" class="tap-pane fade in active">
                <div class="grid-view">
                    <table class="items">
                        <thead>
                            <tr>
                                <th style="width:5px;">
                                    <?php echo Yii::t('app', '#');?>
                                </th>
                                <th style="width:20px;">
                                    <?php echo Yii::t('app', 'Date de Visite');?>
                                </th>                               
                                <th style="width:80px;">
                                    <?php echo Yii::t('app', 'Type de Cas');?>
                                </th>
                                <th style="width:100px;">
                                    <?php echo Yii::t('app', 'Description');?>
                                </th>
                                <th style="width:100px;">
                                    <?php echo Yii::t('app', 'Décision');?>
                                </th>
                            </tr>
                        </thead>
                        <?php
                            foreach($studData as $std){
                        ?>
                        <tr class="<?php echo evenOdd($i);?>">
                            <td style="width:5px;"><?php echo $i+1;?></td>
                            
                            <td style="width:20px;">
                                <a href="<?php echo $std->id; ?>">
                                    <?php echo $rcrdData = $std->incident_date; ?>
                                </a>
                            </td>   
                            
                            <td style="width:80px;">
                                <a href="<?php echo $std->id; ?>">
                                    <?php echo $rcrdData = $std->caseType->name;?>
                                </a>
                            </td>
                            <td style="width:100px;"><?php echo $rcrdData = $std->case_description;?></td>
                            <td style="width:100px;"><?php echo $rcrdData = $std->final_decision;?></td>
                            <td style="width:20px;">                               
                                <a href="<?= Yii::app()->baseUrl;?>/index.php/Infirmerie/recordCase/delete?id=<?= $std->id;?>" 
                                   method="POST" onclick="confirm('Voulez-vous supprimer cet enregistrement de cas?')">
                                    <i class="fa fa-trash"></i>
                                </a>                                
                            </td>
                        </tr>
                            <?php $i++; }?>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal Description Incident -->
<div class="modal fade" id="incident" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title"><?php echo Yii::t('app', 'Case Description'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $model->case_description; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app','Close'); ?></button>
            </div>
        </div>
    </div>
</div>

<!-- Modal Final Decision -->
<div class="modal fade" id="decision" role="dialog">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</https://www.google.com/search?q=create&oq=create&aqs=chrome..69i57j69i60l3&sourceid=chrome&ie=UTF-8createbutton>
                <h4 class="modal-title"><?php echo Yii::t('app','Décision'); ?></h4>
            </div>
            <div class="modal-body">
                <?php echo $model->final_decision; ?>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"><?php echo Yii::t('app','Close'); ?></button>
            </div>
        </div>
    </div>
</div>
