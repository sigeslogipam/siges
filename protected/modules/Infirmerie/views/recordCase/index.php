<?php
    

    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile($baseUrl."/css/AdminLTE.min.css");
    $cs->registerCssFile($baseUrl."/css/new-dashboard.css");
    $cs->registerCssFile("https://use.fontawesome.com/releases/v5.5.0/css/all.css");
    
    $current_acad_id = 0;
    $month = '';
    $year = ''; 
    $template = '';
    $tit = '';
    $tot_tab_case = '';
    
    $modelC = new BaseCaseType;
    
    $current_year = currentAcad();
    
    if($current_year!=null)
        $current_acad_id = $current_year->id;
    
    $acad_sess = acad_sess();
    $acad=Yii::app()->session['currentId_academic_year'];
    $siges_structure = infoGeneralConfig('siges_structure_session');
    $display_announcement_in_menu = infoGeneralConfig('display_announcement_in_menu');
    $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
    
    if($current_acad=='')
    {
        $condition = '';
        $condition1 = '';
    }
    else
    {
        if($acad!=$current_acad->id)
        {
            $condition = 'p.active IN(1,2) AND ';
            $condition1 ='active IN(1,2) AND ';
        }
        else
        {
             $condition = 'p.active IN(1,2) AND ';
             $condition1 ='active IN(1,2) AND ';
        }
    }
?>
<div class="discv">
    <div class="dash">
        <div class="span1">
            
        </div>
    <?php if(!isAchiveMode($acad_sess)){
            if($acad == $current_acad_id){
               $template = '{update}{delete}';  
        ?>
        
        <?php 
              }
                else { $template = '';  }
              }
        ?>
        <div class="span4">
            <?php
                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app', 'Ajouter Visite').'</i>';
                echo CHtml::link($images, array('recordCase/create'));
            ?>
        </div>
    </div>
</div>
<?php
    $acad_name=Yii::app()->session['currentName_academic_year'];
    $month = getMonth(date('Y-m-d'));
    $month_name = getLongMonth($month);
    $year = getYear(date('Y-m-d'));
    
?>
<?php
    $tot_case = 0;
    $string_sql_case = 'SELECT count(case_type) AS total, case_type FROM record_case rc INNER JOIN case_type ct on(ct.id = rc.case_type) WHERE MONTH(incident_date)= \''.$month.'\' and YEAR(incident_date)= \''.$year.'\'';
    $data_case = BaseRecordCase::model()->findAllBySql($string_sql_case);
    if($data_case!=null)
        {
            foreach ($data_case as $dc)
            {
                $tot_case = $dc->case_type;
            }
        }
    
    $string_sql_tot_case = 'SELECT count(case_type) as total, case_type, ct.name as case_name FROM record_case rc INNER JOIN case_type ct on(ct.id = rc.case_type) WHERE MONTH(incident_date)= \''.$month.'\' and YEAR(incident_date)= \''.$year.'\' group by case_type limit 10';
    $data_case1 = BaseRecordCase::model()->findAllBySql($string_sql_tot_case);
    $category_case = '';
    $tot_category_case = '';
    $s=0;
    if($data_case1!=null)
    {
        foreach($data_case1 as $dt1)
        {
            if($s==0)
            {
                // $category_case = $dt1->case_name;
               //$s=1;
            } 
            else
            {
                //$category_case = $category_case.','.$dt1->case_name;
                
            }
        }
    }
    
    //Nombre de Cas par mois
    
    $string_sql_case_m = 'SELECT count(case_type) AS total, MONTH(incident_date) as month FROM record_case rc group by MONTH(incident_date) ORDER BY incident_date ASC';
    $data_case2 = BaseRecordCase::model()->findAllBySql($string_sql_case_m);
    $month_case = '';
    $tot_case = '';
    $s=0;
    
    if($data_case2!=null)
    {
        foreach($data_case2 as $dc2)
        {
            if($s==0)
            {
                //$month_case = getShortMonth($dc2->month);
                $tot_case = $dc2->total;
                //$s=1;
            }
            else
            {
                //$month_case = $month_case.','.getShortMonth($dc2->month);
               $tot_case = $dc2->total;
            }
        }
    }
?>
<div class="container-fluid" style="text-align: center;">
    <div class="row-fluid">
        <div class="span8">
            <?php
                $criteria = new CDbCriteria;
                $criteria->select = 'count(*) as total_stud';
                
                $criteria->condition = $condition1.'is_student=:item_name';
                $criteria->params = array(':item_name'=>1);
                $total_stud = Persons::model()->find($criteria)->count();
                
                $tot_stud_s=0;
                $female_s = 0;
                $male_s = 0;

                $tot_stud=0;
                $female = 0;
                $male = 0;
                
                //teachers
                $tot_teach_s=0;
                $female1_s = 0;
                $male1_s = 0;

                $tot_teach=0;
                $female1 = 0;
                $male1 = 0;
                
                //employees
                $tot_emp_s=0;
                $female2_s = 0;
                $male2_s = 0;

                $tot_emp=0;
                $female2 = 0;
                $male2 = 0;
                
                $dataProvider = Persons::model()->searchStudentsReport($condition1, $acad_sess);
                if($dataProvider->getItemCount()!=0)
                    $tot_stud = $dataProvider->getItemCount();
                
                $person = $dataProvider->getData();
                foreach($person as $stud)
                {
                    if($stud->gender==1)
                    {
                        $female++;
                    }
                    elseif($stud->gender==0)
                    {
                        $male++;
                    }
                }
                // Fin Student Data
                
                // Teacher Data
                $dataProvider1 = Persons::model()->searchTeacherReport($condition1, $acad_sess);
                if($dataProvider1->getItemCount()!=0)
                    $tot_teach =$dataProvider1->getItemCount();
                
                //recupérer la diff des sexes
                $person = $dataProvider1->getData();
                
                foreach($person as $teacher)
                {
                    if($teacher->gender==1)
                        $female1++;
                    elseif($teacher->gender==0)
                        $male1++;
                }
                //Fin Teacher
                
                // Employées Data
                $dataProvider2 = Persons::model()->searchEmployeeReport($condition1, $acad_sess);
                if($dataProvider2->getItemCount()!=0)
                    $tot_emp =$dataProvider2->getItemCount();
                
                $person = $dataProvider2->getData();
                
                foreach($person as $employee)
                {
                    if($employee->gender==1)
                        $female2++;
                    elseif($employee->gender==0)
                        $male2++;
                }
                // Fin Employee
                
                $tot_pers = $tot_stud + $tot_emp + $tot_teach;
                $tot_fem = $female  + $female1 + $female2;
                $tot_mal = $male  + $male1 + $male2;
                
                //Stat sur Rooms
                $tot_rooms=0;
                $countRooms = Rooms::model()->searchReport();
                if($countRooms->getItemCount()!=0)
                    $tot_rooms = $countRooms->getItemCount();
                
                //Stat sur Subjects
                $tot_sub = 0;
                $countSubjects = Subjects::model()->searchReport();
                if($countSubjects->getItemCount()!=0)
                    $tot_sub = $countSubjects->getItemCount();
                
                //Stat sur Courses
                $countCourses = null;
                $tot_course=0;
                if($acad!='')
                    $countCourses = Courses::model()->searchReport($condition1,$acad_sess);
                
                if($countCourses->getItemCount()!=0)
                    $tot_course = $countCourses->getItemCount();
                
                //echo $tot_case;
            ?>
            
           <!-- <div class="span4 ti-bwat">
                <div class="small-box bg-green" data-html="true" rel="tooltip">
                    <div class="inner">
                    <h3>
                        <span class="teks-blan"><?= $tot_case; ?></span>                        
                        <span class="piti-ekriti teks-blan"><?= Yii::t('app', 'Cas')?></span>
                    </h3>
                        
                    <p class="nom-piti teks-blan">

                    </p>
                    <p class="nom-piti teks-blan">

                    </p>
                   </div>
                    <div class="icon">
                        <i class="fas fa-building"  style="padding-top: 20px"></i>
                    </div>
                </div>
            </div>       
            
            
            </br>
            <div id="container1"  style=" width: 100%; height: 400px;">
                
            </div> -->
            
            <div id="container" style="width:100%; height:400px;"></div>
            
            </div>
           
            <!-- Colonne  à droite-->
            <div class="kolonn_d span4">
                <div class="row-fluid">
                    <h4> 10 <?= Yii::t('app', 'Derniers Cas')?></h4>
                    <?php
                        $modelP = new Persons;           
                        
                        $data_provider = $model->search($acad_sess);
                        //$data_provider2 = $modelC->search($acad_sess);
                        
                        if($data_provider!=null)
                        {
                            $data_provider = $data_provider->getData();
                            //$data_provider2 = $data_provider2->getData();
                            
                            $kanpela=0;
                            foreach($data_provider as $data)
                            {
                                $kanpela++;
                                echo '<div class="kolonn_lis"> <span style="font-weight:bold;">'.$data->student0->first_name.' '.$data->student0->last_name.'</span> ('.$modelP->getShortRooms($data->student0->id,$acad_sess).')'
                                        .'<br/> '
                                       .'<span class="case_name">'.$data->caseType->name.'</span>'
                                        .'<br/> '
                                        .'<span class="case_name">'.$data->incident_date.'</span>';
                                
                                if($acad == $current_acad_id)
                                {
                                    echo '<span style="float:right; margin-right:30px;">'
                                            .CHtml::link('<span class="fa fa-pencil-square-o"></span>', Yii::app()->createUrl("Infirmerie/recordCase/update",array("id"=>$data->id)))
                                        .'</span> ';
                                }
                                echo '</div>';
                                if($kanpela==10)
                                    break;
                            }
                        }
                    ?>
                </div>
            </div>        
     </div>
        
        <br/>
        <div style="clear: both"></div>
        <div class="grid-view">
            <br/>
            <div class="search-form">
                <?php
                    $case_type_name = " ";
                    $modelC = new BaseCaseType();
                    $case_type_name = $modelC->name;
                    
                    $gridview = $this->widget('bootstrap.widgets.TbGridView', array(
                        'id'=>'base-record-case-grid',
                        'summaryText'=>'',
                        'dataProvider'=>$model->search($acad_sess),
                        
                        'columns'=>array(
                            array(
                                'header'=> Yii::t('app', 'First Name'),
                                'type'=>'raw',
                                'value'=>'CHtml::link($data->student0->first_name, Yii::app()->createUrl("Infirmerie/recordCase/view", array("id"=>$data->student)))',
                            ),
                            array(
                                'header'=> Yii::t('app', 'Last Name'),
                                'type'=>'raw',
                                'value'=>'CHtml::link($data->student0->last_name, Yii::app()->createUrl("Infirmerie/recordCase/view",array("id"=>$data->student)))',
                            ),
                            array(
                                'name'=> 'Type de Cas',  
                                'type'=> 'raw',
                                'htmlOptions'=> array('width'=>'200px'),
                                'value'=> '$data->caseType->name'
                            ),
                            array(
                                'name'=>'Date de Visite',
                                'value'=>'$data->incident_date'
                            ),
                            array(
                                'class'=>'CButtonColumn',
                                'template'=>$template,
                                'buttons'=>array(
                                    'update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                                        'imageUrl'=>false,
                                        'options'=>array('title'=> Yii::t('app', 'Update')),
                                        ),
                                    'delete'=>array(
                                        'label'=>'<span class="fa fa-trash-o"></span>',
                                        'imageUrl'=>false,
                                        'options'=>array('title'=> Yii::t('app', 'Delete')),
                                    ),
                                ),
                            ),
                        ),
                    ));
                ?>
                
            </div>
            <?php 
                   $string_sql_cycle = "SELECT COUNT( DISTINCT gpStudent.students) AS Total,Cycle, Section
                                                  FROM room_has_person rhp
                                                  INNER JOIN level_has_person lhp ON(lhp.students=rhp.students AND rhp.academic_year=lhp.academic_year )
                                                   INNER JOIN ( SELECT rhp1.students,c.cycle_description as Cycle, s.section_name AS Section
                                                                FROM room_has_person rhp1 INNER JOIN level_has_person lhp1 ON(lhp1.students=rhp1.students and rhp1.academic_year=lhp1.academic_year) INNER JOIN section_has_cycle shc ON(shc.level=lhp1.level AND shc.academic_year=lhp1.academic_year) INNER JOIN sections s ON(s.id=shc.section) INNER JOIN cycles c ON(shc.cycle=c.id)
                                                                  WHERE rhp1.academic_year=$acad) gpStudent
                                                             ON rhp.students=gpStudent.students
                                                 GROUP BY cycle";
                    
                   $data_case3 = RoomHasPerson::model()->findAllBySql($string_sql_cycle);
                   $tab_cycle = '';
                   $tot_tab_cycle = '';
                   $s3= 0;
                   
                   if($data_case3 != null)
                   {
                       foreach ($data_case3 as $dc3)
                       {
                           if($s3==0)
                           {
                               $tot_tab_cycle = $tab_cycle;
                               $s3=1;
                           }else
                           {
                                $tot_tab_cycle = $tot_tab_cycle.'/'.$tab_cycle;
                           }
                       }
                   }
                    echo '<input type="hidden" id="data_month_case" value="'.$month_case.'" />';
                    echo '<input type="hidden" id="tot_case" value="'.$tot_case.'" />';
                    echo '<input type="hidden" id="data_category_case" value="'.$category_case.'" />';
                    echo '<input type="hidden" id="tot_category_case" value="'.$tot_category_case.'" />';
                    echo '<input type="hidden" id="tot_tab_case" value="'.$tot_tab_case.'" />';
                    echo '<input type="hidden" id="cycle" value="'.$tot_tab_cycle.'" />';
                   
                    
                   // echo '<input type="hidden" id="titleAbs" value="'.Yii::t('app','Case').'" />';
                   // echo '<input type="hidden" id="yaxis" value="'.Yii::t('app','Number of student').'" />';
                   // echo '<input type="hidden" id="title_inf" value="'.Yii::t('app','Cases by month').'" />';
                    //echo '<input type="hidden" id="subtitle_inf" value=" " />';
                    //echo '<input type="hidden" id="title_cycle" value="'.Yii::t('app','Number of student by Section and Cycle').'" />';
                    
                    $monthCase = array(
                        'name'=> 'Table Case',
                        'data'=> $data_case2
                    );
                    
                   // echo '<input type="hidden" id="data_month_case" value="'.$monthCase.'" />';
                ?>            
            
            
        </div>   
             <script>
                $(document).ready(function()
                {
                    //var mwa = null; 
                                     
                    
                    $('.items').DataTable({
                        pageLength: 25,
                        responsive: true,
                        dom: '<"html5buttons"B>lTfgitp',
                        language: {
                            processing: "<?= Yii::t('app', 'Processing...')?>",
                            search: '<i class="fa fa-search"></i>',
                            searchPlaceholder: "<?= Yii::t('app','Search')?>",
                            lengthMenu:  " _MENU_ ",
                            info: "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                            infoEmpty: "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                            infoFiltered: "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                            infoPostFix: "",
                            loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                            zeroRecords: "<?= Yii::t('app','No element to show')?>",
                            emptyTable: "<?= Yii::t('app','No data to show')?>",
                            
                            paginate:{
                                first: "<?= Yii::t('app','First'); ?>",
                                previous: "<?= Yii::t('app','Previous'); ?>",
                                next: "<?= Yii::t('app','Next'); ?>",
                                last: "<?= Yii::t('app','Last'); ?>"
                            },
                            
                            aria:{
                                sortAscending: ": activer pour trier la colonne par ordre croissant",
                                sortDescending: ": activer pour trier la colonne par ordre décroissant"
                            },
                        },
                        
                        buttons:[
                                {extend: 'excel', title: "<?= $tit;?>" },
                                
                        ]
                    });
                    
                    $.get('<?= $baseUrl?>/index.php/Infirmerie/recordCase/getdatabymonth',{},function(data){
                        var data_month = data;
                        //alert(data_month);
                        var bon_data = JSON.parse(data_month);
                        //alert(bon_data);
                   
                    
                    $.get('<?= $baseUrl?>/index.php/Infirmerie/recordCase/getmonth',{},function(data){
                           var mwa = data;  
                           var bon_mwa = JSON.parse(mwa);
                           //alert(bon_mwa);
                           
                      $('#container').highcharts({
                            chart: {
                                type: 'column'
                            },
                            title: {
                                text: 'Nombre de Cas par mois'
                            },
                            xAxis: {
                                categories: bon_mwa 
                            },
                            yAxis: {
                                title: {
                                    text: ' '
                                }
                            },
                            series: [{
                                name: 'Total Cas',
                                colorByPoint: true,
                                data: bon_data,  //[1,2,3,4,5,6,7,8,9,10,11,12],
                                dataLabels:{
                                    enabled: true,
                                    rotation: -90,
                                    color : '#FFFFFF',
                                    align: 'right',
                                    //format: '{point.y:.1f}',
                                    y:5,
                                    style:{
                                        fontSize:'9px',
                                        fontFamily:'Verdana, sans-serif'
                                    },
                                },
                                showInLegend: false
                            }]
                        }); 
                      
                    });  
                       
                });
                       
                        
                        
            });
            </script>
</div>