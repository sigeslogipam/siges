<?php
/* @var $this RecordCaseController */
/* @var $model BaseRecordCase */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
            <?php echo $form->textField($model,'student_last_name',array('size'=>45,'maxlength'=>45,'placeholder'=>Yii::t('app','Student last name'))); ?>
            <?php echo $form->textField($model,'record_by',array('size'=>45,'maxlength'=>45,'placeholder'=>Yii::t('app','Incident recorder'))); ?>
            <?php echo CHtml::submitButton(Yii::t('app','Search')); ?>
	</div>
    
<?php $this->endWidget(); ?>

</div><!-- search-form -->