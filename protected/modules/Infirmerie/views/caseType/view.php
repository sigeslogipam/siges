<?php
/* @var $this CaseTypeController */
/* @var $model BaseCaseType 

$this->breadcrumbs=array(
	'Base Case Types'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List BaseCaseType', 'url'=>array('index')),
	array('label'=>'Create BaseCaseType', 'url'=>array('create')),
	array('label'=>'Update BaseCaseType', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete BaseCaseType', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage BaseCaseType', 'url'=>array('admin')),
);*/
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
?>

<div id="dash">
    <div class="span3">
        <h2>
            <?php echo Yii::t('app','{name} ', array('{name}'=>$model->name)); ?>		    
        </h2> 
    </div>
    <div class="span3">
        <?php 
            if(!isAchiveMode($acad_sess))
              {       
        ?>
        
        <div class="span4">
             <?php
                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                echo CHtml::link($images,array('caseType/create'));
             ?>
        </div>
        
        <div class="span4">
            <?php
                $images = '<i class="fa fa-edit">&nbsp;'.Yii::t('app','Update').'</i>';
                echo CHtml::link($images,array('caseType/update/','id'=>$model->id,'from'=>'view'));
           ?>
        </div>
        <?php } ?>
        
        <div class="span4">
            <?php
                $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                echo CHtml::link($images,array('caseType/index'));                     
            ?>
        </div>
    </div>
</div>

<div style="clear:both"></div>
<div class="span3"></div>
<div style="clear:both"></div>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'description',
	),
)); ?>
