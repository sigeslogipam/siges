<div class="form">
<?php
/* @var $this CaseTypeController */
/* @var $model BaseCaseType */
/* @var $form CActiveForm */

$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
?>



<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm', array(
	'id'=>'base-case-type-form',
	'enableAjaxValidation'=>false,
)); ?>

    
	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>
<div  id="resp_form_siges">
    <form class="resp_form">
	<div class="col-3">
            <label id="resp_form">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>255)); ?>
		<?php echo $form->error($model,'name'); ?>
            </label>
	</div>

	<div class="col-3">
            <label id="resp_form">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>2050)); ?>
		<?php echo $form->error($model,'description'); ?>
            </label>
	</div>
        
        <div class="col-submit">
                  <?php if(!isset($_GET['id'])){
                        if(!isAchiveMode($acad_sess))
                            echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning'));

                        echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                       }
                        else
                          {  if(!isAchiveMode($acad_sess))
                               echo CHtml::submitButton(Yii::t('app', 'Save'),array('name'=>'update','class'=>'btn btn-warning'));

                             echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));

                            } 
                          //back button   
                             $url=Yii::app()->request->urlReferrer;//$_SERVER['REQUEST_URI'];
                             $explode_url= explode("php",substr($url,0));

                            

               ?>
            </div>



<!-- form -->
</form>
</div>
        
</div>
<?php $this->endWidget(); ?>