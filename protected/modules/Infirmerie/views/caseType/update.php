<?php
/* @var $this CaseTypeController */
/* @var $model BaseCaseType 

$this->breadcrumbs=array(
	'Base Case Types'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List BaseCaseType', 'url'=>array('index')),
	array('label'=>'Create BaseCaseType', 'url'=>array('create')),
	array('label'=>'View BaseCaseType', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage BaseCaseType', 'url'=>array('admin')),
);*/

$acad_sess=acad_sess();
?>

<div id="dash">
    <div class="span3">
            <h2>
                <?php echo Yii::t('app','Update {name}',array('{name}'=>$model->name)); ?>		    
            </h2> 
    </div>
    
    <div class="span3">
        <?php 
            if(!isAchiveMode($acad_sess))
               {                   
        ?>
        
        <div class="span4">
            <?php
                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';                           
                 echo CHtml::link($images,array('caseType/create')); 
               ?>
        </div>
        <?php } ?>
        
        <div class="span4">
            <?php
                 $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';                           
                 if(isset($_GET['from'])){
                    if($_GET['from']=='view')
                    {
                        echo CHtml::link($images,array('/Infirmerie/caseType/view','id'=>$_GET['id']));
                        $this->back_url='/Infirmerie/caseType/view?id='.$_GET['id'];
                    }
                  }
                  else
                    { echo CHtml::link($images,array('/Infirmerie/caseType/index')); 
                       $this->back_url='/Infirmerie/caseType/index';
                    }
            ?>
        </div>
    </div>
</div>

<div style="clear:both"></div>
</br>

<div class="b_mail">
    <div class="form">
        <?php echo $this->renderPartial('_form',array('model'=>$model)); ?>
    </div>
</div> 
