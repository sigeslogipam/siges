<?php
/* @var $this CaseTypeController */
/* @var $dataProvider CActiveDataProvider 

$this->breadcrumbs=array(
	'Base Case Types',
);

$this->menu=array(
	array('label'=>'Create BaseCaseType', 'url'=>array('create')),
	array('label'=>'Manage BaseCaseType', 'url'=>array('admin')),
);
?>

<h1>Base Case Types</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
));
 * 
 */

$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];

$tit='';
$template ='';

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('base-case-type-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
 ?>

<div id="dash">
    <div class="span3">
        <h2>
          <?php echo Yii::t('app','Gestion Type de Cas'); ?>          
        </h2> 
    </div>
    <div class="span3">
        <?php 
               if(!isAchiveMode($acad_sess))
                 { $template ='{update}{delete}';    
        ?>
        
        <div class="span4">
                  <?php
                        $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';                           
                        echo CHtml::link($images,array('caseType/create'));                 
                   ?>
         </div> 
        
         <?php if(infoGeneralConfig('grid_creation')!=null){ ?>
        
        <div class="span4">
                     <?php  
                           $images = '<i class="fa fa-table"> &nbsp; '.Yii::t('app','Mass adding').'</i>'; 
                           echo CHtml::link($images,array('caseType/massAddingCaseType'));
                     ?>
        </div>
        
                    <?php } ?>
        <?php } ?>        
        
        <div class="span4">
             <?php
                 $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';                          
                 echo CHtml::link($images,array('/Infirmerie/caseType/index'));
              ?>
         </div>
    </div>    
</div>
<div style="clear:both"></div>

<div class="grid-view">
    <div class="search-form">
        <?php
            $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
            $this->widget('bootstrap.widgets.TbGridView',array(
                'id'=>'base-case-type-grid',
                'summaryText'=>'',
                'showTableOnEmpty'=>'true',
                'dataProvider'=>$model->search(),
                
                'columns'=>array(
                      array(
                          'name' => 'name',
                          'type' => 'raw',
                          'value'=>'$data->name', 
                          'htmlOptions'=>array('width'=>'300px'),
                      ),
                'description',
                        array(
                            'class'=>'CButtonColumn',
                            'template'=>$template,
                            'buttons'=>array(
                                'view'=>array('label'=>'<span class="fa fa-eye"></span>',
                                'imageUrl'=>false,
                                'options'=>array('title'=>Yii::t('app','View')),
                                    
                                 ),
                                
                                'update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                                'imageUrl'=>false,
                                'options'=>array('title'=>Yii::t('app','Update')),
                             
                                ),
                                
                                'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                                'imageUrl'=>false,
                                'options'=>array('title'=>Yii::t('app','Delete')),
                                
                                ),
                            ),
                        ),
           ),
         ));
        ?>
    </div>
    
    <script>
        $(document).ready(function(){
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",
                    
                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                    {extend: 'excel', title: 'siges_custom_report'},
                ]
            });
        });
    </script>
</div>