<?php
/* @var $this CaseTypeController */
/* @var $model BaseCaseType 

$this->breadcrumbs=array(
	'Base Case Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List BaseCaseType', 'url'=>array('index')),
	array('label'=>'Manage BaseCaseType', 'url'=>array('admin')),
);*/

$this->pageTitle = Yii::app()->name. ' - '.Yii::t('app','Create case type');
?>

<div id="dash">
    <div class="span3">
       <h2><?php echo Yii::t('app','Ajouter Type de Cas'); ?></h2>
    </div>
    
    <div class="span3">
        <div class="span4">
            <?php
                $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';                               
                           echo CHtml::link($images,array('/Infirmerie/caseType/index')); 
                           $this->back_url='/Infirmerie/caseType/index';
            ?>
        </div>
    </div>
</div>

<div style="clear:both"></div>
<br/>

<div class="b_mail">
    <div class="form">
        <?php echo $this->renderPartial('_form', array('model'=>$model)); ?>
    </div>
</div>


