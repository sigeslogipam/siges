<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>

<div id="dash">
     <div class="span3">
            <h2>
                <?php echo Yii::t('app','Création Type de Cas par lot');?>              
            </h2> 
     </div>
    
    <div class="span3">
        <div class="span4">
             <?php
                $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                 echo CHtml::link($images,array('/Infirmerie/caseType/index')); 
                // $this->back_url='/configuration/reportcardObservation/index';
             ?>
        </div>
    </div>
</div>

<div style="clear:both"></div>
<br/>

<div class="b_mail">   
    <?php echo $this->renderPartial('_gridcreate', array('model'=>$model)); ?>
</div>