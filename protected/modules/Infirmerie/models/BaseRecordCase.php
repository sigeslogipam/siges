<?php

/**
 * This is the model class for table "record_case".
 *
 * The followings are the available columns in table 'record_case':
 * @property integer $id
 * @property integer $student
 * @property integer $case_type
 * @property string $record_by
 * @property string $incident_date
 * @property integer $academic_period
 * @property integer $exam_period
 * @property string $case_description
 * @property string $final_decision
 * @property string $date_created
 * @property string $date_updated
 * @property string $created_by
 * @property integer $updated_by
 *
 * The followings are the available model relations:
 * @property Academicperiods $academicPeriod
 * @property CaseType $caseType
 * @property LevelHasPerson $student0
 */

class BaseRecordCase extends CActiveRecord
{
       public $month = " ";
       public $total = " ";
        /**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return BaseRecordCase the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'record_case';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student, case_type, incident_date, case_description', 'required'),
			array('student, case_type, updated_by', 'numerical', 'integerOnly'=>true),
			array('record_by, final_decision, created_by', 'length', 'max'=>255),
			array('date_created, date_updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, student, case_type, record_by, incident_date, case_description, final_decision, date_created, date_updated, created_by, updated_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			//'academicPeriod' => array(self::BELONGS_TO, 'Academicperiods', 'academic_period'),
			'caseType' => array(self::BELONGS_TO, 'BaseCaseType', 'case_type'),
			'student0' => array(self::BELONGS_TO, 'Persons', 'student'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student' => 'Elève',
			'case_type' => 'Type de Cas',
			'record_by' => 'Infirmier(ère)',
			'incident_date' => 'Date de Visite',
			//'academic_period' => 'Academic Period',
			//'exam_period' => 'Exam Period',
			'case_description' => 'Description de Cas',
			'final_decision' => 'Décision',
			'date_created' => 'Date Created',
			'date_updated' => 'Date Updated',
			'created_by' => 'Created By',
			'updated_by' => 'Updated By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student',$this->student);
		$criteria->compare('case_type',$this->case_type);
		$criteria->compare('record_by',$this->record_by,true);
		$criteria->compare('incident_date',$this->incident_date,true);
		$criteria->compare('academic_period',$this->academic_period);
		$criteria->compare('exam_period',$this->exam_period);
		$criteria->compare('case_description',$this->case_description,true);
		$criteria->compare('final_decision',$this->final_decision,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('created_by',$this->created_by,true);
		$criteria->compare('updated_by',$this->updated_by);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
        
        public function searchCurrentExamPeriod()
        {
            $criteria1 = new CDbCriteria;           
            
            $criteria1->compare('exam_period',$this->exam_period);
            
            return new CActiveDataProvider($this, array(
			'criteria'=>$criteria1,
		));
            
        }
        
        public function searchStudent()
        {
            $criteria2 = new CDbCriteria;
            $criteria2->compare('student', $this->student);
            
            return new CActiveDataProvider($this, array(
			'criteria'=>$criteria2,
		));
        }
        
 }