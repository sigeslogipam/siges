<?php

class RecordCaseController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $back_url;
        public $month_ =0;
        public $model_d = '';
        public $exam_period=0;
        public $total= '';
        public $month = "";

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			//'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update','getmonth','getdatabymonth'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */      
        
	public function actionView($id)
	{   $student = $id;
           /* if(isset($_POST['BaseRecordCase']['exam_period'])){
                $this->exam_period = $_POST['BaseRecordCase']['exam_period'];
            }else{
                $exam_p = BaseRecordCase::model()->searchCurrentExamPeriod(date('Y-m-d'));
                if(($exam_p!=NULL)&&($exam_p!=''))
                    $this->exam_period = $exam_p->id;
            }
            * 
            */
            $model_d = new BaseRecordCase;
           $model=BaseRecordCase::model()->findByAttributes(array('student'=>$student));
           if(!isset($model->id)){
                $this->redirect (array('index'));
           }else{
               $this->render('view',array(
                    'model'=>$model,
                    //'model_d'=>$model_d,
                ));
           } 
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
                // Pour l'année Académique
                $acad_sess = acad_sess();
                $acad=Yii::app()->session['currentId_academic_year'];
            
		$model=new BaseRecordCase;
                
                // Pour la date de la période 
                $modelInfraction = new RecordPresence;
                $exam_period_ = $modelInfraction->searchCurrentExamPeriod(date('Y-m-d'));
                
                if($exam_period_!=null)
                   $exam_period_id = $exam_period_->id;
                else
                   $exam_period_id = null;


		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
                
                // Pour l'enregistrement
		if(isset($_POST['BaseRecordCase']))
		{
			$model->attributes=$_POST['BaseRecordCase'];
                        
                    if(isset($_POST['create'])){
                       $model->setAttribute('academic_period', $acad_sess);
                       $model->setAttribute('exam_period', $exam_period_id);
                       
                       $model->setAttribute('date_created', date('Y-m-d'));                       
                       $model->setAttribute('created_by', currentUser());
                       
                       
                       if($model->save())
                           $this->redirect (array('view', 'id'=>$model->id));
                    }
                    
                    if(isset($_POST['cancel']))
                    {
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
                    
                        
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}
        
        // Methode pour obtenir la liste des élèves actifs
         public function getStudentname(){
            $academic_year = Yii::app()->session['currentId_academic_year']; 
            $sql  = "SELECT p.id, p.last_name, p.first_name FROM persons p INNER JOIN level_has_person lhp ON (p.id = lhp.students)  WHERE lhp.academic_year = $academic_year";
            $studentArray = CHtml::listData(Persons::model()->findAllBySql($sql), 'id', 'fullName');
            return $studentArray;
          }
          
          //Methode pour obtenir la liste des Infirmiers(ère)
          public function getStaffname(){
              $academic_year = Yii::app()->session['currentId_academic_year'];
              $sql_2  = 'SELECT p.last_name, p.first_name, p.id FROM persons p INNER JOIN persons_has_titles pht ON(p.id = pht.persons_id) INNER JOIN titles t ON (t.id = pht.titles_id)  WHERE t.title_name = "Infirmier(e)"';
              $staffArray = CHtml::listData(Persons::model()->findAllBySql($sql_2), 'id', 'fullname');
              return $staffArray;
          }


          // Methode pour obtenir la liste de tous les types de cas
          public function getCaseType(){
              $academic_year = Yii::app()->session['currentId_academic_year'];
              $sql_1 = "SELECT name FROM case_type";
          }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
                $acad_sess = acad_sess();
                $acad=Yii::app()->session['currentId_academic_year']; 
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BaseRecordCase']))
		{                    
                 
			$model->attributes=$_POST['BaseRecordCase'];                        
                    if(isset($_POST['update']))
                    {
                        $date_rec = $model->incident_date;
                        $exam_period = $model->searchCurrentExamPeriod($date_rec);
                        
                        if($exam_period != null)
                            $exam_period_id = $exam_period->id;
                        else 
                            $exam_period_id = null;
                        
                        $model->setAttribute('academic_period', $acad_sess);
                        $model->setAttribute('exam_period', $exam_period_id);
                    
                        $model->setAttribute('date_updated',date('Y-m-d'));
                        $model->setAttribute('update_by',currentUser());
                        
                        if($model->save())
                            $this->redirect(array('view','id'=>$model->id));
                    }       

                     if(isset($_POST['cancel']))
                          {                             
                              $this->redirect(Yii::app()->request->urlReferrer);
                          }                  
                    
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            try{
		$model_a = $this->loadModel($id);
                $student = $model_a->student;
                $model_a->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('view?id='.$student));
            } catch (CDbException $e)
            {
               if($e->errorInfo[1] == 1451)
               {
                   header($_SERVER["SERVER_PROTOCOL"]." 500 Relation Restriction");
                    echo Yii::t('app',"\n\n There are dependant elements, you have to delete them first.\n\n");
               }else
               {
                   throw $e;
               } 
            }
	}
        
        public function actionGetmonth(){
            $tab = array('Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Août', 'Septembre', 'Octobre', 'Novembre', 'Décembre');
            $json_value = json_encode($tab,false); 
            echo $json_value; 
           
        }
        
        public function actionGetdatabymonth(){           
            
            $sql_data  = 'SELECT count(case_type) AS total, MONTH(incident_date) as month FROM record_case rc group by MONTH(incident_date) ORDER BY incident_date ASC';
            $data_rc = BaseRecordCase::model()->findAllBySql($sql_data);
            
            $tab_data= array(0=>0,1=>0,2=>0,3=>0,4=>0,5=>0,6=>0,7=>0,8=>0,9=>0,10=>0,11=>0);
            
            foreach($data_rc as $dr){
                $tab_data[$dr['month']-1] = $dr['total'];
            }
            
            $json_data = json_encode($tab_data, JSON_NUMERIC_CHECK);
            echo $json_data;
            //print_r($json_data);
        }

        /**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		/*$dataProvider=new CActiveDataProvider('BaseRecordCase');            
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
            //$modelC = new BaseCaseType;
            $this->month = "";
            $this->layout = "";
            $this->total = "";
            $acad_sess = acad_sess();
            $acad=Yii::app()->session['currentId_academic_year'];
            
           if(!isset($_GET['month_']))
            {
                $sql__ = 'SELECT DISTINCT incident_date FROM record_case ORDER BY id DESC';
                $command__ = Yii::app()->db->createCommand($sql__);
                $result__ = $command__->queryAll();
                
            if($result__ != null)
            {
                foreach($result__ as $r)
                { 
                    $current_month = getMonth($r['incident_date']);
                     break;
                }
            }else{
                $current_month = getMonth(date('Y-m-d'));
            }
            }
            else{
              $current_month = $_GET['month_'];
           }
            
            $model=new BaseRecordCase("search_($current_month, $acad_sess)");
            $model->unsetAttributes();
            if(isset($_GET['BaseRecordCase']))
			$model->attributes = $_GET['BaseRecordCase'];
            
            $this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 
	public function actionAdmin()
	{
		$model=new BaseRecordCase('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BaseRecordCase']))
			$model->attributes=$_GET['BaseRecordCase'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        */
        
         // Export to CSV 
         public function behaviors() {
            return array(
              'exportableGrid' => array(
              'class' => 'application.components.ExportableGridBehavior',
              'filename' => Yii::t('app','recordcase.csv'),
              'csvDelimiter' => ',',
              ));
        }       
	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BaseRecordCase the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BaseRecordCase::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
        
	/**
	 * Performs the AJAX validation.
	 * @param BaseRecordCase $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='base-record-case-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
