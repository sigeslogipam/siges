<?php

class CaseTypeController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
        public $back_url;

        /**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view'),
				'users'=>array('*'),
			),
			array('allow', // allow authenticated user to perform 'create' and 'update' actions
				'actions'=>array('create','update', 'massAddingCaseType'),
				'users'=>array('@'),
			),
			array('allow', // allow admin user to perform 'admin' and 'delete' actions
				'actions'=>array('admin','delete'),
				'users'=>array('admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);          
           
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new BaseCaseType;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BaseCaseType']))
		{
		     $model->attributes=$_POST['BaseCaseType'];http://localhost/siges_zotolan/index.php/Infirmerie/caseType/index
                        
                     if(isset($_POST['create']))
                     {
                         if($model->save())
				$this->redirect(array('index'));
                     }
                     if(isset($_POST['cancel']))
                     {
                         $this->redirect(Yii::app()->request->urlReferrer);
                     }
			
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['BaseCaseType']))
		{
		    $model->attributes=$_POST['BaseCaseType'];
                    
                    if(isset($_POST['update']))
                    {
                        if($model->save())
				$this->redirect(array('index'));
                    }
                    if(isset($_POST['cancel']))
                    {
                        $this->redirect(Yii::app()->request->urlReferrer);
                    }
			
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
            try
            {
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
            }catch(CDbException $e)
            {
                 if($e->errorInfo[1] == 1451) {
			        //header("HTTP/1.0 400 Relation Restriction");
			        header($_SERVER["SERVER_PROTOCOL"]." 500 Relation Restriction");
			        echo Yii::t('app',"\n\n There are dependant elements, you have to delete them first.\n\n");
			    } else {
			        throw $e;
			    }
            }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
                /*$dataProvider=new CActiveDataProvider('BaseCaseType');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
		));*/
            
            if(isset($_GET['pageSize']))
            {
                Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
                unset($_GET['pageSize']);
            }
            
            $model=new BaseCaseType('search');
            $model->unsetAttributes();
            if(isset($_GET['BaseCaseType']))
			$model->attributes=$_GET['BaseCaseType'];
            
            $this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new BaseCaseType('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['BaseCaseType']))
			$model->attributes=$_GET['BaseCaseType'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}
        
        public function actionMassAddingCaseType()
        {
            $model = new BaseCaseType();
            
            $number_line = infoGeneralConfig('nb_grid_line');
            $name = array(); 
            $description = array();
            $error_report = False;
            $j = 0;
            $caseType = array();
            
            if(isset($_POST['btnSave']))
            {
                for($i=0;$i<$number_line;$i++)
                {
                    if((isset($_POST['name'.$i]) && $_POST['name'.$i]!="") && isset($_POST['description'.$i]))
                    {
                        $name[$i] = $_POST['name'.$i];
                        $description[$i] = $_POST['description'.$i];
                        
                        $model->setAttribute('name', $name[$i]);
                        $model->setAttribute('description', $description[$i]);
                       
                       if($model->save()){
                           
                       }
                       else{
                           $error_report = True;
                           $caseType[$j] = $name[$i];
                           $j++;
                       }                      
                       
                    }
                    
                    $model->unsetAttributes(); 
                    $model = new BaseCaseType();
                }
                
                if($error_report)
                {
                    $liste_case = "";
                    for($i=0; $i<count($caseType); $i++){
                        $liste_case .= $caseType[$i].'<br/>';
                    }
                    
                    $message=Yii::t('app',"At least {name} error(s) occured when you saved case type !<br/> The following case type were about to be duplicate :<br/><b> {caseType}</b>",array('{name}'=>$j,'{caseType}'=>$liste_case));
                    Yii::app()->user->setFlash(Yii::t('app','Error'), $message);                    
                }else{
                    $this->redirect(array('index'));
                }
            }
            
            if(isset($_POST['btnCancel'])){
                $this->redirect(array('index'));
            }
            
            $this->render('gridcreate',array(
			'model'=>$model,
		));
            
        }

        /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return BaseCaseType the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=BaseCaseType::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param BaseCaseType $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='base-case-type-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
