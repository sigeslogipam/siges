<?php
 /*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 *
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

$baseUrl = Yii::app()->baseUrl;

$cs = Yii::app()->getClientScript();
$cs->registerCssFile($baseUrl."/css/AdminLTE.min.css");
$cs->registerCssFile($baseUrl."/css/new-dashboard.css");
$cs->registerCssFile("https://use.fontawesome.com/releases/v5.5.0/css/all.css");

$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
  
 $acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
$siges_structure = infoGeneralConfig('siges_structure_session');

$display_announcement_in_menu = infoGeneralConfig('display_announcement_in_menu');

$current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));

$presence_method = infoGeneralConfig('presence_method');

$is_special_attendance = infoGeneralConfig('special_attendance');

 if($current_acad==''){
        $condition = '';
          $condition1 ='';
 }
    else
    {  if($acad!=$current_acad->id)
       {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
      else
        {  $condition = 'p.active IN(1,2) AND ';
          $condition1 ='active IN(1,2) AND ';
        }
    }

$tit='';
   $template = '';


?>
<div class="discv">
<div id="dash">
   <div class="span1">
      
  </div>

     


       <?php

           if(!isAchiveMode($acad_sess))
              {    
                if($acad == $current_acad_id)
		  {
                   $template = '{update}{delete}';

         ?>
  

         <div class="span4">
                  <?php

                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add Infraction').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('recordInfraction/create'));

                   ?>
         </div>
         <?php
         // Pour le NCB  avec sa formule de discipline 
          if(infoGeneralConfig('discipline_ncb')!==NULL || infoGeneralConfig('discipline_ncb')==1){
         ?>

         <div class="span4">
                  <?php

                 $images = '<i class="fa fa-calculator">&nbsp;'.Yii::t('app','Caculer note discipline').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('recordInfraction/buildDiscGrade'));

                   ?>
         </div>

          <?php } ?>

 <?php
        

      ?>

               
          <div class=" span4">
            <?php
                    
                        // build the link in Yii standard
                        // Switch the system of reccord attendance 
                        switch($presence_method){
                            case 0 : 
                            {
                                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Record attendance ').'</i>';
                                echo CHtml::link($images,array('recordPresence/recordPresence'));
                            }
                            break; 
                            case 1 : 
                            {
                                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Record attendance').'</i>';
                                echo CHtml::link($images,array('recordPresence/scanpresence'));
                            }
                            break;
                            case 2: 
                            {
                                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Record lateness').'</i>';
                                echo CHtml::link($images,array('recordPresence/recordRetard'));
                            }
                            break;
                        }
                     

             ?>

          </div>
          <?php if($is_special_attendance == 1) { ?>
        <div class=" span4">
                     <?php 
                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Record special attendance').'</i>';
                     echo CHtml::link($images,array('recordPresence/specialattendance'));
                     ?>   
        </div>
    
          <?php } ?>
    
    
    
    <?php 
              }
           else { $template = '';  }
              }
    ?>
          

   </div>

</div>




<?php

$acad_name=Yii::app()->session['currentName_academic_year'];

                             $month = getMonth(date('Y-m-d'));
                             $month_name = getLongMonth($month);

                             $year = getYear(date('Y-m-d'));



 //$string_sql_present = 'SELECT count(presence_type) as total, presence_type FROM record_presence rp WHERE MONTH(date_record)= \''.$month.'\' and YEAR(date_record)=\''.$year.'\' group by presence_type';
  $string_sql_present = "SELECT DISTINCT student, presence_type FROM `record_presence` WHERE MONTH(date_record) = MONTH(NOW()) AND YEAR(date_record) = YEAR(NOW())";
                             
  $data2 = RecordPresence::model()->findAllBySql($string_sql_present);

   $tab_presen='';
   $tot_tab_presen='';
   $tot_abs=0;
   $tot_reta=0;
   $s1=0;
 if($data2 !=null)
 {
 foreach($data2 as $da){

     switch($da->presence_type)
        {
            case 0:
               // $tab_presen =Yii::t('app','P').','.$da->total.',false';
               // break;
            case 1:
                {
                 $tot_abs++;
                 $tab_presen =Yii::t('app','ANE').','.$da->total.',false';
                 }
                 break;
            case 2:
                {
                    $tot_abs++;
                    $tab_presen =Yii::t('app','AWE').','.$da->total.',false';
                }
                break;
            case 3:
            {
                $tot_reta++;
                $tab_presen =Yii::t('app','TNE').','.$da->total.',false';
            }
                break;
            case 4:
            {
                $tot_reta++;
                $tab_presen =Yii::t('app','TWE').','.$da->total.',false';
            }
                break;

            }

            if($s1==0)
       {  if($tab_presen!='')
          {
           $tot_tab_presen = $tab_presen;

           $s1=1;
          }
        }
     else
        {
          $tot_tab_presen = $tot_tab_presen.'/'.$tab_presen;

        }

 }
 }

//infraction

 $tot_infr = 0;
 $string_sql_infract_c = 'SELECT count(infraction_type) AS total, infraction_type FROM record_infraction ri INNER JOIN infraction_type it on(it.id = ri.infraction_type) WHERE MONTH(incident_date)= \''.$month.'\' and YEAR(incident_date)= \''.$year.'\' ';
 $data1c = RecordInfraction::model()->findAllBySql($string_sql_infract_c);
 if($data1c !=null)
 {
 foreach($data1c as $ap){
    $tot_infr = $ap->total;

 }
 }


 $string_sql_infract = 'SELECT count(infraction_type) AS total, infraction_type, it.name as infraction_name FROM record_infraction ri INNER JOIN infraction_type it on(it.id = ri.infraction_type) WHERE MONTH(incident_date)= \''.$month.'\' and YEAR(incident_date)= \''.$year.'\' group by infraction_type limit 10 ';
     $data1 = RecordInfraction::model()->findAllBySql($string_sql_infract);
  $category_infract ='';
  $tot_category_infract ='';
  $tot_infr =0;
     $s = 0;

 if($data1 !=null)
 {
 foreach($data1 as $ap){

     if($s==0)
       {   $category_infract = $ap->infraction_name;
         $tot_category_infract = $ap->total;
           $s=1;
           $tot_infr = $ap->total;
        }
     else
        {
          $category_infract = $category_infract.','.$ap->infraction_name;
          $tot_category_infract = $tot_category_infract.','.$ap->total;

          $tot_infr = $tot_infr + $ap->total;
        }



 }
 }

 if( ($data2 ==null)&&($data1 ==null) )
 {
     $s1=0;

     $month = $month -1;
     $month_name = getLongMonth($month);

    // $string_sql_present = 'SELECT count(presence_type) as total, presence_type FROM record_presence rp WHERE MONTH(date_record)= \''.$month.'\' and YEAR(date_record)=\''.$year.'\' group by presence_type';
    $string_sql_present = "SELECT DISTINCT student, presence_type FROM `record_presence` WHERE MONTH(date_record) = MONTH(NOW()) AND YEAR(date_record) = YEAR(NOW())";
     $data2_ = RecordPresence::model()->findAllBySql($string_sql_present);
     if($data2_ !=null)
 {
 foreach($data2_ as $da){

     switch($da->presence_type)
        {
            case 0:
               // $tab_presen =Yii::t('app','P').','.$da->total.',false';
                break;
            case 1:
                 $tot_abs++;
                 break;
            case 2:
                $tot_abs++;
                $tab_presen =Yii::t('app','AWE').','.$da->total.',false';
                break;
            case 3:
                $tot_reta++;
                $tab_presen =Yii::t('app','TNE').','.$da->total.',false';
                break;
            case 4:
                $tot_reta++;
                $tab_presen =Yii::t('app','TWE').','.$da->total.',false';
                break;

            }

            if($s1==0)
       {  if($tab_presen!='')
          {
           $tot_tab_presen = $tab_presen;

           $s1=1;
          }
        }
     else
        {
          $tot_tab_presen = $tot_tab_presen.'/'.$tab_presen;

        }

 }
 }


 $tot_infr = 0;
 $string_sql_infract_c = 'SELECT count(infraction_type) AS total, infraction_type FROM record_infraction ri INNER JOIN infraction_type it on(it.id = ri.infraction_type) WHERE MONTH(incident_date)= \''.$month.'\' and YEAR(incident_date)= \''.$year.'\' ';
 $data1c = RecordInfraction::model()->findAllBySql($string_sql_infract_c);
 if($data1c !=null)
 {
 foreach($data1c as $ap){
    $tot_infr = $ap->total;

 }
 }

 $string_sql_infract = 'SELECT count(infraction_type) AS total, infraction_type, it.name as infraction_name FROM record_infraction ri INNER JOIN infraction_type it on(it.id = ri.infraction_type) WHERE MONTH(incident_date)= \''.$month.'\' and YEAR(incident_date)= \''.$year.'\' group by infraction_type limit 10 ';
     $data1 = RecordInfraction::model()->findAllBySql($string_sql_infract);
  $category_infract ='';
  $tot_category_infract ='';
     $s = 0;


 if($data1 !=null)
 {
 foreach($data1 as $ap){

     if($s==0)
       {   $category_infract = $ap->infraction_name;
         $tot_category_infract = $ap->total;
           $s=1;

        }
     else
        {
          $category_infract = $category_infract.','.$ap->infraction_name;
          $tot_category_infract = $tot_category_infract.','.$ap->total;

        }

 }
 }




 }



/// enfraksyon pa mwa
$string_sql_infract = 'SELECT count(infraction_type) AS total, MONTH(incident_date) as month FROM record_infraction ri WHERE `academic_period`='.$acad_sess.' group by MONTH(incident_date) ORDER BY incident_date ASC';
     $data14 = RecordInfraction::model()->findAllBySql($string_sql_infract);
  $month_infract ='';
  $tot_infract ='';
     $s4 = 0;


 if($data14 !=null)
 {
 foreach($data14 as $ap){

     if($s4==0)
       {   $month_infract = getShortMonth($ap->month);
         $tot_infract = $ap->total;
           $s4=1;

        }
     else
        {
          $month_infract = $month_infract.','.getShortMonth($ap->month);
          $tot_infract = $tot_infract.','.$ap->total;

        }

 }
 }



  ?>





<div class="container-fluid" style="text-align:center;" >


<div class="row-fluid">

<div  class="span8" >


				<!-- ================================================== -->

	<?php





							            $criteria = new CDbCriteria;
							            $criteria->select = 'count(*) as total_stud';


							              $criteria->condition= $condition1.' is_student=:item_name ';

							            $criteria->params=array(':item_name'=>1,);
							            $total_stud = Persons::model()->find($criteria)->count();





							        $tot_stud_s=0;
							        $female_s = 0;
							        $male_s = 0;

							        $tot_stud=0;
							        $female = 0;
							        $male = 0;
							        //teachers
							        $tot_teach_s=0;
							        $female1_s = 0;
							        $male1_s = 0;

							        $tot_teach=0;
							        $female1 = 0;
							        $male1 = 0;
							        //employees
							        $tot_emp_s=0;
							        $female2_s = 0;
							        $male2_s = 0;

							        $tot_emp=0;
							        $female2 = 0;
							        $male2 = 0;

							         $level = array();




							        // Student data
							       $dataProvider= Persons::model()->searchStudentsReport($condition,$acad_sess);

							        if($dataProvider->getItemCount()!=0)
							            $tot_stud =$dataProvider->getItemCount();


							        //reccuperer la qt des diff. sexes
							        $person=$dataProvider->getData();

							        foreach($person as $stud)
							         {
								        if($stud->gender==1)
								            $female++;
								        elseif($stud->gender==0)
								            $male++;
							         }
							        // Fin student data

							        // Teachers data

							        $dataProvider1= Persons::model()->searchTeacherReport($condition,$acad_sess);

							        if($dataProvider1->getItemCount()!=0)
							        $tot_teach =$dataProvider1->getItemCount();


							        //reccuperer la qt des diff. sexes
							        $person=$dataProvider1->getData();

							        foreach($person as $teacher)
							         {
								        if($teacher->gender==1)
								             $female1++;
								        elseif($teacher->gender==0)
								              $male1++;
							         }
							        // Fin teachers

							        // debut Employes
							        $dataProvider2= Persons::model()->searchEmployeeReport($condition,$acad_sess);

							        if($dataProvider2->getItemCount()!=0)
							           $tot_emp =$dataProvider2->getItemCount();


							        //reccuperer la qt des diff. sexes
							        $person=$dataProvider2->getData();

							        foreach($person as $employee)
							         {
								        if($employee->gender==1)
								             $female2++;
								        elseif($employee->gender==0)
								            $male2++;
							          }

							// fin employes


							// Total pesonnes
							$tot_pers = $tot_stud + $tot_emp + $tot_teach;

							//reccuperer la qt des diff. sexes
							$tot_fem = $female  + $female1 + $female2;
							$tot_mal = $male  + $male1 + $male2;
							// fin total personne

							// Stat sur Rooms
							$tot_rooms=0;
							$countRooms = Rooms::model()->searchReport();
							if($countRooms->getItemCount()!=0)
							    $tot_rooms = $countRooms->getItemCount();

							// Stat sur Subjects
							$tot_sub=0;
							$countSubjects = Subjects::model()->searchReport();
							if($countSubjects->getItemCount()!=0)
							    $tot_sub = $countSubjects->getItemCount();

							// Stat sur Courses
							 $countCourses = null;
							$tot_course=0;
							if($acad!='')
							  $countCourses = Courses::model()->searchReport($condition1,$acad_sess);



							if($countCourses->getItemCount()!=0)
							    $tot_course = $countCourses->getItemCount();

							 ?>



			 <!--===========================================-->









                      <!--  Test du nouveau design du dashboard
                      Le test etant concluant on implement le dashboard ak sa yo
                      -->
                     <!-- infraction -->
		     <div class="span4 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-green" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_infr;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','infractions') ?></span></h3>

                                    <p class="nom-piti teks-blan">

                                    </p>
                                    <p class="nom-piti teks-blan">

                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fas fa-building"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>

                     <!-- tardy -->
                      <div class="span4 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-blue" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_reta;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Tardy').'(s)' ?></span></h3>

                                    <p class="nom-piti teks-blan">

                                    </p>
                                    <p class="nom-piti teks-blan">

                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fa fa-group"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>
                      <!-- Fin du test -->
                      <!-- absence -->
                      <div class="span4 ti-bwat">
                                 <!-- small box -->
                              <div class="small-box bg-fuchsia" data-html="true" rel="tooltip">
                                <div class="inner">
                                    <h3><span class="teks-blan"><?= $tot_abs;?></span> <span class="piti-ekriti teks-blan"> <?= Yii::t('app','Absence').'(s)' ?></span></h3>

                                    <p class="nom-piti teks-blan">

                                    </p>
                                    <p class="nom-piti teks-blan">

                                    </p>
                                    </div>

                               <div class="icon">
                                        <i class="fas fa-chalkboard-teacher"  style="padding-top: 20px"></i>
                               </div>



                              </div>
                       </div>











	<br/>





                     <div id="container1"  style=" width: 100%; height: 500px;  "></div>

               	   <!--  <div id="container" class='' style="float: left; width: 50%; height: 400px;  "></div>    -->


  </div>


  <!-- kolonn dwat  -->

         <div  class="kolonn_d span4" >
           <div class="row-fluid">

           <h4> 10 <?= Yii::t('app','Last offenses') ?></h4>



             <?php     $modelP = new Persons;

                     $data_provider_ = $model->search($acad_sess);
                      if($data_provider_!=null)
                      {
                          $data_provider = $data_provider_->getData();
                      $kanpela = 0;
                       foreach ($data_provider as $data)
                         {
                           $kanpela++;

                            echo '<div class="kolonn_lis"> <span style="font-weight:bold;">'.$data->student0->first_name.' '.$data->student0->last_name.'</span> ('.$modelP->getShortRooms($data->student0->id,$acad_sess).')'
                                    . '<br/> '
                                    . '<span class="infraction_name">'.$data->infractionType->name.'</span>'
                                    . '<br/>'
                                    . '<span class="infraction_name">'.Yii::t('app','Value Deduction').' : '.$data->value_deduction.'</span>'
                                    . '<br/>'
                                    . '<span class="infraction_name">'.$data->incidentDate.'</span>';
            if($acad == $current_acad_id)
		  {
                               echo    ' <span style="float:right; margin-right:30px;">'.CHtml::link('<span class="fa fa-pencil-square-o"></span>',Yii::app()->createUrl("discipline/recordInfraction/update",array("id"=>$data->id))).'</span> ';
                  }
                                   echo ' </div>';

                            if($kanpela==10)
                                break;
                         }

                      }

             ?>

           </div>
         </div>
            </div>
         <br/>
           <div style="clear:both"></div>


           <div class="grid-view">

           <br/>
           <div  class="search-form">

           <?php


                   $gridWidget = $this->widget('bootstrap.widgets.TbGridView',array(
           	'id'=>'record-infraction-grid',
           	'summaryText'=>'',
           	'dataProvider'=>$model->search($acad_sess),

           	'columns'=>array(
           		array(
                               //'name' => 'student',
                               'header'=> Yii::t('app','First Name'),
                               'type' => 'raw',
                               'value'=>'CHtml::link($data->student0->first_name,Yii::app()->createUrl("discipline/recordInfraction/view",array("id"=>$data->id)))',
                                ),


                           array(
                                // 'name' => 'student',
                               'header'=> Yii::t('app','Last Name'),
                               'type' => 'raw',
                               'value'=>'CHtml::link($data->student0->last_name,Yii::app()->createUrl("discipline/recordInfraction/view",array("id"=>$data->id)))',
                                ),





                           array(
                               'name' => 'infraction_type',
                               'type' => 'raw',
                               'value'=>'$data->infractionType->name',
                               'htmlOptions'=>array('width'=>'200px'),
                                ),
           		//'record_by',

                           'value_deduction',

                           array('name'=>'incident_date','value'=>'$data->incidentDate'),

           		     	array(
           			'class'=>'CButtonColumn',
                                   'template'=>$template,
                                   'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                                       'imageUrl'=>false,
                                       'options'=>array('title'=>Yii::t('app','Update')),

                                       ),
                                       'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                                       'imageUrl'=>false,
                                       'options'=>array('title'=>Yii::t('app','Delete')),

                                       ),

                                       ),


           		),
           	),
           ));






           ?>



           </div>




           <?php








            $string_sql_cycle = "SELECT COUNT( DISTINCT gpStudent.students) AS Total,Cycle, Section
                                                   FROM room_has_person rhp
                                                   INNER JOIN level_has_person lhp ON(lhp.students=rhp.students AND rhp.academic_year=lhp.academic_year )
                                                   INNER JOIN ( SELECT rhp1.students,c.cycle_description as Cycle, s.section_name AS Section
                                                                 FROM room_has_person rhp1 INNER JOIN level_has_person lhp1 ON(lhp1.students=rhp1.students and rhp1.academic_year=lhp1.academic_year) INNER JOIN section_has_cycle shc ON(shc.level=lhp1.level AND shc.academic_year=lhp1.academic_year) INNER JOIN sections s ON(s.id=shc.section) INNER JOIN cycles c ON(shc.cycle=c.id)
                                                                  WHERE rhp1.academic_year=$acad) gpStudent
                                                             ON rhp.students=gpStudent.students
                                                 GROUP BY cycle";
             $data3 = RoomHasPerson::model()->findAllBySql($string_sql_cycle);

             $tab_cycle='';
              $tot_tab_cycle='';
              $s3=0;

           if($data3 !=null)
            {
            foreach($data3 as $da){

               $tab_cycle= $da->Cycle.','.$da->Total.','.$da->Section;


                if($s3==0)
                  {  if($tab_cycle!='')
                     {
                      $tot_tab_cycle = $tab_cycle;

                      $s3=1;
                     }
                   }
                else
                   {
                     $tot_tab_cycle = $tot_tab_cycle.'/'.$tab_cycle;

                   }
                 }

            }




            echo '<input type="hidden" id="data_month_infract" value="'.$month_infract.'" />';
            echo '<input type="hidden" id="tot_infract" value="'.$tot_infract.'" />';

            echo '<input type="hidden" id="data_category_infract" value="'.$category_infract.'" />';
            echo '<input type="hidden" id="tot_category_infract" value="'.$tot_category_infract.'" />';

            echo '<input type="hidden" id="tot_tab_presen" value="'.$tot_tab_presen.'" />';

            echo '<input type="hidden" id="cycle" value="'.$tot_tab_cycle.'" />';

            echo '<input type="hidden" id="titleAbs" value="'.Yii::t('app','Attendance/Punctuality').'" />';
            echo '<input type="hidden" id="yaxis" value="'.Yii::t('app','Number of student').'" />';
            echo '<input type="hidden" id="title_inf" value="'.Yii::t('app','Infractions by month').'" />';
            echo '<input type="hidden" id="subtitle_inf" value=" " />';
            echo '<input type="hidden" id="title_cycle" value="'.Yii::t('app','Number of student by Section and Cycle').'" />';




           ?>




           <script>



           $(document).ready( function() {

                $('.items').DataTable({
                           pageLength: 25,
                           responsive: true,
                           dom: '<"html5buttons"B>lTfgitp',
                            language: {
                               processing:     "<?= Yii::t('app','Processing ...')?>",
                               search: '<i class="fa fa-search"></i>',
                               searchPlaceholder: "<?= Yii::t('app','Search')?>",
                               lengthMenu:    " _MENU_ ",
                               info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                               infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                               infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                               infoPostFix:    "",
                               loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                               zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                               emptyTable:     "<?= Yii::t('app','No data to show')?>",

                               paginate: {
                                   first:       "<?= Yii::t('app','First'); ?>",
                                   previous:    "<?= Yii::t('app','Previous'); ?>",
                                   next:        "<?= Yii::t('app','Next'); ?>",
                                   last:        "<?= Yii::t('app','Last'); ?>"
                               },
                               aria: {
                                  sortAscending:  ": activer pour trier la colonne par ordre croissant",
                                  sortDescending: ": activer pour trier la colonne par ordre décroissant"
                               },
                           },
                           buttons: [
                             //  { extend: 'copy'},
                             //  {extend: 'csv'},
                               {extend: 'excel', title: "<?= $tit;?>" },

                               /*{extend: 'print',
                                customize: function (win){
                                       $(win.document.body).addClass('white-bg');
                                       $(win.document.body).css('font-size', '10px');

                                       $(win.document.body).find('table')
                                               .addClass('compact')
                                               .css('font-size', 'inherit');
                               }
                               }
                               */

                           ]

                       });





           var titleAbs = document.getElementById("titleAbs").value;
           var data_tab_presen=document.getElementById("tot_tab_presen").value;
           	var tab_present = data_tab_presen.split("/");
            var j;
            var k;
            var tab_present_split;
             var final_tab=[];


              //table value for
              for(j=0;j<tab_present.length; j++)
               {
                   tab_present_split = tab_present[j].split(",");

                var a=1;
                var temp=[];

                 for(k=0;k<tab_present_split.length; k++)
                  {
                   if(a!=2)
                   temp.push(tab_present_split[k]);
                   else
                       temp.push(parseFloat(tab_present_split[k]));

                   a++;
                   if(a==4)
                     a=1;

                   }

                  final_tab.push(temp);



               	}

              $('#container').highcharts({

                   chart: {
                   styledMode: true
               },

               title: {
                   text: titleAbs
               },



           plotOptions: {
                   pie: {
                       dataLabels: {
                           enabled: true,
                           format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
                           distance: -50,
                           filter: {
                               property: 'percentage',
                               operator: '>',
                               value: 4
                           },

                       }
                   }
               },
               series: [{
                   type: 'pie',
                   allowPointSelect: true,
                   keys: ['name', 'y', 'selected', 'sliced'],
                   name: 'Total',
                   data: final_tab,
                   showInLegend: false,

               }]
            });


           var title_inf = document.getElementById("title_inf").value;
            var subtitle_inf = document.getElementById("subtitle_inf").value;
           var data_month_infract=document.getElementById("data_month_infract").value;
           	var tab_month_infract = data_month_infract.split(",");
            var data_tot_infract=document.getElementById("tot_infract").value;
           	var tab_tot_infract = data_tot_infract.split(",");
             var tab_tot=[];
              var i;

              //table value for male
              for(i=0;i<tab_tot_infract.length; i++)
               {
               	tab_tot.push(parseFloat(tab_tot_infract[i]));
               	}

               $('#container1').highcharts({


               title: {
                   text: title_inf
               },

               subtitle: {
                   text: subtitle_inf
               },
                  xAxis: {
                   categories: tab_month_infract
               },
                  yAxis: {
                   title: {
                       text: ' '
                   }
               },

               series: [{
                   type: 'column',
                   colorByPoint: true,
                   name: 'Total',
                   data: tab_tot,
                   dataLabels: {
                       enabled: true,
                       rotation: -90,
                       color: '#FFFFFF',
                       align: 'right',
                       format: '{point.y:.1f}', // one decimal
                       y: 5, // 10 pixels down from the top
                       style: {
                           fontSize: '9px',
                           fontFamily: 'Verdana, sans-serif'
                       }
                   },
                   showInLegend: false
               }]



               });





           });






           </script>

         </div>




 </div>


</div>






<!-- ================================================== -->
