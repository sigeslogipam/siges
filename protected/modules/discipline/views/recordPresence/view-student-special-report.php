<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo "$nom_etudiant<br/>$date_presence<br/>$is_late";
/*
$room_stud_data = RoomHasPerson::model()->findAllByAttributes(array('academic_year'=>$acad,'students'=>$student));
$room_id = 0;
foreach($room_stud_data as $rsd){
    $room_id = $rsd->room;
}
$room_name = Rooms::model()->findByPk($room_id)->short_room_name;

if(Persons::model()->findByPk($student)->image==NULL){
                $string_image_location = "<i class='fas fa-user-graduate fa-3x'></i>";

            }else{
                $string_image_location = "<img class='direct-chat-img' src='".Yii::app()->baseUrl."/documents/photo-Uploads/1/".Persons::model()->findByPk($student)->image."'>";
            }
 * 
 */
$date_format = date('Y-m-d',strtotime($date));

$sql_str = "SELECT p.id, p.last_name, p.first_name, p.gender, r.short_room_name, p.id_number, rp.date_record FROM record_special_presence rp "
        . "INNER JOIN persons p ON (rp.student = p.id) "
        . "INNER JOIN room_has_person rhp ON (rhp.students = rp.student)"
        . "INNER JOIN rooms r ON (r.id = rhp.room)"
        . "WHERE DATE(rp.date_record) = DATE('$date_format') AND rhp.academic_year = $acad";
$all_stud_late = Persons::model()->findAllBySql($sql_str);

$tit = Yii::t('app','List of student present for this event on {date_event}',array('{date_event}'=>$date_format));

?>

        <div class="span12">
            <table class="table responsive bordered, stripped">
                <h5><?= Yii::t('app','List of student present for this event on {date_event}',array('{date_event}'=>$date_format));?></h5>
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= Yii::t('app','Code') ?></th>
                        <th><?= Yii::t('app','First name') ?></th>
                        <th><?= Yii::t('app','Last name') ?></th>
                        <th><?= Yii::t('app','Gender') ?></th>
                        <th><?= Yii::t('app','Room') ?></th>
                        <th><?= Yii::t('app','Date') ?></th>
                        <th><?= Yii::t('app','Time') ?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $k = 1;
                    foreach($all_stud_late as $as){
                        ?>
                    <tr>
                        <td><?= $k; ?></td>
                        <td><?= $as->id_number; ?></td>
                        <td><?= $as->first_name; ?></td>
                        <td><?= $as->last_name; ?></td>
                        <td><?= $as->sexe; ?></td>
                        <td><?= $as->short_room_name; ?></td>
                        <td><?= date("d-m-Y",strtotime($as->date_record)); ?></td>
                        <td><?= date("H:i:s",strtotime($as->date_record)); ?></td>
                    </tr>
                    <?php
                    $k++;
                    }
                    ?>
                </tbody>
            </table>
        </div>

 <script>
    $(document).ready(function(){
      
            $('.table').DataTable({
                pageLength: 100,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  
                    {extend: 'excel', title: "<?= $tit ?>"},
                  
                 
                ]

            });
            

        });
</script>       
       
