<?php
 $acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
$template = '';
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

?>
<div id="dash">
   <div class="span3"><h2>
          <?php echo Yii::t('app','View student special event repport'); ?>

   </h2> </div>

     <div class="span3">

 <?php

     if(!isAchiveMode($acad_sess))
        {    $template = '{update}';

   ?>

         <div class="span4">
                  <?php

                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Record special attendance').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('recordPresence/specialattendance'));

                   ?>
  </div>
  <?php
        }

      ?>

               <div class="span4">
              <?php

                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/discipline/recordPresence/admin'));

               ?>
          </div>
   </div>

</div>

<div style="clear:both"></div>

<div class="row-fluid">
    <div class="input-group input-group-sm zon-sezi">
        <input class="form-control" id="date-event" type="text" placeholder="<?= Yii::t('app','Choose date')?>">
            <span class="input-group-btn">
                <button type="button" id="btnSave" class="btn btn-info btn-flat"><i class="fa fa-searc"></i> <?= Yii::t('app','View report');?></button>
            </span>
    </div>
</div>

<div style="clear:both"></div>

<div class="row-fluid" id="rezilta">

</div>



<script>
    $(document).ready(function(){
        $('#date-event').datepicker({
            autoclose: true,
            dateFormat: 'dd-mm-yy'
            
        });
    });
    
    $(document).keypress(function(e) {
        if(e.which === 13) {
           var valeur = $("#date-event").val();
           $('#date-event').val('');
           $.get('<?= $baseUrl?>/index.php/discipline/recordPresence/makespecialreport',{date:valeur},function(data){
                            $('#rezilta').html(data);

           });
        }
    });

    $('#btnSave').click(function(){
        var valeur = $("#date-event").val();
           $('#date-event').val('');
           $.get('<?= $baseUrl?>/index.php/discipline/recordPresence/makespecialreport',{date:valeur},function(data){
                            $('#rezilta').html(data);

           });
    });
</script>
