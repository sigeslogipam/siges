<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$acad=Yii::app()->session['currentId_academic_year']; 

$sql_str = "SELECT p.id, p.first_name, p.last_name  FROM persons p "
        . "INNER JOIN room_has_person rhp ON (rhp.students = p.id) "
        . "WHERE p.id NOT IN (SELECT student FROM record_presence WHERE DATE(date_record)  = DATE(NOW()) "
        . "AND room = $room AND academic_period = $acad) AND rhp.academic_year = $acad AND rhp.room = $room AND p.active IN (1,2)";

$student_absent = Persons::model()->findAllBySql($sql_str);
$str_id_stud = null;
foreach($student_absent as $sa){
    $str_id_stud .= $sa->id.',';
}
function evenOdd($num)
            {
                ($num % 2==0) ? $class = 'odd' : $class = 'even';
                return $class;
            }
            
        
?>

<div class="grid-view">
    
    <?php 
       // echo $str_id_stud; 
          $form=$this->beginWidget('CActiveForm', array(
            'id'=>'record-presence-form',
            'enableAjaxValidation'=>true,
        ));   
    ?>
    <input id="str-id-stud" value="<?= $str_id_stud; ?>" type="hidden">
    <table class="items">
        <thead>
            <tr>
                <th><?php  echo Yii::t('app','#'); ?></th>
                <th><?php  echo Yii::t('app','Last name'); ?></th>
                <th><?php  echo Yii::t('app','First name'); ?></th>
                <th><?php  echo Yii::t('app','Presence type'); ?></th> 
               </tr>    
        </thead>
        <tbody>
            <?php
             $line_number = 1;
             foreach($student_absent as $sa){
                 ?>
            <tr class="<?php echo evenOdd($line_number); ?>">
                <td><?php echo $line_number; ?></td>
                <td><?php echo $sa->last_name; ?></td>
                <td><?php echo $sa->first_name; ?></td>
                <td>
                <?php
               echo $form->dropDownList($model, "presence_type[$sa->id]",$model->getAbsenceStatus());
                ?>
                </td>
                
            </tr>
            <?php 
            $line_number++;
             }
            ?>
        </tbody>
    </table>
        <?php $this->endWidget(); ?>
</div>            

<script>
  $(document).ready(function(){
    
  });
</script>