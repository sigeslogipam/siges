<?php
 $acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
$template = '';
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();

?>
<div id="dash">
   <div class="span3"><h2>
          <?php echo Yii::t('app','Manage attendance record'); ?>

   </h2> </div>

     <div class="span3">

 <?php

     if(!isAchiveMode($acad_sess))
        {    $template = '{update}';

   ?>

         <div class="span4">
                  <?php

                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Take absence').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('recordPresence/recordAbsence'));

                   ?>
  </div>
  <?php
        }

      ?>

               <div class="span4">
              <?php

                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/discipline/recordPresence/admin'));

               ?>
          </div>
   </div>

</div>

<div style="clear:both"></div>

<div class="row-fluid">
    <div class="input-group input-group-sm zon-sezi">
        <input class="form-control" id="id-student" type="text" placeholder="<?= Yii::t('app','Type or Scan student ID here to take attendance.')?>">
            <span class="input-group-btn">
                <button type="button" id="btnSave" class="btn btn-info btn-flat"><i class="fa fa-save"></i> Enregistrer</button>
            </span>
    </div>
</div>

<div style="clear:both"></div>

<div class="row-fluid" id="rezilta">

</div>



<script>
    $(document).keypress(function(e) {
        if(e.which === 13) {
           var valeur = $("#id-student").val();
           $('#id-student').val('');
           $.get('<?= $baseUrl?>/index.php/discipline/recordPresence/makepresence',{student:valeur},function(data){
                            $('#rezilta').html(data);

           });
        }
    });

    $('#btnSave').click(function(){
        var valeur = $("#id-student").val();
           $('#id-student').val('');
           $.get('<?= $baseUrl?>/index.php/discipline/recordPresence/makepresence',{student:valeur},function(data){
                            $('#rezilta').html(data);

           });
    });
</script>
