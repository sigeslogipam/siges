<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
$baseUrl = Yii::app()->baseUrl;

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));
function evenOdd($num)
            {
                ($num % 2==0) ? $class = 'odd' : $class = 'even';
                return $class;
            }
            
$room_data = Rooms::model()->findAll(); 
$presence_method = infoGeneralConfig('presence_method');

?>

<div id="dash">
		<div class="span3"><h2>
		      
       <?php  	
	      echo Yii::t('app','Record Attendance '); 
		?>
              
        </h2> </div>
         
    <div class="span3">
             <div class="span4">

                      <?php
                        
                     $images = '<i class="fa fa-file-o"> &nbsp;'.Yii::t('app','Attendance Journal').'</i>';

                               // build the link in Yii standard

                                echo CHtml::link($images,array("/discipline/recordPresence/admin")); 
                                $this->back_url='/discipline/recordPresence/index';   
                     
                   ?>

                  </div> 
    
         </div>
    <div class="span3">
    <div class=" span4">
            <?php
                    $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Record attendance ').'</i>';
                        // build the link in Yii standard
                        // Switch the system of reccord attendance 
                        switch($presence_method){
                            case 0 : 
                            {
                                echo CHtml::link($images,array('recordPresence/recordPresence'));
                            }
                            break; 
                            case 1 : 
                            {
                                echo CHtml::link($images,array('recordPresence/scanpresence'));
                            }
                            break;
                            case 2: 
                            {
                                echo CHtml::link($images,array('recordPresence/recordRetard'));
                            }
                            break;
                        }
                     

             ?>

          </div>
        </div>
    
              
      <div class="span3">
             <div class="span4">

                      <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                               // build the link in Yii standard

                                echo CHtml::link($images,array('/discipline/recordPresence/index')); 
                                $this->back_url='/discipline/recordPresence/index';   
                     
                   ?>

                  </div> 
    
         </div>
 </div>

<div class="box box-info">
    <div class="box-body">
       
                            <div style="padding:0px;">
                               
                                <div class="left" style="margin-left:10px;">
                                    
                                    <select id="room-id">
                                        <option><?= Yii::t('app','-- Please select room --'); ?></option>
                                        <?php 
                                        foreach($room_data as $rd){
                                            ?>
                                        <option value="<?= $rd->id?>"><?= $rd->room_name; ?></option>
                                        <?php 
                                        }
                                        ?>
                                    </select>
                                    
                                </div>
                                
                                
                                <?php 
                                 if(!isAchiveMode($acad_sess)){
                                ?>
                                <div class="left espas-bouton" style="margin-left:10px;" >
                                    <a class="btn btn-warning" id="btn-save">
                                        <?= Yii::t('app', 'Save'); ?>
                                    </a>
                                     
   
                                </div>
                                <div class="left espas-bouton" style="margin-left:10px;">
                                    <a class="btn btn-warning" id="btn-save-email">
                                        <?= Yii::t('app', 'Save & Send email to parent'); ?>
                                    </a>
                                     
   
                                </div>
                                 <?php } ?>
                                
                            </div>   
    </div>
</div>



<?php 

$string_message = Yii::t('app','Absence recorded with success ! Choose a other room to continue the process!');

$message_box = "<span class='fas fa-comments alert alert-info alert-dismissible'>"
        . "<a href='#' class='close' data-dismiss='alert' aria-label='close'>×</a>"
        . "$string_message"
        . "</span>";

?>
<div id="konfime">
    <?= $message_box; ?>
</div>
<div id="rezilta">
    
</div>


<script>
  $(document).ready(function(){
      $('.espas-bouton').hide();
      $('#konfime').hide();
  });
  
  $(function() {
    $( ".custom_date" ).datepicker({
        changeMonth: true,
        changeYear: true
      });
  });
  
  
  $('#room-id').change(function(){
      var room_id = $('#room-id').val(); 
      $('.espas-bouton').show();
      $.get('<?= $baseUrl?>/index.php/discipline/recordPresence/showroomabsence',{room:room_id},function(data){
              $('#rezilta').html(data);
         });
    });
  
   $('#btn-save-email').click(function(){
       var tab_id_stud = $('#str-id-stud').val();
       var tab_stud_array = tab_id_stud.split(",");
       var room_id = $('#room-id').val();
       var konte = 0;
        for (i = 0; i < tab_stud_array.length; i++) {
            var status_absent = $('#RecordPresence_presence_type_'+tab_stud_array[i]).val(); 
            if(typeof status_absent!=='undefined'){
                // C'est ici que tout vas se passer 
                
                $.get('<?= $baseUrl?>/index.php/discipline/recordPresence/saveabsence',{student:tab_stud_array[i],room:room_id,status:status_absent,sendemail:1},function(data){
                     
                    });
              }
                    
                konte++;
            }
             $('#konfime').show();
            
     });
  
  
  
  </script>