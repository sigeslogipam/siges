<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

//echo "$nom_etudiant<br/>$date_presence<br/>$is_late";
$room_stud_data = RoomHasPerson::model()->findAllByAttributes(array('academic_year'=>$acad,'students'=>$student));
$room_id = 0;
foreach($room_stud_data as $rsd){
    $room_id = $rsd->room;
}
$room_name = Rooms::model()->findByPk($room_id)->short_room_name;

if(Persons::model()->findByPk($student)->image==NULL){
                $string_image_location = "<i class='fas fa-user-graduate fa-3x'></i>";

            }else{
                $string_image_location = "<img class='direct-chat-img' src='".Yii::app()->baseUrl."/documents/photo-Uploads/1/".Persons::model()->findByPk($student)->image."'>";
            }
$sql_str = "SELECT p.id, p.last_name, p.first_name, p.gender, r.short_room_name, p.id_number, rp.date_record FROM record_checkout rp "
        . "INNER JOIN persons p ON (rp.student = p.id) "
        . "INNER JOIN room_has_person rhp ON (rhp.students = rp.student)"
        . "INNER JOIN rooms r ON (r.id = rhp.room)"
        . "WHERE DATE(rp.date_record) = DATE(NOW()) AND rhp.academic_year = $acad";
$all_stud_late = Persons::model()->findAllBySql($sql_str);

?>
<div class="span3">
            <div class="prof-side">
                  <span class="prof-userpic"> <?= $string_image_location; ?> </span>
                  <div class="prof-usertitle"><?= Persons::model()->findByPk($student)->fullName; ?></div>
                </span><br/>
                <span class="prof-klas" style="font-weight: bold; font-size: 17px;">
                      <?= $room_name; ?>

                </span><br/>
                <span class="prof-date">
                    <span style="font-weight: bold; font-size: 12px;"> <?= Yii::t('app',"Checkout date and time")?> : </span><br/> <?= date("d",strtotime($date_presence)).' '.strftime('%B',strtotime($date_presence)).' '.strftime('%Y',strtotime($date_presence)); ?>
                    <?= Yii::t('app',' at {time}',array('{time}'=>date("H:i:s",strtotime($date_presence)))); ?>
                </span>
                </div>
        </div>
        <div class="span8">
            <table class="table responsive bordered, stripped">
                <h5><?= Yii::t('app','List of  checkout');?></h5>
                <thead>
                    <tr>
                        <th>#</th>
                        <th><?= Yii::t('app','Code') ?></th>
                        <th><?= Yii::t('app','First name') ?></th>
                        <th><?= Yii::t('app','Last name') ?></th>
                        <th><?= Yii::t('app','Gender') ?></th>
                        <th><?= Yii::t('app','Room') ?></th>
                        <th><?= Yii::t('app','Time In') ?></th>
                        <th><?= Yii::t('app','Time Out');?></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    $k = 1;
                    foreach($all_stud_late as $as){
                        ?>
                    <tr>
                        <td><?= $k; ?></td>
                        <td><?= $as->id_number; ?></td>
                        <td><?= $as->first_name; ?></td>
                        <td><?= $as->last_name; ?></td>
                        <td><?= $as->sexe; ?></td>
                        <td><?= $as->short_room_name; ?></td>
                        <td><?= date("H:i:s",strtotime($as->getCheckInTime($as->id, $as->date_record))); ?></td>
                        <td><?= date("H:i:s",strtotime($as->date_record)); ?></td>
                    </tr>
                    <?php
                    $k++;
                    }
                    ?>
                </tbody>
            </table>
        </div>

        
       
