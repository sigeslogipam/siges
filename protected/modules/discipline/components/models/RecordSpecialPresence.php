<?php

/**
 * This is the model class for table "record_special_presence".
 *
 * The followings are the available columns in table 'record_special_presence':
 * @property integer $id
 * @property integer $student
 * @property integer $academic_year
 * @property string $date_record
 * @property integer $presence_type
 * @property string $date_created
 * @property string $create_by
 * @property string $date_updated
 * @property string $update_by
 *
 * The followings are the available model relations:
 * @property Academicperiods $academicYear
 * @property Persons $student0
 */
class RecordSpecialPresence extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return RecordSpecialPresence the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'record_special_presence';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student, academic_year, date_record, presence_type, date_created, create_by', 'required'),
			array('student, academic_year, presence_type', 'numerical', 'integerOnly'=>true),
			array('create_by, update_by', 'length', 'max'=>64),
			array('date_updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, student, academic_year, date_record, presence_type, date_created, create_by, date_updated, update_by', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'academicYear' => array(self::BELONGS_TO, 'Academicperiods', 'academic_year'),
			'student0' => array(self::BELONGS_TO, 'Persons', 'student'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student' => 'Student',
			'academic_year' => 'Academic Year',
			'date_record' => 'Date Record',
			'presence_type' => 'Presence Type',
			'date_created' => 'Date Created',
			'create_by' => 'Create By',
			'date_updated' => 'Date Updated',
			'update_by' => 'Update By',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student',$this->student);
		$criteria->compare('academic_year',$this->academic_year);
		$criteria->compare('date_record',$this->date_record,true);
		$criteria->compare('presence_type',$this->presence_type);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('update_by',$this->update_by,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}