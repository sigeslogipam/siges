<?php

/**
 * This is the model class for table "disc_grade_period".
 *
 * The followings are the available columns in table 'disc_grade_period':
 * @property integer $id
 * @property integer $student
 * @property integer $id_period
 * @property double $grade_value
 * @property integer $academic_year
 *
 * The followings are the available model relations:
 * @property Academicperiods $idPeriod
 * @property Academicperiods $academicYear
 * @property Persons $student0
 */
class DiscGradePeriod extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DiscGradePeriod the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'disc_grade_period';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('student, id_period, deduction_value, academic_year', 'required'),
			array('student, id_period, academic_year', 'numerical', 'integerOnly'=>true),
			array('deduction_value', 'numerical'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, student, id_period, deduction_value, academic_year', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'idPeriod' => array(self::BELONGS_TO, 'Academicperiods', 'id_period'),
			'academicYear' => array(self::BELONGS_TO, 'Academicperiods', 'academic_year'),
			'student0' => array(self::BELONGS_TO, 'Persons', 'student'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'student' => 'Student',
			'id_period' => 'Id Period',
			'deduction_value' => 'Grade Value',
			'academic_year' => 'Academic Year',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('student',$this->student);
		$criteria->compare('id_period',$this->id_period);
		$criteria->compare('deduction_value',$this->grade_value);
		$criteria->compare('academic_year',$this->academic_year);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}