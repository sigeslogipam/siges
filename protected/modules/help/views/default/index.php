<div class="row-fluid">

    <div id="dash" class="row-fluid">
        <h2><?= Yii::t('app','SIGES Help center'); ?></h2>
        <div class="row-fluid">
        
        <div class="span12 alert alert-success" style="text-align: left">
            <p style="font-size: 14px;">
            Le service à la clientèle et la satisfaction de nos clients vont de pair chez Logipam. Notre équipe est amicale et compétente, elle est prête à vous aider à chaque contact, par téléphone, e-mail, WhatsApp, Telegram ou en présentiel.  Elle est toujours là pour recevoir vos commandes, résoudre des questions techniques ou de vous diriger vers des ressources utiles.
            </p>
            <p style="font-size: 14px;">
                &bull; <a target="_new" href="<?= Yii::app()->baseUrl;?>/documents/Guide_SIGES_2019.pdf">Télécharger le Guide d’utilisateur de SIGES.</a>
            </p>
            <p style="font-size: 14px;">
&bull; Rechercher et visualiser nos tutoriels (Veuillez saisir les mots clés qui pourraient correspondre à vos questions dans le champ de recherche ci-dessous).
            </p>
            <p style="font-size: 14px;">
&bull; Communiquer avec nous par email ou par téléphone à :
            </p>
            <p style="font-size: 14px;">
                &nbsp;&nbsp; &nbsp;  - support@logipam.com
            </p>
            <p style="font-size: 14px;">
            &nbsp;&nbsp; &nbsp; - WhatsApp, Telegram: + 509 36 01 29 59 / 48 64 69 50 / 33 31 75 28.
            </p>
<p style="font-size: 14px;">

Votre succès est notre succès !
            
</p>   
        </div>
        
    </div>
    </div>
</div>

<?php 
  
    $help = Help::model()->findAllBySql("SELECT * FROM help ORDER BY title ASC"); 
   
    $help_arr = array();
    $konte = 0;
    $ko = 0;
    foreach($help as $h){
        $ko++;
    }
    if($ko>0){
    foreach($help as $h){
        $help_arr['title'][$konte] = $h->title; 
        $help_arr['url'][$konte] = $h->url; 
        $help_arr['desc'][$konte] = $h->description;
        $konte++;
    }
   // echo $konte;
    if(($konte%4)>0){
        for($i=0;$i<4-$konte%4;$i++){
            array_push($help_arr['title'],""); 
            array_push($help_arr['url'],"");
            array_push($help_arr['desc'],""); 
        }
       // echo 4-$konte%4;
    }
?>

<div class="row-fluid">
    
    <div class="span12">
        
       <div class="row-fluid">
                
        <table class="videyo table-responsive table-condensed">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                <tbody>
           <?php 
            $l = 1; 
            if(sizeof($help_arr['title'])<4)
                $k = 4;
            else
                $k = sizeof($help_arr['title']);
            for($i=0;$i<$k;$i++){
                if($i>=sizeof($help_arr['title'])){
                    for($j=0;$j<4-sizeof($help_arr['title']);$j++){
                        echo '<td></td>';//'<div class="span3"></div>';
                    }
                    break;
                }
                if($l==1)
                    echo '<tr>';//'<div class="row-fluid">';
                
                $youtube_arr = explode("=",$help_arr['url'][$i]);
                 if(isset($youtube_arr[1])){
                       $youtube_id = $youtube_arr[1];
                 }else
                     $youtube_id = "";
                 if($youtube_id!=""){
                ?>
                <td>
                <span class="span3">
                    <h4><?= $help_arr['title'][$i]; ?></h4>
                    <img src="https://img.youtube.com/vi/<?= $youtube_id; ?>/hqdefault.jpg" class="img img-rounded img-responsive youtube-link" youtubeid="<?= $youtube_id?>"/>
                    
                    <p> <?= $help_arr['desc'][$i]; ?></p>
                    
                </span>
                </td>  
            <?php
                 } 
                 else {
                     ?>
                <td>
                    <span class="span3"></span>
                </td>
                <?php 
                 }
            if($l==4)
                echo '</tr>';//'</div>'; 
                $l++;
                if($l>4){
                    $l=1;
                }
                
            }
           ?> 
            
            </tbody>
                </table>           
               
               
         
        </div>
    </div>

</div>
    

    <?php }else{ ?>
        <div class="row-fluid">
            <div class="alert alert-success alert-dismissible">
                <?= Yii::t('app','No video found !');?>
            </div>
        </div>
    <?php } ?>

<script>
    
     // Demo video 1
    $(".youtube-link").grtyoutube({
            autoPlay:true,
            theme: "dark"
    });

    // Demo video 2
    $(".youtube-link-dark").grtyoutube({
            autoPlay:false,
            theme: "light"
    });
    
    $(document).ready(function(){
        $('.videyo').DataTable({
                pageLength: 3,
               // responsive: true,
                bLengthChange: false,
                fnDrawCallback: function() {
                    $(".videyo thead").remove();
                },
                initComplete: function () {
                        $('.dataTables_filter input[type="search"]').css({ 'width': '350px', 'display': 'inline-block' });
                    },
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                 
                ]

            });
            $('.html5buttons').remove();
          
    });
    
    
    
   

</script>