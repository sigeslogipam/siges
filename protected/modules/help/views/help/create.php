<?php
/* @var $this HelpController */
/* @var $model Help */

$this->breadcrumbs=array(
	'Helps'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Help', 'url'=>array('index')),
	array('label'=>'Manage Help', 'url'=>array('admin')),
);
?>

<div id="dash">
   <div class="span3"><h2>
        <?php echo Yii::t('app','Create Help video');?>
        
   </h2> </div>
    
    <div class="span3">
        
        
        <div class="span4">
            <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                     echo CHtml::link($images,array('help/index')); 

                    ?>
        </div>
    </div>
</div>


<?php echo $this->renderPartial('_form', array('model'=>$model)); ?>