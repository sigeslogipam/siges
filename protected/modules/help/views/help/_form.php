<?php
/* @var $this HelpController */
/* @var $model Help */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'help-form',
	'enableAjaxValidation'=>false,
)); ?>
<p class="note">
    
    <?php echo Yii::t('app','Fields with <span class="required">*</span> are required.'); ?>
    <?php echo $form->errorSummary($model); ?>
</p>
    
	<div  id="resp_form_siges">
            <form  id="resp_form">
                <div class="col-4">
                    <label id="resp_form">
                        <?php echo $form->labelEx($model,'title'); ?>
                        <?php echo $form->textField($model,'title',array('size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'title'); ?>
                    </label>
                </div>
                <div class="col-4">
                    <label id="resp_form">
                        <?php echo $form->labelEx($model,'url'); ?>
                        <?php echo $form->textField($model,'url',array('size'=>60,'maxlength'=>255)); ?>
                        <?php echo $form->error($model,'url'); ?>
                    </label>
                </div>
                <div class="col-4">
                    <label id="resp_form">
                        <?php echo $form->labelEx($model,'description'); ?>
                        <?php echo $form->textArea($model,'description',array('rows'=>6, 'cols'=>50)); ?>
                        <?php echo $form->error($model,'description'); ?>
                    </label>
                </div>
                <div class="col-submit">
                                
                    <?php if(!isset($_GET['id'])){
                              
                                  echo CHtml::submitButton(Yii::t('app', 'Create '),array('name'=>'create','class'=>'btn btn-warning'));

                             echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));
                            }
                             else
                               {   
                                      echo CHtml::submitButton(Yii::t('app', 'Save'),array('name'=>'update','class'=>'btn btn-warning'));

                                  echo CHtml::submitButton(Yii::t('app', 'Cancel '),array('name'=>'cancel','class'=>'btn btn-secondary'));

                                 } 
                               //back button   
                                  $url=Yii::app()->request->urlReferrer;
                                  $explode_url= explode("php",substr($url,0));

                                 if(!isset($_GET['from']))
                                    echo '<a href="'.$explode_url[0].'php'.$this->back_url.'" class="btn btn-secondary">'.Yii::t('app', 'Back').'</a>';

                    ?>
    </div>  
            </form>
        </div>

	

	

<?php $this->endWidget(); ?>

</div><!-- form -->