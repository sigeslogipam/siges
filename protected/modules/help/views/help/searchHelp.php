<?php 
  if($search ==""){
        $help = Help::model()->findAllBySql("SELECT * FROM help ORDER BY title ASC"); 
    }else{
        $help = Help::model()->findAllBySql("SELECT * FROM help WHERE title LIKE '%$search%' OR description LIKE '%$search%' ORDER BY title ASC");
    }
    $help_arr = array();
    $konte = 0;
    foreach($help as $h){
        $help_arr['title'][$konte] = $h->title; 
        $help_arr['url'][$konte] = $h->url; 
        $help_arr['desc'][$konte] = $h->description;
        $konte++;
    }
    
?>

 
    <div class="span12">
        <input type="hidden" id="konte" value="<?= $konte;?>">
        <div class="row-fluid plisvideo">
                
        <table class="table-responsive table-striped dataTables-exemple">
                    <thead>
                        <tr>
                            <th></th>
                            <th></th>
                            <th></th>
                            <th></th>
                        </tr>
                    </thead>
                <tbody>
           <?php 
            $l = 1; 
            if(sizeof($help_arr['title'])<4)
                $k = 4;
            else
                $k = sizeof($help_arr['title']);
            for($i=0;$i<$k;$i++){
                if($i>=sizeof($help_arr['title'])){
                    for($j=0;$j<4-sizeof($help_arr['title']);$j++){
                        echo '<td></td>';//'<div class="span3"></div>';
                    }
                    break;
                }
                if($l==1)
                    echo '<tr>';//'<div class="row-fluid">';
                
                $youtube_arr = explode("=",$help_arr['url'][$i]);
                 if(isset($youtube_arr[1])){
                       $youtube_id = $youtube_arr[1];
                 }
                ?>
                <td>
                <span class="span3">
                    <h4><?= $help_arr['title'][$i]; ?></h4>
                    <img src="https://img.youtube.com/vi/<?= $youtube_id; ?>/hqdefault.jpg" class="img img-rounded img-responsive nav-link youtube-link" youtubeid="<?= $youtube_id?>"/>
                    
                        <?= $help_arr['desc'][$i]; ?>
                    
                </span>
                </td>  
            <?php
            if($l==4)
                echo '</tr>';//'</div>'; 
                $l++;
                if($l>4){
                    $l=1;
                }
                
            }
           ?> 
            
            </tbody>
                </table>           
               
               
         
        </div>
    </div>




<script>
    var start = 0;
    var limit = 8;
    var reachedMax = false;
     // Demo video 1
    $(".youtube-link").grtyoutube({
            autoPlay:true,
            theme: "dark"
    });

    // Demo video 2
    $(".youtube-link-dark").grtyoutube({
            autoPlay:false,
            theme: "light"
    });
    
    $('.dataTables-exemple').DataTable({
                pageLength: 2,
                bLengthChange: false,
                responsive: true,
                bSortCellsTop: true,
                bPaginate: true,
                fnDrawCallback: function() {
                    $(".dataTables-exemple thead").remove();
                },
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "Traitement en cours...",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    //lengthMenu:    " _MENU_ ",
                   // info:           "<?= Yii::t('app','Show student'); ?> _START_ &agrave; _END_ sur _TOTAL_  ss &eacute;l&eacute;ments",
                    infoEmpty:      "",
                   // infoFiltered:   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
                    infoPostFix:    "",
                    loadingRecords: "Chargement en cours...",
                    zeroRecords:    "Aucun &eacute;l&eacute;ment &agrave; afficher",
                    emptyTable:     "<?= Yii::t('app','No data to show !')?>",
                    

                    paginate: {
                        first:      "Premier",
                        previous:   "Pr&eacute;c&eacute;dent",
                        next:       "Suivant",
                        last:       "Dernier"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                   // {extend: 'excel', title: 'siges_custom_report'},
                  
                /*    {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
    
   

</script>