<div id="dash">
   <div class="span3"><h2>
        <?php echo Yii::t('app','Manage SIGES Help center');?>
        
   </h2> </div>
    
    <div class="span3">
        <div class="span4">
            <?php
                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                               // build the link in Yii standard
                     echo CHtml::link($images,array('help/create')); 
            ?>
        </div>
        
        <div class="span4">
            <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                     echo CHtml::link($images,array('help/index')); 

                    ?>
        </div>
    </div>
</div>

<div class="clear"></div>

<?php 
//$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']);
$this->widget('bootstrap.widgets.TbGridView', array(
	'id'=>'help-index-grid',
	'summaryText'=>'',
	'dataProvider'=>$model->search(),
	'itemsCssClass' => 'table-bordered items',
	'columns'=>array(
		
		
            array(
                    'name' => 'title',
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->title,Yii::app()->createUrl("help/help/update",array("id"=>$data->id)))',
                   
                     ),
            'description',
		
		'url',
            
		
		
		array(
			'class'=>'CButtonColumn',
			'template'=>'{update}{delete}',
                        'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                        
                        
		),
	),
)); 
?>

            
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                 
                ]

            });
           

        });
</script>