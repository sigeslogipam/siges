<?php

/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is a free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>
    <?php

$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
 

 $template =''; 
 
    $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
      if($current_acad==null)
						          $condition = '';
						     else{
						     	   if($acad!=$current_acad->id)
							         $condition = '';
							      else
							         $condition = 'teacher0.active IN(1,2) AND ';
						        }

     
     
$tit = Yii::t('app','Courses');     

?>


<!-- Menu of CRUD  -->

<div id="dash">
          
          <div class="span3"><h2>


 <?php echo Yii::t('app','Courses'); ?></h2> </div>
     
		   <div class="span3">

        <?php 
               if(!isAchiveMode($acad_sess))
                 {     $template ='{update}{delete}';    
        ?>
             		<div class="span4">

                  <?php

                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';

                               // build the link in Yii standard

                        if(isset($_GET['from'])&&($_GET['from']=='teach'))
                          {  
                              echo CHtml::link($images,array('courses/create/from/teach')); 
                              
                           }
                        else
                          echo CHtml::link($images,array('courses/create')); 

                   ?>

            	</div>
                       <?php if(infoGeneralConfig('grid_creation')!=null){ ?>
                       <div class="span4">
                          <?php  
                          $images = '<i class="fa fa-table"> &nbsp; '.Yii::t('app','Mass adding').'</i>'; 
                          echo CHtml::link($images,array('courses/gridcreateCourse/isstud/1/pg/lr/mn/std'));
                          ?>
                       </div>              
                       
                       <?php } ?>
        
       <?php
                 }
      
      ?>       
       
        
        
          <div class="span4">

                      <?php

                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';


                               // build the link in Yii standard
                      if(isset($_GET['from'])&&($_GET['from']=='teach'))
                        {  
                          
                          
                          echo CHtml::link($images,array('/academic/persons/listForReport/isstud/0/from/teach')); 
                          
                         }
                      else
                       {  
                          echo CHtml::link($images,array('/academic/persons/viewListAdmission/isstud/1/pg/lr/mn/std')); 
                          
                         }

                   ?>

            </div>   


         </div>
 </div>



<div style="clear:both"></div>



<div class="grid-view">

          
           <div  class="search-form">

<?php 

  $columns = array(
		
          array('name'=>'subject_name',
			'header'=>Yii::t('app','Subject name'),
			'value'=>'$data->subject0->subjectName'),
		
			'weight',
		
          array('name'=>Yii::t('app','Minimum Passing'),
			'header'=>Yii::t('app','Minimum Passing'),
			'value'=>'$data->getPassingGradeForCourse($data->id)',
			),
            
         array('name'=>'room_name',
			'header'=>Yii::t('app','Room'),
			'value'=>'$data->room0->short_room_name'),
           
           array('name'=>'debase',
                 'header'=>Yii::t('app','De base'),
                 'value'=>'$data->Debase',
                 ),
                 
           array('name'=>'optional',
                 'header'=>Yii::t('app','Optional'),
                 'value'=>'$data->Optional',
                 ),   
		
           array(
                    'name' => 'teacher_lname',
                    'header'=>Yii::t('app','Teacher Name'),
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->teacher0->first_name." ".$data->teacher0->last_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->teacher,"isstud"=>0,"from"=>"teach")))',
                    'htmlOptions'=>array('width'=>'150px'),  
                ),
                
			
		                               
				array(
			'class'=>'CButtonColumn',
                        'template'=> $template,
                        'buttons'=>array(
                           'update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/schoolconfig/courses/update?id=$data->id&pg=tea&pers=$data->id")',
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                        		),
            
	);
  
  /*
    if(infoGeneralConfig("display_homework_grade_menu") ==1)
    {
        $columns = array(
		
          array('name'=>'subject_name',
			'header'=>Yii::t('app','Subject name'),
			'value'=>'$data->subject0->subjectName'),
		
			'weight',
                       
                        'homework_weight',
		
          array('name'=>Yii::t('app','Minimum Passing'),
			'header'=>Yii::t('app','Minimum Passing'),
			'value'=>'$data->getPassingGradeForCourse($data->id)',
			),
            
         array('name'=>'room_name',
			'header'=>Yii::t('app','Room'),
			'value'=>'$data->room0->short_room_name'),
           
           array('name'=>'debase',
                 'header'=>Yii::t('app','De base'),
                 'value'=>'$data->Debase',
                 ),
                 
           array('name'=>'optional',
                 'header'=>Yii::t('app','Optional'),
                 'value'=>'$data->Optional',
                 ),   
		
           array(
                    'name' => 'teacher_lname',
                    'header'=>Yii::t('app','Teacher Name'),
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->teacher0->first_name." ".$data->teacher0->last_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->teacher,"isstud"=>0,"from"=>"teach")))',
                    'htmlOptions'=>array('width'=>'150px'),  
                ),
                
			
		                               
				array(
			'class'=>'CButtonColumn',
                        'template'=> $template,
                        'buttons'=>array(
                           'update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/schoolconfig/courses/update?id=$data->id&pg=tea&pers=$data->id")',
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                        		),
            
	);
    }
    */
    
        
        $gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'courses-grid',
	'summaryText'=>'',
	'dataProvider'=>$model->search($condition,$acad_sess),
	//'mergeColumns'=>array('subject_name','room_name','teacher_lname'),
           
	'columns'=>$columns,
            
)); 


?>

           </div>
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit;?>" },
                  
                    /*{extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                    
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>

</div>