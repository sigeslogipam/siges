<?php
/*
 * © 2019 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
/**
 * This is the model class for table "devoirs".
 *
 * The followings are the available columns in table 'devoirs':
 * @property integer $id
 * @property integer $student
 * @property integer $course
 * @property integer $academic_period
 * @property double $grade_value
 * @property string $comment
 * @property string $date_created
 * @property string $date_updated
 * @property string $create_by
 * @property string $update_by
 *
 * The followings are the available model relations:
 * @property Academicperiods $academicPeriod
 * @property Courses $course0
 * @property Persons $student0
 */
class Devoirs extends BaseDevoirs
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Devoirs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
public $student_iD;
	
	public $last_name;
	public $first_name;
	public $student_name;
	public $gender;
        //Field for search
        public $s_full_name;
        public $s_subject_name;
        public $weight;
		
        public $max_grade;
        public $name_period;
        public $sum;
        public $average;
        public $course_name;
	public $courseName;
        public $evaluation_date;
	public $subject_name;
	public $room_name;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'devoirs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array_merge(
		    	parent::rules(), array(
			array('student, course, academic_period', 'required'),
			array('student, course, academic_period', 'numerical', 'integerOnly'=>true),
			array('grade_value', 'numerical'),
                            array('grade_value','gradecompare','message'=>'Grade value can\'t be greater than weight!'),
                            array('course_name, courseName', 'length', 'max'=>125),
                        //komante liy sa le ou bezwen plizye devwa nan yon sel peiod,lap bay som devwa yo nan rezilta kane a   
                        array('student+course+academic_period','application.extensions.uniqueMultiColumnValidator'),
                            
			array('comment', 'length', 'max'=>255),
			array('create_by, update_by', 'length', 'max'=>45),
			array('date_created, date_updated', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, first_name, last_name, s_full_name,s_subject_name, student_name, course_name, courseName,student, course, academic_period, grade_value, comment, date_created, date_updated, create_by, update_by', 'safe', 'on'=>'search'),
		));
	}

/*	public function attributeLabels()
	{
		
            return array_merge(
                    parent::attributeLabels(), 
                    array('validate'=>Yii::t('app','Validate'),
                    'publish'=>Yii::t('app','Publish'),
                    
                        )
                    );
           
	}
 * 
 */
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */

public function search($condition,$acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
$siges_structure = infoGeneralConfig('siges_structure_session');

		$criteria=new CDbCriteria;
		$criteria->alias='d';
		$criteria->with= array('student0','course0','academicPeriod',);
		$student_name=$this->first_name.' '.$this->last_name;
        
                $criteria->join='inner join courses c ON(c.id =d.course) inner join subjects s ON(s.id=c.subject)';
                $criteria->join.='inner join academicperiods a ON(a.id =d.academic_period)';

		
		if($siges_structure==1)
		    $criteria->condition = $condition.' (course0.academic_period=:acad  OR a.id=:acad)';
		elseif($siges_structure==0)
		   $criteria->condition = $condition.' (course0.academic_period=:acad  OR a.year=:acad)';


		
		$criteria->params = array(':acad'=>$acad);
			
		$criteria->compare('id',$this->id);
		$criteria->compare('student0.first_name',$this->first_name,true);
		$criteria->compare('course0.subject',$this->course,true);
		$criteria->compare('subject_name',$this->course_name,true);
		$criteria->compare('academic_period',$this->academic_period,true);
		$criteria->compare('grade_value',$this->grade_value);
                $criteria->compare('comment',$this->comment);
		$criteria->compare('academic_period',$this->academic_period,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
                
                $criteria->compare('student0.last_name',$this->last_name, true);
                $criteria->compare('course0.weight',$this->weight,true);
               
                $criteria->order = 'last_name ASC';

		return new CActiveDataProvider($this, array(
			
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
				
			'criteria'=>$criteria,
			
		));
	}
   
    public function searchById($id)
	{   
                        $criteria = new CDbCriteria;
	
			$criteria->condition = 'd.id like(:id)';
			$criteria->params = array(':id' => $id);
            
                        $criteria->alias = 'd';
			
			$criteria->select = '*';
			
		    
			
    return new CActiveDataProvider($this, array(
       
				
		'criteria'=>$criteria,
		
				
    ));
	} 
	
public function searchForTeacherUser($condition,$id_teacher,$acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
 $siges_structure = infoGeneralConfig('siges_structure_session');
 
		$criteria=new CDbCriteria;
		$criteria->alias='d';
		$criteria->with= array('student0','course0','academicPeriod',);
		$student_name=$this->first_name.' '.$this->last_name;
        
                $criteria->join='inner join courses c ON(c.id =d.course) inner join subjects s ON(s.id=c.subject)';
                $criteria->join.='inner join academicperiods a ON(a.id =d.academic_period)';

		
		
		if($siges_structure==1)
		    $criteria->condition = $condition.' (course0.academic_period=:acad  OR a.id=:acad) AND c.teacher=:teacher';
		elseif($siges_structure==0)
		  $criteria->condition = $condition.' (course0.academic_period=:acad  OR a.year=:acad) AND c.teacher=:teacher';


		
		$criteria->params = array(':acad'=>$acad,':teacher'=>$id_teacher);
					
		$criteria->compare('id',$this->id);
		$criteria->compare('student0.first_name',$this->first_name,true);
		$criteria->compare('course0.subject',$this->course,true);
		$criteria->compare('subject_name',$this->course_name,true);
		$criteria->compare('grade_value',$this->grade_value);
		$criteria->compare('comment',$this->comment);
                $criteria->compare('academic_period',$this->academic_period,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
              
                $criteria->compare('student0.last_name',$this->last_name, true);
                $criteria->compare('course0.weight',$this->weight,true);
               
                $criteria->order = 'last_name ASC';

		return new CActiveDataProvider($this, array(
			
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
				
			'criteria'=>$criteria,
			
		));
	}
   


public function searchForTeacherUserRoomCourse($condition,$period,$course_id, $acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
 $siges_structure = infoGeneralConfig('siges_structure_session');
 
		$criteria=new CDbCriteria;
		$criteria->alias='d';
		$criteria->with= array('student0','course0','academicPeriod',);
		$student_name=$this->first_name.' '.$this->last_name;
        
                $criteria->join='inner join courses c ON(c.id =d.course) inner join subjects s ON(s.id=c.subject)';
                $criteria->join.='inner join academicperiods a ON(a.id =d.academic_period)';

                
		if($siges_structure==1)
		    $criteria->condition = $condition.' d.academic_period='.$period.' AND d.course='.$course_id.' AND (course0.academic_period='.$acad.'  OR a.id='.$acad.')';
		elseif($siges_structure==0)
		  $criteria->condition = $condition.' d.academic_period='.$period.' AND d.course='.$course_id.' AND (course0.academic_period='.$acad.'  OR a.year='.$acad.')';
					
		$criteria->compare('id',$this->id);
		$criteria->compare('student0.first_name',$this->student_name,true);
		$criteria->compare('course0.subject',$this->course,true);
		$criteria->compare('subject_name',$this->course_name,true);
		$criteria->compare('grade_value',$this->grade_value);
		$criteria->compare('comment',$this->comment);
                $criteria->compare('academic_period',$this->academic_period,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
                $criteria->compare('student0.last_name',$this->s_full_name, true);
                $criteria->compare('course0.weight',$this->weight,true);
               
			   $criteria->order = 'last_name ASC';

		return new CActiveDataProvider($this, array(
			
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
				
			'criteria'=>$criteria,
			
		));
	}
   
	
    
    // Return a nice formatted string for student grades 
    public function getStudentGrade(){
            
            return $this->student0->fullName.' ('.$this->course0->courseName.') ('.$this->academicPeriod->name_period.') ->'.$this->grade_value; 
        }
	
	
	
	
public function searchByRoom($course_id, $period_id)
	{   
          	 $acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

   
   
          	 $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
		     if($acad!=$current_acad->id)
		         $condition = '';
		      else
		         $condition = 'p.active IN(1,2) AND ';
         
         
         
                        $criteria = new CDbCriteria;
	
			$criteria->condition = $condition.' d.course =:course AND d.academic_period =:evaluation';// AND validate=0';
			$criteria->params = array(':course'=>$course_id,':evaluation'=>$period_id);
			
			$criteria->alias = 'd';
			$criteria->distinct = 'true';
			$criteria->select = 'p.first_name, p.last_name, p.gender, d.id, d.grade_value, c.weight, d.comment '; // on doit avoir shift et section aussi
			$criteria->join .= 'left join persons p on (p.id = d.student) ';
                        $criteria->join .= 'left join courses c on (c.id = d.course) ';
			$criteria->order = 'last_name ASC';
            
		    
		    
			
			
			
                    return new CActiveDataProvider($this, array(
                        'pagination'=>array(
                                                'pageSize'=> 100000,
                                        ),

                                'criteria'=>$criteria,


                    ));
	}
	
        
 //return a boolean. true-false		
public function isCourseHasGrades($course_id, $acad)
	{   	    
         $siges_structure = infoGeneralConfig('siges_structure_session');
	     
	     $boul=false;
			
			if($siges_structure==1)
		    $sql='SELECT d.grade_value FROM devoirs  d inner join courses c on (c.id=d.course) inner join academicperiods a ON(a.id=d.academic_period) WHERE d.course ='.$course_id.' AND (c.academic_period='.$acad.' OR a.id='.$acad.')';
		elseif($siges_structure==0)
		    $sql='SELECT d.grade_value FROM devoirs  d inner join courses c on (c.id=d.course) inner join academicperiods a ON(a.id=d.academic_period) WHERE d.course ='.$course_id.' AND (c.academic_period='.$acad.' OR a.year='.$acad.')';


		   		  $result = Yii::app()->db->createCommand($sql)->queryAll();
           
           if($result!=null)
             {   foreach($result as $r)
                   { $boul=true;
                       break;
                   }
             }
        
        return $boul;    
 }
		
     // Report card function 
        
   	public function searchForReportCard($condition,$student,$course_id, $period_id)
	{   
                        $criteria = new CDbCriteria;
	
			$criteria->condition = $condition.' d.student =:student AND d.course =:course AND d.academic_period =:academicperiod';
			$criteria->params = array(':student' => $student,':course'=>$course_id,':academicperiod'=>$period_id);
            
                        $criteria->alias = 'd';
			$criteria->distinct = 'true';
			$criteria->join = 'inner join persons p on(p.id = d.student)';
			$criteria->select = 'd.id, d.grade_value, d.comment';
			//$criteria->limit = '100';
		    
		 
                    return new CActiveDataProvider($this, array(
                        'pagination'=>array(
                                                'pageSize'=> 100000,
                                        ),

                                'criteria'=>$criteria,


                    ));
	}       
        
   
public function searchByStudentIdForGuestUser($student_id, $acad)
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.
 $siges_structure = infoGeneralConfig('siges_structure_session');
 
		$criteria=new CDbCriteria;
		$criteria->alias='d';
		
		
		if($siges_structure==1)
		    $criteria->condition = "c.old_new=1 AND c.room=(select room from room_has_person rp where rp.students=:student and rp.academic_year=:acad) AND student=:student AND (c.academic_period=:acad OR a.id=:acad) AND g.publish=1";
		elseif($siges_structure==0)
		   $criteria->condition = "c.old_new=1 AND c.room=(select room from room_has_person rp where rp.students=:student and rp.academic_year=:acad) AND student=:student AND (c.academic_period=:acad OR a.year=:acad) AND g.publish=1";

		
		        $criteria->params = array(':student'=>$student_id,':acad'=>$acad);
        
        $criteria->join ="left join room_has_person rh on(d.student=rh.students) inner join persons p on(p.id=d.student) inner join courses c ON(c.id=d.course)  inner join subjects s ON(s.id=c.subject) inner join academicperiods a ON(a.id=d.academic_period) ";
        
		$student_name=$this->first_name.' '.$this->last_name;
 
		$criteria->compare('id',$this->id);
		$criteria->compare('p.first_name',$this->student_name,true);
		$criteria->compare('c.subject',$this->course,true);
		$criteria->compare('subject_name',$this->course_name,true);
		$criteria->compare('grade_value',$this->grade_value);
		$criteria->compare('comment',$this->comment);
		$criteria->compare('academic_period',$this->academic_period,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('date_updated',$this->date_updated,true);
		$criteria->compare('create_by',$this->create_by,true);
		$criteria->compare('update_by',$this->update_by,true);
               $criteria->compare('p.last_name',$this->s_full_name, true);
               $criteria->compare('c.weight',$this->weight,true);
              
			 //  $criteria->order = 'eby.evaluation_date ASC';
			  
			  
			  		return new CActiveDataProvider($this, array(
 			
			'pagination'=>array(
        			'pageSize'=> Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']),
    			),
				
			'criteria'=>$criteria,
			
		));
	}
       
  	 public function gradecompare($attribute, $params){
		            $message = Yii::t('app','The grade must be lower than the weight!');
		            if($this->grade_value >  $this->course0->weight)
		            {
		                $params = array(
		                    '{attribute}'=>$this->course0->weight, '{compareValue}'=>$this->grade_value
		                );
		                $this->addError('grade_value', strtr($message, $params));
		            }
		        }
          
          
        
        
        
        
        
        
        
        
        
        
        
        
}