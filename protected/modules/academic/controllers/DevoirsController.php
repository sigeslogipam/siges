<?php
/*
 * © 2019 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */


Yii::import('ext.tcpdf.*');
Yii::import('ext.tcpdf.SIGESPDF');


class DevoirsController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/column2';
	private $_model;
	
	public $back_url='';
	
	public $message_noGrades=false;
	public $message_gradesOk=false;
	public $message_GradeHigherWeight=false;
	
	public $message_noGradeEntered=false;
	
	public $template_update_only = false;
	
	public $success_=0;
	
	
	public $idShift;
	public $section_id;
	public $idLevel;
	public $room_id;
	public $grade_id;
	
	public $first_name;
	public $last_name;
	
	
	public $idAcademicP;
	
	public $temoin_update=0;
	public $temoin_list;
	public $old;
	
	public $course_id;
	public $period_id;
	
	public $message_room_id=false;
	public $message_course_id=false; 
        public $message_period_id=false;
	public $success=false;
	
	public $messageNoCheck=false;
	public $use_update=false;
	
	public $useCourseReference=false;
	
	
	public $extern=false;

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', 
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		  $explode_url= explode("/",substr($_SERVER['REQUEST_URI'], 1));
            $controller=$explode_url[3];
           
            $actions=$this->getRulesArray($this->module->name,$controller);
              
            if($this->getModuleName($this->module->name))
                {
		            if($actions!=null)
             			 {     return array(
				              	  	array('allow',  
					                	
					                	'actions'=> $actions,
		                                  'users'=>array(Yii::app()->user->name),
				                    	),
				              		  array('deny',  
					                 	'users'=>array('*'),
				                    ),
			                );
             			 }
             			 else
             			  return array(array('deny', 'users'=>array('*')),);
                }
                else
                {
                    return array(array('deny', 'users'=>array('*')),);
                }


	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	 public function actionCreate()
	{   
		
		$model=new Devoirs;
        $modelShift= new Shifts;
		$modelSection= new Sections;
		$modelLevel= new LevelHasPerson;
		$modelRoom=new Rooms;
		$modelCourse=new Courses;
		$modelEvaluation= new Devoirs;
		
		$this->use_update=false;
		
		$this->performAjaxValidation($model);
        
        if(isset($_GET['stud'])&&($_GET['stud']!=""))
            $this->extern=true;
       else
            $this->extern=false;
       
        
		if($this->extern)
		  {
		  	       
		  	    if((Yii::app()->user->profil!='Teacher'))
                  {    
		  	if(isset($_POST['Devoirs']))
               	         {   $modelEvaluation->attributes=$_POST['Devoirs'];
						     $this->period_id=$modelEvaluation->academic_period;
						     Yii::app()->session['Period']=$this->period_id;
						   
						     $modelCourse->attributes=$_POST['Courses'];
						     $this->course_id=$modelCourse->subject;
						     Yii::app()->session['Courses']=$this->course_id;
						     
						      

               	         }
               	      
               	         
		            }

		  }
		else
		 {
			 if((Yii::app()->user->profil!='Teacher'))
     			 {	    if(isset($_POST['Shifts']))
			               	{  //on n'a pas presser le bouton, il faut charger apropriate rooms
							     $modelShift->attributes=$_POST['Shifts'];
					              $this->idShift=$modelShift->shift_name;
					              
											  
								   $modelSection->attributes=$_POST['Sections'];
								   $this->section_id=$modelSection->section_name;
							     						
								   $modelLevel->attributes=$_POST['LevelHasPerson'];
								   $this->idLevel=$modelLevel->level;
								   
								   $modelRoom->attributes=$_POST['Rooms'];
								   $this->room_id=$modelRoom->room_name;
								   Yii::app()->session['Rooms']=$this->room_id; 
								   
								   $modelCourse->attributes=$_POST['Courses'];
								   $this->course_id=$modelCourse->subject;
								   Yii::app()->session['Courses']=$this->course_id;
								   
								   $modelEvaluation->attributes=$_POST['Devoirs'];
								   $this->period_id=$modelEvaluation->academic_period;
								   Yii::app()->session['Period']=$this->period_id;
							
					       
								   
				             }				   
							else
							{  $this->idShift=null;
						       $this->section_id=null;
							   $this->idLevel=null;
							   $this->room_id=null;
							   $this->course_id=null;
							
                                                         }
				
     			 }//fen  if((Yii::app()->user->profil!='Teacher'))
                else // Yii::app()->user->profil=='Teacher'
                  {
                  	                      	   
                  	   if(isset($_POST['Rooms']))
			               	{  //on n'a pas presser le bouton, il faut load apropriate rooms
							    
								   $modelRoom->attributes=$_POST['Rooms'];
								   $this->room_id=$modelRoom->room_name;
								   Yii::app()->session['Rooms']=$this->room_id;
								   
								   $modelCourse->attributes=$_POST['Courses'];
								   $this->course_id=$modelCourse->subject;
								   Yii::app()->session['Courses']=$this->course_id;
								   
								   $modelEvaluation->attributes=$_POST['Devoirs'];
								   $this->period_id=$modelEvaluation->academic_period;
								   Yii::app()->session['Period']=$this->period_id;
							
		           
		                    
					  
								   
				             }				   
							
                  	
                    }// Yii::app()->user->profil=='Teacher'
                    
				
	         }
	         
	         
			 if(isset($_POST['create']))
				  { //on vient de presser le bouton
						 //reccuperer les lignes selectionnees()
					$this->message_room_id=false;
					$this->message_course_id=false; 
					$this->message_period_id=false;
					$this->success=false;
					$temwen=false;
			        
					$this->message_noGradeEntered=false;
					   $this->message_GradeHigherWeight=false;	
					   $no_stud_has_grade=true;
                    $weight = ' ';
                     
                     $result = Courses::model()->getWeight($this->course_id);
                    $result =$result->getData();
                    foreach($result as $r)
                      $weight = $r->weight;
			      
				 
				 if($this->extern)
				  {
			  	         $grade=0;
			  	         $comment='';
					   
					   //tcheke si stud la record pou evaluation sa deja, pran ID a Update li
					   
					   //sinon akseptel kom nouvo ensesyon
					   
					   $this->message_noGradeEntered=false;
					   $no_stud_has_grade=true;
					   
					  foreach($_POST['id_stud'] as $id)
                        {   	   
						           if(isset($_POST['devoirs'][$id])&&($_POST['devoirs'][$id]!=''))
						                $no_stud_has_grade=false;
							
						}
						
					  
					if(!$no_stud_has_grade) 
						{  foreach($_POST['id_stud'] as $id)
	                        {   	   
							           if(isset($_POST['devoirs'][$id]))
							                $grade=$_POST['devoirs'][$id];
										else
											$grade=0;
											
									    if(isset($_POST['comments'][$id]))
							                $comment=$_POST['comments'][$id];
										else
											$comment='';
						               
								   //check if grade is higher than the course weight	
									if($grade <= $weight)  	   
										{      
										   $model->setAttribute('student',$id);
										   $model->setAttribute('course',$this->course_id);
										   $model->setAttribute('academic_period',$this->period_id);
										   $model->setAttribute('grade_value',$grade);
										   $model->setAttribute('comment',$comment);
										   $model->setAttribute('date_created',date('Y-m-d'));
										   $model->setAttribute('create_by',currentUser());
										   
										   if($model->save())
			                                 {  
											   $model->unSetAttributes();
											   $model= new Devoirs;
											   
											   $temwen=true;
											   
									         }
									         
									 	 }
								    else
						               {
						              	     
						              	     $this->message_GradeHigherWeight=true;
						              	 
						               }   
		                     
							}
				        }
				      else //message vous n'avez entre aucune note
						{
							$this->message_noGradeEntered=true;
							
							}
						
				  	  $this->extern=false;
					}
				 else
					{		 
					 if(($this->room_id!="")&&($this->course_id!="")&&($this->period_id!=""))
				        {         $grade=0;
				                  $comment='';
						   
								     $this->message_noGradeEntered=false;
							   $no_stud_has_grade=true;
							   
							  foreach($_POST['id_stud'] as $id)
		                        {   	   
								           if(isset($_POST['devoirs'][$id])&&($_POST['devoirs'][$id]!=''))
								                $no_stud_has_grade=false;
									
								}
								
							  
							if(!$no_stud_has_grade) 
								{  
								   foreach($_POST['id_stud'] as $id)
			                        {   	   
									        if(isset($_POST['devoirs'][$id]))
								                $grade=$_POST['devoirs'][$id];
											else
												$grade=0;
												
												
											if(isset($_POST['comments'][$id]))
								                $comment=$_POST['comments'][$id];
											else
												$comment='';
							           
							                   //check if grade is higher than the course weight
						           if($grade <= $weight)  
						             {  
								      
								       }
						            else
						              {
						              	 $grade='';
						              	 $this->message_GradeHigherWeight=true;
						              	 
						               }
           
									       $model->setAttribute('student',$id);
										   $model->setAttribute('course',$this->course_id);
										   $model->setAttribute('academic_period',$this->period_id);
										   $model->setAttribute('grade_value',$grade);
										   $model->setAttribute('comment',$comment);
										   $model->setAttribute('date_created',date('Y-m-d'));
										   $model->setAttribute('create_by',currentUser());
										   
										   if($model->save())
			                                 {  
											   $model->unSetAttributes();
											   $model= new Devoirs;
											   
											   $temwen=true;
											   
									         }
			                     
										}
										
				                  }
				                else //message vous n'avez entre aucune note
									{
										$this->message_noGradeEntered=true;
							        }
							        
							        
							}						     
						   else
						     {
						     	if($this->room_id=="")
						     	  $this->message_room_id=true;
						     	  
						     	if($this->course_id=="")
						     	  { $this->message_course_id=true;
						     	     $this->message_room_id=true;
						     	  }
						     	  
						     	if($this->period_id=="")
						     	  { $this->message_period_id=true;
						     	    $this->message_room_id=true;
						     	  }
						      }
						      
						   }
						
						if($temwen)
						  $this->success=true;
						  
						
						$this->room_id=null;
						
					
						
		   }
		   
		   
		    if(isset($_POST['cancel']))
              {
                 
                     $this->redirect(Yii::app()->request->urlReferrer);
                          
                 }
		
		
		$this->render('create',array(
			'model'=>$model,
		));
		
	}

	

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
		public function actionUpdate()
	{   
		$current_acad= 0;
		$current_year=currentAcad();
		if($current_year!=null)
			$current_acad= $current_year->id;
		
		
		$acad_sess = acad_sess();
        $acad=Yii::app()->session['currentId_academic_year']; 
if($acad != $current_acad)
{	
$this->redirect(Yii::app()->request->urlReferrer);
}
		
		       $this->message_noGrades=false;
		       $this->message_gradesOk=false;
		       $this->messageNoCheck=false;
		        $this->useCourseReference=false;
		       $this->message_GradeHigherWeight=false;
		      
		       $weight = ' ';
                     
		      
		 if(!isset($_GET['all']))
		  {  $model=$this->loadModel();
              $this->performAjaxValidation($model);
		      
		           
		        $result = Courses::model()->getWeight($model->course);
                    $result =$result->getData();
                    foreach($result as $r)
                      $weight = $r->weight;

            
		     	if(isset($_POST['update']))
				   {   //on vient de presser le bouton
								 //reccuperer le ligne modifiee
									 $grade=0;
									 $comment='';
									 
							   foreach($_POST['id_stud'] as $id)
								{   	   
										   if(isset($_POST['devoirs'][$id]))
												$grade=$_POST['devoirs'][$id];
												
												
											if(isset($_POST['comments'][$id]))
								                $comment=$_POST['comments'][$id];
																						
										   
										  if($grade<=$weight) 
										  {
										  	  $model->setAttribute('date_updated',date('Y-m-d'));
										      $model->setAttribute('grade_value',$grade);
										      $model->setAttribute('comment',$comment);
										      $model->setAttribute('update_by',currentUser());
										   
										   
										     if($model->save())
											    { 											   
											      $this->redirect(array('view','id'=>$model->id,'from'=>'stud'));
											    }
											 
										   }
										 else
										   $this->message_GradeHigherWeight=true;

								 
								}
					
					
						
				}
		     
		    
				if(isset($_POST['cancel']))
                          {
                             
                              $this->redirect(Yii::app()->request->urlReferrer);
                          }
                          
		     
		       	 
		       	 
		  }
		elseif($_GET['all']==1)
		 {	$model=new Devoirs;
			$modelRoom=new Rooms;
			$modelCourse=new Courses;
			$modelEvaluation= new Devoirs;
			
		
			if(isset($_POST['Rooms']))
					{        						   
								$this->message_noGrades=false;
								
							   $modelEvaluation->attributes=$_POST['Devoirs'];
								   $this->period_id=$modelEvaluation->academic_period;
								   Yii::app()->session['Period']=$this->period_id;

                                    $modelRoom->attributes=$_POST['Rooms'];
								   $this->room_id=$modelRoom->room_name;
								   Yii::app()->session['Rooms']=$this->room_id;
								   
								   $modelCourse->attributes=$_POST['Courses'];
								   $this->course_id=$modelCourse->subject;
								   Yii::app()->session['Courses']=$this->course_id;
								   
				   
							   
							   
					           
							   
						    
			$result = Courses::model()->getWeight($this->course_id);
                    $result =$result->getData();
                    foreach($result as $r)
                      $weight = $r->weight;  
 
												   
					 }
				
				  	
				  
				if(isset($_POST['update']))
					{   //on vient de presser le bouton
									 //reccuperer le ligne modifiee
						
						$this->success_=0;
						
						$ok=false;
						$this->message_GradeHigherWeight=false;
						
						
										 $grade=0;
									 $comment='';
										 
						
							   foreach($_POST['id_stud'] as $devoir_id)
							     { 
							     	
							     	if(isset($_POST['devoirs'][$devoir_id]))
										$grade=$_POST['devoirs'][$devoir_id];
										
									if(isset($_POST['comments'][$devoir_id]))
								        $comment=$_POST['comments'][$devoir_id];
							     	
							     	$model=Devoirs::model()->findbyPk($devoir_id);	   
											   
							
												
                                                                                          
											   
											   if($grade<=$weight) 
											      {   
										     	     
										     	    
										     	    $model->setAttribute('date_updated',date('Y-m-d'));
											        $model->setAttribute('grade_value',$grade);	
											         $model->setAttribute('comment',$comment);	
											         $model->setAttribute('update_by',currentUser());								     	   
											        
											       }
											     else
												   $this->message_GradeHigherWeight=true;

                                                                                           
											   if($model->save())
												 {  
												   $model->unSetAttributes();
												   $model= new Devoirs;
												   $this->success_=1;
												   $ok=true;
												  
												 }
		                                     
									         
									}
							     //}
								
						if(($ok==true))	     
						  { $this->success_=1;
										      
							   $this->redirect (array('update?all=1&from=stud&mn=std&ok=yes&greater='.$this->message_GradeHigherWeight));
						  }
						
							
					}
				  	 
			if(isset($_POST['cancel']))
                          {
                             
                              $this->redirect(Yii::app()->request->urlReferrer);
                          }
				    
		  }
		

		$this->render('update',array(
			'model'=>$model,
		));
    
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
		public function actionDelete()
	{
	  $current_acad= 0;
		$current_year=currentAcad();
		if($current_year!=null)
			$current_acad= $current_year->id;
		
		
		$acad_sess = acad_sess();
        $acad=Yii::app()->session['currentId_academic_year']; 
if($acad != $current_acad)
{	
$this->redirect(Yii::app()->request->urlReferrer);
}
		
		try {
			    $_model = $this->loadModel();
			    $_model->delete();
			    
   			 
   			 // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
			  if(!isset($_GET['ajax']))
				$this->redirect( array('index'));
			
			} catch (CDbException $e) {
			    if($e->errorInfo[1] == 1451) {
			       
			        header($_SERVER["SERVER_PROTOCOL"]." 500 Relation Restriction");
			        echo Yii::t('app',"\n\n There are dependant elements, you have to delete them first.\n\n");
			    } else {
			        throw $e;
			    }
			}



	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$this->template_update_only = false;
	
	  $acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

   $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
     if($current_acad==null)
						          $condition = '';
						     else{
						     	   if($acad!=$current_acad->id)
							         $condition = '';
							      else
							         $condition = 'student0.active IN(1, 2) AND ';
						        }
         
       
		if (isset($_GET['pageSize'])) {
		    Yii::app()->user->setState('pageSize',(int)$_GET['pageSize']);
		    unset($_GET['pageSize']);
			}
		
		if((Yii::app()->user->profil!='Teacher'))	    
	      {  $dataProvider=Devoirs::model()->search($condition,$acad_sess);
	      	 $title = Yii::t('app','List of student devoirs: ');
	      	
	      	$model=new Devoirs('search');
			if(isset($_GET['Devoirs']))
				$model->attributes=$_GET['Devoirs'];
	              
	       }
	      else 
	       {  
	       	
	       	$id_teacher='';   
           	
           	 $pers=User::model()->getPersonByUserId(Yii::app()->user->userid);
				 $pers=$pers->getData();
				 foreach($pers as $p)
				      $id_teacher=$p->id;
				
			 $dataProvider=Devoirs::model()->search($condition,$acad_sess);
	      	 $title = Yii::t('app','List of student devoirs: ');
	      	
	      	$model=new Devoirs('search');
	      	if(isset($_GET['Devoirs']))
				$model->attributes=$_GET['Devoirs'];
	               
	         }
	         
	             // Here to export to CSV 
	                if($this->isExportRequest()){
	                $this->exportCSV(array($title), null,false);
	               
	                $this->exportCSV($dataProvider, array(
	                'student0.fullName',
	                'course0.courseName',
	                'academicPeriod.name_period',
	                'grade_value',
	                'course0.weight',
	                'comment')); 
	                }
	

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$model=new Devoirs('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Devoirs']))
			$model->attributes=$_GET['Devoirs'];

		$this->render('admin',array(
			'model'=>$model,
		));
	}

	

         //************************  getLevel($id) ******************************/
   public function getLevel($id)
	{
		$level = new Levels;
		$level=Levels::model()->findByPk($id);
        
			
		    if(isset($level))
				return $level->level_name;
		
	}
	
	//************************  getLevelByStudentId($id) ******************************/
	public function getLevelByStudentId($id)
	{
		$idRoom= $this->getRoomByStudentId($id);
		
		
		$model=new Rooms;
		$idShift = $model->find(array('select'=>'level',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$level = new Levels;
        $result=$level->find(array('select'=>'id,level_name',
                                     'condition'=>'id=:shiftID',
                                     'params'=>array(':shiftID'=>$idShift->level),
                               ));
		
				return $result;
		
	}
	
	//************************  getLevelIdFromPersons ******************************/
	public function getLevelIdFromPersons()
	{    
       
	   $modelLevel=new Levels;
					
			 if(isset($_POST['Devoirs']))
		        $modelLevel->attributes=$_POST['Levels'];
		           
				   $level_id=$modelLevel->level_name;
	               
				   return $level_id;
	}

        
        public function loadLevelByIdShiftSectionIdAcademicP($idShift,$section_id,$acad)
	{    
       	  $code= array();
          $code[null]= Yii::t('app','-- Select --');
	      $modelRoom= new Rooms();
	      $level_id=$modelRoom->findAll(array('alias'=>'r',
		                             'select'=>'r.level',
		                             'join'=>'left join room_has_person rh on(rh.room=r.id) left join levels l on(r.level=l.id)',
                                     'condition'=>'r.shift=:shiftID AND l.section=:sectionID AND rh.academic_year=:acad',
                                     'params'=>array(':shiftID'=>$idShift, ':sectionID'=>$section_id, ':acad'=>$acad),
									 'order'=>'l.level_name ASC',
                               ));
			
			if(isset($level_id))
			 {  
			    foreach($level_id as $i){			   
					  $modelLevel= new Levels();
					   
					  $level=$modelLevel->findAll(array('select'=>'id,level_name',
												 'condition'=>'id=:levelID',
												 'params'=>array(':levelID'=>$i->level),
												 'order'=>'level_name ASC',
										   ));
						
					 if(isset($level)){
						  foreach($level as $l)
						       $code[$l->id]= $l->level_name;
					    }  
							   }						 
		    
						  }	
			
		return $code;
         
	}

        
//xxxxxxxxxxxxxxx  ROOM xxxxxxxxxxxxxxxxxxx
	
	//************************  changeRoom ******************************/
	public function changeRoom()
	{    $modelLevel= new Levels();
           $code= array();
		   
		  if(isset($_POST['Devoirs']['Levels']))
		        $idLevel->attributes=$_POST['Levels'];
		           
				   //$idLevel=$modelLevel->level_name;
	               
				    //return $idLevel;
         
	}
	

//************************  loadRoomByIdTeacher($id_teacher,$acad) ******************************/
	public function loadRoomByIdTeacher($id_teacher,$acad)
	{    

	
		$modelRoom= new Rooms();
           $code= array();
		   
		  $modelPersonRoom=$modelRoom->findAll(array('alias'=>'r',
									 'select'=>'r.id,room_name',
                                     'join'=>'left join courses c on(c.room=r.id)',
                                     'condition'=>'c.academic_period IN(select ap.id from academicperiods ap where (ap.id='.$acad.' OR ap.year='.$acad.') ) AND c.teacher='.$id_teacher,
                                     'order'=>'r.room_name ASC',
                               ));
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonRoom))
			 {  
			    foreach($modelPersonRoom as $room){
			        $code[$room->id]= $room->room_name;
		           
		           }
			 }
		   
		return $code;
         
	}




	//************************  loadRoomByIdShiftSectionLevel ******************************/
	public function loadRoomByIdShiftSectionLevel($shift,$section,$idLevel,$acad)
	{    $modelRoom= new Rooms();
           $code= array();
		   
		  $modelPersonRoom=$modelRoom->findAll(array('alias'=>'r',
									 'select'=>'r.id,r.room_name',
                                     'join'=>'left join room_has_person rh on(rh.room= r.id) left join levels l on(l.id=r.level)',
									 'condition'=>'r.shift=:idShift AND l.section=:idSection AND r.level=:levelID AND rh.academic_year=:acad',
                                     'params'=>array(':idShift'=>$shift,':idSection'=>$section,':levelID'=>$idLevel, ':acad'=>$acad),
									 'order'=>'r.room_name ASC',
                               ));
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonRoom))
			 {  
			    foreach($modelPersonRoom as $room){
			        $code[$room->id]= $room->room_name;
		           
		           }
			 }
		   
		return $code;
         
	}
	
	//************************  loadRoom ******************************/
	public function loadRoom($acad)
	{   
            $section_name = infoGeneralConfig('kindergarden_section');
            $modelRoom= new Rooms();
           $code= array();
		     
		  $modelPersonRoom=$modelRoom->findAll(array('alias'=>'r',
									 'select'=>'*',
                                     'join'=>'left join room_has_person rh on(rh.room=r.id) INNER JOIN levels l ON (r.level = l.id) INNER JOIN sections s ON (l.section = s.id)',
									 'condition'=>'rh.academic_year=:acad AND s.section_name <> "'.$section_name.'"',
                                     'params'=>array(':acad'=>$acad,),
									 'order'=>'r.room_name',
                               ));
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonRoom))
			 {  foreach($modelPersonRoom as $room){
			        $code[$room->id]= $room->room_name;
		           
		           }
			 }
		   
		return $code;
         
	}
   //************************  getRoom($id) ******************************/
   public function getRoom($id)
	{
		$room = new Rooms;
		$room=Rooms::model()->findByPk($id);
        
			
		//echo $id." --- ".$room->room_name;
		    if(isset($room))
				return $room->room_name;
		
	}
	
	
	
	//************************  getRoomByStudentId($id) ******************************/
	public function getRoomByStudentId($id)
	{
		
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

		$model=new RoomHasPerson;
		$idRoom = $model->find(array('select'=>'room',
                                     'condition'=>'students=:studID AND academic_year=:acad',
                                     'params'=>array(':studID'=>$id,':acad'=>$acad_sess),
                               ));
		$room = new Rooms;
        $result=$room->find(array('select'=>'id,room_name',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->room),
                               ));
			
						   

						   
				return $result;
		
	}
	
	//************************  getRoomIdFromDevoirs ******************************/
	public function getRoomIdFromDevoirs()
	{    
       
	   $modelRoom=new Rooms;
					
			 //if(isset($_POST['Devoirs']))
		        $modelRoom->attributes=$_POST['Rooms'];
		           
				   $id=$modelRoom->room_name;
		
				   return $id;
	}
		 

	 //xxxxxxxxxxxxxxx  SHIFT xxxxxxxxxxxxxxxxxxx
	//************************  loadShift ******************************/
	public function loadShift()
	{    $modelShift= new Shifts();
           $code= array();
		   
		  $modelPersonShift=$modelShift->findAll();
            $code[null]= Yii::t('app','-- Select --');
		    foreach($modelPersonShift as $shift){
			    $code[$shift->id]= $shift->shift_name;
		           
		      }
		   
		return $code;
         
	}
   //************************  getShift($id) ******************************/
   public function getShift($id)
	{
		
		$shift=Shifts::model()->findByPk($id);
        
			
		      if(isset($shift))
				return $shift->shift_name;
		
	}
	
	//************************  getShiftByStudentId($id) ******************************/
	public function getShiftByStudentId($id)
	{
		
		$idRoom= $this->getRoomByStudentId($id);
		
		
		$model=new Rooms;
		$idShift = $model->find(array('select'=>'shift',
                                     'condition'=>'id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$shift = new Shifts;
        $result=$shift->find(array('select'=>'id,shift_name',
                                     'condition'=>'id=:shiftID',
                                     'params'=>array(':shiftID'=>$idShift->shift),
                               ));
		
				return $result;
		
	}
	
	//************************  getShiftIdFromPersons ******************************/
	public function getShiftIdFromPersons()
	{    
       
	   $modelShift=new Shifts;
					
			 if(isset($_POST['Grades']))
		        $modelShift->attributes=$_POST['Shifts'];
		           
				   $shift_id=$modelShift->shift_name;
	               
				   return $shift_id;
	}
	
	
	
	
	        //xxxxxxxxxxxxxxx  SECTION  xxxxxxxxxxxxxxxxxxx
	
	//************************  loadSectionByIdShift ******************************/
	public function loadSectionByIdShift($idShift)
	{   
             $section_name = infoGeneralConfig('kindergarden_section');
	      $code= array();
          $code[null]= Yii::t('app','-- Select --');
	      $modelRoom= new Rooms();
	      $section_id=$modelRoom->findAll(array('alias'=>'r',
	                                 'join'=>'left join levels l on(l.id=r.level)',
	                                 'select'=>'l.section',
                                     'condition'=>'r.shift=:shiftID',
                                     'params'=>array(':shiftID'=>$idShift),
                               ));
			if(isset($section_id))
			 {  
			    foreach($section_id as $i){			   
					  $modelSection= new Sections();
					   
					  $section=$modelSection->findAll(array('select'=>'id,section_name',
												 'condition'=>'id=:sectionID AND section_name <> "'.$section_name.'"',
												 'params'=>array(':sectionID'=>$i->section),
										   ));
						
						 if(isset($section)){
						     foreach($section as $s)
							    $code[$s->id]= $s->section_name;
						   }	   
							   }						 }
		   
		return $code;
		$this->section_id=null;
         
	}
	
	//************************  loadSection ******************************/
	public function loadSection()
	{    $modelSection= new Sections();
           $code= array();
		   
		  $modelPersonSection=$modelSection->findAll();
            $code[null]= Yii::t('app','-- Select --');
		    if(isset($modelPersonSection))
			 {  foreach($modelPersonSection as $section){
			        $code[$section->id]= $section->section_name;
		           
		           }
			 }
		   
		return $code;
         
	}
   //************************  getSection($id) ******************************/
   public function getSection($id)
	{
		
		$section=Sections::model()->findByPk($id);
        
			
		       if(isset($section))
				return $section->section_name;
		
	}
	
	//************************  getSectionByStudentId($id) ******************************/
	public function getSectionByStudentId($id)
	{
		$idRoom= $this->getRoomByStudentId($id);
		
		
		$model=new Rooms;
		$idShift = $model->find(array('alias'=>'r',
		                             'join'=>'left join levels l on(l.id=r.level)',
		                             'select'=>'l.section',
                                     'condition'=>'r.id=:roomID',
                                     'params'=>array(':roomID'=>$idRoom->id),
                               ));
		$shift = new Shifts;
        $result=$shift->find(array('select'=>'id,shift_name',
                                     'condition'=>'id=:shiftID',
                                     'params'=>array(':shiftID'=>$idShift->section),
                               ));
		
				return $result;
		
	}
	
		
	
	//************************  getSectionIdFromPersons ******************************/
	public function getSectionIdFromPersons()
	{    
       
	   $modelSection=new Sections;
					
			 if(isset($_POST['Grades']))
		        $modelSection->attributes=$_POST['Sections'];
		           
				   $this->section_id=$modelSection->section_name;
	               
				   return $this->section_id;
	}
	
	
	
	//xxxxxxxxxxxxxxx  SUBJECTS xxxxxxxxxxxxxxxxxxx
		//************************  loadSubject by room_id and level_id  ******************************/
	public function loadSubject($room_id,$level_id,$acad)
	{    
       	  $code= array();
          $code[null]= Yii::t('app','-- Select --');
		  
		  $modelCourse= new Courses();
	       $result=$modelCourse->searchCourseByRoom($room_id,$level_id,$acad);
			
			 if(isset($result))
			  {  $Course=$result->getData();//return a list of Course objects
			    foreach($Course as $i){			   
					
                                $code[$i->id] = $i->subject_name.' ['.$i->teacher_name.'] ';//.$i->name_period;
				}  
			 }	 					 
		      
			
		return $code;
         
	}
	


	//************************  loadSubjectByTeacherRoom($room_id,$id_teacher,$acad)  ******************************/
	public function loadSubjectByTeacherRoom($room_id,$id_teacher,$acad)
	{    
       	  $code= array();
          $code[null]= Yii::t('app','-- Select --');
		  
		  $modelCourse= new Courses();
	       $result=$modelCourse->searchCourseByTeacherRoom($room_id,$id_teacher,$acad);
			
			 if(isset($result))
			  {  $Course=$result->getData();//return a list of Course objects
			    foreach($Course as $i){			   
					
                                $code[$i->id] = $i->subject_name.' ['.$i->teacher_name.'] ';
				}  
			 }	 					 
		      
			
		return $code;
         
	}
	


public function getAllSubjects($room_id,$level_id)
	{    
       	  
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
//acad_sess tou


          $code= array();
          $code[null][null]= Yii::t('app','-- Select --');
		  
		 $modelCourse= new Courses();
	       $result=$modelCourse->searchCourseByRoom($room_id,$level_id,$acad_sess);
			
			 if(isset($result))
			  {  $Course=$result->getData();//return a list of Course objects
			            //$k is a counter
						$k=0;
			    foreach($Course as $i){			   
					  
					  $code[$k][0]= $i->id;
					  $code[$k][1]= $i->subject_name;
					  $code[$k][2]= $i->weight;
					  $code[$k][3]= $i->short_subject_name;
					   $code[$k][4]= $i->reference_id;
					  $k=$k+1;
					  
				}  
			 }	 					 
		      
			
		return $code;
         
	}

        
        	//************************  loadSubject by room_id  ******************************/
		public function loadSubjectByRoom($room_id,$acad)
	{    
       	  $code= array();
          $code[null]= Yii::t('app','-- Select --');
		
		  
		  
	      $modelCourse= new Courses();
	       $result=$modelCourse->searchByRoomId($room_id,$acad); 
			
			 if(isset($result))
			  {  $Course=$result->getData();//return a list of Course objects
			    foreach($Course as $i){			   
					
                                $code[$i->id] = $i->subject_name.' ['.$i->teacher_name.'] ';
				}  
			 }	 					 
		      
			
		return $code;
         
	}
        
        


	//************************  loadEvaluation  ******************************/
	public function loadPeriod($acad)
	{    
       	  $code= array();
          $code[null]= Yii::t('app','-- Select --');
		  
		  $modelEvaluation= new AcademicPeriods();
	       $result=$modelEvaluation->getAllPeriodInAcademicYear($acad);
			
		   
			 if(isset($result))
			  {  $r=$result->getData();//return a list of  objects
			    foreach($r as $eYear){	
			    	$time1 = strtotime($eYear->date_start);
                         $month1=getShortMonth( date("m",$time1) );
                         $year1=date("Y",$time1);
                         $day1=date("j",$time1);
                         
                         $time2 = strtotime($eYear->date_end);
                         $month2=getShortMonth( date("m",$time2) );
                         $year2=date("Y",$time2);
                         $day2=date("j",$time2);
                         
                        $date = $day1.' '.$month1.' '.$year1.' - '.$day2.' '.$month2.' '.$year2; 
                        
					  $code[$eYear->id]= $eYear->name_period.' ('.$date.')';
				}  
			 }	 					 
		      
			
		return $code;
         
	}
	
	

	public function getAcademicPeriodName($acad,$room_id)
	  {    
	        $result=ReportCard::getAcademicPeriodName($acad,$room_id);
                if($result!=null)
                    return $result;//->name_period;
                    else
                        return null;
	  }

	
    /**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Devoirs the loaded model
	 * @throws CHttpException
	 */
	
	public function loadModel()
	{
		if($this->_model===null)
		{
			if(isset($_GET['id']))
				$this->_model=Devoirs::model()->findbyPk($_GET['id']);
			if($this->_model===null)
				throw new CHttpException(404, Yii::t('app', 'The requested page does not exist.'));
		}
		return $this->_model;
	}

            // Export to CSV 
        public function behaviors() {
           return array(
               'exportableGrid' => array(
                   'class' => 'application.components.ExportableGridBehavior',
                   'filename' => Yii::t('app','devoirs.csv'),
                   'csvDelimiter' => ',',
                   ));
        }
     
	/**
	 * Performs the AJAX validation.
	 * @param Devoirs $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='devoirs-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
