<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */    



//
Yii::import('ext.tcpdf.*');
Yii::import('ext.tcpdf.SIGESPDF');






class PalmaresController extends Controller {
    
    public $layout='//layouts/column2';
    
    
    
    
    public function actionIndex(){
        $model = new Persons(); 
        return $this->render("index",array('model'=>$model)); 
    }
    
    public function actionUpload(){
        return json_encode("TESY");
    }
    
    public function actionPalmares(){
        $room = NULL; 
        $eval = NULL; 
        $shift = NULL; 
        if(isset($_GET['room'])){
            $room = $_GET['room']; 
        }
        if(isset($_GET['eval'])){
            $eval = $_GET['eval']; 
        }
        if(isset($_GET['shift'])){
            $shift = $_GET['shift']; 
        }
        
        $acad_sess = acad_sess();
        $acad = Yii::app()->session['currentId_academic_year']; 
        $evaluation = $this->loadEvaluation($acad_sess);
        $to_remove = Yii::t('app','-- Select --');
        if (($key = array_search($to_remove, $evaluation)) !== false) {
            unset($evaluation[$key]);
        }
        return $this->render('palmares',
                array(
                    'room'=>$room,
                    'eval'=>$eval,
                    'shift'=>$shift,
                    'evaluation'=>$evaluation[$eval],
                    )
                ); 
    }
    
    public function getPassingGrade($id_level, $id_academic_period)
	{
		$criteria = new CDbCriteria;
		$criteria->condition='level_or_course=0 AND level=:idLevel AND academic_period=:idAcademicLevel';
		$criteria->params=array(':idLevel'=>$id_level,':idAcademicLevel'=>$id_academic_period);
		$pass_grade = PassingGrades::model()->find($criteria);
	 
	  if(isset($pass_grade))
	  return $pass_grade->minimum_passing; 
	  else 
	    return null;
	}
        
   public function actionUpdategrade($grade, $gradeid,$subject, $coefficient, $validate){
    $model_grade = Grades::model()->findByPk($gradeid);
    $model_grade->grade_value = $grade; 
    $model_grade->validate = 1; 
    $model_grade->date_updated = date('Y-m-d H:i:s'); 
    $model_grade->update_by = currentUser(); 
    $model_grade->save(); 
     echo '<span  id="'.$gradeid.'" class="modifye" data-gradeid="'.$gradeid.'" data-grade="'.$grade.'" data-subject="'.$subject.'" data-coefficient="'.$coefficient.'" data-validate="'.$validate.'" >'
             . '<span class="modifye" style="background-color: yellow; color: black;"><b>'.$grade.'</b></span>'
             . '</span>'; 
   }
   
   public function actionValidation($eval, $room){
       //$model_grade = new Grades();
       //jwenn tout kou ki nan room sa ki nan grades tou
        $acad_sess = acad_sess();
         //$acad=Yii::app()->session['currentId_academic_year'];
       $dataCourse = NULL;
        
       $dataProvider=  Grades::model()->getCourseInGradeByRoom($room,$acad_sess);
       if($dataProvider!=null)
            $dataCourse =  $dataProvider->getData();
       
       //valide chak kou sa yo epi tou fe mwayen klas la pou kou sila
       
       //$grades = Grades::model()->findAllBySql("SELECT g.id, g.validate FROM grades g INNER JOIN courses c ON(c.id=g.course) WHERE c.room=$room AND evaluation= $eval ORDER BY id DESC");  
      
      foreach($dataCourse as $c)
       { 
          $grades_average =0;
          $number_of_grades=0;
            $grades = Grades::model()->findAllBySql("SELECT g.id, g.grade_value, g.validate FROM grades g WHERE course=$c->course AND evaluation= $eval ORDER BY id DESC");  
            foreach($grades as $g)
             {
                
                $grades_average += $g->grade_value;
		$number_of_grades++;
                
                $model = Grades::model()->findByPk($g->id);
                $model->validate = 1;
                $model->publish = 1;
                $model->save(); 
            }
 
              if(($grades_average!=0))
                {		
                   $grades_average = round(($grades_average/$number_of_grades),2);

                   //save subject average for this period			  
                                  $command = Yii::app()->db->createCommand();
                                   //check if already exit
                                          $data =  Grades::model()->checkDataSubjectAverage($acad_sess,$eval,$c->course);
                                          $is_present=false;
                                               if($data==true)
                                                     $is_present=true;

                if($is_present==true)
                    {// yes, update

                    $command->update('subject_average', array(
                                        'average'=>$grades_average,'date_updated'=>date('Y-m-d'),'update_by'=>currentUser(),
                                ), 'academic_year=:year AND evaluation_by_year=:period AND course=:course', array(':year'=>$acad_sess, ':period'=>$eval, ':course'=>$c->course));
                   }
                 else{// no, insert
                   $command->insert('subject_average', array(
                                'academic_year'=>$acad_sess,
                                'evaluation_by_year'=>$eval,
                                'course'=>$c->course,
                                'average'=>$grades_average,
                                'create_by'=>currentUser(),
                                'date_created'=>date('Y-m-d'),
                        ));


                     }

                 } 
                                           
                                           
       
       }
       $this->renderPartial('confirm_validation',array('eval'=>$eval,'room'=>$room));
   }
   
   //************************  loadEvaluation  ******************************/
	public function loadEvaluation($acad)
	{    
       	  $code= array();
          $code[null]= Yii::t('app','-- Select --');
		  
		  $modelEvaluation= new EvaluationByYear();
	       $result=$modelEvaluation->searchIdName($acad);
			
		   
			 if(isset($result))
			  {  $r=$result->getData();//return a list of  objects
			    foreach($r as $i){	
			    	$time = strtotime($i->evaluation_date);
                        $month=date("m",$time);
                         $year=date("Y",$time);
                         $day=date("j",$time);
                         
                        $date = $day.'/'.$month.'/'.$year;  		   
					  $code[$i->id]= $i->name_period; //.' / '.$i->evaluation_name.' ('.$date.')';
				}  
			 }	 					 
		      
			
		return $code;
         
	}
 
        
         //************************  isSubjectEvaluated($subject_id,$room,$period_id)  ******************************/	
	public function isSubjectEvaluated($course_id,$room,$period_id)
	{    
       	  
                $bool=false;
		  
		 $modelCourse= new Courses();
	       $result=$modelCourse->evaluatedSubject($course_id,$room,$period_id);
			
			 if(isset($result)&&($result!=null))
			  {  $Course=$result->getData();//return a list of Course objects
			            //$k is a counter
						$k=0;
						
			    foreach($Course as $i)
			       {		
					  if($i->id!=null)
					       $bool=true;
					 }
			     
			      	 
			       	 
			 }
			
			  	 					 
		    
			
		return $bool;
         
	}
	

        
public function getAllSubjects($room_id,$level_id)
	{    
       	  
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 
//acad_sess tou


          $code= array();
          $code[null][null]= Yii::t('app','-- Select --');
		  
		 $modelCourse= new Courses();
	       $result=$modelCourse->searchCourseByRoom($room_id,$level_id,$acad_sess);
			
			 if(isset($result))
			  {  $Course=$result->getData();//return a list of Course objects
			            //$k is a counter
						$k=0;
			    foreach($Course as $i){			   
					  
					  $code[$k][0]= $i->id;
					  $code[$k][1]= $i->subject_name;
					  $code[$k][2]= $i->weight;
					  $code[$k][3]= $i->short_subject_name;
					   $code[$k][4]= $i->reference_id;
					  $k=$k+1;
					  
				}  
			 }	 					 
		      
			
		return $code;
         
	}
	
	
        
 public function isOldSubjectEvaluated($course_id,$room,$period_id)
	{    
       	  
                $bool=false;
		  
		 $modelCourse= new Courses();
	       $result=$modelCourse->evaluatedOldSubject($course_id,$room,$period_id);
			
			 if(isset($result))
			  {  $Course=$result->getData();//return a list of Course objects
			            //$k is a counter
						$k=0;
						 
			    foreach($Course as $i){			   
					  
					  
					  if($i->id!=null)
					       $bool=true;
					  					  
				}  
			 }	 					 
		      
			
		return $bool;
         
	}
		
        
        
	public function getAverageForAStudent($student, $room, $evaluation, $acad)
    {               
	      $include_discipline=0;
          $max_grade_discipline=0;
     
			//Extract max grade of discipline
			$max_grade_discipline = infoGeneralConfig('note_discipline_initiale');
			//Extract 
			$include_discipline = infoGeneralConfig('include_discipline_grade');
								   				
	      
	      
	      
	      $dataProvider_Course = new Courses;
	        $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
         
               
          $average_base = infoGeneralConfig('average_base');//$average_base = getAverageBase($room,$acad);
   
       if($current_acad==null)
						        {$condition = '';
						           $condition1 = '';
						         }
						     else{
						     	   if($acad!=$current_acad->id)
							          {$condition = '';
							           $condition1 = '';
							         }
							      else
							         { $condition = ' p.active IN(1,2) AND ';
								          $condition1 = '';
								        }
						        }


       $average=0;
		
	   $level_has_person= new LevelHasPerson;
	   $result=$level_has_person->find(array('alias'=>'lhp',
	                                 'select'=>'lhp.level',
                                     'join'=>'left join rooms r on(r.level=lhp.level) ',
									 'condition'=>'r.id=:room AND lhp.academic_year=:acad',
                                     'params'=>array(':room'=>$room,':acad'=>$acad),
                               ));
		$level=null;					   
		if(isset($result))	
           {  
   		     $level=$result->level;
			 
			 }
		 
		$dataProvider_Course=Courses::model()->searchCourseByRoomId($condition1,$room, $acad);
										   
			 $k=0;
			$tot_grade=0;
                                                   
			$max_grade=0;

											           
		  if(isset($dataProvider_Course))
		   { $r=$dataProvider_Course->getData();//return a list of  objects
			foreach($r as $course) 
			 {	
			 	$careAbout= false;
			 		
			 	      if($course->reference_id!=NULL) 
			 	         {  $id_course = $course->reference_id;
							 //si kou a evalye pou peryod sa
							$old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$room,$evaluation);         
							  if($old_subject_evaluated)
								 { $grades=Grades::model()->searchForReportCard($condition,$student,$id_course,$evaluation);
									$careAbout=$old_subject_evaluated; 						                        
								  }
							   else
								 {  $id_course = $course->id;
								 	$grades=Grades::model()->searchForReportCard($condition,$student,$course->id,$evaluation);
										$careAbout=$this->isSubjectEvaluated($course->id,$room,$evaluation); 					                       															                       
									}
		
						 }
					 else
						{  $id_course = $course->id;
							$grades=Grades::model()->searchForReportCard($condition,$student,$course->id,$evaluation);
						   $careAbout=$this->isSubjectEvaluated($course->id,$room,$evaluation);
						}
																	    
			      
 
					//	$grades=Grades::model()->searchForReportCard($condition,$student,$course->id, $evaluation);
						 if(($careAbout))
						  {															  
							if(isset($grades)&&($grades!=null))
							 {
							   $r=$grades->getData();//return a list of  objects
							   foreach($r as $grade) 
								 {									       
									$tot_grade=$tot_grade+$grade->grade_value;
									
																																			 
																			   
								 }
																																		   
							  }
							$max_grade=$max_grade+$course->weight;
						  }
			    }
			    
			 }
       
      //check to include discipline grade
		
		if($include_discipline==1)
		  { 
		  	// To find period name in in evaluation by year 
		        $period_acad_id =null;
				//$result=EvaluationByYear::model()->searchPeriodName($evaluation);
				$period_acad_id = ReportCard::searchPeriodNameForReportCard($evaluation)->id;
				
				  																 
				$grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($student, $period_acad_id);
				
				$tot_grade=$tot_grade+$grade_discipline;
				 $max_grade=$max_grade+$max_grade_discipline;
				
		   }
			 
                                                                                                                 
             
		 if(($average_base==10)||($average_base==100)) 
			{ if($max_grade!=0)  
				$average=round(($tot_grade/$max_grade)*$average_base,2);
		      }
		  else			
			 $average =null;					
								

	    return $average;
	}
        
        public function actionPalmarespdf($room,$eval,$shift){
            
            $acad_sess = acad_sess();
            $acad = Yii::app()->session['currentId_academic_year']; 
            $evaluation = $this->loadEvaluation($acad_sess);
            $to_remove = Yii::t('app','-- Select --');
        if (($key = array_search($to_remove, $evaluation)) !== false) {
            unset($evaluation[$key]);
        }
            $this->renderPartial('pdf_palmares',array(
                'room'=>$room,
                'eval'=>$eval,
                'shift'=>$shift,
                'evaluation'=>$evaluation[$eval]
            ));
        }
	
  
        
        
        
        
        
   
}