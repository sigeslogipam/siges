<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>
<?php
$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
	
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

 $tit=Yii::t('app','Manage Postulants');

$template = '';

$display_communication = infoGeneralConfig('display_communication');
$acad_name=Yii::app()->session['currentName_academic_year'];

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 




?>



<div id="dash">
   <div class="span3"><h2>
         <?php echo $tit; ?>
         
   </h2> </div>
   
   
     <div class="span3">
   
 <?php 
     
      $template = ''; 
      
       if(!isAchiveMode($acad_sess))
         { 
           if($acad == $current_acad_id)
		      {
               $template = '{update}{delete}';  
        
   ?>
   
         <div class="span4">
                  <?php

                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('postulant/create'));
               ?>
        </div>
 
         <?php 
         if($display_communication==1)
            {
         ?>
         <div class="span4">
                  <?php

                $images = '<i class="fa fa-upload">&nbsp;'.Yii::t('app','Publish result on website').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('postulant/publish'));
               ?>
        </div>
         
           
  <?php
            }
         }
         
         }
      
      ?>       
     
      
        <div class="span4">
              <?php

                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/academic/postulant/viewListAdmission/part/enrlis/from/stud')); 
                 
                  
               ?>
         </div>
   </div>

</div>

<div style="clear:both"></div>


<br/>


    <?php
    echo $this->renderPartial('//layouts/navBaseEnrollment',NULL,true);	
    ?>


<div class="b_m">



<div class="grid-view">

<div class="search-form" >

<?php 

$gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'postulant-grid',
	'summaryText'=>'',
	'dataProvider'=>$model->search($acad_sess),
	//'filter'=>$model,
	'showTableOnEmpty'=>true,
	
        //'mergeColumns'=>'level_lname',
        
	'columns'=>array(
		
		'id',
		
		array(
                    'name' => 'first_name',
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->first_name,Yii::app()->createUrl("/academic/postulant/viewAdmissionDetail",array("id"=>$data->id,"part"=>"enrlis","pg"=>"")))',
                    'htmlOptions'=>array('width'=>'150px'),
                     ),
		
		array(
                    'name' => 'last_name',
                    'type' => 'raw',
                    'value'=>'CHtml::link($data->last_name,Yii::app()->createUrl("/academic/postulant/viewAdmissionDetail",array("id"=>$data->id,"part"=>"enrlis","pg"=>"")))',
                    'htmlOptions'=>array('width'=>'150px'),
                     ),
                     
		array(
            'name'=>'gender',
             'value'=>'$data->getGenders1()',
             'htmlOptions'=>array('width'=>'50px'),
						),
        
         array('name'=>'apply_for_level',
			'header'=>Yii::t('app','Level'), 
			'value'=>'$data->applyForLevel->level_name'),
            array('name'=>'date_created',
                'header'=>Yii::t('app','Subscription date'),
                'value'=>'$data->date_created'
                ),
           // 'date_created',

		/*
		array('name'=>'paid',
			'header'=>Yii::t('app','Paid'), 
			'value'=>'$data->Paid'),
		*/
		array('name'=>'status',
			'header'=>Yii::t('app','Status'), 
			'value'=>'$data->Status'),
                array('name'=>'validate',
			'header'=>Yii::t('app','Is Validate'), 
			'value'=>'$data->validate'),
                array('name'=>'online',
                'header'=>Yii::t('app','Is Online'), 
                'value'=>'$data->online'),
                
		
				array(
			'class'=>'CButtonColumn',
                        'template'=>$template,
                        'buttons'=>array('update'=>array('label'=>'<span class="fa fa-pencil-square-o"></span>',
                            'imageUrl'=>false,
                            'url'=>'Yii::app()->createUrl("/academic/postulant/update?id=$data->id&part=enrlis&from=")',
                            'options'=>array('title'=>Yii::t('app','Update')),
                             
                            ),
                            'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),
                            ),
                        
		),
	),
)); 



     
?>

</div>

            
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit ?>"},
                  
                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
            //var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>
		      
</div>



	
	  
