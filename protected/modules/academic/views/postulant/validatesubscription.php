<?php
$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
	
$acad_sess = acad_sess();
$acad = Yii::app()->session['currentId_academic_year'];  

$template = '';


$acad_name=Yii::app()->session['currentName_academic_year'];

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',)); 

$all_non_validate_postulant = Postulant::model()->findAllBySql("SELECT * FROM postulant WHERE academic_year = $acad AND status = 0 AND is_online = 1 AND is_validate = 0");
$baseUrl = Yii::app()->baseUrl;
$cs = Yii::app()->getClientScript();
$cs->registerScriptFile($baseUrl.'/js/jquery-ui.min.js');

if (! isset($_SESSION['csrf_token'])) {
        $_SESSION['csrf_token'] = base64_encode(openssl_random_pseudo_bytes(32));
    }
?>
<div id="dash">
   <div class="span3"><h2>
         <?php echo Yii::t('app','Manage Postulants'); ?>
         
   </h2> </div>
   
   
     <div class="span3">
   
   <?php
   
      if(!isAchiveMode($acad_sess))
         { 
          if($acad == $current_acad_id)
	    {
              $template = '{update}{delete}';  
   ?>

         <div class="span4">
                  <?php

                $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('postulant/create'));
               ?>
        </div>
         
         <div class="span4">
                  <?php

                $images = '<i class="fa fa-upload">&nbsp;'.Yii::t('app','Publish result on website').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('postulant/publish'));
               ?>
        </div>
         
           
  <?php
         }
         
         }
      
      ?>       
     
      
        <div class="span4">
              <?php

                  $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('/academic/postulant/viewListAdmission/part/enrlis/pg/')); 
                 
                  
               ?>
         </div>
   </div>

</div>


<div style="clear:both"></div>

<?php
    echo $this->renderPartial('//layouts/navBaseEnrollment',NULL,true);	
?>
<form action="" method="POST">
    <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>" />
<table class="table responsive table-striped table-hover non-validate">
    <thead>
        <th><?= Yii::t('app','ID')?></th>
        <th><?= Yii::t('app','First name'); ?></th>
        <th><?= Yii::t('app','Last name'); ?></th>
        <th><?= Yii::t('app','Gender'); ?></th>
        <th><?= Yii::t('app','Level'); ?></th>
        <th><?= Yii::t('app','Subscription date'); ?></th>
        <th><?= Yii::t('app','Status'); ?></th>
        <th><?= Yii::t('app','Is Online'); ?></th>
        <th><?= Yii::t('app','Is Validate'); ?></th>
    </thead>
    <tbody>
        <?php 
        foreach($all_non_validate_postulant as $ap){
            ?>
        <tr>
            <td><a href="<?= Yii::app()->baseUrl?>/index.php/academic/postulant/viewAdmissionDetail/id/<?= $ap->id;?>/part/enrlis/pg"><?= $ap->id; ?></a></td>
            <td><a href="<?= Yii::app()->baseUrl?>/index.php/academic/postulant/viewAdmissionDetail/id/<?= $ap->id;?>/part/enrlis/pg"><?= $ap->first_name; ?></a></td>
            <td><a href="<?= Yii::app()->baseUrl?>/index.php/academic/postulant/viewAdmissionDetail/id/<?= $ap->id;?>/part/enrlis/pg"><?= $ap->last_name; ?></a></td>
            <td><?= $ap->sexe; ?></td>
            <td><?= $ap->classe; ?></td>
            <td><?php echo getDay($ap->date_created).'/'. getMonth($ap->date_created).'/'. getYear($ap->date_created) ; ?></td>
            <td><?= $ap->statut; ?></td>
            <td><?= $ap->online; ?></td>
            <td class="modifye" data-type="select" data-name="create-validate" data-pk="<?= $ap->id; ?>" data-source="<?= $baseUrl ?>/index.php/academic/Postulant/listvalidate" data-url="<?= $baseUrl ?>/index.php/academic/Postulant/updatevalidate"><?= $ap->validate; ?></td>
        </tr>
        <?php 
        }
        ?>
    </tbody>
</table>
</form>

<script>
    window.csrf = { csrf_token: '<?php echo $_SESSION['csrf_token']; ?>' };
    $.ajaxSetup({
        data: window.csrf
    });
    $(document).ready(function(){
        
        $('.modifye').editable({
          emptytext:'<i class="fa fa-plus"></i>',
          mode : 'inline',
          
         });
    });
    
    $(document).ready(function(){
      
            $('.non-validate').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= Yii::t('app','non_validate_postulant') ?>"},
                  
                 /*   {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
            //var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>