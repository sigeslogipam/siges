<div class="alert alert-success alert-dismissible">
    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
    <?php 
       echo Yii::t('app','Grades validation successfully complete !');         
    ?>
</div>