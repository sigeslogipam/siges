<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 $current_acad_id= 0;
$current_year = currentAcad();
  if($current_year!=null)
	$current_acad_id = $current_year->id;
   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
$academic_period_name = AcademicPeriods::model()->findBYPk($acad)->name_period;
$room_name = Rooms::model()->findByPk($room)->short_room_name;
$shift_name = Shifts::model()->findByPk(Rooms::model()->findByPk($room)->shift)->shift_name;
$level = Rooms::model()->findByPk($room)->level;
$current_acad = AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
$section_name = Sections::model()->findByPk(Levels::model()->findByPk($level)->section)->section_name;

if($current_acad==null) { 
    $condition = '';
    $condition1 = '';
    }
     else{
           if($acad!=$current_acad->id)
                 { 
               $condition = '';
                $condition1 = '';
                        }
              else
                { 
                  $condition = 'p.active IN(1,2) AND ';
                  $condition1 = 'student0.active IN(1,2) AND ';
                }
 }
$id_teacher='';   
$pers = User::model()->getPersonByUserId(Yii::app()->user->userid);
$pers = $pers->getData();
foreach($pers as $p){
    $id_teacher = $p->id;  
}
?>

 <!--Menu of CRUD  -->

<div id="dash">
    <div class="span3">
        <h2>
        <?php  if((Yii::app()->user->profil!='Teacher'))
                echo Yii::t("app", "List student's grades {room} for {evaluation}",array('{room}'=>$room_name,'{evaluation}'=>$evaluation));
        else
           echo Yii::t("app", "Grades By Rooms");
         ?>
        </h2> 
    </div>
		 
		 
    <div class="span3">
    <?php 
     if(!isAchiveMode($acad_sess))
        { 
           if($acad == $current_acad_id)
		   {
        	if( (Yii::app()->user->profil=='Admin')||(Yii::app()->user->profil=='Manager') )
             {
    ?>
        <div class="span4">

       <?php
            $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
               // build the link in Yii standard
            echo CHtml::link($images,array('/academic/grades/create?from=stud&mn=std')); 

        ?>

       </div>
	<?php 
                }
                
	        if(Yii::app()->user->profil=='Teacher'){
	?>
				     
        <div class="span4">
         <?php
             $images = '<i class="fa fa-edit">&nbsp;'.Yii::t('app','Update').'</i>';
                 // build the link in Yii standard
             echo CHtml::link($images,array('grades/update?all=1&from=stud&mn=std')); 
        ?>
        </div>
				
          <?php
                          }

                ?>        
 <?php
           }
		}
       ?>       
      <div class="span4">
             <?php
                 $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                     // build the link in Yii standard
                echo CHtml::link($images,array('/academic/palmares/index?from=stud&mn=std')); 
            ?>
        </div>   
</div>    

 </div>
<div style="clear:both"></div>

 
 <?php 

$average_base = 0;

$person = new Persons();

$tot_stud=0;
$showButton=false;
$total_succ =0;
$total_fail=0;
$passing_grade='N/A';
$classAverage='N/A';
$gen_done=false;  

$include_discipline = 0;
//Extract 
$include_discipline = infoGeneralConfig("include_discipline_grade");
//Extract max grade of discipline
$max_grade_discipline = infoGeneralConfig('note_discipline_initiale');

$include_discipline_comment=Yii::t('app',"Behavior grade is included in this average.");

//tcheke si gen not pou sal sa deja
$sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) WHERE c.room='.$room.' AND evaluation='.$eval.' ORDER BY id DESC';
															
$command__ = Yii::app()->db->createCommand($sql__);
$result__ = $command__->queryAll(); 

if($result__!=null){ 
    $gen_done = true;
    }
    if($gen_done){
       
        
        $dataProvider_studentEnrolled = Rooms::model()->getStudentsEnrolled($condition,$room, $acad);
        
         
        
        //moyenne de la classe
       // $classAverage = $this->getClassAverage($room, $acad_sess);
            //total reussi, et echoue
            //moyenne de passage classe
        $passing_grade = $this->getPassingGrade($level,$acad_sess); //note de passage pour la classe
         
		$average_base = getAverageBase($room,$acad_sess);
                
        $dataProvider_studentEnrolledInfo= Rooms::model()->getInfoStudentsEnrolled($condition,$room, $acad_sess); 
        
        if(isset($dataProvider_studentEnrolledInfo))
        {  $t=$dataProvider_studentEnrolledInfo->getData();
           foreach($t as $stud)
              {
              // echo $stud->student_id;
                  //moyenne yon elev
               $averag = $this->getAverageForAStudent($stud->student_id, $room, $eval, $acad_sess);
               $stud_order_desc_average[$stud->student_id]= $averag;
              }
        }
        
        //$min_average et $max_average_ 
       // arsort($stud_order_desc_average);
         // get the first item in the $stud_order_desc_average
        // $max_average = reset($stud_order_desc_average);

         // get the last item in the $stud_order_desc_average
        // $min_average = end($stud_order_desc_average);
         
	$dataProvider = $this->getAllSubjects($room,$level);
	//Grades for the current period
	//liste des etudiants
           
        $dataProvider_student = Persons::model()->getStudentsByRoomForGrades($condition,$room, $acad_sess);
			 
     

?>
 <div class="row-fluid">
     <div id="validation_message"></div>
 </div>
 
 
 
<?php 

    }
    $couleur=1;
    $k=0;

?>

 <div class="grid-view" id="table-container" style="margin-top:0px;  margin-bottom: 20px; float:left;   width:100%;  overflow-x:scroll; overflow-y:hidden;  background-color:#EFF4F8;">
     <!-- table-striped table-bordered table-hover table-condensed items
        table table-striped table-bordered table-condensed table-hover table-responsive dataTables-exemple
     
     -->
     <table class="table-striped table-bordered table-hover table-condensed items dataTables-exemple">
         <thead>
             <tr>
                 <th><?= Yii::t('app','#')?></th>
                 <th><?= Yii::t('app',"First Name"); ?></th>
                 <th><?= Yii::t('app',"Last Name"); ?></th>
                 <th><?= Yii::t('app',"Average"); ?></th>
                 
                 <?php 
                 if($include_discipline==1)
                        {    												
                        echo ' <th style="background-color:#E4E9EF; text-align:center; border-left: 1px solid  #E4E9EF;font-size: 1em;">'.Yii::t('app','Discipline').' ('.$max_grade_discipline.')</th>';  		
		
                    }
                    
                    //liste des cours
		while(isset($dataProvider[$k][0]))
				{
	             $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
	             if($dataProvider19->getData()==null)
				  {  
                        if($dataProvider[$k][4]!=NULL) //reference_id
			 	     {  
                            $id_course = $dataProvider[$k][4];
			     //gade si kou sa gen referans
                        $reference_id=0; 
                        $useCourseReference=false;
                        $modelCourse = Courses::model()->findByPk($id_course);

                        $reference_id = $modelCourse->reference_id;

                        if($reference_id==null)//sil pa genyen
                                {
                                 //si kou a evalye pou peryod sa
                                $old_subject_evaluated= $this->isOldSubjectEvaluated($id_course,$room,$eval);         
                                  if($old_subject_evaluated)
                                         {
                                                $id_course = $dataProvider[$k][4];
                                                $careAbout= $this->isOldSubjectEvaluated($dataProvider[$k][4],$room,$eval);						                        
                                          }
                                   else
                                         {   $id_course = $dataProvider[$k][0];
                                                $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);						                       															                       
                                                }

                                }
                     elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
                                        { 
                                                $dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);

                                                        //1) gad sil gen not pou evaluation sa
                                                if($dataProvider9->getData()!=null)
                                                  { 
                                                    $useCourseReference = true;
                                                         //si kou a evalye pou peryod sa
                                                         $old_subject_evaluated= $this->isOldSubjectEvaluated($reference_id,$room,$eval);
                                                          if($old_subject_evaluated)
                                                                { 
                                                                        $id_course = $reference_id;
                                                                        $careAbout=$old_subject_evaluated;
                                                                }
                                                          else
                                                                 { 
                                                                        $careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
                                                                 }

                                                        }
                                                  elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
                                                         {
                                                                 $careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
                                                         }

                                                }

                           }
                    else
                          {  $id_course = $dataProvider[$k][0];
                              $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                           }

             }
           else
                 {  $id_course = $dataProvider[$k][0];
                              $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                  }

    if($careAbout)
             {			

        // $dataProvider[$k][1] subject_name
        // $dataProvider[$k][3] short_subject_name

         echo ' <th>'.'<span data-toggle="tooltip" title="'.$dataProvider[$k][1].'"> '.$dataProvider[$k][3].'<br/> ('.$dataProvider[$k][2].')</span></th>';
         // style="background-color:#E4E9EF; text-align:center; border-left: 1px solid  #E4E9EF;font-size: 1em;"
              }

              $k++;
}
                       
                 ?>
                 
             </tr>
             
                 <?php 
                    		
		//coefficients
		$k=0;
                /*
                if($include_discipline==1)
                    {    												
                        echo '<td class="" style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 1px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:100px ">'.$max_grade_discipline.'</td>'; 		
		
                    }
                 * 
                 */
                    while(isset($dataProvider[$k][0]))
				{
	               $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
	             if($dataProvider19->getData()==null)
				  {  
				  	
	               
	               if($dataProvider[$k][4]!=NULL) //reference_id
			 	         {
                                            $id_course = $dataProvider[$k][4];
						  //gade si kou sa gen referans
                                            $reference_id=0; 
                                            $useCourseReference=false;
                                            $modelCourse = Courses::model()->findByPk($id_course);

                                            $reference_id = $modelCourse->reference_id;
																        
						if($reference_id==null)//sil pa genyen
							{
								 //si kou a evalye pou peryod sa
							 $old_subject_evaluated= $this->isOldSubjectEvaluated($id_course,$room,$eval);         
							  if($old_subject_evaluated)
								 {
								 	 $id_course = $dataProvider[$k][4];
								 	 $careAbout=$old_subject_evaluated;//$this->isOldSubjectEvaluated($id_course,$this->room_id,$this->evaluation_id);						                        
								  }
							   else
								 {   $id_course = $dataProvider[$k][0];
								 	$careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);						                       															                       
									}
							}
					     elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
								{ 
									$dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);
																          	              
										//1) gad sil gen not pou evaluation sa
									if($dataProvider9->getData()!=null)
									  { $useCourseReference=true;
										 //si kou a evalye pou peryod sa
										 $old_subject_evaluated= $this->isOldSubjectEvaluated($reference_id,$room,$eval);
										  if($old_subject_evaluated)
											{ 
												$id_course = $reference_id;
												$careAbout=$old_subject_evaluated;
											}
										  else
											 { 
												$careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
											 }

										}
									  elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
										 {
											 $careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
										 }
								}
						 }
						 else
							{  $id_course = $dataProvider[$k][0];
							    $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
							}
						
					}
					 else
						{  $id_course = $dataProvider[$k][0];
						    $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
						}	        
			  /*
			      if($careAbout)
				     {			
                                     echo '<td>'.$dataProvider[$k][2].'</td>';
                                     //style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 1px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:100px "v
														
				}
				*/	
					$k++;
		  }
                 ?>
           <!--  </tr> -->
             
      </thead>
             <tbody>   
         
       
<?php 
    //pou chak elev

    if($gen_done)		       
        if(isset($dataProvider_student))
            {  
                $students=$dataProvider_student->getData();
                $konte_liy = 1;
		foreach($students as $stud)    //foreach($stud_order_desc_average as $stud)
		   {  
                    $k=0;
                    $average_ = $this->getAverageForAStudent($stud->id, $room, $eval, $acad_sess);
                    
                    ?>
             <tr>
                 <td><?= $konte_liy; ?></td>
                 <td>
                     <b>
                    <?= $stud->first_name; ?>
                     </b>
                 </td>
                 <td>
                     <b>
                    <?= $stud->last_name; ?>
                     </b>
                 </td>
                 <td>
                     <?php if($average_ < $passing_grade) { ?>
                     <span style="color: red;"><b><?= $average_;  ?></b></span>
                     <?php }else { 
                         ?>
                     <b>
                     <?php 
                         
                         echo $average_; 
                     }
                    ?>
                     </b>
                 </td>
             
    <?php 
                    if($include_discipline==1)
			{		
			//discipline grade
			$period_acad_id =null;
                        $result=EvaluationByYear::model()->searchPeriodName($eval);
                        if(isset($result))
                            {  $result=$result->getData();//return a list of  objects
                                    foreach($result as $r)
                                     {
                                            $period_exam_name= $r->name_period;
                                            $period_acad_id = $r->id;
                                      }
                              }
			                                    
			$grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($stud->id, $period_acad_id);
			
                        ?>
         <td>
             <span style="color: green;">
                 <span class="" ><span><?= $grade_discipline; ?></span>
             </span>
         </td>
             
             
             <?php 
                        
                        }
            while(isset($dataProvider[$k][0]))
                {
	        $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
		if($dataProvider19->getData()==null)
                    { 
                    if($dataProvider[$k][4]!=NULL) //reference_id
			{  $id_course = $dataProvider[$k][4];
														
                             //gade si kou sa gen referans
                            $reference_id = 0; 
                            $useCourseReference=false;
                            $modelCourse = Courses::model()->findByPk($id_course);

                            $reference_id = $modelCourse->reference_id;
			    if($reference_id==null)//sil pa genyen
                            {								
                             //si kou a evalye pou peryod sa
                            $old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$room,$eval);         
                              if($old_subject_evaluated)
                                     {
                                        $id_course = $dataProvider[$k][4];
                                        $careAbout=$this->isOldSubjectEvaluated($dataProvider[$k][4],$room,$eval);						                        
                                      }
                               else
                                     {  
                                        $id_course = $dataProvider[$k][0];
                                        $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);						                       															                       
                                      }
                            }
                             elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
                                { 
                                $dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);
                                     //1) gad sil gen not pou evaluation sa
                                if($dataProvider9->getData()!=null)
                                  { 
                                    $useCourseReference=true;
                                     //si kou a evalye pou peryod sa
                                    $old_subject_evaluated=$this->isOldSubjectEvaluated($reference_id,$room,$eval);
                                    if($old_subject_evaluated)
                                    { 
                                            $id_course = $reference_id;
                                            $careAbout=$old_subject_evaluated;
                                    }
                                    else
                                     { 
                                        $careAbout=$this->isSubjectEvaluated($id_course,$room,$eval);
                                     }

                                        }
                                  elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
                                         {
                                             $careAbout=$this->isSubjectEvaluated($id_course,$room,$eval);
                                         }
                                 }
															
			}
			else
                            {  
                            $id_course = $dataProvider[$k][0];
                             $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                            }
			}
                         else
                            {  
                             $id_course = $dataProvider[$k][0];
                             $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                            }
			if($careAbout)
			{			
			//calculer grade puis afficher
                        $debase_passingGrade=0;
                        $debase='';
                        $weight='';
                        $id_course=$dataProvider[$k][0];
                        $passing_value=0;
							                             
                        $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
                        if($dataProvider19->getData()==null)
                            {  
                            if($dataProvider[$k][4]!=NULL) //reference_id
                                {  
                                $id_course = $dataProvider[$k][4];
				//gade si kou sa gen referans
                                $reference_id=0; 
                                $useCourseReference=false;
                                $modelCourse = Courses::model()->findByPk($id_course);
                                $reference_id = $modelCourse->reference_id;
																									        
                                if($reference_id==null)//sil pa genyen
                                {								
                                    //si kou a evalye pou peryod sa
                                  $old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$room,$eval);         
                                  if($old_subject_evaluated)
                                     {
                                        $id_course = $dataProvider[$k][4];						                        
                                      }
                                   else
                                     { 
                                       $id_course = $dataProvider[$k][0];						                       															                       
                                      }

                                  }
                                 elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
                                    { 
                                    $dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);
                                       //1) gad sil gen not pou evaluation sa
                                    if($dataProvider9->getData()!=null)
                                      { 
                                        $useCourseReference=true;
                                         //si kou a evalye pou peryod sa
                                         $id_course = $reference_id;
                                         $old_subject_evaluated=$this->isOldSubjectEvaluated($reference_id,$room,$eval);
                                          if($old_subject_evaluated)
                                            { 
                                              $id_course = $reference_id;
                                            }
                                          else
                                             { 
                                               $id_course = $dataProvider[$k][4];
                                             }

                                            }
                                      elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
                                             {
                                               $id_course = $dataProvider[$k][4];
                                             }

                                    }
                    }
                     else
                        {  
                         $id_course = $dataProvider[$k][0];

                         }
                        }
                     else
                        {  
                         $id_course = $dataProvider[$k][0];
                        }
							                            
                    $resultDebase=Courses::model()->ifCourseDeBase($id_course);
                    if($resultDebase!=null)
                     {
                        foreach($resultDebase as $base)
                         { $weight=$base['weight'];
                           $debase=$base['debase'];
                          }

                       }

                    if($debase==1)
                      {
                         $pass_gra=PassingGrades::model()->getCoursePassingGrade($id_course, $acad_sess);
                          if($pass_gra!=null)
                            $debase_passingGrade=$pass_gra;

                            $passing_value=$debase_passingGrade;

                        }
                     else
                       $passing_value = $dataProvider[$k][2]/2;
$grades=Grades::model()->searchForReportCard($condition,$stud->id,$id_course,$eval); 
    if(isset($grades))
    {
        $r=$grades->getData();//return a list of  objects
        if($r!=null)
        { 
            foreach($r as $grade)
             {
                $estil = 'style="color: green;"'
                ?>
         
         <td class="<?= $grade->id; ?>">
            <?php
                 if( (Yii::app()->user->profil=='Admin')||(Yii::app()->user->profil=='Manager') )
                   {

            ?> 
             <span id="<?= $grade->id ?>" class="modifye" data-gradeid="<?= $grade->id?>" data-grade="<?= $grade->grade_value; ?>" data-subject="<?= $dataProvider[$k][1]; ?>" data-coefficient="<?=$dataProvider[$k][2]; ?>" data-validate="<?= $grade->validate; ?>" > 
                <?php 
                        }
                    else
                      {
                  ?>
                      <span >
                  <?php
                         }
                         
                    if($grade->grade_value!=NULL){
                        if($grade->validate == 0){
                            $estil = 'style="color: black;"';
                        }elseif($grade->grade_value < $passing_value){
                            $estil = 'style="color: red;"';
                        }
                        elseif($grade->validate==1){
                            $estil = 'style="color: green;"';
                        }
                        ?>
                        <span <?= $estil?>>
                            <span title="<?= $grade->comment?>"><span data-toggle="tooltip" title="<?= $dataProvider[$k][1]; ?>" ><?= $grade->grade_value; ?></span></span>
                        </span>
                 <?php 
                        
                    }else{
                        if($grade->validate == 0){
                            $estil = 'style="color: black;"'; 
                        }else{
                            $estil = 'style="color: green"';
                        }
                        
                        ?>
             <span <?= $estil; ?>>
                 N/A
             </span>
             <?php 
                    }
                ?>
             </span>  
         </td>
            
    <?php         

            }
                 }
            else
              { 
                ?>
         <td style="text-align:center;  border-right: 1px solid #E4E9EF">
             <span>N/A</span>
         </td>
             <?php 
                
                }
                $showButton=true;

        }//fin if(isset($grades))

           }//fin careAbout

            $k++;
           // $konte_liy++;

        }//fin while(isset($dataProvider[$k][0]))								  
	
        $konte_liy++;
        ?>
       </tr>
             <?php 
          
                    
        }// fin foreach($students as $stud)
	
        
							  
}// fin if(isset($dataProvider_student))
							  
							  
			            //pou chak elev
			
             ?>
       
         </tbody>
         
     </table>
 </div>
 
 <!-- Modal pour modifier les notes -->
 
 <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
    
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title"><?= Yii::t('app','Update grade for : '); ?><span id="en-tete"></span></h4>
        </div>
        <div class="modal-body">
            <input type="text" name="grade_value" id="grade_value" />
            <input type="hidden" name="grade_id" id="grade_id"/>
            <input type="hidden" name="coefficient" id="coefficient"/>
            <input type="hidden" name="subject" id="subject"/>
            <input type="hidden" name="validate" id="validate"/>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-primary" id="update"><?= Yii::t('app','Update'); ?></button>  
          <button type="button" class="btn btn-default" data-dismiss="modal"><?= Yii::t('app','Close') ?></button>
        </div>
      </div>
      
    </div>
  </div>
 <?php 
 
    // Conversion de l'image du logo en base64
   $path = str_replace('protected','',Yii::app()->basePath).'/css/images/school_logo.png';
   $type = pathinfo($path, PATHINFO_EXTENSION);
   $data = file_get_contents($path);
   $base64 = 'data:image/' . $type . ';base64,' . base64_encode($data);
   $school_name = infoGeneralConfig('school_name');
   $school_address = infoGeneralConfig('school_address'); 
   $school_phone_number = infoGeneralConfig('school_phone_number'); 
   $school_email_address = infoGeneralConfig('school_email_address');
   
   
 ?>
 
 <script>
     
     $(document).ready(function() {
         // Pour modifier les notes depuis le palmares 
         $(".modifye").click(function(){
                var gradeid = $(this).attr("data-gradeid"); 
                var grade = $(this).attr("data-grade"); 
                var subject = $(this).attr("data-subject");
                var coefficient = $(this).attr('data-coefficient');
                var validate = $(this).attr('data-validate'); 
                $("#en-tete").text(subject);
                $("#grade_value").val(grade); 
                $("#grade_id").val(gradeid); 
                $('#coefficient').val(coefficient); 
                $('#validate').val(validate); 
                $('#subject').val(subject);  
                
                $("#myModal").modal();
                 $('#myModal').on('hidden.bs.modal', function () {
                        $("#grade_value").val(""); 
                        $("#grade_id").val("");
                    });
           }); 
           
           $("#update").click(function(){
               var grade_value = parseFloat($("#grade_value").val());
               var grade_id = $("#grade_id").val(); 
               var coefficient = parseFloat($('#coefficient').val());
               var subject = $('#subject').val(); 
               var validate = $('#validate').val(); 
               //alert(grade_value);
              // $("#"+grade_id).html(grade_value);
               if(grade_value <= coefficient){
                    $.get('<?= Yii::app()->baseUrl ?>/index.php/academic/palmares/updategrade',{grade: grade_value, gradeid: grade_id,subject:subject, coefficient: coefficient, validate: validate}, function(data){
                     //  $("."+grade_id).html(data); 
                     $("#"+grade_id).replaceWith(data);
                    });
                    $('#'+grade_id).data('grade',grade_value);
                }else{
                    alert("<?= Yii::t('app','Grade must be lower than !'); ?> "); 
                }
              // alert("Nou klike wi la a ");
               $('#myModal').modal('toggle');
           });
	
	// DataTable initialisation
        var table = $('.dataTables-exemple'); 
        
	table.DataTable(
	{
                pageLength: 50,
                responsive: true,
                bSortCellsTop: true,
                dom: '<"html5buttons"B>lTfgitp',
		paging: true,
                autoWidth: true,
               
                initComplete: function () {
                        $('.dataTables_filter input[type="search"]').css({ 'width': '200px', 'display': 'inline-block' });
                        $('.html5buttons').css({'width': '300px','margin-left': '10px', 'padding-right':'20px','display': 'inline-block'});
                        $('.btn-warning').css({'margin-left': '10px','display': 'inline-block'});
                        $('.buttons-excel').css({'margin-left': '10px','display': 'inline-block'});
                    },
                    language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    }
                },
                "buttons": [
                    {
                        extend: 'excel', 
                        title: 'palmares<?= $room_name; ?>'
                    },
                
                    {
                      text: '<?= Yii::t('app','Validate grades'); ?>',
                      className: 'btn btn-warning',
                        action: function ( e, dt, node, config ) {
                            $.get('<?= Yii::app()->baseUrl ?>/index.php/academic/palmares/validation',{eval:<?= $eval?>,room:<?= $room?>},function(data){
                                $('#validation_message').html(data);
                            });
                        }
                    },
                        {
                            text: '<?= Yii::t('app','Print PDF');?>',
                             className: 'btn btn-success'   
                        },
                        
                        
                        {
                            
                        text: '<?= Yii::t('app','View PDF') ?>',
                        className: 'btn btn-warning',
                        extend: 'pdfHtml5',
                        filename: 'palmares_<?= $room_name."_".$academic_period_name."_".$evaluation?>',
                        orientation: 'landscape', //portrait // landscape
                        pageSize: 'legal', //A4, A3 , A5 , A6 , legal , letter
                        header: true,
                                exportOptions: {
                                        columns: ':visible',
                                        search: 'applied',
                                        order: 'applied'
                                },
				customize: function (doc) {
						//Remove the title created by datatTables
                                    doc.content.splice(0,1);
                                    //Create a date string that we use in the footer. Format is dd-mm-yyyy
                                    var now = new Date();
                                    var jsDate = now.getDate()+'-'+(now.getMonth()+1)+'-'+now.getFullYear();
                                    // Logo converted to base64
                                     var logo = '<?= $base64; ?>';
                                    //var logo = 'data:image/jpeg;base64,/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAICAgICAQICAgIDAgIDAwYEAwMDAwcFBQQGCAcJCAgHCAgJCg0LCQoMCggICw8LDA0ODg8OCQsQERAOEQ0ODg7/2wBDAQIDAwMDAwcEBAcOCQgJDg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg7/wAARCAAwADADASIAAhEBAxEB/8QAGgAAAwEAAwAAAAAAAAAAAAAABwgJBgIFCv/EADUQAAEDAgQDBgUDBAMAAAAAAAECAwQFBgAHESEIEjEJEyJBUXEUI0JhgRVSYhYXMpEzcrH/xAAYAQADAQEAAAAAAAAAAAAAAAAEBQYHAv/EAC4RAAEDAgMGBQQDAAAAAAAAAAECAxEABAUGEhMhMUFRcSIyYaHBFkKB0ZGx8P/aAAwDAQACEQMRAD8Avy44hlhTrqw22kEqUo6BIG5JPkMSxz67RlFPzFquWnDParOaN4QVlmqXDKcKKLS19CCsf8qh6A6e+OfaK573LDTanDJllVV0q8r3ZVIuGqR1fMpdJSdHCCOinN0j7e+FjymydjRKdSbGsikpbSlG5O3/AHfeX5nU6knck6DFdg+DovkquLlWllHE8yeg+f4FBPvluEpEqNC657/4yr4ecm3ZxH1OghzxfptpQERI7X8QrqdPXGNpucXGLltU0SbZ4jazW0tHX4C6IiJcd37HUEj8YoHNtTKOzwuHVPj79rTfhkfCudxEbUOqQQd9Pc4HlaoGRt2JVAcptRsOe54WZZkd6yFHpzakgD3098ahYWuVVDQ/YrKD9wJnvGqfb8UAHH584npWw4eu0+iVO+6Vl3xO2zHy1uKa4GafdcBwqos5w7AOE6lgk+epT68uK8MvNPxmnmHEvMuJCm3EKCkqSRqCCNiCPPHmbzdyWcozkq1rpitVSkzGyqHNbT4HU+S0H6Vp22/9Bw8XZkcQ1wuzLg4V8yqq5U69a0X42zalJXq5NpeuhZJO5LWo0/idPpxI5ryszgyG77D3Nrau+U8weh/cDgQRI3sGXi54VCCKXK6Ku5fnbOcTt2znO/8A0SfFtymcx17llpGqgPTUjDj5WOIOUmYFPpLgjXQ5ES627r43I6R40I9D16fuGEfzPZeyq7afiRtec0W03O/GuSj82wdbdb8ZB89FEjb0xvrIzGk2pmnSrgcdUttl3lkoB2UyrZadPbf8DFFhGHuX+W0bASUyY6kKJg96XPK0XJmt9MrkFuIQw2XNup8IwFbruVaWXkttMgadCCcEfNuPTbbzPkiK87+jVRsTqctlIKVNubkD2J/0RgBVFDVQUpTTEksjdTjpG4xc4TYOvBu5AhB3yf8AcfmgTIUUmiMxcs27+CG42Koy3JqFqym3YLytebuVfRr9gVD2AwvOWt5u2f2qXDle0FK4UhVwijzgFbPMSUlBSftqdcMAqN/TfCVV0yGBDl3O+huMwvZXw6Oqzr67n8jC85VWw/fnakZD2tAaL/wtwGsSuTfu2YyCeY+6ikY5x1yzVlDECB4C8Nn3lEx6SFe9MWtW3R1jfVTu0l4a7lv6wbaz8yqp6p2Z2X6FmXT2U6uVelq8TrQA3UtG6gPMFQG+mJe2Xf8ASL5s1qp0p35qfDLhuHR2M4P8kLT5aH/ePUSpIUnQjUemJh8SXZs2fmVf8/MvJevKyfzNkEuTPhGeamVNZ3JeZGnKonqpPXqQTjE8tZmdwF4hSdbSjvHMHqP1zo24tw8J4EUn9MvWz7iymo9tX27PgTqQ4tMCfGY735SuiFdenTTTyGOIrGV1DSJLCqndb7Z1aamIDEZJHQqGg5vyDga3Fw28bVhS1wqrlHAzAjtkhFSt2sIQHR5HkXoQftjrqJw5cYt81BESDkuxaCVnRU24K0Fpb+/I3qT7Y1b6kygptSi88lKiSWxIEkyRygE8tUUDsbieA71mM2M0mZxlVytTQ0w0jkQlIIQ2PpabR1JJ6Abk4oP2bHDhW6O9WuITMKlLplxV9hMeg06Sn5lPgjdIUPJayedX4HljvOHvs16VbF7Uy/c86/8A3DuyIoOwoAaDdPgL66ts7gqH7lan2xVaJEjQaezFiMIjx2khLbaBoEgYyzMmZTjWi2t0bK3b8qfk+v8AW/jNMGWdn4lGVGv/2SAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICA=';
                                    // A documentation reference can be found at
                                    // https://github.com/bpampuch/pdfmake#getting-started
                                    // Set page margins [left,top,right,bottom] or [horizontal,vertical]
                                    // or one number for equal spread
                                    // It's important to create enough space at the top for a header !!!
                                    doc.pageMargins = [30,80,20,30];
                                    // Set the font size fot the entire document
                                    doc.defaultStyle.fontSize = 7;
                                    // Set the fontsize for the table header
                                    doc.styles.tableHeader.fontSize = 7;
                                    // Create a header object with 3 columns
                                    // Left side: Logo
                                    // Middle: brandname
                                    // Right side: A document title
                                    doc['header']=(function() {
                                            return {
                                        columns: [
                                                {
                                                        image: logo,
                                                        width: 40
                                                },
                                                {
                                                        alignment: 'left',
                                                        italics: true,
                                                        bold: true,
                                                        text: "<?= $school_name?>\n<?= $school_address ?>\n<?= $school_phone_number?>\n<?= $school_email_address?>",
                                                        fontSize: 10,
                                                        margin: [10,0]
                                                },
                                                {
                                                        alignment: 'left',
                                                        italics: true,
                                                        bold: true,
                                                        fontSize: 10,
                                                        text: "<?= Yii::t('app','Grades By Room')?> <?= $room_name?>\n<?= $evaluation?>\n<?=Yii::t('app','Academic year');?> : <?= $academic_period_name?>\n<?= $section_name ?>/<?= $shift_name?>"
                                                }
                                        ],
                                         margin: 30
                                            }
                                    });
                                // Create a footer object with 2 columns
                                // Left side: report creation date
                                // Right side: current page and total pages
                                doc['footer']=(function(page, pages) {
                                return {
                                    columns: [
                                        {
                                                alignment: 'left',
                                                text: ["<?= Yii::t('app','Created on: ') ?>", { text: jsDate.toString() }]
                                        },
                                        {
                                                alignment: 'center',
                                                text: ["<a>me voici</a>"]
                                        },
                                        {
                                                alignment: 'right',
                                                text: ['page ', { text: page.toString() },	'/',	{ text: pages.toString() }]
                                        }
                                    ],
                                        margin: 20
                                }
                                });
						// Change dataTable layout (Table styling)
						// To use predefined layouts uncomment the line below and comment the custom lines below
						 doc.content[0].layout = 'lightHorizontalLines'; // noBorders , headerLineOnly
                                               
				}
				}
                                
                       ]
		});
                table.on('responsive-resize.dt', function(e, datatable, columns) {
		for (var i in columns) {
			var index = parseInt(i, 10) + 1;
			table.find('th:nth-child(' + index + ')').toggle(columns[i]);
		}
                });
});

 
</script>




 