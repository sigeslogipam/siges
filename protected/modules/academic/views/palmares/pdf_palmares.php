<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

// echo "Room : $room<br/>Eval: $eval<br/>Shift: $shift";



/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//require_once __DIR__ . '/vendor/autoload.php';
 $acad = Yii::app()->session['currentId_academic_year'];
 /*
 $array_stud = explode(",", $student); 
 // Define the size 
 $format_paper = array(); 
 $orientation_paper = "";
 */
 $format_paper_ = infoGeneralConfig('size_paper_palmares');
 if($format_paper_ == "Legal"){
     $format_paper = array(216,356);
 }elseif($format_paper_=="Letter"){
     $format_paper = array(216,279); 
 }else{
     $format_paper = array(216,279);
 }
 
 $orientation_paper_ = infoGeneralConfig('orientation_paper_palmares');
 if($orientation_paper_ == "P"){
     $orientation_paper = "P";
 }elseif($orientation_paper_== "L"){
     $orientation_paper = "L";
 }else{
     $orientation_paper = "P";
 }
 
 
$mpdf = new \Mpdf\Mpdf(array(
        'margin_top' =>35,
	'margin_left' =>10 ,
	'margin_right' =>10,
        'margin_bottom'=>40,
	//'mirrorMargins' => true,
        'mode' => 'utf-8', 
        'format' =>$format_paper,
        'orientation'=>$orientation_paper
    ));
// 8.5 * 14 : array(216,356) -> Letter 
// 8.5 * 11 : array(216,279) -> Legal
// P : Portrait
// L : Landscape
//  'Legal-P'
//$mpdf->SetColumns(1);
$mpdf->SetHTMLHeader($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLHeader($this->renderPartial('header_pdf_palmares',array(
     'room'=>$room,
     'eval'=>$eval,
     'shift'=>$eval,
     'acad'=>$acad,
    'evaluation'=>$evaluation
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);
//$mpdf->AddColumn();
//$mpdf->shrink_tables_to_fit = 1;
// $mpdf->SetColumns(2,'J',5);
// $mpdf->keepColumns = true;
//$mpdf->max_colH_correction = 5;
//$mpdf->WriteHTML('<columns column-count="2" vAlign="J" column-gap="7" />');
//
// To add watermark to the PDF Uncomment if you want to apply 
/*
$mpdf->SetWatermarkImage(Yii::app()->baseUrl.'/css/images/school_logo.png');
$mpdf->showWatermarkImage = true;
 * 
 */

$mpdf->WriteHTML($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->WriteHTML($this->renderPartial('body_pdf_palmares',array(
     'room'=>$room,
     'eval'=>$eval,
     'shift'=>$eval,
     'acad'=>$acad,
     'evaluation'=>$evaluation
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);

//$mpdf->SetColumns(1);

$mpdf->SetHTMLFooter($this->renderPartial('fey-estil',array(), true),\Mpdf\HTMLParserMode::HEADER_CSS);
$mpdf->SetHTMLFooter($this->renderPartial('footer_pdf_palmares',array(
     'room'=>$room,
     'eval'=>$eval,
     'shift'=>$eval,
      'acad'=>$acad
    ), true),\Mpdf\HTMLParserMode::HTML_BODY);
//$mpdf->AddColumn();
 

//$mpdf->WriteHTML($kontni);
$room_name = Rooms::model()->findByPk($room)->short_room_name;
//$period_name = KinderPeriod::model()->findByPk($period)->period_name;
$mpdf->Output("$room_name-$evaluation.pdf",\Mpdf\Output\Destination::INLINE);
//echo 'Hello World !';