<?php 
$room_name = Rooms::model()->findByPk($room)->short_room_name;
$shift_name = Shifts::model()->findByPk(Rooms::model()->findByPk($room)->shift)->shift_name;
$level = Rooms::model()->findByPk($room)->level;
$section_name = Sections::model()->findByPk(Levels::model()->findByPk($level)->section)->section_name;
?>
<div id='wrapper' >    
    <div style='float:left;width:10%; padding: 5px'> 
      <img class="img-logo" src="<?= Yii::app()->baseUrl.'/css/images/school_logo.png' ; ?>"/>
      
    </div>    
    <div style='float:left;width:40%'>   
        <span class="non-lekol"><strong><?= infoGeneralConfig('school_name'); ?></strong></span><br/>
        <span class="adres"><?= infoGeneralConfig('school_address'); ?></span><br/>
        <span class="telefon"><?= infoGeneralConfig('school_phone_number'); ?></span><br/>
        <span class="email"><?= infoGeneralConfig('school_email_address'); ?></span>
    </div>
    <div style='float: left;width:40%'> 
        <span>
            <?= Yii::t('app','Grades By Room')?> <?= $room_name?><br/>
        </span>
        <span>
            <?= $evaluation; ?><br/>
        </span>
        <span>
            <?= Yii::t('app','Academic year')?> </strong>: <?= AcademicPeriods::model()->findByPk($acad)->name_period; ?><br/>
        </span>
        <span>
            <?= $section_name ?>/<?= $shift_name?>
        </span>
        <br/>
        
    </div>
    <hr size="30"/>
</div>

