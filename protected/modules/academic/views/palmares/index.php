<?php
$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];

$nom_section = infoGeneralConfig('kindergarden_section'); 

//$sql_str = "SELECT r.id, r.room_name, s.shift_name, r.shift FROM rooms r INNER JOIN shifts s ON (s.id = r.shift) ORDER BY r.room_name";


$sql_str = "SELECT r.id, r.room_name, s.shift_name, r.shift FROM rooms r INNER JOIN shifts s ON (s.id = r.shift) INNER JOIN levels l ON (r.level = l.id) INNER JOIN sections se ON (l.section = se.id)
WHERE se.section_name <> '$nom_section'
ORDER BY r.room_name";
$all_rooms = Rooms::model()->findAllBySql($sql_str);
$evaluation = $this->loadEvaluation($acad_sess);
$to_remove = Yii::t('app','-- Select --');
if (($key = array_search($to_remove, $evaluation)) !== false) {
    unset($evaluation[$key]);
}


?>
<div id="dash">
		<div class="span3"><h2>
		    <?php  if((Yii::app()->user->profil!='Teacher'))
																		echo Yii::t("app", "List student's grades by room");
																	else
																	   echo Yii::t("app", "Grades By Rooms");
																	 ?>
		</h2> </div>


      <div class="span3">


  <?php

     if(!isAchiveMode($acad_sess))
        {
           if($acad == $current_acad_id)
		   {   
		  if( (Yii::app()->user->profil=='Admin')||(Yii::app()->user->profil=='Manager') )
             {
     ?>

				  <div class="span4">

                  <?php



                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';

                               // build the link in Yii standard

                     echo CHtml::link($images,array('/academic/grades/create?from=stud&mn=std'));

                   ?>

              </div>


		<?php 
                }
			  if(Yii::app()->user->profil=='Teacher')
			         {
				?>

			  <div class="span4">

                  <?php



                     $images = '<i class="fa fa-edit">&nbsp;'.Yii::t('app','Update').'</i>';

                               // build the link in Yii standard

                     echo CHtml::link($images,array('grades/update?all=1&from=stud&mn=std'));

                   ?>

              </div>

			  <?php
					  }

				?>

 <?php
        
		     }
		}

      ?>


      <div class="span4">
                      <?php



                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                               // build the link in Yii standard

                     echo CHtml::link($images,array('/academic/persons/listforreport?isstud=1&from=stud'));

                   ?>

                  </div>

     </div>

 </div>

<div class="clear"></div>

<ul id="room">
    <?php

   for($i=0;$i<count($all_rooms); $i++){
       ?>

    <li><a href="#"><?php echo $all_rooms[$i]['room_name'].' ('.$all_rooms[$i]['shift_name'].')';?></a>

        <ul>
            <?php
                foreach($evaluation as $key=>$e){
                    ?>
            <li><?= $e; ?> | <a href="<?= Yii::app()->baseUrl ?>/index.php/academic/palmares/palmares?room=<?= $all_rooms[$i]['id']?>&eval=<?= $key?>&shift=<?= $all_rooms[$i]['shift']?>&from=stud&mn=std" title="<?= Yii::t('app','Grades By Room') ?>"><i class="fa fa-table btn btn-primary"> <?= Yii::t('app','Grades By Room') ?></i></a> 
                <!--
                <a href="<?= $key?>" title="<?=Yii::t('app','Transcript')?>"><i class="fa fa-print btn btn-success"> <?=Yii::t('app','Transcript')?></i></a></li>
            -->
            <?php
                }
            ?>
        </ul>
    </li>

    <?php
   }

?>

</ul>



<script>
    $.fn.extend({
    treed: function (o) {

      var openedClass = 'fa fa-minus';//'glyphicon-minus-sign';
      var closedClass = 'fa fa-plus';//'glyphicon-plus-sign';

      if (typeof o != 'undefined'){
        if (typeof o.openedClass != 'undefined'){
        openedClass = o.openedClass;
        }
        if (typeof o.closedClass != 'undefined'){
        closedClass = o.closedClass;
        }
      };

        //initialize each of the top levels
        var tree = $(this);
        tree.addClass("tree");
        tree.find('li').has("ul").each(function () {
            var branch = $(this); //li with children ul
            branch.prepend("<i class=' " + closedClass + "'></i> ");
            branch.addClass('branch');
            branch.on('click', function (e) {
                if (this == e.target) {
                    var icon = $(this).children('i:first');
                    icon.toggleClass(openedClass + " " + closedClass);
                    $(this).children().children().toggle();
                }
            })
            branch.children().children().toggle();
        });
        //fire event from the dynamically added icon
      tree.find('.branch .indicator').each(function(){
        $(this).on('click', function () {
            $(this).closest('li').click();
        });
      });
        //fire event to open branch if the li contains an anchor instead of text
        tree.find('.branch>a').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
        //fire event to open branch if the li contains a button instead of text
        tree.find('.branch>button').each(function () {
            $(this).on('click', function (e) {
                $(this).closest('li').click();
                e.preventDefault();
            });
        });
    }
});

//Initialization of treeviews

$('#tree1').treed();
$('#room').treed();

$('#tree2').treed({openedClass:'glyphicon-folder-open', closedClass:'glyphicon-folder-close'});

$('#tree3').treed({openedClass:'glyphicon-chevron-right', closedClass:'glyphicon-chevron-down'});

    </script>
    <!--
    indicator glyphicon
    -->
