<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


 $current_acad_id= 0;
$current_year = currentAcad();
  if($current_year!=null)
	$current_acad_id = $current_year->id;
   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year'];
$academic_period_name = AcademicPeriods::model()->findBYPk($acad)->name_period;
$room_name = Rooms::model()->findByPk($room)->short_room_name;
$shift_name = Shifts::model()->findByPk(Rooms::model()->findByPk($room)->shift)->shift_name;
$level = Rooms::model()->findByPk($room)->level;
$current_acad = AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
$section_name = Sections::model()->findByPk(Levels::model()->findByPk($level)->section)->section_name;

if($current_acad==null) { 
    $condition = '';
    $condition1 = '';
    }
     else{
           if($acad!=$current_acad->id)
                 { 
               $condition = '';
                $condition1 = '';
                        }
              else
                { 
                  $condition = 'p.active IN(1,2) AND ';
                  $condition1 = 'student0.active IN(1,2) AND ';
                }
 }
$id_teacher='';   
$pers = User::model()->getPersonByUserId(Yii::app()->user->userid);
$pers = $pers->getData();
foreach($pers as $p){
    $id_teacher = $p->id;  
}
?>

 <!--Menu of CRUD  -->

 <?php 

$average_base = 0;

$person = new Persons();

$tot_stud=0;
$showButton=false;
$total_succ =0;
$total_fail=0;
$passing_grade='N/A';
$classAverage='N/A';
$gen_done=false;  

$include_discipline = 0;
//Extract 
$include_discipline = infoGeneralConfig("include_discipline_grade");
//Extract max grade of discipline
$max_grade_discipline = infoGeneralConfig('note_discipline_initiale');

$include_discipline_comment=Yii::t('app',"Behavior grade is included in this average.");

//tcheke si gen not pou sal sa deja
$sql__ = 'SELECT g.id FROM grades g INNER JOIN courses c ON(c.id=g.course) WHERE c.room='.$room.' AND evaluation='.$eval.' ORDER BY id DESC';
															
$command__ = Yii::app()->db->createCommand($sql__);
$result__ = $command__->queryAll(); 

if($result__!=null){ 
    $gen_done = true;
    }
    if($gen_done){
       
        
        $dataProvider_studentEnrolled = Rooms::model()->getStudentsEnrolled($condition,$room, $acad);
        
         
        
        //moyenne de la classe
       // $classAverage = $this->getClassAverage($room, $acad_sess);
            //total reussi, et echoue
            //moyenne de passage classe
        $passing_grade = $this->getPassingGrade($level,$acad_sess); //note de passage pour la classe
         
		$average_base = getAverageBase($room,$acad_sess);
                
        $dataProvider_studentEnrolledInfo= Rooms::model()->getInfoStudentsEnrolled($condition,$room, $acad_sess); 
        
        if(isset($dataProvider_studentEnrolledInfo))
        {  $t=$dataProvider_studentEnrolledInfo->getData();
           foreach($t as $stud)
              {
              // echo $stud->student_id;
                  //moyenne yon elev
               $averag = $this->getAverageForAStudent($stud->student_id, $room, $eval, $acad_sess);
               $stud_order_desc_average[$stud->student_id]= $averag;
              }
        }
        
        //$min_average et $max_average_ 
       // arsort($stud_order_desc_average);
         // get the first item in the $stud_order_desc_average
        // $max_average = reset($stud_order_desc_average);

         // get the last item in the $stud_order_desc_average
        // $min_average = end($stud_order_desc_average);
         
	$dataProvider = $this->getAllSubjects($room,$level);
	//Grades for the current period
	//liste des etudiants
           
        $dataProvider_student = Persons::model()->getStudentsByRoomForGrades($condition,$room, $acad_sess);
			 
     

?>
 
 
<?php 

    }
    $couleur=1;
    $k=0;

?>

 <div class="grid-view" id="table-container" style="margin-top:0px;  margin-bottom: 20px; float:left;   width:100%;  overflow-x:scroll; overflow-y:hidden;">
     <!-- table-striped table-bordered table-hover table-condensed items
        table table-striped table-bordered table-condensed table-hover table-responsive dataTables-exemple
     
     -->
     <table class="table table-striped table-bordered">
         <thead> 
             <tr style="background: #6F7E94;">
                 <td><?= 'Num' ?></td>
                 <td><?= Yii::t('app',"First Name"); ?></td>
                 <td><?= Yii::t('app',"Last Name"); ?></td>
                 <td><?= Yii::t('app',"Average"); ?></td>
                 
                 <?php 
                 if($include_discipline==1)
                        {    												
                        echo ' <td style="background-color:#E4E9EF; text-align:center; border-left: 1px solid  #E4E9EF;font-size: 1em;">'.Yii::t('app','Discipline').' ('.$max_grade_discipline.')</td>';  		
		
                    }
                    
                    //liste des cours
		while(isset($dataProvider[$k][0]))
				{
	             $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
	             if($dataProvider19->getData()==null)
				  {  
                        if($dataProvider[$k][4]!=NULL) //reference_id
			 	     {  
                            $id_course = $dataProvider[$k][4];
			     //gade si kou sa gen referans
                        $reference_id=0; 
                        $useCourseReference=false;
                        $modelCourse = Courses::model()->findByPk($id_course);

                        $reference_id = $modelCourse->reference_id;

                        if($reference_id==null)//sil pa genyen
                                {
                                 //si kou a evalye pou peryod sa
                                $old_subject_evaluated= $this->isOldSubjectEvaluated($id_course,$room,$eval);         
                                  if($old_subject_evaluated)
                                         {
                                                $id_course = $dataProvider[$k][4];
                                                $careAbout= $this->isOldSubjectEvaluated($dataProvider[$k][4],$room,$eval);						                        
                                          }
                                   else
                                         {   $id_course = $dataProvider[$k][0];
                                                $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);						                       															                       
                                                }

                                }
                     elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
                                        { 
                                                $dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);

                                                        //1) gad sil gen not pou evaluation sa
                                                if($dataProvider9->getData()!=null)
                                                  { 
                                                    $useCourseReference = true;
                                                         //si kou a evalye pou peryod sa
                                                         $old_subject_evaluated= $this->isOldSubjectEvaluated($reference_id,$room,$eval);
                                                          if($old_subject_evaluated)
                                                                { 
                                                                        $id_course = $reference_id;
                                                                        $careAbout=$old_subject_evaluated;
                                                                }
                                                          else
                                                                 { 
                                                                        $careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
                                                                 }

                                                        }
                                                  elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
                                                         {
                                                                 $careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
                                                         }

                                                }

                           }
                    else
                          {  $id_course = $dataProvider[$k][0];
                              $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                           }

             }
           else
                 {  $id_course = $dataProvider[$k][0];
                              $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                  }

    if($careAbout)
             {			

        // $dataProvider[$k][1] subject_name
        // $dataProvider[$k][3] short_subject_name

         echo ' <td style="text-align: center">'.'<span data-toggle="tooltip" title="'.$dataProvider[$k][1].'"> '.$dataProvider[$k][3].' ['.$dataProvider[$k][2].']</span></td>';
         // style="background-color:#E4E9EF; text-align:center; border-left: 1px solid  #E4E9EF;font-size: 1em;"
              }

              $k++;
}
                       
                 ?>
                 
             </tr>
             
                 <?php 
                    		
		//coefficients
		$k=0;
                /*
                if($include_discipline==1)
                    {    												
                        echo '<td class="" style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 1px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:100px ">'.$max_grade_discipline.'</td>'; 		
		
                    }
                 * 
                 */
                    while(isset($dataProvider[$k][0]))
				{
	               $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
	             if($dataProvider19->getData()==null)
				  {  
				  	
	               
	               if($dataProvider[$k][4]!=NULL) //reference_id
			 	         {
                                            $id_course = $dataProvider[$k][4];
						  //gade si kou sa gen referans
                                            $reference_id=0; 
                                            $useCourseReference=false;
                                            $modelCourse = Courses::model()->findByPk($id_course);

                                            $reference_id = $modelCourse->reference_id;
																        
						if($reference_id==null)//sil pa genyen
							{
								 //si kou a evalye pou peryod sa
							 $old_subject_evaluated= $this->isOldSubjectEvaluated($id_course,$room,$eval);         
							  if($old_subject_evaluated)
								 {
								 	 $id_course = $dataProvider[$k][4];
								 	 $careAbout=$old_subject_evaluated;//$this->isOldSubjectEvaluated($id_course,$this->room_id,$this->evaluation_id);						                        
								  }
							   else
								 {   $id_course = $dataProvider[$k][0];
								 	$careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);						                       															                       
									}
							}
					     elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
								{ 
									$dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);
																          	              
										//1) gad sil gen not pou evaluation sa
									if($dataProvider9->getData()!=null)
									  { $useCourseReference=true;
										 //si kou a evalye pou peryod sa
										 $old_subject_evaluated= $this->isOldSubjectEvaluated($reference_id,$room,$eval);
										  if($old_subject_evaluated)
											{ 
												$id_course = $reference_id;
												$careAbout=$old_subject_evaluated;
											}
										  else
											 { 
												$careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
											 }

										}
									  elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
										 {
											 $careAbout= $this->isSubjectEvaluated($id_course,$room,$eval);
										 }
								}
						 }
						 else
							{  $id_course = $dataProvider[$k][0];
							    $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
							}
						
					}
					 else
						{  $id_course = $dataProvider[$k][0];
						    $careAbout= $this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
						}	        
			  /*
			      if($careAbout)
				     {			
                                     echo '<td>'.$dataProvider[$k][2].'</td>';
                                     //style="background-color:#E4E9EF; text-align:center; border-bottom: 3px solid  #E4E9EF; border-right: 1px solid  #E4E9EF; border-top: 3px solid  #E4E9EF;width:100px "v
														
				}
				*/	
					$k++;
		  }
                 ?>
           <!--  </tr> -->
             
     </thead>
             <tbody>   
         
       
<?php 
    //pou chak elev

    if($gen_done)		       
        if(isset($dataProvider_student))
            {  
                $students=$dataProvider_student->getData();
                $konte_liy = 1;
		foreach($students as $stud)    //foreach($stud_order_desc_average as $stud)
		   {  
                    $k=0;
                    $average_ = $this->getAverageForAStudent($stud->id, $room, $eval, $acad_sess);
                    
                    ?>
             <tr>
                 <td><?= $konte_liy; ?></td>
                 <td>
                    
                    <?= $stud->first_name; ?>
                     
                 </td>
                 <td>
                     
                    <?= $stud->last_name; ?>
                     
                 </td>
                 <td style="text-align: center">
                     <?php if($average_ < $passing_grade) { ?>
                     <span style="color: red;"><b><?= $average_;  ?></b></span>
                     <?php }else { 
                         ?>
                     
                     <?php 
                         
                         echo $average_; 
                     }
                    ?>
                     
                 </td>
             
    <?php 
                    if($include_discipline==1)
			{		
			//discipline grade
			$period_acad_id =null;
                        $result=EvaluationByYear::model()->searchPeriodName($eval);
                        if(isset($result))
                            {  $result=$result->getData();//return a list of  objects
                                    foreach($result as $r)
                                     {
                                            $period_exam_name= $r->name_period;
                                            $period_acad_id = $r->id;
                                      }
                              }
			                                    
			$grade_discipline = RecordInfraction::model()->getDisciplineGradeByExamPeriod($stud->id, $period_acad_id);
			
                        ?>
         <td style="text-align: center">
             <span style="color: green;">
                 <span class="" ><span><?= $grade_discipline; ?></span>
             </span>
         </td>
             
             
             <?php 
                        
                        }
            while(isset($dataProvider[$k][0]))
                {
	        $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
		if($dataProvider19->getData()==null)
                    { 
                    if($dataProvider[$k][4]!=NULL) //reference_id
			{  $id_course = $dataProvider[$k][4];
														
                             //gade si kou sa gen referans
                            $reference_id = 0; 
                            $useCourseReference=false;
                            $modelCourse = Courses::model()->findByPk($id_course);

                            $reference_id = $modelCourse->reference_id;
			    if($reference_id==null)//sil pa genyen
                            {								
                             //si kou a evalye pou peryod sa
                            $old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$room,$eval);         
                              if($old_subject_evaluated)
                                     {
                                        $id_course = $dataProvider[$k][4];
                                        $careAbout=$this->isOldSubjectEvaluated($dataProvider[$k][4],$room,$eval);						                        
                                      }
                               else
                                     {  
                                        $id_course = $dataProvider[$k][0];
                                        $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);						                       															                       
                                      }
                            }
                             elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
                                { 
                                $dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);
                                     //1) gad sil gen not pou evaluation sa
                                if($dataProvider9->getData()!=null)
                                  { 
                                    $useCourseReference=true;
                                     //si kou a evalye pou peryod sa
                                    $old_subject_evaluated=$this->isOldSubjectEvaluated($reference_id,$room,$eval);
                                    if($old_subject_evaluated)
                                    { 
                                            $id_course = $reference_id;
                                            $careAbout=$old_subject_evaluated;
                                    }
                                    else
                                     { 
                                        $careAbout=$this->isSubjectEvaluated($id_course,$room,$eval);
                                     }

                                        }
                                  elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
                                         {
                                             $careAbout=$this->isSubjectEvaluated($id_course,$room,$eval);
                                         }
                                 }
															
			}
			else
                            {  
                            $id_course = $dataProvider[$k][0];
                             $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                            }
			}
                         else
                            {  
                             $id_course = $dataProvider[$k][0];
                             $careAbout=$this->isSubjectEvaluated($dataProvider[$k][0],$room,$eval);
                            }
			if($careAbout)
			{			
			//calculer grade puis afficher
                        $debase_passingGrade=0;
                        $debase='';
                        $weight='';
                        $id_course=$dataProvider[$k][0];
                        $passing_value=0;
							                             
                        $dataProvider19=Grades::model()->searchByRoom($dataProvider[$k][0],$eval);  
                        if($dataProvider19->getData()==null)
                            {  
                            if($dataProvider[$k][4]!=NULL) //reference_id
                                {  
                                $id_course = $dataProvider[$k][4];
				//gade si kou sa gen referans
                                $reference_id=0; 
                                $useCourseReference=false;
                                $modelCourse = Courses::model()->findByPk($id_course);
                                $reference_id = $modelCourse->reference_id;
																									        
                                if($reference_id==null)//sil pa genyen
                                {								
                                    //si kou a evalye pou peryod sa
                                  $old_subject_evaluated=$this->isOldSubjectEvaluated($id_course,$room,$eval);         
                                  if($old_subject_evaluated)
                                     {
                                        $id_course = $dataProvider[$k][4];						                        
                                      }
                                   else
                                     { 
                                       $id_course = $dataProvider[$k][0];						                       															                       
                                      }

                                  }
                                 elseif( ($reference_id!=null)&&($reference_id!=0) )//sil genyen
                                    { 
                                    $dataProvider9=Grades::model()->searchByRoom($reference_id,$eval);
                                       //1) gad sil gen not pou evaluation sa
                                    if($dataProvider9->getData()!=null)
                                      { 
                                        $useCourseReference=true;
                                         //si kou a evalye pou peryod sa
                                         $id_course = $reference_id;
                                         $old_subject_evaluated=$this->isOldSubjectEvaluated($reference_id,$room,$eval);
                                          if($old_subject_evaluated)
                                            { 
                                              $id_course = $reference_id;
                                            }
                                          else
                                             { 
                                               $id_course = $dataProvider[$k][4];
                                             }

                                            }
                                      elseif($dataProvider9->getData()==null)//2)sil pa genyen, gad si $this->course_id gen not pou evauatuion
                                             {
                                               $id_course = $dataProvider[$k][4];
                                             }

                                    }
                    }
                     else
                        {  
                         $id_course = $dataProvider[$k][0];

                         }
                        }
                     else
                        {  
                         $id_course = $dataProvider[$k][0];
                        }
							                            
                    $resultDebase=Courses::model()->ifCourseDeBase($id_course);
                    if($resultDebase!=null)
                     {
                        foreach($resultDebase as $base)
                         { $weight=$base['weight'];
                           $debase=$base['debase'];
                          }

                       }

                    if($debase==1)
                      {
                         $pass_gra=PassingGrades::model()->getCoursePassingGrade($id_course, $acad_sess);
                          if($pass_gra!=null)
                            $debase_passingGrade=$pass_gra;

                            $passing_value=$debase_passingGrade;

                        }
                     else
                       $passing_value = $dataProvider[$k][2]/2;
$grades=Grades::model()->searchForReportCard($condition,$stud->id,$id_course,$eval); 
    if(isset($grades))
    {
        $r=$grades->getData();//return a list of  objects
        if($r!=null)
        { 
            foreach($r as $grade)
             {
                $estil = 'style="color: green;"'
                ?>
         
         <td class="<?= $grade->id; ?>" style="text-align: center">
            <?php
                  if($grade->grade_value!=NULL){
                        if($grade->validate == 0){
                            $estil = 'style="color: black;"';
                        }elseif($grade->grade_value < $passing_value){
                            $estil = 'style="color: red;"';
                        }
                        elseif($grade->validate==1){
                            $estil = 'style="color: green;"';
                        }
                        ?>
                        <span <?= $estil?>>
                            <span ><?= $grade->grade_value; ?></span></span>
                        </span>
                 <?php 
                        
                    }else{
                        if($grade->validate == 0){
                            $estil = 'style="color: black;"'; 
                        }else{
                            $estil = 'style="color: green"';
                        }
                        
                        ?>
             <span <?= $estil; ?>>
                 N/A
             </span>
             <?php 
                    }
                ?>
             </span>  
         </td>
            
    <?php         

            }
                 }
            else
              { 
                ?>
         <td style="text-align:center;  border-right: 1px solid #E4E9EF">
             <span>N/A</span>
         </td>
             <?php 
                
                }
                $showButton=true;

        }//fin if(isset($grades))

           }//fin careAbout

            $k++;
           // $konte_liy++;

        }//fin while(isset($dataProvider[$k][0]))								  
	
        $konte_liy++;
        ?>
       </tr>
             <?php 
          
                    
        }// fin foreach($students as $stud)
	
        
							  
}// fin if(isset($dataProvider_student))
							  
							  
			            //pou chak elev
			
             ?>
       
         </tbody>
         
     </table>
     
 </div>
 