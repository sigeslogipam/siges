<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>


<?php
/* @var $this ContactInfoController */
/* @var $model ContactInfo */

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));
    
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

  $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
 $template ='';    
  $tit='';    
   if($current_acad==null)
						          $condition = '';
						     else{
						     	   if($acad!=$current_acad->id)
							         $condition = '';
							      else
							         $condition = 'person0.active IN(1, 2) AND ';
						        }


    $from='';
        if(isset($_GET['from']))
	      { if($_GET['from']=='stud')
				{  $dataProvider =$model->searchforStudent($condition,$acad_sess); 
				    $header=Yii::t('app','Student Name');
				    $tit=Yii::t('app','Student');
				    $from='stud';
				    $link_person='"/academic/persons/viewForReport",array("id"=>$data->person0->id,"isstud"=>1,"pg"=>"ci","from"=>"'.$from.'"';
				    
                                     $el = 'stud';
				}     
		     elseif($_GET['from']=='teach')
				 {   $dataProvider = $model->searchforTeacher($condition,$acad_sess);           
				     $header=Yii::t('app','Teacher Name');
				     $tit=Yii::t('app','Teacher');
				    $from='teach';
				     $link_person='"/academic/persons/viewForReport",array("id"=>$data->person0->id,"isstud"=>0,"pg"=>"ci","from"=>"'.$from.'"';
				     
                                      $el = 'teach';
				}     
		     elseif($_GET['from']=='emp')
				   {  $dataProvider= $model->searchforEmployee($condition,$acad_sess);
				   $header=Yii::t('app','Employee Name');
				    $tit=Yii::t('app','Employee');
				    $from='emp';
				   $link_person='"/academic/persons/viewForReport",array("id"=>$data->person0->id,"pg"=>"ci","from"=>"'.$from.'"';
				      
                                       $el = 'emp';
				}     
		     
	      }
	      
	       
				   
?>



		
<div id="dash">
		<div class="span3"><h2>
		    <?php echo Yii::t('app','Manage contact info').' '.$tit; ?>
		    
		 </h2> </div>
     
		   <div class="span3">
        <?php 
               if(!isAchiveMode($acad_sess))
                 {     $template ='{view}{update}{mail}';    
        ?>
              <div class="span4">
              <?php  
			

                 $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';
                           // build the link in Yii standard
                 echo CHtml::link($images,array('contactInfo/create?from='.$from)); 
               ?>
             </div>
       <?php
                 }
      
      ?>       
            
             <div class="span4">
                  <?php

                 $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';
                           // build the link in Yii standard
                    if(isset($_GET['pers'])&&($_GET['pers']!=""))
                       {   
                       	    if(isset($_GET['from']))
				           { if($_GET['from']=='stud')
				                   echo CHtml::link($images,array('/academic/persons/viewForReport?id='.$_GET['pers'].'&pg=lr&isstud=1&from=stud'));
				             elseif($_GET['from']=='teach')
				                 echo CHtml::link($images,array('/academic/persons/viewForReport?id='.$_GET['pers'].'&pg=lr&isstud=0&from=teach'));
				                 elseif($_GET['from']=='emp')
				                     echo CHtml::link($images,array('/academic/persons/viewForReport?id='.$_GET['pers'].'&pg=lr&from=emp')); 
				                     
				           }   

                       	     
                       
                       }
                     else
                       {  
                       	   if(isset($_GET['from']))
				           { if($_GET['from']=='stud')
				                   echo CHtml::link($images,array('/academic/persons/listforreport?isstud=1&from=stud'));
				             elseif($_GET['from']=='teach')
				                 echo CHtml::link($images,array('/academic/persons/listforreport?&isstud=0&from=teach'));
				                 elseif($_GET['from']=='emp')
				                     echo CHtml::link($images,array('/academic/persons/listforreport?from=emp')); 
				                     
				           }   


                       	      
                       	  
                        }
               ?>
             </div>  
      </div>

</div>

<div style="clear:both"></div>

<div class="grid-view">

          
           <div  class="search-form">	

<?php 
      
       $gridWidget  = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'contact-info-grid',
	'summaryText'=>'',
	'dataProvider'=>$dataProvider,
	//'mergeColumns'=>array('person0.FullName','contact_name'),
	'columns'=>array(
		
         
            		 array(
                                    'name' => 'contact_name',
                                    'value'=>'$data->contact_name',
                                    'htmlOptions'=>array('width'=>'250px'),
                                ),
		
    
		 array(
                                    'name' => 'person0.FullName','header'=>$header,
                                    'type' => 'raw',
                                    'value'=>'CHtml::link($data->person0->FullName,Yii::app()->createUrl('.$link_person.')))',
                                    'htmlOptions'=>array('width'=>'250px'),
                                ),
            'contactRelationship.relation_name',
                
		
		'phone',
		
		
		'email',
		
		array(
                                    'name' => 'main_contact',
                                    'value'=>'$data->mainContact',
                                    'htmlOptions'=>array('width'=>'120px'),
                                ),
		array(
			'class'=>'CButtonColumn',
			
			'template'=>$template,
			   'buttons'=>array (
        
         'view'=>array(
            'label'=>'<i class="fa fa-eye"></i>',
            'imageUrl'=>false,
            
            'url'=>'Yii::app()->createUrl("/academic/contactInfo/view?id=$data->id&from='.$from.'")',
            'options'=>array( 'title'=>Yii::t('app','View') ),
              ),
        'update'=>array(
            'label'=>'<i class="fa fa-pencil-square-o"></i>',
            'imageUrl'=>false,
            
            'url'=>'Yii::app()->createUrl("academic/contactInfo/update?id=$data->id&from='.$from.'")',
            'options'=>array( 'title'=>Yii::t('app','Update') ),
              ),
                               
        'mail'=> array(
            'label'=>'<span class="fa fa-envelope"></span>',
             'imageUrl'=>false,
            
            'url'=>'Yii::app()->createUrl("academic/mails/create?stud=$data->id&from='.$from.'&el='.$el.'&mn=std&&de=ci")',
            'options'=>array('title'=>Yii::t('app','Send email' )),
        ),                       

              ),
            
           

		),
	),
)); 

?>
           </div>
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit;?>" },
                  
                    /*{extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                    
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>


</div>