
<?php 
/*
 * © 2010 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>

<?php



$acad_sess=acad_sess();
         
$acad=Yii::app()->session['currentId_academic_year']; 
    
 $tit = Yii::t('app','Inactive people');   
   ?>
   
   
<div style="clear:both;"></div>
<!-- <div class="liste_note">  -->
</br>

         <div class="box-body">
                  <div class="table-responsive">
                    

   <div  style="padding:0px;">			
			
			<!--room / title-->
			<div class="left" style="margin-left:10px;">
			     <label for="Titles"> <?php 
					
					 echo Yii::t('app','Select '); 
					
				
							  if(isset($this->list_id))
							   {
						          echo $form->dropDownList($model,'list_id', $this->loadInactiveTitle(), array('onchange'=> 'submit()','options' => array($this->list_id=>array('selected'=>true)) )); 
					             }
							   else
							      echo $form->dropDownList($model,'list_id',$this->loadInactiveTitle(), array('onchange'=> 'submit()')); 
						
						echo $form->error($model,'list_id'); 
						
					       									   
					   ?>
				</div>
			
     </div>
                              



			
<!-- <div class="principal"> -->
      
      <?php 
	  
     
		   	
		   	
		   	 $this->female_s =0;
		   $this->male_s =0;
		   $this->tot_stud_s =0;
		   
		   
				 //total by gender
			 
	 if($this->list_id == 1)  //Inactive people
		{	$total_title = Yii::t('app','Total');
			$dataProvider_s1= Persons::model()->searchTotalInactivePerson();		
		   }
		 elseif($this->list_id == 2) //Inactive Students
		   {   $total_title = Yii::t('app','Total Students');
		   	  $dataProvider_s1= Persons::model()->searchTotalExStudents_();
		   	
		     }
		   elseif($this->list_id == 3) //Inactive Teachers
		    {   $total_title = Yii::t('app','Total Teachers');
		    	$dataProvider_s1= Persons::model()->searchTotalExTeachers();
		      }
		    elseif($this->list_id == 4) //Inactive Employees
		      {   $total_title = Yii::t('app','Total Employees');
		      	  $dataProvider_s1= Persons::model()->searchTotalExEmployee();
		        }
			 
				  if(isset($dataProvider_s1)&&($dataProvider_s1!=null))														  
					{ $person_s1=$dataProvider_s1->getData();
																		
						foreach($person_s1 as $stud1)
						  {  
						  	
							if($stud1->gender==0)
							  { 
							  	
							  $this->male_s ++;
							  }
							elseif($stud1->gender==1)
							   $this->female_s ++;
					      }
					      
					      $this->tot_stud_s = $this->female_s + $this->male_s;
						  
					 }
					 
					 
echo '<div style="clear:both;"></div><div class="all_gender" style=" float:left; margin-bottom:-10px;margin-top:20px; padding-bottom:0px;">
<div class="total_student">'. $total_title.'<div>'.$this->tot_stud_s.'</div></div>

<div class="gender">'.Yii::t('app','').'<div class="female">'.Yii::t('app','Female').'<br/>'.$this->female_s.'</div><div class="male">'.Yii::t('app','Male').'<br/>'.$this->male_s.'</div></div>

</div><div style="clear:both;"></div>';
  	
?>
<div class="grid-view">

          
           <div  class="search-form">											

			<?php 		
	    
	  
    if($this->list_id == 1)  //Inactive people
		{	
			     
			     $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); // set controller and model for that before
			     $gridWidget=$this->widget('zii.widgets.grid.CGridView', array(
			            'id'=>'persons-archive-grid',
			           'summaryText'=>'',
                                 'showTableOnEmpty'=>'true',
			            'dataProvider'=>$model->searchInactivePerson(),
			            'columns'=>array(
			                   array(
							'name'=>'last_name',
							 'type' => 'raw',
			                 'value'=>'$data->last_name',
			                                    
			                                    ),
			       array(
							'name'=>'first_name',
							 'type' => 'raw',
			                 'value'=>'$data->first_name',
			                                  
							    ),
			
			                    array(
			                        'name'=>'gender',
			                        'value'=>'$data->genders1',
			                        ),
			
			                    array(
			                        'header'=>Yii::t('app','profil'),
			                        'name'=>'profil',
			                        'value'=>'$data->getProfil($data->id)',
			                        ),
			                        
			                    'status',
			
			          /*          array(
			                            'class'=>'CButtonColumn',
			
			                            'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100),array(
			              'onchange'=>"$.fn.yiiGridView.update('persons-archive-grid',{ data:{pageSize: $(this).val() }})", 
			)),
			                    'template'=>'',
			                       'buttons'=>array (
			
			                             'update'=> array(
			                                    'label'=>'<i class="fa fa-pencil-square-o"></i>',
			                                    'imageUrl'=>false,
			                                    
			                                    'url'=>'Yii::app()->createUrl("/academic/persons/update?id=$data->id&pg=la&from1=rpt")',
			                                    'options'=>array( 'title'=>Yii::t('app','Update') ),
			                            ),
			                            'view'=>array(
			                                    'label'=>'<i class="fa fa-eye"></i>',
			                                    'imageUrl'=>false,
			                                    
			                                    'url'=>'Yii::app()->createUrl("/academic/persons/viewForReport?id=$data->id&pg=la&from1=rpt")',
			                                    'options'=>array( 'title'=>Yii::t('app','View') ),
			                            ),
			
			                    ),
			
			                    ),  */
			            ),
			    ));
	
								
		   }
		 elseif($this->list_id == 2) //Inactive Students
		   {
		   	   $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); // set controller and model for that before
  
         $gridWidget= $this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'persons-grid',
			'dataProvider'=>$model->searchExStudents_(),
			'showTableOnEmpty'=>true,
             'summaryText'=>'',
			
			'columns'=>array(
				
				array(
				'name'=>'last_name',
				 'type' => 'raw',
                 'value'=>'CHtml::link($data->last_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"ext","isstud"=>1,"from"=>"rpt")))',
                                    
                                    ),
       array(
				'name'=>'first_name',
				 'type' => 'raw',
                 'value'=>'CHtml::link($data->first_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"ext","isstud"=>1,"from"=>"rpt")))',
                                 
				    ),


				 array(
							'name'=>'gender',
							'value'=>'$data->getGenders1()',
						), 
				 
				
                'status',
                
				/*array(
					'class'=>'CButtonColumn',
					'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100),array(
                                  'onchange'=>"$.fn.yiiGridView.update('persons-grid',{ data:{pageSize: $(this).val() }})",
                    )),
					'template'=>'',
			   'buttons'=>array (
        
         'view'=>array(
            'label'=>'<i class="fa fa-eye"></i>',
            'imageUrl'=>false,
            
            'url'=>'Yii::app()->createUrl("/academic/persons/viewForReport?id=$data->id&isstud=1&pg=ext&from1=rpt")',
            'options'=>array( 'title'=>Yii::t('app','View') ),
        ),
        
    ),
				), */
			),
		   )); 
	
  	 		     }
		   elseif($this->list_id == 3) //Inactive Teachers
		    {
		    	 $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); // set controller and model for that before
 
                        $gridWidget=$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'persons-grid',
			'dataProvider'=>$model->searchExTeachers(),
			'showTableOnEmpty'=>'true',
                            'summaryText'=>'',
			
			'columns'=>array(
				
				array(
				'name'=>'last_name',
				 'type' => 'raw',
                 'value'=>'CHtml::link($data->last_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"lr","isstud"=>0,"tea"=>"yea","pg"=>"ext","from"=>"rpt")))',
                           
                                    ),
       array(
				'name'=>'first_name',
				 'type' => 'raw',
                 'value'=>'CHtml::link($data->first_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"lr","isstud"=>0,"tea"=>"yea","pg"=>"ext","from"=>"rpt")))',
                               
				    ),

				 array(
							'name'=>'gender',
							'value'=>'$data->getGenders1()',
						), 
				array(
							'name'=>Yii::t('app','Working department'),
							'value'=>$model->getWorkedDepartment($model->id),
						), 
				
 
				
				/* array(
					'class'=>'CButtonColumn',
					'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100),array(
                                  'onchange'=>"$.fn.yiiGridView.update('persons-grid',{ data:{pageSize: $(this).val() }})",
                    )),
					'template'=>'',
			   'buttons'=>array (
        
         'view'=>array(
            'label'=>'<i class="fa fa-eye"></i>',
            'imageUrl'=>false,
            
            'url'=>'Yii::app()->createUrl("/academic/persons/viewForReport?id=$data->id&isstud=0&pg=ext&from1=rpt")',
            'options'=>array( 'title'=>Yii::t('app','View') ),
        ),
        
    ),
				),  */
			),
		   )); 
	
 		      }
		    elseif($this->list_id == 4) //Inactive Employees
		      {
		      	  $pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); // set controller and model for that before
 
          $gridWidget=$this->widget('zii.widgets.grid.CGridView', array(
			'id'=>'persons-grid',
			'dataProvider'=>$model->searchExEmployee(),
			'showTableOnEmpty'=>'true',
              'summaryText'=>'',
			
			'columns'=>array(
				
		array(
				'name'=>'last_name',
				 'type' => 'raw',
                 'value'=>'CHtml::link($data->last_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"lr","pg"=>"ext","from"=>"rpt")))',
                      
                                    ),
       array(
				'name'=>'first_name',
				 'type' => 'raw',
                 'value'=>'CHtml::link($data->first_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"lr","pg"=>"ext","from"=>"rpt")))',
                            
				    ),
				 array(
							'name'=>'gender',
							'value'=>'$data->getGenders1()',
						), 
				array(
							'name'=>Yii::t('app','Working department'),
							'value'=>$model->getWorkedDepartment($model->id),
						), 
				
        
			/*	array(
					'class'=>'CButtonColumn',
					'header'=>CHtml::dropDownList('pageSize',$pageSize,array(10=>10,20=>20,50=>50,100=>100),array(
                                  'onchange'=>"$.fn.yiiGridView.update('persons-grid',{ data:{pageSize: $(this).val() }})",
                    )),
					'template'=>'',
			   'buttons'=>array (
        
         'view'=>array(
            'label'=>'<i class="fa fa-eye"></i>',
            'imageUrl'=>false,
            
            'url'=>'Yii::app()->createUrl("/academic/persons/viewForReport?id=$data->id&isstud=0&pg=ext&from1=rpt")',
            'options'=>array( 'title'=>Yii::t('app','View') ),
        ),
        
    ),
				),  */
			),
		   )); 
		
     
		        }

		
			 
					
			
			
			
			 
			    
				
			     ?>

  
   </div>
 <script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:          "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: "<?= $tit;?>" },
                  
                    /*{extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                    
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>     
      
</div>  

                 
                  
                  </div><!-- /.table-responsive -->
                </div><!-- /.box-body -->
                
            


 <div style="clear:both;"></div>
 
