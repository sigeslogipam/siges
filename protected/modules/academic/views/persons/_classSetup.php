
<?php 
/*
 * © 2015 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

?>

<?php

$this->widget('ext.yiiselect2.YiiSelect2',array('target'=>'select',));
 
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

$siges_structure = infoGeneralConfig('siges_structure_session');

  $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
     if($current_acad==null)
						          $condition = '';
						     else{
						     	   if($acad!=$current_acad->id)
							         $condition = '';
							      else
							         $condition = 'p.active IN(1,2) AND ';
						        }

      




?>


<!-- <div class="principal">  -->
    <div class="box-body" >
                  <div class="table-responsive">
                   
                                                   
<div class="left" style="margin-left:10px;  margin-right:20px;">
	         <label for="Levels"> <?php 
					echo Yii::t('app','Level'); 
					 ?>
				</label> 
	
					 <?php 
					 
					$modelLevelPerson = new LevelHasPerson;
						
							if(isset($this->idLevel))
								    echo $form->dropDownList($modelLevelPerson,'level',$this->loadLevel(), array('options' => array($this->idLevel=>array('selected'=>true)),'onchange'=> 'submit()', 'disabled'=>'')); 
								 else
									{ $this->idLevel=0;
									  echo $form->dropDownList($modelLevelPerson,'level',$this->loadLevel(), array('onchange'=> 'submit()', 'disabled'=>'' ));
						             }
						             
					   
						echo $form->error($modelLevelPerson,'level'); 
						
				 $le='';
	    	          	  
	                $this->idLevel=Yii::app()->session['LevelHasPerson_classSetup'];
					if($this->idLevel!=null)
					  $le=$this->idLevel;
                                        
                                        
        $content=$model->searchStudentsForPdfCSL($condition,$le,$acad_sess)->getData();
 if((isset($content))&&($content!=null))
  {  
   
   echo '<div class="right" style="margin-left:20px;  margin-right:10px;">'.CHtml::submitButton(Yii::t('app', 'Create PDF'),array('name'=>'createPdf','class'=>'btn btn-warning')).'</div>';
   }	 
					  ?>
				</div>
    
  <div class="grid-view">




<div  class="search-form">                    
                      
      	<?php 
      
         	      
				  
if($this->menfp==1)
{	             
$gridWidget = $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'end-year-decision-grid',
	'dataProvider'=>$model->searchStudentsForPdfCSL($condition,$le,$acad_sess),
	'showTableOnEmpty'=>true,
    'summaryText'=>' ',
 
	'columns'=>array(
		
		
		array('name'=>'last_name',
			'header'=>Yii::t('app','Student last name'),
                        'type' => 'raw',
			'value'=>'$data->last_name',   //   'CHtml::link($data->last_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"lfc","pi"=>"no","isstud"=>1,"from"=>"stud")))',
			       
                    ),  
                     	
		array('name'=>'first_name',
			'header'=>Yii::t('app','Student first name'),
                        'type' => 'raw',
			'value'=>'$data->first_name',
			       
                    ),
                    
      'mother_first_name',
      
      'sexe',
         
         
         array(     'header'=>Yii::t('app','Birthday'),
		                    'name'=>Yii::t('app','Birthday'),
		                    'value'=>'$data->Birthday_',
		                ),
		                
       
		array('header'=>Yii::t('app','Birth place'),'name'=>'cities0.city_name'),
		
		'identifiant',
		
		'matricule',
		
		
	),
)); 

}
elseif($this->menfp==0)
{

    $gridWidget = $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'end-year-decision-grid',
	'dataProvider'=>$model->searchStudentsForPdfCSL($condition,$le,$acad_sess),
	'showTableOnEmpty'=>true,
        'summaryText'=>' ',
 
	'columns'=>array(
		
		
		array('name'=>'last_name',
			'header'=>Yii::t('app','Student last name'),
                        'type' => 'raw',
			'value'=>'$data->last_name',  //   'CHtml::link($data->last_name,Yii::app()->createUrl("/academic/persons/viewForReport",array("id"=>$data->id,"pg"=>"lfc","pi"=>"no","isstud"=>1,"from"=>"stud")))',
			       
                    ),  
                     	
		array('name'=>'first_name',
			'header'=>Yii::t('app','Student first name'),
                        'type' => 'raw',
			'value'=>'$data->first_name',
			       
                    ),
                    
         'sexe',
         
        
         array(     'header'=>Yii::t('app','Birthday'),
		                    'name'=>Yii::t('app','Birthday'),
		                    'value'=>'$data->Birthday_',
		                ),
		                
        
		array(     'header'=>Yii::t('app','Person liable phone'),
		                    'name'=>Yii::t('app','Person liable phone'),
		                    'value'=>'$data->person_liable_phone',
		                ),		
		'adresse',
		
		
	),
)); 


}

   

  
       $sess_name='';
                      
                      if($siges_structure==1)
                        {  if($this->noSession)
                             {  Yii::app()->session['currentName_academic_session']=null;
                                Yii::app()->session['currentId_academic_session']=null;
                             	$sess_name=' / ';
                             }
                           else
                             $sess_name=' / '.Yii::app()->session['currentName_academic_session'];
                        }
                        
       $acad_name=Yii::app()->session['currentName_academic_year'];
	   $acad_name_ = strtr( $acad_name.$sess_name, pa_daksan() );
	   
	   $level=$this->getLevel($this->idLevel);
	   $level_ = strtr( $level, pa_daksan() );

if($this->menfp==1)
{	   
	   $file_name = strtr("LFC_CSL_MENFP", pa_daksan() ).'_'.$acad_name_.'_'.$level_.'.pdf';
	   
 }
elseif($this->menfp==0)
{
       $file_name = strtr("LFC_CSL", pa_daksan() ).'_'.$acad_name_.'_'.$level_.'.pdf';

}

   
										 
//  if(file_exists(Yii::app()->basePath.'/../documents/lfc_csl/'.$acad_name_.'/'.$file_name))
//    {   $images = '<i class="btn btn-warning">&nbsp;'.Yii::t('app','View PDF').'</i>';	
//    									   // build the link in Yii standard
//	                           
//	                               echo CHtml::link($images, Yii::app()->baseUrl.'/documents/lfc_csl/'.$acad_name_.'/'.$file_name,array( 'target'=>'_blank'));
//						  
//							  // echo '</div>';
//							   
//      }

			    
				
			     ?>
	
                  </div>
    </div>
                                
                  </div><!-- /.table-responsive -->
               
<!-- /.box-body -->
                
              </div>




<script>
   $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  //  { extend: 'copy'},
                  //  {extend: 'csv'},
                    {extend: 'excel', title: 'Students List'},
                   // {extend: 'pdf', title: 'Students List'},

                  /*  {extend: 'print',
                     customize: function (win){
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                    }
                    }
                    */
                ]

            });
            
            // var element = document.getElementsByName("DataTables_Table_0_length");
            //element.classList.remove("select2-offscreen");

        });
</script>