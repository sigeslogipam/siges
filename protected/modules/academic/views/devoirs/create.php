<?php
/*
 * © 2019 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
/* @var $this DevoirsController */
/* @var $model Devoirs */


?>



		<?php  
		       if((Yii::app()->user->profil!='Teacher'))
     			 {	 
		           if(isset($_GET['stud'])&&($_GET['stud']!=""))
                     {
                     	        $this->room_id= Yii::app()->session['Rooms'];
								   
                           $this->evaluation_id= Yii::app()->session['Period'];

                           if($this->success)
                              {  
                                $this->course_id= '';
                              }
                            else
                               $this->course_id= Yii::app()->session['Courses'];
								       
					            
						        
						        
						        
                     }					       
							
     			 }//fen  if((Yii::app()->user->profil!='Teacher'))
                else // Yii::app()->user->profil=='Teacher'
                  {
                  	   $this->room_id= Yii::app()->session['Rooms'];
								   
                           $this->evaluation_id= Yii::app()->session['Period'];

                           if($this->success)
                              {  
                                $this->course_id= '';
                              }
                            else
                               $this->course_id= Yii::app()->session['Courses'];
								   

			
					  
								   
                    }// Yii::app()->user->profil=='Teacher'
                    

		
		
		?>	

<!-- Menu of CRUD  -->

		
<div id="dash">
		<div class="span3"><h2>
		     <?php echo Yii::t('app','Create Homework Grades'); ?> 
		     
		</h2> </div>
		     
		     
      <div class="span3">
            
            

                  <?php



                     $images = '<i class="fa fa-edit">&nbsp;'.Yii::t('app','Update').'</i>';

                               // build the link in Yii standard

                      if($this->use_update)
				        {
				        	 echo '<div class="span4">';
				           echo CHtml::link($images,array('devoirs/update?all=1&from=stud&mn=std')); 
                            echo '</div>';
				        }
                   ?>

              

             <div class="span4">

                      <?php



                     $images = '<i class="fa fa-arrow-left"> &nbsp;'.Yii::t('app','Cancel').'</i>';

                               // build the link in Yii standard

                        echo CHtml::link($images,array('/academic/devoirs/index?from=stud&mn=std')); 
                            $this->back_url='/academic/devoirs/index?from=stud&mn=std';   
                         

                   ?>

                  </div> 
                    
         
           </div>
 </div>


<div style="clear:both"></div>




</br>


<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'devoirs-form',
	
)); 
echo $this->renderPartial('_create', array(
	'model'=>$model,
	'form' =>$form
	)); ?>
    <div class="clear"></div>


<?php $this->endWidget(); ?>

</div>

