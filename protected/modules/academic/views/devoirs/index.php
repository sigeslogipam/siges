<?php
/*
 * © 2019 LOGIPAM services / www.logipam.com siges@logipam.com et contributeurs (voir www.logipam.com)
 * 
 * This file is part of SIGES.

    SIGES is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License.

    SIGES is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with SIGES.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */
/* @var $this DevoirsController */
/* @var $dataProvider CActiveDataProvider */

 ?>



<?php


   
$acad_sess = acad_sess();
$acad=Yii::app()->session['currentId_academic_year']; 

$current_acad_id= 0;
$current_year=currentAcad();
  if($current_year!=null)
	$current_acad_id= $current_year->id;
		
	

$template = '';

  $current_acad=AcademicPeriods::model()->searchCurrentAcademicPeriod(date('Y-m-d'));
 
        if($current_acad==null)
						          $condition = '';
						     else{
						     	   if($acad!=$current_acad->id)
							         $condition = '';
							      else
							         $condition = 'student0.active IN(1, 2) AND ';
						        }
   

$tit = '';

if(Yii::app()->user->profil!='Teacher')
     $tit = Yii::t('app','Homework Grade');
 else
 $tit = Yii::t('app','Homework Grade by Student');

?>
		

<!-- Menu of CRUD  -->

<div id="dash">
		<div class="span3"><h2><?php echo $tit;
                                                
                                                 ?> 
		</h2> </div>
			
      <div class="span3">
           
<?php 
     
     if(!isAchiveMode($acad_sess))
        {    
            if($acad == $current_acad_id)
	     {   
                $template = '{update}{delete}';  
   ?>
           
             <div class="span4">

                  <?php



                     $images = '<i class="fa fa-plus">&nbsp;'.Yii::t('app','Add').'</i>';

                               // build the link in Yii standard

                     echo CHtml::link($images,array('devoirs/create?from=stud&mn=std')); 

                   ?>

              </div>
   
				  
			
			  
			  <div class="span4">

                  <?php



                     $images = '<i class="fa fa-edit">&nbsp;'.Yii::t('app','Update').'</i>';

                               // build the link in Yii standard

                     echo CHtml::link($images,array('devoirs/update?all=1&from=stud&mn=std')); 

                   ?>

              </div>
              
 <?php
           }
		
		}
      
      ?>       

    
              
			  
			  
      </div>          

 </div>




<div style="clear:both"></div>


<div class="grid-view">

          
           <div  class="search-form">



<?php 
        $id_teacher='';   
           	
           	 $pers=User::model()->getPersonByUserId(Yii::app()->user->userid);
				 $pers=$pers->getData();
				 foreach($pers as $p)
				      $id_teacher=$p->id;
				

        
        
        
        

if((Yii::app()->user->profil!='Teacher'))
  {   $dataProvider=$model->search($condition,$acad_sess);
  
       $method = 'search('.$condition.','.$acad_sess.')';

   }
else // Yii::app()->user->profil=='Teacher'
   {
   	  $dataProvider=$model->searchForTeacherUser($condition,$id_teacher,$acad_sess);
   	  
   	  $method = 'searchForTeacherUser('.$condition.','.$id_teacher.','.$acad_sess.')';
   	  
   	 }

 Yii::app()->clientScript->registerScript($method, "
			$('.search-button').click(function(){
				$('.search-form').toggle();
				return false;
				});
			$('.search-form form').submit(function(){
				$.fn.yiiGridView.update('devoirs-grid', {
data: $(this).serialize()
});
				return false;
				});
			");
			
$pageSize=Yii::app()->user->getState('pageSize',Yii::app()->params['defaultPageSize']); // set controller and model for that before
   $gridWidget = $this->widget('groupgridview.GroupGridView', array(
	'id'=>'devoirs-grid',
	'dataProvider'=>$dataProvider,
	'showTableOnEmpty'=>true,
       'summaryText'=>'',
      // 'mergeColumns'=>array('academicPeriod.name_period','student'),
	
	'columns'=>array(
	
               
               array('name'=>'first_name','value'=>'$data->student0->first_name'),
               array('name'=>'last_name','value'=>'$data->student0->last_name'),
                array('name'=>'academicPeriod.name_period','header'=>Yii::t('app','Name Period'),'htmlOptions'=>array('style'=>'vertical-align: top'),'value'=>'$data->academicPeriod->name_period'),    
                    
		
                array('header'=>Yii::t('app','Course name'),'name'=>'course0.courseName',
                    'type' => 'raw','value'=>'CHtml::link($data->course0->courseName,Yii::app()->createUrl("#"))',
                    
                    ),
                
		'grade_value',
                'course0.weight',
                      
        
          array('header'=>Yii::t('app','Com. '),'name'=>'comment',
                    'type' => 'raw','value'=>'CHtml::link(($data->comment == null) ? " " : "<span data-toggle=\"tooltip\" title=\"$data->comment\"><i class=\"fa fa-comment-o\"></i> </span>
",null)',
                ),
                      
          
		
		array(
			'class'=>'CButtonColumn',
			'template'=>$template,
			'buttons'=>array (
        'update'=> array(
            'label'=>'<i class="fa fa-pencil-square-o"></i>',
            'imageUrl'=>false,
            'url'=>'Yii::app()->createUrl("/academic/devoirs/update?id=$data->id&from=stud")',
            'options'=>array( 'title'=>Yii::t('app','Update') ),
        ),
        
        'delete'=>array('label'=>'<span class="fa fa-trash-o"></span>',
                            'imageUrl'=>false,
                            'options'=>array('title'=>Yii::t('app','Delete')),
                                
                            ),

         'view'=>array(
            'label'=>'View',
            
            'url'=>'Yii::app()->createUrl("/academic/devoirs/view?id=$data->id&from=stud")',
            'options'=>array( 'class'=>'icon-search' ),
        ),
        
    ),
			
		),
	),
)); 
  
 
   ?>

 </div>  
<script>
    $(document).ready(function(){
      
            $('.items').DataTable({
                pageLength: 25,
                responsive: true,
                dom: '<"html5buttons"B>lTfgitp',
                 language: {
                    processing:     "<?= Yii::t('app','Processing ...')?>",
                    search: '<i class="fa fa-search"></i>',
                    searchPlaceholder: "<?= Yii::t('app','Search')?>",
                    lengthMenu:    " _MENU_ ",
                    info:           "<?= Yii::t('app','Show element _START_ to _END_ of _TOTAL_ elements');?>",
                    infoEmpty:      "<?= Yii::t('app','Show element 0 of 0 on 0 element'); ?>",
                    infoFiltered:   "<?= Yii::t('app','filter of _MAX_ elements in total');?>",
                    infoPostFix:    "",
                    loadingRecords: "<?= Yii::t('app','Loading...'); ?>",
                    zeroRecords:    "<?= Yii::t('app','No element to show')?>",
                    emptyTable:     "<?= Yii::t('app','No data to show')?>",

                    paginate: {
                        first:       "<?= Yii::t('app','First'); ?>",
                        previous:    "<?= Yii::t('app','Previous'); ?>",
                        next:        "<?= Yii::t('app','Next'); ?>",
                        last:        "<?= Yii::t('app','Last'); ?>"
                    },
                    aria: {
                       sortAscending:  ": activer pour trier la colonne par ordre croissant",
                       sortDescending: ": activer pour trier la colonne par ordre décroissant"
                    },
                },
                buttons: [
                  
                    {extend: 'excel', title: "<?= $tit ?>"},
                  
                 
                ]

            });
            

        });
</script>

 </div>
 
 