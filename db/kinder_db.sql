/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Nov 25, 2018
 */

CREATE TABLE `kinder_period` ( `id` INT NOT NULL AUTO_INCREMENT , `period_name` VARCHAR(32) NOT NULL , `academic_year` INT NOT NULL , `date_start` DATE NOT NULL , `date_end` DATE NOT NULL , `is_last_period` BOOLEAN NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`period_name`), INDEX (`academic_year`)) ENGINE = InnoDB;
ALTER TABLE `kinder_period` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `kinder_mention` ( `id` INT NOT NULL , `mention_name` VARCHAR(64) NOT NULL , `mention_short_name` VARCHAR(32) NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), UNIQUE (`mention_name`), UNIQUE (`mention_short_name`)) ENGINE = InnoDB;

CREATE TABLE `kinder_cat_mention` ( `id` INT NOT NULL , `cat_name` VARCHAR(128) NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
RENAME TABLE `kinder_cat_mention` TO `kinder_cat_concept`;
ALTER TABLE `kinder_cat_concept` ADD `cat_special` BOOLEAN NULL AFTER `cat_name`;

CREATE TABLE `kinder_concept` ( `id` INT NOT NULL , `concept_name` TEXT NOT NULL , `category` INT NOT NULL , `level` INT NOT NULL , `academic_year` INT NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`category`), INDEX (`level`), INDEX (`academic_year`)) ENGINE = InnoDB;
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`category`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_concept` ADD `teacher` INT NOT NULL AFTER `academic_year`, ADD INDEX (`teacher`);
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`teacher`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `kinder_evaluation` ( `id` INT NOT NULL AUTO_INCREMENT , `student` INT NOT NULL , `concept` INT NOT NULL , `mention` INT NOT NULL , `free_comments` TEXT NULL , `from_special_cat` INT NULL , `academic_year` INT NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`student`), INDEX (`concept`), INDEX (`mention`), INDEX (`from_special_cat`), INDEX (`academic_year`)) ENGINE = InnoDB;
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`student`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`concept`) REFERENCES `kinder_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`from_special_cat`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `kinder_mention` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE kinder_concept DROP FOREIGN KEY kinder_concept_ibfk_2;
ALTER TABLE kinder_evaluation DROP FOREIGN KEY kinder_evaluation_ibfk_4;
ALTER TABLE `kinder_cat_concept` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`category`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`from_special_cat`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE kinder_evaluation DROP FOREIGN KEY kinder_evaluation_ibfk_2;
ALTER TABLE `kinder_concept` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`concept`) REFERENCES `kinder_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`level`) REFERENCES `levels`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `kinder_evaluation` ADD `period` INT NOT NULL AFTER `academic_year`, ADD INDEX (`period`);
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`period`) REFERENCES `kinder_period`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`mention`) REFERENCES `kinder_mention`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

