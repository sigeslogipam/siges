-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 17, 2019 at 04:21 PM
-- Server version: 5.6.16-1~exp1
-- PHP Version: 5.6.40-7+ubuntu16.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siges_demo`
--

-- --------------------------------------------------------

--
-- Table structure for table `academicperiods`
--

CREATE TABLE `academicperiods` (
  `id` int(11) NOT NULL,
  `name_period` varchar(45) NOT NULL,
  `weight` double DEFAULT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `is_year` tinyint(1) DEFAULT NULL,
  `previous_academic_year` int(11) NOT NULL COMMENT 'Annee academique precedente',
  `year` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `academicperiods`
--

INSERT INTO `academicperiods` (`id`, `name_period`, `weight`, `checked`, `date_start`, `date_end`, `is_year`, `previous_academic_year`, `year`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, '2018-2019', NULL, 0, '2018-07-16', '2019-08-09', 1, 0, 1, '2019-07-01 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(2, 'Contrôle I', NULL, 0, '2019-08-01', '2019-08-19', 0, 0, 1, '2019-07-01 00:00:00', '2019-08-07 00:00:00', 'admin', 'admin'),
(3, 'Contrôle 2', NULL, 0, '2019-08-09', '2019-08-26', 0, 0, 1, '2019-07-01 00:00:00', '2019-08-07 00:00:00', 'admin', 'admin'),
(4, 'Contrôle 3', NULL, 0, '2019-04-01', '2019-08-15', 0, 0, 1, '2019-07-01 00:00:00', '2019-08-07 00:00:00', 'admin', 'admin'),
(6, '2019-2020', NULL, 0, '2019-08-10', '2020-08-09', 1, 1, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(7, 'Contrôle 1', NULL, 0, '2019-08-10', '2019-12-31', 0, 0, 6, '2019-08-10 00:00:00', '2019-08-31 00:00:00', 'SIGES', 'LOGIPAM'),
(8, 'Contrôle 2', NULL, 0, '2020-08-09', '2020-09-30', 0, 0, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(9, 'Contrôle 3', NULL, 0, '2020-04-01', '2020-08-31', 0, 0, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `accounting`
--

CREATE TABLE `accounting` (
  `id` int(11) NOT NULL,
  `old_balance` double NOT NULL,
  `expenses` double NOT NULL,
  `incomes` double NOT NULL,
  `new_balance` double NOT NULL,
  `month` int(3) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `id` int(11) NOT NULL,
  `action_id` varchar(64) NOT NULL,
  `action_name` varchar(64) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `module_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES
(1, 'index', 'Lister les utilisateurs', 'User', 5, NULL, NULL, NULL, NULL),
(2, 'create', 'Creation d\'utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(3, 'update', 'Mise a jour d\'un utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(4, 'delete', 'Suppression d\'un utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(5, 'view', 'Affichage d\'un utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(6, 'changePassword', 'Modification du mot de passe', 'User', 5, NULL, NULL, NULL, NULL),
(7, 'updateUser', 'Mise a jour d\'un utilisateur en mode vue', 'User', 5, NULL, NULL, NULL, NULL),
(8, 'disableusers', 'Lister les utilisateurs inactifs', 'User', 5, NULL, NULL, NULL, NULL),
(9, 'listForReport', 'Liste personne pour rapport', 'Persons', 9, NULL, NULL, NULL, NULL),
(10, 'viewForReport', 'Vue personne pour rapport', 'Persons', 9, NULL, NULL, NULL, NULL),
(11, 'list', 'Liste eleve ', 'Persons', 9, NULL, NULL, NULL, NULL),
(12, 'update', 'mise a jour d\'une personne', 'Persons', 9, NULL, NULL, NULL, NULL),
(13, 'roomAffectation', 'Affecter des eleves a une salle', 'Persons', 9, NULL, NULL, NULL, NULL),
(14, 'create', 'Ajouter une nouvelle personne', 'Persons', 9, NULL, NULL, NULL, NULL),
(15, 'listArchive', 'Liste des personnes inactifs ', 'Persons', 9, NULL, NULL, NULL, NULL),
(16, 'exTeachers', 'Liste des anciens professeurs', 'Persons', 9, NULL, NULL, NULL, NULL),
(17, 'exEmployees', 'Liste des anciens employes', 'Persons', 9, NULL, NULL, NULL, NULL),
(18, 'exStudents', 'Liste des anciens eleves', 'Persons', 9, NULL, NULL, NULL, NULL),
(19, 'delete', 'Suppression d\'une personne', 'Persons', 9, NULL, NULL, NULL, NULL),
(20, 'index', 'Liste des eleves ayant des infos additinnelles', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(21, 'create', 'Creation des infos additinnelles pour un eleve', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(22, 'update', 'Mise a jour des infos additinnelles pour eleve', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(23, 'view', 'Affichage des infos additinnelles pour eleve', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(24, 'delete', 'Suppression d\'info additionnelle', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(25, 'index', 'Liste des employes ayant des infos additinnelles', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(26, 'create', 'Creation des infos additionnelles pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(27, 'update', 'Mis a jour des infos additinnelles pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(28, 'view', 'Affichage des infos additinnelles pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(29, 'delete', 'Suppression d\'info additionnelle pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(30, 'index', 'Liste de contact', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(31, 'create', 'Creation de contact pour chaque personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(32, 'update', 'mise a jour de contact pour une personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(33, 'view', 'Affichage de contact pour une personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(34, 'delete', 'Suppression de contact pour une personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(35, 'index', 'Liste des departements ayant des employes', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(36, 'create', 'Ajouter un employe dans un departement', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(37, 'update', 'Mise a jour d\'un employe dans un departement', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(38, 'view', 'Affichage d\'un employe dans un departement ', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(39, 'delete', 'Suppression d\'un employe dans un departement', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(40, 'index', 'Liste de notes de tous les eleves', 'Grades', 9, NULL, NULL, NULL, NULL),
(41, 'create', 'Ajouter une nouvelle note pour un eleve', 'Grades', 9, NULL, NULL, NULL, NULL),
(42, 'update', 'Mise a jour d\'une note pour un eleve', 'Grades', 9, NULL, NULL, NULL, NULL),
(43, 'view', 'Affichage d\'une note pour un eleve', 'Grades', 9, NULL, NULL, NULL, NULL),
(44, 'delete', 'Suppression d\'une note', 'Grades', 9, NULL, NULL, NULL, NULL),
(45, 'listByRoom', 'Affichage des notes par salle', 'Grades', 9, NULL, NULL, NULL, NULL),
(46, 'balance', 'Liste des eleves ayant une balance sup. a zero', 'Balance', 8, NULL, NULL, NULL, NULL),
(47, 'view', 'Affichage de balance pour un eleve', 'Balance', 8, NULL, NULL, NULL, NULL),
(48, 'create', 'Creation de balance pour un eleve', 'Balance', 8, NULL, NULL, NULL, NULL),
(49, 'update', 'Mise a jour d\'une balance pour un eleve', 'Balance', 8, NULL, NULL, NULL, NULL),
(50, 'index', 'Liste des transactions des eleves avec l\'economat', 'Billings', 8, NULL, NULL, NULL, NULL),
(51, 'create', 'Ajouter d\'une nouvelle transaction pour un eleve', 'Billings', 8, NULL, NULL, NULL, NULL),
(52, 'update', 'Mise a jour d\'une transaction pour un eleve', 'Billings', 8, NULL, NULL, NULL, NULL),
(53, 'view', 'Affichage d\'une transaction pour un eleve', 'Billings', 8, NULL, NULL, NULL, NULL),
(54, 'delete', 'Suppression d\'une transaction', 'Billings', 8, NULL, NULL, NULL, NULL),
(55, 'generalReport', 'Affichage d\'un rapport general', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(56, 'create', 'Creation de bulletin pour une salle', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(57, 'report', 'Affichage d\'un bulletin d\'un eleve', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(58, 'admitted', 'Liste des admis par salle', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(59, 'endYearDecision', 'Prise de decision de fin d\'annee', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(60, 'index', 'Liste des cours de l\'ecole', 'Courses', 7, NULL, NULL, NULL, NULL),
(61, 'create', 'Creation d\'un nouveau cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(62, 'update', 'Mise a jour d\'un cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(63, 'view', 'Affichage d\'un cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(64, 'viewForTeacher', 'Liste des cours de l\'ecole pour professeur', 'Courses', 7, NULL, NULL, NULL, NULL),
(65, 'delete', 'Suppression d\'un cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(66, 'index', 'Liste des matieres enseignees ', 'Subjects', 7, NULL, NULL, NULL, NULL),
(67, 'create', 'Ajouter une nouvelle matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(68, 'update', 'Mise a jour d\'une matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(69, 'view', 'Affichage d\'une matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(70, 'delete', 'Suppression d\'une matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(71, 'index', 'Liste des evaluations pour chaque periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(72, 'create', 'Creation d\'une nouvelle evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(73, 'update', 'Mise a jour d\'une evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(74, 'view', 'Affichage d\'une evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(75, 'delete', 'Suppression d\'une evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(76, 'index', 'Liste des heures de cours pour une salle', 'Schedules', 7, NULL, NULL, NULL, NULL),
(77, 'create', 'Ajouter d\'une heure de cours pour une salle', 'Schedules', 7, NULL, NULL, NULL, NULL),
(78, 'update', 'Mise a jour d\'une heure de cours', 'Schedules', 7, NULL, NULL, NULL, NULL),
(79, 'view', 'Affichage d\'une heure de cours pour une salle', 'Schedules', 7, NULL, NULL, NULL, NULL),
(80, 'viewForTeacher', 'Liste des heures de cours pour professeur', 'Schedules', 7, NULL, NULL, NULL, NULL),
(81, 'viewForUpdate', 'Liste des heures de cours pret a etre modifier', 'Schedules', 7, NULL, NULL, NULL, NULL),
(82, 'delete', 'Suppression d\'une heure de cours', 'Schedules', 7, NULL, NULL, NULL, NULL),
(83, 'index', 'Liste des periodes academiques', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(84, 'create', 'Creation d\'une nouvelle periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(85, 'update', 'Mise a jour d\'une periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(86, 'view', 'Affichage d\'une periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(87, 'delete', 'Suppression d\'une periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(88, 'index', 'Liste des departements de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(89, 'create', 'Creation d\'un nouveau departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(90, 'update', 'Mise a jour d\'un departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(91, 'view', 'Affichage d\'un departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(92, 'delete', 'Suppression d\'un departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(93, 'index', 'Liste des devises acceptees a l\'ecole', 'Devises', 1, NULL, NULL, NULL, NULL),
(94, 'create', 'Ajouter une nouvelle devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(95, 'update', 'Mise a jour d\'une devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(96, 'view', 'Affichage d\'une devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(97, 'delete', 'Suppression d\'une devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(98, 'index', 'Liste des evaluations de l\'ecole', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(99, 'create', 'Ajouter une nouvelle evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(100, 'update', 'Mise a jour d\'une evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(101, 'view', 'Affichage d\'une evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(102, 'delete', 'Suppression d\'une evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(103, 'index', 'Liste des frais exigees par l\'ecole', 'Fees', 1, NULL, NULL, NULL, NULL),
(104, 'create', 'Ajouter un nouveau frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(105, 'update', 'Mise a jour d\'un frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(106, 'view', 'Affichage d\'un frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(107, 'delete', 'Suppression d\'un frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(108, 'index', 'Liste des domaines d\'etude des employes', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(109, 'create', 'Ajouter un nouveau domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(110, 'update', 'Mise a jour d\'un domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(111, 'view', 'Affichage d\'un domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(112, 'delete', 'Suppression d\'un domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(113, 'index', 'Liste des champs de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(114, 'admin', 'Liste des champs de configuration generale Pret a modifier', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(115, 'create', 'Creation d\'un nouveau champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(116, 'update', 'Mise a jour d\'un champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(117, 'view', 'Affichage d\'un champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(118, 'delete', 'Suppression d\'un champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(119, 'index', 'Liste des statuts d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(120, 'create', 'Creation d\'un nouveau statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(121, 'update', 'Mise a jour d\'un statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(122, 'view', 'Affichage d\'un statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(123, 'delete', 'Suppression d\'un statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(124, 'index', 'Liste des differents niveaux d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(125, 'create', 'Creation d\'un nouveau niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(126, 'update', 'Mise a jour d\'un niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(127, 'view', 'Affichage d\'un niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(128, 'delete', 'Suppression d\'un niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(129, 'index', 'Liste des notes de passage de l\'ecole', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(130, 'create', 'Creation d\'une nouvelle note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(131, 'update', 'Mise a jour d\'une note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(132, 'view', 'Affichage d\'une note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(133, 'delete', 'Suppression d\'une note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(134, 'index', 'Liste des methodes de paiement exigees par l\'ecole', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(135, 'create', 'Creation d\'une nouvelle methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(136, 'update', 'Mise a jour d\'une methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(137, 'view', 'Affichage d\'une methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(138, 'delete', 'Suppression d\'une methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(139, 'index', 'Liste des differents titres de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(140, 'create', 'Creation d\'un nouveau titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(141, 'update', 'Mise a jour d\'un titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(142, 'view', 'Affichage d\'un titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(143, 'delete', 'Suppression d\'un titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(144, 'index', 'Liste de relations eleve et responsable', 'Relations', 1, NULL, NULL, NULL, NULL),
(145, 'create', 'Creation d\'un nouveau titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(146, 'update', 'Mise a jour d\'un titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(147, 'view', 'Affichage d\'un titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(148, 'delete', 'Suppression d\'un titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(149, 'index', 'Liste de toutes les salles de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(150, 'create', 'Creation d\'une nouvelle salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(151, 'update', 'Mise a jour d\'une salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(152, 'view', 'Affichage d\'une salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(153, 'delete', 'Suppression d\'une salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(154, 'index', 'Liste de toutes les sections de l\'ecole', 'Sections', 1, NULL, NULL, NULL, NULL),
(155, 'create', 'Creation d\'une nouvelle section', 'Sections', 1, NULL, NULL, NULL, NULL),
(156, 'update', 'Mise a jour d\'une section', 'Sections', 1, NULL, NULL, NULL, NULL),
(157, 'view', 'Affichage d\'une section', 'Sections', 1, NULL, NULL, NULL, NULL),
(158, 'delete', 'Suppression d\'une section', 'Sections', 1, NULL, NULL, NULL, NULL),
(159, 'index', 'Liste de toutes les vacations de l\'ecole', 'Shifts', 1, NULL, NULL, NULL, NULL),
(160, 'create', 'Creation d\'une nouvelle vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(161, 'update', 'Mise a jour d\'une vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(162, 'view', 'Affichage d\'une vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(163, 'delete', 'Suppression d\'une vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(164, 'index', 'Liste des positions administratives', 'Titles', 1, NULL, NULL, NULL, NULL),
(165, 'create', 'Creation d\'une nouvelle position', 'Titles', 1, NULL, NULL, NULL, NULL),
(166, 'update', 'Mise a jour d\'une position', 'Titles', 1, NULL, NULL, NULL, NULL),
(167, 'view', 'Affichage d\'une position', 'Titles', 1, NULL, NULL, NULL, NULL),
(168, 'delete', 'Suppression d\'une position', 'Titles', 1, NULL, NULL, NULL, NULL),
(169, 'index', 'Liste de toutes les modules du systeme', 'Modules', 5, NULL, NULL, NULL, NULL),
(170, 'create', 'Creation d\'une nouvelle module du systeme', 'Modules', 5, NULL, NULL, NULL, NULL),
(171, 'update', 'Mise a jour d\'une module', 'Modules', 5, NULL, NULL, NULL, NULL),
(172, 'view', 'Affichage d\'une module', 'Modules', 5, NULL, NULL, NULL, NULL),
(173, 'delete', 'Suppression d\'une module', 'Modules', 5, NULL, NULL, NULL, NULL),
(174, 'index', 'Liste de tous les groupes utilisateur du systeme', 'Groups', 5, NULL, NULL, NULL, NULL),
(175, 'create', 'Creation d\'un nouveau groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(176, 'update', 'Mise a jour d\'un groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(177, 'view', 'Affichage d\'un groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(178, 'delete', 'Suppression d\'un groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(179, 'index', 'Liste de toutes les actions autorisees du systeme', 'Actions', 5, NULL, NULL, NULL, NULL),
(180, 'create', 'Creation d\'une nouvelle action', 'Actions', 5, NULL, NULL, NULL, NULL),
(181, 'update', 'Mise a jour d\'une action', 'Actions', 5, NULL, NULL, NULL, NULL),
(182, 'view', 'Affichage d\'une action', 'Actions', 5, NULL, NULL, NULL, NULL),
(183, 'delete', 'Suppression d\'une action', 'Actions', 5, NULL, NULL, NULL, NULL),
(184, 'validatePublish', 'Validation et Publication des notes', 'Grades', 9, NULL, NULL, NULL, NULL),
(185, 'index', 'Liste des periodes academiques pour INVITE', 'Academicperiods', 10, NULL, NULL, NULL, NULL),
(186, 'balance', 'Liste des eleves ayant une balance sup. a zero  pour INVITE', 'Balance', 10, NULL, NULL, NULL, NULL),
(187, 'index', 'Liste des transactions des eleves avec l\'economat  pour INVITE', 'Billings', 10, NULL, NULL, NULL, NULL),
(188, 'view', 'Affichage des info personnelles parent', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(189, 'update', 'Mise a jour des infos personnelles parent', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(190, 'index', 'Liste des cours que suit l\'eleve', 'Courses', 10, NULL, NULL, NULL, NULL),
(191, 'index', 'Liste des evaluations pour chaque periode  pour INVITE', 'Evaluationbyyear', 10, NULL, NULL, NULL, NULL),
(192, 'index', 'Liste des frais exigees par l\'ecole  pour INVITE', 'Fees', 10, NULL, NULL, NULL, NULL),
(193, 'index', 'Liste des notes (publiees) de l\'eleve', 'Grades', 10, NULL, NULL, NULL, NULL),
(194, 'index', 'Liste des notes de passage de l\'ecole  pour INVITE', 'Passinggrades', 10, NULL, NULL, NULL, NULL),
(195, 'index', 'Liste des methodes de paiement exigees par l\'ecole  pour INVITE', 'Paymentmethod', 10, NULL, NULL, NULL, NULL),
(196, 'report', 'Affichage du bulletin de l\'eleve  pour INVITE', 'Reportcard', 10, NULL, NULL, NULL, NULL),
(197, 'index', 'Liste des heures de cours de l\'eleve  pour INVITE', 'Schedules', 10, NULL, NULL, NULL, NULL),
(198, 'view', 'Affichage des info personnelles eleve', 'StudentOtherInfo', 10, NULL, NULL, NULL, NULL),
(200, 'index', 'Liste des matieres enseignees,  pour INVITE', 'Subjects', 10, NULL, NULL, NULL, NULL),
(201, 'viewcontact', 'Vue des contacts de l\'utilisateur', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(202, 'updateMyContacts', 'Mise a jour de contacts par l\'utilisateur', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(203, 'viewForUpdate', 'Modifier info personnelle utilisateur', 'Persons', 9, NULL, NULL, NULL, NULL),
(204, 'updateMyInfo', 'Mise a jour des infos personnelles utilisateur systeme', 'Persons', 9, NULL, NULL, NULL, NULL),
(205, 'updateParent', 'Mise a jour des infos personnelles d\'un parent', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(206, 'viewForUpdate', 'Mise a jour des infos personnelles d\'un eleve ....', 'Persons', 10, NULL, NULL, NULL, NULL),
(207, 'updateMyInfo', 'Mise a jour des infos personnelles utilisateur eleve', 'Persons', 10, NULL, NULL, NULL, NULL),
(208, 'index', 'Lister emails', 'Mails', 9, NULL, NULL, NULL, NULL),
(209, 'sendEmail', 'Envoyer Email', 'Persons', 9, NULL, NULL, NULL, NULL),
(210, 'index', 'Affiche le calendrier', 'Calendar', 7, NULL, NULL, NULL, NULL),
(211, 'create', 'Ajouter évenements', 'Calendar', 7, NULL, NULL, NULL, NULL),
(212, 'update', 'Modifier un évenement', 'Calendar', 7, NULL, NULL, NULL, NULL),
(213, 'delete', 'Supprimer un évenement', 'Calendar', 7, NULL, NULL, NULL, NULL),
(214, 'view', 'Voir un évenement en mode admin', 'Calendar', 7, NULL, NULL, NULL, NULL),
(215, 'viewForIndex', 'Voir un évenement en mode utilisateur', 'Calendar', 7, NULL, NULL, NULL, NULL),
(216, 'calendarEvents', 'Evenement du calendrier', 'Calendar', 7, NULL, NULL, NULL, NULL),
(217, 'calendarEvents', 'Evenement du calendrier pour invites', 'Calendar', 10, NULL, NULL, NULL, NULL),
(218, 'viewForIndex', 'afficher les evenements en mod utili', 'Calendar', 10, NULL, NULL, NULL, NULL),
(219, 'viewOnlineUsers', 'Voir les utilisateurs connectés', 'User', 5, NULL, NULL, NULL, NULL),
(220, 'viewDecision', 'Voir la liste de decision finale', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(221, 'classSetup', 'Formation des classes', 'Persons', 9, NULL, NULL, NULL, NULL),
(222, 'create', 'Ajouter une nouvelle configuration payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(223, 'update', 'Modifier  configuration payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(224, 'index', 'Affichage des configurations payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(225, 'view', 'Voir configuration payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(226, 'paverage', 'Palmares des moyennes', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(227, 'admission', 'Admission des postulants', 'Persons', 9, NULL, NULL, NULL, NULL),
(228, 'viewListAdmission', 'Voir la liste d\'inscription', 'Postulant', 9, NULL, NULL, NULL, NULL),
(229, 'levelRoomAffectation', 'Affectation a un niveau et/ou a une salle', 'Persons', 9, NULL, NULL, NULL, NULL),
(230, 'viewAdmissionDetail', 'Afficher toutes les info d\'un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(231, 'index', 'Liste des devoirs soumis par les professeurs', 'Homework', 9, NULL, NULL, NULL, NULL),
(232, 'create', 'Ajouter un devoir', 'Homework', 9, NULL, NULL, NULL, NULL),
(233, 'update', 'Modifier un devoir soumis par les professeurs', 'Homework', 9, NULL, NULL, NULL, NULL),
(234, 'view', '	Afficher les info d\'un devoir soumis par un professeur', 'Homework', 9, NULL, NULL, NULL, NULL),
(235, 'index', 'Lister les devoirs soumis par des professeurs', 'Homework', 10, NULL, NULL, NULL, NULL),
(236, 'view', 'Afficher les devoirs soumis par des professeurs', 'Homework', 10, NULL, NULL, NULL, NULL),
(237, 'index', 'Lister les devoirs soumis par des professeurs', 'homeworkSubmission', 10, NULL, NULL, NULL, NULL),
(238, 'create', 'Soumettre un devoir', 'homeworkSubmission', 10, NULL, NULL, NULL, NULL),
(239, 'generalReport', 'Affichage d\'un rapport general pour INVITE', 'Reportcard', 10, NULL, NULL, NULL, NULL),
(240, 'index', 'Liste type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(241, 'create', 'Créer type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(242, 'view', 'Voir type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(243, 'update', 'Modifier type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(244, 'delete', 'Supprimer type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(245, 'index', 'Liste des enregistrements d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(246, 'create', 'Créer enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(247, 'update', 'Modifier enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(248, 'delete', 'Supprimer enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(249, 'view', 'Voir enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(250, 'index', 'Lister enregistrement présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(251, 'view', 'Voir une présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(252, 'update', 'Modifier une présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(253, 'recordPresence', 'Enregistrer présence élèves par salle', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(254, 'admin', 'Rapport présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(255, 'create', 'Prendre présence pour un élève', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(256, 'delete', 'Supprimer Présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(257, 'index', 'Liste des avances sur salaire effectues', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(258, 'disableStudents', 'Rendre des élèves inactifs', 'Persons', 9, NULL, NULL, NULL, NULL),
(259, 'create', 'Ajouter des avances sur salaire', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(260, 'update', 'Modifier avance sur salaire', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(261, 'index', 'Liste des devoirs soumis par les élèves', 'homeworkSubmission', 9, NULL, NULL, NULL, NULL),
(262, 'index', 'Liste des payroll effectues', 'Payroll', 8, NULL, NULL, NULL, NULL),
(263, 'create', 'Ajouter un payroll', 'Payroll', 8, NULL, NULL, NULL, NULL),
(264, 'update', 'Modifier un payroll', 'Payroll', 8, NULL, NULL, NULL, NULL),
(265, 'delete', 'Supprimer un payroll', 'Payroll', 8, NULL, NULL, NULL, NULL),
(266, 'view', 'Afficher payroll en mode vue', 'Payroll', 8, NULL, NULL, NULL, NULL),
(267, 'index', 'Liste des obligations a payer', 'Taxes', 8, NULL, NULL, NULL, NULL),
(268, 'create', 'Ajouter une obligation', 'Taxes', 8, NULL, NULL, NULL, NULL),
(269, 'update', 'Modifier une obligation', 'Taxes', 8, NULL, NULL, NULL, NULL),
(270, 'delete', 'Supprimer une obligation', 'Taxes', 8, NULL, NULL, NULL, NULL),
(271, 'view', 'Afficher une obligation en mode vue', 'Taxes', 8, NULL, NULL, NULL, NULL),
(272, 'index', 'Liste de description des autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(273, 'create', 'Ajouter description autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(274, 'update', 'Modifier description autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(275, 'delete', 'Supprimer description autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(276, 'index', 'Liste des autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(277, 'create', 'Ajouter autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(278, 'update', 'Modifier autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(279, 'delete', 'Supprimer autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(280, 'view', 'Afficher autres rentrées en mode vue', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(281, 'view', 'Afficher un pret en mode vue', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(282, 'admissionSearch', 'Recherche d\'un postulant dans la liste des anciens eleves ', 'Persons', 9, NULL, NULL, NULL, NULL),
(283, 'view', 'Afficher un devoir soumis (INVITE)', 'homeworkSubmission', 10, NULL, NULL, NULL, NULL),
(284, 'index', 'Lister les articles du portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(285, 'create', 'Ajouter un article au portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(286, 'update', 'Modifier un article du portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(287, 'delete', 'Supprimer un article du portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(288, 'view', 'Voir discipline pour un élève', 'RecordInfraction', 10, NULL, NULL, NULL, NULL),
(289, 'viewParent', 'Vue parent de la discipline', 'RecordInfraction', 10, NULL, NULL, NULL, NULL),
(290, 'config', 'Configuration ', 'Billings', 8, NULL, NULL, NULL, NULL),
(291, 'disciplineReport', 'Rapport sur la conduite de l\'eleve', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(292, 'index', 'Liste des partenaires', 'Partners', 1, NULL, NULL, NULL, NULL),
(293, 'create', 'Ajouter un nouveau partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(294, 'update', 'Modifier un partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(295, 'delete', 'Supprimer un partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(296, 'view', 'Voir plus de details sur un partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(297, 'index', 'Liste des boursiers', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(298, 'create', 'Ajouter un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(299, 'update', 'Modifier un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(300, 'delete', 'Supprimer un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(301, 'view', 'Voir plus de details sur un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(302, 'index', 'Liste des libelles depenses', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(303, 'create', 'Ajouter libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(304, 'update', 'Modifier libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(305, 'delete', 'Supprimer libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(306, 'index', 'Liste des depenses', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(307, 'create', 'Ajouter une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(308, 'update', 'Modifier une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(309, 'delete', 'Supprimer une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(310, 'view', 'Voir plus de details d\'une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(311, 'view', 'Voir plus de details d\'un libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(312, 'delete', 'Supprimer un parametre payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(313, 'receipt', 'Produire fiche de paie', 'Payroll', 8, NULL, NULL, NULL, NULL),
(314, 'paymentReceipt', 'Recu de paiement', 'Billings', 8, NULL, NULL, NULL, NULL),
(315, 'create', 'Ajouter libelle frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(316, 'update', 'Modifier libelle frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(317, 'delete', 'Supprimer libelle frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(318, 'index', 'Liste des libelles frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(319, 'uploadLogo', 'Upload logo', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(320, 'etatF', 'Voir les etats financiers', 'Reports', 8, NULL, NULL, NULL, NULL),
(321, 'taxreport', 'Rapport taxe', 'Reports', 8, NULL, NULL, NULL, NULL),
(322, 'create', 'Ajouter une nouvelle vente', 'Sellings', 8, NULL, NULL, NULL, NULL),
(323, 'return', 'Liste des produits retournés', 'Sellings', 8, NULL, NULL, NULL, NULL),
(324, 'returnitem', 'Retourner un produit', 'Sellings', 8, NULL, NULL, NULL, NULL),
(325, 'admin', 'Afficher le rapport de vente', 'Sellings', 8, NULL, NULL, NULL, NULL),
(326, 'emptyCart', 'Vider le panier de vente', 'Sellings', 8, NULL, NULL, NULL, NULL),
(327, 'index', 'Liste des produits en dépot', 'Products', 8, NULL, NULL, NULL, NULL),
(328, 'create', 'Ajouter un produit', 'Stocks', 8, NULL, NULL, NULL, NULL),
(329, 'update', 'Modifier produit en dépôt', 'Products', 8, NULL, NULL, NULL, NULL),
(330, 'delete', 'Supprimer un produit en dépôt', 'Products', 8, NULL, NULL, NULL, NULL),
(331, 'create', 'Ajouter un nouveau produit en dépôt', 'Products', 8, NULL, NULL, NULL, NULL),
(332, 'update', 'Modifier le dépôt', 'Stocks', 8, NULL, NULL, NULL, NULL),
(333, 'uploadLogo', 'Gérer carrousel portal', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(334, 'addCourse', 'Ajouter cours dans l\'horaire', 'Schedules', 7, NULL, NULL, NULL, NULL),
(335, 'schedulesAgenda', 'Liste des heures de cours pour l\'agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(336, 'viewForAgenda', 'Voir plus de details sur un cours en mode agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(337, 'index', 'Vue globale des heures de cours d\'une salle par semaine', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(338, 'addCourse', 'Ajouter cours dans l\'agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(339, 'create', 'Ajouter/Modifier cours dans l\'agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(340, 'schedulesAgenda', 'Liste des heures de cours pour l\'agenda', 'Persons', 9, NULL, NULL, NULL, NULL),
(341, 'index', 'Vue globale des heures de cours d\'une salle/semaine pour INVITE', 'ScheduleAgenda', 10, NULL, NULL, NULL, NULL),
(342, 'schedulesAgenda', 'Liste des heures de cours pour l\'agenda', 'ScheduleAgenda', 10, NULL, NULL, NULL, NULL),
(343, 'viewForAgenda', 'Voir plus de details sur un cours en mode agenda', 'ScheduleAgenda', 10, NULL, NULL, NULL, NULL),
(344, 'updateArticle', 'Modifier article inline', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(345, 'admin', 'Liste de menu du portail', 'CmsMenu', 12, NULL, NULL, NULL, NULL),
(346, 'updateMenu', 'Modifier menu inline', 'CmsMenu', 12, NULL, NULL, NULL, NULL),
(347, 'index', 'liste des documents charges', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(348, 'create', 'Charger un nouveau document', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(349, 'update', 'Modifier un document charge', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(350, 'delete', 'Supprimer un document charge', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(351, 'create', 'Ajouter des observations pour les bulletins', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(352, 'updateObservation', 'Modifier observation inline', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(353, 'gridcreateCourse', 'Ajouter plusieurs cours par salle', 'Courses', 7, NULL, NULL, NULL, NULL),
(354, 'view', 'Voir les info d\'un document qui a ete charge', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(355, 'massAddingFees', 'Ajouter plusieurs frais par classe', 'Fees', 1, NULL, NULL, NULL, NULL),
(356, 'massAddingInfractionType', 'Ajouter plusieurs type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(357, 'massAddingSubjects', 'Créer plusieurs matières', 'Subjects', 7, NULL, NULL, NULL, NULL),
(358, 'index', 'Migration élèves', 'Datamigration', 7, NULL, NULL, NULL, NULL),
(359, 'employees', 'Migration employé', 'Datamigration', 7, NULL, NULL, NULL, NULL),
(360, 'fdm', 'Produire fiche de declaration mensuelle', 'Reports', 8, NULL, NULL, NULL, NULL),
(361, 'massAddingScholarship', 'Ajouter les infos pour un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(362, 'delete', 'Supprimer un article dans le panier', 'Sellings', 8, NULL, NULL, NULL, NULL),
(363, 'index', 'Liste des fourchettes pour le calcul d\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(364, 'create', 'Ajouter les fourchettes permettant de calculer l\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(365, 'update', 'Modifier les fourchettes permettant de calculer l\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(366, 'delete', 'Supprimer les fourchettes permettant de  calculer l\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(367, 'create', 'Créer champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(368, 'update', 'Modifier champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(369, 'delete', 'Supprimer champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(370, 'index', 'Lister champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(371, 'deletedoc', 'Supprimer un document de l\'eleve', 'Persons', 9, NULL, NULL, NULL, NULL),
(372, 'index', 'Listes des documents archives', 'Documents', 7, NULL, NULL, NULL, NULL),
(373, 'index', 'Liste des observations pour les bulletins', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(374, 'delete', 'Supprimer une observation du bulletin', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(375, 'update', 'Modifier les observations du bulletin', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(376, 'transcriptNotesSearch', 'Recherche pour releve de notes', 'Persons', 9, NULL, NULL, NULL, NULL),
(377, 'transcriptNotes', 'Imprimer releve de notes', 'Persons', 9, NULL, NULL, NULL, NULL),
(378, 'viewcontact', 'Liste des contacts (vue eleve)', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(379, 'viewApprovePostulant', 'Voir la liste des postulants acceptes', 'Postulant', 9, NULL, NULL, NULL, NULL),
(380, 'decisionAdmission', 'Afficher la liste des postulants pour lesquels on va decider', 'Postulant', 9, NULL, NULL, NULL, NULL),
(381, 'create', 'Ajouter un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(382, 'update', 'Modifier les infos d\'un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(383, 'delete', 'Supprimer un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(384, 'create', 'Encaisser un frais d\'inscription', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(385, 'update', 'Modifier un encaissement de frais d\'inscription', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(386, 'delete', 'Supprimer un encaissement d\'un frais d\'inscription', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(387, 'index', 'Liste paiement des postulants', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(388, 'exempt', 'Dispenser quelqu\'un de sa dette', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(389, 'index_exempt', 'Liste des dettes annulées', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(390, 'index', 'Afficher la liste des examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(391, 'create', 'Ajouter les examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(392, 'update', 'Modifier examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(393, 'delete', 'Supprimer un examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(394, 'index', 'Afficher la liste des participants au examen du MENFP', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(395, 'create', 'Ajouter ou Modifier les notes(MENFP) d\'un participant', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(396, 'delete', 'Supprimer une note(MENFP) d\'un participant', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(397, 'view', 'Voir notes et decision d\'un participant au MENFP', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(398, 'yearMigrationCheck', 'Migration des donnés de l\'année précédente', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(399, 'periodsSummary', 'Sommaire des periodes', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(400, 'index', 'Afficher la liste des reservations', 'Reservation', 8, NULL, NULL, NULL, NULL),
(401, 'create', 'Ajouter une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(402, 'update', 'Modifier une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(403, 'delete', 'Supprimer une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(404, 'view', 'Afficher une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(405, 'updateEndYearDecision', 'Modifier la décision finale pour une salle', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(406, 'palmares', 'palmares', 'Palmares', 9, NULL, NULL, NULL, NULL),
(407, 'index', 'accueil palmares', 'Palmares', 9, NULL, NULL, NULL, NULL),
(408, 'carrousel', 'Gérer caroussel', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(409, 'lisimaj', 'Liste des images du caroussel', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(410, 'updateCarrousel', 'Mise à jour label image caroussel in line', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(411, 'deleteCaroussel', 'Supprimer image caroussel', 'CmsArticle', 9, NULL, NULL, NULL, NULL),
(412, 'updategrade', 'Modifier les notes depuis le palmares', 'Palmares', 9, NULL, NULL, NULL, NULL),
(413, 'deleteCarrousel', 'Supprimer image carrousel', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(414, 'validation', 'Valider dans le nouveau palmares', 'Palmares', 9, NULL, NULL, NULL, NULL),
(415, 'create', 'Preparer les ID cartes', 'Idcard', 13, NULL, NULL, NULL, NULL),
(416, 'index', 'Imprimer les ID cartes', 'Idcard', 13, NULL, NULL, NULL, NULL),
(417, 'deletecard', 'Supprimer une ID carte', 'Idcard', 13, NULL, NULL, NULL, NULL),
(418, 'printidcard', 'Imprimer une ID carte', 'Idcard', 13, NULL, NULL, NULL, NULL),
(419, 'index', 'Liste evaluation kindergarden', 'KinderEvaluation', 14, NULL, NULL, NULL, NULL),
(420, 'create', 'Créer evaluation kindergarden', 'KinderEvaluation', 14, NULL, NULL, NULL, NULL),
(421, 'list', 'Liste kindergarden', 'KinderEvaluation', 14, NULL, NULL, NULL, NULL),
(422, 'index', 'Configuration Kindergarden', 'KinderPeriod', 14, NULL, NULL, NULL, NULL),
(423, 'index', 'Liste evaluation kindergarden', 'KinderEvaluation', 14, NULL, NULL, NULL, NULL),
(424, 'index', 'Liste des appreciations', 'AppreciationsGrid', 1, NULL, NULL, NULL, NULL),
(425, 'create', 'Ajouter des appreciations', 'AppreciationsGrid', 1, NULL, NULL, NULL, NULL),
(426, 'update', 'Modifier les appreciations', 'AppreciationsGrid', 1, NULL, NULL, NULL, NULL),
(427, 'delete', 'Supprimer une appreciation', 'AppreciationsGrid', 1, NULL, NULL, NULL, NULL),
(428, 'updateAppreciation', 'Modifier appreciations inline', 'AppreciationsGrid', 1, NULL, NULL, NULL, NULL),
(429, 'create', 'Ajouter des notes de devoir', 'Devoirs', 9, NULL, NULL, NULL, NULL),
(430, 'update', 'Modifier des notes de devoir', 'Devoirs', 9, NULL, NULL, NULL, NULL),
(431, 'delete', 'Supprimer des notes de devoir', 'Devoirs', 9, NULL, NULL, NULL, NULL),
(432, 'index', 'Lister des notes de devoir', 'Devoirs', 9, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text CHARACTER SET utf8 NOT NULL,
  `create_by` varchar(128) NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `announcements`
--

INSERT INTO `announcements` (`id`, `title`, `description`, `create_by`, `create_time`) VALUES
(1, 'Hirondelle est pour cet été', '<p><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Nous avons le plaisir de vous informer SIGES 2.0 sera bient&ocirc;t en t&eacute;l&eacute;chargement sur le site de Logipam. Avec ces nouvelles mises &agrave; jour, vous b&eacute;n&eacute;ficiez d&#39;une meilleure interface utilisateur pour am&eacute;liorer votre compr&eacute;hension et votre interaction avec SIGES. Les principales modifications et nouveaut&eacute;s sont apport&eacute;es aux modules param&egrave;tres &eacute;coles, gestion acad&eacute;mique, &eacute;l&egrave;ve, et le tableau de bord.</span></span></p>\r\n\r\n<p style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Cette version s&rsquo;appellera Hirondelle&nbsp;!</span></span></p>\r\n', 'Admin', '2019-07-02 04:07:33'),
(2, 'Testez la démo !', '<p><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Voici les identifiants pour se connecter &agrave; la d&eacute;mo : <strong>Utilisateur</strong> :<strong> <span style="color:#008000">admin</span></strong>; <strong>Password</strong> :<strong><span style="color:#006400"> admin</span></strong>. Avec ces nouvelles mises &agrave; jour, vous b&eacute;n&eacute;ficiez d&#39;une meilleure interface utilisateur pour am&eacute;liorer votre compr&eacute;hension et votre interaction avec SIGES. Les principales modifications et nouveaut&eacute;s sont apport&eacute;es aux modules param&egrave;tres &eacute;coles, gestion acad&eacute;mique, &eacute;l&egrave;ve, et le tableau de bord.</span></span></p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n', 'Admin', '2019-09-16 06:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `appreciations_grid`
--

CREATE TABLE `appreciations_grid` (
  `id` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `start_range` float NOT NULL,
  `end_range` float NOT NULL,
  `comment` varchar(255) NOT NULL,
  `short_text` varchar(5) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `arrondissements`
--

CREATE TABLE `arrondissements` (
  `id` int(11) NOT NULL,
  `arrondissement_name` varchar(45) NOT NULL,
  `departement` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `arrondissements`
--

INSERT INTO `arrondissements` (`id`, `arrondissement_name`, `departement`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Port-au-Prince', 2, NULL, NULL, '', ''),
(2, 'Borgne', 1, NULL, NULL, NULL, NULL),
(3, 'Cap-Haitien', 1, NULL, NULL, NULL, NULL),
(4, 'Grande-Rivière Du Nord', 1, NULL, NULL, NULL, NULL),
(5, 'L\'Acul Du Nord', 1, NULL, NULL, NULL, NULL),
(6, 'Limbé', 1, NULL, NULL, NULL, NULL),
(7, 'Plaisance', 1, NULL, NULL, NULL, NULL),
(8, 'Saint-Raphael', 1, NULL, NULL, NULL, NULL),
(9, 'Arcahaie', 2, NULL, NULL, NULL, NULL),
(10, 'Croix-Des-Bouquets', 2, NULL, NULL, NULL, NULL),
(11, 'La Gonâve', 2, NULL, NULL, NULL, NULL),
(12, 'Léogâne', 2, NULL, NULL, NULL, NULL),
(13, 'Dessalines', 3, NULL, NULL, NULL, NULL),
(14, 'Gonaïves', 3, NULL, NULL, NULL, NULL),
(15, 'Gros-Morne', 3, NULL, NULL, NULL, NULL),
(16, 'Marmelade', 3, NULL, NULL, NULL, NULL),
(17, 'Saint-Marc', 3, NULL, NULL, NULL, NULL),
(18, 'Cerca-La-Source', 4, NULL, NULL, NULL, NULL),
(19, 'Hinche', 4, NULL, NULL, NULL, NULL),
(20, 'Lascahobas', 4, NULL, NULL, NULL, NULL),
(21, 'Mirebalais', 4, NULL, NULL, NULL, NULL),
(22, 'Anse-D\'Hainault', 5, NULL, NULL, NULL, NULL),
(23, 'Corail', 5, NULL, NULL, NULL, NULL),
(24, 'Jérémie', 5, NULL, NULL, NULL, NULL),
(25, 'Anse-A-Veau', 6, NULL, NULL, NULL, NULL),
(26, 'Baradères', 6, NULL, NULL, NULL, NULL),
(27, 'Miragoâne', 6, NULL, NULL, NULL, NULL),
(28, 'Fort-Liberté', 7, NULL, NULL, NULL, NULL),
(29, 'Ouanaminthe', 7, NULL, NULL, NULL, NULL),
(30, 'Trou-Du-Nord', 7, NULL, NULL, NULL, NULL),
(31, 'Vallières', 7, NULL, NULL, NULL, NULL),
(32, 'Môle Saint-Nicolas', 8, NULL, NULL, NULL, NULL),
(33, 'Port-De-Paix', 8, NULL, NULL, NULL, NULL),
(34, 'Saint-Louis Du Nord', 8, NULL, NULL, NULL, NULL),
(35, 'Aquin', 9, NULL, NULL, NULL, NULL),
(36, 'Chardonnières', 9, NULL, NULL, NULL, NULL),
(37, 'Côteaux', 9, NULL, NULL, NULL, NULL),
(38, 'Les Cayes', 9, NULL, NULL, NULL, NULL),
(39, 'Port-Salut', 9, NULL, NULL, NULL, NULL),
(40, 'Bainet', 10, NULL, NULL, NULL, NULL),
(41, 'Belle-Anse', 10, NULL, NULL, NULL, NULL),
(42, 'Jacmel', 10, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `average_by_period`
--

CREATE TABLE `average_by_period` (
  `academic_year` int(11) NOT NULL,
  `evaluation_by_year` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `sum` double NOT NULL,
  `average` double NOT NULL,
  `place` int(11) NOT NULL,
  `reportcard_ref` varchar(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `average_by_period`
--

INSERT INTO `average_by_period` (`academic_year`, `evaluation_by_year`, `student`, `sum`, `average`, `place`, `reportcard_ref`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 1, 52, 815, 81.5, 13, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 54, 772, 77.2, 22, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 55, 820, 82, 12, '', '2019-07-06 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 56, 826, 82.6, 9, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 57, 774, 77.4, 21, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 58, 823, 82.3, 11, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 59, 848, 84.8, 2, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 60, 807, 80.7, 15, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 61, 831, 83.1, 7, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 62, 790, 79, 18, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 63, 808, 80.8, 14, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 64, 900, 90, 1, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 65, 759, 75.9, 24, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 66, 846, 84.6, 4, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 67, 792, 79.2, 17, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 68, 787, 78.7, 19, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 69, 826, 82.6, 9, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 70, 847, 84.7, 3, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 71, 784, 78.4, 20, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 72, 827, 82.7, 8, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 73, 844, 84.4, 6, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 74, 765, 76.5, 23, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 75, 846, 84.6, 4, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 76, 801, 80.1, 16, '', '2019-08-08 00:00:00', NULL, NULL, NULL),
(1, 1, 77, 927, 92.7, 2, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 78, 791, 79.1, 15, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 79, 798, 79.8, 13, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 80, 758, 75.8, 24, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 81, 826, 82.6, 9, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 82, 808, 80.8, 12, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 83, 768, 76.8, 19, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 84, 782, 78.2, 17, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 85, 865, 86.5, 5, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 86, 825, 82.5, 10, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 87, 824, 82.4, 11, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 88, 847, 84.7, 6, '', '2019-07-05 00:00:00', '2019-07-13 00:00:00', NULL, NULL),
(1, 1, 89, 763, 76.3, 22, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 90, 847, 84.7, 6, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 91, 933, 93.3, 1, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 92, 871, 87.1, 3, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 93, 834, 83.4, 8, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 94, 797, 79.7, 14, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 95, 868, 86.8, 4, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 96, 762, 76.2, 23, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 97, 750, 75, 25, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 98, 764, 76.4, 21, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 99, 746, 74.6, 26, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 100, 767, 76.7, 20, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 101, 770, 77, 18, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 102, 784, 78.4, 16, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 103, 742, 74.2, 27, '', '2019-07-13 00:00:00', NULL, NULL, NULL),
(1, 1, 106, 894, 89.4, 2, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 107, 811, 81.1, 10, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 109, 812, 81.2, 9, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 110, 867, 86.7, 3, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 111, 803, 80.3, 11, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 112, 838, 83.8, 5, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 113, 902, 90.2, 1, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 114, 834, 83.4, 6, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 115, 784, 78.4, 16, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 116, 785, 78.5, 15, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 117, 818, 81.8, 8, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 118, 800, 80, 12, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 119, 791, 79.1, 14, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 120, 842, 84.2, 4, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 121, 828, 82.8, 7, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 122, 753, 75.3, 21, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 123, 776, 77.6, 18, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 124, 766, 76.6, 20, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 126, 769, 76.9, 19, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 127, 794, 79.4, 13, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 128, 782, 78.2, 17, '', '2019-07-24 00:00:00', NULL, NULL, NULL),
(1, 1, 132, 830, 83, 8, '', '2019-07-11 00:00:00', NULL, NULL, NULL),
(1, 1, 228, 742, 40.66, 10, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 229, 1516, 83.07, 1, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 230, 1164, 63.78, 3, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 231, 982, 53.81, 6, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 232, 1275, 69.86, 2, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 233, 903, 49.48, 8, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 234, 932, 51.07, 7, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 235, 1066, 58.41, 4, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 236, 865, 47.4, 9, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 237, 995, 54.52, 5, '', '2019-08-05 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 1, 238, 402, 22.03, 11, '', '2019-08-07 00:00:00', '2019-08-08 00:00:00', NULL, NULL),
(1, 2, 229, 1559, 89.09, 3, '', '2019-09-05 00:00:00', NULL, NULL, NULL),
(1, 2, 232, 1665, 95.14, 1, '', '2019-09-05 00:00:00', NULL, NULL, NULL),
(6, 5, 191, 868, 75.48, 1, '', '2019-09-03 00:00:00', NULL, NULL, NULL),
(6, 5, 241, 377.5, 47.19, 10, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 242, 426, 53.25, 5, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 243, 385, 48.13, 9, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 244, 417, 52.13, 7, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 245, 472, 59, 3, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(6, 5, 246, 442, 55.25, 4, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 247, 501, 62.63, 2, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 248, 566, 70.75, 1, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 249, 407, 50.88, 8, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 250, 421, 52.63, 6, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 251, 471.5, 58.94, 3, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 252, 370, 46.25, 11, '', '2019-08-31 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(6, 5, 266, 620, 68.89, 1, '', '2019-09-05 00:00:00', NULL, NULL, NULL),
(6, 5, 267, 555, 61.67, 2, '', '2019-09-05 00:00:00', NULL, NULL, NULL),
(6, 5, 268, 503, 55.89, 3, '', '2019-09-05 00:00:00', NULL, NULL, NULL),
(6, 5, 269, 414, 46, 5, '', '2019-09-05 00:00:00', NULL, NULL, NULL),
(6, 5, 270, 499, 55.44, 4, '', '2019-09-05 00:00:00', NULL, NULL, NULL),
(6, 6, 246, 90, 90, 5, '', '2019-09-06 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE `balance` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `balance` double NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `balance`
--

INSERT INTO `balance` (`id`, `student`, `balance`, `date_created`) VALUES
(1, 88, 10000, '2019-07-07 00:00:00'),
(2, 90, 2000, '2019-07-07 00:00:00'),
(3, 77, 10000, '2019-07-07 00:00:00'),
(5, 82, 10000, '2019-07-07 00:00:00'),
(6, 85, 10000, '2019-07-07 00:00:00'),
(7, 87, 10000, '2019-07-07 00:00:00'),
(8, 93, 10000, '2019-07-07 00:00:00'),
(9, 94, 10000, '2019-07-07 00:00:00'),
(10, 78, 10000, '2019-07-07 00:00:00'),
(11, 79, 10000, '2019-07-07 00:00:00'),
(12, 95, 10000, '2019-07-07 00:00:00'),
(13, 81, 10000, '2019-07-07 00:00:00'),
(14, 96, 10000, '2019-07-07 00:00:00'),
(15, 97, 10000, '2019-07-07 00:00:00'),
(16, 98, 10000, '2019-07-07 00:00:00'),
(17, 99, 10000, '2019-07-07 00:00:00'),
(18, 100, 10000, '2019-07-07 00:00:00'),
(19, 101, 10000, '2019-07-07 00:00:00'),
(20, 86, 10000, '2019-07-07 00:00:00'),
(21, 83, 10000, '2019-07-07 00:00:00'),
(22, 89, 10000, '2019-07-07 00:00:00'),
(23, 84, 10000, '2019-07-07 00:00:00'),
(24, 80, 10000, '2019-07-07 00:00:00'),
(25, 102, 10000, '2019-07-07 00:00:00'),
(26, 103, 10000, '2019-07-07 00:00:00'),
(27, 66, 26000, '2019-07-23 00:00:00'),
(28, 106, 25000, '2019-07-27 00:00:00'),
(29, 114, 25000, '2019-07-27 00:00:00'),
(30, 110, 25000, '2019-07-27 00:00:00'),
(31, 118, 25000, '2019-07-27 00:00:00'),
(33, 113, 25000, '2019-07-27 00:00:00'),
(34, 107, 25000, '2019-07-27 00:00:00'),
(35, 111, 25000, '2019-07-27 00:00:00'),
(36, 120, 25000, '2019-07-27 00:00:00'),
(37, 121, 25000, '2019-07-27 00:00:00'),
(38, 112, 25000, '2019-07-27 00:00:00'),
(39, 116, 25000, '2019-07-27 00:00:00'),
(41, 115, 25000, '2019-07-27 00:00:00'),
(42, 104, 25000, '2019-07-27 00:00:00'),
(43, 123, 25000, '2019-07-27 00:00:00'),
(44, 124, 25000, '2019-07-27 00:00:00'),
(45, 108, 25000, '2019-07-27 00:00:00'),
(46, 109, 25000, '2019-07-27 00:00:00'),
(47, 125, 25000, '2019-07-27 00:00:00'),
(48, 126, 25000, '2019-07-27 00:00:00'),
(49, 117, 25000, '2019-07-27 00:00:00'),
(50, 127, 25000, '2019-07-27 00:00:00'),
(51, 105, 25000, '2019-07-27 00:00:00'),
(53, 132, 10000, '2019-07-30 00:00:00'),
(56, 144, 10000, '2019-07-30 00:00:00'),
(57, 145, 10000, '2019-07-30 00:00:00'),
(58, 148, 10000, '2019-07-30 00:00:00'),
(59, 149, 10000, '2019-07-30 00:00:00'),
(60, 153, 10000, '2019-07-30 00:00:00'),
(61, 137, 10000, '2019-07-30 00:00:00'),
(62, 147, 10000, '2019-07-30 00:00:00'),
(63, 151, 10000, '2019-07-30 00:00:00'),
(64, 130, 10000, '2019-07-30 00:00:00'),
(65, 138, 10000, '2019-07-30 00:00:00'),
(66, 150, 10000, '2019-07-30 00:00:00'),
(67, 141, 10000, '2019-07-30 00:00:00'),
(68, 133, 10000, '2019-07-30 00:00:00'),
(69, 152, 10000, '2019-07-30 00:00:00'),
(70, 142, 10000, '2019-07-30 00:00:00'),
(71, 146, 10000, '2019-07-30 00:00:00'),
(72, 135, 10000, '2019-07-30 00:00:00'),
(73, 139, 10000, '2019-07-30 00:00:00'),
(74, 134, 10000, '2019-07-30 00:00:00'),
(75, 131, 10000, '2019-07-30 00:00:00'),
(76, 136, 10000, '2019-07-30 00:00:00'),
(77, 55, 34000, '2019-07-31 00:00:00'),
(78, 53, 34000, '2019-07-31 00:00:00'),
(79, 60, 34000, '2019-07-31 00:00:00'),
(80, 59, 34000, '2019-07-31 00:00:00'),
(81, 75, 34000, '2019-07-31 00:00:00'),
(82, 72, 34000, '2019-07-31 00:00:00'),
(83, 67, 34000, '2019-07-31 00:00:00'),
(84, 69, 34000, '2019-07-31 00:00:00'),
(85, 54, 34000, '2019-07-31 00:00:00'),
(86, 76, 34000, '2019-07-31 00:00:00'),
(87, 56, 34000, '2019-07-31 00:00:00'),
(88, 63, 34000, '2019-07-31 00:00:00'),
(89, 58, 34000, '2019-07-31 00:00:00'),
(90, 57, 34000, '2019-07-31 00:00:00'),
(91, 65, 34000, '2019-07-31 00:00:00'),
(92, 62, 34000, '2019-07-31 00:00:00'),
(93, 68, 34000, '2019-07-31 00:00:00'),
(94, 74, 34000, '2019-07-31 00:00:00'),
(95, 52, 34000, '2019-07-31 00:00:00'),
(96, 71, 34000, '2019-07-31 00:00:00'),
(97, 64, 34000, '2019-07-31 00:00:00'),
(98, 61, 34000, '2019-07-31 00:00:00'),
(99, 70, 34000, '2019-07-31 00:00:00'),
(101, 229, 24000, '2019-08-05 00:00:00'),
(102, 230, 34000, '2019-08-05 00:00:00'),
(103, 231, 34000, '2019-08-05 00:00:00'),
(104, 232, 34000, '2019-08-05 00:00:00'),
(105, 233, 26000, '2019-08-05 00:00:00'),
(106, 234, 34000, '2019-08-05 00:00:00'),
(107, 235, 34000, '2019-08-05 00:00:00'),
(110, 238, 34000, '2019-08-06 00:00:00'),
(111, 239, 34000, '2019-08-06 00:00:00'),
(112, 73, 24000, '2019-08-07 00:00:00'),
(113, 237, 24000, '2019-08-07 00:00:00'),
(114, 228, 24000, '2019-08-07 00:00:00'),
(115, 191, 30000, '2019-08-07 00:00:00'),
(116, 201, 30000, '2019-08-07 00:00:00'),
(117, 179, 30000, '2019-08-07 00:00:00'),
(118, 180, 20500, '2019-08-07 00:00:00'),
(119, 181, 5000, '2019-08-07 00:00:00'),
(120, 182, 19800, '2019-08-07 00:00:00'),
(121, 183, 21200, '2019-08-07 00:00:00'),
(122, 185, 30000, '2019-08-07 00:00:00'),
(123, 186, 30000, '2019-08-07 00:00:00'),
(124, 187, 23000, '2019-08-07 00:00:00'),
(125, 188, 30000, '2019-08-07 00:00:00'),
(126, 189, 20000, '2019-08-07 00:00:00'),
(127, 190, 30000, '2019-08-07 00:00:00'),
(128, 192, 30000, '2019-08-07 00:00:00'),
(129, 193, 3000, '2019-08-07 00:00:00'),
(130, 194, 7500, '2019-08-07 00:00:00'),
(131, 196, 12500, '2019-08-07 00:00:00'),
(132, 197, 30000, '2019-08-07 00:00:00'),
(133, 198, 30000, '2019-08-07 00:00:00'),
(134, 199, 30000, '2019-08-07 00:00:00'),
(135, 200, 22000, '2019-08-07 00:00:00'),
(136, 202, 30000, '2019-08-07 00:00:00'),
(137, 203, 12000, '2019-08-07 00:00:00'),
(138, 210, 25000, '2019-08-07 00:00:00'),
(139, 205, 25000, '2019-08-07 00:00:00'),
(140, 216, 25000, '2019-08-07 00:00:00'),
(141, 212, 25000, '2019-08-07 00:00:00'),
(142, 204, 25000, '2019-08-07 00:00:00'),
(143, 213, 25000, '2019-08-07 00:00:00'),
(144, 209, 25000, '2019-08-07 00:00:00'),
(145, 214, 25000, '2019-08-07 00:00:00'),
(146, 207, 25000, '2019-08-07 00:00:00'),
(147, 215, 25000, '2019-08-07 00:00:00'),
(148, 206, 25000, '2019-08-07 00:00:00'),
(149, 211, 25000, '2019-08-07 00:00:00'),
(150, 236, 9000, '2019-08-10 00:00:00'),
(151, 269, 0, '2019-09-06 00:00:00'),
(152, 267, 0, '2019-09-06 00:00:00'),
(153, 270, 0, '2019-09-06 00:00:00'),
(154, 275, 35500, '2019-09-06 00:00:00'),
(155, 276, 35500, '2019-09-06 00:00:00'),
(156, 277, 35500, '2019-09-06 00:00:00'),
(157, 278, 35500, '2019-09-06 00:00:00'),
(158, 279, 35500, '2019-09-06 00:00:00'),
(159, 280, 35500, '2019-09-06 00:00:00'),
(160, 281, 35500, '2019-09-06 00:00:00'),
(161, 282, 35500, '2019-09-06 00:00:00'),
(162, 283, 35500, '2019-09-06 00:00:00'),
(163, 284, 35500, '2019-09-06 00:00:00'),
(164, 285, 35500, '2019-09-06 00:00:00'),
(165, 286, 35500, '2019-09-06 00:00:00'),
(166, 287, 35500, '2019-09-06 00:00:00'),
(167, 288, 35500, '2019-09-06 00:00:00'),
(168, 289, 35500, '2019-09-06 00:00:00'),
(169, 290, 35500, '2019-09-06 00:00:00'),
(170, 291, 35500, '2019-09-06 00:00:00'),
(171, 292, 35500, '2019-09-06 00:00:00'),
(172, 293, 35500, '2019-09-06 00:00:00'),
(173, 294, 35500, '2019-09-06 00:00:00'),
(174, 295, 35500, '2019-09-06 00:00:00'),
(175, 296, 35500, '2019-09-06 00:00:00'),
(176, 297, 35500, '2019-09-06 00:00:00'),
(177, 298, 35500, '2019-09-06 00:00:00'),
(178, 299, 35500, '2019-09-06 00:00:00'),
(179, 300, 35500, '2019-09-06 00:00:00'),
(180, 301, 35500, '2019-09-06 00:00:00'),
(181, 302, 35500, '2019-09-06 00:00:00'),
(182, 303, 35500, '2019-09-06 00:00:00'),
(183, 304, 35500, '2019-09-06 00:00:00'),
(184, 308, 35500, '2019-09-06 00:00:00'),
(185, 309, 35500, '2019-09-06 00:00:00'),
(186, 305, 35500, '2019-09-06 00:00:00'),
(187, 306, 35500, '2019-09-06 00:00:00'),
(188, 307, 35500, '2019-09-06 00:00:00'),
(189, 310, 35500, '2019-09-06 00:00:00'),
(190, 311, 35500, '2019-09-06 00:00:00'),
(191, 312, 35500, '2019-09-06 00:00:00'),
(192, 313, 35500, '2019-09-06 00:00:00'),
(193, 314, 35500, '2019-09-06 00:00:00'),
(194, 315, 35500, '2019-09-06 00:00:00'),
(195, 316, 35500, '2019-09-06 00:00:00'),
(196, 317, 35500, '2019-09-06 00:00:00'),
(197, 318, 35500, '2019-09-06 00:00:00'),
(198, 319, 35500, '2019-09-06 00:00:00'),
(199, 320, 35500, '2019-09-06 00:00:00'),
(200, 321, 35500, '2019-09-06 00:00:00'),
(201, 322, 35500, '2019-09-06 00:00:00'),
(202, 323, 35500, '2019-09-06 00:00:00'),
(203, 324, 35500, '2019-09-06 00:00:00'),
(204, 325, 35500, '2019-09-06 00:00:00'),
(205, 326, 35500, '2019-09-06 00:00:00'),
(206, 268, 35500, '2019-09-06 00:00:00'),
(207, 327, 35500, '2019-09-06 00:00:00'),
(208, 328, 35500, '2019-09-06 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `bareme`
--

CREATE TABLE `bareme` (
  `id` int(11) NOT NULL,
  `min_value` double NOT NULL,
  `max_value` double NOT NULL,
  `percentage` double NOT NULL,
  `compteur` int(11) NOT NULL,
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old bareme; 1: new bareme in use',
  `date_created` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bareme`
--

INSERT INTO `bareme` (`id`, `min_value`, `max_value`, `percentage`, `compteur`, `old_new`, `date_created`, `created_by`) VALUES
(1, 0, 60000, 0, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(2, 60000, 240000, 10, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(3, 240000, 480000, 15, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(4, 480000, 1000000, 25, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(5, 1000000, 1100000, 30, 1, 1, '2016-09-29 18:09:17', '_developer_');

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `fee_period` int(11) NOT NULL,
  `amount_to_pay` float NOT NULL,
  `amount_pay` float NOT NULL,
  `balance` float DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `date_pay` date NOT NULL,
  `payment_method` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `fee_totally_paid` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0: paiement partiel; 1: paiement total ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `updated_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `billings`
--

INSERT INTO `billings` (`id`, `student`, `fee_period`, `amount_to_pay`, `amount_pay`, `balance`, `reservation_id`, `academic_year`, `date_pay`, `payment_method`, `comments`, `fee_totally_paid`, `date_created`, `date_updated`, `created_by`, `updated_by`) VALUES
(1, 92, 1, 10000, 10000, 0, NULL, 1, '2018-10-09', 2, '', 1, '2019-07-04 00:00:00', NULL, 'admin', NULL),
(2, 88, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(3, 90, 1, 10000, 8000, 2000, NULL, 1, '2019-08-07', 1, '', 0, '2019-07-07 00:00:00', '2019-08-07 00:00:00', 'SIGES', 'admin'),
(4, 77, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(5, 91, 1, 10000, 10000, 0, NULL, 1, '2019-07-24', 1, '', 1, '2019-07-07 00:00:00', '2019-07-24 00:00:00', 'SIGES', 'admin'),
(6, 82, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(7, 85, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(8, 87, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(9, 93, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(10, 94, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(11, 78, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(12, 79, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(13, 95, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(14, 81, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(15, 96, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(16, 97, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(17, 98, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(18, 99, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(19, 100, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(20, 101, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(21, 86, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(22, 83, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(23, 89, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(24, 84, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(25, 80, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(26, 102, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(27, 103, 1, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-07 00:00:00', NULL, 'SIGES', NULL),
(28, 66, 4, 10000, 8000, 2000, NULL, 1, '2019-07-23', 1, '', 0, '2019-07-23 00:00:00', NULL, 'admin', NULL),
(29, 73, 4, 10000, 10000, 0, NULL, 1, '2019-07-24', 1, '', 1, '2019-07-24 00:00:00', NULL, 'admin', NULL),
(30, 143, 3, 10000, 10000, 0, NULL, 1, '2019-07-24', 1, '', 1, '2019-07-24 00:00:00', NULL, 'admin', NULL),
(31, 106, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(32, 114, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(33, 110, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(34, 118, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(35, 119, 2, 10000, 10000, 0, NULL, 1, '2019-08-06', 1, '', 1, '2019-07-27 00:00:00', '2019-08-07 00:00:00', 'SIGES', 'admin'),
(36, 113, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(37, 107, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(38, 111, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(39, 120, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(40, 121, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(41, 112, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(42, 116, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(43, 122, 2, 10000, 10000, 0, NULL, 1, '2019-08-06', 1, '', 1, '2019-07-27 00:00:00', '2019-08-07 00:00:00', 'SIGES', 'admin'),
(44, 115, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(45, 104, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(46, 123, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(47, 124, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(48, 108, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(49, 109, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(50, 125, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(51, 126, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(52, 117, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(53, 127, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(54, 105, 2, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-27 00:00:00', NULL, 'SIGES', NULL),
(55, 128, 2, 10000, 10000, 0, NULL, 1, '2019-08-07', 1, '', 1, '2019-07-27 00:00:00', '2019-08-07 00:00:00', 'SIGES', 'admin'),
(56, 132, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(58, 140, 3, 10000, 20000, -10000, NULL, 1, '2019-08-06', 1, '', 0, '2019-07-30 00:00:00', '2019-08-06 00:00:00', 'SIGES', 'admin'),
(59, 144, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(60, 145, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(61, 148, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(62, 149, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(63, 153, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(64, 137, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(65, 147, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(66, 151, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(67, 130, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(68, 138, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(69, 150, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(70, 141, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(71, 133, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(72, 152, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(73, 142, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(74, 146, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(75, 135, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(76, 139, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(77, 134, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(78, 131, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(79, 136, 3, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-30 00:00:00', NULL, 'SIGES', NULL),
(80, 55, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(81, 53, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(82, 60, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(83, 59, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(84, 75, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(85, 72, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(86, 67, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(87, 69, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(88, 54, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(89, 76, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(90, 56, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(91, 63, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(92, 58, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(93, 57, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(94, 65, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(95, 62, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(96, 68, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(97, 74, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(98, 52, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(99, 71, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(100, 64, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(101, 61, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(102, 70, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-07-31 00:00:00', NULL, 'SIGES', NULL),
(103, 228, 4, 10000, 10000, 0, NULL, 1, '2019-08-02', 1, '', 1, '2019-08-05 00:00:00', '2019-08-06 00:00:00', 'SIGES', 'admin'),
(104, 229, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-05 00:00:00', NULL, 'SIGES', NULL),
(105, 230, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-05 00:00:00', NULL, 'SIGES', NULL),
(106, 231, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-05 00:00:00', NULL, 'SIGES', NULL),
(107, 232, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-05 00:00:00', NULL, 'SIGES', NULL),
(108, 233, 4, 10000, 8000, 2000, NULL, 1, '2019-08-05', 1, '', 0, '2019-08-05 00:00:00', '2019-08-06 00:00:00', 'SIGES', 'admin'),
(109, 234, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-05 00:00:00', NULL, 'SIGES', NULL),
(110, 235, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-05 00:00:00', NULL, 'SIGES', NULL),
(111, 236, 4, 10000, 10000, 0, NULL, 1, '2019-08-06', 1, '', 1, '2019-08-05 00:00:00', '2019-08-06 00:00:00', 'SIGES', 'admin'),
(112, 237, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-05 00:00:00', NULL, 'SIGES', NULL),
(113, 236, 5, 15000, 15000, 0, NULL, 1, '2019-08-07', 1, '', 1, '2019-08-06 00:00:00', NULL, 'admin', NULL),
(114, 238, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-06 00:00:00', NULL, 'SIGES', NULL),
(115, 239, 4, 10000, 0, 10000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-06 00:00:00', NULL, 'SIGES', NULL),
(117, 55, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(118, 53, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(119, 66, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(120, 60, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(121, 73, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(122, 59, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(123, 229, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(124, 232, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(125, 75, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(126, 72, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(127, 231, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(128, 67, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(129, 230, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(130, 233, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(131, 69, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(132, 54, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(133, 76, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(134, 56, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(135, 63, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(136, 238, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(137, 239, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(138, 58, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(139, 234, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(140, 237, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(141, 57, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(142, 65, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(143, 62, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(144, 68, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(145, 74, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(146, 52, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(147, 71, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(148, 235, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(149, 228, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(150, 64, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(151, 61, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(152, 70, 5, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(153, 128, 10, 15000, 15000, 0, NULL, 1, '2019-08-08', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(154, 122, 10, 15000, 15000, 0, NULL, 1, '2019-08-07', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(155, 119, 10, 15000, 15000, 0, NULL, 1, '2019-08-07', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(156, 184, 14, 25000, 25000, 0, NULL, 1, '2019-08-05', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(157, 184, 15, 5000, 5000, 0, NULL, 1, '2019-08-07', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(158, 195, 14, 25000, 25000, 0, NULL, 1, '2019-08-06', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(159, 195, 15, 5000, 5000, 0, NULL, 1, '2019-08-07', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(160, 208, 18, 25000, 25000, 0, NULL, 1, '2019-08-07', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(161, 208, 19, 5000, 5000, 0, NULL, 1, '2019-08-08', 1, '', 1, '2019-08-07 00:00:00', NULL, 'admin', NULL),
(162, 191, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(163, 201, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(164, 179, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(165, 180, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(166, 181, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(167, 182, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(168, 183, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(169, 185, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(170, 186, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(171, 187, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(172, 188, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(173, 189, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(174, 190, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(175, 192, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(176, 193, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(177, 194, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(178, 196, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(179, 197, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(180, 198, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(181, 199, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(182, 200, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(183, 202, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(184, 203, 14, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(185, 210, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(186, 205, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(187, 216, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(188, 212, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(189, 204, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(190, 213, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(191, 209, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(192, 214, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(193, 207, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(194, 215, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(195, 206, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(196, 211, 18, 25000, 0, 25000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(197, 191, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(198, 201, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(199, 179, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(200, 180, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(201, 181, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(202, 182, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(203, 183, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(204, 185, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(205, 186, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(206, 187, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(207, 188, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(208, 189, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(209, 190, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(210, 192, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(211, 193, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(212, 194, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(213, 196, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(214, 197, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(215, 198, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(216, 199, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(217, 200, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(218, 202, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(219, 203, 15, 5000, 0, 5000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(220, 106, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(221, 114, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(222, 110, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(223, 118, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(224, 113, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(225, 107, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(226, 111, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(227, 120, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(228, 121, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(229, 112, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(230, 116, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(231, 115, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(232, 104, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(233, 123, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(234, 124, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(235, 108, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(236, 109, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(237, 125, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(238, 126, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(239, 117, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(240, 127, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(241, 105, 10, 15000, 0, 15000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-07 00:00:00', NULL, 'SIGES', NULL),
(242, 55, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(243, 53, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(244, 66, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(245, 60, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(246, 73, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(247, 59, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(248, 229, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(249, 232, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(250, 75, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(251, 72, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(252, 231, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(253, 67, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(254, 230, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(255, 233, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(256, 69, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(257, 54, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(258, 76, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(259, 236, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(260, 56, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(261, 63, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(262, 238, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(263, 239, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(264, 58, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(265, 234, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(266, 237, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(267, 57, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(268, 65, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(269, 62, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(270, 68, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(271, 74, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(272, 52, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(273, 71, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(274, 235, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(275, 228, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(276, 64, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(277, 61, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(278, 70, 6, 9000, 0, 9000, NULL, 1, '0000-00-00', NULL, NULL, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(279, 229, 21, 10000, 10000, 0, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(280, 193, 24, 10000, 8500, 1500, NULL, 6, '2019-08-16', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(281, 193, 24, 1500, 5000, -3500, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(282, 193, 29, 5500, 5500, 0, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(283, 193, 27, 8000, 8000, 0, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(284, 203, 24, 10000, 11000, -1000, NULL, 6, '2019-08-17', 3, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(285, 203, 29, 8000, 7000, 1000, NULL, 6, '2019-08-17', 1, '', 0, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(286, 180, 24, 10000, 9500, 500, NULL, 6, '2019-08-17', 2, '', 0, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(287, 196, 24, 10000, 13000, -3000, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(288, 196, 29, 6000, 4500, 1500, NULL, 6, '2019-08-17', 1, '', 0, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(289, 187, 24, 10000, 7000, 3000, NULL, 6, '2019-08-17', 1, '', 0, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(290, 200, 24, 10000, 8000, 2000, NULL, 6, '2019-08-17', 4, '', 0, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(291, 189, 24, 10000, 10000, 0, NULL, 6, '2019-08-17', 2, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(292, 194, 24, 10000, 10000, 0, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(293, 194, 36, 15000, 12500, 2500, NULL, 6, '2019-08-17', 4, '', 0, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(294, 181, 24, 10000, 12000, -2000, NULL, 6, '2019-08-17', 3, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(295, 181, 36, 13000, 13000, 0, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', '2019-08-18 00:00:00', 'admin', 'admin'),
(296, 182, 24, 10000, 9700, 300, NULL, 6, '2019-08-17', 1, '', 1, '2019-08-17 00:00:00', NULL, 'admin', NULL),
(297, 183, 24, 10000, 8800, 1200, NULL, 6, '2019-08-17', 1, 'UB-0838', 0, '2019-08-17 00:00:00', '2019-08-18 00:00:00', 'admin', 'admin'),
(298, 182, 24, 300, 500, -200, NULL, 6, '2019-08-30', 1, '', 0, '2019-08-30 00:00:00', NULL, 'admin', NULL),
(299, 241, 22, 10000, 10000, 0, NULL, 6, '2019-08-31', 1, '', 1, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(300, 266, 40, 35500, 35500, 0, NULL, 6, '2019-09-06', 1, '', 1, '2019-09-06 00:00:00', NULL, 'admin', NULL),
(301, 269, 40, 35500, 2500, 33000, NULL, 6, '2019-09-04', 1, '', 1, '2019-09-06 00:00:00', NULL, 'admin', NULL),
(302, 269, 40, 33000, 33000, 0, NULL, 6, '2019-09-07', 3, '', 1, '2019-09-06 00:00:00', NULL, 'admin', NULL),
(303, 267, 40, 35500, 35500, 0, NULL, 6, '2019-09-06', 3, '', 1, '2019-09-06 00:00:00', NULL, 'admin', NULL),
(307, 270, 41, 8100, 50000, -41900, NULL, 6, '2019-09-06', 3, '', 1, '2019-09-06 00:00:00', '2019-09-15 00:00:00', 'admin', 'admin'),
(308, 275, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(309, 276, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(310, 277, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(311, 278, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(312, 279, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(313, 280, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(314, 281, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(315, 282, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(316, 283, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(317, 284, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(318, 285, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(319, 286, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(320, 287, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(321, 288, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(322, 289, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(323, 290, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(324, 291, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(325, 292, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(326, 293, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(327, 294, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(328, 295, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(329, 296, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(330, 297, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(331, 298, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(332, 299, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(333, 300, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(334, 301, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(335, 302, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(336, 303, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(337, 304, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(338, 308, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(339, 309, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(340, 305, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(341, 306, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(342, 307, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(343, 310, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(344, 311, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(345, 312, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(346, 313, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(347, 314, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(348, 315, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(349, 316, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(350, 317, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(351, 318, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(352, 319, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(353, 320, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(354, 321, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(355, 322, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(356, 323, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(357, 324, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(358, 325, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(359, 326, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(360, 268, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(361, 327, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(362, 328, 40, 35500, 0, 35500, NULL, 6, '0000-00-00', NULL, NULL, 0, '2019-09-06 00:00:00', NULL, 'SIGES', NULL),
(363, 267, 41, 1600, 10000, -8400, NULL, 6, '2019-09-03', 3, '', 1, '2019-09-06 00:00:00', NULL, 'admin', NULL),
(364, 267, 43, 8100, 8100, -300, NULL, 6, '2019-09-06', 3, '', 0, '2019-09-06 00:00:00', NULL, 'admin', NULL),
(365, 270, 40, 35500, 35500, -6400, NULL, 6, '2019-09-15', 3, '', 0, '2019-09-15 00:00:00', NULL, 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `charge_description`
--

CREATE TABLE `charge_description` (
  `id` int(11) NOT NULL,
  `description` varchar(65) NOT NULL,
  `category` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `charge_description`
--

INSERT INTO `charge_description` (`id`, `description`, `category`, `comment`) VALUES
(1, 'Internet', 4, ''),
(2, 'Téléphone', 4, ''),
(3, 'Matériels de Bureau', 4, ''),
(4, 'Matériels Sportifs', 4, ''),
(5, 'Matériels Scolaires', 4, ''),
(6, 'Eau ', 4, ''),
(9, 'Carte d\'Identification Fiscale', 6, ''),
(11, 'Impôts Locatifs', 3, ''),
(13, 'Patente', 6, ''),
(15, 'Formation Staff', 5, ''),
(16, 'Assurances Immeubles', 3, ''),
(18, 'Entretien et Réparation', 3, ''),
(20, 'Eléctricité', 4, ''),
(21, 'Transport et Carburant', 7, ''),
(22, 'Frais et Indemnités', 5, '');

-- --------------------------------------------------------

--
-- Table structure for table `charge_paid`
--

CREATE TABLE `charge_paid` (
  `id` int(11) NOT NULL,
  `id_charge_description` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_date` date NOT NULL,
  `comment` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `created_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `city_name` varchar(45) NOT NULL,
  `arrondissement` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `arrondissement`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Carrefour', 1, NULL, NULL, NULL, NULL),
(2, 'Cite-Soleil', 1, NULL, NULL, NULL, NULL),
(3, 'Delmas', 1, NULL, NULL, NULL, NULL),
(4, 'Tabarre', 1, NULL, NULL, NULL, NULL),
(5, 'Gressier', 1, NULL, NULL, NULL, NULL),
(6, 'Kenscoff', 1, NULL, NULL, NULL, NULL),
(7, 'Petion-Ville', 1, NULL, NULL, NULL, NULL),
(8, 'Port-Au-Prince', 1, NULL, NULL, NULL, NULL),
(9, 'Borgne', 2, NULL, NULL, NULL, NULL),
(10, 'Port-Margot', 2, NULL, NULL, NULL, NULL),
(11, 'Cap-Haitien', 3, NULL, NULL, NULL, NULL),
(12, 'Limonade', 3, NULL, NULL, NULL, NULL),
(13, 'Quartier-Morin', 3, NULL, NULL, NULL, NULL),
(14, 'Bahon', 4, NULL, NULL, NULL, NULL),
(15, 'Grande-Riviere Du Nord', 4, NULL, NULL, NULL, NULL),
(16, 'Acul-Du-Nord', 5, NULL, NULL, NULL, NULL),
(17, 'Milot', 5, NULL, NULL, NULL, NULL),
(18, 'Plaine-Du-Nord', 5, NULL, NULL, NULL, NULL),
(19, 'Bas-Limbe', 6, NULL, NULL, NULL, NULL),
(20, 'Limbe', 6, NULL, NULL, NULL, NULL),
(21, 'Pilate', 7, NULL, NULL, NULL, NULL),
(22, 'Plaisance', 7, NULL, NULL, NULL, NULL),
(23, 'La Victoire', 8, NULL, NULL, NULL, NULL),
(24, 'Pignon', 8, NULL, NULL, NULL, NULL),
(25, 'Ranquitte', 8, NULL, NULL, NULL, NULL),
(26, 'Dondon', 8, NULL, NULL, NULL, NULL),
(27, 'Saint-Raphael', 8, NULL, NULL, NULL, NULL),
(28, 'Arcahaie', 9, NULL, NULL, NULL, NULL),
(29, 'Cabaret', 9, NULL, NULL, NULL, NULL),
(30, 'Cornillon', 10, NULL, NULL, NULL, NULL),
(31, 'Croix Des Bouquets', 10, NULL, NULL, NULL, NULL),
(32, 'Thomazeau', 10, NULL, NULL, NULL, NULL),
(33, 'Fonds-Verrettes', 10, NULL, NULL, NULL, NULL),
(34, 'Ganthier', 10, NULL, NULL, NULL, NULL),
(35, 'Anse-A-Galets', 11, NULL, NULL, NULL, NULL),
(36, 'Pointe-A-Raquette', 11, NULL, NULL, NULL, NULL),
(37, 'Grand-Goave', 12, NULL, NULL, NULL, NULL),
(38, 'Leogane', 12, NULL, NULL, NULL, NULL),
(39, 'Petit-Goave', 12, NULL, NULL, NULL, NULL),
(40, 'Desdunes', 13, NULL, NULL, NULL, NULL),
(41, 'Dessalines', 13, NULL, NULL, NULL, NULL),
(42, 'Grande-Saline', 13, NULL, NULL, NULL, NULL),
(43, 'Petite-Riviere De L\'Artibonite', 13, NULL, NULL, NULL, NULL),
(44, 'Ennery', 14, NULL, NULL, NULL, NULL),
(45, 'Gonaives', 14, NULL, NULL, NULL, NULL),
(46, 'L\'Estere', 14, NULL, NULL, NULL, NULL),
(47, 'Gros-Morne', 15, NULL, NULL, NULL, NULL),
(48, 'Anse-Rouge', 15, NULL, NULL, NULL, NULL),
(49, 'Terre-Neuve', 15, NULL, NULL, NULL, NULL),
(50, 'Marmelade', 16, NULL, NULL, NULL, NULL),
(51, 'Saint-Michel De L\'Attalaye', 16, NULL, NULL, NULL, NULL),
(52, 'La Chapelle', 17, NULL, NULL, NULL, NULL),
(53, 'Saint-Marc', 17, NULL, NULL, NULL, NULL),
(54, 'Verrettes', 17, NULL, NULL, NULL, NULL),
(55, 'Cerca-La-Source', 18, NULL, NULL, NULL, NULL),
(56, 'Thomassique', 18, NULL, NULL, NULL, NULL),
(57, 'Cerca-Carvajal', 19, NULL, NULL, NULL, NULL),
(58, 'Hinche', 19, NULL, NULL, NULL, NULL),
(59, 'Maissade', 19, NULL, NULL, NULL, NULL),
(60, 'Thomonde', 19, NULL, NULL, NULL, NULL),
(61, 'Belladere', 20, NULL, NULL, NULL, NULL),
(62, 'Lascahobas', 20, NULL, NULL, NULL, NULL),
(63, 'Savanette', 20, NULL, NULL, NULL, NULL),
(64, 'Boucan-Carre', 21, NULL, NULL, NULL, NULL),
(65, 'Mirebalais', 21, NULL, NULL, NULL, NULL),
(66, 'Saut-D\'Eau', 21, NULL, NULL, NULL, NULL),
(67, 'Anse D\'Hainault', 22, NULL, NULL, NULL, NULL),
(68, 'Les Irois', 22, NULL, NULL, NULL, NULL),
(69, 'Dame-Marie', 22, NULL, NULL, NULL, NULL),
(70, 'Corail', 23, NULL, NULL, NULL, NULL),
(71, 'Roseaux', 23, NULL, NULL, NULL, NULL),
(72, 'Beaumont', 23, NULL, NULL, NULL, NULL),
(73, 'Pestel', 23, NULL, NULL, NULL, NULL),
(74, 'Abricots', 24, NULL, NULL, NULL, NULL),
(75, 'Bonbon', 24, NULL, NULL, NULL, NULL),
(76, 'Jeremie', 24, NULL, NULL, NULL, NULL),
(77, 'Chambellan', 24, NULL, NULL, NULL, NULL),
(78, 'Moron', 24, NULL, NULL, NULL, NULL),
(79, 'Anse-A-Veau', 25, NULL, NULL, NULL, NULL),
(80, 'Arnaud', 25, NULL, NULL, NULL, NULL),
(81, 'L\'Asile', 25, NULL, NULL, NULL, NULL),
(82, 'Petit-Trou De Nippes', 25, NULL, NULL, NULL, NULL),
(83, 'Plaisance Du Sud', 25, NULL, NULL, NULL, NULL),
(84, 'Baraderes', 26, NULL, NULL, NULL, NULL),
(85, 'Grand-Boucan', 26, NULL, NULL, NULL, NULL),
(86, 'Fonds-Des-Negres', 27, NULL, NULL, NULL, NULL),
(87, 'Miragoane', 27, NULL, NULL, NULL, NULL),
(88, 'Paillant', 27, NULL, NULL, NULL, NULL),
(89, 'Petite Riviere De Nippes', 27, NULL, NULL, NULL, NULL),
(90, 'Ferrier', 28, NULL, NULL, NULL, NULL),
(91, 'Fort-Liberte', 28, NULL, NULL, NULL, NULL),
(92, 'Perches', 28, NULL, NULL, NULL, NULL),
(93, 'Capotille', 29, NULL, NULL, NULL, NULL),
(94, 'Mont-Organise', 29, NULL, NULL, NULL, NULL),
(95, 'Ouanaminthe', 29, NULL, NULL, NULL, NULL),
(96, 'Sainte-Suzanne', 30, NULL, NULL, NULL, NULL),
(97, 'Terrier-Rouge', 30, NULL, NULL, NULL, NULL),
(98, 'Caracol', 30, NULL, NULL, NULL, NULL),
(99, 'Trou-Du-Nord', 30, NULL, NULL, NULL, NULL),
(100, 'Carice', 31, NULL, NULL, NULL, NULL),
(101, 'Mombin Crochu', 31, NULL, NULL, NULL, NULL),
(102, 'Vallieres', 31, NULL, NULL, NULL, NULL),
(103, 'Baie-De-Henne', 32, NULL, NULL, NULL, NULL),
(104, 'Bombardopolis', 32, NULL, NULL, NULL, NULL),
(105, 'Jean Rabel', 32, NULL, NULL, NULL, NULL),
(106, 'Mole Saint-Nicolas', 32, NULL, NULL, NULL, NULL),
(107, 'Bassin Bleu', 33, NULL, NULL, NULL, NULL),
(108, 'Chansolme', 33, NULL, NULL, NULL, NULL),
(109, 'La Tortue', 33, NULL, NULL, NULL, NULL),
(110, 'Port-De-Paix', 33, NULL, NULL, NULL, NULL),
(111, 'Anse-A-Foleur', 34, NULL, NULL, NULL, NULL),
(112, 'Saint-Louis Du Nord', 34, NULL, NULL, NULL, NULL),
(113, 'Aquin', 35, NULL, NULL, NULL, NULL),
(114, 'Cavaillon', 35, NULL, NULL, NULL, NULL),
(115, 'Saint-Louis Du Sud', 35, NULL, NULL, NULL, NULL),
(116, 'Chardonnieres', 36, NULL, NULL, NULL, NULL),
(117, 'Les Anglais', 36, NULL, NULL, NULL, NULL),
(118, 'Tiburon', 36, NULL, NULL, NULL, NULL),
(119, 'Coteaux', 37, NULL, NULL, NULL, NULL),
(120, 'Port-A-Piment', 37, NULL, NULL, NULL, NULL),
(121, 'Roche-A-Bateau', 37, NULL, NULL, NULL, NULL),
(122, 'Camp-Perrin', 38, NULL, NULL, NULL, NULL),
(123, 'Maniche', 38, NULL, NULL, NULL, NULL),
(124, 'Cayes', 38, NULL, NULL, NULL, NULL),
(125, 'Ile-A-Vache', 38, NULL, NULL, NULL, NULL),
(126, 'Chantal', 38, NULL, NULL, NULL, NULL),
(127, 'Torbeck', 38, NULL, NULL, NULL, NULL),
(128, 'Port-Salut', 39, NULL, NULL, NULL, NULL),
(129, 'Arniquet', 39, NULL, NULL, NULL, NULL),
(130, 'Saint-Jean Du Sud', 39, NULL, NULL, NULL, NULL),
(131, 'Bainet', 40, NULL, NULL, NULL, NULL),
(132, 'Cotes De Fer', 40, NULL, NULL, NULL, NULL),
(133, 'Anse-A-Pitre', 41, NULL, NULL, NULL, NULL),
(134, 'Belle-Anse', 41, NULL, NULL, NULL, NULL),
(135, 'Grand-Gosier', 41, NULL, NULL, NULL, NULL),
(136, 'Thiotte', 41, NULL, NULL, NULL, NULL),
(137, 'Jacmel', 42, NULL, NULL, NULL, NULL),
(138, 'La Vallee De Jacmel', 42, NULL, NULL, NULL, NULL),
(139, 'Cayes-Jacmel', 42, NULL, NULL, NULL, NULL),
(140, 'Marigot', 42, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_album_cat`
--

CREATE TABLE `cms_album_cat` (
  `id` int(11) NOT NULL,
  `album_name` varchar(255) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_album_cat`
--

INSERT INTO `cms_album_cat` (`id`, `album_name`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Livres', '2019-05-23 00:00:00', 'LOGIPAM', '2019-07-02 08:33:33', 'admin'),
(2, 'Haiti', '2019-05-23 00:00:00', 'LOGIPAM', '2019-07-02 08:33:21', 'admin'),
(3, 'Normalien', '2019-05-23 00:00:00', 'LOGIPAM', '2019-09-13 17:36:29', 'admin'),
(4, 'Technologie', '2019-05-23 00:00:00', 'LOGIPAM', '2019-07-02 08:32:46', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `cms_article`
--

CREATE TABLE `cms_article` (
  `id` int(11) NOT NULL,
  `article_title` varchar(255) NOT NULL,
  `article_description` longtext NOT NULL,
  `article_menu` int(11) DEFAULT NULL,
  `rank_article` tinyint(1) DEFAULT '0',
  `date_create` timestamp NULL DEFAULT NULL,
  `create_by` varchar(128) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `featured_image` varchar(255) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `set_position` varchar(64) DEFAULT NULL COMMENT 'box or main'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_article`
--

INSERT INTO `cms_article` (`id`, `article_title`, `article_description`, `article_menu`, `rank_article`, `date_create`, `create_by`, `is_publish`, `section`, `featured_image`, `last_update`, `set_position`) VALUES
(1, 'Bienvenue sur la démo de SIGES', '<p style="text-align:justify">Sakapf&egrave;t,</p>\r\n\r\n<p style="text-align:justify"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Cette plateforme est un espace de d&eacute;monstration qui vous permet d&rsquo;essayer SIGES. Vous pouvez vous utiliser&nbsp;les identifiants suivants pour la<strong> <em>connexion au syst&egrave;me&nbsp;: utilisateur (admin), mot de passe (admin)</em></strong>. N&rsquo;ayez pas peur de modifier les informations, comme bon vous semble, une nouvelle base est r&eacute;tablie tous les soirs.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif"><a href="http://www.logipam.com"><span style="color:#008000"><strong>Logipam</strong></span><span style="color:#008000"> </span></a>a con&ccedil;u Siges parce que de plus en plus d&rsquo;&eacute;coles en Ha&iuml;ti souhaitent utiliser des logiciels de gestion scolaire dans la perspective d&rsquo;&ecirc;tre plus efficaces. Si l&rsquo;utilisation des nouvelles technologies dans la gestion scolaire est un atout, il s&rsquo;av&egrave;re cependant que les syst&egrave;mes existant actuellement en Ha&iuml;ti sont tr&egrave;s couteux et limitent les utilisateurs dans des licences contraignantes et payantes qui emp&ecirc;chent de nombreuses institutions scolaires de s&rsquo;informatiser.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">SIGES est un syst&egrave;me int&eacute;gr&eacute; de gestion et d&rsquo;acc&egrave;s &agrave; l&rsquo;information scolaire qui r&eacute;pond aux besoins des &eacute;coles en Ha&iuml;ti en termes de <strong><em>gestion&nbsp;administrative, acad&eacute;mique, p&eacute;dagogique et financi&egrave;re. Siges contient aussi un portail d&rsquo;information et est parfaitement int&eacute;gr&eacute; avec PMB pour la gestion du Centre de documentation de l&rsquo;&eacute;cole.</em></strong></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">C&rsquo;est une solution en t&eacute;l&eacute;chargement gratuit, open source, disponible en fran&ccedil;ais, en cr&eacute;ole ha&iuml;tien et en anglais. Elle est personnalisable &agrave; tout type d&rsquo;&eacute;coles : primaires et secondaires ; professionnelles et techniques ; priv&eacute;es et publiques ; en milieu urbain et rural, etc.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">N.B. La plupart des photos et vid&eacute;os utilis&eacute;es ici sont gratuites et proviennent de <strong><a href="https://www.pexels.com/fr-fr/"><span style="color:#008000">pexels</span></a>.&nbsp;</strong></span></span></p>\r\n\r\n<p style="text-align:justify">Kenbe f&egrave; m,</p>\r\n\r\n<p style="text-align:justify"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Equipe de Logipam</span></span></p>\r\n', 7, 0, '2019-07-02 02:07:45', 'admin', 1, NULL, '', '2019-07-01', 'main'),
(2, 'C\'est quoi l\'open source ?', '<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Logipam et l&#39;open source en Haiti</strong></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Logipam d&eacute;cide de miser sur l&rsquo;open source et le logiciel libre depuis plusieurs ann&eacute;es parce nous croyons au potentiel de ces solutions, en ce qu&rsquo;elles sont des alternatives technologiques viables pour Ha&iuml;ti. Nous nous retrouvons totalement dans les valeurs de l&#39;Open Source : libert&eacute;, respect et ouverture ! Logipam d&eacute;veloppe et supporte SIGES depuis cinq ans.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>C&#39;est qui le logiciel open source&nbsp; ?</strong></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Voici un article de&nbsp;<a href="https://www.1min30.com/author/gabriel-dabi-schwebel" rel="author" title="Articles par Gabriel Dabi-Schwebel">Gabriel Dabi-Schwebel</a>&nbsp;sur ce sujet !</span></span></p>\r\n\r\n<h2 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">D&eacute;finition</span></span></h2>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Un&nbsp;<strong>logiciel Open Source</strong>&nbsp;est un programme informatique dont le&nbsp;<strong>code source</strong>&nbsp;est distribu&eacute; sous une licence permettant &agrave; quiconque de lire, modifier ou redistribuer ce logiciel. Il se distingue cependant du logiciel libre, au sens o&ugrave; l&rsquo;Open Source est davantage tourn&eacute; vers un objectif de d&eacute;veloppement collaboratif que vers la d&eacute;fense des libert&eacute;s individuelles. Dans la pratique, cette nuance est rarement prise en compte, et l&rsquo;usage a tendance &agrave; assimiler &ldquo;<strong>logiciel libre</strong>&rdquo; et &ldquo;logiciel Open Source&rdquo;.&nbsp;<strong>Open Source ne signifie pas non plus &ldquo;gratuit&rdquo;</strong>. Il existe de nombreux<strong>&nbsp;freewares</strong>&nbsp;dont le code source est propri&eacute;taire (il n&rsquo;est pas permis d&rsquo;y acc&eacute;der, de le modifier ou de le redistribuer).</span></span></p>\r\n\r\n<h2 style="text-align:justify"><strong><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Logiciel Open Source et D&eacute;veloppement Collaboratif</span></span></strong></h2>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">De nombreux projets Open Source sont bas&eacute;s sur&nbsp;<strong>un mod&egrave;le de d&eacute;veloppement collaboratif</strong>&nbsp;: plusieurs d&eacute;veloppeurs, r&eacute;partis partout dans le monde, participent &agrave; la cr&eacute;ation, l&rsquo;am&eacute;lioration et l&rsquo;&eacute;volution du programme, en suivant des r&egrave;gles et un processus d&rsquo;approbation d&eacute;fini &agrave; l&rsquo;avance. S&rsquo;int&eacute;grer dans une &eacute;quipe de d&eacute;veloppement sur un projet&nbsp;<strong>Open Source</strong>&nbsp;n&eacute;cessite donc de s&rsquo;appuyer sur des outils favorisant les interactions entre les d&eacute;veloppeurs, permettant la validation des modifications ou encore offrant la possibilit&eacute; de g&eacute;rer les diff&eacute;rentes versions. Dans ce domaine, de nombreux projets utilisent le c&eacute;l&egrave;bre&nbsp;<strong>CVS</strong>&nbsp;(<strong>Concurrent Versions System)</strong>. Il faut &eacute;galement choisir un h&eacute;bergement pour rendre le projet et le code accessibles &agrave; tous en phase de d&eacute;veloppement. Dans ce domaine, on peut citer l&rsquo;incontournable&nbsp;<a href="https://github.com/" rel="noopener">GitHub</a>, une des plateformes les plus utilis&eacute;es au monde.</span></span></p>\r\n\r\n<h2 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Exemples de logiciels Open Source</span></span></h2>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Il existe des milliers, voire des millions de projets de d&eacute;veloppement logiciel Open Source. Le plus embl&eacute;matique d&rsquo;entre eux est&nbsp;<strong>Linux</strong>, le noyau de nombreux&nbsp;<strong>syst&egrave;mes d&rsquo;exploitation</strong>, dont sont issus des distributions comme&nbsp;<strong>Debian</strong>,&nbsp;<strong>Ubuntu</strong>,&nbsp;<strong>Fedora</strong>&nbsp;ou&nbsp;<strong>Red Hat</strong>. Dans le domaine des&nbsp;<a href="https://www.1min30.com/dictionnaire-du-web/gestionnaire-de-contenu-cms/" title="Gestionnaire de contenu (CMS)">gestionnaires de contenus</a>,&nbsp;<strong>Joomla</strong>,&nbsp;<strong>WordPress</strong>,&nbsp;<strong>Drupal</strong>ou encore&nbsp;<strong>Plone</strong>&nbsp;sont des&nbsp;<strong>CMS Open Source</strong>.&nbsp;<strong>SugarCRM Community Edition</strong>&nbsp;est un&nbsp;<strong>CRM Open Source</strong>, tout comme&nbsp;<strong>OpenERP</strong>.</span></span></p>\r\n\r\n<h2 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Open Source, gratuit et Wikip&eacute;dia : la grande confusion</span></span></h2>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><a href="https://cdn.1min30.com/wp-content/uploads/dicoduweb/open-source.jpg"><img alt="Logiciel open-source : la grande confusion" height="190" src="https://cdn.1min30.com/wp-content/uploads/dicoduweb/open-source-600x220.jpg" style="float:left" width="520" /></a>On entend tout et n&rsquo;importe quoi sur l&rsquo;Open Source. Objet de tous les fantasmes, il est victime de nombreux clich&eacute;s. Parmi les id&eacute;es re&ccedil;ues &agrave; combattre, nous en avons s&eacute;lectionn&eacute;es quelques unes :</span></span></p>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Open Source = gratuit. Oui, sur le plan de la licence. Mais ensuite, qui va vous aider &agrave; configurer, h&eacute;berger et utiliser le syst&egrave;me ? Des prestataires. Se r&eacute;mun&egrave;rent-ils d&rsquo;amour et d&rsquo;eau fraiche ? Non. On est sur un mod&egrave;le &eacute;conomique de vente de services. Un&nbsp;<strong>projet Open Source</strong>, en particulier dans le monde professionnel, a un co&ucirc;t.</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Wikip&eacute;dia</strong>&nbsp;= Open Source. Non. Wikip&eacute;dia est un site de production collaborative de contenus. Aucun rapport logique avec le logiciel Open Source, si ce n&rsquo;est que Wikip&eacute;dia utilise le&nbsp;<strong>CMS Open Source M&eacute;diaWiki</strong>.</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Open Source = garanti de respect des libert&eacute;s individuelles. L&agrave; non plus, peu de rapport avec le sujet. C&rsquo;est plut&ocirc;t le dada des promoteurs du logiciel libre. Google Chrome est un&nbsp;<a href="https://www.1min30.com/dictionnaire-du-web/navigateur-web-browser/" title="Navigateur Web (web browser)">navigateur web</a>&nbsp;bas&eacute; sur le projet&nbsp;<strong>Open Source Chromium</strong>, mais il ne garantit pas plus qu&rsquo;<strong>Internet Explorer</strong>&nbsp;le caract&egrave;re priv&eacute; des donn&eacute;es de navigation.</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&rsquo;Open Source, c&rsquo;est pour les&nbsp;<a href="https://www.1min30.com/dictionnaire-du-web/hacker/" title="Hacker">hackers</a>&nbsp;et les geeks. Comment vous dire&hellip;</span></span></li>\r\n</ul>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif; font-size:14px">Post&eacute; par&nbsp;</span><a href="https://www.1min30.com/author/gabriel-dabi-schwebel" rel="author" style="font-size: 14px; font-family: arial, helvetica, sans-serif;" title="Articles par Gabriel Dabi-Schwebel">Gabriel Dabi-Schwebel</a></p>\r\n', 11, 0, '2019-07-02 10:07:14', 'admin', 1, NULL, 'logiciellibre.jpeg', '2019-07-02', 'main'),
(3, 'Principe de la licence GPL', '<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">La licence publique g&eacute;n&eacute;rale GNU, ou GNU General Public License (son seul nom officiel en anglais, commun&eacute;ment abr&eacute;g&eacute; GNU GPL, voire simplement &laquo; GPL &raquo;), est une licence qui fixe les conditions l&eacute;gales de distribution d&#39;un logiciel libre du projet GNU. Richard Stallman, pr&eacute;sident et fondateur de la Free Software Foundation en est l&#39;auteur. Sa derni&egrave;re version est la &laquo; GNU GPL version 3 &raquo; publi&eacute;e le 29 juin 2007 avec le concours juridique d&#39;Eben Moglen.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Cette licence a depuis &eacute;t&eacute; adopt&eacute;e, en tant que document d&eacute;finissant le mode d&#39;utilisation, donc d&#39;usage et de diffusion, par de nombreux auteurs de logiciels libres, en dehors des projets GNU.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Principe de la licence GPL&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Modifier</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">La GPL met en &oelig;uvre la notion de copyleft, un jeu de mots anglais faisant r&eacute;f&eacute;rence &agrave; la notion de copyright (right en anglais signifie &agrave; la fois le droit, c&#39;est-&agrave;-dire la r&egrave;gle juridique, et la droite, qui marque une direction) que l&#39;on peut transposer en fran&ccedil;ais en parlant de &laquo; Gauche d&#39;auteur &raquo; par r&eacute;f&eacute;rence au Droit d&#39;auteur. Pour autant le copyleft n&#39;est pas l&#39;antith&egrave;se du copyright, au contraire, puisque le premier s&#39;appuie sur le second. Ainsi le copyleft comme le copyright d&eacute;finissent et encadrent les droits des utilisateurs de fa&ccedil;on contraignante. Le m&eacute;canisme est identique, mais les objectifs diff&egrave;rent : le copyright garantit exclusivement les droits de l&#39;auteur, le copyleft s&#39;attarde tout particuli&egrave;rement aux droits des utilisateurs, et vise &agrave; pr&eacute;server la libert&eacute; d&#39;utiliser, d&#39;&eacute;tudier, de modifier et de diffuser le logiciel et ses versions d&eacute;riv&eacute;es.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">La GPL est la licence de logiciel libre la plus utilis&eacute;e. En avril 2004, 74,6 % des 23 479 projets libres list&eacute;s sur le site Freshmeat &mdash; devenu Freecode, en octobre 2011 &mdash; et 68,5 % des 52 183 projets libres list&eacute;s sur SourceForge.net &eacute;taient publi&eacute;s sous licence GPL. Certains contestent cette m&eacute;thode de mesure en affirmant qu&#39;elle rel&egrave;ve du quantitatif (nombre de projets) et ne rend pas compte du qualitatif (utilit&eacute; des logiciels), mais nul ne conteste depuis longtemps que de nombreux utilisateurs emploient une quantit&eacute; croissante de logiciels diffus&eacute;s sous GPL.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">La GNU GPL a une licence s&oelig;ur, la LGPL (GNU Lesser General Public License et plus anciennement GNU Library General Public License), une version modifi&eacute;e pour &ecirc;tre moins contraignante quant &agrave; son utilisation dans un contexte de cohabitation avec des logiciels propri&eacute;taires. Elle a une autre licence s&oelig;ur, la GFDL (GNU Free Documentation License) qui, elle, est applicable aux manuels, livres ou autres documents &eacute;crits. Cette derni&egrave;re pr&eacute;sente toutefois des inconv&eacute;nients, mis en avant par le projet Debian ; on peut choisir &agrave; sa place la GPL, qui est tout &agrave; fait applicable &agrave; un livre, article de carnet Web ou autre cr&eacute;ation.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>L&#39;esprit et l&#39;objectif&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </strong></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Modifier</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&#39;objectif de la licence GNU GPL, selon ses cr&eacute;ateurs est de garantir &agrave; l&#39;utilisateur les droits suivants (appel&eacute;s libert&eacute;s) sur un programme informatique :</span></span></p>\r\n\r\n<p style="text-align:justify">&nbsp;</p>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Libert&eacute; 0. La libert&eacute; d&#39;ex&eacute;cuter le logiciel, pour n&#39;importe quel usage ;</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Libert&eacute; 1. La libert&eacute; d&#39;&eacute;tudier le fonctionnement d&#39;un programme et de l&#39;adapter &agrave; ses besoins, ce qui passe par l&#39;acc&egrave;s aux codes sources ;</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Libert&eacute; 2. La libert&eacute; de redistribuer des copies ;</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Libert&eacute; 3. L&#39;obligation de faire b&eacute;n&eacute;ficier la communaut&eacute; des versions modifi&eacute;es.</span></span></li>\r\n</ul>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Pour la premi&egrave;re libert&eacute;, cela exclut donc toutes limitations d&#39;utilisation d&#39;un programme par rapport &agrave; l&#39;architecture (notamment le processeur et le syst&egrave;me d&#39;exploitation) ou &agrave; l&#39;utilisation qui va en &ecirc;tre faite.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">La quatri&egrave;me libert&eacute; passe par un choix : la deuxi&egrave;me autorisant de modifier un programme, il n&#39;est pas tenu de publier une version modifi&eacute;e tant qu&#39;elle est pour un usage personnel ; par contre, en cas de distribution d&#39;une version modifi&eacute;e, la quatri&egrave;me libert&eacute; am&egrave;ne l&#39;obligation &agrave; ce que les modifications soient retourn&eacute;es &agrave; la communaut&eacute; sous la m&ecirc;me licence.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-size:18px"><a href="https://fr.m.wikipedia.org/wiki/Licence_publique_g%C3%A9n%C3%A9rale_GNU"><span style="font-family:arial,helvetica,sans-serif">Wikip&eacute;dia</span></a></span></p>\r\n', 12, 0, '2019-07-02 11:07:15', 'admin', 1, NULL, 'gpl_bnr.jpg', '2019-07-02', 'main'),
(4, 'Pré-inscription aux concours d\'admission', '<h3 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&rsquo;Universit&eacute; d&rsquo;&Eacute;tat d&rsquo;Ha&iuml;ti (UEH) avise le public en g&eacute;n&eacute;ral, les postulants(es) en particulier que les inscriptions au concours d&rsquo;entr&eacute;e &agrave; l&rsquo;UEH se tiendront du 20 juillet au 24 ao&ucirc;t 2018. Les postulants(es) peuvent s&rsquo;inscrire dans trois (3) facult&eacute;s au maximum.</span></span></h3>\r\n\r\n<h3 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Conditions d&#39;admission :</span></span></h3>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>1. Ne pas &ecirc;tre d&eacute;j&agrave; &eacute;tudiants(tes) &agrave; l&rsquo;UEH ;</strong></span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>2. Constituer un dossier complet ;</strong></span></span></h4>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">2.1. D&eacute;tenir les certificats de fin d&rsquo;&eacute;tudes secondaires : BAC. I et BAC. II, Certificat du Nouveau secondaire ou Certificat du BAC. Unique;</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">2.2. &Ecirc;tre munis (es) de leur acte de Naissance ou d&rsquo;un extrait des Archives Nationales ;</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">2.3. Detenir leur matricule fiscal (NIF) ;</span></span></li>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">2.4. Pr&eacute;senter deux photos identiques et r&eacute;centes ( 6 mois) de format passeport;</span></span></li>\r\n</ul>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Etapes de l&#39;inscription :</strong></span></span></h4>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">1. Remplir un formulaire en ligne en utilisant l&rsquo;adresse suivante :&nbsp;<a href="http://admission.ueh.edu.ht/inscription">http://admission.ueh.edu.ht/inscription</a>&nbsp;.</span></span></li>\r\n</ul>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">2. Payer pour chaque entit&eacute; choisie les frais d&rsquo;inscription de 500 Gdes (Cinq cents gourdes) dans n&rsquo;importe quelle succursale de la SOGEBANK au nom du compte&nbsp;<strong>UEH/DEMANDE D&rsquo;ADMISSION</strong>, num&eacute;ro :&nbsp;<strong>706054558</strong>&nbsp;;</span></span></li>\r\n</ul>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">3. Pr&eacute;senter les pi&egrave;ces exig&eacute;es au secr&eacute;tariat de chacune des entit&eacute;s choisies entre 8h30 AM et 3h30 PM pour validation aux jours et aux dates indiqu&eacute;s ;</span></span></li>\r\n</ul>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>N.B. Au moment de l&rsquo;inscription, il faut:</strong></span></span></h4>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">a. Soumettre un dossier complet pour chaque inscription ;</span></span></li>\r\n</ul>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">b. Inscrire au dos de chaque photo : nom et pr&eacute;nom, t&eacute;l&eacute;phone et NIF ;</span></span></li>\r\n</ul>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">c. Pr&eacute;senter le formulaire d&ucirc;ment rempli en ligne ;</span></span></li>\r\n</ul>\r\n\r\n<ul>\r\n	<li style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">d. Pr&eacute;senter une fiche de d&eacute;p&ocirc;t de 500 gourdes de la SOGEBANK pour chaque entit&eacute; choisie.</span></span></li>\r\n</ul>\r\n', 10, 0, '2019-07-02 11:07:39', 'admin', 1, NULL, 'condition admission-finale.jpg', '2019-07-02', 'main'),
(5, 'Les parents et l\'école', '<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif">Pour mieux accompagner votre jeune&nbsp;dans son&nbsp;<strong>orientation scolaire&nbsp;</strong>et&nbsp;<strong>professionnelle</strong></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><em>Espace parents a &eacute;t&eacute; con&ccedil;u pour aider les parents de l&rsquo;&eacute;cole &agrave; mieux comprendre ce&nbsp;que vivent leurs jeunes en leur donnant des outils pour les accompagner dans leur d&eacute;veloppement personnel et identitaire, et ce, tout au long de leurs &eacute;tudes secondaires.</em></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif">La relation de confiance et le dialogue entre les parents et l&#39;&eacute;cole constituent un enjeu d&eacute;terminant pour la r&eacute;ussite de tous les enfants. Propices aux &eacute;changes entre les personnels et les parents, ces espaces encouragent les relations entre parents eux-m&ecirc;mes dans leur diversit&eacute;.</span></p>\r\n', 8, 0, '2019-07-02 12:07:59', 'admin', 1, NULL, 'parents.jpeg', '2019-07-02', 'main'),
(6, 'Men nou wi...', '<div class="col-sm-12 col-md-12" id="sp-component">\r\n<div class="sp-column ">\r\n<div class="blog">\r\n<div class="items-row row-0 row clearfix">\r\n<div class="col-sm-12">\r\n<div id="centercontent_both">\r\n<div class="clearpad">\r\n<div class="item-page">\r\n<p style="text-align: justify;"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">La mission de Logipam est de munir les institutions et les PME de solutions informatiques modernes, adapt&eacute;es et performantes; tout en promouvant l&rsquo;int&eacute;gration et l&rsquo;utilisation des logiciels libres en Ha&iuml;ti.</span></span></p>\r\n\r\n<p><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Logipam vous assiste dans la formation, l&#39;int&eacute;gration, l&#39;impl&eacute;mentation, l&#39;h&eacute;bergement et la prise en charge des logiciels libres propos&eacute;s.</span></span></p>\r\n\r\n<p style="text-align: justify;"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif"><strong>Nous nous sp&eacute;cialisons dans:</strong></span></span></p>\r\n\r\n<p style="text-align: justify;"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Le d&eacute;veloppement et l&rsquo;int&eacute;gration des solutions open source de gestion d&rsquo;&eacute;coles, de biblioth&egrave;ques, de centres de documentation, de cliniques avec une gamme compl&egrave;te de services pour la personnalisation, la formation du personnel et de supports. Imm&eacute;diatement apr&egrave;s son arriv&eacute;e &agrave; Logipam, le client est pris en charge par un de nos sp&eacute;cialistes qui est un point de contact, et la personne en charge de la coop&eacute;ration avec l&#39;entreprise cliente.</span></span></p>\r\n\r\n<p style="text-align: justify;"><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif"><strong>Nous savons comment:</strong></span></span></p>\r\n\r\n<ul>\r\n	<li style="text-align:justify">\r\n	<p><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Adapter les logiciels aux besoins et aux habitudes de travail de nos clients de mani&egrave;re efficace</span></span></p>\r\n	</li>\r\n	<li style="text-align:justify">\r\n	<p><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Faire fonctionner le logiciel et le mat&eacute;riel de mani&egrave;re correcte</span></span></p>\r\n	</li>\r\n	<li style="text-align:justify">\r\n	<p><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Prot&eacute;ger les solutions contre les menaces externes ou internes</span></span></p>\r\n	</li>\r\n	<li style="text-align:justify">\r\n	<p><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Former le personnel pour la prise en charge rapide des solutions</span></span></p>\r\n	</li>\r\n	<li style="text-align:justify">\r\n	<p><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Servir nos clients</span></span></p>\r\n	</li>\r\n</ul>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n</div>\r\n', 16, 0, '2019-07-03 03:07:49', 'LOGIPAM', 1, NULL, 'quisommesnous.jpeg', '2019-07-02', 'main'),
(7, 'Jeux d’esprit et jeux d\'équipe', '<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Ce programme est fond&eacute; sur le jeu, tout &agrave; la fois lib&eacute;rateur et formateur. Il fait na&icirc;tre au travers des joyeux plaisirs qu&rsquo;il procure un &eacute;tat de confiance, suscite une prise de risque, engage une libert&eacute; nouvelle, un esprit souple et inventif rendant les participants disponibles &agrave; ce qui les environne et aptes &agrave; g&eacute;rer ensemble les situations les plus inattendues.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Les outils utilis&eacute;s :</strong></span></span></h4>\r\n\r\n<ul>\r\n	<li style="text-align: justify;">\r\n	<h4><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">relaxation collective active, &ecirc;tre &agrave; soi pour mieux &ecirc;tre &agrave; l&rsquo;autre.</span></span></h4>\r\n	</li>\r\n	<li style="text-align: justify;">\r\n	<h4><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">assouplissement corporel et &eacute;veil psychophysique, acqu&eacute;rir une vitalit&eacute; physique et mentale et &ecirc;tre ainsi ouvert et r&eacute;actif.</span></span></h4>\r\n	</li>\r\n	<li style="text-align: justify;">\r\n	<h4><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">pratique de jeux collectifs d&eacute;veloppe les qualit&eacute;s primordiales &agrave; la constitution et la bonne marche d&rsquo;une &eacute;quipe: plaisir, humour, complicit&eacute;, r&eacute;activit&eacute;, inventivit&eacute;, strat&eacute;gie.</span></span></h4>\r\n	</li>\r\n	<li style="text-align: justify;">\r\n	<h4><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">improvisations, jeux de r&ocirc;les dont les th&eacute;matiques sont tir&eacute;es de la vie de l&rsquo;entreprise.</span></span></h4>\r\n	</li>\r\n	<li style="text-align: justify;">\r\n	<h4><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">temps de partage.</span></span></h4>\r\n	</li>\r\n</ul>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>M&eacute;thode p&eacute;dagogique</strong></span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Ce stage met en jeu des qualit&eacute;s tr&egrave;s sp&eacute;cifiques que l&rsquo;on trouve dans les jeux collectifs d&rsquo;enfants et leurs r&egrave;gles fondamentales &agrave; la constitution d&rsquo;une &eacute;quipe solidaire et efficiente.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">En pr&eacute;ambule un entra&icirc;nement corporel personnel est accompli ensemble, primordial pour rendre disponible le corps et l&rsquo;esprit et susciter le d&eacute;sir de cr&eacute;er un groupe et de travailler ensemble dans la joie&hellip;Ensuite des jeux physiques pour sortir des habitudes corporelles, stimuler son corps et son esprit, traverser les peurs et les limites physiques, partager audace et &eacute;lan de jeu en groupe, soutenir et &ecirc;tre soutenu dans ces exp&eacute;riences.Les improvisations sont des occasions de cr&eacute;er ensemble des histoires et les jouer, les principes dramatiques sont &eacute;prouv&eacute;s pour construire la situation. Un temps de partage cl&ocirc;ture chaque journ&eacute;e r&eacute;serv&eacute; &agrave; la parole personnelle, &agrave; l&rsquo;analyse de son engagement et de son parcours.</span></span></h4>\r\n\r\n<p><u><span style="color:#006400"><span style="font-size:16px"><span style="font-family:arial,helvetica,sans-serif"><strong>Le sport &agrave; l&#39;&eacute;cole</strong></span></span></span></u></p>\r\n\r\n<p style="text-align:center"><img alt="" height="415" src="/siges/cms_files/images/Soccer.jpeg" width="993" /></p>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Le programme sportif &agrave; l&#39;&eacute;cole D&eacute;mo de SIGES permet &agrave; tous les &eacute;l&egrave;ves, de la petite maternelle &agrave; la terminale, de d&eacute;velopper des habilet&eacute;s motrices dans une atmosph&egrave;re multiculturelle unique sur Delmas.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>L&#39;&eacute;ducation physique et sportive</strong></span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&rsquo;&eacute;ducation physique et sportive est enseign&eacute;e &agrave; chaque niveau de classe au Lycee. Le programme d&rsquo;EPS propose un enseignement vari&eacute; &agrave; travers de nombreuses activit&eacute;s physiques et sportives individuelles et collectives. Chaque &eacute;l&egrave;ve apprend &agrave; se g&eacute;rer physiquement tout en acqu&eacute;rant de nombreuses comp&eacute;tences sportives et sociales, afin de devenir un citoyen du monde, bilingue, sportivement &eacute;duqu&eacute;.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Les Grenadiers</strong></span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Le programme athl&eacute;tique des Grenadiers, ouvert &agrave; tous, offre aux &eacute;l&egrave;ves un ensemble de 24 &eacute;quipes de sports &agrave; partir desquelles les athl&egrave;tes vont pouvoir se sp&eacute;cialiser et vivre une exp&eacute;rience comp&eacute;titive exceptionnelle au cours des trois saisons d&rsquo;automne, d&rsquo;hiver et de printemps. Nos diff&eacute;rentes &eacute;quipes coll&eacute;giennes et lyc&eacute;ennes obtiennent chaque ann&eacute;e d&rsquo;excellents r&eacute;sultats dans leurs championnats respectifs, compos&eacute;s des meilleures &eacute;coles priv&eacute;es de Chicago et ses environs. Nos entraineurs, tous professionnels, s&rsquo;efforcent d&rsquo;inspirer nos jeunes athl&egrave;tes tant sur le plan &eacute;ducatif que comp&eacute;titif afin de prolonger l&rsquo;exp&eacute;rience p&eacute;dagogique du LFC, bas&eacute;e sur le respect, la responsabilit&eacute; et le plaisir d&rsquo;apprendre.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Des touts petits aux plus grands, des parents aux enseignants, les Flames entretiennent avec fiert&eacute; la flamme de notre &eacute;tablissement.</span></span></h4>\r\n', 13, 0, '2019-07-03 03:07:02', 'LOGIPAM', 1, NULL, 'Scrabble.jpeg', '2019-07-03', 'main'),
(8, 'Assistance scolaire, émotionnelle ou de socialisation', '<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Service d&rsquo;aide aux &eacute;l&egrave;ves</span></span></h4>\r\n\r\n<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Ce service a pour but d&rsquo;assurer le succ&egrave;s scolaire de votre (vos) enfant(s). Notre &eacute;quipe est compos&eacute;e de deux enseignantes sp&eacute;cialistes, une enseignante sp&eacute;cialiste de la lecture et de deux psychologues scolaires. Ces sp&eacute;cialistes travaillent en collaboration avec les parents et les enseignants afin d&rsquo;identifier les &eacute;l&egrave;ves ayant besoin d&rsquo;assistance. Notre &eacute;quipe assiste les &eacute;l&egrave;ves &agrave; l&rsquo;int&eacute;rieur et &agrave; l&rsquo;ext&eacute;rieur de la classe en &eacute;tablissant le meilleur moyen de leur venir en aide. Au coll&egrave;ge et au lyc&eacute;e, les sp&eacute;cialistes travaillent avec la CPE et/ou la conseill&egrave;re d&rsquo;orientation.</span></span></h4>\r\n\r\n<h4><strong><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Assistance d&rsquo;ordre scolaire, &eacute;motionnelle ou de socialisation</span></span></strong></h4>\r\n\r\n<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Lorsqu&rsquo;un &eacute;l&egrave;ve a besoin d&rsquo;assistance dans ces domaines chaque cas est trait&eacute; individuellement. Les sp&eacute;cialistes du Service d&rsquo;aide aux &eacute;l&egrave;ves facilitent la coop&eacute;ration entre enseignants, &eacute;l&egrave;ves, familles et sp&eacute;cialistes ext&eacute;rieurs (si besoin est) afin de mettre en place les plans d&rsquo;intervention et de soutien adapt&eacute;s qui permettent la r&eacute;ussite des &eacute;l&egrave;ves.</span></span></h4>\r\n\r\n<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">La mission du Service d&rsquo;aide aux &eacute;l&egrave;ves du LFC est de proposer des services d&rsquo;assistance et de pr&eacute;vention qui favorisent le d&eacute;veloppement scolaire, social et &eacute;motionnel de tous les &eacute;l&egrave;ves. En coop&eacute;ration avec les &eacute;l&egrave;ves, enseignants et les parents, les conseill&egrave;res aident chaque &eacute;l&egrave;ve &agrave; acqu&eacute;rir les comp&eacute;tences requises pour devenir des citoyens du monde responsables et l&rsquo;acquisition de ces comp&eacute;tences. Les conseill&egrave;res scolaires sont des professionnelles qui apportent un soutien et des ressources permettant de faciliter la r&eacute;ussite des &eacute;l&egrave;ves. Elles proposent des programmes de pr&eacute;vention enseignant &agrave; chaque &eacute;l&egrave;ve comment faire face &agrave; des difficult&eacute;s &eacute;ventuelles.</span></span></h4>\r\n\r\n<h4><strong><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">&Eacute;valuations, tests et recommandations</span></span></strong></h4>\r\n\r\n<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Notre &eacute;quipe de sp&eacute;cialistes dispose d&rsquo;une longue liste de professionnels pouvant assurer une large gamme de services, qu&rsquo;il s&rsquo;agisse d&rsquo;&eacute;valuations diagnostiques ou de consultations &agrave; long terme. Lorsqu&rsquo;une &eacute;valuation ou l&rsquo;intervention d&rsquo;un autre professionnel s&rsquo;av&egrave;re n&eacute;cessaire, notre &eacute;quipe recommandera un certain nombre de consultants aux parents concern&eacute;s. Les sp&eacute;cialistes auront &eacute;galement recours &agrave; des &eacute;valuations diagnostiques afin d&rsquo;identifier les points forts et les faiblesses des &eacute;l&egrave;ves pour leur prodiguer l&rsquo;assistance la mieux adapt&eacute;e &agrave; leurs besoins.</span></span></h4>\r\n\r\n<h4><strong><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Proc&eacute;dure g&eacute;n&eacute;rale</span></span></strong></h4>\r\n\r\n<ul>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Demande de l&rsquo;enseignant, du parent ou de l&rsquo;&eacute;l&egrave;ve ;</span></span></h4>\r\n	</li>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Observation et/ou &eacute;valuation par la sp&eacute;cialiste suivie d&rsquo;un examen de la situation avec tous les enseignants int&eacute;ress&eacute;s ainsi que le Directeur ;</span></span></h4>\r\n	</li>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Notification des parents suivie d&rsquo;un entretien pour &eacute;changer des informations et discuter des strat&eacute;gies envisageables ;&nbsp;</span></span></h4>\r\n	</li>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Assistance apport&eacute;e &agrave; l&rsquo;&eacute;l&egrave;ve : services personnalis&eacute;s ou en petits groupes, accommodations sp&eacute;ciales et strat&eacute;gies</span></span></h4>\r\n	</li>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Coordination et r&eacute;unions de suivi.</span></span></h4>\r\n	</li>\r\n</ul>\r\n\r\n<h4><strong><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Si n&eacute;cessaire :</span></span></strong></h4>\r\n\r\n<ul>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">R&eacute;union en pr&eacute;sence des enseignants, des parents, de la/des sp&eacute;cialistes, des Directeurs et, le cas &eacute;ch&eacute;ant, des sp&eacute;cialistes ext&eacute;rieurs afin de coordonner la mise en oeuvre du plan d&rsquo;action &eacute;ventuellement retenu ;</span></span></h4>\r\n	</li>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Demande d&rsquo;&eacute;valuations et observations compl&eacute;mentaires (un certain nombre d&rsquo;&eacute;valuations diagnostiques sont offertes au Lyc&eacute;e) ;</span></span></h4>\r\n	</li>\r\n	<li>\r\n	<h4><span style="font-size:14px"><span style="font-family:arial,helvetica,sans-serif">Mise en place d&rsquo;un plan d&rsquo;action individualis&eacute;.</span></span></h4>\r\n	</li>\r\n</ul>\r\n', 14, 0, '2019-07-03 04:07:37', 'LOGIPAM', 0, NULL, 'aide.jpeg', '2019-09-13', 'main'),
(9, 'Quand et comment venir au CDI?', '<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Ouvert &agrave; tous les &eacute;l&egrave;ves, aux enseignants et aux parents d&rsquo;&eacute;l&egrave;ves, le CDI joue pleinement son r&ocirc;le d&rsquo;outil p&eacute;dagogique au service de la communaut&eacute;.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Lundi - vendredi :&nbsp; 8:15 am - 4:30 pm</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>El&egrave;ves et professeurs</strong></span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les &eacute;l&egrave;ves du primaire viennent biblioth&egrave;que accompagn&eacute;s de leur professeur, une fois toutes les deux semaines au minimum. Ils peuvent &eacute;galement venir apr&egrave;s la fin des cours avec leurs parents.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les &eacute;l&egrave;ves du coll&egrave;ge qui souhaitent aller au CDI pendant une heure d&#39;&eacute;tudes doivent pr&eacute;alablement demander l&#39;autorisation &agrave; un des personnels de la vie scolaire.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Lyc&eacute;e</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les &eacute;l&egrave;ves du lyc&eacute;e sont les bienvenus et sont encourag&eacute;s &agrave; venir au CDI pendant leurs heures de libre.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Parents</strong></span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les parents sont &eacute;galement les bienvenus. Ils peuvent avoir leur propre compte afin d&rsquo;emprunter des livres et des DVD pour leurs enfants ou pour leur propre usage.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Dons</strong></span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Notre biblioth&egrave;que accepte toutes les donations. Certains ouvrages sont soit int&eacute;gr&eacute;s dans le fonds de la biblioth&egrave;que, distribu&eacute;s dans des classes (ouvrages en double ou sp&eacute;cialis&eacute;s) ou encore revendus lors de nos &eacute;ventuelles foires aux livres.</span></span></h4>\r\n\r\n<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Le personnel</strong></span></span></h4>\r\n\r\n<ul>\r\n	<li>\r\n	<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Un professeur documentaliste &agrave; temps plein.</span></span></h4>\r\n	</li>\r\n	<li>\r\n	<h4 style="text-align: justify;"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Une aide documentaliste &agrave; mi-temps, qui enseigne &eacute;galement la musique au lyc&eacute;e pour les classes de l&rsquo;&eacute;cole primaire.</span></span></h4>\r\n	</li>\r\n</ul>\r\n', 15, 0, '2019-07-03 04:07:19', 'LOGIPAM', 1, NULL, 'library.jpeg', NULL, 'main');
INSERT INTO `cms_article` (`id`, `article_title`, `article_description`, `article_menu`, `rank_article`, `date_create`, `create_by`, `is_publish`, `section`, `featured_image`, `last_update`, `set_position`) VALUES
(10, 'Le mouvement du logiciel libre', '<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&#39;histoire du logiciel libre est intimement imbriqu&eacute;e avec celle de l&#39;informatique et celle du g&eacute;nie logiciel. Elle ne commence en tant que telle qu&#39;&agrave; partir du moment o&ugrave; est apparu le besoin de distinguer le logiciel libre du logiciel propri&eacute;taire, selon le principe &eacute;nonc&eacute; pour Le Cru et le Cuit.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Ces pr&eacute;mices datant de la fin du XXe si&egrave;cle, il ne peut s&#39;agir d&#39;une discipline acad&eacute;mique. L&#39;histoire du logiciel libre est donc pr&eacute;sent&eacute;e ici de fa&ccedil;on informelle.</span></span><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Un mouvement social s&#39;est petit &agrave; petit constitu&eacute; pour faire &eacute;voluer les droits que les utilisateurs ont sur le logiciel afin d&#39;acc&eacute;der &agrave; la libre circulation des informations dans ce domaine.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><strong><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les id&eacute;es</span></span></strong></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Exposition des probl&egrave;mes</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Richard Stallman explique dans Pourquoi les logiciels ne doivent pas avoir de propri&eacute;taire une &laquo; analogie abusive avec les objets mat&eacute;riels3. &raquo;. Pour expliquer ce qui sera repris dans la notion de biens rivaux il y utilise la m&eacute;taphore culinaire (qu&#39;il reprendra r&eacute;guli&egrave;rement4) pour dissocier le plat qui, s&#39;il est pris par quelqu&#39;un d&#39;autre peut entra&icirc;ner un manque, et la recette de cuisine. Partager la recette selon lui ne peut &ecirc;tre que b&eacute;n&eacute;fique, puisque cela ne cr&eacute;e pas de manque, et qu&#39;au contraire peut profiter &agrave; celui qui la donne s&#39;il peut profiter &agrave; son tour des am&eacute;liorations apport&eacute;es.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Il pr&eacute;cise dans le &quot;manifeste GNU&quot; que&nbsp; &laquo; Extraire de l&#39;argent des utilisateurs d&#39;un programme en restreignant leur utilisation du programme est destructif parce que, au bout du compte, cela r&eacute;duit la quantit&eacute; de richesse que l&#39;humanit&eacute; tire du programme. {&hellip;} C&#39;est la morale kantienne, ou r&egrave;gle d&#39;or. Puisque je n&#39;aime pas la situation qui r&eacute;sulte d&#39;une r&eacute;tention g&eacute;n&eacute;rale de l&#39;information, il me revient de consid&eacute;rer comme immoral d&#39;agir ainsi5. &raquo; Le probl&egrave;me n&#39;&eacute;tait donc pas seulement de permettre la r&eacute;utilisation du code source, mais de faire en sorte que l&#39;information qu&#39;il contient reste en libre circulation, et que les avantages qu&#39;il procure restent librement utilisables.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Stallman pr&eacute;cise les raisons de cette diff&eacute;renciation6 : de son point de vue, ramen&eacute;s &agrave; quelques concepts &eacute;l&eacute;mentaires les logiciels sont des id&eacute;es, et on peut les d&eacute;couper en logiques simples dont le nombre devient alors limit&eacute;.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">De ce point de vue une partie de code r&eacute;utilisable peut se trouver en situation de ne pouvoir fonctionner qu&#39;associ&eacute; &agrave; des &eacute;l&eacute;ments propri&eacute;taire. Dans ce cas, son utilisation dans un contexte fonctionnel est donc soumis aux limites de copyright des &eacute;l&eacute;ments propri&eacute;taires indissociables.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><strong><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Solution utilis&eacute;e</span></span></strong></h4>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Consid&eacute;rant que le syst&egrave;me d&#39;exploitation est un &eacute;l&eacute;ment strat&eacute;gique indispensable, il entreprit en 1983 la conception d&#39;un syst&egrave;me d&#39;exploitation libre, GNU, auquel il se consacra enti&egrave;rement &agrave; partir de 1984 apr&egrave;s avoir d&eacute;missionn&eacute; du MIT.</span></span></p>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Pour mettre en place son projet GNU, il dut pr&eacute;ciser les principes du logiciel libre d&eacute;compos&eacute;s en quatre libert&eacute;s fondamentales : celle d&#39;utiliser, d&#39;&eacute;tudier, de modifier et de redistribuer des versions modifi&eacute;es.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><strong><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Le copyleft</span></span></strong></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les principes &eacute;nonc&eacute;s pour le logiciel libre &eacute;taient d&eacute;j&agrave; existant dans le cadre universitaire par exemple, mais pour d&eacute;velopper GNU, les contributeurs int&eacute;ress&eacute;s par le projet (et Richard Stallman lui-m&ecirc;me) voulaient &ecirc;tre certain que leurs apports resteraient libres.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&#39;id&eacute;e d&#39;interdire d&#39;interdire sera alors exprim&eacute;e par le copyleft (ou gauche d&#39;auteur7) dont la finalit&eacute; est clairement expos&eacute;e dans le manifeste GNU :&laquo; L&#39;id&eacute;e centrale du gauche d&#39;auteur est de donner &agrave; quiconque la permission d&#39;ex&eacute;cuter le programme, de le copier, de le modifier, et d&#39;en distribuer des versions modifi&eacute;es - mais pas la permission d&#39;ajouter des restrictions de son cru. C&#39;est ainsi que les libert&eacute;s cruciales qui d&eacute;finissent le &laquo; logiciel libre &raquo; sont garanties pour quiconque en poss&egrave;de une copie; elles deviennent des droits inali&eacute;nables8. &raquo;</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Ce copyleft va au-del&agrave; des principes du logiciel libre en les imposant d&eacute;finitivement &agrave; tout ce qui est cr&eacute;&eacute; &agrave; partir d&#39;&eacute;l&eacute;ments copyleft&eacute;s. Il a alors &eacute;t&eacute; qualifi&eacute; viral et la question de savoir si appliquer le copyleft rend le logiciel plus libre ou moins libre est l&#39;un des d&eacute;bats sans fin les plus courants (voir troll).</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><strong><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les Licences GNU</span></span></strong></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Ce principe de copyleft dut s&#39;inscrire dans le cadre l&eacute;gal pour &ecirc;tre utilisable. C&#39;est avec Eben Moglen que sont mises au point les licences qui pr&eacute;cisent sur le principe du droit d&#39;auteur les conditions d&#39;utilisation que celui-ci impose aux utilisateurs, en l&#39;occurrence la p&eacute;rennit&eacute; des principes du logiciel libre.</span></span></h4>\r\n\r\n<h4 style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Les licences GNU font leur apparition avec la licence GNU Emacs de 1985. Mais cette licence manque de formalisme juridique, ne s&#39;applique qu&#39;au logiciel GNU Emacs, et exige l&#39;envoi de toute modification du code au projet GNU, ce qui cr&eacute;e une hi&eacute;rarchisation de fait entre programmeurs. Stallman s&#39;attache donc rapidement &agrave; &eacute;laborer une licence mieux r&eacute;dig&eacute;e, plus &eacute;galitaire et d&#39;un objet plus g&eacute;n&eacute;ral, la GPL. Celle-ci va &eacute;voluer sur plusieurs ann&eacute;es, jusqu&#39;&agrave; la version 1.0 de 1989. La GNU General Public License sera rapidement accompagn&eacute; de versions adapt&eacute;es &agrave; des cas particulier, la LGPL (GNU Lesser General Public License et la GFDL (GNU Free Documentation License).</span></span></h4>\r\n', 17, 0, '2019-07-05 22:07:11', 'admin', 1, NULL, 'Richard_Stallman.jpeg', '2019-07-05', 'main'),
(11, 'Notes de versions', '<p>Nous sommes heureux de pr&eacute;senter la derni&egrave;re version SIGES 2.0. &Agrave; chaque version, nous am&eacute;liorons des fonctionnalit&eacute;s et en introduisons de nouvelles afin de vous offrir une meilleure exp&eacute;rience utilisateur. Avec cette version, nous apportons des fonctionnalit&eacute;s essentielles que vous nous avez demand&eacute;es &ndash; Module Kindergarten, Tableau d&rsquo;honneur, Gall&eacute;rie Album Photos, Importations CSV, Cartes &eacute;tudiants. Et nous apportons aussi quelques am&eacute;liorations &agrave; plusieurs fonctionnalit&eacute;s comme le palmar&egrave;s, le choix de la taille des bulletins et palmar&egrave;s &agrave; imprimer, l&rsquo;admission en ligne, le calendrier, Badges, Economat, discipline, choix de th&egrave;mes, etc.</p>\r\n\r\n<p>Cette fois, nous d&eacute;dions cette version de SIGES &agrave; l&rsquo;oiseau z&ograve;tolan (<em>Columbina passerine)&nbsp;</em>; le plus petit oiseau (18 cm) de la famille des pigeons, tourterelles, ramiers, etc. C&rsquo;est un oiseau qui embellit nos compagnes et qui rend service &agrave; nos planteurs en les d&eacute;barrassant des mauvaises herbes.</p>\r\n\r\n<p>Prot&eacute;geons-le&nbsp;!</p>\r\n\r\n<p><strong>Nouveaut&eacute;s de siges 2.0</strong></p>\r\n\r\n<ul>\r\n	<li>Dashboard principal avec tableau d&rsquo;honneur</li>\r\n	<li>Gestion de Pr&eacute;scolaire/Kindergarten\r\n	<ul style="list-style-type:circle">\r\n		<li>&nbsp;</li>\r\n		<li>Listes enfants/Jardini&egrave;res</li>\r\n		<li>Bulletins personnalis&eacute;s</li>\r\n	</ul>\r\n	</li>\r\n	<li>Impression de cartes d&rsquo;&eacute;l&egrave;ves\r\n	<ul style="list-style-type:circle">\r\n		<li>Choix de couleurs personnalis&eacute;es</li>\r\n		<li>Choix d&rsquo;orientation de cartes</li>\r\n	</ul>\r\n	</li>\r\n	<li>\r\n	<ul style="list-style-type:circle">\r\n		<li>&nbsp;</li>\r\n		<li>Option de saisie d&rsquo;infraction multiple</li>\r\n	</ul>\r\n	</li>\r\n	<li>\r\n	<ul style="list-style-type:circle">\r\n		<li>&nbsp;</li>\r\n		<li>Option de saisie de recette multiple</li>\r\n	</ul>\r\n	</li>\r\n	<li>Cr&eacute;ation de rapports personnalisables\r\n	<ul style="list-style-type:circle">\r\n		<li>Option d&eacute;veloppeur&nbsp;/ Requ&ecirc;te Mysql</li>\r\n	</ul>\r\n	</li>\r\n	<li>\r\n	<ul style="list-style-type:circle">\r\n		<li>Inscription en ligne</li>\r\n		<li>Publication de r&eacute;sultats sur le portail</li>\r\n	</ul>\r\n	</li>\r\n	<li>Album photos</li>\r\n	<li>Page Aide</li>\r\n</ul>\r\n\r\n<p><strong>Fonctionnalit&eacute;s am&eacute;lior&eacute;es</strong></p>\r\n\r\n<ul>\r\n	<li>Articles en vedette\r\n	<ul style="list-style-type:circle">\r\n		<li>Blocs annonce</li>\r\n		<li>Ev&egrave;nements &agrave; venir et calendrier</li>\r\n	</ul>\r\n	</li>\r\n	<li>Panorama classe</li>\r\n	<li>\r\n	<ul style="list-style-type:circle">\r\n		<li>Ajout de l&eacute;gende</li>\r\n		<li>Choix d&rsquo;orientation et de choix de taille de papier</li>\r\n	</ul>\r\n	</li>\r\n	<li>\r\n	<ul style="list-style-type:circle">\r\n		<li>Choix de th&egrave;mes (Yucca, Belle, Kitty, Kira et SIGES)</li>\r\n		<li>Choix de langue</li>\r\n		<li>Choix d&rsquo;ann&eacute;es acad&eacute;miques ant&eacute;rieures</li>\r\n	</ul>\r\n	</li>\r\n	<li>Configuration g&eacute;n&eacute;rale enrichie et am&eacute;lior&eacute;e</li>\r\n	<li>Choix dans l&rsquo;affichage ou pas de certains modules</li>\r\n	<li>Choix dans l&rsquo;affichage ou pas de certains &eacute;l&eacute;ments par d&eacute;faut</li>\r\n	<li>Am&eacute;lioration dans la prise de d&eacute;cision de fin d&rsquo;ann&eacute;e</li>\r\n	<li>Autres anomalies corrig&eacute;es</li>\r\n</ul>\r\n', 13, 0, '2019-08-18 20:08:33', 'admin', 1, NULL, 'gpl_bnr.jpg', '2019-09-16', 'main'),
(12, 'Article pour Djuly', '<div class="intro">\r\n<p><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Un accord &laquo;&nbsp;<em>historique</em>&nbsp;&raquo; selon Bercy. Le g&eacute;ant am&eacute;ricain de l&#39;internet &eacute;tait vis&eacute; par une enqu&ecirc;te pour fraude fiscale. Google a accept&eacute; ce jeudi 12 septembre de verser pr&egrave;s d&#39;un milliard d&#39;euros pour solder l&#39;ensemble de ses contentieux avec le fisc fran&ccedil;ais. Ce qui revient au passage &agrave; reconna&icirc;tre que les faits reproch&eacute;s peuvent correspondre au d&eacute;lit de fraude &agrave; l&#39;imp&ocirc;t sur les soci&eacute;t&eacute;s.</span></span></p>\r\n</div>\r\n\r\n<div>\r\n<p><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&#39;accord est en deux parties. La multinationale a accept&eacute; d&#39;une part de payer 500 millions d&#39;euros d&#39;amende pour mettre un terme &agrave; une enqu&ecirc;te du <a href="http://www.tribunal-de-paris.justice.fr/75/le-pnf" target="_blank"><strong>parquet national financier </strong></a>(PNF), et d&#39;autre part de verser 465 millions d&#39;euros de rattrapage fiscal pour clore les proc&eacute;dures de redressement engag&eacute;es &agrave; son encontre. &laquo;&nbsp;<em>Cet accord est historique, &agrave; la fois pour nos finances publiques et parce qu&#39;il marque la fin d&#39;une &eacute;poque</em>&nbsp;&raquo;, a salu&eacute; le ministre de l&#39;Action et des Comptes publics G&eacute;rald Darmanin dans un communiqu&eacute;. &laquo;&nbsp;<em>Nous restons persuad&eacute;s qu&#39;une r&eacute;forme coordonn&eacute;e du syst&egrave;me fiscal international est la meilleure fa&ccedil;on d&#39;offrir un cadre clair aux entreprises op&eacute;rant dans le monde entier</em>&nbsp;&raquo;, a r&eacute;agi de son c&ocirc;t&eacute; le groupe am&eacute;ricain.</span></span></p>\r\n\r\n<p><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Google, qui &eacute;tait dans le <a href="http://www.rfi.fr/economie/20160225-france-le-fisc-reclame-16-milllard-euros-google-impots" target="_blank"><strong>collimateur du fisc fran&ccedil;ais</strong></a> depuis plusieurs ann&eacute;es, a d&eacute;j&agrave; pass&eacute; de tels accords &agrave; l&#39;&eacute;tranger. <a href="http://www.rfi.fr/economie/20160123-google-rattrape-fisc-britannique-optimisation-fiscale" target="_blank"><strong>Notamment au Royaume-Uni</strong></a> et en Italie, o&ugrave; il a d&eacute;bours&eacute; plusieurs centaines de millions d&#39;euros pour obtenir un abandon des poursuites.</span></span></p>\r\n</div>\r\n', 13, 0, '2019-09-12 17:09:58', 'admin', 1, NULL, 'biblio.jpeg', NULL, 'main'),
(13, 'Le président ukrainien', '<div class="intro">\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Le pr&eacute;sident ukrainien Volodymyr Zelensky a mis en garde vendredi les Occidentaux contre toute lev&eacute;e des sanctions frappant Moscou pour l&#39;annexion de la Crim&eacute;e et son soutien aux s&eacute;paratistes prorusses, au moment o&ugrave; Paris op&egrave;re un rapprochement avec la Russie.</span></span></p>\r\n</div>\r\n\r\n<div>\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">&laquo;&nbsp;<em>Les sanctions sont une arme diplomatique puissante et efficace. Je ne me lasse pas de r&eacute;p&eacute;ter &agrave; nos partenaires occidentaux qui nous aident mais pensent parfois &agrave; la lev&eacute;e des sanctions: vous perdez de l&#39;argent, vraiment? Mais nous, nous perdons des gens</em>&nbsp;&raquo;, a lanc&eacute; M. Zelensky en r&eacute;f&eacute;rence aux milliers de <a href="http://www.rfi.fr/europe/20190626-est-ukraine-belligerants-desengagement-historique" target="_blank"><strong>victimes de la guerre</strong></a> dans l&#39;est de l&#39;Ukraine, d&eacute;clench&eacute;e en 2014.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Il a estim&eacute; que les sanctions &laquo;&nbsp;<em>sont un imp&ocirc;t pour le maintien de la paix</em>&nbsp;&raquo;. &laquo;&nbsp;<em>Tant qu&#39;elle n&#39;a pas &eacute;t&eacute; restaur&eacute;e, les sanctions doivent &ecirc;tre pr&eacute;serv&eacute;es</em>&nbsp;&raquo;, a-t-il poursuivi dans un discours lors d&#39;un Forum &agrave; Kiev, organis&eacute; par la fondation de <a href="http://www.rfi.fr/europe/20120929-ukraine-liberte-presse-mise-epreuve-le-pouvoir" target="_blank"><strong>l&#39;oligarque Viktor Pinchuk</strong></a>.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><strong>Les repr&eacute;sailles de la Russie</strong></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px"><a href="http://www.rfi.fr/europe/20190907-ukraine-russie-echange-prisonniers-sommet-paix-premier-pas-poutine-macron" target="_blank"><strong>Les relations entre Kiev et Moscou </strong></a>traversent une crise sans pr&eacute;c&eacute;dent depuis l&#39;annexion en 2014 par la Russie de la p&eacute;ninsule ukrainienne de Crim&eacute;e, suivie par le d&eacute;clenchement d&#39;une guerre avec des s&eacute;paratistes prorusses dans l&#39;est de l&#39;Ukraine, qui a fait pr&egrave;s de <a href="http://www.rfi.fr/europe/20190911-crimee-russie-ukraine-toujours-guerre-devant-cedh" target="_blank"><strong>13&nbsp;000 morts</strong></a>.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Kiev et les Occidentaux accusent Moscou de soutenir militairement les s&eacute;paratistes en leur fournissant des combattants et des armes, ce que la Russie d&eacute;ment en d&eacute;pit des constatations de plusieurs m&eacute;dias dont l&#39;AFP.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">L&#39;Union europ&eacute;enne a <a href="http://europa.eu/newsroom/highlights/special-coverage/eu-sanctions-against-russia-over-ukraine-crisis_fr" target="_blank"><strong>prolong&eacute; jeudi de six mois les sanctions</strong></a> &agrave; l&#39;encontre de responsables russes et ukrainiens pour leur implication dans le conflit en Ukraine.</span></span></p>\r\n\r\n<p style="text-align:justify"><span style="font-family:arial,helvetica,sans-serif"><span style="font-size:14px">Ces sanctions, renouvel&eacute;es depuis cinq ans, s&#39;accompagnent de restrictions &eacute;conomiques dans les secteurs du p&eacute;trole, de la d&eacute;fense et des banques russes. Moscou avait adopt&eacute;, en repr&eacute;sailles, un embargo sur la plupart des produits alimentaires europ&eacute;ens, portant un coup au secteur agroalimentaire de plusieurs pays dont la France.</span></span></p>\r\n</div>\r\n', 8, 0, '2019-09-13 17:09:44', 'admin', 1, NULL, 'Ekri-2.jpeg', '2019-09-16', 'main');

-- --------------------------------------------------------

--
-- Table structure for table `cms_doc`
--

CREATE TABLE `cms_doc` (
  `id` int(11) NOT NULL,
  `document_name` varchar(128) NOT NULL,
  `document_title` varchar(128) NOT NULL,
  `document_description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_image`
--

CREATE TABLE `cms_image` (
  `id` int(11) NOT NULL,
  `label_image` varchar(255) NOT NULL,
  `type_image` varchar(64) NOT NULL COMMENT 'Carrousel or Logo',
  `nom_image` varchar(255) NOT NULL,
  `is_publish` tinyint(1) NOT NULL,
  `album` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_image`
--

INSERT INTO `cms_image` (`id`, `label_image`, `type_image`, `nom_image`, `is_publish`, `album`) VALUES
(15, 'tech3.', 'album', 'tech3.jpeg', 1, 4),
(16, 'tech2.', 'album', 'tech2.jpeg', 1, 4),
(17, 'tech1.', 'album', 'tech1.jpeg', 1, 4),
(18, 'tech4.', 'album', 'tech4.jpeg', 1, 4),
(19, 'dog1.', 'album', 'dog1.jpeg', 1, 3),
(20, 'book2.', 'album', 'book2.jpeg', 1, 1),
(21, 'book1.', 'album', 'book1.jpeg', 1, 1),
(22, 'book.', 'album', 'book.jpeg', 1, 1),
(23, 'book3.', 'album', 'book3.jpeg', 1, 1),
(25, 'dog4.', 'album', 'dog4.jpeg', 1, 3),
(26, 'dog2.', 'album', 'dog2.jpeg', 1, 3),
(27, 'dog3.', 'album', 'dog3.jpeg', 1, 3),
(28, 'hirondelle.', 'album', 'hirondelle.jpg', 1, 2),
(29, 'tourisme1.', 'album', 'tourisme1.jpg', 1, 2),
(31, 'tourisme2.', 'album', 'tourisme2.jpg', 1, 2),
(32, 'tourisme 3.', 'album', 'tourisme_3.jpg', 1, 2),
(34, 'zotolan-1.', 'carrousel', 'zotolan-1.jpg', 1, NULL),
(36, 'bbbbelfoto.', 'carrousel', 'bbbbelfoto.jpg', 1, NULL),
(39, 'plante.', 'carrousel', 'plante.jpg', 1, NULL),
(42, 'woman-notebook-working-girl.', 'album', 'woman-notebook-working-girl.jpg', 1, 2),
(43, 'open-source-word-cloud.', 'album', 'open-source-word-cloud.jpg', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menu`
--

CREATE TABLE `cms_menu` (
  `id` int(11) NOT NULL,
  `menu_label` varchar(64) NOT NULL,
  `menu_position` int(11) DEFAULT NULL,
  `is_home` tinyint(1) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  `is_parent_menu` tinyint(1) DEFAULT NULL,
  `parent_menu` int(11) DEFAULT NULL,
  `is_special` tinyint(1) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_menu`
--

INSERT INTO `cms_menu` (`id`, `menu_label`, `menu_position`, `is_home`, `is_publish`, `is_parent_menu`, `parent_menu`, `is_special`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(3, 'Qui sommes nous', 1, NULL, 1, 1, NULL, NULL, NULL, '2016-05-16 21:29:58', NULL, 'admin'),
(4, 'Actualités', 0, NULL, 1, 1, NULL, NULL, NULL, '2016-05-16 21:30:02', NULL, 'admin'),
(5, 'Admission', 2, NULL, 0, 0, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 'Vie étudiante', 3, NULL, 1, 1, NULL, NULL, NULL, '2018-08-03 03:10:22', NULL, 'LOGIPAM'),
(7, 'Accueil', 0, 1, 1, NULL, NULL, 1, NULL, NULL, NULL, NULL),
(8, 'Espace parents', NULL, NULL, 1, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'LOGIPAM', NULL),
(10, 'Condition d\'admission', 0, 1, 1, NULL, NULL, 1, '2019-06-27 00:00:00', NULL, 'LOGIPAM', NULL),
(11, 'Open source en Haiti', NULL, NULL, 1, 0, 4, NULL, NULL, NULL, NULL, NULL),
(12, 'Licence GPL', NULL, NULL, 1, 0, 4, NULL, NULL, NULL, NULL, NULL),
(13, 'Activités parascolaires', NULL, NULL, 1, 0, 6, NULL, NULL, NULL, NULL, NULL),
(14, 'Service d\' aide aux élèves', NULL, NULL, 1, 0, 6, NULL, NULL, NULL, NULL, NULL),
(15, 'Bibliothèque', NULL, NULL, 1, 0, 6, NULL, NULL, NULL, NULL, NULL),
(16, 'Notre mission', NULL, NULL, 1, 0, 3, NULL, NULL, NULL, NULL, NULL),
(17, 'Notre histoire', NULL, NULL, 1, 0, 3, NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_section`
--

CREATE TABLE `cms_section` (
  `id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `create_by` varchar(128) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `code`
--
CREATE TABLE `code` (
`code_id` varchar(15)
,`id` int(11)
);

-- --------------------------------------------------------

--
-- Table structure for table `contact_info`
--

CREATE TABLE `contact_info` (
  `id` int(11) NOT NULL,
  `person` int(11) NOT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `contact_relationship` int(11) DEFAULT NULL,
  `profession` varchar(100) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `one_more` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contact_info`
--

INSERT INTO `contact_info` (`id`, `person`, `contact_name`, `contact_relationship`, `profession`, `phone`, `address`, `email`, `date_created`, `date_updated`, `create_by`, `update_by`, `one_more`) VALUES
(1, 210, 'Jean Gilels', 1, '', '', '', 'jcpoulard@gmail.com', '2019-07-30 00:00:00', NULL, 'LOGIPAM', NULL, 0),
(2, 228, 'MIchele Jeanty', 3, 'Marchande', '3456789', '', 'jcpoulard@gmail.com', '2019-08-05 00:00:00', NULL, 'admin', NULL, 0),
(3, 65, 'MIchele Jeanty', 3, 'Marchande', '3456789', '', 'jcpoulard@gmail.com', '2019-08-05 00:00:00', NULL, 'admin', NULL, 2),
(4, 55, 'MIchele Jeanty', 3, 'Marchande', '3456789', '', 'jcpoulard@gmail.com', '2019-08-07 00:00:00', NULL, 'admin', NULL, 2),
(5, 241, 'Albert Buron', 7, 'Enseignant', '+50934356765', '24, Rue Bourjolly, Fontamara 27', 'metminwi@gmail.com', '2019-08-31 00:00:00', NULL, 'admin', NULL, 0),
(6, 262, 'Judith Larose', 3, '', '38594497', '39, Rue Brutus, Fontamara 27', 'jlarose2@gmail.com', '2019-09-01 00:00:00', NULL, 'admin', NULL, 0),
(7, 140, 'Pierre Paul', 12, 'Tailleur', '509356782', '', '', '2019-09-01 00:00:00', NULL, 'admin', NULL, 0),
(8, 243, 'Jude LOUIS', 1, '', '37452204', '39, Rue Brutus, Fontamara 27', 'judelouis113@gmail.com', '2019-09-01 00:00:00', NULL, 'admin', NULL, 0),
(9, 246, 'Carl Henry GUILLAUME', 1, 'Employe Capital Bank', '38032690', 'Delmas 60 Musseau', 'carlhguil1362@yahoo.fr', '2019-09-01 00:00:00', NULL, 'admin', NULL, 0),
(10, 252, 'Darline ETIENNE', 3, 'Employe Matelec', '47194780', 'Delmas 32 Rue Acacia # 26', '', '2019-09-01 00:00:00', NULL, 'admin', NULL, 0),
(11, 266, 'Jean Pierre GEORGES', 1, 'Informaticien', '35125990', '12, Rue Nicolas, Port-au-Prince', 'jcpoulard@gmail.com', '2019-09-06 00:00:00', NULL, 'admin', NULL, 0),
(12, 91, 'Jean Pierre GEORGES', 1, 'Informaticien', '35125990', '12, Rue Nicolas, Port-au-Prince', 'jcpoulard@gmail.com', '2019-09-06 00:00:00', NULL, 'admin', NULL, 11);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `weight` float DEFAULT NULL COMMENT 'Weight : Le coefficient du cours',
  `homework_weight` float DEFAULT NULL,
  `debase` tinyint(1) NOT NULL DEFAULT '0',
  `optional` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0: not optional; 1: optional(reportcard will not take care about))',
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old ; 1: new, in use',
  `reference_id` int(11) DEFAULT NULL COMMENT 'id kou li ranplase a ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `subject`, `teacher`, `room`, `academic_period`, `weight`, `homework_weight`, `debase`, `optional`, `old_new`, `reference_id`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 77, 7, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(2, 70, 47, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(3, 8, 43, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(4, 68, 30, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(5, 1, 6, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(6, 99, 49, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(7, 10, 21, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(8, 92, 50, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(9, 105, 47, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(10, 108, 40, 2, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:15', NULL, 'admin', NULL),
(11, 68, 34, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(12, 87, 10, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(13, 116, 26, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(14, 100, 36, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(15, 101, 36, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(16, 87, 26, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(17, 99, 34, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(18, 99, 25, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(19, 103, 26, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(20, 6, 31, 3, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:04', NULL, 'admin', NULL),
(21, 87, 18, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(22, 100, 36, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(23, 86, 22, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(24, 84, 6, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(25, 72, 27, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(26, 98, 8, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(27, 116, 32, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(28, 99, 27, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(29, 71, 47, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(30, 97, 43, 1, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:26', NULL, 'admin', NULL),
(31, 6, 38, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(32, 69, 46, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(33, 87, 18, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(34, 105, 22, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(35, 104, 32, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(36, 80, 40, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(37, 99, 47, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(38, 100, 3, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(39, 112, 34, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(40, 109, 19, 5, 1, 100, NULL, 0, 0, 1, NULL, '2019-07-03 20:07:38', NULL, 'admin', NULL),
(41, 77, 5, 1, 1, 150, NULL, 0, 0, 1, NULL, '2019-08-02 00:00:00', NULL, 'LOGIPAM', NULL),
(42, 77, 218, 14, 1, 150, NULL, 0, 0, 1, NULL, '2019-08-05 00:00:00', NULL, 'admin', NULL),
(43, 79, 218, 14, 1, 100, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:39', NULL, 'admin', NULL),
(44, 123, 218, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 00:00:00', NULL, 'admin', NULL),
(45, 2, 221, 14, 1, 200, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:02', NULL, 'admin', NULL),
(46, 1, 221, 14, 1, 200, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:02', NULL, 'admin', NULL),
(47, 86, 221, 14, 1, 100, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:03', NULL, 'admin', NULL),
(48, 6, 221, 14, 1, 200, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:03', NULL, 'admin', NULL),
(49, 122, 226, 14, 1, 150, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:18', NULL, 'admin', NULL),
(50, 8, 226, 14, 1, 75, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:18', NULL, 'admin', NULL),
(51, 104, 226, 14, 1, 75, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:18', NULL, 'admin', NULL),
(52, 121, 219, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:05', NULL, 'admin', NULL),
(53, 99, 219, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:05', NULL, 'admin', NULL),
(54, 120, 219, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:05', NULL, 'admin', NULL),
(55, 98, 219, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:05', NULL, 'admin', NULL),
(56, 118, 220, 14, 1, 100, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:11', NULL, 'admin', NULL),
(57, 119, 220, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:11', NULL, 'admin', NULL),
(58, 74, 220, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:11', NULL, 'admin', NULL),
(59, 72, 220, 14, 1, 100, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:11', NULL, 'admin', NULL),
(60, 75, 220, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:11', NULL, 'admin', NULL),
(61, 73, 220, 14, 1, 50, NULL, 0, 0, 1, NULL, '2019-08-05 13:08:11', NULL, 'admin', NULL),
(62, 77, 7, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(63, 70, 47, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(64, 8, 43, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(65, 68, 30, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(66, 1, 6, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(67, 99, 49, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(68, 10, 21, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(69, 92, 50, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(70, 105, 47, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(71, 108, 40, 2, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(72, 68, 34, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(73, 87, 10, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(74, 116, 26, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(75, 100, 36, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(76, 101, 36, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(77, 87, 26, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(78, 99, 34, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(79, 99, 25, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(80, 103, 26, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(81, 6, 31, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(82, 87, 18, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(83, 100, 36, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(84, 86, 22, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(85, 84, 6, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(86, 72, 27, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(87, 98, 8, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(88, 116, 32, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(89, 99, 27, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(90, 71, 47, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(91, 97, 43, 1, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(92, 6, 38, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(93, 69, 46, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(94, 87, 18, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(95, 105, 22, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(96, 104, 32, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(97, 80, 40, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(98, 99, 47, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(99, 100, 3, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(100, 112, 34, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(101, 109, 19, 5, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(102, 77, 5, 1, 6, 150, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(103, 77, 218, 14, 6, 150, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(104, 79, 218, 14, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(105, 123, 218, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(106, 2, 221, 14, 6, 200, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(107, 1, 221, 14, 6, 200, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(108, 86, 221, 14, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(109, 6, 221, 14, 6, 200, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(110, 122, 226, 14, 6, 150, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(111, 8, 226, 14, 6, 75, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(112, 104, 226, 14, 6, 75, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(113, 121, 219, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(114, 99, 219, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(115, 120, 219, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(116, 98, 219, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(117, 118, 220, 14, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(118, 119, 220, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(119, 74, 220, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(120, 72, 220, 14, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(121, 75, 220, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(122, 73, 220, 14, 6, 50, NULL, 0, 0, 1, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(123, 99, 255, 4, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(124, 1, 254, 3, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(125, 2, 256, 4, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(126, 97, 259, 4, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(127, 59, 258, 4, 6, 200, NULL, 0, 0, 1, NULL, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(128, 87, 257, 4, 6, 100, NULL, 0, 0, 1, NULL, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(129, 1, 254, 4, 6, 200, NULL, 0, 0, 1, NULL, '2019-08-31 00:00:00', NULL, 'admin', NULL),
(130, 127, 271, 12, 6, 200, NULL, 0, 0, 1, NULL, '2019-09-05 00:00:00', NULL, 'admin', NULL),
(131, 1, 272, 12, 6, 200, NULL, 0, 0, 1, NULL, '2019-09-05 15:09:05', NULL, 'admin', NULL),
(132, 116, 273, 12, 6, 200, NULL, 0, 0, 1, NULL, '2019-09-05 15:09:05', NULL, 'admin', NULL),
(133, 124, 274, 12, 6, 100, NULL, 0, 0, 1, NULL, '2019-09-05 15:09:16', NULL, 'admin', NULL),
(134, 115, 33, 12, 6, 200, NULL, 0, 0, 1, NULL, '2019-09-05 15:09:16', NULL, 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_field`
--

CREATE TABLE `custom_field` (
  `id` int(11) NOT NULL,
  `field_name` varchar(64) NOT NULL,
  `field_label` varchar(45) DEFAULT NULL,
  `field_type` varchar(45) DEFAULT 'text',
  `value_type` varchar(16) DEFAULT NULL,
  `field_option` text,
  `field_related_to` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `custom_field`
--

INSERT INTO `custom_field` (`id`, `field_name`, `field_label`, `field_type`, `value_type`, `field_option`, `field_related_to`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'religion', 'Religion de l\'élève', 'combo', NULL, 'Catholique,Protestant,Musulman,Voudouissant,Boudhiste,Autres, Aucune', 'stud', NULL, NULL, NULL, NULL),
(2, 'date_de_bapteme', 'Date de Bapteme', 'date', NULL, NULL, 'stud', NULL, NULL, NULL, NULL),
(3, 'Predisposition_Pathologique__', 'Predisposition Pathologique ', 'txt', NULL, NULL, 'stud', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_data`
--

CREATE TABLE `custom_field_data` (
  `id` bigint(20) NOT NULL,
  `field_link` int(11) DEFAULT NULL,
  `field_data` text,
  `object_id` int(11) DEFAULT NULL,
  `object_type` varchar(12) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `custom_field_data`
--

INSERT INTO `custom_field_data` (`id`, `field_link`, `field_data`, `object_id`, `object_type`) VALUES
(1, 1, 'Boudhiste', 6, 'post'),
(2, 2, '2007-12-11', 6, 'post'),
(3, 1, 'Catholique', 266, NULL),
(4, 2, '', 266, NULL),
(5, 1, 'Catholique', 267, NULL),
(6, 2, '', 267, NULL),
(7, 1, 'Catholique', 268, NULL),
(8, 2, '', 268, NULL),
(9, 1, 'Catholique', 269, NULL),
(10, 2, '', 269, NULL),
(11, 1, 'Catholique', 270, NULL),
(12, 2, '', 270, NULL),
(13, 1, '', 275, NULL),
(14, 2, '', 275, NULL),
(15, 1, 'Protestant', 7, 'post'),
(16, 2, '', 7, 'post'),
(17, 1, 'Catholique', 329, NULL),
(18, 2, '', 329, NULL),
(19, 3, '', 329, NULL),
(20, 1, 'Catholique', 330, NULL),
(21, 2, '', 330, NULL),
(22, 3, '', 330, NULL),
(23, 1, 'Catholique', 331, NULL),
(24, 2, '', 331, NULL),
(25, 3, '', 331, NULL),
(26, 1, 'Catholique', 332, NULL),
(27, 2, '', 332, NULL),
(28, 3, '', 332, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cycles`
--

CREATE TABLE `cycles` (
  `id` int(11) NOT NULL,
  `cycle_description` varchar(100) NOT NULL,
  `average_base` int(5) DEFAULT NULL COMMENT 'si li null pran sa ki nan generalconfig la',
  `academic_year` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cycles`
--

INSERT INTO `cycles` (`id`, `cycle_description`, `average_base`, `academic_year`) VALUES
(1, 'Cycle III', NULL, 6),
(2, 'Secondaire', NULL, 6),
(3, 'Cycle II', NULL, 6),
(4, 'Prescolaire', NULL, 6),
(5, 'Cycle I', NULL, 6);

-- --------------------------------------------------------

--
-- Table structure for table `decision_finale`
--

CREATE TABLE `decision_finale` (
  `id` bigint(20) NOT NULL,
  `student` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `general_average` float DEFAULT NULL,
  `mention` varchar(45) DEFAULT NULL,
  `report_mention` int(11) DEFAULT NULL,
  `comments` varchar(128) DEFAULT NULL,
  `is_move_to_next_year` tinyint(1) DEFAULT NULL,
  `current_level` int(11) DEFAULT NULL,
  `next_level` int(11) DEFAULT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: poko pase migrasyon; 1: pase migrasyon deja',
  `create_by` varchar(100) DEFAULT NULL,
  `update_by` varchar(100) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `decision_finale`
--

INSERT INTO `decision_finale` (`id`, `student`, `academic_year`, `general_average`, `mention`, `report_mention`, `comments`, `is_move_to_next_year`, `current_level`, `next_level`, `checked`, `create_by`, `update_by`, `date_created`, `date_updated`) VALUES
(1, 55, 1, 78.35, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(2, 66, 1, 81.82, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(3, 60, 1, 81.4, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(4, 73, 1, 82.64, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(5, 59, 1, 82.14, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(6, 75, 1, 80.04, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(7, 72, 1, 83.83, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(8, 67, 1, 83.95, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(9, 69, 1, 86.69, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(10, 54, 1, 82.34, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(11, 76, 1, 82.62, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(12, 56, 1, 85.61, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(13, 63, 1, 84.19, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(14, 58, 1, 82.11, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(15, 57, 1, 79.7, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(16, 65, 1, 79.95, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(17, 62, 1, 78.98, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(18, 68, 1, 81.87, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(19, 74, 1, 77.82, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(20, 52, 1, 78.36, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(21, 71, 1, 79.16, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(22, 64, 1, 81.57, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(23, 61, 1, 77.03, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(24, 70, 1, 81.96, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:39:40', '2019-08-10 21:39:40'),
(25, 229, 1, 86.08, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-09-05 00:00:00'),
(26, 232, 1, 82.5, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-09-05 00:00:00'),
(27, 231, 1, 68.48, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(28, 230, 1, 71.44, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(29, 233, 1, 67.57, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(30, 236, 1, 67.33, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(31, 238, 1, 78.33, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(32, 239, 1, 43.37, 'À refaire à l\'école', 5, '', 0, 1, 1, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(33, 234, 1, 68.31, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(34, 237, 1, 66.78, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(35, 235, 1, 73.52, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(36, 228, 1, 62.36, 'Admis(e) en classe supérieure', 1, '', 1, 1, 2, 1, 'Admin', 'Admin', '2019-08-10 21:45:40', '2019-08-10 21:45:40'),
(37, 88, 1, 81.2, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(38, 90, 1, 85.1, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(39, 77, 1, 85.75, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(40, 91, 1, 89.8, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(41, 82, 1, 82.25, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(42, 92, 1, 85.4, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(43, 85, 1, 79.2, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(44, 87, 1, 82.9, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(45, 93, 1, 82.95, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(46, 94, 1, 78.45, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(47, 78, 1, 78.3, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(48, 79, 1, 81.35, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(49, 95, 1, 80.75, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(50, 81, 1, 75.2, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(51, 96, 1, 74.5, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(52, 97, 1, 74.75, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(53, 98, 1, 74.4, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(54, 99, 1, 76.35, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(55, 100, 1, 80.2, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(56, 101, 1, 77.3, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(57, 86, 1, 81.6, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(58, 89, 1, 77.55, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:19', '2019-08-10 21:52:19'),
(59, 83, 1, 82.8, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:20', '2019-08-10 21:52:20'),
(60, 84, 1, 78, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:20', '2019-08-10 21:52:20'),
(61, 80, 1, 81.65, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:20', '2019-08-10 21:52:20'),
(62, 102, 1, 80.65, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:20', '2019-08-10 21:52:20'),
(63, 103, 1, 73.1, 'Admis(e) en classe supérieure', 1, '', 1, 2, 3, 1, 'Admin', 'Admin', '2019-08-10 21:52:20', '2019-08-10 21:52:20'),
(64, 106, 1, 81.5, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(65, 114, 1, 81.3, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(66, 110, 1, 84.55, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(67, 118, 1, 80.4, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(68, 119, 1, 83.25, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(69, 113, 1, 86.6, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(70, 107, 1, 83.85, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(71, 111, 1, 79.7, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(72, 120, 1, 78.8, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(73, 121, 1, 74.1, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(74, 112, 1, 69.4, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(75, 116, 1, 71.75, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(76, 122, 1, 71.5, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(77, 115, 1, 77.1, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(78, 123, 1, 71.8, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(79, 124, 1, 76.6, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(80, 109, 1, 81.35, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(81, 126, 1, 79.35, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(82, 117, 1, 82, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(83, 127, 1, 77.25, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(84, 128, 1, 75.5, 'Admis(e) en classe supérieure', 1, '', 1, 3, 4, 1, 'Admin', 'Admin', '2019-08-10 21:57:47', '2019-08-10 21:57:47'),
(85, 132, 1, 82.6, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:58', '2019-08-10 22:01:58'),
(86, 154, 1, 88.65, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:58', '2019-08-10 22:01:58'),
(87, 140, 1, 89, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:58', '2019-08-10 22:01:58'),
(88, 144, 1, 90, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:58', '2019-08-10 22:01:58'),
(89, 145, 1, 88.2, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:59', '2019-08-10 22:01:59'),
(90, 148, 1, 80.3, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:59', '2019-08-10 22:01:59'),
(91, 149, 1, 84.65, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:59', '2019-08-10 22:01:59'),
(92, 143, 1, 81.8, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:59', '2019-08-10 22:01:59'),
(93, 153, 1, 73.65, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:59', '2019-08-10 22:01:59'),
(94, 137, 1, 79.55, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:59', '2019-08-10 22:01:59'),
(95, 147, 1, 81.9, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:01:59', '2019-08-10 22:01:59'),
(96, 129, 1, 80.15, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:00', '2019-08-10 22:02:00'),
(97, 151, 1, 80.25, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:00', '2019-08-10 22:02:00'),
(98, 130, 1, 75.5, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:00', '2019-08-10 22:02:00'),
(99, 138, 1, 78.95, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:00', '2019-08-10 22:02:00'),
(100, 150, 1, 78.05, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:00', '2019-08-10 22:02:00'),
(101, 141, 1, 79.6, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:00', '2019-08-10 22:02:00'),
(102, 133, 1, 70.85, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:01', '2019-08-10 22:02:01'),
(103, 152, 1, 78.1, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:01', '2019-08-10 22:02:01'),
(104, 142, 1, 77.65, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:01', '2019-08-10 22:02:01'),
(105, 146, 1, 68.55, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:01', '2019-08-10 22:02:01'),
(106, 135, 1, 68.35, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:01', '2019-08-10 22:02:01'),
(107, 139, 1, 70.25, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:01', '2019-08-10 22:02:01'),
(108, 134, 1, 75.7, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:01', '2019-08-10 22:02:01'),
(109, 136, 1, 77.95, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:02', '2019-08-10 22:02:02'),
(110, 131, 1, 79.6, 'Admis(e) en classe supérieure', 1, '', 1, 4, 9, 1, 'Admin', 'Admin', '2019-08-10 22:02:02', '2019-08-10 22:02:02');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Nord', NULL, NULL, '', ''),
(2, 'Ouest', NULL, NULL, '', ''),
(3, 'Artibonite', NULL, NULL, NULL, NULL),
(4, 'Centre', NULL, NULL, NULL, NULL),
(5, 'Grand\'Anse', NULL, NULL, NULL, NULL),
(6, 'Nippes', NULL, NULL, NULL, NULL),
(7, 'Nord-Est', NULL, NULL, NULL, NULL),
(8, 'Nord-Ouest', NULL, NULL, NULL, NULL),
(9, 'Sud', NULL, NULL, NULL, NULL),
(10, 'Sud-Est', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department_has_person`
--

CREATE TABLE `department_has_person` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `updated_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department_in_school`
--

CREATE TABLE `department_in_school` (
  `id` int(11) NOT NULL,
  `department_name` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `updated_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `devises`
--

CREATE TABLE `devises` (
  `id` int(11) NOT NULL,
  `devise_name` varchar(45) NOT NULL,
  `devise_symbol` varchar(45) NOT NULL,
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `devises`
--

INSERT INTO `devises` (`id`, `devise_name`, `devise_symbol`, `description`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'Gourde', 'HTG', '', NULL, NULL, NULL, NULL),
(2, 'Dollar', 'USD', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `devoirs`
--

CREATE TABLE `devoirs` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `grade_value` float DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `employee_info`
--

CREATE TABLE `employee_info` (
  `id` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `country_of_birth` varchar(45) NOT NULL,
  `university_or_school` varchar(45) DEFAULT NULL,
  `number_of_year_of_study` int(11) DEFAULT NULL,
  `field_study` int(11) DEFAULT NULL,
  `qualification` int(11) DEFAULT NULL,
  `job_status` int(11) DEFAULT NULL,
  `permis_enseignant` varchar(45) NOT NULL,
  `leaving_date` date DEFAULT NULL,
  `comments` text,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee_info`
--

INSERT INTO `employee_info` (`id`, `employee`, `hire_date`, `country_of_birth`, `university_or_school`, `number_of_year_of_study`, `field_study`, `qualification`, `job_status`, `permis_enseignant`, `leaving_date`, `comments`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 8, '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '2019-07-02 00:00:00', '2019-07-02 00:00:00', 'admin', NULL),
(2, 15, '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '2019-07-02 00:00:00', '2019-07-02 00:00:00', 'admin', NULL),
(3, 33, '0000-00-00', '', NULL, NULL, NULL, NULL, 1, '', NULL, NULL, '2019-07-07 00:00:00', '2019-07-07 00:00:00', 'admin', NULL),
(4, 37, '0000-00-00', '', NULL, NULL, NULL, NULL, 1, '', NULL, NULL, '2019-08-07 00:00:00', '2019-08-07 00:00:00', 'admin', NULL),
(5, 17, '0000-00-00', '', NULL, NULL, NULL, NULL, 1, '', NULL, NULL, '2019-08-07 00:00:00', '2019-08-07 00:00:00', 'admin', NULL),
(6, 40, '0000-00-00', '', NULL, NULL, NULL, NULL, 1, '', NULL, NULL, '2019-08-07 00:00:00', '2019-08-07 00:00:00', 'admin', NULL),
(7, 49, '0000-00-00', '', NULL, NULL, NULL, NULL, 4, '', NULL, NULL, '2019-08-07 00:00:00', '2019-08-07 00:00:00', 'admin', NULL),
(8, 20, '0000-00-00', '', NULL, NULL, NULL, NULL, 3, '', NULL, NULL, '2019-08-07 00:00:00', '2019-08-07 00:00:00', 'admin', NULL),
(9, 225, '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '2019-08-25 00:00:00', '2019-08-25 00:00:00', 'admin', NULL),
(10, 271, '0000-00-00', '', NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_income`
--

CREATE TABLE `enrollment_income` (
  `id` int(11) NOT NULL,
  `postulant` int(11) NOT NULL,
  `apply_level` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(11) NOT NULL,
  `evaluation_name` varchar(64) NOT NULL,
  `weight` float DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluations`
--

INSERT INTO `evaluations` (`id`, `evaluation_name`, `weight`, `academic_year`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Contrôle ', 100, 1, '2019-07-01 00:00:00', '2019-07-01 00:00:00', NULL, NULL),
(2, 'Contrôle ', 100, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_by_year`
--

CREATE TABLE `evaluation_by_year` (
  `id` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `evaluation_date` date NOT NULL,
  `last_evaluation` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `evaluation_by_year`
--

INSERT INTO `evaluation_by_year` (`id`, `evaluation`, `academic_year`, `evaluation_date`, `last_evaluation`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 1, 2, '2018-12-22', 0, '2019-07-01 00:00:00', '2019-07-01 00:00:00', NULL, NULL),
(2, 1, 3, '2019-03-12', 1, '2019-07-01 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(3, 1, 4, '2019-06-11', 0, '2019-07-01 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(5, 2, 7, '2019-09-16', 0, '2019-08-17 00:00:00', '2019-08-17 00:00:00', NULL, NULL),
(6, 2, 8, '2020-03-16', 0, '2019-08-17 00:00:00', '2019-08-17 00:00:00', NULL, NULL),
(7, 2, 9, '2020-06-22', 1, '2019-08-17 00:00:00', '2019-08-17 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `examen_menfp`
--

CREATE TABLE `examen_menfp` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  `amount` float NOT NULL,
  `devise` int(11) DEFAULT NULL,
  `date_limit_payment` date DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fees`
--

INSERT INTO `fees` (`id`, `level`, `academic_period`, `fee`, `amount`, `devise`, `date_limit_payment`, `checked`, `description`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 2, 1, 1, 10000, NULL, '2019-07-07', 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(2, 3, 1, 1, 10000, NULL, '2019-07-27', 1, '', '2019-07-23 00:00:00', NULL, 'admin', NULL),
(3, 4, 1, 1, 10000, NULL, '2019-07-30', 1, '', '2019-07-23 00:00:00', NULL, 'admin', NULL),
(4, 1, 1, 1, 10000, NULL, '2019-07-31', 1, '', '2019-07-23 00:00:00', NULL, NULL, NULL),
(5, 1, 1, 5, 15000, NULL, '2019-08-07', 1, '', '2019-08-06 00:00:00', NULL, 'admin', NULL),
(6, 1, 1, 3, 9000, NULL, '2019-08-10', 1, '', '2019-08-06 00:00:00', NULL, 'admin', NULL),
(7, 1, 1, 4, 5000, NULL, '2019-08-20', 0, '', '2019-08-06 00:00:00', NULL, 'admin', NULL),
(8, 1, 1, 2, 8000, NULL, '2019-08-30', 0, '', '2019-08-06 00:00:00', NULL, 'admin', NULL),
(10, 3, 1, 3, 15000, NULL, '2019-08-06', 1, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(11, 3, 1, 5, 20000, NULL, '2019-08-14', 0, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(12, 3, 1, 4, 10000, NULL, '2019-08-24', 0, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(13, 3, 1, 2, 15000, NULL, '2019-08-31', 0, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(14, 6, 1, 5, 25000, NULL, '2019-08-01', 1, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(15, 6, 1, 1, 5000, NULL, '2019-08-06', 1, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(16, 6, 1, 3, 5000, NULL, '2019-08-15', 0, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(17, 6, 1, 4, 5000, NULL, '2019-08-31', 0, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(18, 7, 1, 5, 25000, NULL, '2019-08-01', 1, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(19, 7, 1, 1, 5000, NULL, '2019-08-15', 0, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(20, 7, 1, 3, 5000, NULL, '2019-08-31', 0, '', '2019-08-07 00:00:00', NULL, 'admin', NULL),
(21, 2, 6, 1, 10000, NULL, '2020-07-07', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(22, 3, 6, 1, 10000, NULL, '2020-07-27', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(23, 4, 6, 1, 10000, NULL, '2020-07-30', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(24, 1, 6, 1, 10000, NULL, '2020-07-31', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(25, 6, 6, 1, 5000, NULL, '2020-08-06', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(26, 7, 6, 1, 5000, NULL, '2020-08-15', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(27, 1, 6, 2, 8000, NULL, '2020-08-30', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(28, 3, 6, 2, 15000, NULL, '2020-08-31', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(29, 1, 6, 3, 9000, NULL, '2020-08-10', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(30, 3, 6, 3, 15000, NULL, '2020-08-06', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(31, 6, 6, 3, 5000, NULL, '2020-08-15', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(32, 7, 6, 3, 5000, NULL, '2020-08-31', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(33, 1, 6, 4, 5000, NULL, '2020-08-20', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(34, 3, 6, 4, 10000, NULL, '2020-08-24', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(35, 6, 6, 4, 5000, NULL, '2020-08-31', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(36, 1, 6, 5, 15000, NULL, '2020-08-07', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(37, 3, 6, 5, 20000, NULL, '2020-08-14', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(38, 6, 6, 5, 25000, NULL, '2020-08-01', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(39, 7, 6, 5, 25000, NULL, '2020-08-01', 0, '', '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(40, 10, 6, 5, 35500, NULL, '2019-08-10', 1, '', '2019-09-06 00:00:00', NULL, 'admin', NULL),
(41, 10, 6, 1, 8100, NULL, '2019-10-25', 0, '', '2019-09-06 00:00:00', NULL, 'admin', NULL),
(42, 10, 6, 3, 8100, NULL, '2019-12-06', 0, '', '2019-09-06 00:00:00', NULL, 'admin', NULL),
(43, 10, 6, 4, 8100, NULL, '2020-02-21', 0, '', '2019-09-06 00:00:00', NULL, 'admin', NULL),
(44, 10, 6, 2, 8100, NULL, '2020-04-24', 0, '', '2019-09-06 00:00:00', NULL, 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `fees_label`
--

CREATE TABLE `fees_label` (
  `id` int(11) NOT NULL,
  `fee_label` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: other fees, no pending; 1: tuition fees, pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `fees_label`
--

INSERT INTO `fees_label` (`id`, `fee_label`, `status`) VALUES
(1, 'Versement 1', 1),
(2, 'Versement 4', 1),
(3, 'Versement 2', 1),
(4, 'Versement 3', 1),
(5, 'Frais d\'entrée', 1),
(7, 'Frais divers', 0);

-- --------------------------------------------------------

--
-- Table structure for table `field_study`
--

CREATE TABLE `field_study` (
  `id` int(11) NOT NULL,
  `field_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `field_study`
--

INSERT INTO `field_study` (`id`, `field_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Sciences humaines et sociales', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(2, 'Sciences ', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(3, 'Lettres et Langues', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(4, 'Art', '2019-08-10 00:00:00', NULL, NULL, NULL),
(5, 'Santé', '2019-08-10 00:00:00', NULL, NULL, NULL),
(6, 'Administration', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(7, 'Communication', '2019-08-10 00:00:00', NULL, NULL, NULL),
(8, 'Sciences naturelles', '2019-08-10 00:00:00', NULL, NULL, NULL),
(9, 'Philosophie', '2019-08-10 00:00:00', NULL, NULL, NULL),
(10, 'Religion', '2019-08-10 00:00:00', NULL, NULL, NULL),
(11, 'Techniques', '2019-08-10 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `general_average_by_period`
--

CREATE TABLE `general_average_by_period` (
  `academic_year` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `general_average` double NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_average_by_period`
--

INSERT INTO `general_average_by_period` (`academic_year`, `academic_period`, `student`, `general_average`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 3, 229, 86.08, '2019-09-05 00:00:00', NULL, NULL, NULL),
(1, 3, 232, 82.5, '2019-09-05 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `general_config`
--

CREATE TABLE `general_config` (
  `id` int(11) NOT NULL,
  `item_name` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `item_value` text,
  `description` text,
  `english_comment` text,
  `category` varchar(12) DEFAULT NULL,
  `visibility` int(5) NOT NULL DEFAULT '0',
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_config`
--

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'school_name', 'Nom établissement', 'Ecole Demo LOGIPAM', 'Saisir le nom de l\'établissement', 'Enter the name of the school', 'sys', 0, '0000-00-00 00:00:00', '2019-09-12 00:00:00', '', ''),
(2, 'school_address', 'Adresse établissement', 'Delmas 29, Delmas, Haiti', 'Adresse de l\'école', 'School address', 'sys', 0, '0000-00-00 00:00:00', '2019-07-01 00:00:00', '', ''),
(3, 'school_phone_number', 'Tél établissement', '+ 509 36 01 29 59 | 33 31 75 28 ', 'Saisir les numéros de téléphone, s\'il y a plusieurs numéros, utiliser un "/" pour les séparer.', 'Enter the phone number, if there are multiple phone numbers, use a "/" to separated them.', 'sys', 0, NULL, '2019-07-01 00:00:00', NULL, NULL),
(4, 'school_director_name', 'Nom Directeur', 'Jean Val Jean', 'Saisir le nom du Directeur de l\'école.', 'Enter the name of the director of the school.', 'sys', 0, NULL, '2019-08-31 00:00:00', NULL, NULL),
(5, 'school_email_address', 'Email établissement', 'info@logipam.com', 'Saisir l\'adresse email de l\'école.', 'Enter school email address.', 'sys', 0, NULL, '2019-07-01 00:00:00', NULL, NULL),
(6, 'school_site_web', 'Site web établissement', 'www.slogipam.com/demo', 'Saisir URL du site web de l\'école. (Exemple: http:://logipam.com)', 'Enter the URL of the school site web (Example: http:://logipam.com)', 'sys', 0, NULL, '2019-07-01 00:00:00', NULL, NULL),
(7, 'academic_success', 'Message pour réussite', 'Success', 'Message pour un élève qui a réussi.', 'Message for a successful student. ', 'acad', 0, NULL, '2016-05-31 00:00:00', NULL, NULL),
(8, 'a_link', '', 'link', NULL, NULL, NULL, 0, NULL, '2015-08-24 00:00:00', NULL, NULL),
(9, 'ppe_number', '', NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, NULL),
(10, 'cie_number', 'Numéro Carte Identification École', '', 'Numéro de la carte d\'identité d\'etablissement (CIE)', 'ID Number of the school ID Card.', 'sys', 0, NULL, '2016-05-31 00:00:00', NULL, NULL),
(11, 'school_licence_number', 'Numéro de la licence', '', 'Numéro de licence de l\'établissement.', 'Authorisation license of the school. ', 'sys', 0, NULL, '2016-05-31 00:00:00', NULL, NULL),
(12, 'default_vacation', 'Vacation par défaut', 'Matin', 'Vacation  par defaut.', 'Default Shift.', 'acad', 0, '2015-05-20 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(13, 'default_section', 'Section par défaut', 'Fondamental', 'Section par défaut', 'Default Section', 'acad', 0, '2015-05-20 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(14, 'success_mention', 'Mention pour réussite', 'Succès', 'Message pour la mention de réussite', 'Message for success notification. ', 'acad', 0, '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(15, 'failure_mention', 'Mention pour échec', 'Echec', 'Message en cas d\'échec d\'un élève.', 'Message for failed student.', 'acad', 0, '2015-07-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(16, 'decision_finale_success', 'Message de réussite pour décision de fin d\'année', 'Admis(e) en classe supérieure/Admis(e) ailleurs/Admis(e) à l\'école, sous probation.', 'Option de message en cas de réussite. Sépare chaque option par un "/". ', 'Message option in case of success. Separate each option with a "/". ', 'acad', 0, '2015-07-01 00:00:00', '2019-07-01 00:00:00', NULL, NULL),
(17, 'decision_finale_failure', 'Message d\'échec pour décision de fin d\'année', 'À refaire ailleurs/À refaire à l\'école', 'Option de message en cas d\'échec d\'un élève. Sépare chaque option par un /.', 'Message option in case of student failed. Separate each option with a /.', 'acad', 0, '2015-07-01 00:00:00', '2019-07-01 00:00:00', NULL, NULL),
(18, 'code1', 'Code liste de formation classe (MENFP)', '', 'Code fourni par le ministère de l\'éducation nationale d\'Haïti pour l\'école.', 'Education department official code for the school. ', 'sys', 0, '2015-08-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(19, 'code2', 'Code à 11 chiffres du MENFP', '', 'Code a 11 chiffres fourni par le ministere de l\'éducation nationale d\'Haïti.', 'Education department code with 11 digit for the school.', 'sys', 0, '2015-08-01 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(20, 'methode_deduction_note_discipline', 'Méthode déduction note discipline', '1', '0: Valeur ------- 1 : Pourcentage -------\nValeur : les deductions affectent la valeur de la note de discipline -------- Pourcentage : les deductions affectent un pourcentage de la note de discipline', '0: Value ------ 1: Percentage ------\nValue: the deduction will affect the value of discipline grade ------ \nPercentage : the deduction will affect a percentage of discipline grade. ', 'disc', 0, '2015-08-19 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(21, 'note_discipline_initiale', 'Note discipline initiale', '100', 'Valeur initiale de la note de discipline.  Par exemple : 100 ou 20 ou 50. ', 'Initial value for discipline grade. Example : 100 or 20 or 50', 'disc', 0, '2015-08-19 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(22, 'max_amount_loan', 'Montant maximun d\'un prêt', '50000', 'Le montant maximum que l\'on peut donner comme prêter a un employé.', 'Maximun loan can be granted to an employee. ', 'sys', 0, '2015-09-18 00:00:00', '2019-08-07 00:00:00', NULL, NULL),
(23, 'School_acronym', 'Acronyme établissement', '', 'Acronyme du nom de l\'école. Placer entre parenthèse.', 'Acronym for school name. Place within parenthesis. ', 'econ', 0, '2015-09-19 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(24, 'include_discipline_grade', 'Inclure note discipline', '0', ' 2: ne pas afficher discipline dans le carnet. ------ 1: oui, on doit inclure la note de discipline dans le calcul de la moyenne ------ 0: non, le calcul de la moyenne ne tient pas compte de la note de discipline.', '2:  do not display discipline in reportcard. ----- 1: include discipline grade in calculation of average. ------ 0: no, the calculation of average don\'t include discipline grade. ', 'disc', 0, NULL, '2019-08-31 00:00:00', NULL, NULL),
(25, 'average_base', 'Moyenne de base', '100', 'La base de la moyenne: 10 ou 100.', 'Base average: 10 or 100.', 'acad', 0, NULL, '2019-08-05 00:00:00', NULL, NULL),
(26, 'reportcard_structure', 'Format bulletin', '1', '1: Simple (une évaluation par période, moyenne générale sur demande);  2: Avancé (Plusieurs évaluations sur UNE période, moyenne générale automatique)', '1: Basic (One evaluation by Period, general average on request); 2: Advanced (Many evaluations in ONE Period, general average automatic)', 'acad', 0, NULL, '2016-05-31 00:00:00', NULL, NULL),
(27, 'include_place', 'Inclure rang élève dans bulletin', '0', '0: le rang de l\'eleve ne figure pas dans le bulletin. 1: le rang de l\'eleve figure dans le bulletin.', '0: student rank not include in the reportcard. 1: student rang include in the reportcard. ', 'acad', 0, NULL, '2019-08-05 00:00:00', NULL, NULL),
(28, 'tardy_absence_display', 'Inclure "retard" et "absence" dans le bulletin', '1', '1:Figure OU 0:Ne figure pas', '1:Display OR 0:Do not display ', 'disc', 0, '2015-12-18 00:00:00', '2016-05-31 00:00:00', 'SIGES', NULL),
(29, 'total_payroll', 'Nombre de payroll par année', '12', 'Nombre de payroll pour l\'annee', 'Number of payroll per year', 'econ', 0, '2015-12-08 00:00:00', '2016-05-31 00:00:00', 'SIGES', NULL),
(30, 'limit_payroll_update', 'Délai modifcation payroll', '', 'Nombre de jours (à partir de la date de paiement) durant lesquels le payroll peut etre modifié.', 'Number of days (from the payment date) the payrolls can be updated. ', 'econ', 0, '2015-12-14 00:00:00', '2016-05-31 00:00:00', 'SIGES', NULL),
(32, 'currency_name_symbol', 'Nom et symbole monétaire', 'Gourde/HTG', 'Le nom et le symbol de la devise utilisée par l\'école', 'Currency name and symbol used', 'econ', 0, NULL, '2016-05-31 00:00:00', 'SIGES', NULL),
(33, 'facebook_page', 'Page facebook', 'LOGIPAM', 'Adresse de la page facebook de l\'établissement (Exemple : facebook.com/logipam -> Ecrivez : logipam) ', 'Facebook address of the institution. Example : facebook.com/logipam -> type logipam) ', 'sys', 0, '2016-03-25 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(34, 'twitter_page', 'Compte twitter', 'LOGIPAM', 'Nom d\'utilisateur twitter de l\'établissement.', 'Twitter username of the institution.', 'sys', 0, '2016-03-25 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(35, 'youtube_page', 'Page youtube', 'https://youtube.com', 'Adresse de la Chaine YouTube de l\'institution.', 'Address of the YouTube Channel of the instutition.', 'sys', 0, '2016-03-25 00:00:00', '2016-05-31 00:00:00', NULL, NULL),
(36, 'devise_school', 'Nom complémentaire de l\'établissement', 'Système intégré de gestion d\'école', 'La phrase devise de l\'établissement.', 'The motto phrase of the institution.', 'sys', 0, '2016-03-25 00:00:00', '2019-07-01 00:00:00', NULL, NULL),
(37, 'display_schedule_agenda', 'Schedule/Agenda displaying', '0', '0: ne pas afficher l\'agenda; 1: afficher l\'agenda a la place de l\'horaire; 2: afficher l\'agenda ainsi que l\'horaire', '0: do not display; 1: display without schedules; 2: display with schedules', NULL, 0, NULL, '2016-05-31 00:00:00', NULL, NULL),
(38, 'agenda_duration', 'Subdivision des heures', '3', '1: subdivision par 10; 2: subdivision par 15; 3: subdivision par 30; ', '1: subdivide by 10; 2: subdivide by 15; 3: subdivide by 30; ', NULL, 0, NULL, '2016-05-31 00:00:00', NULL, NULL),
(39, 'number_article_per_page', 'Nombre d\'articles par page', '5', 'Nombre d\'articles s\'affichant sur une page du Portail SIGES.', 'Number of articles viewing in a SIGES Portal page.', 'sys', 0, NULL, '2016-05-31 00:00:00', 'admin', NULL),
(40, 'number_article_archive', 'Nombre d\'articles avant archivage', '7', 'Nombre maximal d\'article à afficher avant que le système les place dans la vue d\'archivage du Portail SIGES.', 'Maximum number of article to show before the system put them in the archive view of SIGES Portal', 'sys', 0, NULL, '2016-05-31 00:00:00', 'admin', NULL),
(41, 'theme_portail_siges', 'Thème Portail SIGES', 'kann', 'Saisir le thème du portail de SIGES. Choisir entre siges, seriz, zaboka, zoranj, tamaren', 'Type the theme of SIGES portal. Choose between siges, seriz, zaboka, zoranj, tamaren', 'sys', 0, NULL, '2019-08-30 00:00:00', 'admin', NULL),
(42, 'slogan', 'Devise de l\'établissement', 'Siges gen lisans GPL. Telechaje l gratis, modifye l, pataje l ak zanmi w jan w pito !', 'La phrase devise de l\'établissement.', 'The motto phrase of the institution.', 'sys', 0, NULL, '2019-07-01 00:00:00', 'admin', NULL),
(43, 'grid_creation', 'Création en grille', '1', '1: Activer la creation en Grille - 0: Desactiver la création en Grille ', '1: Activate grid creation - 0: Desactivate grid creation', 'sys', 0, NULL, NULL, 'admin', NULL),
(44, 'nb_grid_line', 'Nombre ligne à afficher', '10', 'Nombre de ligne a afficher sur la grille.', 'Number of line to show on the grid.', 'sys', 0, NULL, NULL, 'admin', NULL),
(45, 'siges_structure_session', 'Utiliser la version session', '0', '0 :pour utiliser la structure de base de SIGES; 1 :pour utiliser la structure session', '0 :to use the basic structure of SIGES; 1 :to use the session structure ', NULL, 0, '2016-10-15 00:00:00', NULL, 'SIGES', NULL),
(46, 'observation_line', 'Ajouter une ligne d observation', '0', '0 :pour cacher la ligne; 1 :pour montrer la ligne', '0 :to hide the line; 1 :to show the line ', NULL, 0, '2016-10-15 00:00:00', '2016-12-22 00:00:00', 'SIGES', NULL),
(47, 'display_student_code', 'Ajouter le code de l eleve dans le bulletin', '0', '0 :pour cacher le code; 1 :pour montrer le code', '0 :to hide the code; 1 :to show the code ', NULL, 0, '2016-10-15 00:00:00', '2016-12-22 00:00:00', 'SIGES', NULL),
(48, 'display_administration_signature', 'Ajouter la signature de la directtion', '1', ' :pour cacher la signature de la direction; 1 :pour montrer la signature de la direction', '0 :to hide the administration signature; 1 :to show the administration signature ', NULL, 0, '2016-10-15 00:00:00', NULL, 'SIGES', NULL),
(49, 'display_parent_signature', 'Ajouter la signature du responsable', '0', '0 :pour cacher la signature du responsable; 1 :pour montrer la signature du responsable', '0 :to hide parent signature; 1 :to show parent signature ', NULL, 0, '2016-10-15 00:00:00', NULL, 'SIGES', NULL),
(50, 'administration_signature_text', 'Definir le texte sous la ligne de la signature de la direction', 'Directeur', 'Definir le texte sous la ligne de signature de la direction', 'Define the text which is under the administration signature line', NULL, 0, '2016-10-15 00:00:00', NULL, 'SIGES', NULL),
(51, 'parent_signature_text', 'Definir le texte sous la ligne de la signature du responsable', 'Parent', 'Definir le texte sous la ligne de signature du responsable', 'Define the text which is under the parent signature line', NULL, 0, '2016-10-15 00:00:00', NULL, 'SIGES', NULL),
(52, 'display_created_date', 'Ajouter la date de creation du bulletin', '1', '0 :pour cacher la date de creation du bulletin; 1 :pour montrer la date de creation du bulletin', '0 :to hide the reportcard created date; 1 :to show the reportcard created date ', NULL, 0, '2016-10-15 00:00:00', NULL, 'SIGES', NULL),
(53, 'grades_comment', 'Commentaire note', '0', '0: enlever le champ commentaire des notes 1: ajouter le champ commentaire des notes', '0: hide comment field for grades 1: display comment field for grades', NULL, 0, NULL, NULL, NULL, NULL),
(54, 'transcript_note_text', 'Texte pour relevé de notes', 'La Direction du {name} certifie par la présente que l\'élève <strong>{name1}</strong> a suivi les cours réguliers d\'enseignement classique en <strong>{name2}</strong>, niveau {name3} durant <strong>l\'année académique {name4}</strong>. Il a obtenu les notes suivantes:', 'Texte pour relevé de notes', NULL, NULL, 0, NULL, NULL, NULL, NULL),
(55, 'transcript_footer_text', 'Texte bas de page pour relevé de notes', 'En foi de quoi, ce présent document lui est délivré pour servir et valoir ce que de droit.', 'Texte bas de page pour relevé de notes', NULL, NULL, 0, NULL, NULL, NULL, NULL),
(60, 'automatic_code', 'Code automatique', '1', '0 : pour inserrer le code de la personne manuellement; 1 : pour laisser à SIGES le soin de fournir le code automatiquement', '0 : to insert person\'s code manually 1 : to let SIGES provides the code automatically', NULL, 0, NULL, '2017-10-23 00:00:00', NULL, NULL),
(61, 'admission_note', 'Remarque pour admission', 'En inscrivant mon enfant à l\'ecole, j\'accepte de me conformer aux règlements de l\'établissement.. ', 'Remarque concernant l’admission', 'Note related to enrollment', NULL, 0, NULL, '2019-07-01 00:00:00', NULL, NULL),
(64, 'display_class_enroll', 'Afficher l\'effectif de la salle', '1', '0: ne pas afficher l\'effectif de la salle dans le bulletin 1: Afficher l\'effectif de la salle dans le bulletin ', '0: do not display class enroll in report card 1: display class enroll in report card ', NULL, 0, NULL, NULL, NULL, NULL),
(65, 'use_automatic_mention', 'Mention automatique(Decision Fin Annee)', '0', '0: Ne pas utiliser la mention automatique dans la Decision de Fin d\'Annee 1: Utiliser la mention automatique dans la Decision de Fin d\'Annee', '0: do not use automatic mention for End Year Decision 1: Use automatic mention for End Year Decision', NULL, 0, NULL, '2017-07-06 00:00:00', NULL, NULL),
(66, 'use_period_weight', 'Ponderation des periodes', '0', '0: Ne pas utiliser la ponderation des periodes dans le bulletin 1: Utiliser la ponderation des periodes dans le bulletin', '0: do not care about period weight in report card 1: take care about period weight in report card ', NULL, 0, NULL, NULL, NULL, NULL),
(67, 'display_period_summary', 'Inclure sommaire dans le bulletin', '1', '0: ne pas afficher le sommaire des periodes dans le bulletin 1: Afficher le sommaire des periodes dans le bulletin', '0: do not display period summary in report card 1: display period summary in report card ', NULL, 0, NULL, '2017-06-21 00:00:00', NULL, NULL),
(68, 'day_for_currentYear_postulant', 'Nombre de jour limite pour candidature', '45', 'Nombre de jour limite a partir de la date debut de l\'annee(la session) en cours pour la reception de candidatures la concernant', 'Deadline from the current academic year(session) starting date to receive his postulants', NULL, 0, NULL, NULL, NULL, NULL),
(69, 'use_mention_summary', 'Decision finale avec soomaire', '1', '0: ne pas tenir compte du sommaire des periodes; 1: tenir compte du sommaire des periodes', '0: do not care about periods summary; 0: care about periods summary', 'sys', 0, NULL, NULL, NULL, NULL),
(70, 'use_behavior_summary_inDecision', 'Decision finale avec sommaire discipline', '1', '0: ne pas tenir compte du sommaire disciplinaire dans la decision finale; 1: tenir compte du sommaire disciplinaire dans la decision finale', '0: do not care about behavior summary in decision; 1: care about behavior summary in decision', 'sys', 0, NULL, NULL, NULL, NULL),
(71, 'bonis_set', 'Methode de calcul du bonis', '1', '0: ne pas tenir compte des payrolls sur les 12 mois de l\'annee; 1: tenir compte des payrolls sur les 12 mois de l\'annee; 2: en utilisant un pourcentage.', '0: do not care about payrolls for the year; 1: care about payrolls for the year; 2: using a percentage.', 'sys', 0, NULL, NULL, NULL, NULL),
(72, 'lower_behaviorGrade_summary', 'Note minimale sommaire discipline', '80', '', '', 'sys', 0, '2018-06-30 00:00:00', NULL, 'logipam', NULL),
(74, 'header_line_color', 'Couleur de la ligne  d\'en-tête (Header line color)', '0', '0: noire; 1: rouge; 2: bleue; 3: verte; 4: orange', '0: black; 1: red; 2: blue; 3: green; 4: orange', NULL, 0, NULL, NULL, NULL, NULL),
(75, 'header_schoolname_color', 'Couleur de l\'en-tête', '0', '0: noire; 1: rouge; 2: bleue; 3: verte; 4: orange', '0: black; 1: red; 2: blue; 3: green; 4: orange', NULL, 0, NULL, NULL, NULL, NULL),
(76, 'header_position', 'Position des textes de l\'en-tête', '1', '1: à gauche; 2: milieu; 3: à droite', '1: left; 2: center; 3: right', NULL, 0, NULL, NULL, NULL, NULL),
(80, 'display_archives', 'Afficher menu Archives', '0', '0: ne pas afficher le menu ArchiveS; 1: afficher le menu Archives.', '0: do not display Archives menu; 1: display Archives menu.', 'sys', 0, NULL, '2019-08-31 00:00:00', NULL, NULL),
(81, 'display_billing_menu', 'Afficher menu economat', '1', '0: ne pas afficher le menu economat; 1: afficher le menu economat.', '0: do not display billing menu; 1: display billing menu.', 'sys', 0, NULL, '2019-07-21 00:00:00', NULL, NULL),
(82, 'display_behavior_menu', 'Afficher menu discipline', '1', '0: ne pas afficher le menu discipline; 1: afficher le menu discipline.', '0: do not display behavior menu; 1: display behavior menu.', 'sys', 0, NULL, '2019-07-21 00:00:00', NULL, NULL),
(83, 'display_calendar_in_menu', 'Afficher calendrier dans le menu', '1', '0: ne pas afficher le calendrier dans le menu; 1: afficher le calendrier dans le menu.', '0: do not display calendar in menu; 1: display calendar in menu.', 'sys', 0, NULL, '2019-07-01 00:00:00', NULL, NULL),
(84, 'display_schedules_in_menu', 'Afficher horaire dans le menu', '0', '0: ne pas afficher horaire dans le menu; 1: afficher horaire dans le menu.', '0: do not display schedules in menu; 1: display schedules in menu.', 'sys', 0, NULL, NULL, NULL, NULL),
(95, 'display_announcement_in_menu', 'Afficher annonce dans le menu', '1', '0: ne pas afficher annonce dans le menu; 1: afficher annonce dans le menu.', '0: do not display announcement in menu; 1: display announcement in menu.', 'sys', 0, NULL, NULL, NULL, NULL),
(96, 'display_homework_menu', 'Afficher menu devoir', '1', '0: ne pas afficher le menu devoir; 1: afficher le menu devoir.', '0: do not display homework menu; 1: display homework menu.', 'dev', 1, NULL, '2019-08-02 00:00:00', NULL, NULL),
(97, 'display_pos_in_menu', 'Afficher menu point de vente', '1', '0: ne pas afficher le menu point de vente; 1: afficher le menu point de vente.', '0: do not display point of sale menu; 1: display point of sale menu.', 'sys', 0, NULL, '2019-08-01 00:00:00', NULL, NULL),
(98, 'display_loan_in_menu', 'Afficher menu prêt', '1', '0: ne pas afficher le menu prêt; 1: afficher le menu prêt.', '0: do not display loan menu; 1: display loan menu.', 'sys', 0, NULL, '2019-08-07 00:00:00', NULL, NULL),
(99, 'display_cycle_in_menu', 'Afficher Cycle au menu', '1', '0: ne pas afficher Cycle au menu; 1: afficher Cycle au menu.', '0: do not display Cycle in menu; 1: display Cycle in menu.', 'sys', 0, NULL, '2019-07-01 00:00:00', NULL, NULL),
(100, 'set_palmares', 'Choisir Palmarès', '1', '0: Ancien Palmarès | 1: Nouveau Palmarès', '0: Old Grade book | 1: New Grade Book', 'sys', 0, '2018-06-30 00:00:00', NULL, 'logipam', NULL),
(101, 'idcard_orientation', 'Orientation de la carte', 'L', 'P: orientation portrait; L: orientation paysage;', 'P: portrait; L: landscape;', NULL, 0, NULL, '2019-08-08 00:00:00', NULL, NULL),
(102, 'idcard_template', 'Le numero du template choisi', '1', '', '', NULL, 0, NULL, NULL, NULL, NULL),
(103, 'main_color', 'Couleur principale', '#4169E1', '', '', NULL, 0, NULL, '2019-09-12 00:00:00', NULL, NULL),
(104, 'secondary_color', 'Couleur secondaire', 'white', '', '', NULL, 0, NULL, NULL, NULL, NULL),
(105, 'balance_color', 'Couleur intermediaire', 'white', '', '', NULL, 0, NULL, NULL, NULL, NULL),
(106, 'line_color', 'Couleur de la ligne', 'white', '', '', NULL, 0, NULL, NULL, NULL, NULL),
(107, 'is_admission_open', 'Ouverture de l\'admission', '1', '0: La période d\'admission est fermée /\r\n1: La période d\'admission est ouverte', '', 'portal', 0, '2019-06-12 00:00:00', '2019-08-09 00:00:00', 'LOGIPAM', NULL),
(108, 'email_relay_host', 'Hote de l\'email de relais', 'mail.slogipam.com', 'Email de relais pour lancer des mails par SMTP.', NULL, 'portal', 0, '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL),
(109, 'email_relay_username', 'Nom utilisateur email de relais', 'sigessystem@slogipam.com', 'Nom utilisateur email de relais. Pour un compte gmail saisir votre email.', NULL, 'portal', 0, '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL),
(110, 'host_relay_password', 'Mot de passe du relais email', 'VilokanVulcan007#', 'Mot de passe du relais email, pour gmail, votre mot de passe gmail', NULL, 'portal', 0, '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL),
(111, 'host_relay_port', 'Port du relais mail', '465', 'Pour gmail.com 587', NULL, 'portal', 0, '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL),
(112, 'instruction_admission', 'Instruction admission', 'Veuillez imprimer ce formulaire d\'inscription <strong>en double (2) exemplaires</strong>, puis passer au {school_name} sise au <strong>{school_address}</strong> pour payer les frais d\'inscriptions avant le \r\n<strong>{date_limite}</strong>	', 'Instruction pour le proicessus d\'inscription en ligne.', NULL, 'portal', 0, '2019-06-30 00:00:00', NULL, 'LOGIPAM', NULL),
(113, 'admission_pieces', 'Pieces requises admission', 'Ici il y aura la liste des peices requise pour l\'admission. \r\n', 'List des pieces requises pour admission', NULL, 'portal', 0, '2019-06-30 00:00:00', NULL, 'LOGIPAM', NULL),
(119, 'template_email_admission', 'Message email de reception admission', 'Cher(e) {personne_responsable},\r\nMerci d\'avoir inscrit votre enfant {nom_postulant} en classe de {classe_admission} en date du {date_inscription} dans notre établissement. Pour compléter le processus d\'inscription, veuillez passer à la direction de l\'école {school_name}, à l’adresse {school_address}, muni du formulaire d\'inscription dûment rempli que vous avez imprim&eacute;. Il est possible de réimprimer le formulaire en cliquant sur ce lien {link_form}. \r\nConservez précieusement le numéro d\'inscription <strong>{admission_number}</strong>, car il sera utilisé comme code d’identifiant lors des publications des résultats sur notre site.\r\nAttention. Il est extrêmement important de compléter l’inscription à la direction de l’école dans les 5 jours ouvrables qui suivent, sinon votre application en ligne sera rejetée. Il vous faudra recommencer le processus.\r\nListe de pièces à apporter au moment de l’inscription\r\n•	Acte de naissance ou extrait des archives (original et copie)\r\n•	Deux photos d’identité de date récente\r\n•	Bulletin de la dernière étape du dernier établissement scolaire fréquenté\r\n•	Certificat médical attestant les vaccinations obligatoires, délivré par le pédiatre de l’enfant\r\n•	Frais d’inscription de  x gourdes.', 'Template de l\'email pour la reception de l\'admission. ', NULL, 'portal', 0, '2019-07-02 00:00:00', NULL, 'LOGIPAM', NULL),
(120, 'display_subscription_billing', 'Afficher Econamat dans l\'inscription', '0', '0 - Ne pas afficher l\'economat dans l\'inscription\r\n1 - Afficher l\'economat dans l\'inscription', '0 - Ne pas afficher l\'economat dans l\'inscription\r\n1 - Afficher l\'economat dans l\'inscription', 'sys', 0, '2019-07-09 00:00:00', NULL, 'LOGIPAM', NULL),
(121, 'publish_admission_result', 'Publication résultats admission', '1', '0: Ne pas publier les résultats sur le site web\r\n1: Publier les résultats sur le site web', '0: Do not publish on web site\r\n1: Publish on web site', 'sys', 0, '2019-07-13 00:00:00', NULL, 'LOGIPAM', NULL),
(122, 'kindergarden_module', 'Module Kindergarden', '1', '0 : Pas Activé\r\n1 : Activé', '0 : Not active\r\n1 : Active', 'sys', 0, '2019-07-17 00:00:00', '2019-07-18 00:00:00', 'LOGIPAM', NULL),
(123, 'kindergarden_section', 'Nom section kindergarden', 'Prescolaire', 'Un nom pour la section kindergarden\r\nPar défaut : Prescolaire ', 'A name for the kindegarden section. \r\nDefault section : Prescolaire ', 'sys', 0, '2019-07-17 00:00:00', NULL, 'LOGIPAM', NULL),
(124, 'presence_method', 'Methode de prise de présence', '2', '0 : Methode de prise de présence manuelle \r\n1 : Methode de présence par scanner et manuelle \r\n2 : Méthode de prise de retard \r\n', '0 : Methode de prise de présence manuelle \r\n1 : Methode de présence par scanner et manuelle \r\n2 : Méthode de prise de retard ', 'disc', 0, '2019-07-27 00:00:00', '2019-09-05 00:00:00', 'LOGIPAM', NULL),
(125, 'time_zone', 'Fuseau Horaire en PHP', 'America/New_York', 'Pour une liste complete visiter : https://www.php.net/manual/en/timezones.php\r\n\r\nPour les ameriques : \r\nhttps://www.php.net/manual/en/timezones.america.php\r\n', 'Pour une liste complete visiter : https://www.php.net/manual/en/timezones.php\r\n\r\nPour les ameriques : \r\nhttps://www.php.net/manual/en/timezones.america.php\r\n', 'sys', 0, '2019-07-27 00:00:00', NULL, 'LOGIPAM', NULL),
(126, 'attendance_time', 'Heure de présence', '08:00', 'Affiche l\'heure de presence au format : H:m (Exemple : 08:00 ou 16:00)', 'Affiche l\'heure de presence au format : H:m (Exemple : 08:00 ou 16:00)', 'disc', 0, '2019-07-29 00:00:00', '2019-07-31 00:00:00', 'LOGIPAM', NULL),
(127, 'message_late', 'Message de retard', 'Cher parent, <br /> Par cette note, la direction de {school_name} vous informe que votre enfant {student_name} actuellement scolarisé(e) en classe de {room} s’est présenté(e) en retard, soit à {time} en date du {date}. <br /> La direction vous saura gré de bien vouloir en discuter avec votre enfant et de prendre les mesures nécessaires pour éviter un cumul de retards susceptibles d’impacter ses performances scolaires et ses notes disciplinaires. <br /> En vous remerciant de votre vigilance, la direction vous prie de croire, Cher parent, en ses salutations les plus respectueuses. <br /> La Direction.', 'Note à envoyer au parent pour les retards non motivés', 'Note à envoyer au parent pour les retards non motivés', 'disc', 0, '2019-07-31 00:00:00', '2019-08-27 00:00:00', 'LOGIPAM', NULL),
(128, 'library_display', 'Afficher bibliotheque', '0', '0: n\'afficher pas\r\n 1: Affiche', ' ', 'dev', 1, '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL),
(129, 'display_communication', 'Module communication', '1', '', '', 'dev', 1, '2019-08-22 00:00:00', '2019-08-25 00:00:00', 'LOGIPAM', NULL),
(130, 'special_attendance', 'Presence special', '1', '0: Pas de présence spéciale\r\n1: Présence spéciale ', '0: No special attendance\r\n1: Special attendance', 'disc', 0, '2019-09-03 00:00:00', NULL, 'LOGIPAM', NULL),
(131, 'module_checkout', 'Activer checkout', '1', '0: Module checkout pas activé\r\n1: Module checkout activé', '0: Module checkout not active\r\n1: Module checkout active', 'disc', 0, '2019-09-04 00:00:00', NULL, 'LOGIPAM', NULL),
(132, 'remarques_generales', 'Remarques générales', ' ', ' ', '', 'sys', 0, '2019-09-06 00:00:00', NULL, 'LOGIPAM', NULL),
(133, 'display_portal', 'Activer/Desactiver portail', '1', '', NULL, 'sys', 0, '2019-09-07 00:00:00', NULL, 'LOGIPAM', NULL),
(134, 'display_homework_grade_menu', 'Activer/Desactiver notes devoir', '1', '0: Nafficher pas le menu notes devoir; 1: Afficher le menu notes devoir', NULL, 'dev', 1, '2019-09-07 00:00:00', NULL, 'LOGIPAM', NULL),
(135, 'signature_au', 'Signature dans la carte', '0', '0: Nafficher pas la signature dans la carte; 1: Afficher la signature dans la carte', NULL, 'dev', 1, '2019-09-14 00:00:00', NULL, 'LOGIPAM', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `grade_value` float DEFAULT NULL,
  `rewrite_grade_value` float DEFAULT NULL,
  `validate` tinyint(4) NOT NULL DEFAULT '0',
  `publish` tinyint(4) NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `grades`
--

INSERT INTO `grades` (`id`, `student`, `course`, `evaluation`, `grade_value`, `rewrite_grade_value`, `validate`, `publish`, `comment`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 55, 29, 1, 90, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(2, 53, 29, 1, 56, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(3, 66, 29, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(4, 60, 29, 1, 45, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(5, 73, 29, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(6, 59, 29, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(7, 75, 29, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(8, 72, 29, 1, 74, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(9, 67, 29, 1, 76, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(10, 69, 29, 1, 72, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(11, 54, 29, 1, 58, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(12, 76, 29, 1, 51, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(13, 56, 29, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(14, 63, 29, 1, 74, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(15, 58, 29, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(16, 57, 29, 1, 56, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(17, 65, 29, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(18, 62, 29, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(19, 68, 29, 1, 99, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(20, 74, 29, 1, 65, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(21, 52, 29, 1, 66, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(22, 71, 29, 1, 68, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(23, 64, 29, 1, 100, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(24, 61, 29, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(25, 70, 29, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(26, 55, 22, 1, 90, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(27, 53, 22, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(28, 66, 22, 1, 97, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(29, 60, 22, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(30, 73, 22, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(31, 59, 22, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(32, 75, 22, 1, 94, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(33, 72, 22, 1, 93, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(34, 67, 22, 1, 92, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(35, 69, 22, 1, 91, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(36, 54, 22, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(37, 76, 22, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(38, 56, 22, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(39, 63, 22, 1, 86, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(40, 58, 22, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(41, 57, 22, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(42, 65, 22, 1, 83, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(43, 62, 22, 1, 82, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(44, 68, 22, 1, 91, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(45, 74, 22, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(46, 52, 22, 1, 79, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(47, 71, 22, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(48, 64, 22, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(49, 61, 22, 1, 76, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(50, 70, 22, 1, 75, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(51, 55, 23, 1, 71, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(52, 53, 23, 1, 72, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(53, 66, 23, 1, 72, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(54, 60, 23, 1, 73, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(55, 73, 23, 1, 74, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(56, 59, 23, 1, 75, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(57, 75, 23, 1, 76, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(58, 72, 23, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(59, 67, 23, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(60, 69, 23, 1, 79, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(61, 54, 23, 1, 80, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(62, 76, 23, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(63, 56, 23, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(64, 63, 23, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(65, 58, 23, 1, 86, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(66, 57, 23, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(67, 65, 23, 1, 68, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(68, 62, 23, 1, 65, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(69, 68, 23, 1, 66, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(70, 74, 23, 1, 68, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(71, 52, 23, 1, 69, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(72, 71, 23, 1, 83, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(73, 64, 23, 1, 81, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(74, 61, 23, 1, 80, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(75, 70, 23, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(76, 55, 21, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(77, 53, 21, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(78, 66, 21, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(79, 60, 21, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(80, 73, 21, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(81, 59, 21, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(82, 75, 21, 1, 83, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(83, 72, 21, 1, 86, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(84, 67, 21, 1, 82, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(85, 69, 21, 1, 81, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(86, 54, 21, 1, 75, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(87, 76, 21, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(88, 56, 21, 1, 79, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(89, 63, 21, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(90, 58, 21, 1, 74, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(91, 57, 21, 1, 71, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(92, 65, 21, 1, 70, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(93, 62, 21, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(94, 68, 21, 1, 79, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(95, 74, 21, 1, 55, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(96, 52, 21, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(97, 71, 21, 1, 65, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(98, 64, 21, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(99, 61, 21, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(100, 70, 21, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(101, 55, 25, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(102, 53, 25, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(103, 66, 25, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(104, 60, 25, 1, 86, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(105, 73, 25, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(106, 59, 25, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(107, 75, 25, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(108, 72, 25, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(109, 67, 25, 1, 56, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(110, 69, 25, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(111, 54, 25, 1, 97, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(112, 76, 25, 1, 94, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(113, 56, 25, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(114, 63, 25, 1, 74, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(115, 58, 25, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(116, 57, 25, 1, 75, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(117, 65, 25, 1, 45, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(118, 62, 25, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(119, 68, 25, 1, 55, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(120, 74, 25, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(121, 52, 25, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(122, 71, 25, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(123, 64, 25, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(124, 61, 25, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(125, 70, 25, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(126, 55, 27, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(127, 53, 27, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(128, 66, 27, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(129, 60, 27, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(130, 73, 27, 1, 94, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(131, 59, 27, 1, 97, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(132, 75, 27, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(133, 72, 27, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(134, 67, 27, 1, 92, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(135, 69, 27, 1, 91, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(136, 54, 27, 1, 90, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(137, 76, 27, 1, 93, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(138, 56, 27, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(139, 63, 27, 1, 79, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(140, 58, 27, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(141, 57, 27, 1, 65, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(142, 65, 27, 1, 68, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(143, 62, 27, 1, 69, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(144, 68, 27, 1, 63, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(145, 74, 27, 1, 64, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(146, 52, 27, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(147, 71, 27, 1, 55, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(148, 64, 27, 1, 99, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(149, 61, 27, 1, 65, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(150, 70, 27, 1, 63, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(151, 55, 24, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(152, 53, 24, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(153, 66, 24, 1, 81, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(154, 60, 24, 1, 82, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(155, 73, 24, 1, 83, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(156, 59, 24, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(157, 75, 24, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(158, 72, 24, 1, 76, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(159, 67, 24, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(160, 69, 24, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(161, 54, 24, 1, 56, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(162, 76, 24, 1, 59, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(163, 56, 24, 1, 58, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(164, 63, 24, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(165, 58, 24, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(166, 57, 24, 1, 86, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(167, 65, 24, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(168, 62, 24, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(169, 68, 24, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(170, 74, 24, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(171, 52, 24, 1, 90, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(172, 71, 24, 1, 91, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(173, 64, 24, 1, 92, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(174, 61, 24, 1, 93, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(175, 70, 24, 1, 94, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(176, 55, 30, 1, 90, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(177, 53, 30, 1, 97, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(178, 66, 30, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(179, 60, 30, 1, 94, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(180, 73, 30, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(181, 59, 30, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(182, 75, 30, 1, 92, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(183, 72, 30, 1, 93, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(184, 67, 30, 1, 91, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(185, 69, 30, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(186, 54, 30, 1, 79, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(187, 76, 30, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(188, 56, 30, 1, 97, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(189, 63, 30, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(190, 58, 30, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(191, 57, 30, 1, 97, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(192, 65, 30, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(193, 62, 30, 1, 93, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(194, 68, 30, 1, 92, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(195, 74, 30, 1, 97, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(196, 52, 30, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(197, 71, 30, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(198, 64, 30, 1, 93, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(199, 61, 30, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(200, 70, 30, 1, 91, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(201, 55, 26, 1, 50, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(202, 53, 26, 1, 51, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(203, 66, 26, 1, 54, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(204, 60, 26, 1, 56, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(205, 73, 26, 1, 53, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(206, 59, 26, 1, 57, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(207, 75, 26, 1, 58, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(208, 72, 26, 1, 59, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(209, 67, 26, 1, 60, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(210, 69, 26, 1, 61, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(211, 54, 26, 1, 63, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(212, 76, 26, 1, 62, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(213, 56, 26, 1, 65, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(214, 63, 26, 1, 64, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(215, 58, 26, 1, 67, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(216, 57, 26, 1, 69, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(217, 65, 26, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(218, 62, 26, 1, 55, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(219, 68, 26, 1, 64, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(220, 74, 26, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(221, 52, 26, 1, 74, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(222, 71, 26, 1, 78, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(223, 64, 26, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(224, 61, 26, 1, 74, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(225, 70, 26, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(226, 55, 28, 1, 90, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(227, 53, 28, 1, 95, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(228, 66, 28, 1, 96, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(229, 60, 28, 1, 93, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(230, 73, 28, 1, 92, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(231, 59, 28, 1, 94, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(232, 75, 28, 1, 98, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(233, 72, 28, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(234, 67, 28, 1, 88, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(235, 69, 28, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(236, 54, 28, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(237, 76, 28, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(238, 56, 28, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(239, 63, 28, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(240, 58, 28, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(241, 57, 28, 1, 86, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(242, 65, 28, 1, 84, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(243, 62, 28, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(244, 68, 28, 1, 89, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(245, 74, 28, 1, 85, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(246, 52, 28, 1, 86, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(247, 71, 28, 1, 83, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(248, 64, 28, 1, 87, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(249, 61, 28, 1, 77, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(250, 70, 28, 1, 99, NULL, 1, 1, '', '2019-07-03 00:00:00', NULL, 'admin', NULL),
(251, 88, 8, 1, 80, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(252, 90, 8, 1, 90, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(253, 77, 8, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(254, 91, 8, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(255, 82, 8, 1, 96, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(256, 92, 8, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(257, 85, 8, 1, 85, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(258, 87, 8, 1, 64, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(259, 93, 8, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(260, 94, 8, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(261, 78, 8, 1, 92, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(262, 79, 8, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(263, 95, 8, 1, 84, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(264, 81, 8, 1, 73, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(265, 96, 8, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(266, 97, 8, 1, 79, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(267, 98, 8, 1, 79, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(268, 99, 8, 1, 90, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(269, 100, 8, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(270, 101, 8, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(271, 86, 8, 1, 90, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(272, 89, 8, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(273, 83, 8, 1, 88, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(274, 84, 8, 1, 88, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(275, 80, 8, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(276, 102, 8, 1, 79, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(277, 103, 8, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(278, 88, 2, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(279, 90, 2, 1, 80, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(280, 77, 2, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(281, 91, 2, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(282, 82, 2, 1, 85, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(283, 92, 2, 1, 84, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(284, 85, 2, 1, 83, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(285, 87, 2, 1, 81, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(286, 93, 2, 1, 82, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(287, 94, 2, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(288, 78, 2, 1, 95, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(289, 79, 2, 1, 93, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(290, 95, 2, 1, 93, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(291, 81, 2, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(292, 96, 2, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(293, 97, 2, 1, 73, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(294, 98, 2, 1, 72, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(295, 99, 2, 1, 71, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(296, 100, 2, 1, 60, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(297, 101, 2, 1, 69, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(298, 86, 2, 1, 67, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(299, 89, 2, 1, 65, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(300, 83, 2, 1, 64, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(301, 84, 2, 1, 63, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(302, 80, 2, 1, 62, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(303, 102, 2, 1, 61, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(304, 103, 2, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(305, 88, 5, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(306, 90, 5, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(307, 77, 5, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(308, 91, 5, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(309, 82, 5, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(310, 92, 5, 1, 95, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(311, 85, 5, 1, 94, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(312, 87, 5, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(313, 93, 5, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(314, 94, 5, 1, 74, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(315, 78, 5, 1, 73, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(316, 79, 5, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(317, 95, 5, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(318, 81, 5, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(319, 96, 5, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(320, 97, 5, 1, 79, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(321, 98, 5, 1, 70, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(322, 99, 5, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(323, 100, 5, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(324, 101, 5, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(325, 86, 5, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(326, 89, 5, 1, 71, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(327, 83, 5, 1, 56, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(328, 84, 5, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(329, 80, 5, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(330, 102, 5, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(331, 103, 5, 1, 59, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(332, 88, 1, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(333, 90, 1, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(334, 77, 1, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(335, 91, 1, 1, 96, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(336, 82, 1, 1, 96, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(337, 92, 1, 1, 94, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(338, 85, 1, 1, 93, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(339, 87, 1, 1, 91, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(340, 93, 1, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(341, 94, 1, 1, 95, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(342, 78, 1, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(343, 79, 1, 1, 69, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(344, 95, 1, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(345, 81, 1, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(346, 96, 1, 1, 74, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(347, 97, 1, 1, 73, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(348, 98, 1, 1, 72, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(349, 99, 1, 1, 77, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(350, 100, 1, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(351, 101, 1, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(352, 86, 1, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(353, 83, 1, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(354, 89, 1, 1, 74, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(355, 84, 1, 1, 88, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(356, 80, 1, 1, 99, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(357, 102, 1, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(358, 103, 1, 1, 90, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(359, 88, 10, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(360, 90, 10, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(361, 77, 10, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(362, 91, 10, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(363, 82, 10, 1, 9, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(364, 92, 10, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(365, 85, 10, 1, 79, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(366, 87, 10, 1, 96, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(367, 93, 10, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(368, 94, 10, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(369, 78, 10, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(370, 79, 10, 1, 88, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(371, 95, 10, 1, 99, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(372, 81, 10, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(373, 96, 10, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(374, 97, 10, 1, 55, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(375, 98, 10, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(376, 99, 10, 1, 67, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(377, 100, 10, 1, 88, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(378, 101, 10, 1, 45, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(379, 86, 10, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(380, 83, 10, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(381, 89, 10, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(382, 84, 10, 1, 56, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(383, 80, 10, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(384, 102, 10, 1, 99, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(385, 103, 10, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(386, 88, 3, 1, 67, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(387, 90, 3, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(388, 77, 3, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(389, 91, 3, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(390, 82, 3, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(391, 92, 3, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(392, 85, 3, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(393, 87, 3, 1, 81, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(394, 93, 3, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(395, 94, 3, 1, 50, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(396, 78, 3, 1, 67, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(397, 79, 3, 1, 59, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(398, 95, 3, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(399, 81, 3, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(400, 96, 3, 1, 68, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(401, 97, 3, 1, 69, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(402, 98, 3, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(403, 99, 3, 1, 65, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(404, 100, 3, 1, 67, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(405, 101, 3, 1, 77, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(406, 86, 3, 1, 88, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(407, 83, 3, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(408, 89, 3, 1, 55, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(409, 84, 3, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(410, 80, 3, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(411, 102, 3, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(412, 103, 3, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(413, 88, 9, 1, 90, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(414, 90, 9, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(415, 77, 9, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(416, 91, 9, 1, 99, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(417, 82, 9, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(418, 92, 9, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(419, 85, 9, 1, 96, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(420, 87, 9, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(421, 93, 9, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(422, 94, 9, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(423, 78, 9, 1, 85, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(424, 79, 9, 1, 84, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(425, 95, 9, 1, 93, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(426, 81, 9, 1, 92, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(427, 96, 9, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(428, 97, 9, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(429, 98, 9, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(430, 99, 9, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(431, 100, 9, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(432, 101, 9, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(433, 86, 9, 1, 74, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(434, 89, 9, 1, 73, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(435, 83, 9, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(436, 84, 9, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(437, 80, 9, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(438, 102, 9, 1, 77, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(439, 103, 9, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(440, 88, 6, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(441, 90, 6, 1, 39, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(442, 77, 6, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(443, 91, 6, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(444, 82, 6, 1, 67, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(445, 92, 6, 1, 65, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(446, 85, 6, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(447, 87, 6, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(448, 93, 6, 1, 84, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(449, 94, 6, 1, 73, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(450, 78, 6, 1, 72, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(451, 79, 6, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(452, 95, 6, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(453, 81, 6, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(454, 96, 6, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(455, 97, 6, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(456, 98, 6, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(457, 99, 6, 1, 56, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(458, 100, 6, 1, 57, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(459, 101, 6, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(460, 86, 6, 1, 95, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(461, 89, 6, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(462, 83, 6, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(463, 84, 6, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(464, 80, 6, 1, 55, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(465, 102, 6, 1, 56, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(466, 103, 6, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(467, 88, 4, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(468, 90, 4, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(469, 77, 4, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(470, 91, 4, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(471, 82, 4, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(472, 92, 4, 1, 88, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(473, 85, 4, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(474, 87, 4, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(475, 93, 4, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(476, 94, 4, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(477, 78, 4, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(478, 79, 4, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(479, 95, 4, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(480, 81, 4, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(481, 96, 4, 1, 96, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(482, 97, 4, 1, 95, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(483, 98, 4, 1, 94, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(484, 99, 4, 1, 93, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(485, 100, 4, 1, 92, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(486, 101, 4, 1, 91, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(487, 86, 4, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(488, 83, 4, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(489, 89, 4, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(490, 84, 4, 1, 85, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(491, 80, 4, 1, 84, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(492, 102, 4, 1, 83, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(493, 103, 4, 1, 82, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(494, 88, 7, 1, 89, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(495, 90, 7, 1, 90, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(496, 77, 7, 1, 98, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(497, 91, 7, 1, 97, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(498, 82, 7, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(499, 92, 7, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(500, 85, 7, 1, 85, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(501, 87, 7, 1, 84, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(502, 93, 7, 1, 83, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(503, 94, 7, 1, 82, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(504, 78, 7, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(505, 79, 7, 1, 87, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(506, 95, 7, 1, 86, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(507, 81, 7, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(508, 96, 7, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(509, 97, 7, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(510, 98, 7, 1, 74, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(511, 99, 7, 1, 73, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(512, 100, 7, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(513, 101, 7, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(514, 86, 7, 1, 74, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(515, 83, 7, 1, 78, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(516, 89, 7, 1, 76, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(517, 84, 7, 1, 66, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(518, 80, 7, 1, 75, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(519, 102, 7, 1, 77, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(520, 103, 7, 1, 74, NULL, 1, 1, '', '2019-07-04 00:00:00', NULL, 'admin', NULL),
(521, 132, 36, 1, 100, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(522, 154, 36, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(523, 140, 36, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(524, 144, 36, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(525, 145, 36, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(526, 148, 36, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(527, 149, 36, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(528, 143, 36, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(529, 153, 36, 1, 34, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(530, 137, 36, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(531, 147, 36, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(532, 129, 36, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(533, 151, 36, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(534, 130, 36, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(535, 138, 36, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(536, 150, 36, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(537, 141, 36, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(538, 133, 36, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(539, 152, 36, 1, 85, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(540, 142, 36, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(541, 146, 36, 1, 83, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(542, 135, 36, 1, 81, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(543, 139, 36, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(544, 134, 36, 1, 64, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(545, 136, 36, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(546, 131, 36, 1, 68, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(547, 132, 38, 1, 89, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(548, 154, 38, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(549, 140, 38, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(550, 144, 38, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(551, 145, 38, 1, 96, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(552, 148, 38, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(553, 149, 38, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(554, 143, 38, 1, 89, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(555, 153, 38, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(556, 137, 38, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(557, 147, 38, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(558, 129, 38, 1, 83, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(559, 151, 38, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(560, 130, 38, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(561, 138, 38, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(562, 150, 38, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(563, 141, 38, 1, 79, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(564, 133, 38, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(565, 152, 38, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(566, 142, 38, 1, 77, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(567, 146, 38, 1, 88, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(568, 135, 38, 1, 99, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(569, 139, 38, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(570, 134, 38, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(571, 136, 38, 1, 64, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(572, 131, 38, 1, 56, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(573, 132, 40, 1, 69, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(574, 154, 40, 1, 89, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(575, 140, 40, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(576, 144, 40, 1, 99, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(577, 145, 40, 1, 88, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(578, 148, 40, 1, 77, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(579, 149, 40, 1, 66, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(580, 143, 40, 1, 55, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(581, 153, 40, 1, 44, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(582, 137, 40, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(583, 147, 40, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(584, 129, 40, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(585, 151, 40, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(586, 130, 40, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(587, 138, 40, 1, 66, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(588, 150, 40, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(589, 141, 40, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(590, 133, 40, 1, 66, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(591, 152, 40, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(592, 142, 40, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(593, 146, 40, 1, 64, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(594, 135, 40, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(595, 139, 40, 1, 66, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(596, 134, 40, 1, 55, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(597, 136, 40, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(598, 131, 40, 1, 88, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(599, 132, 33, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(600, 154, 33, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(601, 140, 33, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(602, 144, 33, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(603, 145, 33, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(604, 148, 33, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(605, 149, 33, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(606, 143, 33, 1, 96, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(607, 153, 33, 1, 55, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(608, 137, 33, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(609, 147, 33, 1, 57, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(610, 129, 33, 1, 54, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(611, 151, 33, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(612, 130, 33, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(613, 138, 33, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(614, 150, 33, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(615, 141, 33, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(616, 133, 33, 1, 55, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(617, 152, 33, 1, 44, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(618, 142, 33, 1, 88, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(619, 146, 33, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(620, 135, 33, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(621, 139, 33, 1, 55, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(622, 134, 33, 1, 77, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(623, 136, 33, 1, 54, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(624, 131, 33, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(625, 132, 31, 1, 89, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL);
INSERT INTO `grades` (`id`, `student`, `course`, `evaluation`, `grade_value`, `rewrite_grade_value`, `validate`, `publish`, `comment`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(626, 154, 31, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(627, 140, 31, 1, 99, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(628, 144, 31, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(629, 145, 31, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(630, 148, 31, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(631, 149, 31, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(632, 143, 31, 1, 85, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(633, 153, 31, 1, 85, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(634, 137, 31, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(635, 147, 31, 1, 83, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(636, 129, 31, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(637, 151, 31, 1, 72, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(638, 130, 31, 1, 71, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(639, 138, 31, 1, 70, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(640, 150, 31, 1, 79, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(641, 141, 31, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(642, 133, 31, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(643, 152, 31, 1, 69, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(644, 142, 31, 1, 68, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(645, 146, 31, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(646, 135, 31, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(647, 139, 31, 1, 49, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(648, 134, 31, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(649, 136, 31, 1, 45, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(650, 131, 31, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', '2019-07-11 16:08:21', 'admin', 'admin'),
(651, 132, 39, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(652, 154, 39, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(653, 140, 39, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(654, 144, 39, 1, 96, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(655, 145, 39, 1, 95, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(656, 148, 39, 1, 94, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(657, 149, 39, 1, 93, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(658, 143, 39, 1, 92, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(659, 153, 39, 1, 91, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(660, 137, 39, 1, 99, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(661, 147, 39, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(662, 129, 39, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(663, 151, 39, 1, 96, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(664, 130, 39, 1, 94, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(665, 138, 39, 1, 93, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(666, 150, 39, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(667, 141, 39, 1, 99, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(668, 133, 39, 1, 94, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(669, 152, 39, 1, 93, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(670, 142, 39, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(671, 146, 39, 1, 94, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(672, 135, 39, 1, 99, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(673, 139, 39, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(674, 134, 39, 1, 99, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(675, 136, 39, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(676, 131, 39, 1, 96, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(677, 132, 34, 1, 89, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(678, 154, 34, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(679, 140, 34, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(680, 144, 34, 1, 85, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(681, 145, 34, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(682, 148, 34, 1, 83, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(683, 149, 34, 1, 82, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(684, 143, 34, 1, 80, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(685, 153, 34, 1, 81, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(686, 137, 34, 1, 89, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(687, 147, 34, 1, 79, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(688, 129, 34, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(689, 151, 34, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(690, 130, 34, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(691, 138, 34, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(692, 150, 34, 1, 73, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(693, 141, 34, 1, 72, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(694, 133, 34, 1, 71, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(695, 152, 34, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(696, 142, 34, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(697, 146, 34, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(698, 135, 34, 1, 69, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(699, 139, 34, 1, 59, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(700, 134, 34, 1, 95, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(701, 136, 34, 1, 93, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(702, 131, 34, 1, 93, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(703, 132, 35, 1, 88, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(704, 154, 35, 1, 79, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(705, 140, 35, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(706, 144, 35, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(707, 145, 35, 1, 85, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(708, 148, 35, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(709, 149, 35, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(710, 143, 35, 1, 83, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(711, 153, 35, 1, 82, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(712, 137, 35, 1, 81, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(713, 147, 35, 1, 80, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(714, 129, 35, 1, 79, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(715, 151, 35, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(716, 130, 35, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(717, 138, 35, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(718, 150, 35, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(719, 141, 35, 1, 57, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(720, 133, 35, 1, 49, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(721, 152, 35, 1, 73, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(722, 142, 35, 1, 72, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(723, 146, 35, 1, 71, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(724, 135, 35, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(725, 139, 35, 1, 64, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(726, 134, 35, 1, 63, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(727, 131, 35, 1, 62, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(728, 136, 35, 1, 69, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(729, 132, 37, 1, 39, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(730, 154, 37, 1, 68, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(731, 140, 37, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(732, 144, 37, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(733, 145, 37, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(734, 148, 37, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(735, 149, 37, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(736, 143, 37, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(737, 153, 37, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(738, 137, 37, 1, 96, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(739, 147, 37, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(740, 129, 37, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(741, 151, 37, 1, 85, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(742, 130, 37, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(743, 138, 37, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(744, 150, 37, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(745, 141, 37, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(746, 133, 37, 1, 73, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(747, 152, 37, 1, 72, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(748, 142, 37, 1, 71, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(749, 146, 37, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(750, 135, 37, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(751, 139, 37, 1, 64, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(752, 134, 37, 1, 67, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(753, 131, 37, 1, 54, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(754, 136, 37, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(755, 132, 32, 1, 90, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(756, 154, 32, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(757, 140, 32, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(758, 144, 32, 1, 96, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(759, 145, 32, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(760, 148, 32, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(761, 149, 32, 1, 86, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(762, 143, 32, 1, 84, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(763, 153, 32, 1, 83, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(764, 137, 32, 1, 82, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(765, 147, 32, 1, 81, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(766, 129, 32, 1, 78, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(767, 151, 32, 1, 79, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(768, 130, 32, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(769, 138, 32, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(770, 150, 32, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(771, 141, 32, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(772, 133, 32, 1, 74, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(773, 152, 32, 1, 76, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(774, 142, 32, 1, 75, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(775, 146, 32, 1, 79, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(776, 135, 32, 1, 57, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(777, 139, 32, 1, 98, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(778, 134, 32, 1, 97, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(779, 131, 32, 1, 87, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(780, 136, 32, 1, 65, NULL, 1, 1, '', '2019-07-11 00:00:00', NULL, 'admin', NULL),
(781, 106, 14, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(782, 114, 14, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(783, 110, 14, 1, 96, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(784, 118, 14, 1, 65, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(785, 119, 14, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(786, 113, 14, 1, 84, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(787, 107, 14, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(788, 111, 14, 1, 82, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(789, 120, 14, 1, 95, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(790, 121, 14, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(791, 112, 14, 1, 97, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(792, 116, 14, 1, 54, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(793, 122, 14, 1, 58, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(794, 115, 14, 1, 82, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(795, 123, 14, 1, 65, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(796, 104, 14, 1, 63, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(797, 108, 14, 1, 61, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(798, 124, 14, 1, 68, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(799, 109, 14, 1, 74, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(800, 125, 14, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(801, 126, 14, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(802, 117, 14, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(803, 127, 14, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(804, 128, 14, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(805, 105, 14, 1, 77, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(806, 106, 16, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(807, 114, 16, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(808, 110, 16, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(809, 118, 16, 1, 97, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(810, 119, 16, 1, 66, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(811, 113, 16, 1, 90, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(812, 107, 16, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(813, 111, 16, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(814, 120, 16, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(815, 121, 16, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(816, 112, 16, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(817, 116, 16, 1, 65, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(818, 122, 16, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(819, 115, 16, 1, 90, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(820, 123, 16, 1, 65, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(821, 104, 16, 1, 55, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(822, 108, 16, 1, 48, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(823, 124, 16, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(824, 109, 16, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(825, 125, 16, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(826, 126, 16, 1, 92, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(827, 117, 16, 1, 91, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(828, 127, 16, 1, 90, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(829, 128, 16, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(830, 105, 16, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(831, 106, 12, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(832, 114, 12, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(833, 110, 12, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(834, 118, 12, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(835, 119, 12, 1, 56, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(836, 113, 12, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(837, 107, 12, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(838, 111, 12, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(839, 120, 12, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(840, 121, 12, 1, 77, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(841, 112, 12, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(842, 116, 12, 1, 93, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(843, 122, 12, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(844, 115, 12, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(845, 123, 12, 1, 85, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(846, 104, 12, 1, 37, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(847, 108, 12, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(848, 124, 12, 1, 56, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(849, 109, 12, 1, 43, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(850, 125, 12, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(851, 126, 12, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(852, 117, 12, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(853, 127, 12, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(854, 128, 12, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(855, 105, 12, 1, 77, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(856, 106, 15, 1, 90, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(857, 114, 15, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(858, 110, 15, 1, 97, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(859, 118, 15, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(860, 119, 15, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(861, 113, 15, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(862, 107, 15, 1, 85, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(863, 111, 15, 1, 85, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(864, 120, 15, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(865, 121, 15, 1, 75, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(866, 112, 15, 1, 72, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(867, 116, 15, 1, 71, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(868, 122, 15, 1, 70, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(869, 115, 15, 1, 79, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(870, 123, 15, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(871, 124, 15, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(872, 109, 15, 1, 96, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(873, 126, 15, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(874, 117, 15, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(875, 127, 15, 1, 59, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(876, 128, 15, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(877, 106, 19, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(878, 114, 19, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(879, 110, 19, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(880, 118, 19, 1, 90, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(881, 119, 19, 1, 90, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(882, 113, 19, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(883, 107, 19, 1, 97, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(884, 111, 19, 1, 97, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(885, 120, 19, 1, 90, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(886, 121, 19, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(887, 112, 19, 1, 97, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(888, 116, 19, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(889, 122, 19, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(890, 115, 19, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(891, 123, 19, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(892, 124, 19, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(893, 109, 19, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(894, 126, 19, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(895, 117, 19, 1, 65, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(896, 127, 19, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(897, 128, 19, 1, 77, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(898, 106, 13, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(899, 114, 13, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(900, 110, 13, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(901, 118, 13, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(902, 119, 13, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(903, 113, 13, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(904, 107, 13, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(905, 111, 13, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(906, 120, 13, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(907, 121, 13, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(908, 112, 13, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(909, 116, 13, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(910, 122, 13, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(911, 115, 13, 1, 56, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(912, 123, 13, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(913, 124, 13, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(914, 109, 13, 1, 99, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(915, 126, 13, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(916, 117, 13, 1, 77, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(917, 127, 13, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(918, 128, 13, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(919, 106, 20, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(920, 114, 20, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(921, 110, 20, 1, 77, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(922, 118, 20, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(923, 119, 20, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(924, 113, 20, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(925, 107, 20, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(926, 111, 20, 1, 56, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(927, 120, 20, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(928, 121, 20, 1, 54, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(929, 112, 20, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(930, 116, 20, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(931, 122, 20, 1, 77, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(932, 115, 20, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(933, 123, 20, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(934, 124, 20, 1, 85, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(935, 109, 20, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(936, 126, 20, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(937, 117, 20, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(938, 127, 20, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(939, 128, 20, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(940, 106, 18, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(941, 114, 18, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(942, 110, 18, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(943, 118, 18, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(944, 119, 18, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(945, 113, 18, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(946, 107, 18, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(947, 111, 18, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(948, 120, 18, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(949, 121, 18, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(950, 112, 18, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(951, 116, 18, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(952, 122, 18, 1, 56, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(953, 115, 18, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(954, 123, 18, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(955, 124, 18, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(956, 109, 18, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(957, 126, 18, 1, 65, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(958, 117, 18, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(959, 127, 18, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(960, 128, 18, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(961, 106, 11, 1, 89, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(962, 114, 11, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(963, 110, 11, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(964, 118, 11, 1, 97, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(965, 119, 11, 1, 66, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(966, 113, 11, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(967, 107, 11, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(968, 111, 11, 1, 88, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(969, 120, 11, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(970, 121, 11, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(971, 112, 11, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(972, 116, 11, 1, 75, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(973, 122, 11, 1, 56, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(974, 115, 11, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(975, 123, 11, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(976, 124, 11, 1, 57, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(977, 109, 11, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(978, 126, 11, 1, 56, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(979, 117, 11, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(980, 127, 11, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(981, 128, 11, 1, 45, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(982, 106, 17, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(983, 114, 17, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(984, 110, 17, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(985, 118, 17, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(986, 119, 17, 1, 87, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(987, 113, 17, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(988, 107, 17, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(989, 111, 17, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(990, 120, 17, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(991, 121, 17, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(992, 112, 17, 1, 98, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(993, 116, 17, 1, 78, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(994, 122, 17, 1, 76, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(995, 115, 17, 1, 75, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(996, 123, 17, 1, 45, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(997, 124, 17, 1, 86, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(998, 109, 17, 1, 85, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(999, 126, 17, 1, 84, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(1000, 117, 17, 1, 83, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(1001, 127, 17, 1, 85, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(1002, 128, 17, 1, 67, NULL, 1, 1, '', '2019-07-24 00:00:00', NULL, 'admin', NULL),
(1003, 232, 42, 1, 140, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1004, 229, 42, 1, 150, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1005, 231, 42, 1, 100, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1006, 233, 42, 1, 70, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1007, 230, 42, 1, 99, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1008, 236, 42, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1009, 234, 42, 1, 100, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1010, 237, 42, 1, 120, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1011, 235, 42, 1, 134, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1012, 228, 42, 1, 23, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1013, 232, 43, 1, 90, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1014, 229, 43, 1, 76, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1015, 231, 43, 1, 56, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1016, 233, 43, 1, 20, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1017, 230, 43, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1018, 236, 43, 1, 89, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1019, 234, 43, 1, 98, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1020, 237, 43, 1, 100, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1021, 235, 43, 1, 23, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1022, 228, 43, 1, 56, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1023, 232, 44, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1024, 229, 44, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1025, 231, 44, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1026, 233, 44, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1027, 230, 44, 1, NULL, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1028, 236, 44, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-07 17:26:26', 'admin', 'admin'),
(1029, 234, 44, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1030, 237, 44, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1031, 235, 44, 1, 32, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1032, 228, 44, 1, 28, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1033, 232, 49, 1, 120, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1034, 229, 49, 1, 150, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1035, 231, 49, 1, 146, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1036, 233, 49, 1, 86, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1037, 230, 49, 1, 145, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1038, 236, 49, 1, 56, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1039, 234, 49, 1, 143, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1040, 237, 49, 1, 110, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1041, 235, 49, 1, 98, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1042, 228, 49, 1, 0, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1043, 229, 52, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1044, 232, 52, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1045, 231, 52, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1046, 233, 52, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1047, 230, 52, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1048, 236, 52, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1049, 234, 52, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1050, 237, 52, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1051, 235, 52, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1052, 228, 52, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1053, 232, 50, 1, 75, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1054, 229, 50, 1, 75, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1055, 231, 50, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1056, 233, 50, 1, 26, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1057, 230, 50, 1, 65, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1058, 236, 50, 1, 70, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1059, 234, 50, 1, 66, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1060, 237, 50, 1, 65, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1061, 235, 50, 1, 74, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1062, 228, 50, 1, 15, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1063, 232, 45, 1, 120, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:29', 'admin', 'admin'),
(1064, 229, 45, 1, 150, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:29:09', 'admin', 'admin'),
(1065, 231, 45, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1066, 233, 45, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1067, 230, 45, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1068, 236, 45, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1069, 234, 45, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1070, 237, 45, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1071, 235, 45, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1072, 228, 45, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1073, 229, 53, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1074, 232, 53, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1075, 231, 53, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:49', 'admin', 'admin'),
(1076, 233, 53, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1077, 230, 53, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1078, 236, 53, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1079, 234, 53, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1080, 237, 53, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1081, 235, 53, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1082, 228, 53, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1083, 232, 56, 1, 75, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1084, 229, 56, 1, 80, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:29:45', 'admin', 'admin'),
(1085, 231, 56, 1, 65, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1086, 233, 56, 1, 76, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1087, 230, 56, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1088, 236, 56, 1, 80, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1089, 234, 56, 1, 70, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1090, 237, 56, 1, 55, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1091, 235, 56, 1, 85, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1092, 228, 56, 1, 60, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(1093, 232, 54, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1094, 229, 54, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1095, 231, 54, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1096, 233, 54, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1097, 230, 54, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1098, 236, 54, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1099, 234, 54, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1100, 237, 54, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1101, 235, 54, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1102, 228, 54, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1103, 229, 48, 1, 100, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:17', 'admin', 'admin'),
(1104, 232, 48, 1, 115, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:38', 'admin', 'admin'),
(1105, 231, 48, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1106, 233, 48, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1107, 230, 48, 1, 100, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:23', 'admin', 'admin'),
(1108, 236, 48, 1, 100, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:33', 'admin', 'admin'),
(1109, 234, 48, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1110, 237, 48, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1111, 235, 48, 1, 190, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:20', 'admin', 'admin'),
(1112, 228, 48, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1113, 232, 55, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1114, 229, 55, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1115, 231, 55, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1116, 233, 55, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1117, 230, 55, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1118, 236, 55, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1119, 234, 55, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1120, 237, 55, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1121, 235, 55, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1122, 228, 55, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1123, 229, 46, 1, 190, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:28:47', 'admin', 'admin'),
(1124, 232, 46, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1125, 231, 46, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1126, 233, 46, 1, 180, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:30:46', 'admin', 'admin'),
(1127, 230, 46, 1, 170, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:30:58', 'admin', 'admin'),
(1128, 236, 46, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1129, 234, 46, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1130, 237, 46, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1131, 235, 46, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1132, 228, 46, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1133, 232, 47, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1134, 229, 47, 1, 90, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:29:21', 'admin', 'admin'),
(1135, 231, 47, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1136, 233, 47, 1, 20, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1137, 230, 47, 1, 90, NULL, 1, 1, '', '2019-08-05 00:00:00', '2019-08-05 14:31:19', 'admin', 'admin'),
(1138, 236, 47, 1, 20, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1139, 234, 47, 1, 20, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1140, 237, 47, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1141, 235, 47, 1, 10, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1142, 228, 47, 1, 60, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1143, 232, 61, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1144, 229, 61, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1145, 231, 61, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1146, 233, 61, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1147, 230, 61, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1148, 236, 61, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1149, 234, 61, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1150, 237, 61, 1, 46, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1151, 235, 61, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1152, 228, 61, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1153, 232, 59, 1, 75, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1154, 229, 59, 1, 65, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1155, 231, 59, 1, 70, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1156, 233, 59, 1, 80, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1157, 230, 59, 1, 75, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1158, 236, 59, 1, 60, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1159, 234, 59, 1, 85, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1160, 237, 59, 1, 76, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1161, 235, 59, 1, 80, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1162, 228, 59, 1, 55, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1163, 232, 57, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1164, 229, 57, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1165, 231, 57, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1166, 233, 57, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1167, 230, 57, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1168, 236, 57, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1169, 234, 57, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1170, 237, 57, 1, 46, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1171, 235, 57, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1172, 228, 57, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1173, 232, 58, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1174, 229, 58, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1175, 231, 58, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1176, 233, 58, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1177, 230, 58, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1178, 236, 58, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1179, 234, 58, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1180, 237, 58, 1, 46, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1181, 235, 58, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1182, 228, 58, 1, 50, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1183, 232, 60, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1184, 229, 60, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1185, 231, 60, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1186, 233, 60, 1, 30, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1187, 230, 60, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1188, 236, 60, 1, 40, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1189, 234, 60, 1, 35, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1190, 237, 60, 1, 46, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1191, 235, 60, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1192, 228, 60, 1, 45, NULL, 1, 1, '', '2019-08-05 00:00:00', NULL, 'admin', NULL),
(1193, 238, 45, 1, 120, NULL, 1, 1, '', '2019-08-06 00:00:00', NULL, 'admin', NULL),
(1194, 238, 48, 1, 157, NULL, 1, 1, '', '2019-08-06 00:00:00', NULL, 'admin', NULL),
(1195, 238, 46, 1, 125, NULL, 1, 1, '', '2019-08-06 00:00:00', NULL, 'admin', NULL),
(1196, 55, 23, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1197, 66, 23, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1198, 60, 23, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1199, 73, 23, 2, 60, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1200, 59, 23, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1201, 75, 23, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1202, 72, 23, 2, 85, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1203, 67, 23, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1204, 69, 23, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1205, 54, 23, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1206, 76, 23, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1207, 56, 23, 2, 79, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1208, 63, 23, 2, 69, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1209, 58, 23, 2, 68, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1210, 57, 23, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1211, 65, 23, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1212, 62, 23, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1213, 68, 23, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1214, 74, 23, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1215, 52, 23, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1216, 71, 23, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1217, 64, 23, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1218, 61, 23, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1219, 70, 23, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1220, 55, 25, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1221, 66, 25, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1222, 60, 25, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1223, 73, 25, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1224, 59, 25, 2, 60, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1225, 75, 25, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1226, 72, 25, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1227, 67, 25, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1228, 69, 25, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1229, 54, 25, 2, 68, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1230, 76, 25, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL);
INSERT INTO `grades` (`id`, `student`, `course`, `evaluation`, `grade_value`, `rewrite_grade_value`, `validate`, `publish`, `comment`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1231, 56, 25, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1232, 63, 25, 2, 79, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1233, 58, 25, 2, 69, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1234, 57, 25, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1235, 65, 25, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1236, 62, 25, 2, 96, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1237, 68, 25, 2, 86, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1238, 74, 25, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1239, 52, 25, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1240, 71, 25, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1241, 64, 25, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1242, 61, 25, 2, 85, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1243, 70, 25, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1244, 55, 29, 2, 60, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1245, 66, 29, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1246, 60, 29, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1247, 73, 29, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1248, 59, 29, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1249, 75, 29, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1250, 72, 29, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1251, 67, 29, 2, 85, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1252, 69, 29, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1253, 54, 29, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1254, 76, 29, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1255, 56, 29, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1256, 63, 29, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1257, 58, 29, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1258, 57, 29, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1259, 65, 29, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1260, 62, 29, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1261, 68, 29, 2, 79, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1262, 74, 29, 2, 69, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1263, 52, 29, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1264, 71, 29, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1265, 64, 29, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1266, 61, 29, 2, 68, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1267, 70, 29, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(1268, 55, 22, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1269, 66, 22, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1270, 60, 22, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1271, 73, 22, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1272, 59, 22, 2, 74, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1273, 75, 22, 2, 73, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1274, 72, 22, 2, 71, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1275, 67, 22, 2, 72, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1276, 69, 22, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1277, 54, 22, 2, 69, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1278, 76, 22, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1279, 56, 22, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1280, 63, 22, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1281, 58, 22, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1282, 57, 22, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1283, 65, 22, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1284, 62, 22, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1285, 68, 22, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1286, 74, 22, 2, 91, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1287, 52, 22, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1288, 71, 22, 2, 93, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1289, 64, 22, 2, 94, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1290, 61, 22, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1291, 70, 22, 2, 96, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1292, 55, 21, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1293, 66, 21, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1294, 60, 21, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1295, 73, 21, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1296, 59, 21, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1297, 75, 21, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1298, 72, 21, 2, 94, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1299, 67, 21, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1300, 69, 21, 2, 93, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1301, 54, 21, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1302, 76, 21, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1303, 56, 21, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1304, 63, 21, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1305, 58, 21, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1306, 57, 21, 2, 91, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1307, 65, 21, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1308, 62, 21, 2, 68, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1309, 68, 21, 2, 69, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1310, 74, 21, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1311, 52, 21, 2, 64, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1312, 71, 21, 2, 63, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1313, 64, 21, 2, 62, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1314, 61, 21, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1315, 70, 21, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1316, 55, 27, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1317, 66, 27, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1318, 60, 27, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1319, 73, 27, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1320, 59, 27, 2, 83, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1321, 75, 27, 2, 84, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1322, 72, 27, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1323, 67, 27, 2, 85, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1324, 69, 27, 2, 86, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1325, 54, 27, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1326, 76, 27, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1327, 56, 27, 2, 81, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1328, 63, 27, 2, 82, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1329, 58, 27, 2, 83, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1330, 57, 27, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1331, 65, 27, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1332, 62, 27, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1333, 68, 27, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1334, 74, 27, 2, 96, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1335, 52, 27, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1336, 71, 27, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1337, 64, 27, 2, 94, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1338, 61, 27, 2, 93, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1339, 70, 27, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1340, 55, 24, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1341, 66, 24, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1342, 60, 24, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1343, 73, 24, 2, 86, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1344, 59, 24, 2, 85, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1345, 75, 24, 2, 84, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1346, 72, 24, 2, 83, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1347, 67, 24, 2, 82, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1348, 69, 24, 2, 81, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1349, 54, 24, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1350, 76, 24, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1351, 56, 24, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1352, 63, 24, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1353, 58, 24, 2, 96, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1354, 57, 24, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1355, 65, 24, 2, 94, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1356, 62, 24, 2, 93, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1357, 68, 24, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1358, 74, 24, 2, 91, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1359, 52, 24, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1360, 71, 24, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1361, 64, 24, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1362, 61, 24, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1363, 70, 24, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1364, 55, 30, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1365, 66, 30, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1366, 60, 30, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1367, 73, 30, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1368, 59, 30, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1369, 75, 30, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1370, 72, 30, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1371, 67, 30, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1372, 69, 30, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1373, 54, 30, 2, 68, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1374, 76, 30, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1375, 56, 30, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1376, 63, 30, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1377, 58, 30, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1378, 57, 30, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1379, 65, 30, 2, 62, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1380, 62, 30, 2, 61, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1381, 68, 30, 2, 68, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1382, 74, 30, 2, 69, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1383, 52, 30, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1384, 71, 30, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1385, 64, 30, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1386, 61, 30, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1387, 70, 30, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1388, 55, 41, 2, 120, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1389, 66, 41, 2, 130, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1390, 60, 41, 2, 140, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1391, 73, 41, 2, 150, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1392, 59, 41, 2, 149, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1393, 75, 41, 2, 148, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1394, 72, 41, 2, 147, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1395, 67, 41, 2, 146, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1396, 69, 41, 2, 145, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1397, 54, 41, 2, 144, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1398, 76, 41, 2, 143, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1399, 56, 41, 2, 142, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1400, 63, 41, 2, 141, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1401, 58, 41, 2, 139, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1402, 57, 41, 2, 138, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1403, 65, 41, 2, 137, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1404, 62, 41, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1405, 68, 41, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1406, 74, 41, 2, 134, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1407, 52, 41, 2, 60, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1408, 71, 41, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1409, 64, 41, 2, 30, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1410, 61, 41, 2, 20, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1411, 70, 41, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1412, 55, 26, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1413, 66, 26, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1414, 60, 26, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1415, 73, 26, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1416, 59, 26, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1417, 75, 26, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1418, 72, 26, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1419, 67, 26, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1420, 69, 26, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1421, 54, 26, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1422, 76, 26, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1423, 56, 26, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1424, 63, 26, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1425, 58, 26, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1426, 57, 26, 2, 23, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1427, 65, 26, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1428, 62, 26, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1429, 68, 26, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1430, 74, 26, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1431, 52, 26, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1432, 71, 26, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1433, 64, 26, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1434, 61, 26, 2, 33, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1435, 70, 26, 2, 22, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1436, 55, 28, 2, 11, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1437, 66, 28, 2, 22, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1438, 60, 28, 2, 33, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1439, 73, 28, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1440, 59, 28, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1441, 75, 28, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1442, 72, 28, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1443, 67, 28, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1444, 69, 28, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1445, 54, 28, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1446, 76, 28, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1447, 56, 28, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1448, 63, 28, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1449, 58, 28, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1450, 57, 28, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1451, 65, 28, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1452, 62, 28, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1453, 68, 28, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1454, 74, 28, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1455, 52, 28, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1456, 71, 28, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1457, 64, 28, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1458, 61, 28, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1459, 70, 28, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1460, 232, 45, 2, 198, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1461, 229, 45, 2, 150, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1462, 231, 45, 2, 160, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1463, 233, 45, 2, 170, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1464, 230, 45, 2, 180, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1465, 236, 45, 2, 185, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1466, 239, 45, 2, 195, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1467, 238, 45, 2, 200, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1468, 234, 45, 2, 185, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1469, 237, 45, 2, 157, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1470, 235, 45, 2, 189, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1471, 228, 45, 2, 178, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1472, 232, 48, 2, 190, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1473, 229, 48, 2, 189, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1474, 231, 48, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1475, 233, 48, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1476, 230, 48, 2, 12, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1477, 236, 48, 2, 123, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1478, 239, 48, 2, 123, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1479, 238, 48, 2, 165, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1480, 234, 48, 2, 166, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1481, 237, 48, 2, 178, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1482, 235, 48, 2, 145, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1483, 228, 48, 2, 134, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1484, 232, 46, 2, 200, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1485, 229, 46, 2, 199, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1486, 231, 46, 2, 189, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1487, 233, 46, 2, 198, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1488, 230, 46, 2, 197, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1489, 236, 46, 2, 196, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1490, 239, 46, 2, 195, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1491, 238, 46, 2, 189, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1492, 234, 46, 2, 165, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1493, 237, 46, 2, 123, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1494, 235, 46, 2, 167, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1495, 228, 46, 2, 123, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1496, 232, 47, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1497, 229, 47, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1498, 231, 47, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1499, 233, 47, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1500, 230, 47, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1501, 236, 47, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1502, 239, 47, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1503, 238, 47, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1504, 234, 47, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1505, 237, 47, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1506, 235, 47, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1507, 228, 47, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1508, 232, 56, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1509, 229, 56, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1510, 231, 56, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1511, 233, 56, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1512, 230, 56, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1513, 236, 56, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1514, 239, 56, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1515, 238, 56, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1516, 234, 56, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1517, 237, 56, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1518, 235, 56, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1519, 228, 56, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1520, 232, 61, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1521, 229, 61, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1522, 231, 61, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1523, 233, 61, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1524, 230, 61, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1525, 236, 61, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1526, 239, 61, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1527, 238, 61, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1528, 234, 61, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1529, 237, 61, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1530, 235, 61, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1531, 228, 61, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1532, 232, 59, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1533, 229, 59, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1534, 231, 59, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1535, 233, 59, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1536, 230, 59, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1537, 236, 59, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1538, 239, 59, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1539, 238, 59, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1540, 234, 59, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1541, 237, 59, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1542, 235, 59, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1543, 228, 59, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1544, 232, 57, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1545, 229, 57, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1546, 231, 57, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1547, 233, 57, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1548, 230, 57, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1549, 236, 57, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1550, 239, 57, 2, 42, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1551, 238, 57, 2, 41, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1552, 234, 57, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1553, 237, 57, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1554, 235, 57, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1555, 228, 57, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1556, 232, 58, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1557, 229, 58, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1558, 231, 58, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1559, 233, 58, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1560, 230, 58, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1561, 236, 58, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1562, 239, 58, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1563, 238, 58, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1564, 234, 58, 2, 42, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1565, 237, 58, 2, 46, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1566, 235, 58, 2, 47, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1567, 228, 58, 2, 48, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1568, 232, 60, 2, 47, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1569, 229, 60, 2, 48, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1570, 231, 60, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1571, 233, 60, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1572, 230, 60, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1573, 236, 60, 2, 7, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1574, 239, 60, 2, 20, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1575, 238, 60, 2, 23, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1576, 234, 60, 2, 23, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1577, 237, 60, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1578, 235, 60, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1579, 228, 60, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1580, 232, 43, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1581, 229, 43, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1582, 231, 43, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1583, 233, 43, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1584, 230, 43, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1585, 236, 43, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1586, 239, 43, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1587, 238, 43, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1588, 234, 43, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1589, 237, 43, 2, 0, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1590, 235, 43, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1591, 228, 43, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1592, 232, 44, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1593, 229, 44, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1594, 231, 44, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1595, 233, 44, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1596, 230, 44, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1597, 236, 44, 2, 46, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1598, 239, 44, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1599, 238, 44, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1600, 234, 44, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1601, 237, 44, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1602, 235, 44, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1603, 228, 44, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1604, 232, 50, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1605, 229, 50, 2, 74, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1606, 231, 50, 2, 74, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1607, 233, 50, 2, 73, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1608, 230, 50, 2, 72, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1609, 236, 50, 2, 71, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1610, 239, 50, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1611, 238, 50, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1612, 234, 50, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1613, 237, 50, 2, 74, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1614, 235, 50, 2, 72, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1615, 228, 50, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1616, 232, 51, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1617, 229, 51, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1618, 231, 51, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1619, 233, 51, 2, 73, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1620, 230, 51, 2, 72, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1621, 236, 51, 2, 71, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1622, 239, 51, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1623, 238, 51, 2, 72, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1624, 234, 51, 2, 70, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1625, 237, 51, 2, 72, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1626, 235, 51, 2, 71, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1627, 228, 51, 2, 73, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1628, 232, 49, 2, 140, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1629, 229, 49, 2, 123, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1630, 231, 49, 2, 123, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1631, 233, 49, 2, 140, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1632, 230, 49, 2, 124, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1633, 236, 49, 2, 150, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1634, 239, 49, 2, 143, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1635, 238, 49, 2, 123, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1636, 234, 49, 2, 124, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1637, 237, 49, 2, 125, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1638, 235, 49, 2, 149, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1639, 228, 49, 2, 150, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1640, 232, 52, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1641, 229, 52, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1642, 231, 52, 2, 38, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1643, 233, 52, 2, 35, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1644, 230, 52, 2, 47, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1645, 236, 52, 2, 48, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1646, 239, 52, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1647, 238, 52, 2, 46, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1648, 234, 52, 2, 35, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1649, 237, 52, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1650, 235, 52, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1651, 228, 52, 2, 20, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1652, 232, 55, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1653, 229, 55, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1654, 231, 55, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1655, 233, 55, 2, 42, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1656, 230, 55, 2, 41, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1657, 236, 55, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1658, 239, 55, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1659, 238, 55, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1660, 234, 55, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1661, 237, 55, 2, 38, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1662, 235, 55, 2, 36, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1663, 228, 55, 2, 38, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1664, 232, 54, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1665, 229, 54, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1666, 231, 54, 2, 46, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1667, 233, 54, 2, 47, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1668, 230, 54, 2, 48, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1669, 236, 54, 2, 49, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1670, 239, 54, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1671, 238, 54, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1672, 234, 54, 2, 42, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1673, 237, 54, 2, 41, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1674, 235, 54, 2, 39, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1675, 228, 54, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1676, 232, 53, 2, 49, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1677, 229, 53, 2, 40, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1678, 231, 53, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1679, 233, 53, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1680, 230, 53, 2, 41, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1681, 236, 53, 2, 42, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1682, 239, 53, 2, 46, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1683, 238, 53, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1684, 234, 53, 2, 42, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1685, 237, 53, 2, 41, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1686, 235, 53, 2, 49, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1687, 228, 53, 2, 50, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1688, 132, 31, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1689, 154, 31, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1690, 140, 31, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1691, 144, 31, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1692, 145, 31, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1693, 148, 31, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1694, 149, 31, 2, 96, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1695, 143, 31, 2, 94, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1696, 153, 31, 2, 93, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1697, 137, 31, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1698, 147, 31, 2, 91, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1699, 129, 31, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1700, 151, 31, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1701, 130, 31, 2, 91, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1702, 138, 31, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1703, 150, 31, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1704, 141, 31, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1705, 133, 31, 2, 79, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1706, 152, 31, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1707, 142, 31, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1708, 146, 31, 2, 75, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1709, 135, 31, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1710, 139, 31, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1711, 134, 31, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1712, 136, 31, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1713, 131, 31, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1714, 132, 36, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1715, 154, 36, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1716, 140, 36, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1717, 144, 36, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1718, 145, 36, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1719, 148, 36, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1720, 149, 36, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1721, 143, 36, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1722, 153, 36, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1723, 137, 36, 2, 93, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1724, 147, 36, 2, 92, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1725, 129, 36, 2, 91, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1726, 151, 36, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1727, 130, 36, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1728, 138, 36, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1729, 150, 36, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1730, 141, 36, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1731, 133, 36, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1732, 152, 36, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1733, 142, 36, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1734, 146, 36, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1735, 135, 36, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1736, 139, 36, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1737, 134, 36, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1738, 136, 36, 2, 69, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1739, 131, 36, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1740, 132, 38, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1741, 154, 38, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1742, 140, 38, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1743, 144, 38, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1744, 145, 38, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1745, 148, 38, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1746, 149, 38, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1747, 143, 38, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1748, 153, 38, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1749, 137, 38, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1750, 147, 38, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1751, 129, 38, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1752, 151, 38, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1753, 130, 38, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1754, 138, 38, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1755, 150, 38, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1756, 141, 38, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1757, 133, 38, 2, 57, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1758, 152, 38, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1759, 142, 38, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1760, 146, 38, 2, 54, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1761, 135, 38, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1762, 139, 38, 2, 23, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1763, 134, 38, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1764, 136, 38, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1765, 131, 38, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1766, 132, 40, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1767, 154, 40, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1768, 140, 40, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1769, 144, 40, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1770, 145, 40, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1771, 148, 40, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1772, 149, 40, 2, 79, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1773, 143, 40, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1774, 153, 40, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1775, 137, 40, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1776, 147, 40, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1777, 129, 40, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1778, 151, 40, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1779, 130, 40, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1780, 138, 40, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1781, 150, 40, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1782, 141, 40, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1783, 133, 40, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1784, 152, 40, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1785, 142, 40, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1786, 146, 40, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1787, 135, 40, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1788, 139, 40, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1789, 134, 40, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1790, 136, 40, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1791, 131, 40, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1792, 132, 33, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1793, 154, 33, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1794, 140, 33, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1795, 144, 33, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1796, 145, 33, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1797, 148, 33, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1798, 149, 33, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1799, 143, 33, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1800, 153, 33, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1801, 137, 33, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1802, 147, 33, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1803, 129, 33, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1804, 151, 33, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1805, 130, 33, 2, 33, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1806, 138, 33, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1807, 150, 33, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1808, 141, 33, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1809, 133, 33, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1810, 152, 33, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1811, 142, 33, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1812, 146, 33, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1813, 135, 33, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1814, 139, 33, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1815, 134, 33, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1816, 136, 33, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1817, 131, 33, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1818, 132, 39, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1819, 154, 39, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1820, 140, 39, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1821, 144, 39, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1822, 145, 39, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1823, 148, 39, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1824, 149, 39, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1825, 143, 39, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1826, 153, 39, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1827, 137, 39, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1828, 147, 39, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1829, 129, 39, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1830, 151, 39, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1831, 130, 39, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1832, 138, 39, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1833, 150, 39, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL);
INSERT INTO `grades` (`id`, `student`, `course`, `evaluation`, `grade_value`, `rewrite_grade_value`, `validate`, `publish`, `comment`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1834, 141, 39, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1835, 133, 39, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1836, 152, 39, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1837, 142, 39, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1838, 146, 39, 2, 32, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1839, 135, 39, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1840, 139, 39, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1841, 134, 39, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1842, 136, 39, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1843, 131, 39, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1844, 132, 34, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1845, 154, 34, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1846, 140, 34, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1847, 144, 34, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1848, 145, 34, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1849, 148, 34, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1850, 149, 34, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1851, 143, 34, 2, 54, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1852, 153, 34, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1853, 137, 34, 2, 58, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1854, 147, 34, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1855, 129, 34, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1856, 151, 34, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1857, 130, 34, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1858, 138, 34, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1859, 150, 34, 2, 12, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1860, 141, 34, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1861, 133, 34, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1862, 152, 34, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1863, 142, 34, 2, 32, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1864, 146, 34, 2, 12, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1865, 135, 34, 2, 23, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1866, 139, 34, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1867, 134, 34, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1868, 136, 34, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1869, 131, 34, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1870, 132, 35, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1871, 154, 35, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1872, 140, 35, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1873, 144, 35, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1874, 145, 35, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1875, 148, 35, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1876, 149, 35, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1877, 143, 35, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1878, 153, 35, 2, 23, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1879, 137, 35, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1880, 147, 35, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1881, 129, 35, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1882, 151, 35, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1883, 130, 35, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1884, 138, 35, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1885, 150, 35, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1886, 141, 35, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1887, 133, 35, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1888, 152, 35, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1889, 142, 35, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1890, 146, 35, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1891, 135, 35, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1892, 139, 35, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1893, 134, 35, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1894, 136, 35, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1895, 131, 35, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1896, 132, 37, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1897, 154, 37, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1898, 140, 37, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1899, 144, 37, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1900, 145, 37, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1901, 148, 37, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1902, 149, 37, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1903, 143, 37, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1904, 153, 37, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1905, 137, 37, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1906, 147, 37, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1907, 129, 37, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1908, 151, 37, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1909, 130, 37, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1910, 138, 37, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1911, 150, 37, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1912, 141, 37, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1913, 133, 37, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1914, 152, 37, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1915, 142, 37, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1916, 146, 37, 2, 54, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1917, 135, 37, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1918, 139, 37, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1919, 134, 37, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1920, 136, 37, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1921, 131, 37, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1922, 132, 32, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1923, 154, 32, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1924, 140, 32, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1925, 144, 32, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1926, 145, 32, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1927, 148, 32, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1928, 149, 32, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1929, 143, 32, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1930, 153, 32, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1931, 137, 32, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1932, 147, 32, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1933, 129, 32, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1934, 151, 32, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1935, 130, 32, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1936, 138, 32, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1937, 150, 32, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1938, 141, 32, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1939, 133, 32, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1940, 152, 32, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1941, 142, 32, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1942, 146, 32, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1943, 135, 32, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1944, 139, 32, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1945, 134, 32, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1946, 136, 32, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1947, 131, 32, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1948, 88, 5, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1949, 90, 5, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1950, 77, 5, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1951, 91, 5, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1952, 82, 5, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1953, 92, 5, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1954, 85, 5, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1955, 87, 5, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1956, 93, 5, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1957, 94, 5, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1958, 78, 5, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1959, 79, 5, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1960, 95, 5, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1961, 81, 5, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1962, 96, 5, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1963, 97, 5, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1964, 98, 5, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1965, 99, 5, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1966, 100, 5, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1967, 101, 5, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1968, 86, 5, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1969, 89, 5, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1970, 83, 5, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1971, 84, 5, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1972, 80, 5, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1973, 102, 5, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1974, 103, 5, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1975, 88, 8, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1976, 90, 8, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1977, 77, 8, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1978, 91, 8, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1979, 82, 8, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1980, 92, 8, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1981, 85, 8, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1982, 87, 8, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1983, 93, 8, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1984, 94, 8, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1985, 78, 8, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1986, 79, 8, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1987, 95, 8, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1988, 81, 8, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1989, 96, 8, 2, 33, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1990, 97, 8, 2, 21, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1991, 98, 8, 2, 12, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1992, 99, 8, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1993, 100, 8, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1994, 101, 8, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1995, 86, 8, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1996, 89, 8, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1997, 83, 8, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1998, 84, 8, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(1999, 80, 8, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2000, 102, 8, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2001, 103, 8, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2002, 88, 2, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2003, 90, 2, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2004, 77, 2, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2005, 91, 2, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2006, 82, 2, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2007, 92, 2, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2008, 85, 2, 2, 0, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2009, 87, 2, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2010, 93, 2, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2011, 94, 2, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2012, 78, 2, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2013, 79, 2, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2014, 95, 2, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2015, 81, 2, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2016, 96, 2, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2017, 97, 2, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2018, 98, 2, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2019, 99, 2, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2020, 100, 2, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2021, 101, 2, 2, 95, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2022, 86, 2, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2023, 89, 2, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2024, 83, 2, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2025, 84, 2, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2026, 80, 2, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2027, 102, 2, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2028, 103, 2, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2029, 88, 1, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2030, 90, 1, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2031, 77, 1, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2032, 91, 1, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2033, 82, 1, 2, 85, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2034, 92, 1, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2035, 85, 1, 2, 86, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2036, 87, 1, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2037, 93, 1, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2038, 94, 1, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2039, 78, 1, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2040, 79, 1, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2041, 95, 1, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2042, 81, 1, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2043, 96, 1, 2, 54, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2044, 97, 1, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2045, 98, 1, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2046, 99, 1, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2047, 100, 1, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2048, 101, 1, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2049, 86, 1, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2050, 89, 1, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2051, 83, 1, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2052, 84, 1, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2053, 80, 1, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2054, 102, 1, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2055, 103, 1, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2056, 88, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2057, 90, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2058, 77, 10, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2059, 91, 10, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2060, 82, 10, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2061, 92, 10, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2062, 85, 10, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2063, 87, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2064, 93, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2065, 94, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2066, 78, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2067, 79, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2068, 95, 10, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2069, 81, 10, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2070, 96, 10, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2071, 97, 10, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2072, 98, 10, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2073, 99, 10, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2074, 100, 10, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2075, 101, 10, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2076, 86, 10, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2077, 89, 10, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2078, 83, 10, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2079, 84, 10, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2080, 80, 10, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2081, 102, 10, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2082, 103, 10, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2083, 88, 9, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2084, 90, 9, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2085, 77, 9, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2086, 91, 9, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2087, 82, 9, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2088, 92, 9, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2089, 85, 9, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2090, 87, 9, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2091, 93, 9, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2092, 94, 9, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2093, 78, 9, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2094, 79, 9, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2095, 95, 9, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2096, 81, 9, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2097, 96, 9, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2098, 97, 9, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2099, 98, 9, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2100, 99, 9, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2101, 100, 9, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2102, 101, 9, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2103, 86, 9, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2104, 89, 9, 2, 80, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2105, 83, 9, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2106, 84, 9, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2107, 80, 9, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2108, 102, 9, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2109, 103, 9, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2110, 88, 3, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2111, 90, 3, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2112, 77, 3, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2113, 91, 3, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2114, 82, 3, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2115, 92, 3, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2116, 85, 3, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2117, 87, 3, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2118, 93, 3, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2119, 94, 3, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2120, 78, 3, 2, 97, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2121, 79, 3, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2122, 95, 3, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2123, 81, 3, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2124, 96, 3, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2125, 97, 3, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2126, 98, 3, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2127, 99, 3, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2128, 100, 3, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2129, 101, 3, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2130, 86, 3, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2131, 89, 3, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2132, 83, 3, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2133, 84, 3, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2134, 80, 3, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2135, 102, 3, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2136, 103, 3, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2137, 88, 6, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2138, 90, 6, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2139, 77, 6, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2140, 91, 6, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2141, 82, 6, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2142, 92, 6, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2143, 85, 6, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2144, 87, 6, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2145, 93, 6, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2146, 94, 6, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2147, 78, 6, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2148, 79, 6, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2149, 95, 6, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2150, 81, 6, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2151, 96, 6, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2152, 97, 6, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2153, 98, 6, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2154, 99, 6, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2155, 100, 6, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2156, 101, 6, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2157, 86, 6, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2158, 89, 6, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2159, 83, 6, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2160, 84, 6, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2161, 80, 6, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2162, 102, 6, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2163, 103, 6, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2164, 88, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2165, 90, 4, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2166, 77, 4, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2167, 91, 4, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2168, 82, 4, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2169, 92, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2170, 85, 4, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2171, 87, 4, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2172, 93, 4, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2173, 94, 4, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2174, 78, 4, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2175, 79, 4, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2176, 95, 4, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2177, 81, 4, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2178, 96, 4, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2179, 97, 4, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2180, 98, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2181, 99, 4, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2182, 100, 4, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2183, 101, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2184, 86, 4, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2185, 89, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2186, 83, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2187, 84, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2188, 80, 4, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2189, 102, 4, 2, 79, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2190, 103, 4, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2191, 88, 7, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2192, 90, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2193, 77, 7, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2194, 91, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2195, 82, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2196, 92, 7, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2197, 85, 7, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2198, 87, 7, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2199, 93, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2200, 94, 7, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2201, 78, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2202, 79, 7, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2203, 95, 7, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2204, 81, 7, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2205, 96, 7, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2206, 97, 7, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2207, 98, 7, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2208, 99, 7, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2209, 100, 7, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2210, 101, 7, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2211, 86, 7, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2212, 89, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2213, 83, 7, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2214, 84, 7, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2215, 80, 7, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2216, 102, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2217, 103, 7, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2218, 106, 20, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2219, 114, 20, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2220, 110, 20, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2221, 118, 20, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2222, 119, 20, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2223, 113, 20, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2224, 107, 20, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2225, 111, 20, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2226, 120, 20, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2227, 121, 20, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2228, 112, 20, 2, 8, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2229, 116, 20, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2230, 122, 20, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2231, 115, 20, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2232, 123, 20, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2233, 124, 20, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2234, 109, 20, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2235, 126, 20, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2236, 117, 20, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2237, 127, 20, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2238, 128, 20, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2239, 106, 14, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2240, 114, 14, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2241, 110, 14, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2242, 118, 14, 2, 76, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2243, 119, 14, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2244, 113, 14, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2245, 107, 14, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2246, 111, 14, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2247, 120, 14, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2248, 121, 14, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2249, 112, 14, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2250, 116, 14, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2251, 122, 14, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2252, 115, 14, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2253, 123, 14, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2254, 124, 14, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2255, 109, 14, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2256, 126, 14, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2257, 117, 14, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2258, 127, 14, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2259, 128, 14, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2260, 106, 12, 2, 100, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2261, 114, 12, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2262, 110, 12, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2263, 118, 12, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2264, 119, 12, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2265, 113, 12, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2266, 107, 12, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2267, 111, 12, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2268, 120, 12, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2269, 121, 12, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2270, 112, 12, 2, 33, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2271, 116, 12, 2, 22, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2272, 122, 12, 2, 11, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2273, 115, 12, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2274, 123, 12, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2275, 124, 12, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2276, 109, 12, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2277, 126, 12, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2278, 117, 12, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2279, 127, 12, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2280, 128, 12, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2281, 106, 16, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2282, 114, 16, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2283, 110, 16, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2284, 118, 16, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2285, 119, 16, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2286, 113, 16, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2287, 107, 16, 2, 87, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2288, 111, 16, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2289, 120, 16, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2290, 121, 16, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2291, 112, 16, 2, 22, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2292, 116, 16, 2, 34, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2293, 122, 16, 2, 11, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2294, 115, 16, 2, 23, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2295, 123, 16, 2, 44, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2296, 124, 16, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2297, 109, 16, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2298, 126, 16, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2299, 117, 16, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2300, 127, 16, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2301, 128, 16, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2302, 106, 15, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2303, 114, 15, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2304, 110, 15, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2305, 118, 15, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2306, 119, 15, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2307, 113, 15, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2308, 107, 15, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2309, 111, 15, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2310, 120, 15, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2311, 121, 15, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2312, 112, 15, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2313, 116, 15, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2314, 122, 15, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2315, 115, 15, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2316, 123, 15, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2317, 124, 15, 2, 45, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2318, 109, 15, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2319, 126, 15, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2320, 117, 15, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2321, 127, 15, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2322, 128, 15, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2323, 106, 19, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2324, 114, 19, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2325, 110, 19, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2326, 118, 19, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2327, 119, 19, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2328, 113, 19, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2329, 107, 19, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2330, 111, 19, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2331, 120, 19, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2332, 121, 19, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2333, 112, 19, 2, 8, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2334, 116, 19, 2, 7, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2335, 122, 19, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2336, 115, 19, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2337, 123, 19, 2, 5, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2338, 124, 19, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2339, 109, 19, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2340, 126, 19, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2341, 117, 19, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2342, 127, 19, 2, 77, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2343, 128, 19, 2, 66, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2344, 106, 13, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2345, 114, 13, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2346, 110, 13, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2347, 118, 13, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2348, 119, 13, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2349, 113, 13, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2350, 107, 13, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2351, 111, 13, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2352, 120, 13, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2353, 121, 13, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2354, 112, 13, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2355, 116, 13, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2356, 122, 13, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2357, 115, 13, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2358, 123, 13, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2359, 124, 13, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2360, 109, 13, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2361, 126, 13, 2, 56, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2362, 117, 13, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2363, 127, 13, 2, 65, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2364, 128, 13, 2, 43, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2365, 106, 17, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2366, 114, 17, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2367, 110, 17, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2368, 118, 17, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2369, 119, 17, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2370, 113, 17, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2371, 107, 17, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2372, 111, 17, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2373, 120, 17, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2374, 121, 17, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2375, 112, 17, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2376, 116, 17, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2377, 122, 17, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2378, 115, 17, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2379, 123, 17, 2, 99, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2380, 124, 17, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2381, 109, 17, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2382, 126, 17, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2383, 117, 17, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2384, 127, 17, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2385, 128, 17, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2386, 106, 18, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2387, 114, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2388, 110, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2389, 118, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2390, 119, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2391, 113, 18, 2, 79, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2392, 107, 18, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2393, 111, 18, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2394, 120, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2395, 121, 18, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2396, 112, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2397, 116, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2398, 122, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2399, 115, 18, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2400, 123, 18, 2, 9, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2401, 124, 18, 2, 88, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2402, 109, 18, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2403, 126, 18, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2404, 117, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2405, 127, 18, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2406, 128, 18, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2407, 106, 11, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2408, 114, 11, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2409, 110, 11, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2410, 118, 11, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2411, 119, 11, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2412, 113, 11, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2413, 107, 11, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2414, 111, 11, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2415, 120, 11, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2416, 121, 11, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2417, 112, 11, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2418, 116, 11, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2419, 122, 11, 2, 55, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2420, 115, 11, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2421, 123, 11, 2, 89, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2422, 124, 11, 2, 78, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2423, 109, 11, 2, 68, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2424, 126, 11, 2, 67, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2425, 117, 11, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2426, 127, 11, 2, 90, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2427, 128, 11, 2, 98, NULL, 1, 1, '', '2019-08-10 00:00:00', NULL, 'admin', NULL),
(2428, 191, 84, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2429, 201, 84, 5, 52, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2430, 180, 84, 5, 56, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2431, 181, 84, 5, 89, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2432, 182, 84, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2433, 183, 84, 5, 70, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2434, 187, 84, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2435, 189, 84, 5, 62, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2436, 193, 84, 5, 68, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2437, 194, 84, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2438, 196, 84, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2439, 200, 84, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2440, 203, 84, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2441, 191, 86, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2442, 201, 86, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2443, 180, 86, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2444, 181, 86, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2445, 182, 86, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2446, 183, 86, 5, 74, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL);
INSERT INTO `grades` (`id`, `student`, `course`, `evaluation`, `grade_value`, `rewrite_grade_value`, `validate`, `publish`, `comment`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(2447, 187, 86, 5, 71, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2448, 189, 86, 5, 72, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2449, 193, 86, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2450, 194, 86, 5, 84, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2451, 196, 86, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2452, 200, 86, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2453, 203, 86, 5, 81, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2454, 191, 90, 5, 89, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2455, 201, 90, 5, 56, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2456, 180, 90, 5, 52, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2457, 181, 90, 5, 57, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2458, 182, 90, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2459, 183, 90, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2460, 187, 90, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2461, 189, 90, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2462, 193, 90, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2463, 194, 90, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2464, 196, 90, 5, 78, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2465, 200, 90, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2466, 203, 90, 5, 74, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2467, 191, 83, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2468, 201, 83, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2469, 180, 83, 5, 84, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2470, 181, 83, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2471, 182, 83, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2472, 183, 83, 5, 74, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2473, 187, 83, 5, 68, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2474, 189, 83, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2475, 193, 83, 5, 56, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2476, 194, 83, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2477, 196, 83, 5, 84, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2478, 200, 83, 5, 82, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2479, 203, 83, 5, 81, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2480, 191, 82, 5, 78, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2481, 201, 82, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2482, 181, 82, 5, 68, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2483, 182, 82, 5, 15, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2484, 180, 82, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2485, 183, 82, 5, 68, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2486, 187, 82, 5, 78, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2487, 189, 82, 5, 79, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2488, 193, 82, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2489, 194, 82, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2490, 196, 82, 5, 68, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2491, 200, 82, 5, 74, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2492, 203, 82, 5, 72, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2493, 191, 88, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2494, 201, 88, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2495, 182, 88, 5, 25, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2496, 180, 88, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2497, 181, 88, 5, 14, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2498, 183, 88, 5, 78, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2499, 187, 88, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2500, 189, 88, 5, 52, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2501, 193, 88, 5, 57, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2502, 194, 88, 5, 68, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2503, 196, 88, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2504, 200, 88, 5, 67, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2505, 203, 88, 5, 72, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2506, 191, 85, 5, 89, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2507, 201, 85, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2508, 182, 85, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2509, 180, 85, 5, 20, NULL, 1, 1, '', '2019-08-17 00:00:00', '2019-09-13 16:22:44', 'admin', 'admin'),
(2510, 181, 85, 5, 74, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2511, 183, 85, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2512, 187, 85, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2513, 189, 85, 5, 25, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2514, 193, 85, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2515, 194, 85, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2516, 196, 85, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2517, 200, 85, 5, 78, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2518, 203, 85, 5, 74, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2519, 191, 91, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2520, 201, 91, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2521, 182, 91, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2522, 180, 91, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2523, 181, 91, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2524, 183, 91, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2525, 187, 91, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2526, 189, 91, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2527, 193, 91, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2528, 194, 91, 5, 58, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2529, 196, 91, 5, 57, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2530, 200, 91, 5, 59, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2531, 203, 91, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2532, 191, 102, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2533, 201, 102, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2534, 180, 102, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2535, 181, 102, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2536, 182, 102, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2537, 183, 102, 5, 85, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2538, 187, 102, 5, 98, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2539, 189, 102, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2540, 193, 102, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2541, 194, 102, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2542, 196, 102, 5, 78, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2543, 200, 102, 5, 59, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2544, 203, 102, 5, 95, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2545, 191, 87, 5, 86, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2546, 201, 87, 5, 59, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2547, 180, 87, 5, 68, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2548, 181, 87, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2549, 182, 87, 5, 78, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2550, 183, 87, 5, 98, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2551, 187, 87, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2552, 189, 87, 5, 74, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2553, 193, 87, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2554, 194, 87, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2555, 196, 87, 5, 75, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2556, 200, 87, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2557, 203, 87, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2558, 191, 89, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2559, 201, 89, 5, 50, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2560, 180, 89, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2561, 181, 89, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2562, 182, 89, 5, 25, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2563, 183, 89, 5, 12, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2564, 187, 89, 5, 52, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2565, 189, 89, 5, 65, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2566, 193, 89, 5, 15, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2567, 194, 89, 5, 45, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2568, 196, 89, 5, 25, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2569, 200, 89, 5, 35, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2570, 203, 89, 5, 15, NULL, 1, 1, '', '2019-08-17 00:00:00', NULL, 'admin', NULL),
(2571, 247, 123, 5, 90, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2572, 251, 123, 5, 78, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2573, 252, 123, 5, 76, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2574, 246, 123, 5, 32, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2575, 244, 123, 5, 65, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2576, 245, 123, 5, 12, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2577, 243, 123, 5, 10, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2578, 248, 123, 5, 78, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2579, 250, 123, 5, 45, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2580, 241, 123, 5, 92, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 15:53:09', 'admin', 'admin'),
(2581, 242, 123, 5, 10, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2582, 249, 123, 5, 12, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2583, 247, 126, 5, 70, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2584, 251, 126, 5, 65, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2585, 252, 126, 5, 60, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2586, 246, 126, 5, 85, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2587, 244, 126, 5, 55, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2588, 245, 126, 5, 65, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2589, 243, 126, 5, 45, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2590, 248, 126, 5, 75, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2591, 250, 126, 5, 55, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2592, 241, 126, 5, 35, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2593, 242, 126, 5, 25, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2594, 249, 126, 5, 65, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2595, 247, 127, 5, 80, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2596, 251, 127, 5, 65, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2597, 252, 127, 5, 23, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2598, 246, 127, 5, 87, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2599, 244, 127, 5, 39, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2600, 245, 127, 5, 77, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2601, 243, 127, 5, 69, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2602, 248, 127, 5, 90, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2603, 250, 127, 5, 87, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2604, 241, 127, 5, 66, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2605, 242, 127, 5, 89, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2606, 249, 127, 5, 70, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2607, 247, 129, 5, 110, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2608, 251, 129, 5, 150, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2609, 252, 129, 5, 95, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2610, 246, 129, 5, 150, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2611, 244, 129, 5, 100, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2612, 245, 129, 5, 153, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2613, 243, 129, 5, 145, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2614, 248, 129, 5, 156, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2615, 250, 129, 5, 85, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2616, 241, 129, 5, 35, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2617, 242, 129, 5, 170, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2618, 249, 129, 5, 125, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2619, 247, 125, 5, 96, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2620, 251, 125, 5, 88.5, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2621, 252, 125, 5, 41, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2622, 246, 125, 5, 56, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-09-01 14:37:39', 'admin', 'admin'),
(2623, 244, 125, 5, 78, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2624, 245, 125, 5, 90, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2625, 243, 125, 5, 56, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2626, 248, 125, 5, 77, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2627, 250, 125, 5, 89, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2628, 241, 125, 5, 99.5, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2629, 242, 125, 5, 67, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2630, 249, 125, 5, 90, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2631, 247, 128, 5, 55, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2632, 251, 128, 5, 25, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2633, 252, 128, 5, 75, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2634, 246, 128, 5, 32, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2635, 244, 128, 5, 80, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2636, 243, 128, 5, 60, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2637, 248, 128, 5, 90, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2638, 245, 128, 5, 75, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2639, 250, 128, 5, 60, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2640, 241, 128, 5, 50, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2641, 242, 128, 5, 65, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2642, 249, 128, 5, 45, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2643, 260, 126, 5, 89, NULL, 1, 1, '', '2019-08-31 00:00:00', NULL, 'admin', NULL),
(2644, 260, 125, 5, 80, NULL, 1, 1, '', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 'admin', 'admin'),
(2645, 266, 130, 5, 120, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2646, 267, 130, 5, 150, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2647, 269, 130, 5, 90, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2648, 270, 130, 5, 110, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2649, 268, 130, 5, 45, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2650, 266, 131, 5, 110, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2651, 267, 131, 5, 130, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2652, 269, 131, 5, 59, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2653, 270, 131, 5, 180, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2654, 268, 131, 5, 120, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2655, 266, 132, 5, 90, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2656, 267, 132, 5, 150, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2657, 269, 132, 5, 105, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2658, 270, 132, 5, 137, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2659, 268, 132, 5, 118, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2660, 266, 133, 5, 100, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2661, 267, 133, 5, 25, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2662, 269, 133, 5, 50, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2663, 270, 133, 5, 20, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2664, 268, 133, 5, 100, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2665, 266, 134, 5, 200, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2666, 267, 134, 5, 100, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2667, 269, 134, 5, 110, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2668, 270, 134, 5, 52, NULL, 1, 1, '', '2019-09-05 00:00:00', '2019-09-05 15:46:03', 'admin', 'admin'),
(2669, 268, 134, 5, 120, NULL, 1, 1, '', '2019-09-05 00:00:00', NULL, 'admin', NULL),
(2670, 247, 123, 6, 90, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2671, 251, 123, 6, 100, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2672, 252, 123, 6, 87, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2673, 263, 123, 6, 100, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2674, 246, 123, 6, 90, NULL, 1, 0, '', '2019-09-06 00:00:00', '2019-09-06 14:27:30', 'neard255', 'admin'),
(2675, 244, 123, 6, 99, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2676, 243, 123, 6, 12, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2677, 260, 123, 6, 30, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2678, 248, 123, 6, 50, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2679, 250, 123, 6, 78, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2680, 241, 123, 6, 90, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2681, 265, 123, 6, 100, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2682, 242, 123, 6, 50, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL),
(2683, 249, 123, 6, 25, NULL, 0, 0, '', '2019-09-06 00:00:00', NULL, 'neard255', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(32) NOT NULL,
  `belongs_to_profil` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `belongs_to_profil`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES
(1, 'Developer', 1, '2015-05-04 00:00:00', '2019-09-05 00:00:00', '_developer_', 'LOGIPAM'),
(2, 'Default Group', 5, NULL, '2015-05-07 00:00:00', NULL, '_developer_'),
(3, 'Student', 5, NULL, '2015-08-31 00:00:00', NULL, '_developer_'),
(4, 'Parent', 5, NULL, '2015-08-30 00:00:00', NULL, '_developer_'),
(5, 'Direction', 1, NULL, '2019-09-05 00:00:00', NULL, 'LOGIPAM'),
(6, 'Administration', 2, NULL, '2019-09-05 00:00:00', NULL, 'LOGIPAM'),
(7, 'Economat', 3, NULL, '2016-07-07 00:00:00', NULL, '_developer_'),
(8, 'Teacher', 4, NULL, '2015-09-03 00:00:00', NULL, '_developer_'),
(9, 'Reporter', 6, NULL, '2016-07-04 00:00:00', NULL, '_developer_'),
(10, 'Discipline', 2, '2015-09-05 00:00:00', '2015-09-06 00:00:00', '_developer_', '_developer_'),
(11, 'Pedagogie', 2, '2015-09-06 00:00:00', '2019-09-05 00:00:00', '_developer_', 'LOGIPAM'),
(12, 'Publication', 7, '2016-04-14 00:00:00', '2016-04-16 00:00:00', 'master_user', 'master_user'),
(13, 'Economat ADM', 3, '2016-07-09 00:00:00', '2019-08-21 00:00:00', '_developer_', 'LOGIPAM'),
(14, 'Administrateur systeme', 1, '2016-12-10 00:00:00', '2019-09-05 00:00:00', '_developer_', 'LOGIPAM'),
(15, 'Kinder', 2, '2019-08-11 00:00:00', NULL, 'LOGIPAM', NULL),
(16, 'Prefet-1', 2, '2019-08-21 00:00:00', '2019-08-21 00:00:00', 'LOGIPAM', 'LOGIPAM'),
(17, 'Prefet-2', 2, '2019-08-21 00:00:00', NULL, 'LOGIPAM', NULL),
(18, 'Surveillant', 2, '2019-08-21 00:00:00', NULL, 'LOGIPAM', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups_has_actions`
--

CREATE TABLE `groups_has_actions` (
  `groups_id` int(11) NOT NULL,
  `actions_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups_has_actions`
--

INSERT INTO `groups_has_actions` (`groups_id`, `actions_id`) VALUES
(1, 1),
(5, 1),
(14, 1),
(1, 2),
(5, 2),
(14, 2),
(1, 3),
(5, 3),
(7, 3),
(10, 3),
(13, 3),
(14, 3),
(15, 3),
(1, 4),
(5, 4),
(14, 4),
(1, 5),
(5, 5),
(8, 5),
(14, 5),
(1, 6),
(3, 6),
(4, 6),
(5, 6),
(6, 6),
(7, 6),
(8, 6),
(9, 6),
(10, 6),
(11, 6),
(12, 6),
(13, 6),
(14, 6),
(15, 6),
(16, 6),
(17, 6),
(18, 6),
(1, 7),
(5, 7),
(7, 7),
(10, 7),
(12, 7),
(13, 7),
(14, 7),
(15, 7),
(1, 8),
(5, 8),
(14, 8),
(1, 9),
(5, 9),
(6, 9),
(7, 9),
(8, 9),
(10, 9),
(11, 9),
(12, 9),
(13, 9),
(14, 9),
(15, 9),
(1, 10),
(5, 10),
(6, 10),
(7, 10),
(8, 10),
(10, 10),
(11, 10),
(12, 10),
(13, 10),
(14, 10),
(15, 10),
(1, 11),
(5, 11),
(6, 11),
(7, 11),
(8, 11),
(10, 11),
(11, 11),
(12, 11),
(13, 11),
(14, 11),
(15, 11),
(1, 12),
(5, 12),
(6, 12),
(7, 12),
(10, 12),
(11, 12),
(13, 12),
(14, 12),
(15, 12),
(1, 13),
(5, 13),
(6, 13),
(10, 13),
(14, 13),
(15, 13),
(1, 14),
(5, 14),
(6, 14),
(7, 14),
(10, 14),
(11, 14),
(13, 14),
(14, 14),
(15, 14),
(1, 15),
(5, 15),
(6, 15),
(7, 15),
(10, 15),
(11, 15),
(12, 15),
(13, 15),
(14, 15),
(15, 15),
(1, 16),
(5, 16),
(6, 16),
(7, 16),
(10, 16),
(11, 16),
(12, 16),
(13, 16),
(14, 16),
(15, 16),
(1, 17),
(5, 17),
(6, 17),
(7, 17),
(10, 17),
(11, 17),
(12, 17),
(13, 17),
(14, 17),
(15, 17),
(1, 18),
(5, 18),
(6, 18),
(7, 18),
(10, 18),
(11, 18),
(12, 18),
(13, 18),
(14, 18),
(15, 18),
(1, 19),
(5, 19),
(6, 19),
(7, 19),
(10, 19),
(11, 19),
(13, 19),
(14, 19),
(15, 19),
(1, 20),
(5, 20),
(6, 20),
(7, 20),
(10, 20),
(11, 20),
(12, 20),
(13, 20),
(14, 20),
(15, 20),
(1, 21),
(5, 21),
(6, 21),
(7, 21),
(10, 21),
(11, 21),
(13, 21),
(14, 21),
(15, 21),
(1, 22),
(5, 22),
(6, 22),
(7, 22),
(10, 22),
(11, 22),
(13, 22),
(14, 22),
(15, 22),
(1, 23),
(5, 23),
(6, 23),
(7, 23),
(10, 23),
(11, 23),
(12, 23),
(13, 23),
(14, 23),
(15, 23),
(1, 24),
(5, 24),
(6, 24),
(7, 24),
(11, 24),
(13, 24),
(14, 24),
(1, 25),
(5, 25),
(6, 25),
(7, 25),
(10, 25),
(11, 25),
(12, 25),
(13, 25),
(14, 25),
(1, 26),
(5, 26),
(6, 26),
(7, 26),
(10, 26),
(11, 26),
(13, 26),
(14, 26),
(15, 26),
(1, 27),
(5, 27),
(6, 27),
(7, 27),
(8, 27),
(10, 27),
(11, 27),
(13, 27),
(14, 27),
(15, 27),
(1, 28),
(5, 28),
(6, 28),
(7, 28),
(8, 28),
(10, 28),
(11, 28),
(12, 28),
(13, 28),
(14, 28),
(15, 28),
(1, 29),
(5, 29),
(6, 29),
(7, 29),
(11, 29),
(13, 29),
(14, 29),
(1, 30),
(5, 30),
(6, 30),
(7, 30),
(10, 30),
(11, 30),
(12, 30),
(13, 30),
(14, 30),
(15, 30),
(1, 31),
(5, 31),
(6, 31),
(7, 31),
(10, 31),
(11, 31),
(13, 31),
(14, 31),
(15, 31),
(1, 32),
(5, 32),
(6, 32),
(7, 32),
(10, 32),
(11, 32),
(13, 32),
(14, 32),
(15, 32),
(1, 33),
(5, 33),
(6, 33),
(7, 33),
(10, 33),
(11, 33),
(12, 33),
(13, 33),
(14, 33),
(15, 33),
(1, 34),
(5, 34),
(6, 34),
(7, 34),
(11, 34),
(13, 34),
(14, 34),
(1, 35),
(5, 35),
(6, 35),
(7, 35),
(10, 35),
(11, 35),
(12, 35),
(13, 35),
(14, 35),
(15, 35),
(1, 36),
(5, 36),
(6, 36),
(7, 36),
(11, 36),
(13, 36),
(14, 36),
(1, 37),
(5, 37),
(6, 37),
(7, 37),
(10, 37),
(11, 37),
(13, 37),
(14, 37),
(15, 37),
(1, 38),
(5, 38),
(6, 38),
(7, 38),
(10, 38),
(11, 38),
(12, 38),
(13, 38),
(14, 38),
(15, 38),
(1, 39),
(5, 39),
(6, 39),
(7, 39),
(11, 39),
(13, 39),
(14, 39),
(1, 40),
(5, 40),
(6, 40),
(7, 40),
(8, 40),
(10, 40),
(11, 40),
(12, 40),
(13, 40),
(14, 40),
(15, 40),
(16, 40),
(1, 41),
(5, 41),
(6, 41),
(8, 41),
(11, 41),
(12, 41),
(14, 41),
(16, 41),
(1, 42),
(5, 42),
(6, 42),
(8, 42),
(11, 42),
(12, 42),
(14, 42),
(16, 42),
(1, 43),
(5, 43),
(6, 43),
(7, 43),
(8, 43),
(10, 43),
(11, 43),
(12, 43),
(13, 43),
(14, 43),
(15, 43),
(16, 43),
(1, 44),
(5, 44),
(6, 44),
(8, 44),
(11, 44),
(12, 44),
(14, 44),
(1, 45),
(5, 45),
(6, 45),
(7, 45),
(8, 45),
(10, 45),
(11, 45),
(12, 45),
(13, 45),
(14, 45),
(15, 45),
(16, 45),
(1, 46),
(5, 46),
(6, 46),
(7, 46),
(13, 46),
(14, 46),
(1, 47),
(5, 47),
(6, 47),
(7, 47),
(13, 47),
(14, 47),
(1, 48),
(5, 48),
(6, 48),
(7, 48),
(13, 48),
(14, 48),
(1, 49),
(5, 49),
(6, 49),
(7, 49),
(13, 49),
(14, 49),
(1, 50),
(5, 50),
(6, 50),
(7, 50),
(13, 50),
(14, 50),
(1, 51),
(5, 51),
(6, 51),
(7, 51),
(13, 51),
(14, 51),
(1, 52),
(5, 52),
(6, 52),
(7, 52),
(13, 52),
(14, 52),
(1, 53),
(5, 53),
(6, 53),
(7, 53),
(13, 53),
(14, 53),
(1, 54),
(5, 54),
(7, 54),
(13, 54),
(14, 54),
(1, 55),
(5, 55),
(6, 55),
(7, 55),
(8, 55),
(10, 55),
(11, 55),
(12, 55),
(13, 55),
(14, 55),
(15, 55),
(1, 56),
(5, 56),
(6, 56),
(7, 56),
(10, 56),
(11, 56),
(13, 56),
(14, 56),
(15, 56),
(1, 57),
(5, 57),
(6, 57),
(7, 57),
(10, 57),
(11, 57),
(13, 57),
(14, 57),
(15, 57),
(1, 58),
(5, 58),
(6, 58),
(7, 58),
(10, 58),
(11, 58),
(13, 58),
(14, 58),
(15, 58),
(1, 59),
(5, 59),
(6, 59),
(7, 59),
(10, 59),
(11, 59),
(13, 59),
(14, 59),
(15, 59),
(1, 60),
(5, 60),
(6, 60),
(7, 60),
(10, 60),
(11, 60),
(12, 60),
(13, 60),
(14, 60),
(15, 60),
(16, 60),
(18, 60),
(1, 61),
(5, 61),
(6, 61),
(11, 61),
(14, 61),
(1, 62),
(5, 62),
(6, 62),
(11, 62),
(14, 62),
(1, 63),
(5, 63),
(6, 63),
(7, 63),
(10, 63),
(11, 63),
(12, 63),
(13, 63),
(14, 63),
(15, 63),
(16, 63),
(1, 64),
(5, 64),
(6, 64),
(7, 64),
(8, 64),
(10, 64),
(11, 64),
(12, 64),
(13, 64),
(14, 64),
(15, 64),
(16, 64),
(18, 64),
(1, 65),
(5, 65),
(6, 65),
(11, 65),
(14, 65),
(1, 66),
(5, 66),
(6, 66),
(7, 66),
(10, 66),
(11, 66),
(12, 66),
(13, 66),
(14, 66),
(15, 66),
(1, 67),
(5, 67),
(6, 67),
(11, 67),
(14, 67),
(1, 68),
(5, 68),
(6, 68),
(11, 68),
(14, 68),
(1, 69),
(5, 69),
(6, 69),
(7, 69),
(10, 69),
(11, 69),
(12, 69),
(13, 69),
(14, 69),
(15, 69),
(1, 70),
(5, 70),
(6, 70),
(11, 70),
(14, 70),
(1, 71),
(5, 71),
(6, 71),
(7, 71),
(8, 71),
(10, 71),
(11, 71),
(12, 71),
(13, 71),
(14, 71),
(15, 71),
(1, 72),
(5, 72),
(6, 72),
(11, 72),
(14, 72),
(1, 73),
(5, 73),
(6, 73),
(11, 73),
(14, 73),
(1, 74),
(5, 74),
(6, 74),
(7, 74),
(10, 74),
(11, 74),
(12, 74),
(13, 74),
(14, 74),
(15, 74),
(18, 74),
(1, 75),
(5, 75),
(6, 75),
(11, 75),
(14, 75),
(1, 76),
(5, 76),
(6, 76),
(7, 76),
(10, 76),
(11, 76),
(12, 76),
(13, 76),
(14, 76),
(15, 76),
(16, 76),
(18, 76),
(1, 77),
(5, 77),
(6, 77),
(10, 77),
(11, 77),
(14, 77),
(15, 77),
(1, 78),
(5, 78),
(6, 78),
(10, 78),
(11, 78),
(14, 78),
(15, 78),
(1, 79),
(5, 79),
(6, 79),
(7, 79),
(10, 79),
(11, 79),
(12, 79),
(13, 79),
(14, 79),
(15, 79),
(16, 79),
(18, 79),
(1, 80),
(5, 80),
(6, 80),
(7, 80),
(8, 80),
(10, 80),
(11, 80),
(12, 80),
(13, 80),
(14, 80),
(15, 80),
(18, 80),
(1, 81),
(5, 81),
(6, 81),
(10, 81),
(11, 81),
(12, 81),
(14, 81),
(15, 81),
(1, 82),
(5, 82),
(6, 82),
(10, 82),
(11, 82),
(14, 82),
(15, 82),
(1, 83),
(5, 83),
(6, 83),
(11, 83),
(14, 83),
(1, 84),
(5, 84),
(6, 84),
(11, 84),
(14, 84),
(1, 85),
(5, 85),
(6, 85),
(11, 85),
(14, 85),
(1, 86),
(5, 86),
(6, 86),
(11, 86),
(14, 86),
(1, 87),
(5, 87),
(6, 87),
(11, 87),
(14, 87),
(1, 88),
(5, 88),
(6, 88),
(11, 88),
(14, 88),
(1, 89),
(5, 89),
(6, 89),
(11, 89),
(14, 89),
(1, 90),
(5, 90),
(6, 90),
(11, 90),
(14, 90),
(1, 91),
(5, 91),
(6, 91),
(11, 91),
(14, 91),
(1, 92),
(5, 92),
(6, 92),
(11, 92),
(14, 92),
(1, 93),
(5, 93),
(6, 93),
(14, 93),
(1, 94),
(5, 94),
(6, 94),
(14, 94),
(1, 95),
(5, 95),
(6, 95),
(14, 95),
(1, 96),
(5, 96),
(6, 96),
(14, 96),
(1, 97),
(5, 97),
(6, 97),
(14, 97),
(1, 98),
(5, 98),
(6, 98),
(11, 98),
(14, 98),
(1, 99),
(5, 99),
(6, 99),
(11, 99),
(14, 99),
(1, 100),
(5, 100),
(6, 100),
(11, 100),
(14, 100),
(1, 101),
(5, 101),
(6, 101),
(11, 101),
(14, 101),
(1, 102),
(5, 102),
(6, 102),
(11, 102),
(14, 102),
(1, 103),
(5, 103),
(6, 103),
(13, 103),
(14, 103),
(1, 104),
(5, 104),
(6, 104),
(13, 104),
(14, 104),
(1, 105),
(5, 105),
(6, 105),
(13, 105),
(14, 105),
(1, 106),
(5, 106),
(6, 106),
(13, 106),
(14, 106),
(1, 107),
(5, 107),
(6, 107),
(13, 107),
(14, 107),
(1, 108),
(5, 108),
(6, 108),
(11, 108),
(14, 108),
(1, 109),
(5, 109),
(6, 109),
(11, 109),
(14, 109),
(1, 110),
(5, 110),
(6, 110),
(11, 110),
(14, 110),
(1, 111),
(5, 111),
(6, 111),
(11, 111),
(14, 111),
(1, 112),
(5, 112),
(6, 112),
(11, 112),
(14, 112),
(1, 113),
(5, 113),
(6, 113),
(14, 113),
(1, 114),
(5, 114),
(6, 114),
(14, 114),
(1, 115),
(5, 115),
(6, 115),
(14, 115),
(1, 116),
(5, 116),
(6, 116),
(14, 116),
(1, 117),
(5, 117),
(6, 117),
(14, 117),
(1, 118),
(5, 118),
(6, 118),
(14, 118),
(1, 119),
(5, 119),
(6, 119),
(14, 119),
(1, 120),
(5, 120),
(6, 120),
(14, 120),
(1, 121),
(5, 121),
(6, 121),
(14, 121),
(1, 122),
(5, 122),
(6, 122),
(14, 122),
(1, 123),
(5, 123),
(6, 123),
(14, 123),
(1, 124),
(5, 124),
(6, 124),
(11, 124),
(14, 124),
(1, 125),
(5, 125),
(6, 125),
(11, 125),
(14, 125),
(1, 126),
(5, 126),
(6, 126),
(11, 126),
(14, 126),
(1, 127),
(5, 127),
(6, 127),
(11, 127),
(14, 127),
(1, 128),
(5, 128),
(6, 128),
(11, 128),
(14, 128),
(1, 129),
(5, 129),
(6, 129),
(11, 129),
(14, 129),
(1, 130),
(5, 130),
(6, 130),
(11, 130),
(14, 130),
(1, 131),
(5, 131),
(6, 131),
(11, 131),
(14, 131),
(1, 132),
(5, 132),
(6, 132),
(11, 132),
(14, 132),
(1, 133),
(5, 133),
(6, 133),
(11, 133),
(14, 133),
(1, 134),
(5, 134),
(6, 134),
(13, 134),
(14, 134),
(1, 135),
(5, 135),
(6, 135),
(13, 135),
(14, 135),
(1, 136),
(5, 136),
(6, 136),
(13, 136),
(14, 136),
(1, 137),
(5, 137),
(6, 137),
(13, 137),
(14, 137),
(1, 138),
(5, 138),
(6, 138),
(13, 138),
(14, 138),
(1, 139),
(5, 139),
(6, 139),
(11, 139),
(14, 139),
(1, 140),
(5, 140),
(6, 140),
(11, 140),
(14, 140),
(1, 141),
(5, 141),
(6, 141),
(11, 141),
(14, 141),
(1, 142),
(5, 142),
(6, 142),
(11, 142),
(14, 142),
(1, 143),
(5, 143),
(6, 143),
(11, 143),
(14, 143),
(1, 144),
(5, 144),
(6, 144),
(11, 144),
(14, 144),
(1, 145),
(5, 145),
(6, 145),
(11, 145),
(14, 145),
(1, 146),
(5, 146),
(6, 146),
(11, 146),
(14, 146),
(1, 147),
(5, 147),
(6, 147),
(11, 147),
(14, 147),
(1, 148),
(5, 148),
(6, 148),
(11, 148),
(14, 148),
(1, 149),
(5, 149),
(6, 149),
(11, 149),
(14, 149),
(1, 150),
(5, 150),
(6, 150),
(11, 150),
(14, 150),
(1, 151),
(5, 151),
(6, 151),
(11, 151),
(14, 151),
(1, 152),
(5, 152),
(6, 152),
(11, 152),
(14, 152),
(1, 153),
(5, 153),
(6, 153),
(11, 153),
(14, 153),
(1, 154),
(5, 154),
(11, 154),
(14, 154),
(1, 155),
(5, 155),
(6, 155),
(11, 155),
(14, 155),
(1, 156),
(5, 156),
(6, 156),
(11, 156),
(14, 156),
(1, 157),
(5, 157),
(6, 157),
(11, 157),
(14, 157),
(1, 158),
(5, 158),
(6, 158),
(11, 158),
(14, 158),
(1, 159),
(5, 159),
(6, 159),
(11, 159),
(14, 159),
(1, 160),
(5, 160),
(6, 160),
(11, 160),
(14, 160),
(1, 161),
(5, 161),
(6, 161),
(11, 161),
(14, 161),
(1, 162),
(5, 162),
(6, 162),
(11, 162),
(14, 162),
(1, 163),
(5, 163),
(6, 163),
(11, 163),
(14, 163),
(1, 164),
(5, 164),
(6, 164),
(11, 164),
(14, 164),
(1, 165),
(5, 165),
(6, 165),
(11, 165),
(14, 165),
(1, 166),
(5, 166),
(6, 166),
(11, 166),
(14, 166),
(1, 167),
(5, 167),
(6, 167),
(11, 167),
(14, 167),
(1, 168),
(5, 168),
(6, 168),
(11, 168),
(14, 168),
(1, 169),
(5, 169),
(14, 169),
(1, 170),
(5, 170),
(14, 170),
(1, 171),
(5, 171),
(14, 171),
(1, 172),
(5, 172),
(14, 172),
(1, 173),
(5, 173),
(14, 173),
(1, 174),
(5, 174),
(14, 174),
(1, 175),
(5, 175),
(14, 175),
(1, 176),
(5, 176),
(14, 176),
(1, 177),
(5, 177),
(14, 177),
(1, 178),
(5, 178),
(14, 178),
(1, 179),
(5, 179),
(14, 179),
(1, 180),
(5, 180),
(14, 180),
(1, 181),
(5, 181),
(14, 181),
(1, 182),
(5, 182),
(14, 182),
(1, 183),
(5, 183),
(14, 183),
(1, 184),
(5, 184),
(6, 184),
(11, 184),
(14, 184),
(1, 185),
(3, 185),
(4, 185),
(5, 185),
(6, 185),
(14, 185),
(1, 186),
(3, 186),
(4, 186),
(5, 186),
(6, 186),
(14, 186),
(1, 187),
(3, 187),
(4, 187),
(5, 187),
(6, 187),
(14, 187),
(1, 188),
(4, 188),
(5, 188),
(6, 188),
(14, 188),
(1, 189),
(4, 189),
(5, 189),
(6, 189),
(14, 189),
(1, 190),
(3, 190),
(4, 190),
(5, 190),
(6, 190),
(14, 190),
(1, 191),
(3, 191),
(4, 191),
(5, 191),
(6, 191),
(14, 191),
(1, 192),
(3, 192),
(4, 192),
(5, 192),
(6, 192),
(14, 192),
(1, 193),
(3, 193),
(4, 193),
(5, 193),
(6, 193),
(14, 193),
(1, 194),
(3, 194),
(4, 194),
(5, 194),
(6, 194),
(14, 194),
(1, 195),
(3, 195),
(4, 195),
(5, 195),
(6, 195),
(14, 195),
(1, 196),
(3, 196),
(4, 196),
(5, 196),
(6, 196),
(14, 196),
(1, 197),
(3, 197),
(4, 197),
(5, 197),
(6, 197),
(14, 197),
(1, 198),
(3, 198),
(4, 198),
(5, 198),
(6, 198),
(14, 198),
(1, 200),
(3, 200),
(4, 200),
(5, 200),
(6, 200),
(14, 200),
(1, 201),
(5, 201),
(6, 201),
(7, 201),
(8, 201),
(10, 201),
(11, 201),
(12, 201),
(13, 201),
(14, 201),
(15, 201),
(1, 202),
(5, 202),
(6, 202),
(7, 202),
(8, 202),
(10, 202),
(11, 202),
(12, 202),
(13, 202),
(14, 202),
(15, 202),
(1, 203),
(5, 203),
(6, 203),
(7, 203),
(8, 203),
(10, 203),
(11, 203),
(12, 203),
(13, 203),
(14, 203),
(15, 203),
(1, 204),
(5, 204),
(6, 204),
(7, 204),
(8, 204),
(10, 204),
(11, 204),
(12, 204),
(13, 204),
(14, 204),
(15, 204),
(1, 205),
(4, 205),
(5, 205),
(6, 205),
(14, 205),
(1, 206),
(3, 206),
(5, 206),
(6, 206),
(14, 206),
(1, 207),
(3, 207),
(5, 207),
(6, 207),
(14, 207),
(1, 208),
(5, 208),
(6, 208),
(7, 208),
(10, 208),
(11, 208),
(12, 208),
(13, 208),
(14, 208),
(15, 208),
(1, 209),
(5, 209),
(6, 209),
(7, 209),
(10, 209),
(11, 209),
(12, 209),
(13, 209),
(14, 209),
(15, 209),
(1, 210),
(5, 210),
(6, 210),
(7, 210),
(8, 210),
(10, 210),
(11, 210),
(12, 210),
(13, 210),
(14, 210),
(15, 210),
(16, 210),
(18, 210),
(1, 211),
(5, 211),
(6, 211),
(7, 211),
(10, 211),
(11, 211),
(12, 211),
(13, 211),
(14, 211),
(15, 211),
(1, 212),
(5, 212),
(6, 212),
(7, 212),
(10, 212),
(11, 212),
(12, 212),
(13, 212),
(14, 212),
(15, 212),
(1, 213),
(5, 213),
(6, 213),
(7, 213),
(10, 213),
(11, 213),
(12, 213),
(13, 213),
(14, 213),
(15, 213),
(1, 214),
(5, 214),
(6, 214),
(7, 214),
(8, 214),
(10, 214),
(11, 214),
(12, 214),
(13, 214),
(14, 214),
(15, 214),
(1, 215),
(5, 215),
(6, 215),
(7, 215),
(8, 215),
(10, 215),
(11, 215),
(12, 215),
(13, 215),
(14, 215),
(15, 215),
(16, 215),
(18, 215),
(1, 216),
(5, 216),
(6, 216),
(7, 216),
(8, 216),
(10, 216),
(11, 216),
(12, 216),
(13, 216),
(14, 216),
(15, 216),
(1, 217),
(2, 217),
(3, 217),
(4, 217),
(5, 217),
(6, 217),
(14, 217),
(1, 218),
(2, 218),
(3, 218),
(4, 218),
(5, 218),
(6, 218),
(14, 218),
(1, 219),
(5, 219),
(14, 219),
(1, 220),
(5, 220),
(6, 220),
(7, 220),
(10, 220),
(11, 220),
(13, 220),
(14, 220),
(15, 220),
(1, 221),
(5, 221),
(6, 221),
(7, 221),
(10, 221),
(11, 221),
(13, 221),
(14, 221),
(15, 221),
(1, 222),
(5, 222),
(7, 222),
(13, 222),
(1, 223),
(5, 223),
(7, 223),
(13, 223),
(1, 224),
(5, 224),
(7, 224),
(13, 224),
(1, 225),
(5, 225),
(6, 225),
(7, 225),
(13, 225),
(1, 226),
(5, 226),
(6, 226),
(7, 226),
(10, 226),
(11, 226),
(13, 226),
(14, 226),
(15, 226),
(1, 227),
(5, 227),
(6, 227),
(7, 227),
(10, 227),
(11, 227),
(13, 227),
(14, 227),
(15, 227),
(1, 228),
(5, 228),
(6, 228),
(7, 228),
(10, 228),
(11, 228),
(13, 228),
(14, 228),
(15, 228),
(1, 229),
(5, 229),
(6, 229),
(10, 229),
(14, 229),
(15, 229),
(1, 230),
(5, 230),
(6, 230),
(7, 230),
(10, 230),
(11, 230),
(13, 230),
(14, 230),
(15, 230),
(1, 231),
(5, 231),
(6, 231),
(7, 231),
(8, 231),
(10, 231),
(11, 231),
(12, 231),
(13, 231),
(14, 231),
(15, 231),
(1, 232),
(5, 232),
(6, 232),
(8, 232),
(11, 232),
(12, 232),
(14, 232),
(1, 233),
(5, 233),
(6, 233),
(8, 233),
(12, 233),
(14, 233),
(1, 234),
(5, 234),
(6, 234),
(8, 234),
(11, 234),
(12, 234),
(14, 234),
(1, 235),
(3, 235),
(4, 235),
(5, 235),
(6, 235),
(14, 235),
(1, 236),
(3, 236),
(4, 236),
(5, 236),
(6, 236),
(14, 236),
(1, 237),
(3, 237),
(4, 237),
(5, 237),
(6, 237),
(14, 237),
(1, 238),
(3, 238),
(5, 238),
(6, 238),
(14, 238),
(1, 239),
(3, 239),
(4, 239),
(5, 239),
(6, 239),
(14, 239),
(1, 240),
(5, 240),
(6, 240),
(10, 240),
(11, 240),
(14, 240),
(15, 240),
(16, 240),
(17, 240),
(18, 240),
(1, 241),
(5, 241),
(6, 241),
(10, 241),
(11, 241),
(14, 241),
(15, 241),
(16, 241),
(17, 241),
(1, 242),
(5, 242),
(6, 242),
(10, 242),
(11, 242),
(14, 242),
(15, 242),
(16, 242),
(17, 242),
(18, 242),
(1, 243),
(5, 243),
(6, 243),
(10, 243),
(11, 243),
(14, 243),
(15, 243),
(16, 243),
(17, 243),
(1, 244),
(5, 244),
(6, 244),
(10, 244),
(11, 244),
(14, 244),
(15, 244),
(16, 244),
(17, 244),
(1, 245),
(5, 245),
(6, 245),
(10, 245),
(11, 245),
(14, 245),
(15, 245),
(16, 245),
(17, 245),
(18, 245),
(1, 246),
(5, 246),
(6, 246),
(10, 246),
(11, 246),
(14, 246),
(15, 246),
(16, 246),
(17, 246),
(1, 247),
(5, 247),
(6, 247),
(10, 247),
(11, 247),
(14, 247),
(15, 247),
(16, 247),
(17, 247),
(1, 248),
(5, 248),
(6, 248),
(10, 248),
(11, 248),
(14, 248),
(15, 248),
(16, 248),
(17, 248),
(1, 249),
(5, 249),
(6, 249),
(10, 249),
(11, 249),
(14, 249),
(15, 249),
(16, 249),
(17, 249),
(18, 249),
(1, 250),
(5, 250),
(6, 250),
(10, 250),
(11, 250),
(14, 250),
(15, 250),
(16, 250),
(17, 250),
(18, 250),
(1, 251),
(5, 251),
(6, 251),
(10, 251),
(11, 251),
(14, 251),
(15, 251),
(16, 251),
(17, 251),
(18, 251),
(1, 252),
(5, 252),
(6, 252),
(10, 252),
(11, 252),
(14, 252),
(15, 252),
(16, 252),
(17, 252),
(18, 252),
(1, 253),
(5, 253),
(6, 253),
(10, 253),
(11, 253),
(14, 253),
(15, 253),
(16, 253),
(17, 253),
(18, 253),
(1, 254),
(5, 254),
(6, 254),
(10, 254),
(11, 254),
(14, 254),
(15, 254),
(16, 254),
(17, 254),
(18, 254),
(1, 255),
(5, 255),
(6, 255),
(10, 255),
(11, 255),
(14, 255),
(15, 255),
(16, 255),
(17, 255),
(18, 255),
(1, 256),
(5, 256),
(6, 256),
(10, 256),
(11, 256),
(14, 256),
(15, 256),
(16, 256),
(17, 256),
(18, 256),
(1, 257),
(5, 257),
(7, 257),
(13, 257),
(1, 258),
(5, 258),
(6, 258),
(14, 258),
(1, 259),
(5, 259),
(7, 259),
(13, 259),
(1, 260),
(5, 260),
(7, 260),
(13, 260),
(1, 261),
(5, 261),
(6, 261),
(8, 261),
(10, 261),
(11, 261),
(12, 261),
(14, 261),
(15, 261),
(1, 262),
(5, 262),
(7, 262),
(12, 262),
(13, 262),
(1, 263),
(5, 263),
(7, 263),
(13, 263),
(1, 264),
(5, 264),
(7, 264),
(13, 264),
(1, 265),
(5, 265),
(7, 265),
(13, 265),
(1, 266),
(5, 266),
(6, 266),
(7, 266),
(8, 266),
(10, 266),
(11, 266),
(12, 266),
(13, 266),
(15, 266),
(1, 267),
(5, 267),
(7, 267),
(13, 267),
(14, 267),
(1, 268),
(5, 268),
(7, 268),
(13, 268),
(1, 269),
(5, 269),
(7, 269),
(13, 269),
(1, 270),
(5, 270),
(7, 270),
(13, 270),
(1, 271),
(5, 271),
(7, 271),
(12, 271),
(13, 271),
(1, 272),
(5, 272),
(7, 272),
(13, 272),
(14, 272),
(1, 273),
(5, 273),
(7, 273),
(13, 273),
(14, 273),
(1, 274),
(5, 274),
(7, 274),
(13, 274),
(14, 274),
(1, 275),
(5, 275),
(7, 275),
(13, 275),
(14, 275),
(1, 276),
(5, 276),
(6, 276),
(7, 276),
(13, 276),
(14, 276),
(1, 277),
(5, 277),
(6, 277),
(7, 277),
(13, 277),
(14, 277),
(1, 278),
(5, 278),
(6, 278),
(7, 278),
(13, 278),
(14, 278),
(1, 279),
(5, 279),
(7, 279),
(13, 279),
(14, 279),
(1, 280),
(5, 280),
(6, 280),
(7, 280),
(13, 280),
(14, 280),
(1, 281),
(5, 281),
(6, 281),
(7, 281),
(8, 281),
(10, 281),
(11, 281),
(12, 281),
(13, 281),
(15, 281),
(1, 282),
(5, 282),
(6, 282),
(13, 282),
(14, 282),
(1, 283),
(3, 283),
(4, 283),
(5, 283),
(14, 283),
(1, 284),
(5, 284),
(6, 284),
(11, 284),
(12, 284),
(14, 284),
(1, 285),
(5, 285),
(6, 285),
(11, 285),
(12, 285),
(14, 285),
(1, 286),
(5, 286),
(6, 286),
(11, 286),
(12, 286),
(14, 286),
(1, 287),
(5, 287),
(6, 287),
(12, 287),
(14, 287),
(1, 288),
(3, 288),
(5, 288),
(14, 288),
(1, 289),
(4, 289),
(5, 289),
(14, 289),
(1, 290),
(5, 290),
(7, 290),
(13, 290),
(1, 291),
(5, 291),
(6, 291),
(7, 291),
(10, 291),
(12, 291),
(13, 291),
(14, 291),
(15, 291),
(1, 292),
(5, 292),
(6, 292),
(14, 292),
(1, 293),
(5, 293),
(6, 293),
(14, 293),
(1, 294),
(5, 294),
(6, 294),
(14, 294),
(1, 295),
(5, 295),
(6, 295),
(14, 295),
(1, 296),
(5, 296),
(6, 296),
(14, 296),
(1, 297),
(5, 297),
(7, 297),
(13, 297),
(14, 297),
(1, 298),
(5, 298),
(6, 298),
(7, 298),
(13, 298),
(14, 298),
(1, 299),
(5, 299),
(6, 299),
(7, 299),
(13, 299),
(14, 299),
(1, 300),
(5, 300),
(7, 300),
(13, 300),
(14, 300),
(1, 301),
(5, 301),
(6, 301),
(7, 301),
(13, 301),
(14, 301),
(1, 302),
(5, 302),
(7, 302),
(13, 302),
(14, 302),
(1, 303),
(5, 303),
(7, 303),
(13, 303),
(14, 303),
(1, 304),
(5, 304),
(7, 304),
(13, 304),
(14, 304),
(1, 305),
(5, 305),
(7, 305),
(13, 305),
(14, 305),
(1, 306),
(5, 306),
(7, 306),
(13, 306),
(14, 306),
(1, 307),
(5, 307),
(7, 307),
(13, 307),
(14, 307),
(1, 308),
(5, 308),
(6, 308),
(7, 308),
(13, 308),
(14, 308),
(1, 309),
(5, 309),
(7, 309),
(13, 309),
(14, 309),
(1, 310),
(5, 310),
(7, 310),
(13, 310),
(14, 310),
(1, 311),
(5, 311),
(7, 311),
(13, 311),
(14, 311),
(1, 312),
(5, 312),
(7, 312),
(13, 312),
(1, 313),
(5, 313),
(7, 313),
(12, 313),
(13, 313),
(1, 314),
(5, 314),
(6, 314),
(7, 314),
(12, 314),
(13, 314),
(1, 315),
(5, 315),
(7, 315),
(13, 315),
(14, 315),
(1, 316),
(5, 316),
(7, 316),
(13, 316),
(14, 316),
(1, 317),
(5, 317),
(7, 317),
(13, 317),
(14, 317),
(1, 318),
(5, 318),
(7, 318),
(13, 318),
(14, 318),
(1, 319),
(5, 319),
(6, 319),
(14, 319),
(1, 320),
(5, 320),
(7, 320),
(13, 320),
(1, 321),
(5, 321),
(7, 321),
(13, 321),
(1, 322),
(5, 322),
(6, 322),
(7, 322),
(13, 322),
(14, 322),
(1, 323),
(5, 323),
(6, 323),
(7, 323),
(13, 323),
(14, 323),
(1, 324),
(5, 324),
(6, 324),
(7, 324),
(13, 324),
(14, 324),
(1, 325),
(5, 325),
(6, 325),
(7, 325),
(13, 325),
(14, 325),
(1, 326),
(5, 326),
(6, 326),
(7, 326),
(13, 326),
(14, 326),
(1, 327),
(5, 327),
(6, 327),
(7, 327),
(13, 327),
(14, 327),
(1, 328),
(5, 328),
(6, 328),
(7, 328),
(13, 328),
(14, 328),
(1, 329),
(5, 329),
(6, 329),
(7, 329),
(13, 329),
(14, 329),
(1, 330),
(5, 330),
(6, 330),
(7, 330),
(13, 330),
(14, 330),
(1, 331),
(5, 331),
(6, 331),
(7, 331),
(13, 331),
(14, 331),
(1, 332),
(5, 332),
(6, 332),
(7, 332),
(13, 332),
(14, 332),
(1, 333),
(5, 333),
(6, 333),
(11, 333),
(12, 333),
(14, 333),
(1, 334),
(5, 334),
(6, 334),
(11, 334),
(14, 334),
(16, 334),
(1, 335),
(5, 335),
(6, 335),
(8, 335),
(11, 335),
(14, 335),
(16, 335),
(18, 335),
(1, 336),
(5, 336),
(6, 336),
(8, 336),
(11, 336),
(14, 336),
(18, 336),
(1, 337),
(5, 337),
(6, 337),
(8, 337),
(11, 337),
(14, 337),
(1, 338),
(5, 338),
(6, 338),
(11, 338),
(14, 338),
(16, 338),
(1, 339),
(5, 339),
(6, 339),
(11, 339),
(14, 339),
(1, 340),
(5, 340),
(6, 340),
(7, 340),
(10, 340),
(11, 340),
(13, 340),
(14, 340),
(15, 340),
(1, 341),
(3, 341),
(4, 341),
(5, 341),
(14, 341),
(1, 342),
(3, 342),
(4, 342),
(5, 342),
(14, 342),
(1, 343),
(3, 343),
(4, 343),
(5, 343),
(14, 343),
(1, 344),
(5, 344),
(6, 344),
(12, 344),
(14, 344),
(1, 345),
(5, 345),
(6, 345),
(12, 345),
(14, 345),
(1, 346),
(5, 346),
(6, 346),
(12, 346),
(14, 346),
(1, 347),
(5, 347),
(6, 347),
(12, 347),
(14, 347),
(1, 348),
(5, 348),
(6, 348),
(12, 348),
(14, 348),
(1, 349),
(5, 349),
(6, 349),
(12, 349),
(14, 349),
(1, 350),
(5, 350),
(6, 350),
(12, 350),
(14, 350),
(1, 351),
(5, 351),
(6, 351),
(11, 351),
(14, 351),
(1, 352),
(5, 352),
(6, 352),
(14, 352),
(1, 353),
(5, 353),
(6, 353),
(11, 353),
(14, 353),
(1, 354),
(5, 354),
(6, 354),
(12, 354),
(14, 354),
(1, 355),
(5, 355),
(6, 355),
(11, 355),
(13, 355),
(14, 355),
(1, 356),
(5, 356),
(6, 356),
(10, 356),
(11, 356),
(14, 356),
(15, 356),
(16, 356),
(17, 356),
(1, 357),
(5, 357),
(6, 357),
(14, 357),
(1, 358),
(5, 358),
(6, 358),
(14, 358),
(1, 359),
(5, 359),
(6, 359),
(14, 359),
(1, 360),
(5, 360),
(7, 360),
(13, 360),
(1, 361),
(5, 361),
(6, 361),
(7, 361),
(13, 361),
(14, 361),
(1, 362),
(5, 362),
(7, 362),
(13, 362),
(14, 362),
(1, 363),
(5, 363),
(14, 363),
(1, 364),
(5, 364),
(1, 365),
(5, 365),
(1, 366),
(5, 366),
(1, 367),
(5, 367),
(6, 367),
(14, 367),
(1, 368),
(5, 368),
(14, 368),
(1, 369),
(5, 369),
(14, 369),
(1, 370),
(5, 370),
(14, 370),
(1, 371),
(5, 371),
(6, 371),
(11, 371),
(14, 371),
(1, 372),
(5, 372),
(6, 372),
(7, 372),
(11, 372),
(13, 372),
(14, 372),
(1, 373),
(5, 373),
(6, 373),
(11, 373),
(14, 373),
(1, 374),
(5, 374),
(6, 374),
(11, 374),
(14, 374),
(1, 375),
(5, 375),
(6, 375),
(11, 375),
(14, 375),
(1, 376),
(5, 376),
(6, 376),
(11, 376),
(14, 376),
(1, 377),
(5, 377),
(6, 377),
(7, 377),
(11, 377),
(13, 377),
(14, 377),
(1, 378),
(3, 378),
(4, 378),
(5, 378),
(1, 379),
(5, 379),
(6, 379),
(10, 379),
(11, 379),
(13, 379),
(15, 379),
(1, 380),
(5, 380),
(6, 380),
(13, 380),
(1, 381),
(5, 381),
(6, 381),
(10, 381),
(11, 381),
(13, 381),
(15, 381),
(1, 382),
(5, 382),
(6, 382),
(10, 382),
(11, 382),
(13, 382),
(15, 382),
(1, 383),
(5, 383),
(6, 383),
(10, 383),
(11, 383),
(13, 383),
(15, 383),
(1, 384),
(5, 384),
(6, 384),
(7, 384),
(13, 384),
(1, 385),
(5, 385),
(6, 385),
(7, 385),
(13, 385),
(1, 386),
(5, 386),
(6, 386),
(7, 386),
(13, 386),
(1, 387),
(5, 387),
(6, 387),
(7, 387),
(13, 387),
(1, 388),
(5, 388),
(13, 388),
(1, 389),
(5, 389),
(13, 389),
(1, 390),
(5, 390),
(6, 390),
(11, 390),
(14, 390),
(1, 391),
(5, 391),
(6, 391),
(11, 391),
(14, 391),
(1, 392),
(5, 392),
(6, 392),
(11, 392),
(14, 392),
(1, 393),
(5, 393),
(6, 393),
(11, 393),
(14, 393),
(1, 394),
(5, 394),
(6, 394),
(11, 394),
(14, 394),
(1, 395),
(5, 395),
(6, 395),
(11, 395),
(14, 395),
(1, 396),
(5, 396),
(6, 396),
(11, 396),
(14, 396),
(1, 397),
(5, 397),
(6, 397),
(11, 397),
(14, 397),
(1, 398),
(5, 398),
(14, 398),
(1, 399),
(5, 399),
(6, 399),
(10, 399),
(11, 399),
(14, 399),
(15, 399),
(1, 400),
(5, 400),
(6, 400),
(7, 400),
(13, 400),
(14, 400),
(1, 401),
(5, 401),
(6, 401),
(7, 401),
(13, 401),
(14, 401),
(1, 402),
(5, 402),
(6, 402),
(7, 402),
(13, 402),
(14, 402),
(1, 403),
(5, 403),
(6, 403),
(7, 403),
(13, 403),
(14, 403),
(1, 404),
(5, 404),
(6, 404),
(7, 404),
(13, 404),
(14, 404),
(1, 405),
(5, 405),
(1, 406),
(5, 406),
(6, 406),
(7, 406),
(11, 406),
(13, 406),
(14, 406),
(1, 407),
(5, 407),
(6, 407),
(7, 407),
(11, 407),
(13, 407),
(16, 407),
(1, 408),
(5, 408),
(6, 408),
(14, 408),
(1, 409),
(5, 409),
(6, 409),
(14, 409),
(1, 410),
(5, 410),
(6, 410),
(14, 410),
(1, 411),
(5, 411),
(1, 412),
(5, 412),
(6, 412),
(11, 412),
(14, 412),
(1, 413),
(5, 413),
(6, 413),
(14, 413),
(1, 414),
(5, 414),
(6, 414),
(11, 414),
(14, 414),
(1, 415),
(5, 415),
(13, 415),
(14, 415),
(1, 416),
(5, 416),
(13, 416),
(14, 416),
(1, 417),
(5, 417),
(13, 417),
(14, 417),
(1, 418),
(5, 418),
(13, 418),
(14, 418),
(1, 419),
(5, 419),
(6, 419),
(15, 419),
(1, 420),
(5, 420),
(15, 420),
(1, 421),
(5, 421),
(15, 421),
(1, 422),
(5, 422),
(15, 422),
(5, 423),
(15, 423),
(1, 424),
(5, 424),
(6, 424),
(11, 424),
(14, 424),
(1, 425),
(5, 425),
(6, 425),
(11, 425),
(14, 425),
(1, 426),
(5, 426),
(6, 426),
(11, 426),
(14, 426),
(1, 427),
(5, 427),
(6, 427),
(11, 427),
(14, 427),
(1, 428),
(5, 428),
(6, 428),
(11, 428),
(14, 428),
(1, 429),
(5, 429),
(6, 429),
(8, 429),
(11, 429),
(14, 429),
(1, 430),
(5, 430),
(6, 430),
(8, 430),
(11, 430),
(14, 430),
(1, 431),
(5, 431),
(6, 431),
(8, 431),
(11, 431),
(14, 431),
(1, 432),
(5, 432),
(6, 432),
(8, 432),
(11, 432),
(14, 432);

-- --------------------------------------------------------

--
-- Table structure for table `groups_has_modules`
--

CREATE TABLE `groups_has_modules` (
  `groups_id` int(11) NOT NULL,
  `modules_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups_has_modules`
--

INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES
(1, 1),
(5, 1),
(6, 1),
(7, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(15, 1),
(16, 1),
(17, 1),
(18, 1),
(1, 5),
(2, 5),
(3, 5),
(4, 5),
(5, 5),
(7, 5),
(8, 5),
(9, 5),
(10, 5),
(12, 5),
(13, 5),
(14, 5),
(1, 6),
(5, 6),
(7, 6),
(8, 6),
(9, 6),
(10, 6),
(12, 6),
(13, 6),
(14, 6),
(1, 7),
(5, 7),
(7, 7),
(8, 7),
(9, 7),
(10, 7),
(12, 7),
(13, 7),
(14, 7),
(1, 8),
(5, 8),
(7, 8),
(8, 8),
(9, 8),
(10, 8),
(12, 8),
(13, 8),
(14, 8),
(1, 9),
(5, 9),
(7, 9),
(8, 9),
(9, 9),
(10, 9),
(12, 9),
(13, 9),
(14, 9),
(1, 10),
(2, 10),
(3, 10),
(4, 10),
(5, 10),
(10, 10),
(14, 10),
(1, 11),
(5, 11),
(7, 11),
(9, 11),
(10, 11),
(12, 11),
(13, 11),
(14, 11),
(1, 12),
(5, 12),
(6, 12),
(7, 12),
(9, 12),
(10, 12),
(11, 12),
(12, 12),
(13, 12),
(14, 12),
(15, 12),
(16, 12),
(17, 12),
(18, 12),
(1, 13),
(5, 13),
(13, 13),
(14, 13),
(1, 14),
(3, 14),
(4, 14),
(5, 14),
(7, 14),
(8, 14),
(13, 14),
(14, 14);

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `categorie` varchar(64) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `help`
--

INSERT INTO `help` (`id`, `title`, `url`, `description`, `categorie`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Hirondelle', 'https://www.youtube.com/watch?v=k69iAwGJf-Y', 'Chants d\'Oiseaux - Chant de l\'hirondelle rustique', 'general', NULL, NULL, NULL, NULL),
(2, 'Colibri', 'https://www.youtube.com/watch?v=YplZ0BYj-oA', 'Le colibri à gorge rubis', 'general', NULL, NULL, NULL, NULL),
(3, 'Colibri 2', 'https://www.youtube.com/watch?v=LVRpya85OgM', 'Ces colibris sont les animaux les plus rapides sur Terre', 'general', NULL, NULL, NULL, NULL),
(4, 'Hirondelle bicolore', 'https://www.youtube.com/watch?v=YfNSxZIR4s4', 'Fou des oiseaux Les hirondelles', 'general', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `homework`
--

CREATE TABLE `homework` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `limit_date_submission` date NOT NULL,
  `given_date` date NOT NULL,
  `attachment_ref` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `homework`
--

INSERT INTO `homework` (`id`, `person_id`, `course`, `title`, `description`, `limit_date_submission`, `given_date`, `attachment_ref`, `academic_year`) VALUES
(1, 221, 45, 'Programme hebdo', 'En plus de SIGES qui est déjà adopté par plusieurs écoles, Logipam a déjà implémenté .', '2019-08-31', '2019-08-06', 'dewey 99 pages_9218.pdf', 1),
(2, 256, 125, 'Past tense', 'La partie 2 du plan sera soumis au moment opportun. Il concernera les importations des lecteurs et les travaux nécessaires à entreprendre. ', '2019-09-07', '2019-08-31', 'Affecter une persone à un groupe_6388.pdf', 6),
(3, 256, 125, 'Past tense', 'La partie 2 du plan sera soumis au moment opportun. Il concernera les importations des lecteurs et les travaux nécessaires à entreprendre. ', '2019-09-07', '2019-08-31', 'Affecter une persone à un groupe_5103.pdf', 6),
(4, 271, 130, 'La vitese est elle lie a l\'acceleration', 'La vitese est elle lie a l\'acceleration\r\nLa vitese est elle lie a l\'acceleration\r\nLa vitese est elle lie a l\'acceleration\r\n', '2019-09-13', '2019-09-06', 'Plan de formation Aout2019_2578.pdf', 6);

-- --------------------------------------------------------

--
-- Table structure for table `homework_submission`
--

CREATE TABLE `homework_submission` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `homework_id` int(11) NOT NULL,
  `date_submission` date NOT NULL,
  `comment` varchar(255) NOT NULL,
  `attachment_ref` varchar(100) NOT NULL,
  `grade_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `homework_submission`
--

INSERT INTO `homework_submission` (`id`, `student`, `homework_id`, `date_submission`, `comment`, `attachment_ref`, `grade_value`) VALUES
(1, 230, 1, '2019-08-06', 'Devoir non terminé, mais remis quand meme.', 'Neuvieme_Periode_I_2018-2019_4078.pdf', 0),
(2, 266, 4, '2019-09-06', 'Je vous soumets ce devoir nais', 'proposition_oposition_plurielle_7887.pdf', 0);

-- --------------------------------------------------------

--
-- Table structure for table `idcard`
--

CREATE TABLE `idcard` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `prenom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `nom` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `sexe` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `image_name` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `date_ajout` datetime NOT NULL,
  `is_print` tinyint(1) DEFAULT NULL,
  `date_print` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `idcard`
--

INSERT INTO `idcard` (`id`, `person_id`, `prenom`, `nom`, `sexe`, `image_name`, `date_ajout`, `is_print`, `date_print`) VALUES
(1, 164, 'Amerlin', 'Delinois', '0', '164.jpg', '2019-08-08 07:08:28', 1, '2019-09-12 00:00:00'),
(2, 102, 'Jamine', 'Tavernier', '1', '102.jpeg', '2019-08-23 12:08:17', 1, '2019-08-23 00:00:00'),
(3, 208, 'Alain', 'Possible', '0', '208.jpg', '2019-08-25 12:08:59', 1, '2019-09-13 00:00:00'),
(4, 248, 'Jameson', 'Louis', '0', '248.jpg', '2019-08-31 07:08:40', 1, '2019-09-01 00:00:00'),
(5, 262, 'Pascal', 'ADONIS', '0', '262.jpg', '2019-09-01 01:09:41', 1, '2019-09-10 00:00:00'),
(6, 243, 'Annedjee Mitchellina', 'LOUIS', '1', '243.jpg', '2019-09-01 01:09:02', 1, '2019-09-01 00:00:00'),
(8, 245, 'Hadassa Klaoudjinn', 'LOUIS', '1', '245.jpg', '2019-09-01 01:09:01', 1, '2019-09-01 00:00:00'),
(13, 250, 'Emile', 'Pierre', '0', '250.jpg', '2019-09-01 02:09:19', 1, '2019-09-01 00:00:00'),
(15, 241, 'Gabriel', 'Poulard', '0', '241.jpg', '2019-09-01 02:09:19', 1, '2019-09-11 00:00:00'),
(16, 263, 'Jacques', 'gabriel', '0', '263.jpg', '2019-09-01 02:09:19', 1, '2019-09-01 00:00:00'),
(17, 244, 'Vanessa', 'Jean', '1', '244.jpg', '2019-09-01 02:09:46', 1, '2019-09-01 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `infraction_type`
--

CREATE TABLE `infraction_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `deductible_value` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `infraction_type`
--

INSERT INTO `infraction_type` (`id`, `name`, `description`, `deductible_value`) VALUES
(1, 'Tapage en classe', '', 20),
(2, 'Bagarre', 'Quand deux élèves se battent dans la salle de classe ou sur la cour de l\'école', 30),
(3, 'Sommeil en classe', '', 5),
(4, 'Absences répétées', '', 10),
(5, 'Leçons non sues', '', 10),
(6, 'Bavardage', '', 5),
(7, 'Tricherie', '', 100),
(8, 'Vandalisme', '', 30),
(9, 'Vol', '', 100),
(10, 'Port d\'arme', '', 50),
(12, 'Refus d\'obéir, effronterie', '', 15),
(13, 'Usage de mots grivois', '', 50),
(14, 'Manque de respect envers un membre du personnel', '', 20),
(15, 'Falsification d\'informations ', '', 20),
(16, 'Faire du bruit excessif a l’école', '', 10),
(17, 'Gaspillage de ressources', '', 10),
(18, 'Devoirs non faits', '', 15),
(25, 'Salir la cour de l\'école', '', 10),
(26, 'Salir la salle de classe', '', 10),
(27, 'Boire ou manger en salle de classe', '', 5),
(28, 'Utilisation du téléphone portable sans autorisation', '', 15),
(32, 'Cris ou autres bruits incongrus dans l\'enceinte de l\'école', '', 5),
(36, 'Absence non motivée', '', 5),
(37, 'Communication excessive en salle d\'examens', 'Communiquer avec un autre camarade pendant l\'examen sans interruption', 20),
(38, 'Retard non motivé', 'Retard sans explication', 10),
(39, 'Consommation, distribution ou vente de produits illicites', 'Fabrication, consommation, distribution ou vente de drogues de toutes sortes ou de boissons alcoolisées', 20),
(40, 'Comportement violent, agressif', 'Violence verbale ou physique', 25),
(41, 'Retards répétés', '', 10),
(42, 'Ecole buissonnière', 'l\'élève ne se présente pas à l\'école', 15),
(43, 'Déplacement sans laissez-passer', 'Circulation non autorisée sur les galeries et sur la cour de l\'école', 5),
(44, 'Manque de respect envers un camarade', '', 10),
(45, 'Regard autain', '', 10),
(46, 'Injures', '', 20);

-- --------------------------------------------------------

--
-- Table structure for table `job_status`
--

CREATE TABLE `job_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_status`
--

INSERT INTO `job_status` (`id`, `status_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Contractuel', '2015-08-20 00:00:00', NULL, NULL, NULL),
(2, 'Temps plein', '2019-08-07 00:00:00', NULL, NULL, NULL),
(3, 'Consultant', '2019-08-07 00:00:00', NULL, NULL, NULL),
(4, 'Temps partiel', '2019-08-07 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kinder_cat_concept`
--

CREATE TABLE `kinder_cat_concept` (
  `id` int(11) NOT NULL,
  `cat_name` varchar(128) NOT NULL,
  `cat_special` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kinder_cat_concept`
--

INSERT INTO `kinder_cat_concept` (`id`, `cat_name`, `cat_special`, `create_by`, `update_by`, `create_date`, `update_date`) VALUES
(1, 'Compétences Spécifiques', 0, 'admin', NULL, '2019-07-23 21:40:15', NULL),
(2, 'Communication orale', 0, 'admin', NULL, '2019-07-23 21:40:29', NULL),
(3, 'Observations', 1, 'admin', NULL, '2019-07-23 21:42:41', NULL),
(4, 'Préparation à l\'école fondamentale', 1, 'admin', NULL, '2019-07-23 21:43:08', NULL),
(5, 'Perception sensorielle', 0, 'admin', 'admin', '2019-07-23 21:43:46', '2019-07-23 21:44:28'),
(6, 'Travaux manuels', 0, 'admin', 'admin', '2019-07-23 21:44:04', '2019-07-23 21:44:15'),
(7, 'Psychomotricité', 0, 'admin', NULL, '2019-07-23 21:44:58', NULL),
(8, 'Orientation spacio-temporelle', 0, 'admin', NULL, '2019-07-23 21:45:22', NULL),
(9, 'Raisonnement logique', 1, 'admin', NULL, '2019-07-23 21:45:37', NULL),
(10, 'Pré-requis', 0, 'admin', NULL, '2019-08-06 19:05:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kinder_concept`
--

CREATE TABLE `kinder_concept` (
  `id` int(11) NOT NULL,
  `concept_name` text NOT NULL,
  `category` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `teacher` int(11) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kinder_concept`
--

INSERT INTO `kinder_concept` (`id`, `concept_name`, `category`, `level`, `academic_year`, `teacher`, `create_by`, `update_by`, `create_date`, `update_date`) VALUES
(1, 'Apprend à distinguer le gauche de la droite', 8, 7, 1, 48, 'admin', NULL, '2019-07-23 21:51:47', NULL),
(2, 'Apprend à situer des objets ou des personnes les uns par rapport aux autres', 8, 7, 1, 48, 'admin', NULL, '2019-07-23 21:52:17', NULL),
(3, 'Perçoit la succession des moments et des évènements les uns par rapport aux autres', 8, 7, 1, 48, 'admin', NULL, '2019-07-23 21:52:30', NULL),
(4, 'Art plastique', 6, 7, 1, 48, 'admin', 'admin', '2019-07-23 21:53:39', '2019-07-23 21:59:09'),
(5, 'Dessins libres et dirigés', 6, 7, 1, 48, 'admin', 'admin', '2019-07-23 21:55:44', '2019-07-23 21:59:26'),
(6, 'Connait les différents manifestations de la vie et des éléments', 1, 7, 1, 48, 'admin', NULL, '2019-07-23 21:56:45', NULL),
(7, 'Connait les histoires bibliques qui lui sont racontées', 1, 7, 1, 48, 'admin', NULL, '2019-07-23 21:56:59', NULL),
(8, 'Ecoute, comprend et reprend les histoires racontés', 1, 7, 1, 48, 'admin', NULL, '2019-07-23 21:58:08', NULL),
(9, 'Reconnait et nomme différentes parties du corps humain', 1, 7, 1, 48, 'admin', NULL, '2019-07-23 21:58:19', NULL),
(10, 'Chante', 2, 7, 1, 48, 'admin', NULL, '2019-07-23 22:00:32', NULL),
(11, 'Fait la conversation', 2, 7, 1, 48, 'admin', NULL, '2019-07-23 22:00:49', NULL),
(12, 'Identifie les différents éléments des thèmes présentés', 2, 7, 1, 48, 'admin', NULL, '2019-07-23 22:01:09', NULL),
(13, 'Récite des poésie', 2, 7, 1, 48, 'admin', NULL, '2019-07-23 22:01:29', NULL),
(14, 'S\'exprime en créole', 2, 7, 1, 48, 'admin', NULL, '2019-07-23 22:01:49', NULL),
(15, 's\'exprime en francais', 2, 7, 1, 48, 'admin', NULL, '2019-07-23 22:02:05', NULL),
(16, 's\'exprime en anglais', 2, 7, 1, 48, 'admin', NULL, '2019-07-23 22:02:19', NULL),
(17, 'Perception auditive (son)', 5, 7, 1, 35, 'admin', NULL, '2019-07-23 22:04:00', NULL),
(18, 'Perception gustative (gout)', 5, 7, 1, 35, 'admin', NULL, '2019-07-23 22:04:11', NULL),
(19, 'Perception Olfactive (odeur)', 5, 7, 1, 35, 'admin', NULL, '2019-07-23 22:04:29', NULL),
(20, 'Perception Tactique (toucher)', 5, 7, 1, 35, 'admin', NULL, '2019-07-23 22:04:40', NULL),
(21, 'Perception Visuelle (couleur)', 5, 7, 1, 35, 'admin', NULL, '2019-07-23 22:04:50', NULL),
(22, 'Perception Visuelle (forme)', 5, 7, 1, 35, 'admin', NULL, '2019-07-23 22:04:59', NULL),
(23, 'Peut effectuer les actions physiques fondamentales', 7, 7, 1, 35, 'admin', NULL, '2019-07-23 22:06:09', NULL),
(24, 'Peut utiliser ses mains et ses droigts et manipuler des objets adéquatement', 7, 7, 1, 35, 'admin', NULL, '2019-07-23 22:06:26', NULL),
(25, 'Pré-écriture', 10, 7, 1, 48, 'admin', NULL, '2019-08-06 19:06:25', NULL),
(26, 'Pré-lecture', 10, 7, 1, 48, 'admin', NULL, '2019-08-06 19:06:39', NULL),
(27, 'Pré-calcul', 10, 7, 1, 48, 'admin', NULL, '2019-08-06 19:06:49', NULL),
(28, 'Pré-Maths', 10, 7, 1, 48, 'admin', NULL, '2019-08-06 19:06:59', NULL),
(29, 'Dessin', 10, 7, 1, 48, 'admin', NULL, '2019-08-06 19:07:07', NULL),
(30, 'Application au travail', 10, 7, 1, 48, 'admin', NULL, '2019-08-06 19:07:21', NULL),
(31, 'Reconnait et nomme différentes parties du corps humain (schéma corporel)', 1, 7, 6, 224, 'admin', NULL, '2019-09-04 14:48:32', NULL),
(32, 'Connait les histoire bibliques qui lui sont racontées', 1, 7, 6, 224, 'admin', NULL, '2019-09-04 14:48:58', NULL),
(33, 'S\'exprime en créole', 2, 7, 6, 224, 'admin', NULL, '2019-09-04 14:50:06', NULL),
(34, 'Fait la conversation', 2, 7, 6, 224, 'admin', NULL, '2019-09-04 14:50:20', NULL),
(35, 'Perception Visuelle (couleur)', 5, 7, 6, 224, 'admin', NULL, '2019-09-04 14:50:51', NULL),
(36, 'Perception visuelle (forme)', 5, 7, 6, 224, 'admin', NULL, '2019-09-04 14:51:33', NULL),
(37, 'Art Plastique', 6, 7, 6, 224, 'admin', NULL, '2019-09-04 14:52:02', NULL),
(38, 'Dessin libres et dirigés', 6, 7, 6, 224, 'admin', NULL, '2019-09-04 14:52:28', NULL),
(39, 'Peut effectuer les actions physiques fondamentales (Motricité globale)', 7, 7, 6, 224, 'admin', NULL, '2019-09-14 15:40:17', NULL),
(41, 'Apprend à distinguer le gauche de la droite (Orientation spatiale)', 8, 7, 6, 224, 'admin', NULL, '2019-09-14 15:49:07', NULL),
(42, 'Apprend à situer des objets ou des personnes les uns par rapport aux autres (Orientation spatiale)', 8, 7, 6, 224, 'admin', NULL, '2019-09-14 15:49:44', NULL),
(43, 'Reconnait et nomme différentes parties du corps humain (schéma corporel)', 1, 8, 6, 222, 'admin', NULL, '2019-09-14 15:52:10', NULL),
(44, 'Connait les histoires bibliques qui lui sont racontées (Bible)', 1, 8, 6, 222, 'admin', NULL, '2019-09-14 15:52:50', NULL),
(45, 'S\'exprime en créole (Créole)', 2, 8, 6, 222, 'admin', NULL, '2019-09-14 15:53:28', NULL),
(46, 'S\'exprime en Francais (Langage)', 2, 8, 6, 222, 'admin', NULL, '2019-09-14 15:53:47', NULL),
(47, 'Fait la conversation ', 2, 8, 6, 222, 'admin', NULL, '2019-09-14 16:01:25', NULL),
(48, 'Reconnait et nomme différentes parties du corps humain (schéma corporel)', 1, 12, 6, 24, 'admin', NULL, '2019-09-14 16:05:14', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kinder_evaluation`
--

CREATE TABLE `kinder_evaluation` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `concept` int(11) DEFAULT NULL,
  `mention` int(11) DEFAULT NULL,
  `free_comments` text,
  `from_special_cat` int(11) DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `period` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kinder_evaluation`
--

INSERT INTO `kinder_evaluation` (`id`, `student`, `concept`, `mention`, `free_comments`, `from_special_cat`, `academic_year`, `period`, `create_by`, `update_by`, `create_date`, `update_date`) VALUES
(1, 210, 6, 1, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:33:56', NULL),
(2, 210, 7, 3, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:34:03', NULL),
(3, 210, 8, 5, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:39:31', NULL),
(4, 210, 9, 5, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:39:36', NULL),
(5, 210, 15, 2, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:39:56', NULL),
(6, 210, 10, 1, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:40:01', NULL),
(7, 210, 11, 3, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:40:07', NULL),
(8, 210, 12, 5, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:40:13', NULL),
(9, 210, 13, 5, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:40:19', NULL),
(10, 210, 14, 2, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:40:23', NULL),
(11, 210, 16, 1, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 22:40:29', NULL),
(12, 210, 17, 1, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:20:29', NULL),
(13, 210, 18, 3, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:20:34', NULL),
(14, 210, 19, 3, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:20:41', NULL),
(15, 210, 20, 1, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:20:45', NULL),
(16, 210, 21, 2, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:20:50', NULL),
(17, 210, 22, 2, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:20:54', NULL),
(18, 210, 4, 2, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:21:03', NULL),
(19, 210, 5, 5, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:21:07', NULL),
(20, 210, 23, 2, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:21:14', NULL),
(21, 210, 24, 4, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:21:17', NULL),
(22, 210, 1, 1, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:21:23', NULL),
(23, 210, 2, 1, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:21:27', NULL),
(24, 210, 3, 2, NULL, NULL, 1, 1, 'admin', NULL, '2019-07-23 23:21:33', NULL),
(25, 210, NULL, NULL, 'A la maternelle, l\'évaluation doit se faire par rapport à l’enfant lui-même, à ses compétences, à son propre rythme. Souvent les enseignants s\'appuient sur des carnets d’évaluation dont les critères sont acquis et non acquis. Ce type de jugement s’avère être néfaste, démotivant, et parfois même culpabilisant, tant pour les enfants que pour leurs parents. Le point avec Jean Epstein.', 3, 1, 1, 'admin', NULL, '2019-07-23 23:23:13', NULL),
(26, 210, NULL, NULL, 'Pour progresser, un enfant, même petit, a sans aucun doute besoin d’être évalué … Cependant, au niveau de la maternelle, cette évaluation doit selon Jean Epstein se faire par rapport à l’enfant lui-même, à ses compétences, à son propre rythme, et en aucun cas selon une « pseudo-norme » amenant à comparer les enfants entre eux.', 4, 1, 1, 'admin', NULL, '2019-07-23 23:27:40', NULL),
(27, 210, NULL, NULL, 'Evaluer les capacités en dénombrement en début de grande section, tel est l\'objectif de ces fiches. On peut voir sur celle de gauche si les enfants comptent et reconnaissent les chiffres jusqu\'à 6, et sur les 2 autres jusqu\'à 9. Celle de gauche permet de repérer les enfants en difficulté, les 2 autres ceux qui sont en avance. 2 modèles sont proposés pour éviter la copie.', 9, 1, 1, 'admin', NULL, '2019-07-23 23:28:16', NULL),
(28, 216, 6, 3, NULL, NULL, 1, 3, 'LOGIPAM', 'LOGIPAM', '2019-07-26 14:10:21', '2019-07-27 12:49:46'),
(29, 216, 8, 2, NULL, NULL, 1, 3, 'LOGIPAM', NULL, '2019-07-27 12:49:50', NULL),
(30, 216, 7, 1, NULL, NULL, 1, 3, 'LOGIPAM', NULL, '2019-08-03 12:34:14', NULL),
(31, 216, 7, 2, NULL, NULL, 1, 3, 'LOGIPAM', NULL, '2019-08-03 12:34:20', NULL),
(32, 205, 6, 2, NULL, NULL, 1, 1, 'LOGIPAM', NULL, '2019-08-03 12:34:46', NULL),
(33, 210, 6, 4, NULL, NULL, 1, 3, 'admin', 'admin', '2019-08-06 12:45:35', '2019-08-07 17:56:16'),
(34, 210, 7, 4, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 12:45:41', NULL),
(35, 210, 8, 3, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 12:46:07', NULL),
(36, 210, 9, 5, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 12:46:13', NULL),
(37, 210, 25, 6, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 19:10:39', NULL),
(38, 210, 26, 8, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 19:10:46', NULL),
(39, 210, 27, 7, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 19:10:56', NULL),
(40, 210, 28, 8, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 19:11:02', NULL),
(41, 210, 29, 8, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 19:11:09', NULL),
(42, 210, 30, 7, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 19:11:14', NULL),
(43, 216, 9, 7, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-06 19:11:59', NULL),
(44, 210, 10, 3, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:45:32', NULL),
(45, 210, 11, 5, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:45:39', NULL),
(46, 210, 12, 6, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:45:46', NULL),
(47, 210, 13, 3, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:45:53', NULL),
(48, 210, 14, 8, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:45:59', NULL),
(49, 210, 15, 4, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:46:06', NULL),
(50, 210, 16, 2, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:46:14', NULL),
(51, 210, 17, 1, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:46:34', NULL),
(52, 210, 18, 4, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:46:39', NULL),
(53, 210, 19, 5, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:46:45', NULL),
(54, 210, 20, 5, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:46:51', NULL),
(55, 210, 21, 7, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:46:58', NULL),
(56, 210, 22, 1, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:47:07', NULL),
(57, 210, 4, 1, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:47:24', NULL),
(58, 210, 5, 4, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:47:29', NULL),
(59, 210, 24, 1, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:47:39', NULL),
(60, 210, 23, 4, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:47:45', NULL),
(61, 210, 3, 1, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:49:14', NULL),
(62, 210, 2, 2, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:49:20', NULL),
(63, 210, 1, 3, NULL, NULL, 1, 3, 'admin', NULL, '2019-08-07 00:49:27', NULL),
(64, 216, NULL, NULL, 'En plus de SIGES qui est déjà adopté par plusieurs écoles, Logipam a déjà implémenté OpenEmr (Logiciel de gestion des dossiers médicaux des patients) dans plusieurs cabinets médicaux et implémenté PMB (Logiciel de gestion de bibliothèques) dans plusieurs bibliothèques. ', 3, 1, 3, 'admin', NULL, '2019-08-07 00:50:22', NULL),
(65, 216, NULL, NULL, 'En plus de SIGES qui est déjà adopté par plusieurs écoles, Logipam a déjà implémenté OpenEmr (Logiciel de gestion des dossiers médicaux des patients) dans plusieurs cabinets médicaux et implémenté PMB (Logiciel de gestion de bibliothèques) dans plusieurs bibliothèques. ', 4, 1, 3, 'admin', NULL, '2019-08-07 00:51:41', NULL),
(66, 216, NULL, NULL, 'En plus de SIGES qui est déjà adopté par plusieurs écoles, Logipam a déjà implémenté OpenEmr (Logiciel de gestion des dossiers médicaux des patients) dans plusieurs cabinets médicaux et implémenté PMB (Logiciel de gestion de bibliothèques) dans plusieurs bibliothèques. ', 9, 1, 3, 'admin', NULL, '2019-08-07 00:52:12', NULL),
(67, 332, 48, 4, NULL, NULL, 6, 4, 'admin', NULL, '2019-09-16 06:26:26', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kinder_mention`
--

CREATE TABLE `kinder_mention` (
  `id` int(11) NOT NULL,
  `mention_name` varchar(64) NOT NULL,
  `mention_short_name` varchar(32) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kinder_mention`
--

INSERT INTO `kinder_mention` (`id`, `mention_name`, `mention_short_name`, `create_by`, `update_by`, `create_date`, `update_date`) VALUES
(1, 'Très bien', 'TB', 'admin', NULL, '2019-07-23 21:48:54', NULL),
(2, 'Excellent', 'E', 'admin', NULL, '2019-07-23 21:49:07', NULL),
(3, 'Assez bien', 'AB', 'admin', NULL, '2019-07-23 21:49:22', NULL),
(4, 'Pas satisfaisant', 'P', 'admin', NULL, '2019-07-23 21:49:39', NULL),
(5, 'Bien', 'B', 'admin', NULL, '2019-07-23 21:49:48', NULL),
(6, 'Parfois', 'PAR', 'admin', NULL, '2019-08-06 19:09:50', NULL),
(7, 'Souvent', 'S', 'admin', NULL, '2019-08-06 19:10:01', NULL),
(8, 'Toujours', 'T', 'admin', NULL, '2019-08-06 19:10:13', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kinder_period`
--

CREATE TABLE `kinder_period` (
  `id` int(11) NOT NULL,
  `period_name` varchar(32) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `is_last_period` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kinder_period`
--

INSERT INTO `kinder_period` (`id`, `period_name`, `academic_year`, `date_start`, `date_end`, `is_last_period`, `create_by`, `update_by`, `create_date`, `update_date`) VALUES
(1, 'Période I', 1, '2018-09-03', '2018-12-14', 0, 'admin', 'admin', '2019-07-22 17:50:53', '2019-07-23 23:38:54'),
(2, 'Période II', 1, '2018-12-15', '2019-03-31', 0, 'admin', 'admin', '2019-07-22 17:52:36', '2019-07-23 23:39:31'),
(3, 'Période III', 1, '2019-04-01', '2019-08-31', 1, 'admin', 'admin', '2019-07-22 17:53:08', '2019-07-23 23:39:44'),
(4, 'Periode 1', 6, '2019-09-04', '2019-10-31', 0, 'admin', NULL, '2019-09-04 14:52:59', NULL),
(5, '2e Periode', 6, '2019-11-04', '2019-12-27', 0, 'admin', NULL, '2019-09-14 16:08:17', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `kinder_teacher`
--

CREATE TABLE `kinder_teacher` (
  `id` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `new_teacher` int(11) DEFAULT NULL,
  `create_by` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(255) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `kinder_teacher`
--

INSERT INTO `kinder_teacher` (`id`, `teacher`, `room`, `academic_year`, `new_teacher`, `create_by`, `create_date`, `update_by`, `update_date`) VALUES
(1, 37, 8, 6, NULL, 'admin', '2019-09-16 06:28:27', NULL, NULL),
(2, 20, 9, 6, NULL, 'admin', '2019-09-16 06:28:35', NULL, NULL),
(3, 12, 16, 6, NULL, 'admin', '2019-09-16 06:28:44', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `label_category_for_billing`
--

CREATE TABLE `label_category_for_billing` (
  `id` int(11) NOT NULL,
  `category` varchar(220) NOT NULL,
  `income_expense` varchar(3) NOT NULL COMMENT 'ri: income; di: expense'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `label_category_for_billing`
--

INSERT INTO `label_category_for_billing` (`id`, `category`, `income_expense`) VALUES
(1, 'Donations and grants', 'ri'),
(2, 'Other incomes', 'ri'),
(3, 'Rent expenses', 'di'),
(4, 'Amenities and services', 'di'),
(5, 'Staff', 'di'),
(6, 'Tax', 'di'),
(7, 'Other expenses', 'di');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL COMMENT 'School level (calling classes dans le system scolaire Haitien)',
  `level_name` varchar(45) NOT NULL,
  `short_level_name` varchar(45) NOT NULL,
  `previous_level` int(11) DEFAULT NULL,
  `section` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `level_name`, `short_level_name`, `previous_level`, `section`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Septième Année', '7ème AF', 6, 1, '2019-07-01 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(2, 'Huitième Année', '8ème AF', 1, 1, '2019-07-01 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(3, 'Neuvième Année', '9ème AF', 2, 1, '2019-07-01 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(4, 'Secondaire I', 'Sec I', 3, 2, '2019-07-01 00:00:00', '2019-07-01 00:00:00', 'admin', NULL),
(5, 'Cinquième Année', '5ème AF', 16, 1, '2019-07-05 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(6, 'Sixième Année', '6ème AF ', 5, 1, '2019-07-05 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(7, 'Section des Petits', 'Petits', NULL, 3, '2019-07-22 00:00:00', '2019-07-23 00:00:00', 'admin', 'admin'),
(8, 'Section des Moyens', 'Moyens', 7, 3, '2019-08-03 00:00:00', '2019-08-10 00:00:00', 'LOGIPAM', 'admin'),
(9, 'Secondaire 2', 'Sec 2', 4, 2, '2019-08-05 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(10, 'Secondaire 3', 'Sec 3', 9, 2, '2019-08-05 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(11, 'Secondaire 4', 'Sec 4', 10, 2, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', 'admin'),
(12, 'Section des Grands', 'Grands', 8, 3, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(13, 'Première Année ', '1ère AF', 12, 1, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(14, 'Deuxième Année ', ' 2ème AF', 13, 1, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(15, 'Troisième Année ', '3ème AF', 14, 1, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(16, 'Quatrième Année ', '4ème AF', 15, 1, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `level_has_person`
--

CREATE TABLE `level_has_person` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `level_has_person`
--

INSERT INTO `level_has_person` (`id`, `level`, `students`, `academic_year`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 1, 52, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(2, 1, 53, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(3, 1, 54, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(4, 1, 55, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(5, 1, 56, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(6, 1, 57, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(7, 1, 58, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(8, 1, 59, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(9, 1, 60, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(10, 1, 61, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(11, 1, 62, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(12, 1, 63, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(13, 1, 64, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(14, 1, 65, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(15, 1, 66, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(16, 1, 67, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(17, 1, 68, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(18, 1, 69, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(19, 1, 70, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(20, 1, 71, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(21, 1, 72, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(22, 1, 73, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(23, 1, 74, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(24, 1, 75, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(25, 1, 76, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(26, 2, 77, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(27, 2, 78, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(28, 2, 79, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(29, 2, 80, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(30, 2, 81, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(31, 2, 82, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(32, 2, 83, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(33, 2, 84, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(34, 2, 85, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(35, 2, 86, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(36, 2, 87, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(37, 2, 88, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(38, 2, 89, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(39, 2, 90, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(40, 2, 91, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(41, 2, 92, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(42, 2, 93, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(43, 2, 94, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(44, 2, 95, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(45, 2, 96, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(46, 2, 97, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(47, 2, 98, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(48, 2, 99, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(49, 2, 100, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(50, 2, 101, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(51, 2, 102, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(52, 2, 103, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(53, 3, 104, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(54, 3, 105, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(55, 3, 106, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(56, 3, 107, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(57, 3, 108, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(58, 3, 109, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(59, 3, 110, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(60, 3, 111, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(61, 3, 112, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(62, 3, 113, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(63, 3, 114, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(64, 3, 115, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(65, 3, 116, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(66, 3, 117, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(67, 3, 118, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(68, 3, 119, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(69, 3, 120, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(70, 3, 121, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(71, 3, 122, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(72, 3, 123, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(73, 3, 124, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(74, 3, 125, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(75, 3, 126, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(76, 3, 127, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(77, 3, 128, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(78, 4, 129, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(79, 4, 130, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(80, 4, 131, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(81, 4, 132, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(82, 4, 133, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(83, 4, 134, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(84, 4, 135, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(85, 4, 136, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(86, 4, 137, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(87, 4, 138, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(88, 4, 139, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(89, 4, 140, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(90, 4, 141, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(91, 4, 142, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(92, 4, 143, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(93, 4, 144, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(94, 4, 145, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(95, 4, 146, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(96, 4, 147, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(97, 4, 148, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(98, 4, 149, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(99, 4, 150, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(100, 4, 151, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(101, 4, 152, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(102, 4, 153, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(103, 4, 154, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(104, 5, 155, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(105, 5, 156, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(106, 5, 157, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(107, 5, 158, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(108, 5, 159, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(109, 5, 160, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(110, 5, 161, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(111, 5, 162, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(112, 5, 163, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(113, 5, 164, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(114, 5, 165, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(115, 5, 166, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(116, 5, 167, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(117, 5, 168, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(118, 5, 169, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(119, 5, 170, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(120, 5, 171, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(121, 5, 172, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(122, 5, 173, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(123, 5, 174, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(124, 5, 175, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(125, 5, 176, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(126, 5, 177, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(127, 5, 178, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(128, 6, 179, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(129, 6, 180, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(130, 6, 181, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(131, 6, 182, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(132, 6, 183, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(133, 6, 184, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(134, 6, 185, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(135, 6, 186, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(136, 6, 187, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(137, 6, 188, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(138, 6, 189, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(139, 6, 190, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(140, 6, 191, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(141, 6, 192, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(142, 6, 193, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(143, 6, 194, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(144, 6, 195, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(145, 6, 196, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(146, 6, 197, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(147, 6, 198, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(148, 6, 199, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(149, 6, 200, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(150, 6, 201, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(151, 6, 202, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(152, 6, 203, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(153, 7, 204, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(154, 7, 205, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(155, 7, 206, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(156, 7, 207, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(157, 7, 208, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(158, 7, 209, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(159, 7, 210, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(160, 7, 211, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(161, 7, 212, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(162, 7, 213, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(163, 7, 214, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(164, 7, 215, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(165, 7, 216, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(166, 1, 228, 1, NULL, NULL, NULL, NULL),
(167, 1, 229, 1, NULL, NULL, NULL, NULL),
(168, 1, 230, 1, NULL, NULL, NULL, NULL),
(169, 1, 231, 1, NULL, NULL, NULL, NULL),
(170, 1, 232, 1, NULL, NULL, NULL, NULL),
(171, 1, 233, 1, NULL, NULL, NULL, NULL),
(172, 1, 234, 1, NULL, NULL, NULL, NULL),
(173, 1, 235, 1, NULL, NULL, NULL, NULL),
(174, 1, 236, 1, NULL, NULL, NULL, NULL),
(175, 1, 237, 1, NULL, NULL, NULL, NULL),
(176, 1, 238, 1, NULL, NULL, NULL, NULL),
(177, 1, 239, 1, NULL, NULL, NULL, NULL),
(178, 2, 240, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(179, 2, 55, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(180, 2, 66, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(181, 2, 60, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(182, 2, 73, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(183, 2, 59, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(184, 2, 75, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(185, 2, 72, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(186, 2, 67, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(187, 2, 69, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(188, 2, 54, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(189, 2, 76, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(190, 2, 56, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(191, 2, 63, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(192, 2, 58, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(193, 2, 57, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(194, 2, 65, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(195, 2, 62, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(196, 2, 68, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(197, 2, 74, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(198, 2, 52, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(199, 2, 71, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(200, 2, 64, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(201, 2, 61, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(202, 2, 70, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(203, 2, 229, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(204, 2, 232, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(205, 2, 231, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(206, 2, 230, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(207, 2, 233, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(208, 2, 236, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(209, 2, 238, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(210, 1, 239, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(211, 2, 234, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(212, 2, 237, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(213, 2, 235, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(214, 2, 228, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(215, 3, 88, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(216, 3, 90, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(217, 3, 77, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(218, 3, 91, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(219, 3, 82, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(220, 3, 92, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(221, 3, 85, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(222, 3, 87, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(223, 3, 93, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(224, 3, 94, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(225, 3, 78, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(226, 3, 79, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(227, 3, 95, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(228, 3, 81, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(229, 3, 96, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(230, 3, 97, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(231, 3, 98, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(232, 3, 99, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(233, 3, 100, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(234, 3, 101, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(235, 3, 86, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(236, 3, 89, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(237, 3, 83, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(238, 3, 84, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(239, 3, 80, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(240, 3, 102, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(241, 3, 103, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(242, 4, 106, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(243, 4, 114, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(244, 4, 110, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(245, 4, 118, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(246, 4, 119, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(247, 4, 113, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(248, 4, 107, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(249, 4, 111, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(250, 4, 120, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(251, 4, 121, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(252, 4, 112, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(253, 4, 116, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(254, 4, 122, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(255, 4, 115, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(256, 4, 123, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(257, 4, 124, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(258, 4, 109, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(259, 4, 126, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(260, 4, 117, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(261, 4, 127, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(262, 4, 128, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(263, 9, 132, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(264, 9, 154, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(265, 9, 140, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(266, 9, 144, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(267, 9, 145, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(268, 9, 148, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(269, 9, 149, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(270, 9, 143, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(271, 9, 153, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(272, 9, 137, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(273, 9, 147, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(274, 9, 129, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(275, 9, 151, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(276, 9, 130, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(277, 9, 138, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(278, 9, 150, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(279, 9, 141, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(280, 9, 133, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(281, 9, 152, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(282, 9, 142, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(283, 9, 146, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(284, 9, 135, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(285, 9, 139, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(286, 9, 134, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(287, 9, 136, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(288, 9, 131, 6, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(289, 6, 164, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(290, 6, 173, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(291, 6, 162, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(292, 6, 158, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(293, 6, 166, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(294, 6, 156, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(295, 6, 176, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(296, 6, 170, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(297, 6, 159, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(298, 6, 161, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(299, 6, 165, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(300, 6, 172, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(301, 6, 177, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(302, 6, 163, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(303, 6, 155, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(304, 6, 174, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(305, 6, 171, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(306, 6, 178, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(307, 6, 167, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(308, 6, 168, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(309, 6, 160, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(310, 6, 169, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(311, 6, 175, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(312, 8, 208, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(313, 8, 212, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(314, 8, 216, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(315, 8, 209, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(316, 8, 210, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(317, 8, 207, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(318, 8, 214, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(319, 8, 213, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(320, 8, 205, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(321, 8, 206, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(322, 8, 215, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(323, 8, 211, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(324, 8, 204, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(325, 1, 184, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(326, 1, 195, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(327, 1, 190, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(328, 1, 192, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(329, 1, 179, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(330, 1, 185, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(331, 1, 186, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(332, 1, 188, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(333, 1, 199, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(334, 1, 197, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(335, 1, 202, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(336, 1, 198, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(337, 1, 157, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(338, 1, 201, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(339, 1, 180, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(340, 1, 196, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(341, 1, 187, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(342, 1, 200, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(343, 1, 193, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(344, 1, 189, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(345, 1, 203, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(346, 1, 194, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(347, 1, 191, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(348, 1, 181, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(349, 1, 182, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(350, 1, 183, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(351, 3, 241, 6, NULL, NULL, NULL, NULL),
(352, 3, 242, 6, NULL, NULL, NULL, NULL),
(353, 3, 243, 6, NULL, NULL, NULL, NULL),
(354, 3, 244, 6, NULL, NULL, NULL, NULL),
(355, 3, 245, 6, NULL, NULL, NULL, NULL),
(356, 3, 247, 6, NULL, NULL, NULL, NULL),
(357, 3, 248, 6, NULL, NULL, NULL, NULL),
(358, 3, 249, 6, NULL, NULL, NULL, NULL),
(359, 3, 246, 6, NULL, NULL, NULL, NULL),
(360, 3, 250, 6, NULL, NULL, NULL, NULL),
(361, 3, 251, 6, NULL, NULL, NULL, NULL),
(362, 3, 252, 6, NULL, NULL, NULL, NULL),
(363, 3, 260, 6, NULL, NULL, NULL, NULL),
(364, 13, 262, 6, NULL, NULL, NULL, NULL),
(365, 3, 263, 6, NULL, NULL, NULL, NULL),
(366, 3, 265, 6, NULL, NULL, NULL, NULL),
(367, 10, 266, 6, NULL, NULL, NULL, NULL),
(368, 10, 267, 6, NULL, NULL, NULL, NULL),
(369, 10, 268, 6, NULL, NULL, NULL, NULL),
(370, 10, 269, 6, NULL, NULL, NULL, NULL),
(371, 10, 270, 6, NULL, NULL, NULL, NULL),
(372, 10, 275, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(373, 10, 276, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(374, 10, 277, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(375, 10, 278, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(376, 10, 279, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(377, 10, 280, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(378, 10, 281, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(379, 10, 282, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(380, 10, 283, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(381, 10, 284, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(382, 10, 285, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(383, 10, 286, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(384, 10, 287, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(385, 10, 288, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(386, 10, 289, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(387, 10, 290, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(388, 10, 291, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(389, 10, 292, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(390, 10, 293, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(391, 10, 294, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(392, 10, 295, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(393, 10, 296, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(394, 10, 297, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(395, 10, 298, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(396, 10, 299, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(397, 10, 300, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(398, 10, 301, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(399, 10, 302, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(400, 10, 303, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(401, 10, 304, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(402, 10, 305, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(403, 10, 306, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(404, 10, 307, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(405, 10, 308, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(406, 10, 309, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(407, 10, 310, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(408, 10, 311, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(409, 10, 312, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(410, 10, 313, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(411, 10, 314, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(412, 10, 315, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(413, 10, 316, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(414, 10, 317, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(415, 10, 318, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(416, 10, 319, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(417, 10, 320, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(418, 10, 321, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(419, 10, 322, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(420, 10, 323, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(421, 10, 324, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(422, 10, 325, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(423, 10, 326, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(424, 10, 327, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(425, 10, 328, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(426, 7, 329, 6, NULL, NULL, NULL, NULL),
(427, 7, 330, 6, NULL, NULL, NULL, NULL),
(428, 12, 331, 6, NULL, NULL, NULL, NULL),
(429, 12, 332, 6, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `loan_of_money`
--

CREATE TABLE `loan_of_money` (
  `id` int(11) NOT NULL,
  `loan_date` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payroll_month` int(3) NOT NULL,
  `deduction_percentage` int(3) NOT NULL,
  `solde` double NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `number_of_month_repayment` int(5) NOT NULL,
  `remaining_month_number` int(5) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(11) NOT NULL,
  `sender` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `receivers` text CHARACTER SET utf8,
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `is_read` int(1) DEFAULT NULL,
  `id_sender` int(11) DEFAULT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` int(11) DEFAULT NULL,
  `is_my_send` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menfp_decision`
--

CREATE TABLE `menfp_decision` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `total_grade` double NOT NULL,
  `average` double DEFAULT NULL,
  `mention` varchar(100) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menfp_grades`
--

CREATE TABLE `menfp_grades` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `menfp_exam` int(11) NOT NULL,
  `grade` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `module_short_name` varchar(64) NOT NULL,
  `module_name` varchar(64) NOT NULL,
  `mod_lateral_menu` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_short_name`, `module_name`, `mod_lateral_menu`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES
(1, 'configuration', 'Paramètre Ecole', '//layouts/menuSchoolSetting', NULL, NULL, NULL, NULL),
(5, 'users', 'Utilisateur', '//layouts/menuUser', NULL, NULL, NULL, NULL),
(6, 'reports', 'Reports', '//layouts/menuReportManager', NULL, NULL, NULL, NULL),
(7, 'schoolconfig', 'Gestion académique', '//layouts/menuAcademicSetting', NULL, NULL, NULL, NULL),
(8, 'billings', 'Facturation', '//layouts/menuBilling', NULL, NULL, NULL, NULL),
(9, 'academic', 'Académique', '//layouts/menuStudentManager', NULL, NULL, NULL, NULL),
(10, 'guest', 'Invite', NULL, NULL, NULL, NULL, NULL),
(11, 'discipline', 'Discipline', '', NULL, NULL, NULL, NULL),
(12, 'portal', 'Portal', '', NULL, NULL, NULL, NULL),
(13, 'idcards', 'ID card', NULL, NULL, NULL, NULL, NULL),
(14, 'kindergarden', 'Kindergarden', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `other_incomes`
--

CREATE TABLE `other_incomes` (
  `id` int(11) NOT NULL,
  `id_income_description` int(11) NOT NULL,
  `amount` double NOT NULL,
  `income_date` date NOT NULL,
  `academic_year` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_incomes`
--

INSERT INTO `other_incomes` (`id`, `id_income_description`, `amount`, `income_date`, `academic_year`, `description`, `date_created`, `date_updated`, `created_by`, `updated_by`) VALUES
(1, 6, 10000, '2019-08-18', 6, 'Une visite guidée sera organiser dans le nord. La citadelle et le palais sans souci font partie des lieux que nous auront à visiter.', '2019-08-18 00:00:00', '0000-00-00 00:00:00', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `other_incomes_description`
--

CREATE TABLE `other_incomes_description` (
  `id` int(11) NOT NULL,
  `income_description` varchar(65) NOT NULL,
  `category` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `other_incomes_description`
--

INSERT INTO `other_incomes_description` (`id`, `income_description`, `category`, `comment`) VALUES
(6, 'Sorties éducatives-Excursion', 2, '');

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `activity_field` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `partners`
--

INSERT INTO `partners` (`id`, `name`, `address`, `email`, `phone`, `activity_field`, `contact`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'HELP', '3, Rue Chardonnière, Delmas 65', 'help@gmail.com', '+509 34 45 4837', 'Education', 'Andrise Joseph', '2019-07-04 00:00:00', NULL, NULL, NULL),
(2, 'Fonkoze', '', 'gjacques@gmail.com', '', '', 'Gilbert Jacques', '2019-08-06 00:00:00', NULL, NULL, NULL),
(3, 'Georges', '23 Rue Montb;anc', '', '', '', '', '2019-08-06 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `passing_grades`
--

CREATE TABLE `passing_grades` (
  `id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `course` int(11) DEFAULT NULL,
  `academic_period` int(11) NOT NULL,
  `minimum_passing` float NOT NULL,
  `level_or_course` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: level, 1: course',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(20) DEFAULT NULL,
  `update_by` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `passing_grades`
--

INSERT INTO `passing_grades` (`id`, `level`, `course`, `academic_period`, `minimum_passing`, `level_or_course`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 2, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(2, 3, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(3, 4, NULL, 1, 50, 0, '2019-07-01 00:00:00', NULL, NULL, NULL),
(4, 1, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(5, 13, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(6, 14, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(7, 15, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(8, 16, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(9, 5, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(10, 6, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(11, 9, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(12, 10, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(13, 11, NULL, 1, 50, 0, '2019-08-10 00:00:00', NULL, NULL, NULL),
(14, 2, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(15, 3, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(16, 4, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(17, 1, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(18, 13, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(19, 14, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(20, 15, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(21, 16, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(22, 5, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(23, 6, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(24, 9, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(25, 10, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL),
(26, 11, NULL, 6, 50, 0, '2019-08-10 00:00:00', NULL, 'SIGES', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `method_name` varchar(45) NOT NULL,
  `description` text,
  `is_default` tinyint(2) NOT NULL DEFAULT '0',
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `method_name`, `description`, `is_default`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'Liquide', '', 0, '2019-07-04 00:00:00', '2019-09-06 00:00:00', NULL, NULL),
(2, 'Chèque', '', 0, '2019-07-04 00:00:00', NULL, NULL, NULL),
(3, 'Dépôt', '', 1, '2019-07-04 00:00:00', '2019-09-06 00:00:00', NULL, NULL),
(4, 'Virement', '', 0, '2019-08-10 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `id_payroll_set` int(11) NOT NULL,
  `id_payroll_set2` int(11) DEFAULT NULL COMMENT 'recois l''id_payrollsetting professeur s''il est a la fois employe et professeur',
  `payroll_month` int(3) NOT NULL,
  `payroll_date` date NOT NULL,
  `missing_hour` int(11) NOT NULL,
  `taxe` double NOT NULL,
  `gross_salary` double NOT NULL,
  `percentage` double DEFAULT NULL,
  `net_salary` double NOT NULL,
  `payment_date` date NOT NULL,
  `cash_check` varchar(45) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payroll`
--

INSERT INTO `payroll` (`id`, `id_payroll_set`, `id_payroll_set2`, `payroll_month`, `payroll_date`, `missing_hour`, `taxe`, `gross_salary`, `percentage`, `net_salary`, `payment_date`, `cash_check`, `date_created`, `date_updated`, `created_by`, `updated_by`) VALUES
(1, 2, NULL, 2, '2019-02-23', 0, 360, 4000, NULL, 3640, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(2, 3, NULL, 2, '2019-02-23', 0, 11500, 50000, NULL, 38500, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(3, 4, NULL, 2, '2019-02-23', 0, 225, 2500, NULL, 2275, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(4, 5, NULL, 2, '2019-02-23', 0, 405, 4500, NULL, 4095, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(5, 6, NULL, 2, '2019-02-23', 0, 8100, 40000, NULL, 31900, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(6, 7, NULL, 2, '2019-02-23', 0, 6900, 35000, NULL, 28100, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(7, 8, NULL, 2, '2019-02-23', 0, 3300, 20000, NULL, 16700, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(8, 9, NULL, 2, '2019-02-23', 0, 2445, 15500, NULL, 13055, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(9, 10, NULL, 2, '2019-02-23', 0, 1400, 10000, NULL, 8600, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(10, 11, NULL, 2, '2019-02-23', 0, 1780, 12000, NULL, 10220, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(11, 12, NULL, 2, '2019-02-23', 0, 944, 7600, NULL, 6656, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(12, 13, NULL, 2, '2019-02-23', 0, 4020, 23000, NULL, 18980, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(13, 14, NULL, 2, '2019-02-23', 0, 12860, 54000, NULL, 41140, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(14, 15, NULL, 2, '2019-02-23', 0, 1210, 9000, NULL, 7790, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(15, 16, NULL, 2, '2019-02-23', 0, 1495, 10500, NULL, 9005, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(16, 17, NULL, 2, '2019-02-23', 0, 1590, 11000, NULL, 9410, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(17, 18, NULL, 2, '2019-02-23', 0, 1780, 12000, NULL, 10220, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(18, 19, NULL, 2, '2019-02-23', 0, 9800, 45000, NULL, 35200, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(19, 20, NULL, 2, '2019-02-23', 0, 3660, 21500, NULL, 17840, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(20, 21, NULL, 2, '2019-02-23', 0, 1856, 12400, NULL, 10544, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(21, 22, NULL, 2, '2019-02-23', 0, 10004, 45600, NULL, 35596, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(22, 24, NULL, 2, '2019-02-23', 0, 3195.5, 19450, NULL, 16254.5, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(23, 23, NULL, 2, '2019-02-23', 0, 4020, 23000, NULL, 18980, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(24, 25, NULL, 2, '2019-02-23', 0, 6804, 34600, NULL, 27796, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(25, 26, NULL, 2, '2019-02-23', 0, 3540, 21000, NULL, 17460, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(26, 27, NULL, 2, '2019-02-23', 0, 4020, 23000, NULL, 18980, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(27, 28, NULL, 2, '2019-02-23', 0, 3540, 21000, NULL, 17460, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(28, 29, NULL, 2, '2019-02-23', 0, 4020, 23000, NULL, 18980, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(29, 30, NULL, 2, '2019-02-23', 0, 982, 7800, NULL, 6818, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', ''),
(30, 31, NULL, 2, '2019-02-23', 0, 320.4, 3560, NULL, 3239.6, '2019-02-23', 'Cash', '2019-08-06', '0000-00-00', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_settings`
--

CREATE TABLE `payroll_settings` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `an_hour` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0:no, 1:yes',
  `number_of_hour` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `as` int(2) DEFAULT '0' COMMENT '0: employee; 1: teacher',
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old setting; 1: new setting',
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payroll_settings`
--

INSERT INTO `payroll_settings` (`id`, `person_id`, `amount`, `an_hour`, `number_of_hour`, `academic_year`, `as`, `old_new`, `date_created`, `date_updated`, `created_by`, `updated_by`) VALUES
(1, 33, 1200, 1, 0, 1, 0, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(2, 40, 250, 1, 16, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(3, 8, 50000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(4, 49, 2500, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(5, 221, 250, 1, 18, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(6, 220, 40000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(7, 5, 35000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(8, 219, 20000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(9, 3, 15500, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(10, 38, 10000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(11, 7, 12000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(12, 6, 7600, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(13, 18, 23000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(14, 47, 54000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(15, 226, 9000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(16, 22, 10500, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(17, 32, 11000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(18, 34, 12000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(19, 50, 45000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(20, 46, 21500, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(21, 218, 12400, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(22, 30, 45600, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(23, 19, 23000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(24, 27, 19450, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(25, 21, 34600, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(26, 10, 21000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(27, 36, 23000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(28, 26, 21000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(29, 43, 23000, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(30, 31, 7800, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', ''),
(31, 25, 3560, 0, 0, 1, 1, 1, '2019-08-06', '0000-00-00', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `payroll_setting_taxes`
--

CREATE TABLE `payroll_setting_taxes` (
  `id` int(11) NOT NULL,
  `id_payroll_set` int(11) NOT NULL,
  `id_taxe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payroll_setting_taxes`
--

INSERT INTO `payroll_setting_taxes` (`id`, `id_payroll_set`, `id_taxe`) VALUES
(1, 2, 1),
(2, 2, 3),
(3, 2, 5),
(4, 2, 6),
(5, 2, 7),
(6, 3, 1),
(7, 3, 3),
(8, 3, 5),
(9, 3, 6),
(10, 3, 7),
(11, 4, 1),
(12, 4, 3),
(13, 4, 5),
(14, 4, 6),
(15, 4, 7),
(16, 5, 1),
(17, 5, 3),
(18, 5, 5),
(19, 5, 6),
(20, 5, 7),
(21, 6, 1),
(22, 6, 3),
(23, 6, 5),
(24, 6, 6),
(25, 6, 7),
(26, 7, 1),
(27, 7, 3),
(28, 7, 5),
(29, 7, 6),
(30, 7, 7),
(31, 8, 1),
(32, 8, 3),
(33, 8, 5),
(34, 8, 6),
(35, 8, 7),
(36, 9, 1),
(37, 9, 3),
(38, 9, 5),
(39, 9, 6),
(40, 9, 7),
(41, 10, 1),
(42, 10, 3),
(43, 10, 5),
(44, 10, 6),
(45, 10, 7),
(46, 11, 1),
(47, 11, 3),
(48, 11, 5),
(49, 11, 6),
(50, 11, 7),
(51, 12, 1),
(52, 12, 3),
(53, 12, 5),
(54, 12, 6),
(55, 12, 7),
(56, 13, 1),
(57, 13, 3),
(58, 13, 5),
(59, 13, 6),
(60, 13, 7),
(61, 14, 1),
(62, 14, 3),
(63, 14, 5),
(64, 14, 6),
(65, 14, 7),
(66, 15, 1),
(67, 15, 3),
(68, 15, 5),
(69, 15, 6),
(70, 15, 7),
(71, 16, 1),
(72, 16, 3),
(73, 16, 5),
(74, 16, 6),
(75, 16, 7),
(76, 17, 1),
(77, 17, 3),
(78, 17, 5),
(79, 17, 6),
(80, 17, 7),
(81, 18, 1),
(82, 18, 3),
(83, 18, 5),
(84, 18, 6),
(85, 18, 7),
(86, 19, 1),
(87, 19, 3),
(88, 19, 5),
(89, 19, 6),
(90, 19, 7),
(91, 20, 1),
(92, 20, 3),
(93, 20, 5),
(94, 20, 6),
(95, 20, 7),
(96, 21, 1),
(97, 21, 3),
(98, 21, 5),
(99, 21, 6),
(100, 21, 7),
(101, 22, 1),
(102, 22, 3),
(103, 22, 5),
(104, 22, 6),
(105, 22, 7),
(106, 23, 1),
(107, 23, 3),
(108, 23, 5),
(109, 23, 6),
(110, 23, 7),
(111, 24, 1),
(112, 24, 3),
(113, 24, 5),
(114, 24, 6),
(115, 24, 7),
(116, 25, 1),
(117, 25, 3),
(118, 25, 5),
(119, 25, 6),
(120, 25, 7),
(121, 26, 1),
(122, 26, 3),
(123, 26, 5),
(124, 26, 6),
(125, 26, 7),
(126, 27, 1),
(127, 27, 3),
(128, 27, 5),
(129, 27, 6),
(130, 27, 7),
(131, 28, 1),
(132, 28, 3),
(133, 28, 5),
(134, 28, 6),
(135, 28, 7),
(136, 29, 1),
(137, 29, 3),
(138, 29, 5),
(139, 29, 6),
(140, 29, 7),
(141, 30, 1),
(142, 30, 3),
(143, 30, 5),
(144, 30, 6),
(145, 30, 7),
(146, 31, 1),
(147, 31, 3),
(148, 31, 5),
(149, 31, 6),
(150, 31, 7);

-- --------------------------------------------------------

--
-- Table structure for table `pending_balance`
--

CREATE TABLE `pending_balance` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `balance` double NOT NULL,
  `is_paid` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not yet; 1: paid',
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id` int(11) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `blood_group` varchar(10) NOT NULL,
  `birthday` date DEFAULT NULL,
  `id_number` varchar(50) DEFAULT NULL,
  `is_student` tinyint(1) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `nif_cin` varchar(100) NOT NULL,
  `cities` int(11) DEFAULT NULL,
  `citizenship` varchar(45) NOT NULL,
  `mother_first_name` varchar(55) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `identifiant` varchar(100) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `matricule` varchar(100) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `paid` tinyint(2) DEFAULT NULL COMMENT 'for admission list. 0: not yet paid; 1: already paid; NULL: left admission list ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `inactive_reason` tinyint(4) DEFAULT NULL COMMENT '1:abandon 2:expulsion 3:maladie 4:voyage',
  `image` varchar(50) DEFAULT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `last_name`, `first_name`, `gender`, `blood_group`, `birthday`, `id_number`, `is_student`, `adresse`, `phone`, `email`, `nif_cin`, `cities`, `citizenship`, `mother_first_name`, `identifiant`, `matricule`, `paid`, `date_created`, `date_updated`, `create_by`, `update_by`, `active`, `inactive_reason`, `image`, `comment`) VALUES
(1, 'Super', 'Admin', NULL, '', NULL, 'ADSU1', NULL, NULL, NULL, 'info@logipam.com', '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, ''),
(2, 'Dessalines', 'Jean-Jacques ', '0', '', '1758-09-20', 'JEDE2', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(3, 'Christophe', 'Henri  ', '0', '', '1767-10-06', 'HECH3', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(4, 'Pétion', 'Alexandre ', '0', '', '1870-04-02', 'ALPÉ4', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(5, 'Boyer', 'Jean Pierre ', '0', '', '1776-02-15', 'JEBO5', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(6, 'Hérard', 'Rivière ', '0', '', '1789-02-16', 'RIHÉ6', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(7, 'Guerrier', 'Philippe ', '0', '', '1757-12-19', 'PHGU7', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(8, ' Pierrot', 'Jean-Louis', '0', '', '1761-00-00', 'JE P8', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, '2019-07-02 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(9, 'Riché', 'Jean-Baptiste ', '0', '', '1777-00-00', 'JERI9', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(10, 'Soulouque', 'Faustin ', '0', '', '1782-08-15', 'FASO10', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(11, 'Geffrard', 'Fabre Nicolas', '0', '', '1803-09-23', 'FAGE11', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(12, 'Salnave', 'Sylvain ', '0', '', '1826-02-07', 'SYSA12', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(13, 'Nissage-Saget', 'Jean-Nicolas', '0', '', '2010-10-10', 'JENI13', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(14, 'Domingue', 'Michel ', '0', '', '2010-10-10', 'MIDO14', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(15, 'Boisrond-Canal', 'Pierre Théomas', '0', '', '1832-06-12', 'PIBO15', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, '2019-07-02 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(16, 'Salomon', 'Lysius ', '0', '', '1815-06-30', 'LYSA16', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(17, ' Légitime', 'François D', '0', '', '1841-11-20', 'FR L17', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, '2019-08-07 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(18, 'Hyppolite', 'Louis Mondestin Florvil ', '0', '', '1828-03-02', 'LOHY18', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(19, 'Sam', 'Tiresias Simon', '0', '', '1835-05-15', 'TISA19', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(20, 'Alexis', 'Nord ', '0', '', '1820-08-02', 'NOAL20', 0, '', '', '', '', 31, '', NULL, NULL, NULL, NULL, NULL, '2019-08-07 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(21, 'Simon', 'Francois Antoine ', '0', '', '1943-10-10', 'FRSI21', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(22, 'Leconte', 'Cincinnatus ', '0', '', '1854-09-29', 'CILE22', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(23, 'Auguste', 'Tancrède ', '0', '', '1856-03-16', 'TAAU23', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(24, 'Oreste', 'Michel ', '0', '', '1859-04-08', 'MIOR24', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(25, 'Zamor', 'Oreste ', '0', '', '1861-00-00', 'ORZA25', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(26, 'Théodore', 'Davilmar ', '0', '', '1847-00-00', 'DATH26', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(27, 'Sam', 'Vilbrun Guillaume ', '0', '', '1860-00-00', 'VISA27', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(28, 'Dartiguenave', 'Sudre ', '0', '', '1862-04-06', 'SUDA28', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(29, 'Borno', 'Louis ', '0', '', '1860-09-29', 'LOBO29', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(30, 'Roy', 'Eugène ', '0', '', '1861-00-00', 'EURO30', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(31, 'Vincent', 'Sténio ', '0', '', '1874-02-22', 'STVI31', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(32, 'Lescot', 'Elie ', '0', '', '1889-12-03', 'ELLE32', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(33, ' Estimé', 'Dumarsais', '0', '5', '1900-04-21', 'DU E33', 0, '', '', '', '', 108, '', NULL, NULL, NULL, NULL, NULL, '2019-07-07 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(34, 'Magloire', 'Paul ', '0', '', '1907-07-17', 'PAMA34', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(35, 'Pierre-Louis', 'Nemours ', '0', '', '1900-10-24', 'NEPI35', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(36, 'Sylvain', 'Franck ', '0', '', '1909-08-03', 'FRSY36', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(37, ' Fignolé', 'Daniel', '0', '', '1913-00-00', 'DA F37', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, '2019-08-07 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(38, 'Duvalier', 'François ', '0', '', '1907-04-14', 'FRDU38', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(39, 'Duvalier', 'Jean-Claude ', '0', '', '1952-07-03', 'JEDU39', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(40, ' Manigat', 'Leslie', '0', '', '1930-08-16', 'LE M40', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, NULL, '2019-08-07 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(41, 'Namphy', 'Henri ', '0', '', '1932-11-02', 'HENA41', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(42, 'Avril', 'Prosper ', '0', '', '1937-12-12', 'PRAV42', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(43, 'Trouillot', 'Ertha P', '1', '', '1943-08-13', 'ERTR43', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(44, 'Aristide', 'Jean Bertrand ', '0', '', '1953-07-15', 'JEAR44', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(45, 'Moise', 'Jovenel', '0', '', '1968-06-26', 'JOMO45', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-07-02 00:00:00', '2019-07-02 00:00:00', NULL, NULL, 2, NULL, '', ''),
(46, 'Nérette', 'Joseph', '0', '', '1924-04-09', 'JONÉ46', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(47, 'Jonassaint', 'Emile', '0', '', '1913-05-13', 'EMJO47', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(48, 'Préval', 'René', '0', '', '1843-01-17', 'REPR48', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(49, 'Alexandre', 'Boniface', '0', '', '1936-07-31', 'BOAL49', 0, '', '', '', '', 44, '', NULL, NULL, NULL, NULL, NULL, '2019-08-07 00:00:00', 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(50, 'Martelly', 'Michel Joseph ', '0', '', '1961-02-12', 'MIMA50', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(51, 'Privert', 'Jocelerme ', '0', '', '1954-02-01', 'JOPR51', 0, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(52, 'Moravia', 'Charles', '0', '', '1876-06-17', 'CHMO52', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(53, 'Blot', 'Probus', '0', '', '1876-05-15', 'PRBL53', 1, '', '', '', '', NULL, '', '', '', '', NULL, NULL, '2019-08-31 00:00:00', 'siges_migration_tool', NULL, 1, 2, NULL, ''),
(54, 'Faubert', 'Ida', '1', '', '1883-02-14', 'IDFA54', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(55, 'Bayard', 'Othello', '0', '', '1885-09-02', 'OTBA55', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(56, 'Grimard', 'Luc', '0', '', '1886-00-00', 'LUGR56', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(57, 'Laleau', 'Léon', '0', '', '1892-08-03', 'LÉLA57', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(58, 'Jastram', 'Gervais', '0', '', '1899-08-12', 'GEJA58', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(59, 'Carriès-Lemaire', 'Emmeline', '1', '', '1899-06-30', 'EMCA59', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(60, 'Brouard', 'Carl', '0', '', '0000-00-00', 'CABR60', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(61, 'Roumer', 'Emile', '0', '', '0000-00-00', 'EMRO61', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(62, 'Marcellin', 'Philippe-Thoby', '0', '', '1904-00-00', 'PHMA62', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(63, 'Heurtelou', 'Daniel', '0', '', '1906-12-19', 'DAHE63', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(64, 'Roumain', 'Jacques', '0', '', '0000-00-00', 'JARO64', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(65, 'Lespès', 'Anthony', '0', '', '0000-00-00', 'ANLE65', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(66, 'Brierre', 'Jean Fernand', '0', '', '1909-00-00', 'JEBR66', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(67, 'Dauphin', 'Marcel', '0', '', '1910-07-23', 'MADA67', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(68, 'Martineau', 'Fernand', '0', '', '0000-00-00', 'FEMA68', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(69, 'Dorsinville', 'Roger', '0', '', '1911-00-00', 'RODO69', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(70, 'Saint-Aude', 'Clément Magloire', '0', '', '1912-00-00', 'CLSA70', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(71, 'Morisseau-Leroy', 'Félix', '0', '', '1912-00-00', 'FÉMO71', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(72, 'Chassagne', 'Roland', '0', '', '1912-08-18', 'ROCH72', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(73, 'Camille', 'Roussan', '0', '', '1912-08-27', 'ROCA73', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(74, 'Moise', 'Rodolphe', '0', '', '1914-00-00', 'ROMO74', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(75, 'Charles Bernard', 'Régnor', '0', '', '1915-10-18', 'RÉCH75', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(76, 'Fouché', 'Franck', '0', '', '1915-11-27', 'FRFO76', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(77, 'Belance', 'René', '0', '', '0000-00-00', 'REBE77', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(78, 'Garoute', 'Hamilton', '0', '', '1920-00-00', 'HAGA78', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(79, 'Laraque', 'Paul', '0', '', '1920-09-21', 'PALA79', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(80, 'Saint-Louis', 'Carlos', '0', '', '0000-00-00', 'CASA80', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(81, 'Lemoine', 'Lucien', '0', '', '1923-12-19', 'LULE81', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(82, 'Chassagne', 'Raymond', '0', '', '1924-00-00', 'RACH82', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(83, 'Philoctète', 'Raymond', '0', '', '1925-00-00', 'RAPH83', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(84, 'Pierre-Louis', 'Ulysse', '0', '', '1925-01-20', 'ULPI84', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(85, 'Depestre', 'René', '0', '', '1926-08-29', 'REDE85', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(86, 'Phelps', 'Anthony ', '0', '', '1928-00-00', 'ANPH86', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(87, 'Dorcely', 'Roland', '0', '', '1930-00-00', 'RODO87', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(88, 'Bajeux', 'Jean-Claude', '0', '', '1931-09-17', 'JEBA88', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(89, 'Philoctète', 'René', '0', '', '1932-11-16', 'REPH89', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(90, 'Beaugé-Rosier', 'Jacqueline', '1', '', '0000-00-00', 'JABE90', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(91, 'Castera', 'Georges', '0', '', '1936-12-27', 'GECA91', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(92, 'Davertige', 'Davertige', '0', '', '0000-00-00', 'DADA92', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(93, 'Etienne', 'Gérard', '0', '', '1936-05-28', 'GÉET93', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(94, 'Frank', 'Etienne', '0', '', '0000-00-00', 'ETFR94', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(95, 'Legagneur', 'Serge', '0', '', '0000-00-00', 'SELE95', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(96, 'Lerebours', 'Michel-Philippe', '0', '', '1933-00-00', 'MILE96', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(97, 'Magloire', 'Francis-Séjour', '0', '', '1940-00-00', 'FRMA97', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(98, 'Martineau', 'Jean-Claude', '0', '', '1937-00-00', 'JEMA98', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(99, 'Métellus', 'Jean', '0', '', '1937-00-00', 'JEMÉ99', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(100, 'Morisseau', 'Roland', '0', '', '1933-00-00', 'ROMO100', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(101, 'Paul', 'Cauvin L.', '0', '', '1938-00-00', 'CAPA101', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(102, 'Tavernier', 'Jamine', '1', '', '1935-03-23', 'JATA102', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, '102.jpeg', ''),
(103, 'Thénor', 'Auguste', '0', '', '1933-06-20', 'AUTH103', 1, '35, Rue Avril, Léogane', '', 'auguste@gmail.com', '', 11, 'Haitienne', 'Andréa', '5667777665', '876554544', NULL, NULL, '2019-07-06 00:00:00', 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(104, 'Laforest', 'Jean-Richard', '0', '', '1940-00-00', 'JELA104', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 0, 1, NULL, ''),
(105, 'Pierre', 'Claude C.', '0', '', '1941-00-00', 'CLPI105', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 0, 1, NULL, ''),
(106, 'Antoine', 'Yves', '0', '', '1941-00-00', 'YVAN106', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(107, 'Campfort', 'Gérard', '0', '', '0000-00-00', 'GÉCA107', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(108, 'Large', 'Josaphat-Robert', '0', '', '1942-12-15', 'JOLA108', 1, '', '', '', '', NULL, '', '', '', '', NULL, NULL, '2019-08-31 00:00:00', 'siges_migration_tool', NULL, 1, 2, NULL, ''),
(109, 'Lescouflair', 'Rony', '0', '', '1942-00-00', 'ROLE109', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(110, 'Badiguy-Gilbert', 'Serge', '0', '', '1943-07-19', 'SEBA110', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(111, 'Cavé', 'Syto', '0', '', '1944-00-00', 'SYCA111', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(112, 'Charlier', 'Jacques', '0', '', '1944-00-00', 'JACH112', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(113, 'Calvin', 'Jean-Max', '0', '', '1945-00-00', 'JECA113', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(114, 'Assali', 'Donald', '0', '', '1945-00-00', 'DOAS114', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(115, 'Jean', 'Yanick', '0', '', '1946-05-25', 'YAJE115', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(116, 'Ejèn', 'Manno', '0', '', '0000-00-00', 'MAEJ116', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(117, 'Ménard', 'Guy-Gérald', '0', '', '1947-00-00', 'GUMÉ117', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(118, 'Berrouet-Oriol', 'Robert', '0', '', '1951-00-00', 'ROBE118', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(119, 'Brisson', 'Richard', '0', '', '1951-00-00', 'RIBR119', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(120, 'Charles', 'Christophe', '0', '', '1951-04-29', 'CHCH120', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(121, 'Charles ', 'Jean-Claude', '0', '', '1949-10-20', 'JECH121', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(122, 'Gousse', 'Edgard', '0', '', '1950-00-00', 'EDGO122', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(123, 'Laforest', 'Jean-Richard', '0', '', '1940-00-00', 'JELA123', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(124, 'Large', 'Josaphat-Robert', '0', '', '1942-12-15', 'JOLA124', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(125, 'Lescouflair', 'Rony', '0', '', '1942-00-00', 'ROLE125', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 0, 2, NULL, ''),
(126, 'Madhère', 'Serge', '0', '', '1950-08-12', 'SEMA126', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(127, 'Muller', 'Rudolph', '0', '', '1950-12-10', 'RUMU127', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(128, 'Pierre', 'Claude C.', '0', '', '1941-00-00', 'CLPI128', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(129, 'Hyppolite', 'Michel-Ange', '0', '', '1952-00-00', 'MIHY129', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(130, 'Manuel', 'Robert', '0', '', '1953-02-18', 'ROMA130', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(131, 'Trouillot', 'Evelyne', '1', '', '1954-00-00', 'EVTR131', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(132, 'Agnant', 'Marie-Célie', '1', '', '1954-00-00', 'MAAG132', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(133, 'Noel', 'Lochard', '0', '', '1955-08-29', 'LONO133', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(134, 'Soukar', 'Michel', '0', '', '1955-08-22', 'MISO134', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(135, 'Saint-Natus', 'Clotaire', '0', '', '1956-04-26', 'CLSA135', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(136, 'Trouillot', 'Lyonel', '0', '', '1956-00-00', 'LYTR136', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(137, 'Guichard', 'Marie-Claude', '1', '', '1957-05-09', 'MAGU137', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(138, 'Mars', 'Kettly P.', '1', '', '1958-00-00', 'KEMA138', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(139, 'Satyre', 'Joubert ', '0', '', '1958-00-00', 'JOSA139', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(140, 'Augustin', 'Gary ', '0', '', '1958-00-00', 'GAAU140', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(141, 'Narcisse', 'Pierre-Richard', '0', '', '1961-06-19', 'PINA141', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(142, 'Roche', 'Jacques', '0', '', '1961-07-21', 'JARO142', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(143, 'Exavier', 'Marc', '0', '', '1962-00-00', 'MAEX143', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(144, 'Batraville', 'Dominique', '0', '', '1962-00-00', 'DOBA144', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(145, 'Dalembert', 'Louis-Philippe', '0', '', '1962-08-12', 'LODA145', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(146, 'Saint-Eloi', 'Rodney', '0', '', '1963-09-27', 'ROSA146', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(147, 'Henriquez', 'Pradel', '0', '', '1964-10-09', 'PRHE147', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(148, 'Dugé', 'Jean Amorce', '0', '', '1964-00-00', 'JEDU148', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(149, 'Edouard', 'Willems', '0', '', '1965-04-23', 'WIED149', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(150, 'Milce', 'Jean Ephèle', '0', '', '1969-00-00', 'JEMI150', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(151, 'Lhérisson', 'Farah-Martine', '1', '', '1970-11-30', 'FALH151', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(152, 'Prophète', 'Emmelie', '1', '', '1971-06-15', 'EMPR152', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(153, 'Fouad', 'André', '0', '', '1972-02-05', 'ANFO153', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(154, 'Auguste', 'Bonel', '0', '', '1973-06-03', 'BOAU154', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', 'admin', 1, NULL, NULL, ''),
(155, 'Blanc', 'Jean-Claude', '0', '', NULL, 'JEBL155', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(156, 'Blanco', 'Dionisio', '0', '', NULL, 'DIBL156', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(157, 'Bonaventure', 'Jean Louis', '0', '', NULL, 'JEBO157', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(158, 'Carrier', 'Clausel', '0', '', NULL, 'CLCA158', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(159, 'Charles', 'Ersnt-Jean', '0', '', NULL, 'ERCH159', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(160, 'Constant', 'Presler', '0', '', NULL, 'PRCO160', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(161, 'Coutard', 'Gabriel', '0', '', NULL, 'GACO161', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(162, 'Dambreville', 'Claude', '0', '', NULL, 'CLDA162', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(163, 'Day', 'Jackson', '0', '', NULL, 'JADA163', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(164, 'Delinois', 'Amerlin', '0', '', NULL, 'AMDE164', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, '164.jpg', ''),
(165, 'Desarmes', 'Georges', '0', '', NULL, 'GEDE165', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(166, 'Dominique', 'Clotaire', '0', '', NULL, 'CLDO166', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(167, 'Dubic', 'Nelson', '0', '', NULL, 'NEDU167', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(168, 'Dreux', 'Nicolas', '0', '', NULL, 'NIDR168', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(169, 'Duffaut', 'Préfète', '0', '', NULL, 'PRDU169', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(170, 'Prosper', 'Eriveaux', '0', '', NULL, 'ERPR170', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(171, 'Exil', 'Levoy', '0', '', NULL, 'LEEX171', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(172, 'Fortuné', 'Gérard', '0', '', NULL, 'GÉFO172', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(173, 'Henry', 'Calixte', '0', '', NULL, 'CAHE173', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(174, 'Hilome', 'José', '0', '', NULL, 'JOHI174', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(175, 'Jean-Baptiste', 'Waking', '0', '', NULL, 'WAJE175', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(176, 'Jean-Louis', 'Eric', '0', '', NULL, 'ERJE176', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(177, 'Jean-Louis', 'Henri', '0', '', NULL, 'HEJE177', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(178, 'Jean-Louis', 'Maxan', '0', '', NULL, 'MAJE178', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(179, 'Joseph', 'Emmanuel', '0', '', NULL, 'EMJO179', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(180, 'Joseph', 'Lesly', '0', '', NULL, 'LEJO180', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(181, 'Joseph', 'Raymond', '0', '', NULL, 'RAJO181', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(182, 'Joseph', 'Reynald', '0', '', NULL, 'REJO182', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(183, 'Labbe', 'Serge', '0', '', NULL, 'SELA183', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(184, 'Lobbe', 'Arnoux', '0', '', NULL, 'ARLO184', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(185, 'Louizor', 'Ernst', '0', '', NULL, 'ERLO185', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(186, 'Lumarc', 'Evans', '0', '', NULL, 'EVLU186', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(187, 'Magloire', 'Magda', '1', '', NULL, 'MAMA187', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(188, 'Merise', 'Fritz', '0', '', NULL, 'FRME188', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(189, 'Onel', 'ONEL', '0', '', NULL, 'ONON189', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(190, 'Paul', 'Dieuseul', '0', '', NULL, 'DIPA190', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(191, 'Augustin', 'Pierre-Sylvain', '0', '', NULL, 'PIAU191', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(192, 'Petizil', 'Durand', '0', '', NULL, 'DUPE192', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(193, 'Richard', 'Nesly', '0', '', NULL, 'NERI193', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(194, 'Riché', 'Pierre-Louis', '0', '', NULL, 'PIRI194', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(195, 'Rousseau', 'Denis', '0', '', NULL, 'DERO195', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(196, 'Saint-Fleurant', 'Louisiane', '0', '', NULL, 'LOSA196', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(197, 'Saint-Jean', 'Harold', '0', '', NULL, 'HASA197', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(198, 'Seide', 'Jean Adrien', '0', '', NULL, 'JESE198', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(199, 'Semerand', 'Galland', '0', '', NULL, 'GASE199', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(200, 'Siméon', 'Michel', '0', '', NULL, 'MISI200', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(201, 'Garoute', 'Jean-Claude', '0', '', NULL, 'JEGA201', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(202, 'Walton', 'Jean', '0', '', NULL, 'JEWA202', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(203, 'Zachari', 'Pierre', '0', '', NULL, 'PIZA203', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(204, 'Marc', 'Yucca', '1', '', NULL, 'YUMA204', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(205, 'Hilaire', 'Kira', '1', '', NULL, 'KIHI205', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(206, 'Prosper', 'Kitty', '1', '', NULL, 'KIPR206', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(207, 'Poulard', 'Isabelle', '1', '', NULL, 'ISPO207', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(208, 'Possible', 'Alain', '0', '', NULL, 'ALPO208', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, '208.jpg', ''),
(209, 'Potter', 'Harry', '0', '', NULL, 'HAPO209', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(210, 'Granger', 'Hermione', '1', '', '2016-08-04', 'HEGR210', 1, '', '', '', '', 1, '', '', '', '', NULL, NULL, '2019-08-03 00:00:00', 'siges_migration_tool', NULL, 1, 1, 'Hermione_Granger.jpg', ''),
(211, 'Weasley', 'Ronald', '0', '', NULL, 'ROWE211', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(212, 'Malefoy', 'Drago', '0', '', NULL, 'DRMA212', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(213, 'Marc', 'Jean-Baptiste', '0', '', NULL, 'JEMA213', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(214, 'Poulard', 'Jean Came', '0', '', NULL, 'JEPO214', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(215, 'Prosper', 'Metuschael', '0', '', NULL, 'MEPR215', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(216, 'Hilaire', 'Eder', '0', '', NULL, 'EDHI216', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(218, 'Poulard', 'Jean Came', '0', '', '0000-00-00', 'JEPO218', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(219, 'Charles', 'ROSS TESSY SAMMANTHA', '1', '', '0000-00-00', 'ROCH219', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(220, 'bataille', 'harold', '0', '', '0000-00-00', 'HABA220', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(221, 'Atis', 'Murielle', '1', '', '0000-00-00', 'MUAT221', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(222, 'Poulard', 'Gabrielle', '1', '', '0000-00-00', 'GAPO222', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(223, 'Jean', 'Ronald', '0', '', '0000-00-00', 'ROJE223', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(224, 'Charles', 'Fania', '1', '', '0000-00-00', 'FACH224', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(225, 'Borgelin', 'Clifford', '0', '2', '0000-00-00', 'CLBO225', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-25 00:00:00', NULL, NULL, 2, NULL, '', ''),
(226, 'JOSEPH', 'JEAN', '0', '', '0000-00-00', 'JEJO226', 0, '', '7184213349', 'icdhhaiti@gmail.com', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(227, 'Viral', 'Johnson', '0', '', '0000-00-00', 'JOVI227', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(228, 'Poulard', 'Jeanne', '1', '', '0000-00-00', 'JEPO228', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(229, 'Charles', 'Tara', '1', '', '0000-00-00', 'TACH229', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(230, 'Désir', 'Anglade', '0', '', '0000-00-00', 'ANDÉ230', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(231, 'Covington', 'Jovenul', '0', '', '0000-00-00', 'JOCO231', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(232, 'Charles', 'Geraldine', '1', '', '0000-00-00', 'GECH232', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(233, 'Desir', 'Cassandra', '1', '', '2017-08-07', 'CADE233', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(234, 'JOSEPH', 'Carmelite', '1', '', '0000-00-00', 'CAJO234', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(235, 'noel', 'david', '0', '', '0000-00-00', 'DANO235', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(236, 'Francois', 'Beatrice', '1', '', '0000-00-00', 'BEFR236', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(237, 'laguerre', 'jacob', '0', '', '2007-08-15', 'JALA237', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-05 00:00:00', '2019-08-06 00:00:00', NULL, 'admin', 1, 1, '', ''),
(238, 'Jacques', 'Vanessa', '1', '', '0000-00-00', 'VAJA238', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-06 00:00:00', '2019-08-06 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(239, 'JACQUES', 'Mergens', '0', '', '0000-00-00', 'MEJA239', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-06 00:00:00', '2019-08-06 00:00:00', NULL, 'admin', 1, NULL, '', ''),
(240, 'Sabine', 'Frédéric', '1', '3', NULL, 'FRSA240', 1, '', '+509 34 32 23 32', NULL, '', 67, '', NULL, NULL, NULL, NULL, '2019-08-10 00:00:00', NULL, 'SIGES', NULL, 2, NULL, NULL, ''),
(241, 'Poulard', 'Gabriel', '0', '', '2009-03-03', 'GAPO241', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '241.jpg', ''),
(242, 'Sainjour', 'Wilna', '1', '', '2007-07-10', 'WISA242', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(243, 'LOUIS', 'Annedjee Mitchellina', '1', '', '2007-11-23', 'AMLO243', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '243.jpg', ''),
(244, 'Jean', 'Vanessa', '1', '', '2000-05-02', 'VAJE244', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '244.jpg', ''),
(245, 'LOUIS', 'Hadassa Klaoudjinn', '1', '', '2008-02-19', 'HKLO245', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 0, 4, '245.jpg', ''),
(246, 'Guillaume', 'Carl', '0', '', '2012-01-04', 'CAGU246', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(247, 'CONDE', 'Jeffery', '0', '1', '2010-08-18', 'JECO247', 1, '', '', '', '', 111, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 1, 1, '', ''),
(248, 'Louis', 'Jameson', '0', '', '2001-12-15', 'JALO248', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '248.jpg', ''),
(249, 'St Juste', 'Santia', '1', '', '2010-09-28', 'SASJ249', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(250, 'Pierre', 'Emile', '0', '', '2009-06-10', 'EMPI250', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '250.jpg', ''),
(251, 'CONDE', 'Margalita', '1', '', '2010-01-13', 'MACO251', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(252, 'Etienne', 'Pipo', '0', '', '2009-04-08', 'PIET252', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 0, 4, '', ''),
(253, 'Emile', 'Francois', '0', '', '1973-03-08', 'FREM253', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 1, NULL, '', ''),
(254, 'Junette', 'LEONARD', '1', '', '1980-05-18', 'LEJU254', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(255, 'Néard', 'Angelo', '0', '', '0000-00-00', 'ANNE255', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(256, 'Roger Meyer', 'TIBULLE', '0', '', '1967-08-23', 'TIRM256', 0, '', '50937801234', '', '0031236987', 16, 'Haitienne', NULL, NULL, NULL, NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(257, 'Charles', 'Sandra', '1', '', '0000-00-00', 'SACH257', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(258, 'Joseph', 'Garry', '0', '', '2000-05-02', 'GAJO258', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(259, 'Frack', 'Gaston', '0', '', '1968-09-10', 'GAFR259', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 2, NULL, '', ''),
(260, 'LOUIS', 'Vilner', '0', '', '1985-08-23', 'VILO260', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL, 1, 2, '', ''),
(261, 'ADONIS', 'Sasmide', '1', '', '2013-08-30', 'SAAD261', 1, '', '', '', '', 131, '', 'Judith', '', '', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL, 2, NULL, '', ''),
(262, 'ADONIS', 'Pascal', '0', '', '2013-07-20', 'PAAD262', 1, '21, Rue Bourjolly, Fontamara 27', '', '', '', 131, '', '', '', '', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL, 2, NULL, '262.jpg', ''),
(263, 'gabriel', 'Jacques', '0', '', '2004-06-08', 'JAGA263', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL, 2, NULL, '263.jpg', ''),
(264, 'Victor', 'Sabruna', '1', '', '2010-05-10', 'SAVI264', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL, 2, NULL, '', ''),
(265, 'Reynald', 'Sabruna', '1', '', '2005-10-15', 'SARE265', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL, 2, NULL, '', ''),
(266, 'GEORGES', 'Maryse', '1', '1', '2005-05-11', 'MAGE266', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(267, 'JACQUES', 'Coralie', '1', '', '0000-00-00', 'COJA267', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(268, 'SYLVAIN', 'Judith', '1', '', '0000-00-00', 'JUSY268', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(269, 'JEAN CHARLES', 'Lorie', '1', '', '2011-09-05', 'LOJC269', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 1, NULL, '', ''),
(270, 'MATHUTIN', 'Johanne', '1', '', '2012-09-10', 'JOMA270', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(271, 'PIERRE', 'Titus', '0', '', '0000-00-00', 'TIPI271', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(272, 'PHYCIEN', 'Natacha', '1', '', '0000-00-00', 'NAPH272', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', '');
INSERT INTO `persons` (`id`, `last_name`, `first_name`, `gender`, `blood_group`, `birthday`, `id_number`, `is_student`, `adresse`, `phone`, `email`, `nif_cin`, `cities`, `citizenship`, `mother_first_name`, `identifiant`, `matricule`, `paid`, `date_created`, `date_updated`, `create_by`, `update_by`, `active`, `inactive_reason`, `image`, `comment`) VALUES
(273, 'VILBRUN', 'Sarah', '1', '', '0000-00-00', 'SAVI273', 0, '', '', '', '', NULL, '', NULL, NULL, NULL, NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(274, 'CHARLES', 'Guilene', '1', '', '1988-09-11', 'GUCH274', 0, '', '', '', '', 8, '', NULL, NULL, NULL, NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL, 2, NULL, '', ''),
(275, 'ANDRE ', 'Duddley-Fils', '0', '', '0000-00-00', 'DUAN275', 1, '', '', '', '', NULL, '', '', '', '', NULL, NULL, '2019-09-06 00:00:00', 'siges_migration_tool', NULL, 1, 2, NULL, ''),
(276, 'ANTOINE ', 'Jean Marc Lewis', '0', '', NULL, 'JMAN276', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(277, 'ARISTIDE ', 'Youri Stravinski I.', '0', '', NULL, 'YSAR277', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(278, 'ASTREMON ', 'Johnsley', '0', '', NULL, 'JOAS278', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(279, 'AUGUSTIN  ', 'Hutchins Guervensky', '0', '', NULL, 'HGAU279', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(280, 'BONAMY ', 'Christie Marie Chelsie ', '1', '', NULL, 'CMBO280', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(281, 'BOURSIQUOT ', 'Julmène', '1', '', NULL, 'JUBO281', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(282, 'CANGE ', 'Rood-Christopher', '0', '', NULL, 'ROCA282', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(283, 'CHARLES ', 'Marie Christie', '1', '', NULL, 'MCCH283', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(284, 'CHARLES ', 'Samuel Peterson', '0', '', NULL, 'SPCH284', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(285, 'CHENET ', 'Marvin Dudley ', '0', '', NULL, 'MDCH285', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(286, 'CHEREMOND ', 'Laurenson', '0', '', NULL, 'LACH286', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(287, 'DACIER ', 'Yvenson', '0', '', NULL, 'YVDA287', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(288, 'DESY ', 'Tchailovsky Westember ', '0', '', NULL, 'TWDE288', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(289, 'DIEUDONNE ', 'Jiverson', '0', '', NULL, 'JIDI289', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(290, 'DORT Louis ', 'Dodley', '0', '', NULL, 'DODL290', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(291, 'DORVIL', ' Arnhem Lorentz', '0', '', NULL, 'ADO291', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(292, 'DORVIL', ' Lisna ', '1', '', NULL, 'LDO292', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(293, 'DUPERVAL ', 'Kervens', '0', '', NULL, 'KEDU293', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(294, 'DUVERT ', 'Lourdes w.', '1', '', NULL, 'LWDU294', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(295, 'ESPERANCE ', 'Jonathan', '0', '', NULL, 'JOES295', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(296, 'FILIUS', ' Bwadley', '0', '', NULL, 'BFI296', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(297, 'FLECHE ', 'Jose ', '0', '', NULL, 'JOFL297', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(298, 'FRAGILAS ', 'Jules-Nader', '0', '', NULL, 'JUFR298', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(299, 'GERE ', 'Zeroberto', '0', '', NULL, 'ZEGE299', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(300, 'GOURDET ', 'Fredlyn', '0', '', NULL, 'FRGO300', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(301, 'JEAN ', 'Alan Joey ', '0', '', NULL, 'AJJE301', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(302, 'JEAN-Fils ', 'Mitsouka', '1', '', NULL, 'MIJE302', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(303, 'JEAN-PIERRE ', 'Chrisley', '1', '', NULL, 'CHJE303', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(304, 'JOCELYN ', 'Steeve ', '0', '', NULL, 'STJO304', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(305, 'JOSEPH  ', 'Anickson', '0', '', NULL, 'ANJO305', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(306, 'JOSEPH', ' Bleck Dawensky ', '0', '', NULL, 'BJO306', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(307, 'JOSEPH ', 'Evaninson', '0', '', NULL, 'EVJO307', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(308, 'JOSEPH ', 'Kenley ', '0', '', NULL, 'KEJO308', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(309, 'JOSEPH ', 'Winsley', '0', '', NULL, 'WIJO309', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(310, 'LAINE', ' Cleeford Ralf Yves Gar ', '0', '', NULL, 'CLA310', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(311, 'LAVEUS ', 'Sebastien', '0', '', NULL, 'SELA311', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(312, 'LEOME ', 'Bodina Holding ', '1', '', NULL, 'BHLE312', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(313, 'LOUIS ', 'Adler Volsky', '0', '', NULL, 'AVLO313', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(314, 'MONTEAU ', 'Dave', '0', '', NULL, 'DAMO314', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(315, 'MORAND ', 'Antherton Emmanuel Fils ', '0', '', NULL, 'AEMO315', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(316, 'MYRTHYL ', 'Miss Esperancia', '1', '', NULL, 'MEMY316', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(317, 'NAUSSE', ' Frantzy', '0', '', NULL, 'FNA317', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(318, 'NOBERT', ' Jn Gardy', '0', '', NULL, 'JNO318', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(319, 'PAUL ', ' Johnny', '0', '', NULL, 'JPA319', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(320, 'PAUL ', 'Amelus', '0', '', NULL, 'AMPA320', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(321, 'PETIOTE ', 'Neyssa ', '1', '', NULL, 'NEPE321', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(322, 'RAYMOND ', 'Rikelmey', '0', '', NULL, 'RIRA322', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(323, 'REMEVIL ', 'Marc Elton', '0', '', NULL, 'MERE323', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(324, 'RICHARD ', 'Sterline', '1', '', NULL, 'STRI324', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(325, 'SAIMPAS ', 'Herby', '0', '', NULL, 'HESA325', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(326, 'ST CLAIR', ' Gallardo ', '0', '', NULL, 'GSC326', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(327, 'TELLY ', 'Emmanuel', '0', '', NULL, 'EMTE327', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(328, 'THELUSMA ', 'Leonardo', '0', '', NULL, 'LETH328', 1, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, 'siges_migration_tool', NULL, 1, NULL, NULL, ''),
(329, 'PIERRE', 'André', '0', '', '0000-00-00', 'ANPI329', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL, 2, NULL, '', ''),
(330, 'LAGUERRE', 'Mackendy', '0', '', '0000-00-00', 'MALA330', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL, 2, NULL, '', ''),
(331, 'MOREAU', 'Raphaella', '1', '', '0000-00-00', 'RAMO331', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL, 2, NULL, '', ''),
(332, 'BELFORT', 'Michaella', '1', '', '0000-00-00', 'MIBE332', 1, '', '', '', '', NULL, '', '', '', '', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL, 2, NULL, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `persons_has_titles`
--

CREATE TABLE `persons_has_titles` (
  `persons_id` int(11) NOT NULL,
  `titles_id` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `persons_has_titles`
--

INSERT INTO `persons_has_titles` (`persons_id`, `titles_id`, `academic_year`) VALUES
(33, 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `person_history`
--

CREATE TABLE `person_history` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `entry_hire_date` datetime DEFAULT NULL,
  `leaving_date` datetime NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `disable_date` datetime NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `profil` varchar(32) NOT NULL,
  `job_status_name` varchar(100) NOT NULL,
  `last_level_name` varchar(70) DEFAULT NULL,
  `academic_year` varchar(70) NOT NULL,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `person_history`
--

INSERT INTO `person_history` (`id`, `person_id`, `entry_hire_date`, `leaving_date`, `reason`, `disable_date`, `title`, `profil`, `job_status_name`, `last_level_name`, `academic_year`, `created_by`) VALUES
(1, 53, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Expulsion', '2019-07-05 00:00:00', NULL, 'Admin', '', 'Septième', '2018-2019', 'admin'),
(2, 210, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Abandon', '2019-07-23 00:00:00', NULL, 'Admin', '', 'Section des Petits', '2018-2019', 'admin'),
(3, 104, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Abandon', '2019-07-24 00:00:00', NULL, 'Admin', '', 'Neuvième', '2018-2019', 'admin'),
(4, 105, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Abandon', '2019-07-24 00:00:00', NULL, 'Admin', '', 'Neuvième', '2018-2019', 'admin'),
(5, 108, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Expulsion', '2019-07-24 00:00:00', NULL, 'Admin', '', 'Neuvième', '2018-2019', 'admin'),
(6, 125, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Expulsion', '2019-07-24 00:00:00', NULL, 'Admin', '', 'Neuvième', '2018-2019', 'admin'),
(7, 237, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Abandon', '2019-08-06 00:00:00', NULL, 'Admin', '', 'Septième', '2018-2019', 'admin'),
(8, 260, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Expulsion', '2019-08-31 00:00:00', NULL, 'Admin', '', 'Neuvième Année', '2019-2020', 'admin'),
(9, 247, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Abandon', '2019-08-31 00:00:00', NULL, 'Admin', '', 'Neuvième Année', '2019-2020', 'admin'),
(10, 245, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Voyage', '2019-08-31 00:00:00', NULL, 'Admin', '', 'Neuvième Année', '2019-2020', 'admin'),
(11, 275, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Expulsion', '2019-09-06 00:00:00', NULL, 'Admin', '', 'Secondaire 3', '2019-2020', 'admin'),
(12, 252, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'Voyage', '2019-09-13 00:00:00', NULL, 'Admin', '', 'Neuvième Année', '2019-2020', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `postulant`
--

CREATE TABLE `postulant` (
  `id` int(11) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `blood_group` varchar(10) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `cities` int(11) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(128) DEFAULT NULL,
  `citizenship` varchar(128) DEFAULT NULL,
  `health_state` varchar(255) DEFAULT NULL,
  `person_liable` varchar(100) DEFAULT NULL,
  `person_liable_phone` varchar(65) DEFAULT NULL,
  `person_liable_adresse` varchar(255) DEFAULT NULL,
  `person_liable_relation` int(11) DEFAULT NULL,
  `apply_for_level` int(11) NOT NULL,
  `previous_level` int(11) DEFAULT NULL,
  `previous_school` varchar(255) DEFAULT NULL,
  `school_date_entry` date DEFAULT NULL,
  `last_average` double DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `academic_year` int(11) DEFAULT NULL,
  `is_validate` tinyint(1) DEFAULT NULL,
  `is_online` tinyint(1) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `postulant`
--

INSERT INTO `postulant` (`id`, `first_name`, `last_name`, `gender`, `blood_group`, `birthday`, `cities`, `adresse`, `phone`, `email`, `citizenship`, `health_state`, `person_liable`, `person_liable_phone`, `person_liable_adresse`, `person_liable_relation`, `apply_for_level`, `previous_level`, `previous_school`, `school_date_entry`, `last_average`, `status`, `academic_year`, `is_validate`, `is_online`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Albert', 'Buron', '0', '4', NULL, 58, '45, Rue de Champs de Mars', '+509 34 32 23 32', NULL, NULL, '', 'Gary Victor', '+509 34 32 23 32', '', 7, 6, 5, '', '2019-07-05', NULL, NULL, 1, NULL, NULL, '2019-07-05 00:00:00', NULL, 'admin', NULL),
(2, 'Albert', 'Buron', '0', '2', NULL, 122, '', '', NULL, NULL, '', '', '', '', 3, 3, 2, '', '2019-07-06', NULL, NULL, 1, NULL, NULL, '2019-07-06 00:00:00', NULL, 'admin', NULL),
(4, 'Catherine', 'FLon', '0', '3', '2019-07-20', 67, '13 Rue Nicolas', '463456789', 'jcpoulard@gmail.com', 'Haitienne', NULL, 'Hyppolite Saint Jean', '31068691', NULL, NULL, 2, NULL, 'College H Hebert', '0000-00-00', NULL, 0, 1, 1, 0, '2019-07-31 22:14:10', '2019-08-07 00:00:00', 'Guest', 'admin'),
(5, 'Val', 'Jean', '0', '3', '2019-07-20', 67, '13 Rue Nicolas', '463456789', 'jcpoulard@gmail.com', 'Haitienne', NULL, 'Hyppolite Saint Jean', '31068691', NULL, NULL, 2, NULL, 'College H Hebert', '0000-00-00', NULL, 0, 1, 1, 0, '2019-07-31 22:15:50', '2019-08-07 00:00:00', 'Guest', 'admin'),
(6, 'Marie Jeanne', 'Picard', '1', '1', '2003-09-05', 8, '13 Rue Nicolas', '46092345', 'jesuismoined@gmail.com', 'Haitienne', NULL, 'Joseph Picard', '36900000', NULL, NULL, 10, NULL, 'Ecole Jacquues', NULL, NULL, 1, 6, 1, 1, '2019-09-05 14:20:26', '2019-09-05 14:25:18', 'admin', 'admin'),
(7, 'Junior', 'Dor', '0', '3', '2004-04-13', 101, '45, Rue André, Croix-des-Bouquets', '+509252523', 'djemsyetienne1987@gmail.com', 'haitienne', NULL, 'Janvier André', '+509252523', NULL, NULL, 3, NULL, 'Shalom', NULL, NULL, 1, 6, 1, 1, '2019-09-11 16:59:10', '2019-09-11 17:03:31', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `stock_alert` int(11) NOT NULL DEFAULT '0',
  `is_forsale` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` int(11) NOT NULL,
  `profil_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `profil_name`) VALUES
(1, 'Admin'),
(2, 'Manager'),
(3, 'Billing'),
(4, 'Teacher'),
(5, 'Guest'),
(6, 'Reporter'),
(7, 'Information');

-- --------------------------------------------------------

--
-- Table structure for table `profil_has_modules`
--

CREATE TABLE `profil_has_modules` (
  `id` int(11) NOT NULL,
  `profil_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profil_has_modules`
--

INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES
(1, 1, 1),
(2, 1, 5),
(3, 1, 6),
(4, 1, 7),
(5, 1, 8),
(6, 1, 9),
(7, 1, 10),
(8, 2, 1),
(9, 2, 5),
(10, 2, 6),
(11, 2, 7),
(12, 2, 9),
(13, 2, 10),
(14, 3, 5),
(15, 3, 6),
(16, 3, 8),
(17, 4, 5),
(18, 4, 6),
(19, 4, 7),
(20, 4, 9),
(21, 5, 5),
(22, 5, 10),
(23, 6, 5),
(24, 6, 6),
(25, 1, 11),
(26, 2, 11),
(27, 2, 8),
(28, 3, 7),
(29, 3, 9),
(30, 1, 12),
(31, 2, 12),
(32, 4, 8),
(33, 7, 7),
(34, 7, 9),
(35, 7, 12),
(36, 7, 6),
(37, 7, 8),
(38, 7, 5),
(39, 3, 1),
(40, 3, 11),
(41, 3, 12),
(42, 6, 1),
(43, 6, 7),
(44, 6, 8),
(45, 6, 9),
(46, 6, 11),
(47, 6, 12),
(48, 7, 1),
(49, 7, 11),
(50, 1, 13),
(51, 2, 12),
(52, 3, 13),
(53, 1, 14),
(55, 2, 14),
(56, 3, 14),
(57, 4, 14),
(58, 5, 14),
(59, 6, 14),
(60, 7, 14);

-- --------------------------------------------------------

--
-- Table structure for table `qualifications`
--

CREATE TABLE `qualifications` (
  `id` int(11) NOT NULL,
  `qualification_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `qualifications`
--

INSERT INTO `qualifications` (`id`, `qualification_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Licence en Sciences de l\'Education ', '2019-08-10 00:00:00', NULL, NULL, NULL),
(2, 'Diplôme de Sécrétariat ', '2019-08-10 00:00:00', NULL, NULL, NULL),
(3, 'Diplôme en Technique Informatique', '2019-08-10 00:00:00', NULL, NULL, NULL),
(4, 'Licence en Sciences Informatiques', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(5, 'Licence en Sciences Comptables', '2019-08-10 00:00:00', NULL, NULL, NULL),
(6, 'Licence en Sciences Infirmières', '2019-08-10 00:00:00', NULL, NULL, NULL),
(7, 'Certificat de Fin d\'études en Auto-Ecole', '2019-08-10 00:00:00', NULL, NULL, NULL),
(8, 'Normalien Niveau Fondamental', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(9, 'Normalien Niveau Supérieur', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `raise_salary`
--

CREATE TABLE `raise_salary` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `raising_date` date NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_checkout`
--

CREATE TABLE `record_checkout` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_record` datetime NOT NULL,
  `presence_type` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `create_by` varchar(64) NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_infraction`
--

CREATE TABLE `record_infraction` (
  `id` bigint(20) NOT NULL,
  `student` int(20) NOT NULL,
  `infraction_type` int(11) NOT NULL,
  `record_by` varchar(64) NOT NULL,
  `incident_date` date NOT NULL,
  `academic_period` int(11) DEFAULT NULL,
  `exam_period` int(11) DEFAULT NULL,
  `incident_description` text NOT NULL,
  `decision_description` text,
  `value_deduction` float DEFAULT NULL,
  `general_comment` text,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record_infraction`
--

INSERT INTO `record_infraction` (`id`, `student`, `infraction_type`, `record_by`, `incident_date`, `academic_period`, `exam_period`, `incident_description`, `decision_description`, `value_deduction`, `general_comment`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 145, 4, 'Emile Jonassaint', '2018-10-09', 1, NULL, 'C\'est un flanneur', '', 5, '', '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(2, 75, 2, 'Henri  Namphy', '2018-11-22', 1, NULL, 'blalala', '', 10, '', '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(3, 59, 2, 'Joseph Nérette', '2018-12-29', 1, NULL, 'Avant que ne débute la 15e édition de la Gold Cup, les Grenadiers, qui feront leurs grands débuts face aux Bermudes le 16 juin à San Jose (Costa Rica), terminent leur préparation sur une bonne note. Ils ont gagné contre Guyana (3-1), lundi 10 juin, leur ultime match amical. ', '', 0, '', '2019-07-03', '0000-00-00', 'admin', ''),
(4, 121, 5, 'Elie  Lescot', '2019-01-30', 1, NULL, 'Après deux revers subis face au Salvador, le 2 juin (0-1) et contre le Chili, le 6 juin (1-2), les Grenadiers ont bien réagi face à Guyana, un adversaire visiblement faible par rapport aux deux premiers rivaux d’Haïti en amical.', '', 5, '', '2019-07-03', '0000-00-00', 'admin', ''),
(5, 58, 7, 'Davilmar  Théodore', '2018-11-15', 1, NULL, 'près deux revers subis face au Salvador, le 2 juin (0-1) et contre le Chili, le 6 juin (1-2), les Grenadiers ont bien réagi face à Guyana, un adversaire visiblement faible par rapport aux deux premiers rivaux d’Haïti en amical', '', 100, '', '2019-07-03', '0000-00-00', 'admin', ''),
(6, 57, 13, 'Elie  Lescot', '2018-12-12', 1, NULL, 'près deux revers subis face au Salvador, le 2 juin (0-1) et contre le Chili, le 6 juin (1-2), les Grenadiers ont bien réagi face à Guyana, un adversaire visiblement faible par rapport aux deux premiers rivaux d’Haïti en amical', '', 0, '', '2019-07-03', '0000-00-00', 'admin', ''),
(7, 115, 14, 'Paul  Magloire', '2019-01-18', 1, 3, 'près deux revers subis face au Salvador, le 2 juin (0-1) et contre le Chili, le 6 juin (1-2), les Grenadiers ont bien réagi face à Guyana, un adversaire visiblement faible par rapport aux deux premiers rivaux d’Haïti en amical', '', 20, '', '2019-07-03', '2019-07-06', 'admin', 'admin'),
(8, 123, 3, 'Faustin  Soulouque', '2019-03-20', 1, NULL, 'près deux revers subis face au Salvador, le 2 juin (0-1) et contre le Chili, le 6 juin (1-2), les Grenadiers ont bien réagi face à Guyana, un adversaire visiblement faible par rapport aux deux premiers rivaux d’Haïti en amical', '', 5, '', '2019-07-03', '0000-00-00', 'admin', ''),
(9, 163, 8, 'Ertha P Trouillot', '2019-02-21', 1, NULL, 'La situation économique est extrêmement grave', '', 0, '', '2019-07-05', '0000-00-00', 'admin', ''),
(10, 54, 10, 'Franck  Sylvain', '2019-01-23', 1, NULL, 'La situation économique est extrêmement grave', '', 0, '', '2019-07-05', '0000-00-00', 'admin', ''),
(11, 116, 2, 'Jean-Baptiste  Riché', '2019-02-05', 1, NULL, 'La situation économique est extrêmement grave.', '', 0, '', '2019-07-05', '0000-00-00', 'admin', ''),
(12, 148, 1, 'Sténio  Vincent', '2019-02-28', 1, NULL, 'La situation économique est extrêmement grave', '', 5, '', '2019-07-05', '0000-00-00', 'admin', ''),
(13, 228, 16, 'Daniel  Fignolé', '2019-08-05', 1, 4, 'Il etait 10:30 quand elle a commence a faire du bruit de maniere execissive a l\'ecole !', 'Elle a ete renvoyee chercher ses parents ', 10, 'Elle aime faire ces genres de chsoes ', '2019-08-06', '0000-00-00', 'admin', ''),
(14, 155, 2, 'Daniel  Fignolé', '2019-08-07', 1, 4, 'Il etait le premier a frapper.', '2 jours de suspension', 30, '', '2019-08-06', '0000-00-00', 'admin', ''),
(15, 114, 16, 'Jean-Louis  Pierrot', '2019-08-06', 1, 4, 'il etait 10h quand il a commencé a faire du bruit', 'il fera 3 jours à la maison', 0, 'il fera 3 jours à la maison', '2019-08-06', '0000-00-00', 'admin', ''),
(16, 228, 12, 'Jean Pierre  Boyer', '2019-08-06', 1, 4, 'Elle aime faire ce genre de truc', '', 15, '', '2019-08-07', '0000-00-00', 'admin', ''),
(17, 106, 2, 'Daniel  Fignolé', '2019-08-10', 6, 3, 'Il a tabassé un de ses camarades.', '', 0, '', '2019-08-10', '2019-08-10', 'admin', 'admin'),
(18, 114, 6, 'François D  Légitime', '2019-08-10', 6, 3, 'Il parle beaucoup trop en classe.', '', 0, '', '2019-08-10', '0000-00-00', 'admin', ''),
(19, 144, 36, 'Leslie  Manigat', '2019-08-16', 6, 3, 'Il n\'est pas venu en salle de classe et n\'a avertit Personne de la raison de son absence ', '', 0, '', '2019-08-17', '0000-00-00', 'admin', ''),
(20, 191, 40, 'Boniface Alexandre', '2019-08-17', 6, 3, 'Il s\'est battu avec un autre élève de la même salle de classe que lui ', '', 25, '', '2019-08-17', '0000-00-00', 'admin', ''),
(21, 229, 39, 'Louis  Borno', '2019-08-13', 6, 3, 'on l\'a surpris en train de consommer de l\'alcool dans la salle de classe', '', 20, '', '2019-08-17', '0000-00-00', 'admin', ''),
(22, 154, 18, 'Jean-Jacques  Dessalines', '2020-08-04', 6, 3, 'Il n\'a pas remis ses devoirs et a évoqué des excuses non acceptables', '', 0, '', '2019-08-17', '0000-00-00', 'admin', ''),
(23, 77, 7, 'Henri   Christophe', '2019-08-27', 6, 7, 'On l\'a surpris en train de tricher dans la salle d\'examen en utilisant son Téléphone Portable', '', 100, '', '2019-08-17', '2019-08-31', 'admin', 'admin'),
(24, 189, 4, 'Dumarsais  Estimé', '2019-08-17', 6, 3, 'L\'élève ONEL Onel n\'est jamais présent dans mon cours pourtant j\'ai la confirmation qu\'il est à l\'école.', '', 10, '', '2019-08-17', '0000-00-00', 'admin', ''),
(25, 201, 27, 'Leslie  Manigat', '2019-08-16', 6, 3, 'L\'élève Garoute apporte toujours de la nourriture en classe et en plus Il est toujours entrain de manger avec ses camarades pendant que le professeur dispense le cours.', '', 5, '', '2019-08-17', '0000-00-00', 'admin', ''),
(26, 183, 18, 'Jean-Louis  Pierrot', '2019-08-13', 6, 3, 'L\'élève Serge Labbe ne remet jamais ses devoirs.', '', 15, '', '2019-08-17', '0000-00-00', 'admin', ''),
(27, 182, 17, 'Nord  Alexis', '2019-08-12', 6, 3, 'il a laissé la lumière de la salle de classe pourtant il était le dernier à quitter la salle.', '', 0, '', '2019-08-17', '0000-00-00', 'admin', ''),
(28, 90, 3, 'Paul  Magloire', '2019-08-21', 6, 3, 'Test', '', 5, '', '2019-08-21', '0000-00-00', 'admin', ''),
(29, 241, 2, 'Dumarsais  Estimé', '2019-08-27', 6, 7, 'Il s\'est battu avec 2 autres eleves ', '', 30, '', '2019-08-31', '2019-08-31', 'admin', 'admin'),
(30, 243, 4, 'Leslie  Manigat', '2019-08-30', 6, 3, 'Pendant le mois d\'aout, elle a eu six absences', '', 0, '', '2019-08-31', '0000-00-00', 'admin', ''),
(33, 252, 2, 'Daniel  Fignolé', '2018-08-08', 6, 3, 'livre dechire', 'convocation parent', 0, '', '2019-08-31', '0000-00-00', 'admin', ''),
(34, 244, 36, 'François D  Légitime', '2019-08-08', 6, 3, 'abscence non motive', '', 0, '', '2019-08-31', '0000-00-00', 'admin', ''),
(35, 246, 6, 'Dumarsais  Estimé', '2019-08-07', 6, 3, 'stylo vole', '2 jours a la maison', 0, '', '2019-08-31', '0000-00-00', 'admin', ''),
(36, 249, 27, 'Sandra Charles', '2019-08-05', 6, 3, 'Mange dans la classe', '', 0, '', '2019-08-31', '0000-00-00', 'admin', ''),
(37, 247, 36, 'Leslie  Manigat', '2019-08-15', 6, 3, 'Absences à trois reprises', '', 5, '', '2019-08-31', '0000-00-00', 'admin', ''),
(38, 67, 27, 'Boniface Alexandre', '2019-09-01', 6, 7, 'Malgré les interdictions, il a désobéi et ceci â maintes reprises.', '', 5, '', '2019-09-01', '0000-00-00', 'admin', ''),
(39, 266, 45, 'LEONARD Junette', '2019-09-05', 6, 7, 'Elle a regarde la Soeur principale d\'une facon anormale ', 'Elle a ete mise en retenue', 10, 'Elle affiche souvent ce genre de comportement', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(40, 270, 6, 'François D  Légitime', '0000-00-00', 6, 7, 'bavarde', 'elle est en retenue ', 5, 'elle parle beaucoup en classe', '2019-09-05', '0000-00-00', 'admin', ''),
(41, 267, 2, 'Leslie  Manigat', '2019-09-02', 6, 7, 'Elle se bat en salle de classe en presence du professeur', 'Le professeur lui a donne un devoir supplementaire', 30, 'Elle affiche toujours ce genre de comportement', '2019-09-05', '0000-00-00', 'admin', ''),
(42, 268, 27, 'Leslie  Manigat', '2019-09-04', 6, 7, 'Elle n\'arrete pas de manger en salle de classe', '', 5, '', '2019-09-05', '0000-00-00', 'admin', ''),
(43, 303, 25, 'Titus PIERRE', '2019-09-13', 6, 7, 'hjvg,vfhgjggh', '', 10, '', '2019-09-13', '0000-00-00', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `record_presence`
--

CREATE TABLE `record_presence` (
  `id` int(11) NOT NULL,
  `student` int(20) NOT NULL,
  `room` int(11) DEFAULT NULL,
  `academic_period` int(11) DEFAULT NULL,
  `exam_period` int(11) DEFAULT NULL,
  `date_record` datetime NOT NULL,
  `presence_type` int(11) NOT NULL,
  `comments` text,
  `input_method` varchar(255) DEFAULT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record_presence`
--

INSERT INTO `record_presence` (`id`, `student`, `room`, `academic_period`, `exam_period`, `date_record`, `presence_type`, `comments`, `input_method`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 52, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(2, 53, 1, 1, 2, '2018-12-11 00:00:00', 2, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(3, 54, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(4, 55, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(5, 56, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(6, 57, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(7, 58, 1, 1, 2, '2018-12-11 00:00:00', 2, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(8, 59, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(9, 60, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(10, 61, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(11, 62, 1, 1, 2, '2018-12-11 00:00:00', 3, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(12, 63, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(13, 64, 1, 1, 2, '2018-12-11 00:00:00', 3, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(14, 65, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(15, 66, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(16, 67, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(17, 68, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(18, 69, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(19, 70, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(20, 71, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(21, 72, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(22, 73, 1, 1, 2, '2018-12-11 00:00:00', 3, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(23, 74, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(24, 75, 1, 1, 2, '2018-12-11 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(25, 76, 1, 1, 2, '2018-12-11 00:00:00', 1, '', NULL, '2019-07-03', '0000-00-00', 'LOGIPAM', ''),
(26, 129, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(27, 130, 5, 1, 2, '2018-11-22 00:00:00', 1, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(28, 131, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(29, 132, 5, 1, 2, '2018-11-22 00:00:00', 1, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(30, 133, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(31, 134, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(32, 135, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(33, 136, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(34, 137, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(35, 138, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(36, 139, 5, 1, 2, '2018-11-22 00:00:00', 1, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(37, 140, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(38, 141, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(39, 142, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(40, 143, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(41, 144, 5, 1, 2, '2018-11-22 00:00:00', 3, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(42, 145, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(43, 146, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(44, 147, 5, 1, 2, '2018-11-22 00:00:00', 1, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(45, 148, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(46, 149, 5, 1, 2, '2018-11-22 00:00:00', 4, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(47, 150, 5, 1, 2, '2018-11-22 00:00:00', 2, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(48, 151, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(49, 152, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(50, 153, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(51, 154, 5, 1, 2, '2018-11-22 00:00:00', 0, '', NULL, '2019-07-03', '0000-00-00', 'admin', ''),
(52, 77, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(53, 78, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(54, 79, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(55, 80, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(56, 81, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(57, 82, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(58, 83, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(59, 84, 2, 1, 3, '2019-01-23 00:00:00', 2, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(60, 85, 2, 1, 3, '2019-01-23 00:00:00', 1, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(61, 86, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(62, 87, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(63, 88, 2, 1, 3, '2019-01-23 00:00:00', 3, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(64, 89, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(65, 90, 2, 1, 3, '2019-01-23 00:00:00', 2, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(66, 91, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(67, 92, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(68, 93, 2, 1, 3, '2019-01-23 00:00:00', 2, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(69, 94, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(70, 95, 2, 1, 3, '2019-01-23 00:00:00', 3, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(71, 96, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(72, 97, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(73, 98, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(74, 99, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(75, 100, 2, 1, 3, '2019-01-23 00:00:00', 4, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(76, 101, 2, 1, 3, '2019-01-23 00:00:00', 0, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(77, 102, 2, 1, 3, '2019-01-23 00:00:00', 3, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(78, 103, 2, 1, 3, '2019-01-23 00:00:00', 4, '', NULL, '2019-07-05', '0000-00-00', 'admin', ''),
(80, 211, 8, 1, 4, '2019-07-29 17:11:37', 3, NULL, 'Scanner', '2019-07-29', '0000-00-00', 'LOGIPAM', ''),
(81, 211, 8, 1, 4, '2019-07-30 13:46:46', 0, NULL, 'Scanner', '2019-07-30', '0000-00-00', 'LOGIPAM', ''),
(82, 210, 8, 1, 4, '2019-07-30 14:09:56', 0, NULL, 'Scanner', '2019-07-30', '0000-00-00', 'LOGIPAM', ''),
(87, 210, 8, 1, 4, '2019-07-31 11:21:48', 3, NULL, 'Scanner', '2019-07-31', '0000-00-00', 'LOGIPAM', ''),
(88, 210, 8, 1, 4, '2019-08-01 13:07:08', 3, NULL, 'Scanner', '2019-08-01', '0000-00-00', 'LOGIPAM', ''),
(89, 230, 2, 6, 3, '2019-08-11 00:00:00', 3, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(90, 65, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(91, 236, 2, 6, 3, '2019-08-11 00:00:00', 4, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(92, 60, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(93, 234, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(94, 233, 2, 6, 3, '2019-08-11 00:00:00', 1, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(95, 52, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(96, 70, 2, 6, 3, '2019-08-11 00:00:00', 4, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(97, 63, 2, 6, 3, '2019-08-11 00:00:00', 1, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(98, 235, 2, 6, 3, '2019-08-11 00:00:00', 3, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(99, 61, 2, 6, 3, '2019-08-11 00:00:00', 4, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(100, 59, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(101, 68, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(102, 76, 2, 6, 3, '2019-08-11 00:00:00', 4, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(103, 240, 2, 6, 3, '2019-08-11 00:00:00', 4, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(104, 71, 2, 6, 3, '2019-08-11 00:00:00', 3, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(105, 232, 2, 6, 3, '2019-08-11 00:00:00', 1, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(106, 58, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(107, 54, 2, 6, 3, '2019-08-11 00:00:00', 3, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(108, 237, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(109, 64, 2, 6, 3, '2019-08-11 00:00:00', 4, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(110, 66, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(111, 228, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(112, 231, 2, 6, 3, '2019-08-11 00:00:00', 4, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(113, 56, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(114, 57, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(115, 67, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(116, 55, 2, 6, 3, '2019-08-11 00:00:00', 0, 'Il est toujours présent.', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(117, 62, 2, 6, 3, '2019-08-11 00:00:00', 2, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(118, 74, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(119, 69, 2, 6, 3, '2019-08-11 00:00:00', 2, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(120, 72, 2, 6, 3, '2019-08-11 00:00:00', 2, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(121, 73, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(122, 75, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(123, 229, 2, 6, 3, '2019-08-11 00:00:00', 0, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(124, 238, 2, 6, 3, '2019-08-11 00:00:00', 1, '', NULL, '2019-08-11', '0000-00-00', 'admin', ''),
(125, 201, 1, 6, 3, '2019-08-15 00:00:00', 1, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(126, 180, 1, 6, 3, '2019-08-15 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(127, 196, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(128, 187, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(129, 200, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(130, 193, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(131, 189, 1, 6, 3, '2019-08-15 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(132, 203, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(133, 194, 1, 6, 3, '2019-08-15 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(134, 191, 1, 6, 3, '2019-08-15 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(135, 181, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(136, 182, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(137, 183, 1, 6, 3, '2019-08-15 00:00:00', 1, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(138, 201, 1, 6, 3, '2019-08-15 00:00:00', 2, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(139, 180, 1, 6, 3, '2019-08-15 00:00:00', 3, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(140, 196, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(141, 187, 1, 6, 3, '2019-08-15 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(142, 200, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(143, 193, 1, 6, 3, '2019-08-15 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(144, 189, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(145, 203, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(146, 194, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(147, 191, 1, 6, 3, '2019-08-15 00:00:00', 1, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(148, 181, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(149, 182, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(150, 183, 1, 6, 3, '2019-08-15 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(151, 201, 1, 6, 3, '2019-08-18 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(152, 180, 1, 6, 3, '2019-08-18 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(153, 196, 1, 6, 3, '2019-08-18 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(154, 187, 1, 6, 3, '2019-08-18 00:00:00', 1, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(155, 200, 1, 6, 3, '2019-08-18 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(156, 193, 1, 6, 3, '2019-08-18 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(157, 189, 1, 6, 3, '2019-08-18 00:00:00', 2, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(158, 203, 1, 6, 3, '2019-08-18 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(159, 194, 1, 6, 3, '2019-08-18 00:00:00', 4, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(160, 191, 1, 6, 3, '2019-08-18 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(161, 181, 1, 6, 3, '2019-08-18 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(162, 182, 1, 6, 3, '2019-08-18 00:00:00', 0, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(163, 183, 1, 6, 3, '2019-08-18 00:00:00', 2, '', NULL, '2019-08-18', '0000-00-00', 'admin', ''),
(165, 208, 9, 6, 3, '2019-08-23 21:50:35', 3, NULL, 'Scanner', '2019-08-23', '0000-00-00', 'admin', ''),
(166, 208, 9, 6, 3, '2019-08-24 21:27:20', 3, NULL, 'Scanner', '2019-08-24', '0000-00-00', 'admin', ''),
(167, 164, 7, 6, 3, '2019-08-24 21:28:18', 3, NULL, 'Scanner', '2019-08-24', '0000-00-00', 'admin', ''),
(168, 230, 2, 6, 3, '2019-08-24 21:28:22', 3, NULL, 'Scanner', '2019-08-24', '0000-00-00', 'admin', ''),
(170, 236, 2, 6, 3, '2019-08-24 21:33:48', 3, NULL, 'Scanner', '2019-08-24', '0000-00-00', 'admin', ''),
(171, 70, 2, 6, 3, '2019-08-24 21:34:51', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(172, 60, 2, 6, 3, '2019-08-24 21:34:51', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(173, 65, 2, 6, 3, '2019-08-24 21:34:51', 2, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(174, 52, 2, 6, 3, '2019-08-24 21:34:51', 2, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(175, 233, 2, 6, 3, '2019-08-24 21:34:51', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(176, 234, 2, 6, 3, '2019-08-24 21:34:51', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(177, 235, 2, 6, 3, '2019-08-24 21:34:52', 2, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(178, 63, 2, 6, 3, '2019-08-24 21:34:51', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(179, 59, 2, 6, 3, '2019-08-24 21:34:52', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(180, 71, 2, 6, 3, '2019-08-24 21:34:52', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(181, 61, 2, 6, 3, '2019-08-24 21:34:52', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(182, 76, 2, 6, 3, '2019-08-24 21:34:52', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(183, 68, 2, 6, 3, '2019-08-24 21:34:52', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(184, 232, 2, 6, 3, '2019-08-24 21:34:52', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(185, 54, 2, 6, 3, '2019-08-24 21:34:52', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(186, 237, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(187, 58, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(188, 64, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(189, 56, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(190, 240, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(191, 57, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(192, 231, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(193, 228, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(194, 66, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(195, 67, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(196, 74, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(197, 73, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(198, 55, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(199, 62, 2, 6, 3, '2019-08-24 21:34:53', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(200, 69, 2, 6, 3, '2019-08-24 21:34:54', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(201, 72, 2, 6, 3, '2019-08-24 21:34:54', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(202, 75, 2, 6, 3, '2019-08-24 21:34:54', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(203, 229, 2, 6, 3, '2019-08-24 21:34:54', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(204, 238, 2, 6, 3, '2019-08-24 21:34:54', 1, NULL, 'Manual', '2019-08-24', '0000-00-00', 'admin', ''),
(205, 209, 9, 6, 3, '2019-08-25 10:15:28', 3, NULL, 'Scanner', '2019-08-25', '0000-00-00', 'admin', ''),
(206, 208, 9, 6, 3, '2019-08-26 16:36:48', 3, NULL, 'Scanner', '2019-08-26', '0000-00-00', 'admin', ''),
(207, 196, 1, 6, 3, '2019-08-26 16:39:22', 3, NULL, 'Scanner', '2019-08-26', '0000-00-00', 'admin', ''),
(208, 65, 2, 6, 3, '2019-08-26 16:39:28', 3, NULL, 'Scanner', '2019-08-26', '0000-00-00', 'admin', ''),
(209, 230, 2, 6, 3, '2019-08-27 07:03:29', 0, NULL, 'Scanner', '2019-08-27', '0000-00-00', 'admin', ''),
(210, 60, 2, 6, 3, '2019-08-27 07:06:54', 0, NULL, 'Scanner', '2019-08-27', '0000-00-00', 'admin', ''),
(211, 101, 3, 6, 3, '2019-08-27 07:09:43', 0, NULL, 'Scanner', '2019-08-27', '0000-00-00', 'admin', ''),
(212, 59, 2, 6, 3, '2019-08-28 17:39:58', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(214, 184, 14, 6, 3, '2019-08-28 17:42:28', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(215, 65, 2, 6, 3, '2019-08-28 17:42:33', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(216, 158, 7, 6, 3, '2019-08-28 17:42:57', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(217, 162, 7, 6, 3, '2019-08-28 17:43:01', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(218, 140, 13, 6, 3, '2019-08-28 17:44:21', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(219, 161, 7, 6, 3, '2019-08-28 17:44:25', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(220, 63, 2, 6, 3, '2019-08-28 17:51:44', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(221, 134, 13, 6, 3, '2019-08-28 17:51:56', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(222, 98, 3, 6, 3, '2019-08-28 17:58:59', 3, NULL, 'Scanner', '2019-08-28', '0000-00-00', 'admin', ''),
(223, 126, 5, 6, 3, '2019-08-30 10:08:38', 3, NULL, 'Scanner', '2019-08-30', '0000-00-00', 'admin', ''),
(224, 205, 9, 6, 3, '2019-08-30 19:04:27', 3, NULL, 'Scanner', '2019-08-30', '0000-00-00', 'admin', ''),
(226, 230, 2, 6, 3, '2019-08-30 19:49:01', 3, NULL, 'Scanner', '2019-08-30', '0000-00-00', 'admin', ''),
(227, 122, 5, 6, 3, '2019-08-30 20:20:21', 3, NULL, 'Scanner', '2019-08-30', '0000-00-00', 'admin', ''),
(228, 241, 4, 6, 3, '2019-08-31 00:00:00', 1, '', NULL, '2019-08-31', '2019-08-31', 'admin', 'admin'),
(229, 242, 4, 6, 3, '2019-08-31 00:00:00', 1, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(230, 243, 4, 6, 3, '2019-08-31 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(231, 244, 4, 6, 3, '2019-08-31 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(232, 245, 4, 6, 3, '2019-08-31 00:00:00', 2, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(233, 247, 4, 6, 3, '2019-08-31 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(234, 248, 4, 6, 3, '2019-08-31 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(235, 249, 4, 6, 3, '2019-08-31 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(236, 246, 4, 6, 3, '2019-08-31 00:00:00', 1, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(237, 250, 4, 6, 3, '2019-08-31 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(238, 251, 4, 6, 3, '2019-08-31 00:00:00', 3, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(239, 252, 4, 6, 3, '2019-08-31 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(240, 241, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(241, 242, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(242, 243, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(243, 244, 4, 6, 2, '2019-09-03 00:00:00', 2, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(244, 245, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(245, 247, 4, 6, 2, '2019-09-03 00:00:00', 3, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(246, 248, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(247, 249, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(248, 246, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(249, 250, 4, 6, 2, '2019-09-03 00:00:00', 3, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(250, 251, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(251, 252, 4, 6, 2, '2019-09-03 00:00:00', 0, '', NULL, '2019-08-31', '0000-00-00', 'admin', ''),
(252, 208, 9, 6, 7, '2019-09-03 11:10:49', 3, NULL, 'Scanner', '2019-09-03', '0000-00-00', 'admin', ''),
(253, 266, 12, 6, 7, '2019-09-02 00:00:00', 1, '', NULL, '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(254, 267, 12, 6, 7, '2019-09-02 00:00:00', 0, '', NULL, '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(255, 268, 12, 6, 7, '2019-09-02 00:00:00', 0, '', NULL, '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(256, 269, 12, 6, 7, '2019-09-02 00:00:00', 3, '', NULL, '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(257, 270, 12, 6, 7, '2019-09-02 00:00:00', 0, '', NULL, '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(258, 202, 14, 6, 7, '2019-09-05 14:23:32', 3, NULL, 'Scanner', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(259, 209, 9, 6, 7, '2019-09-05 14:23:51', 3, NULL, 'Scanner', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(260, 207, 9, 6, 7, '2019-09-05 14:23:57', 3, NULL, 'Scanner', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(261, 201, 1, 6, 7, '2019-09-05 14:24:41', 3, NULL, 'Scanner', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(262, 195, 14, 6, 7, '2019-09-05 14:27:39', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(263, 179, 14, 6, 7, '2019-09-05 14:27:39', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(264, 190, 14, 6, 7, '2019-09-05 14:27:39', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(265, 186, 14, 6, 7, '2019-09-05 14:27:39', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(266, 184, 14, 6, 7, '2019-09-05 14:27:39', 2, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(267, 197, 14, 6, 7, '2019-09-05 14:27:39', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(268, 198, 14, 6, 7, '2019-09-05 14:27:39', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(269, 157, 14, 6, 7, '2019-09-05 14:27:39', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(270, 188, 14, 6, 7, '2019-09-05 14:27:39', 1, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(271, 199, 14, 6, 7, '2019-09-05 14:27:40', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(272, 192, 14, 6, 7, '2019-09-05 14:27:41', 0, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(273, 185, 14, 6, 7, '2019-09-05 14:27:42', 1, NULL, 'Manual', '2019-09-05', '0000-00-00', 'LOGIPAM', ''),
(274, 241, 4, 6, 7, '2019-09-11 11:23:36', 3, NULL, 'Scanner', '2019-09-11', '0000-00-00', 'admin', ''),
(275, 123, 5, 6, 7, '2019-09-11 11:26:37', 3, NULL, 'Scanner', '2019-09-11', '0000-00-00', 'admin', ''),
(276, 134, 13, 6, 7, '2019-09-11 11:26:43', 3, NULL, 'Scanner', '2019-09-11', '0000-00-00', 'admin', ''),
(277, 122, 5, 6, 7, '2019-09-12 13:15:22', 3, NULL, 'Scanner', '2019-09-12', '0000-00-00', 'admin', ''),
(278, 123, 5, 6, 7, '2019-09-12 13:15:51', 3, NULL, 'Scanner', '2019-09-12', '0000-00-00', 'admin', ''),
(279, 124, 5, 6, 7, '2019-09-12 13:15:54', 3, NULL, 'Scanner', '2019-09-12', '0000-00-00', 'admin', ''),
(280, 122, 5, 6, 7, '2019-09-13 13:09:03', 3, NULL, 'Scanner', '2019-09-13', '0000-00-00', 'admin', ''),
(281, 123, 5, 6, 7, '2019-09-13 13:09:19', 3, NULL, 'Scanner', '2019-09-13', '0000-00-00', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `record_special_presence`
--

CREATE TABLE `record_special_presence` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_record` datetime NOT NULL,
  `presence_type` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `create_by` varchar(64) NOT NULL,
  `date_updated` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `record_special_presence`
--

INSERT INTO `record_special_presence` (`id`, `student`, `academic_year`, `date_record`, `presence_type`, `date_created`, `create_by`, `date_updated`, `update_by`) VALUES
(1, 10, 6, '2019-09-05 14:29:01', 0, '2019-09-05 00:00:00', 'admin', NULL, NULL),
(2, 200, 6, '2019-09-05 14:29:04', 0, '2019-09-05 00:00:00', 'admin', NULL, NULL),
(3, 202, 6, '2019-09-05 14:29:10', 0, '2019-09-05 00:00:00', 'LOGIPAM', NULL, NULL),
(4, 201, 6, '2019-09-05 14:29:27', 0, '2019-09-05 00:00:00', 'LOGIPAM', NULL, NULL),
(5, 234, 6, '2019-09-05 14:29:45', 0, '2019-09-05 00:00:00', 'LOGIPAM', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `relation_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `relation_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Père ', NULL, NULL, NULL, NULL),
(3, 'Mère', NULL, NULL, NULL, NULL),
(5, 'Autres', NULL, NULL, NULL, NULL),
(6, 'Tante', '2019-07-03 00:00:00', NULL, NULL, NULL),
(7, 'Oncle', '2019-07-03 00:00:00', NULL, NULL, NULL),
(8, 'Tuteur(trice)', '2019-07-03 00:00:00', NULL, NULL, NULL),
(9, 'Frère', '2019-07-03 00:00:00', NULL, NULL, NULL),
(10, 'Soeur', '2019-07-03 00:00:00', NULL, NULL, NULL),
(11, 'Cousin(e)', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(12, 'Grand-père', '2019-08-10 00:00:00', NULL, NULL, NULL),
(13, 'Grand-mère', '2019-08-10 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reportcard_observation`
--

CREATE TABLE `reportcard_observation` (
  `id` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `start_range` float NOT NULL,
  `end_range` float NOT NULL,
  `comment` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `reportcard_observation`
--

INSERT INTO `reportcard_observation` (`id`, `section`, `start_range`, `end_range`, `comment`, `academic_year`, `create_by`, `update_by`, `create_date`, `update_date`) VALUES
(1, 1, 0, 49.99, 'Effort insuffisant', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(2, 1, 50, 59.99, 'Passable', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(3, 1, 60, 69.99, 'Assez bien. Tu peux faire mieux', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(4, 1, 70, 79.99, 'Bien. continue.', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(5, 1, 80, 89.99, 'Très bien', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(6, 1, 90, 100, 'Excellent', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(7, 2, 0, 49.99, 'Effort insuffisant', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(8, 2, 50, 59.99, 'Passable', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(9, 2, 60, 69.99, 'Assez bien. Tu peux faire mieux', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(10, 2, 70, 79.99, 'Bien. continue.', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(11, 2, 80, 89.99, 'Très bien', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(12, 2, 90, 100, 'Excellent', 1, 'admin', NULL, '2019-07-01 20:07:28', NULL),
(13, 1, 0, 49.99, 'Effort insuffisant', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(14, 1, 50, 59.99, 'Passable', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(15, 1, 60, 69.99, 'Assez bien. Tu peux faire mieux', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(16, 1, 70, 79.99, 'Bien. continue.', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(17, 1, 80, 89.99, 'Très bien', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(18, 1, 90, 100, 'Excellent', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(19, 2, 0, 49.99, 'Effort insuffisant', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(20, 2, 50, 59.99, 'Passable', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(21, 2, 60, 69.99, 'Assez bien. Tu peux faire mieux', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(22, 2, 70, 79.99, 'Bien. continue.', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(23, 2, 80, 89.99, 'Très bien', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL),
(24, 2, 90, 100, 'Excellent', 6, 'SIGES', NULL, '2019-08-10 00:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `postulant_student` int(11) NOT NULL,
  `is_student` tinyint(2) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `already_checked` tinyint(2) NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_history`
--

CREATE TABLE `return_history` (
  `id` int(11) NOT NULL,
  `id_transaction` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `return_amount` float NOT NULL,
  `return_quantity` int(11) NOT NULL,
  `date_return` datetime NOT NULL,
  `return_by` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_name` varchar(45) NOT NULL,
  `short_room_name` varchar(45) NOT NULL,
  `level` int(11) NOT NULL,
  `shift` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rooms`
--

INSERT INTO `rooms` (`id`, `room_name`, `short_room_name`, `level`, `shift`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Septième Année B', '7e B', 1, 1, '2019-07-01 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(2, 'Huitième', '8e', 2, 1, '2019-07-01 00:00:00', '2019-07-06 00:00:00', 'admin', 'admin'),
(3, 'Neuvième', '9e A', 3, 1, '2019-07-01 00:00:00', '2019-07-06 00:00:00', 'admin', 'admin'),
(4, 'Neuvième Année Fondamentale B', '9e AF B', 3, 1, '2019-07-01 00:00:00', '2019-08-31 00:00:00', 'admin', 'LOGIPAM'),
(5, 'Secondaire I', 'Sec I', 4, 1, '2019-07-01 00:00:00', '2019-07-06 00:00:00', 'admin', 'admin'),
(6, 'Cinquième', '5e', 5, 1, '2019-07-05 00:00:00', '2019-07-06 00:00:00', 'admin', 'admin'),
(7, 'Sixième', '6e', 6, 1, '2019-07-05 00:00:00', '2019-07-06 00:00:00', 'admin', 'admin'),
(8, 'Papillon', 'Papillon', 7, 1, '2019-07-22 00:00:00', '2019-07-22 00:00:00', 'admin', NULL),
(9, 'Section des moyens', 'Moyens', 8, 1, '2019-08-03 00:00:00', '2019-08-03 00:00:00', 'LOGIPAM', NULL),
(10, 'Secondaire 4B', 'sec 4B', 11, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 'admin', NULL),
(12, 'Secondaire 3 A', 'Sec 3A', 10, 1, '2019-08-05 00:00:00', '2019-09-05 00:00:00', 'admin', 'admin'),
(13, 'SEC 2C', 'SEC 2C', 9, 1, '2019-08-05 00:00:00', '2019-08-09 00:00:00', 'admin', 'admin'),
(14, 'Septième Année A', '7e A', 1, 1, '2019-08-05 00:00:00', '2019-08-10 00:00:00', 'admin', 'admin'),
(15, 'Première Année', '1ère AF', 13, 1, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 'admin', NULL),
(16, 'Grands', 'Grands', 12, 1, '2019-09-14 00:00:00', '2019-09-14 00:00:00', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `room_has_person`
--

CREATE TABLE `room_has_person` (
  `id` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `room_has_person`
--

INSERT INTO `room_has_person` (`id`, `room`, `students`, `academic_year`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 1, 52, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(2, 1, 53, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(3, 1, 54, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(4, 1, 55, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(5, 1, 56, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(6, 1, 57, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(7, 1, 58, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(8, 1, 59, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(9, 1, 60, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(10, 1, 61, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(11, 1, 62, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(12, 1, 63, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(13, 1, 64, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(14, 1, 65, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(15, 1, 66, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(16, 1, 67, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(17, 1, 68, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(18, 1, 69, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(19, 1, 70, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(20, 1, 71, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(21, 1, 72, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(22, 1, 73, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(23, 1, 74, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(24, 1, 75, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(25, 1, 76, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(26, 2, 77, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(27, 2, 78, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(28, 2, 79, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(29, 2, 80, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(30, 2, 81, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(31, 2, 82, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(32, 2, 83, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(33, 2, 84, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(34, 2, 85, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(35, 2, 86, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(36, 2, 87, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(37, 2, 88, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(38, 2, 89, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(39, 2, 90, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(40, 2, 91, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(41, 2, 92, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(42, 2, 93, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(43, 2, 94, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(44, 2, 95, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(45, 2, 96, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(46, 2, 97, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(47, 2, 98, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(48, 2, 99, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(49, 2, 100, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(50, 2, 101, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(51, 2, 102, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(52, 2, 103, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(53, 3, 104, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(54, 3, 105, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(55, 3, 106, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(56, 3, 107, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(57, 3, 108, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(58, 3, 109, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(59, 3, 110, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(60, 3, 111, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(61, 3, 112, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(62, 3, 113, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(63, 3, 114, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(64, 3, 115, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(65, 3, 116, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(66, 3, 117, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(67, 3, 118, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(68, 3, 119, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(69, 3, 120, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(70, 3, 121, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(71, 3, 122, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(72, 3, 123, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(73, 3, 124, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(74, 3, 125, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(75, 3, 126, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(76, 3, 127, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(77, 3, 128, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(78, 5, 129, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(79, 5, 130, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(80, 5, 131, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(81, 5, 132, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(82, 5, 133, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(83, 5, 134, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(84, 5, 135, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(85, 5, 136, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(86, 5, 137, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(87, 5, 138, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(88, 5, 139, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(89, 5, 140, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(90, 5, 141, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(91, 5, 142, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(92, 5, 143, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(93, 5, 144, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(94, 5, 145, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(95, 5, 146, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(96, 5, 147, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(97, 5, 148, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(98, 5, 149, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(99, 5, 150, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(100, 5, 151, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(101, 5, 152, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(102, 5, 153, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(103, 5, 154, 1, '2019-07-03 00:00:00', NULL, 'siges_migration_tool', NULL),
(104, 6, 155, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(105, 6, 156, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(106, 6, 157, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(107, 6, 158, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(108, 6, 159, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(109, 6, 160, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(110, 6, 161, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(111, 6, 162, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(112, 6, 163, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(113, 6, 164, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(114, 6, 165, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(115, 6, 166, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(116, 6, 167, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(117, 6, 168, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(118, 6, 169, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(119, 6, 170, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(120, 6, 171, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(121, 6, 172, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(122, 6, 173, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(123, 6, 174, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(124, 6, 175, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(125, 6, 176, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(126, 6, 177, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(127, 6, 178, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(128, 7, 179, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(129, 7, 180, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(130, 7, 181, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(131, 7, 182, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(132, 7, 183, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(133, 7, 184, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(134, 7, 185, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(135, 7, 186, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(136, 7, 187, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(137, 7, 188, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(138, 7, 189, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(139, 7, 190, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(140, 7, 191, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(141, 7, 192, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(142, 7, 193, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(143, 7, 194, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(144, 7, 195, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(145, 7, 196, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(146, 7, 197, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(147, 7, 198, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(148, 7, 199, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(149, 7, 200, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(150, 7, 201, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(151, 7, 202, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(152, 7, 203, 1, '2019-07-05 00:00:00', NULL, 'siges_migration_tool', NULL),
(153, 8, 204, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(154, 8, 205, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(155, 8, 206, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(156, 8, 207, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(157, 8, 208, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(158, 8, 209, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(159, 8, 210, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(160, 8, 211, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(161, 8, 212, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(162, 8, 213, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(163, 8, 214, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(164, 8, 215, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(165, 8, 216, 1, '2019-07-23 00:00:00', NULL, 'siges_migration_tool', NULL),
(166, 14, 228, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(167, 14, 229, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(168, 14, 230, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(169, 14, 231, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(170, 14, 232, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(171, 14, 233, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(172, 14, 234, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(173, 14, 235, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(174, 14, 236, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(175, 14, 237, 1, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(176, 14, 238, 1, '2019-08-06 00:00:00', '2019-08-06 00:00:00', NULL, NULL),
(177, 14, 239, 1, '2019-08-06 00:00:00', '2019-08-06 00:00:00', NULL, NULL),
(178, 2, 230, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(179, 2, 65, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(180, 2, 236, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(181, 2, 60, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(182, 2, 234, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(183, 2, 233, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(184, 2, 52, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(185, 2, 70, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(186, 2, 63, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(187, 2, 235, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(188, 2, 61, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(189, 2, 59, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(190, 2, 68, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(191, 2, 76, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(192, 2, 240, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(193, 2, 71, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(194, 2, 232, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(195, 2, 58, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(196, 2, 54, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(197, 2, 237, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(198, 2, 64, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(199, 2, 66, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(200, 2, 228, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(201, 2, 231, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(202, 2, 56, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(203, 2, 57, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(204, 2, 67, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(205, 2, 55, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(206, 2, 62, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(207, 2, 74, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(208, 2, 69, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(209, 2, 72, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(210, 2, 73, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(211, 2, 75, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(212, 2, 229, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(213, 2, 238, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(214, 3, 86, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(215, 3, 103, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(216, 3, 80, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(217, 3, 101, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(218, 3, 92, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(219, 3, 94, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(220, 3, 97, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(221, 3, 91, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(222, 3, 93, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(223, 3, 78, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(224, 3, 90, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(225, 3, 102, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(226, 3, 99, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(227, 3, 88, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(228, 3, 98, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(229, 3, 81, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(230, 3, 96, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(231, 3, 79, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(232, 3, 82, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(233, 3, 83, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(234, 3, 77, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(235, 3, 85, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(236, 3, 89, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(237, 3, 87, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(238, 3, 100, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(239, 3, 95, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(240, 3, 84, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(241, 5, 120, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(242, 5, 128, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(243, 5, 114, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(244, 5, 122, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(245, 5, 117, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(246, 5, 107, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(247, 5, 112, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(248, 5, 121, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(249, 5, 113, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(250, 5, 123, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(251, 5, 124, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(252, 5, 116, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(253, 5, 119, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(254, 5, 118, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(255, 5, 109, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(256, 5, 127, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(257, 5, 110, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(258, 5, 126, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(259, 5, 111, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(260, 5, 115, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(261, 5, 106, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(262, 13, 153, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(263, 13, 154, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(264, 13, 135, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(265, 13, 144, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(266, 13, 152, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(267, 13, 131, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(268, 13, 151, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(269, 13, 140, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(270, 13, 142, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(271, 13, 148, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(272, 13, 150, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(273, 13, 139, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(274, 13, 138, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(275, 13, 133, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(276, 13, 145, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(277, 13, 136, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(278, 13, 143, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(279, 13, 137, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(280, 13, 132, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(281, 13, 134, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(282, 13, 129, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(283, 13, 141, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(284, 13, 147, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(285, 13, 130, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(286, 13, 146, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(287, 13, 149, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(311, 9, 208, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(312, 9, 212, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(313, 9, 216, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(314, 9, 209, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(315, 9, 210, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(316, 9, 207, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(317, 9, 214, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(318, 9, 213, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(319, 9, 205, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(320, 9, 206, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(321, 9, 215, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(322, 9, 211, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(323, 9, 204, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(324, 14, 184, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(325, 14, 195, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(326, 14, 190, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(327, 14, 192, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(328, 14, 179, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(329, 14, 185, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(330, 14, 186, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(331, 14, 188, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(332, 14, 199, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(333, 14, 197, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(334, 14, 202, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(335, 14, 198, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(336, 14, 157, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(337, 1, 201, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(338, 1, 180, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(339, 1, 196, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(340, 1, 187, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(341, 1, 200, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(342, 1, 193, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(343, 1, 189, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(344, 1, 203, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(345, 1, 194, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(346, 1, 191, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(347, 1, 181, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(348, 1, 182, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(349, 1, 183, 6, '2019-08-10 00:00:00', '2019-08-10 00:00:00', 'admin', NULL),
(350, 4, 241, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(351, 4, 242, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(352, 4, 243, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(353, 4, 244, 6, NULL, NULL, NULL, NULL),
(354, 4, 245, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(355, 4, 247, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(356, 4, 248, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(357, 4, 249, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(358, 4, 246, 6, NULL, NULL, NULL, NULL),
(359, 4, 250, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(360, 4, 251, 6, NULL, NULL, NULL, NULL),
(361, 4, 252, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(362, 4, 260, 6, '2019-08-31 00:00:00', '2019-08-31 00:00:00', NULL, NULL),
(363, 15, 262, 6, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(364, 4, 263, 6, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(365, 4, 265, 6, '2019-09-01 00:00:00', '2019-09-01 00:00:00', NULL, NULL),
(366, 12, 266, 6, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(367, 12, 267, 6, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(368, 12, 268, 6, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(369, 12, 269, 6, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(370, 12, 270, 6, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(371, 12, 275, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(372, 12, 276, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(373, 12, 277, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(374, 12, 278, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(375, 12, 279, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(376, 12, 280, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(377, 12, 281, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(378, 12, 282, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(379, 12, 283, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(380, 12, 284, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(381, 12, 285, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(382, 12, 286, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(383, 12, 287, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(384, 12, 288, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(385, 12, 289, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(386, 12, 290, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(387, 12, 291, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(388, 12, 292, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(389, 12, 293, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(390, 12, 294, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(391, 12, 295, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(392, 12, 296, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(393, 12, 297, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(394, 12, 298, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(395, 12, 299, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(396, 12, 300, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(397, 12, 301, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(398, 12, 302, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(399, 12, 303, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(400, 12, 304, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(401, 12, 305, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(402, 12, 306, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(403, 12, 307, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(404, 12, 308, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(405, 12, 309, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(406, 12, 310, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(407, 12, 311, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(408, 12, 312, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(409, 12, 313, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(410, 12, 314, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(411, 12, 315, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(412, 12, 316, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(413, 12, 317, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(414, 12, 318, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(415, 12, 319, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(416, 12, 320, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(417, 12, 321, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(418, 12, 322, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(419, 12, 323, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(420, 12, 324, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(421, 12, 325, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(422, 12, 326, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(423, 12, 327, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(424, 12, 328, 6, '2019-09-06 00:00:00', NULL, 'siges_migration_tool', NULL),
(425, 7, 164, 6, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 'admin', NULL),
(426, 7, 173, 6, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 'admin', NULL),
(427, 7, 162, 6, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 'admin', NULL),
(428, 7, 158, 6, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 'admin', NULL),
(429, 7, 166, 6, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 'admin', NULL),
(430, 7, 156, 6, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 'admin', NULL),
(431, 7, 165, 6, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 'admin', NULL),
(432, 8, 329, 6, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL),
(433, 8, 330, 6, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL),
(434, 16, 331, 6, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL),
(435, 16, 332, 6, '2019-09-14 00:00:00', '2019-09-14 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `rpt_custom`
--

CREATE TABLE `rpt_custom` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `parameters` text,
  `academic_year` int(11) DEFAULT NULL,
  `categorie` int(11) NOT NULL,
  `variables` text,
  `setup_variable` text,
  `create_by` varchar(64) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rpt_custom`
--

INSERT INTO `rpt_custom` (`id`, `title`, `data`, `parameters`, `academic_year`, `categorie`, `variables`, `setup_variable`, `create_by`, `create_date`) VALUES
(1, 'Liste des matieres par année académique et par classe', 'SELECT DISTINCT(s.subject_name) AS \'Nom matière\', s.short_subject_name AS \'Code matière\' FROM courses c INNER JOIN subjects s ON (s.id = c.subject) INNER JOIN rooms r ON (r.id = c.room) INNER JOIN levels l ON (l.id = r.level) \r\nWHERE academic_period = {%annee_academique%} AND l.id = {%classe%}                                                                        ', '', NULL, 4, 'annee_academique,classe', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC","dynamic-combo1":"SELECT id,level_name FROM `levels` ORDER BY id ASC"}}', '_developer_', '2018-05-12 22:54:54'),
(2, 'Liste des cours par année académique et par salle', ' SELECT DISTINCT(s.subject_name) AS \'Nom matière\', s.short_subject_name AS \'Code matière\', CONCAT(p.first_name,\' \',p.last_name) AS \'Nom Professeurs\', IF(p.gender=1,\'Féminin\',\'Masculin\') AS \'SEXE\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN subjects s ON (s.id = c.subject) \r\nINNER JOIN rooms r ON (r.id = c.room) \r\nWHERE academic_period = {%annee_academique%} AND r.id = {%salle%}      \r\nORDER BY s.subject_name ASC                                                                 ', '', NULL, 4, 'annee_academique,salle', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC","dynamic-combo1":"SELECT id,short_room_name FROM rooms ORDER BY short_room_name"}}', '_developer_', '2018-05-12 17:09:21'),
(3, 'Liste des cours par année académique, par salle et par coefficient', 'SELECT DISTINCT(s.subject_name) AS \'Nom matière\', s.short_subject_name AS \'Code matière\',c.weight AS \'Coefficient\', CONCAT(p.first_name,\' \',p.last_name) AS \'Nom Professeurs\', IF(p.gender=1,\'Féminin\',\'Masculin\') AS \'SEXE\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN subjects s ON (s.id = c.subject) \r\nINNER JOIN rooms r ON (r.id = c.room) \r\nWHERE academic_period = {%annee_academique%} AND r.id = {%salle%} AND c.weight = {%coefficient%}                                                                     ', '', NULL, 4, 'annee_academique,salle,coefficient', '{"data_type":["dynamic-combobox","dynamic-combobox","txt"],"param_value":{"static-combo0":"","static-combo1":"","static-combo2":"","dynamic-combo0":"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC","dynamic-combo1":"SELECT id, room_name FROM rooms","dynamic-combo2":""}}', '_developer_', '2018-05-16 13:05:18'),
(4, 'Nombre de cours total pour une année donnée', 'SELECT count(c.id) AS \'Nombre de cours\' FROM courses c \r\nWHERE c.academic_period = {%annee_academique%}', '', NULL, 4, 'annee_academique', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC"}}', '_developer_', '2018-05-16 14:37:23'),
(5, 'Nombre de cours par année académique et par salle', 'SELECT count(c.id) AS \'Nombre de cours\' FROM courses c \r\nINNER JOIN rooms r ON (r.id = c.room) \r\nWHERE c.academic_period = {%annee_academique%} AND r.id = {%salle%}                                                                             ', '', NULL, 4, 'annee_academique,salle', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC","dynamic-combo1":"SELECT id, room_name FROM rooms"}}', '_developer_', '2018-05-16 20:47:59'),
(6, 'Liste des professeurs actifs par classe pour l\'année en cours', 'SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', l.short_level_name AS \'Classe\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end AND l.id = {%classe%}    ORDER BY Nom ASC                                                                                                                                                                   ', '', NULL, 5, 'classe', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id, level_name FROM levels"}}', '_developer_', '2018-05-16 15:14:33'),
(7, 'Liste de tous les professeurs', 'SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\' , l.level_name AS \'Classe\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end\r\nORDER BY l.level_name                                                                                                                           ', '', NULL, 5, '', NULL, '_developer_', '2018-05-17 00:26:58'),
(8, 'Liste éleves par statut pour l\'année en cours', 'SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\')  AS \'Sexe\', DATE_FORMAT(p.birthday,"%e %M %Y" ) AS \'Date de naissance\'  FROM persons p \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id)\r\nINNER JOIN academicperiods a ON (a.id = lhp.academic_year) \r\nWHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active  = {%statut%} ORDER BY p.last_name ASC                                                                                                                                                                                                                       ', '', NULL, 1, 'statut', '{"data_type":["static-combobox"],"param_value":{"static-combo0":"2:Nouveau,1:Actif,0:Inactif","dynamic-combo0":""}}', '_developer_', '2018-08-20 14:20:29'),
(9, 'Liste élèves actifs par salle pour l\'année en cours', 'SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\',  p.phone AS \'Téléphone\', r.short_room_name AS \'Salle\' FROM persons p INNER JOIN room_has_person rhp ON (rhp.students = p.id)  INNER JOIN academicperiods a ON (a.id = rhp.academic_year)  INNER JOIN rooms r ON (r.id = rhp.room) WHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active IN (1,2) AND rhp.room = {%salle%}   ORDER BY p.last_name ASC                                                                        ', '', NULL, 1, 'salle', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id, short_room_name FROM `rooms` order BY short_room_name ASC"}}', '_developer_', '2018-08-20 14:29:06'),
(10, 'Moyenne par période et par salle pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\', r.short_room_name AS \'Salle\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  \r\nINNER JOIN persons p ON (p.id = abp.student)\r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE eby.academic_year = {%periode%} AND r.id = {%salle%}                                                                                                ', '', NULL, 1, 'periode,salle', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period","dynamic-combo1":"SELECT id,short_room_name FROM rooms ORDER BY short_room_name"}}', '_developer_', '2018-07-05 14:42:32'),
(11, 'Moyenne par periode et par classe pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\',  l.short_level_name AS \'Classe\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  \r\nINNER JOIN persons p ON (p.id = abp.student)\r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE eby.academic_year = {%periode%} AND l.id = {%classe%}                                                                                               ', '', NULL, 1, 'periode,classe', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period","dynamic-combo1":"SELECT id,short_level_name FROM `levels` ORDER BY short_level_name"}}', '_developer_', '2018-07-05 11:34:13'),
(12, 'Moyenne entre une intervalle par classe et par periode pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\',  l.short_level_name AS \'Classe\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  INNER JOIN persons p ON (p.id = abp.student)INNER JOIN room_has_person rhp ON (rhp.students = p.id)INNER JOIN rooms r ON (r.id = rhp.room)INNER JOIN levels l ON (l.id = r.level)INNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) INNER JOIN academicperiods a ON (a.id = eby.academic_year)WHERE eby.academic_year = {%periode%} AND l.id = {%classe%}   AND abp.average BETWEEN {%moyenne_minimale%} AND {%moyenne_maximale%}                                               ', '', NULL, 1, 'periode,classe,moyenne_minimale,moyenne_maximale', '{"data_type":["dynamic-combobox","dynamic-combobox","txt","txt"],"param_value":{"static-combo0":"","static-combo1":"","static-combo2":"","static-combo3":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period","dynamic-combo1":"SELECT id,short_level_name FROM `levels` ORDER BY short_level_name","dynamic-combo2":"","dynamic-combo3":""}}', '_developer_', '2018-07-18 15:59:33'),
(13, 'Liste d\'elèves par salle, par matière, par période et sur condition d\'une note x pour l\'année en cours', 'SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\',\'Féminin\') AS \'Sexe\', s.subject_name AS \'Matière\', g.grade_value AS \'Note\', c.weight AS \'Coefficient\', a.name_period AS \'Période\', r.short_room_name AS \'Salle\'\r\nFROM grades g \r\nINNER JOIN persons p ON (p.id = g.student)\r\nINNER JOIN courses c ON (c.id = g.course) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN subjects s ON (s.id = c.subject) \r\nINNER JOIN evaluation_by_year eby ON (eby.id = g.evaluation) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE r.id = {%salle%}  AND s.id = {%matiere%} AND a.id = {%periode%} AND grade_value {%condition%}  {%note%}                                                                                     ', '', NULL, 1, 'salle,matiere,periode,condition,note', '{"data_type":["dynamic-combobox","dynamic-combobox","dynamic-combobox","static-combobox","txt"],"param_value":{"static-combo0":"","static-combo1":"","static-combo2":"","static-combo3":"=:Egale,>:Sup\\u00e9rieure,<:Inf\\u00e9rieure,>=:Sup\\u00e9rieure ou \\u00e9gale,<=:Inf\\u00e9rieure ou \\u00e9gale","static-combo4":"","dynamic-combo0":"SELECT id, short_room_name FROM `rooms` order BY short_room_name ASC","dynamic-combo1":"SELECT id, subject_name FROM subjects ORDER BY subject_name ASC","dynamic-combo2":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period","dynamic-combo3":"","dynamic-combo4":""}}', '_developer_', '2018-08-20 14:22:23'),
(14, 'Liste d\'elèves  par période et sur condition d\'une moyenne x pour l\'année en cours', 'SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\',  l.short_level_name AS \'Classe\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  \r\nINNER JOIN persons p ON (p.id = abp.student)\r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE eby.academic_year = {%periode%} AND  abp.average {%condition%}  {%valeur_moyenne%}                                                                                                                      ', '', NULL, 1, 'periode,condition,valeur_moyenne', '{"data_type":["dynamic-combobox","static-combobox","txt"],"param_value":{"static-combo0":"","static-combo1":"=:Egale,>:Sup\\u00e9rieure,<:Inf\\u00e9rieure,>=:Sup\\u00e9rieure ou \\u00e9gale,<=:Inf\\u00e9rieure ou \\u00e9gale","static-combo2":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period","dynamic-combo1":"","dynamic-combo2":""}}', '_developer_', '2018-08-20 14:22:43'),
(15, 'Liste de décision finale par année académique et par classe', 'SELECT UPPER(p.last_name) AS \'Nom\', p.first_name AS \'Prénom\', IF(p.gender = 0, \'Masculin\',\'Féminin\') AS \'Sexe\', DATE_FORMAT(p.birthday,"%e %M %Y") AS \'Date de naissance\', c.city_name AS \'Lieu de naissance\', df.general_average AS \'Moyenne générale\', l.short_level_name AS \'Classe\', df.mention AS \'Mention\'  FROM decision_finale df \r\nINNER JOIN persons p ON (p.id = df.student) \r\nINNER JOIN cities c ON (c.id = p.cities)\r\nINNER JOIN levels l ON (df.current_level = l.id)\r\nWHERE df.academic_year = {%annee_academique%} AND df.current_level = {%classe%};                                                                                                                                                                     ', '', NULL, 1, 'annee_academique,classe', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1 ORDER BY id DESC","dynamic-combo1":"SELECT id, level_name FROM levels"}}', '_developer_', '2018-07-14 15:08:19'),
(16, 'Tri des élèves désactivés par motif pour l\'année en cours', 'SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\', DATE_FORMAT(p.birthday,"%e %M %Y" ) AS \'Date de naissance\' FROM persons p INNER JOIN level_has_person lhp ON (lhp.students = p.id) INNER JOIN academicperiods a ON (a.id = lhp.academic_year) WHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active = 0 and inactive_reason={%motif%} ORDER BY p.last_name ASC                                                                                                                                                                         ', '', NULL, 1, 'motif', '{"data_type":["static-combobox"],"param_value":{"static-combo0":"1:Abandon,2:Expulsion,3:Maladie,4:Voyage","dynamic-combo0":""}}', '_developer_', '2018-08-20 14:25:31'),
(17, 'Liste éleves désactivés pour l\'année en cours', 'SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\',  l.level_name AS \'Classe\', CASE WHEN inactive_reason =1 THEN \'Abandon\' WHEN inactive_reason =2 THEN \'Expulsion\' WHEN inactive_reason =3 THEN \'Maladie\'  WHEN inactive_reason =4 THEN \'Voyage\'  ELSE NULL END AS \'Motif\' FROM persons p INNER JOIN level_has_person lhp ON (lhp.students = p.id) INNER JOIN levels l ON (lhp.level = l.id) INNER JOIN academicperiods a ON (a.id = lhp.academic_year) WHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active = 0 ORDER BY p.last_name ASC                                                                                                                                                                                                                                                                                                                                                 ', '', NULL, 1, '', NULL, '_developer_', '2018-08-20 14:35:42'),
(18, 'Liste élèves en retard de paiement par année académique', 'SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\',  IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\', SUM(ba.balance) AS \'Montant\', l.short_level_name AS \'Classe\' , a.name_period AS \'Année académique\'  FROM persons p INNER JOIN balance ba ON (ba.student = p.id)\r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id)\r\nINNER JOIN levels l ON (l.id = lhp.level) \r\nINNER JOIN academicperiods a ON (a.id = lhp.academic_year) \r\nWHERE ba.balance > 0 AND lhp.academic_year = {%annee_academique%} \r\nGROUP BY p.id ', '', NULL, 6, 'annee_academique', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1 ORDER BY id DESC"}}', '_developer_', '2018-08-20 14:36:24'),
(19, 'Liste élèves en retard de paiement par salle par année académique', ' SELECT p.id_number AS \'Code\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\', SUM(ba.balance) AS \'Montant\', r.short_room_name AS \'Salle\', a.name_period AS \'Année académique\' FROM persons p \r\nINNER JOIN balance ba ON (ba.student = p.id) \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id) \r\nINNER JOIN levels l ON (l.id = lhp.level) \r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN academicperiods a ON (a.id = lhp.academic_year) \r\nWHERE ba.balance > 0 AND lhp.academic_year = {%annee_academique%} AND r.id = {%salle%} GROUP BY p.id                                                                                                                                                                  ', '', NULL, 6, 'salle,annee_academique', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id, room_name FROM `rooms` ","dynamic-combo1":"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1  ORDER BY id DESC"}}', '_developer_', '2018-08-20 14:38:16'),
(21, 'Total recette-scolarite entre 2 dates', 'SELECT SUM(amount_pay) AS \'Total recette scolarite\' FROM `billings` b INNER JOIN fees f ON(b.fee_period=f.id) INNER JOIN fees_label fl ON(f.fee=fl.id) WHERE fl.status=1 AND date_pay BETWEEN \'{%date_debut%}\' AND \'{%date_fin%}\'                                                 ', '', NULL, 6, 'date_debut,date_fin', '{"data_type":["date","date"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"","dynamic-combo1":""}}', '_developer_', '2018-07-18 09:34:15'),
(22, 'Total autres-recettes entre 2 dates', 'SELECT SUM(amount) AS \'Total autres-recettes \' FROM `other_incomes` oi INNER JOIN other_incomes_description oid ON(oi.id_income_description=oid.id) WHERE  income_date BETWEEN \'{%date_debut%}\' AND \'{%date_fin%}\'                         ', '', NULL, 6, 'date_debut,date_fin', '{"data_type":["date","date"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"","dynamic-combo1":""}}', '_developer_', '2018-07-18 09:44:02'),
(23, 'Total recette-scolarite par versement et par année académiquee', 'SELECT fl.fee_label AS \'Versement\', SUM(amount_pay) AS \'Total recette scolarite\' FROM `billings` b INNER JOIN fees f ON(b.fee_period=f.id) INNER JOIN fees_label fl ON(f.fee=fl.id) WHERE fl.status=1 AND fl.id={%versement%} AND b.academic_year={%annee_academique%}                             ', '', NULL, 6, 'versement,annee_academique', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id, fee_label FROM `fees_label` WHERE status = 1  AND fee_label NOT LIKE \'Pending balance\' ","dynamic-combo1":"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1  ORDER BY id DESC"}}', '_developer_', '2018-07-19 22:16:04'),
(24, 'Total recette-scolarite par mois et par année académiquee', 'SELECT CASE WHEN MONTH(date_pay) =1 THEN \'Janvier\' WHEN MONTH(date_pay) =2 THEN \'Février\' WHEN MONTH(date_pay) =3 THEN \'Mars\'  WHEN MONTH(date_pay) =4 THEN \'Avril\'   WHEN MONTH(date_pay) =5 THEN \'MAi\'  WHEN MONTH(date_pay) =6 THEN \'Juin\'  WHEN MONTH(date_pay) =7 THEN \'Juillet\'  WHEN MONTH(date_pay) =8 THEN \'Août\'  WHEN MONTH(date_pay) =9 THEN \'Septembre\'  WHEN MONTH(date_pay) =10 THEN \'Octobre\'  WHEN MONTH(date_pay) =11 THEN \'Novembre\'  WHEN MONTH(date_pay) =12 THEN \'Décembre\' ELSE NULL END AS \'MOIS\', SUM(amount_pay) AS \'Total recette scolarite\' FROM `billings` b INNER JOIN fees f ON(b.fee_period=f.id) INNER JOIN fees_label fl ON(f.fee=fl.id) WHERE fl.status=1 AND MONTH(date_pay)={%mois%} AND b.academic_year={%annee_academique%}                                                 ', '', NULL, 6, 'mois,annee_academique', '{"data_type":["static-combobox","dynamic-combobox"],"param_value":{"static-combo0":"1:Janvier,2:F\\u00e9vrier,3:Mars,4:Avril,5:Mai,6:Juin,7:Juillet,8:Ao\\u00fbt,9:Septembre,10:Octobre,11:Novembre,12:D\\u00e9cembre","static-combo1":"","dynamic-combo0":"","dynamic-combo1":"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1  ORDER BY id DESC"}}', '_developer_', '2018-07-18 10:12:05'),
(25, 'Liste des plus fortes moyennes des salles par période pour l\'année en cours', ' SELECT first_name AS \'Prénom\',last_name AS \'Nom\',short_room_name AS \'Salle\',MaxAverage AS \'Moyenne\'\r\n            FROM average_by_period abp1\r\n            INNER JOIN persons p ON(p.id=abp1.student)\r\n            INNER JOIN ( SELECT student,MAX(average) as MaxAverage,room_name,short_room_name,AcademicYear\r\n			            FROM average_by_period abp \r\n			            INNER JOIN evaluation_by_year eby ON(eby.id=abp.evaluation_by_year)\r\n			            INNER JOIN ( SELECT students,room_name,short_room_name, ap.id as AcademicYear \r\n			                          FROM room_has_person rhp INNER JOIN rooms r ON(r.id=rhp.room)  INNER JOIN academicperiods ap ON(ap.id=rhp.academic_year)\r\n			                           WHERE NOW() BETWEEN ap.date_start AND ap.date_end) gpRoom \r\n			                      ON eby.academic_year= {%periode%} and abp.student=gpRoom.students \r\n			          GROUP BY room_name\r\n            \r\n                      )gpWithMax\r\n                ON gpWithMax.MaxAverage=abp1.average and abp1.academic_year=gpWithMax.AcademicYear      ', '', NULL, 1, 'periode', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period"}}', '_developer_', '2018-07-23 17:21:43'),
(26, 'Liste des plus fortes moyennes des classes par période pour l\'année en cours', 'SELECT  l.short_level_name AS \'Classe\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', a.name_period AS \'Période\', abp.average AS \'Moyenne\' FROM persons p \r\nINNER JOIN average_by_period abp ON (abp.student = p.id) \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id  AND abp.academic_year=lhp.academic_year)\r\nINNER JOIN levels l ON (l.id = lhp.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nINNER JOIN \r\n(\r\n    SELECT DISTINCT student, max(average) max_average, av.academic_year \r\n    FROM average_by_period av INNER JOIN level_has_person lep ON (lep.students = av.student  AND av.academic_year=lep.academic_year) \r\n    INNER JOIN evaluation_by_year e ON (e.id = av.evaluation_by_year) \r\n    INNER JOIN academicperiods a ON (a.id = e.academic_year)\r\n    WHERE e.academic_year = {%periode%}\r\n    GROUP BY lep.level\r\n)\r\nc ON  (abp.average = c.max_average  AND abp.academic_year= c.academic_year )\r\nWHERE eby.academic_year = {%periode%}                                       ', '', NULL, 1, 'periode', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period"}}', '_developer_', '2019-06-05 14:23:36'),
(27, 'Liste des plus faibles moyennes des classes par période pour l\'année en cours', 'SELECT  l.short_level_name AS \'Classe\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', a.name_period AS \'Période\', abp.average AS \'Moyenne\' FROM persons p \r\nINNER JOIN average_by_period abp ON (abp.student = p.id) \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id  AND abp.academic_year=lhp.academic_year)\r\nINNER JOIN levels l ON (l.id = lhp.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nINNER JOIN \r\n(\r\n    SELECT DISTINCT student, min(average) min_average, av.academic_year \r\n    FROM average_by_period av INNER JOIN level_has_person lep ON (lep.students = av.student  AND av.academic_year=lep.academic_year) \r\n    INNER JOIN evaluation_by_year e ON (e.id = av.evaluation_by_year) \r\n    INNER JOIN academicperiods a ON (a.id = e.academic_year)\r\n    WHERE e.academic_year = {%periode%}\r\n    GROUP BY lep.level\r\n)\r\nc ON  (abp.average = c.min_average  AND abp.academic_year= c.academic_year )\r\nWHERE eby.academic_year = {%periode%}                                                ', '', NULL, 1, 'periode', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period"}}', '_developer_', '2019-06-05 14:41:18'),
(28, 'Liste des plus faibles moyennes des salles par période pour l\'année en cours', 'SELECT  r.short_room_name AS \'Salle\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', a.name_period AS \'Période\', abp.average AS \'Moyenne\' FROM persons p \r\nINNER JOIN average_by_period abp ON (abp.student = p.id) \r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nINNER JOIN \r\n(\r\n    SELECT student, min(average) min_average \r\n    FROM average_by_period av INNER JOIN room_has_person rop ON (rop.students = av.student) \r\n    INNER JOIN evaluation_by_year e ON (e.id = av.evaluation_by_year) \r\n	INNER JOIN academicperiods a ON (a.id = e.academic_year)\r\n    WHERE e.academic_year = {%periode%}\r\n    GROUP BY rop.room\r\n)\r\nc ON  abp.average = c.min_average\r\nWHERE eby.academic_year = {%periode%}                                                                                                                                                                                                                                                                                                                                                       ', '', NULL, 1, 'periode', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period"}}', '_developer_', '2018-07-23 17:31:39'),
(29, 'Liste des professeurs actifs par salle pour l\'année en cours', 'SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', r.short_room_name AS \'Salle\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end AND r.id = {%salle%}    ORDER BY Nom ASC                            ', '', NULL, 5, 'salle', '{"data_type":["dynamic-combobox"],"param_value":{"static-combo0":"","dynamic-combo0":"SELECT id, room_name FROM rooms"}}', '_developer_', '2018-07-21 15:07:44'),
(30, 'Liste des professeurs inactifs pour l\'année en cours', '                            SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', if(p.active=0,\'Inactif\',\'Actif\') AS \'Statut\'  FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end AND p.active=0\r\n                                                   ', '', NULL, 5, '', NULL, '_developer_', '2018-07-21 15:16:17'),
(31, 'Liste des employés inactifs ', '                             SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', if(p.active=0,\'Inactif\',\'Actif\') AS \'Statut\'  FROM persons p \r\nWHERE p.id NOT IN(SELECT teacher from courses) AND p.active=0                        ', '', NULL, 5, '', NULL, '_developer_', '2018-07-21 15:20:18'),
(32, 'Taux de réussite par salle et par période pour une année académique donnée', 'SELECT RoomName AS \'Salle\', TotalStudent AS \'Effectif\',TotalSuccess AS \'Succes\',CONCAT(ROUND(TotalSuccess/TotalStudent*100,2),\'%\') AS \'Taux de réussite\'\n									            FROM average_by_period abp \n									            \n                                                INNER JOIN (SELECT abp.student,COUNT(abp.student) AS TotalStudent,RoomId,RoomName,AcademicYear \n                                                              FROM average_by_period abp \n                                                               INNER JOIN evaluation_by_year eby ON(eby.id=abp.evaluation_by_year) \n                                                               INNER JOIN ( SELECT students,r.id as RoomId,short_room_name AS RoomName, ap.id as AcademicYear FROM room_has_person rhp \n                                                                             INNER JOIN rooms r ON(r.id=rhp.room) \n                                                                              INNER JOIN academicperiods ap ON(ap.id=rhp.academic_year) \n                                                                              WHERE rhp.academic_year={%annee_academique%}) gpRoom \n                                                                    ON eby.academic_year= {%periode%} and abp.student=gpRoom.students \n                                                            GROUP BY RoomId\n                                                 )gpTotalStud\n                                               \n                                               ON(abp.student=gpTotalStud.student)\n                                                \n                                                INNER JOIN (SELECT abp1.student,COUNT(abp1.student) AS TotalSuccess,RoomId1,AcademicYear1\n                                                             FROM average_by_period abp1 \n                                                             INNER JOIN evaluation_by_year eby ON(eby.id=abp1.evaluation_by_year)\n                                                              INNER JOIN ( SELECT students,r.id as RoomId1,room_name,short_room_name, ap.id as AcademicYear1 \n									                                         FROM room_has_person rhp INNER JOIN rooms r ON(r.id=rhp.room)  INNER JOIN academicperiods ap ON(ap.id=rhp.academic_year)\n									                                            WHERE rhp.academic_year={%annee_academique%}) gpRoom1 \n									                                  ON eby.academic_year= {%periode%} and abp1.student=gpRoom1.students\n                                                                 WHERE abp1.average >=(SELECT minimum_passing \n																                          FROM passing_grades pg \n																                          INNER JOIN rooms ro ON (ro.level = pg.level) \n																						  WHERE ro.id = gpRoom1.RoomId1 and pg.academic_period={%annee_academique%} GROUP BY gpRoom1.short_room_name)      \n                                                               GROUP BY RoomId1				\n							                              )gpSuccess\n                                                     ON(gpTotalStud.RoomId=gpSuccess.RoomId1 and gpTotalStud.AcademicYear=gpSuccess.AcademicYear1 )\n                                                          GROUP BY RoomName', '', NULL, 1, 'periode,annee_academique', '{"data_type":["dynamic-combobox","dynamic-combobox"],"param_value":{"static-combo0":"","static-combo1":"","static-combo2":"","static-combo3":"","dynamic-combo0":"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period","dynamic-combo1":"SELECT id, name_period FROM academicperiods WHERE is_year = 1"}}', 'developer', '2019-01-23 10:29:32'),
(33, 'Liste des postes', '                                                                                SELECT title_name FROM titles ORDER BY title_name ASC                                                ', '', NULL, 5, '', NULL, 'admin', '2019-08-31 01:55:40');

-- --------------------------------------------------------

--
-- Table structure for table `rpt_custom_cat`
--

CREATE TABLE `rpt_custom_cat` (
  `id` int(11) NOT NULL,
  `categorie_name` varchar(255) NOT NULL,
  `cat` varchar(32) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `update_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rpt_custom_cat`
--

INSERT INTO `rpt_custom_cat` (`id`, `categorie_name`, `cat`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'Elèves', 'stud', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 'Matières et Cours', 'mec', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 'Staff', 'prof', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 'Economat', 'eco', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sale_transaction`
--

CREATE TABLE `sale_transaction` (
  `id` int(11) NOT NULL,
  `id_transaction` int(11) NOT NULL,
  `amount_sale` float NOT NULL,
  `discount` float DEFAULT NULL,
  `amount_receive` float NOT NULL,
  `amount_balance` float NOT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scalendar`
--

CREATE TABLE `scalendar` (
  `id` int(11) NOT NULL,
  `c_title` varchar(255) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `is_all_day_event` smallint(6) NOT NULL,
  `color` varchar(200) DEFAULT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scalendar`
--

INSERT INTO `scalendar` (`id`, `c_title`, `location`, `description`, `start_date`, `end_date`, `start_time`, `end_time`, `is_all_day_event`, `color`, `academic_year`) VALUES
(1, 'Publication de la version 2.0 de SIGES', '', '', '2019-07-31', NULL, '00:00:00', '00:00:00', 0, 'FFFFFF', 1),
(2, 'Présentation des nouveautés de SIGES à nos clients', '', '', '2019-08-20', NULL, '00:00:00', '00:00:00', 0, 'FFFFFF', 1),
(3, 'Ouverture des classes année 2019-2010', '', '', '2019-09-09', NULL, '00:00:00', '00:00:00', 0, 'FFFFFF', 1),
(4, 'Formation', '', '', '2019-08-21', NULL, '00:00:00', '00:00:00', 0, 'FFFFFF', 1),
(5, 'Ouverture des classes', '', '', '2019-09-09', '2019-09-09', '00:00:00', '00:00:00', 0, 'FFFFFF', 6),
(6, 'Réunion des parents', '', '', '2019-10-30', '2019-10-30', '00:00:00', '00:00:00', 0, 'FFFFFF', 6),
(7, 'Mort de Dessalines', '', '', '2019-10-17', '2019-10-17', '00:00:00', '00:00:00', 0, 'FFFFFF', 6);

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `day_course` varchar(32) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_agenda`
--

CREATE TABLE `schedule_agenda` (
  `id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `c_description` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `is_all_day_event` smallint(6) NOT NULL DEFAULT '0',
  `color` varchar(200) DEFAULT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_holder`
--

CREATE TABLE `scholarship_holder` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `partner` int(11) DEFAULT NULL,
  `fee` int(11) DEFAULT NULL COMMENT 'Do not let NULL value if it is not a whole scholarship please specify',
  `percentage_pay` double NOT NULL,
  `is_internal` tinyint(1) NOT NULL DEFAULT '0',
  `academic_year` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `scholarship_holder`
--

INSERT INTO `scholarship_holder` (`id`, `student`, `partner`, `fee`, `percentage_pay`, `is_internal`, `academic_year`, `comment`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 129, NULL, NULL, 100, 1, 1, NULL, '2019-07-04 17:07:36', NULL, 'admin', NULL),
(2, 236, 2, 7, 100, 0, 1, NULL, '2019-08-06 10:08:31', NULL, 'admin', NULL),
(3, 154, NULL, 3, 100, 1, 1, 'admin a exempté cette balance. (2019-08-06) ', '2019-08-06 00:00:00', NULL, NULL, NULL),
(4, 165, NULL, NULL, 100, 1, 1, NULL, '2019-08-08 00:08:00', NULL, 'admin', NULL),
(5, 193, 1, 33, 100, 0, 6, NULL, '2019-08-17 12:08:42', NULL, 'admin', NULL),
(6, 193, 1, 27, 75, 0, 6, NULL, '2019-08-17 12:08:42', NULL, 'admin', NULL),
(7, 201, 2, NULL, 100, 0, 6, NULL, '2019-08-17 12:08:32', NULL, 'admin', NULL),
(8, 191, NULL, NULL, 100, 0, 6, NULL, '2019-08-17 18:08:21', NULL, 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `section_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `section_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Fondamental', '2014-09-23 00:00:00', '2014-09-23 00:00:00', NULL, NULL),
(2, 'Secondaire', '2014-09-23 00:00:00', '2015-08-20 02:08:20', NULL, NULL),
(3, 'Préscolaire', '2019-07-26 00:00:00', '2019-07-26 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `section_has_cycle`
--

CREATE TABLE `section_has_cycle` (
  `id` int(11) NOT NULL,
  `cycle` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `section_has_cycle`
--

INSERT INTO `section_has_cycle` (`id`, `cycle`, `section`, `level`, `academic_year`) VALUES
(1, 1, 1, 1, 1),
(2, 1, 1, 2, 1),
(3, 1, 1, 3, 1),
(4, 2, 2, 4, 1),
(5, 3, 1, 5, 1),
(6, 3, 1, 6, 1),
(7, 4, 3, 7, 1),
(8, 4, 3, 8, 1),
(9, 2, 2, 11, 1),
(10, 4, 3, 12, 1),
(11, 5, 1, 13, 1),
(12, 5, 1, 14, 1),
(13, 5, 1, 15, 1),
(14, 3, 1, 16, 1),
(15, 2, 2, 10, 1),
(16, 2, 1, 9, 1),
(17, 2, 2, 9, 1),
(18, 1, 1, 1, 6),
(19, 1, 1, 2, 6),
(20, 1, 1, 3, 6),
(21, 2, 2, 4, 6),
(22, 3, 1, 5, 6),
(23, 3, 1, 6, 6),
(24, 4, 3, 7, 6),
(25, 4, 3, 8, 6),
(26, 2, 2, 11, 6),
(27, 4, 3, 12, 6),
(28, 5, 1, 13, 6),
(29, 5, 1, 14, 6),
(30, 5, 1, 15, 6),
(31, 3, 1, 16, 6),
(32, 2, 2, 10, 6),
(33, 2, 1, 9, 6),
(34, 2, 2, 9, 6);

-- --------------------------------------------------------

--
-- Table structure for table `sellings`
--

CREATE TABLE `sellings` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `id_products` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `selling_date` datetime NOT NULL,
  `client_name` varchar(128) DEFAULT NULL,
  `sell_by` varchar(64) DEFAULT NULL,
  `amount_receive` float DEFAULT NULL,
  `amount_selling` float DEFAULT NULL,
  `amount_balance` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `unit_selling_price` float DEFAULT NULL,
  `is_return` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  `user_id` int(11) NOT NULL,
  `last_activity` datetime NOT NULL,
  `last_ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(45) NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `shift_name`, `time_start`, `time_end`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Matin', '07:30:00', '15:00:00', '2014-09-23 00:00:00', '2019-08-10 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `siges_payment`
--

CREATE TABLE `siges_payment` (
  `id` int(11) NOT NULL,
  `id_siges_payment_set` int(11) NOT NULL,
  `amount_pay` double NOT NULL,
  `balance` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `siges_payment_set`
--

CREATE TABLE `siges_payment_set` (
  `id` int(11) NOT NULL,
  `old_balance` double NOT NULL,
  `amount_to_pay` double NOT NULL,
  `devise` int(11) DEFAULT NULL,
  `display_on` date DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `acquisition_date` date NOT NULL,
  `buiying_price` float DEFAULT NULL,
  `selling_price` float DEFAULT NULL,
  `is_donation` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stock_history`
--

CREATE TABLE `stock_history` (
  `id` int(11) NOT NULL,
  `id_stock` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `buying_date` date NOT NULL,
  `buying_price` float NOT NULL,
  `selling_price` float NOT NULL,
  `create_by` varchar(64) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_documents`
--

CREATE TABLE `student_documents` (
  `id` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `file_name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_other_info`
--

CREATE TABLE `student_other_info` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `school_date_entry` date DEFAULT NULL,
  `leaving_date` datetime NOT NULL,
  `previous_school` varchar(255) DEFAULT NULL,
  `previous_level` varchar(45) DEFAULT NULL,
  `apply_for_level` varchar(45) DEFAULT NULL,
  `health_state` varchar(255) NOT NULL,
  `father_full_name` varchar(45) NOT NULL,
  `mother_full_name` varchar(100) NOT NULL,
  `person_liable` varchar(100) NOT NULL,
  `person_liable_phone` varchar(65) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `student_other_info`
--

INSERT INTO `student_other_info` (`id`, `student`, `school_date_entry`, `leaving_date`, `previous_school`, `previous_level`, `apply_for_level`, `health_state`, `father_full_name`, `mother_full_name`, `person_liable`, `person_liable_phone`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 103, '0000-00-00', '0000-00-00 00:00:00', 'Ecole Chavannes', '2', NULL, 'Mal de tête', 'Ronald Auguste', 'Andréa Auguste', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(2, 210, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, '', '', '', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(3, 228, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(4, 229, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(5, 230, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(6, 231, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(7, 232, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(8, 233, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(9, 234, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, 'NY', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(10, 235, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(11, 236, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(12, 237, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(13, 238, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-06 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(14, 239, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-06 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(15, 240, '2019-07-13', '0000-00-00 00:00:00', '', NULL, '2', '', '', '', '', '', '2019-08-10 00:00:00', '0000-00-00 00:00:00', 'SIGES', NULL),
(16, 241, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(17, 242, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(18, 243, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(19, 244, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, '', '', '', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(20, 245, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(21, 247, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(22, 248, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(23, 249, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(24, 246, '0000-00-00', '0000-00-00 00:00:00', '', '3', NULL, '', '', '', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(25, 250, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(26, 251, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, '', '', '', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(27, 252, '0000-00-00', '0000-00-00 00:00:00', '', '3', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(28, 260, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-08-31 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(29, 53, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, '', '', '', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(30, 108, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, '', '', '', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(31, 262, '2019-08-19', '0000-00-00 00:00:00', 'Les Petits Génies', '12', NULL, '', '', '', '', '', '2019-09-01 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(32, 263, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-01 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(33, 265, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-01 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(34, 266, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(35, 267, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(36, 268, '0000-00-00', '0000-00-00 00:00:00', '', '10', NULL, '', '', '', '', '', '2019-09-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(37, 269, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(38, 270, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-05 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(39, 275, NULL, '0000-00-00 00:00:00', NULL, NULL, NULL, '', '', '', '', '', NULL, '0000-00-00 00:00:00', NULL, NULL),
(40, 329, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-14 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(41, 330, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-14 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(42, 331, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-14 00:00:00', '0000-00-00 00:00:00', NULL, NULL),
(43, 332, '0000-00-00', '0000-00-00 00:00:00', '', '', NULL, '', '', '', '', '', '2019-09-14 00:00:00', '0000-00-00 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(45) NOT NULL,
  `short_subject_name` varchar(5) DEFAULT NULL,
  `is_subject_parent` tinyint(1) DEFAULT NULL,
  `subject_parent` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL COMMENT '	',
  `create_by` varchar(45) DEFAULT NULL COMMENT '	',
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`, `short_subject_name`, `is_subject_parent`, `subject_parent`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Espagnol', 'ESPA', 0, 117, '2014-10-04 01:10:44', '2019-08-05 00:00:00', NULL, NULL),
(2, 'Anglais', 'ANGL', 0, 117, '2014-10-04 01:10:47', '2019-08-05 00:00:00', NULL, NULL),
(6, 'Créole', 'CREO', 0, 117, '2014-10-11 11:10:20', '2019-08-05 00:00:00', NULL, NULL),
(8, 'Biologie', 'BIOL', 0, 59, '2014-10-11 11:10:56', '2019-08-05 00:00:00', NULL, NULL),
(10, 'Sciences sociales', 'SCSO', 1, NULL, '2014-10-11 11:10:06', '2016-09-21 00:00:00', NULL, NULL),
(13, 'Savoir-vivre', 'SAVO', 0, 69, '2014-10-11 11:10:41', '2016-09-09 00:00:00', NULL, NULL),
(15, 'Chimie', 'CHIM', 0, 63, '2014-10-11 11:10:59', '2016-11-07 00:00:00', NULL, NULL),
(23, 'Physique', 'PHYS', 0, 63, '2014-10-16 05:10:30', '2016-09-09 00:00:00', NULL, NULL),
(40, 'Catéchèse', 'CAT', 0, 69, '2014-11-03 07:11:55', '2016-09-09 00:00:00', NULL, NULL),
(59, 'Sciences expérimentales  ', 'SCEX', 1, NULL, '2014-11-03 08:11:11', '2019-08-05 00:00:00', NULL, NULL),
(63, 'Sciences', 'SCIE', 1, NULL, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(64, 'Mathématiques', 'MATH', 1, NULL, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(66, 'Langues', 'LANG', 1, NULL, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(67, 'Littérature', 'LITT', 1, NULL, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(68, 'Communication française', 'COFR', 1, NULL, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(69, 'Culture et Religion', 'CUER', 1, NULL, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(70, 'Arts Plastiques', 'ARPL', 0, 69, '2016-09-09 13:09:11', NULL, '_jacques1003', NULL),
(71, 'Education physique et sport', 'EPS', 0, 69, '2016-09-09 13:09:33', '2019-08-05 00:00:00', '_jacques1003', NULL),
(72, 'Grammaire', 'GRAM', 0, 114, '2016-09-09 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(73, 'Communication écrite', 'COEC', 0, 114, '2016-09-09 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(74, 'Orthographe / Vocabulaire', 'ORVO', 0, 114, '2016-09-09 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(75, 'Pièces classiques', 'PICL', 0, 114, '2016-09-09 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(76, 'Texte et vocabulaire', 'TEEV', 0, 68, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(77, 'Algèbre ', 'ALG', 0, 64, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(78, 'Arithmétique', 'ARIT', 0, 64, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(79, 'Géométrie', 'GEO', 0, 64, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(80, 'Informatique', 'INFO', 0, 91, '2016-09-09 00:00:00', '2016-09-29 00:00:00', NULL, NULL),
(82, 'Physique électrique', 'PHE', 0, 63, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(83, 'Physique optique', 'PHO', 0, 63, '2016-09-09 00:00:00', '2018-10-25 00:00:00', NULL, NULL),
(84, 'Latin', 'LATI', 0, 66, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(86, 'Musique', 'MUSI', 0, 117, '2016-09-09 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(87, 'Conjugaison', 'CONJ', 0, 68, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(88, 'Français 1', 'FR1', 0, 67, '2016-09-09 00:00:00', '2018-09-28 00:00:00', NULL, NULL),
(89, 'Français 2', 'FR2', 0, 67, '2016-09-09 00:00:00', '2018-09-28 00:00:00', NULL, NULL),
(90, 'Méthodologie', 'MET', 0, 67, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(91, 'Technologie', 'TECH', 1, NULL, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(92, 'Volet professionnel', 'VOPR', 0, 91, '2016-09-09 00:00:00', '2016-12-21 00:00:00', NULL, NULL),
(93, 'Algèbre et Géométrie', 'ALEG', 0, 64, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(94, 'Analyse', 'ANAL', 0, 64, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(95, 'Philosophie', 'PHIL', 0, 67, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(96, 'Physiologie', 'PHYS', 0, 63, '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(97, 'Alg/Géom/Proba', 'ALG/', 0, 64, '2016-09-09 00:00:00', '2016-10-21 00:00:00', NULL, NULL),
(98, 'Education à la citoyenneté', 'EDCI', 0, 10, '2016-09-21 13:09:10', '2017-10-05 00:00:00', '_developer_', NULL),
(99, 'Histoire ', 'HIS', 0, 10, '2016-09-29 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(100, 'Histoire de l\'art', 'HIDL', 0, 69, '2016-09-29 00:00:00', '2016-09-29 00:00:00', NULL, NULL),
(101, 'Initiation à la lecture', 'INLE', 0, 68, '2016-09-29 00:00:00', '2017-10-05 00:00:00', NULL, NULL),
(102, 'Texte', 'TEXT', 0, 68, '2016-10-14 00:00:00', '2016-10-14 00:00:00', NULL, NULL),
(103, 'Vocabulaire', 'VOCA', 0, 68, '2016-10-14 00:00:00', '2016-10-14 00:00:00', NULL, NULL),
(104, 'Géologie', 'GEOL', 0, 59, '2016-10-18 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(105, 'Economie', 'ECON', 0, 63, '2016-10-21 00:00:00', '2016-10-21 00:00:00', NULL, NULL),
(106, 'Psychologie', 'PSYC', 0, 67, '2016-10-21 00:00:00', '2018-09-05 00:00:00', NULL, NULL),
(107, 'Analyse/Suite', 'ANAL', 0, 64, '2016-10-21 00:00:00', '2016-10-21 00:00:00', NULL, NULL),
(108, 'Probabilité', 'PROB', 0, 64, '2016-10-21 16:10:56', NULL, '_jacques1003', NULL),
(109, 'Morale', 'MORA', 0, 69, '2016-10-24 00:00:00', '2016-10-24 00:00:00', NULL, NULL),
(110, 'Trigonométrie', 'TRIG', 0, 64, '2016-10-24 00:00:00', '2016-10-24 00:00:00', NULL, NULL),
(111, 'Suites', 'SUIT', 0, 64, '2016-10-25 00:00:00', '2016-10-25 00:00:00', NULL, NULL),
(112, 'Statistique et géométrie', 'STGE', 0, 64, '2017-10-05 00:00:00', '2017-10-05 00:00:00', NULL, NULL),
(113, 'Art et musique ', 'ARMU', 0, 69, '2018-09-05 00:00:00', '2018-09-05 00:00:00', NULL, NULL),
(114, 'Français ', 'FR', 1, NULL, '2018-09-28 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(115, 'Litterature Haitienne', 'LH', 1, NULL, '2018-10-04 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(116, 'Littérature française', 'LF', 0, 68, '2018-10-04 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(117, 'Langues / Arts', 'LAA', 1, NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(118, 'Art Oratoire', 'ARO', 0, 114, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(119, 'Lecture Expliquée', 'LEEX', 0, 114, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(120, 'Géographie', 'GEOG', 0, 10, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(121, 'Cultiure Générale', 'CUlGE', 0, 10, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(122, 'Sciences Physiques', 'SCIPH', 0, 59, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(123, 'Math Financière et Stat.', 'MAFS', 0, 64, '2019-08-05 00:00:00', '2019-08-05 00:00:00', NULL, NULL),
(124, 'Religion', 'REL', 0, NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(125, 'Sciences Appliquées', 'SA', 1, NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', NULL, NULL),
(126, 'Electronique', 'EL', 0, 125, '2019-09-05 15:09:57', NULL, 'admin', NULL),
(127, 'Mécanique', 'MEC', 0, 125, '2019-09-05 15:09:57', NULL, 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject_average`
--

CREATE TABLE `subject_average` (
  `academic_year` int(11) NOT NULL,
  `evaluation_by_year` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `average` double NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(100) NOT NULL,
  `update_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subject_average`
--

INSERT INTO `subject_average` (`academic_year`, `evaluation_by_year`, `course`, `average`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 1, 1, 85, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 2, 77, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 3, 76.59, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 4, 86.85, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 5, 79.67, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 6, 76.19, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 7, 80.63, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 8, 84.37, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 9, 83.48, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 10, 78.26, '2019-07-04', '2019-07-11', 'admin', 'admin'),
(1, 1, 11, 77.24, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 12, 78.4, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 13, 80.62, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 14, 79.08, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 15, 81.29, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 16, 82.92, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 17, 81.62, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 18, 81.81, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 19, 86.57, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 20, 78.71, '2019-07-24', '0000-00-00', 'admin', ''),
(1, 1, 21, 80.12, '2019-07-03', '0000-00-00', 'admin', ''),
(1, 1, 22, 87.2, '2019-07-03', '2019-07-03', 'admin', 'admin'),
(1, 1, 23, 76.76, '2019-07-03', '2019-07-03', 'admin', 'admin'),
(1, 1, 24, 82.08, '2019-07-03', '0000-00-00', 'admin', ''),
(1, 1, 25, 82.64, '2019-07-03', '0000-00-00', 'admin', ''),
(1, 1, 26, 65.28, '2019-07-03', '0000-00-00', 'admin', ''),
(1, 1, 27, 82.44, '2019-07-03', '0000-00-00', 'admin', ''),
(1, 1, 28, 88.44, '2019-07-03', '0000-00-00', 'admin', ''),
(1, 1, 29, 76.12, '2019-07-03', '2019-07-03', 'admin', 'admin'),
(1, 1, 30, 93.52, '2019-07-03', '0000-00-00', 'admin', ''),
(1, 1, 31, 75.08, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 32, 82.08, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 33, 72.58, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 34, 81.92, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 35, 75.88, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 36, 81, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 37, 74.54, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 38, 81.92, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 39, 95.65, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 40, 76.65, '2019-07-11', '0000-00-00', 'admin', ''),
(1, 1, 42, 98.1, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 43, 65.3, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 44, 34.2, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 45, 48.18, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 46, 75.91, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 47, 34, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 48, 80.18, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 49, 105.4, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 50, 57.6, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 52, 40, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 53, 40.5, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 54, 40, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 55, 40, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 56, 69.1, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 57, 40.1, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 58, 40.1, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 59, 72.1, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 60, 39.6, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 1, 61, 40.1, '2019-08-05', '2019-08-06', 'admin', '_estim'),
(1, 2, 1, 80.11, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 2, 75.96, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 3, 90.07, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 4, 81.96, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 5, 77.22, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 6, 82.81, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 7, 79.41, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 8, 67.78, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 9, 84.52, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 10, 73.19, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 11, 81.29, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 12, 71.19, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 13, 82.38, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 14, 81.29, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 15, 73.9, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 16, 56.95, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 17, 90.67, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 18, 78.29, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 19, 69.1, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 20, 72.29, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 21, 84.67, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 22, 82.46, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 23, 81.58, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 24, 91.25, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 25, 85.17, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 26, 66.42, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 27, 87.83, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 28, 74.83, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 29, 84, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 30, 82.46, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 31, 86.23, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 32, 80.54, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 33, 72.46, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 34, 67.5, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 35, 76.77, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 36, 87.96, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 37, 77.81, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 38, 75.46, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 39, 74.5, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 40, 86.65, '2019-08-10', '0000-00-00', 'admin', ''),
(1, 2, 41, 115.79, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 43, 86.42, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 44, 44.25, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 45, 178.92, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 46, 178.42, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 47, 91.42, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 48, 135.25, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 49, 134.5, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 50, 71.25, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 51, 71.58, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 52, 42, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 53, 44.42, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 54, 45, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 55, 42.33, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 56, 81.75, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 57, 45, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 58, 44.75, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 59, 88.42, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 60, 36.5, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(1, 2, 61, 44.33, '2019-08-10', '2019-08-10', 'admin', 'admin'),
(6, 5, 82, 64.62, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 83, 77.08, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 84, 70.15, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 85, 63.15, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 86, 76.46, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 87, 72.15, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 88, 59.85, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 89, 34.92, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 90, 67.85, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 91, 66.46, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 102, 75.77, '2019-08-17', '0000-00-00', 'admin', ''),
(6, 5, 123, 50, '2019-08-31', '2019-09-06', 'admin', 'admin'),
(6, 5, 125, 77.54, '2019-08-31', '2019-09-06', 'admin', 'admin'),
(6, 5, 126, 60.69, '2019-08-31', '2019-09-06', 'admin', 'admin'),
(6, 5, 127, 70.17, '2019-08-31', '2019-09-06', 'admin', 'admin'),
(6, 5, 128, 59.33, '2019-08-31', '2019-09-06', 'admin', 'admin'),
(6, 5, 129, 122.83, '2019-08-31', '2019-09-06', 'admin', 'admin'),
(6, 5, 130, 103, '2019-09-05', '2019-09-06', 'admin', 'admin'),
(6, 5, 131, 119.8, '2019-09-05', '2019-09-06', 'admin', 'admin'),
(6, 5, 132, 120, '2019-09-05', '2019-09-06', 'admin', 'admin'),
(6, 5, 133, 59, '2019-09-05', '2019-09-06', 'admin', 'admin'),
(6, 5, 134, 116.4, '2019-09-05', '2019-09-06', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `taxe_description` varchar(120) NOT NULL,
  `employeur_employe` int(2) DEFAULT NULL COMMENT '0: employe; 1: employeur',
  `taxe_value` double NOT NULL,
  `particulier` int(1) NOT NULL DEFAULT '0' COMMENT '0: for general taxes, 1: for a particular tax ',
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `taxes`
--

INSERT INTO `taxes` (`id`, `taxe_description`, `employeur_employe`, `taxe_value`, `particulier`, `academic_year`) VALUES
(1, 'IRI', 0, 0, 0, 1),
(2, 'TMS', 1, 1, 0, 1),
(3, 'ONA', 0, 6, 0, 1),
(4, 'ONA', 1, 6, 0, 1),
(5, 'CAS', 0, 1, 0, 1),
(6, 'FDU', 0, 1, 0, 1),
(7, 'CT', 0, 1, 0, 1),
(9, 'IRI', 0, 0, 0, 6),
(10, 'TMS', 1, 1, 0, 6),
(11, 'ONA', 0, 6, 0, 6),
(12, 'ONA', 1, 6, 0, 6),
(13, 'CAS', 0, 1, 0, 6),
(14, 'FDU', 0, 1, 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `id` int(11) NOT NULL,
  `title_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`id`, `title_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Secrétaire de scolarité', '2014-10-04 00:00:00', '2017-08-29 00:00:00', NULL, NULL),
(4, 'Préfet des études', '2014-10-04 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(6, 'Econome', '2014-10-04 00:00:00', '2014-10-04 00:00:00', NULL, NULL),
(9, 'Technicien en informatique', '2015-08-20 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(10, 'Personnel de service', '2015-08-20 00:00:00', '2016-11-17 00:00:00', NULL, NULL),
(11, 'Agent de securité', '2016-08-23 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(15, 'Chauffeur', '2016-08-23 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(16, 'Responsable vie scolaire', '2016-08-23 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(17, 'Infirmière', '2016-08-23 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(18, 'Administrateur de réseaux', '2016-08-23 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(19, 'Responsable Informatique', '2016-08-23 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(23, 'Responsable bibliothèque', '2016-08-23 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(24, 'Imprimeur', '2016-08-23 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(25, 'Surveillant Général', '2016-08-23 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(26, 'Secrétaire de l\'économat', '2016-08-23 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(28, 'Secrétaire de direction', '2016-08-23 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(29, 'Secrétaire aux archives', '2016-08-23 00:00:00', '2016-08-23 00:00:00', NULL, NULL),
(31, 'Directeur Général', '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(32, 'Assistant-Directeur', '2016-09-09 00:00:00', '2016-09-09 00:00:00', NULL, NULL),
(33, 'Conseiller principal d\'éducation ', '2017-08-29 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(34, 'Conseiller d\'éducation', '2017-08-29 00:00:00', '2017-08-29 00:00:00', NULL, NULL),
(35, 'Conseiller d\'orientation psychologique', '2017-08-29 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(38, 'Entraineur de sport ', '2018-10-03 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(39, 'Responable section pré-scolaire', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(40, 'Responsable section fondamentale', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(41, 'Responsable section secondaire', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(42, 'Conseiller d\'orientation professionnelle', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL),
(43, 'Responsable laboratoire', '2019-08-10 00:00:00', '2019-08-10 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `person_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `profil` int(11) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `is_parent` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `last_ip` varchar(100) NOT NULL,
  `last_activity` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `active`, `person_id`, `full_name`, `create_by`, `update_by`, `date_created`, `date_updated`, `profil`, `group_id`, `is_parent`, `user_id`, `last_ip`, `last_activity`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 'Admin', 'admin', NULL, '2014-01-10 00:00:00', '2014-01-10 00:00:00', 1, 5, NULL, 0, '200.113.234.211', '2019-09-17 08:21:30'),
(2, 'master_user', 'ab870ed903fcd87be2eae516ecf3d138', 1, 0, 'Super User', 'admin', NULL, '2015-03-07 00:00:00', '2015-03-07 00:00:00', 1, 1, NULL, 0, '190.115.152.226', '2019-09-12 13:15:52'),
(3, 'dessalines2', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 2, 'Jean-Jacques  Dessalines', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(4, 'christophe3', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 3, 'Henri   Christophe', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(5, 'p', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 4, 'Alexandre  Pétion', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(6, 'boyer5', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 5, 'Jean Pierre  Boyer', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 4, 8, 0, 0, '::1', '2019-08-09 08:20:17'),
(7, 'h', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 6, 'Rivière  Hérard', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(8, 'guerrier7', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 7, 'Philippe  Guerrier', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(9, '_pierrot8', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 8, 'Jean-Louis  Pierrot', 'siges_migration_tool', 'admin', '2019-07-02 21:07:22', '2019-07-02 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(10, 'rich', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 9, 'Jean-Baptiste  Riché', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(11, 'soulouque10', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 10, 'Faustin  Soulouque', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(12, 'geffrard11', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 11, 'Fabre Nicolas Geffrard', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(13, 'salnave12', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 12, 'Sylvain  Salnave', 'siges_migration_tool', NULL, '2019-07-02 21:07:22', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(14, 'nissage-saget13', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 13, 'Jean-Nicolas Nissage-Saget', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(15, 'domingue14', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 14, 'Michel  Domingue', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(16, 'boisrond-canal15', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 15, 'Pierre Théomas Boisrond-Canal', 'siges_migration_tool', 'admin', '2019-07-02 21:07:21', '2019-07-02 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(17, 'salomon16', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 16, 'Lysius  Salomon', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(18, '_l', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 17, 'François D  Légitime', 'siges_migration_tool', 'admin', '2019-07-02 21:07:21', '2019-08-07 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(19, 'hyppolite18', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 18, 'Louis Mondestin Florvil  Hyppolite', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(20, 'sam19', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 19, 'Tiresias Simon Sam', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(21, 'alexis20', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 20, 'Nord  Alexis', 'siges_migration_tool', 'admin', '2019-07-02 21:07:21', '2019-08-07 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(22, 'simon21', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 21, 'Francois Antoine  Simon', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(23, 'leconte22', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 22, 'Cincinnatus  Leconte', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(24, 'auguste23', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 23, 'Tancrède  Auguste', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(25, 'oreste24', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 24, 'Michel  Oreste', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(26, 'zamor25', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 25, 'Oreste  Zamor', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(27, 'th', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 26, 'Davilmar  Théodore', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(28, 'sam27', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 27, 'Vilbrun Guillaume  Sam', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(29, 'dartiguenave28', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 28, 'Sudre  Dartiguenave', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(30, 'borno29', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 29, 'Louis  Borno', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(31, 'roy30', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 30, 'Eugène  Roy', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(32, 'vincent31', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 31, 'Sténio  Vincent', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(33, 'lescot32', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 32, 'Elie  Lescot', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(34, '_estim', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 33, 'Dumarsais  Estimé', 'siges_migration_tool', 'admin', '2019-07-02 21:07:21', '2019-07-07 00:00:00', 3, 13, 0, 0, '', '2019-08-06 12:07:26'),
(35, 'magloire34', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 34, 'Paul  Magloire', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(36, 'pierre-louis35', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 35, 'Nemours  Pierre-Louis', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(37, 'sylvain36', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 36, 'Franck  Sylvain', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(38, '_fignol', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 37, 'Daniel  Fignolé', 'siges_migration_tool', 'admin', '2019-07-02 21:07:21', '2019-08-07 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(39, 'duvalier38', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 38, 'François  Duvalier', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(40, 'duvalier39', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 39, 'Jean-Claude  Duvalier', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(41, '_manigat40', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 40, 'Leslie  Manigat', 'siges_migration_tool', 'admin', '2019-07-02 21:07:21', '2019-08-07 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(42, 'namphy41', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 41, 'Henri  Namphy', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(43, 'avril42', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 42, 'Prosper  Avril', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(44, 'trouillot43', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 43, 'Ertha P Trouillot', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(45, 'aristide44', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 44, 'Jean Bertrand  Aristide', 'siges_migration_tool', NULL, '2019-07-02 21:07:21', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(46, 'moise45', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 45, 'Jovenel Moise', 'admin', NULL, '2019-07-02 00:00:00', '2019-07-02 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(47, 'n', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 46, 'Joseph Nérette', 'siges_migration_tool', NULL, '2019-07-02 22:07:43', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(48, 'jonassaint47', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 47, 'Emile Jonassaint', 'siges_migration_tool', NULL, '2019-07-02 22:07:44', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(49, 'pr', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 48, 'René Préval', 'siges_migration_tool', NULL, '2019-07-02 22:07:44', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(50, 'alexandre49', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 49, 'Boniface Alexandre', 'siges_migration_tool', 'admin', '2019-07-02 22:07:44', '2019-08-07 00:00:00', 2, 10, 0, 0, '', '2019-08-09 17:40:57'),
(51, 'martelly50', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 50, 'Michel Joseph  Martelly', 'siges_migration_tool', NULL, '2019-07-02 22:07:44', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(52, 'privert51', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 51, 'Jocelerme  Privert', 'siges_migration_tool', NULL, '2019-07-02 22:07:44', NULL, 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(53, 'moravia52', '45184ea57bc2e2796635045733d7c42b', 1, 52, 'Charles Moravia', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(55, 'faubert54', '2614bb5d6481efb7cb1193bd431fb22e', 1, 54, 'Ida Faubert', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(56, 'bayard55', '265144ab62d5217f1541250efe1f4861', 1, 55, 'Othello Bayard', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(57, 'grimard56', 'aad48117632799633c5ac71d5d90b110', 1, 56, 'Luc Grimard', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(58, 'laleau57', 'b0a9d8040af4af538f4d695a6fc4950b', 1, 57, 'Léon Laleau', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(59, 'jastram58', 'dd06696b83411ca95e0f374d11267fd6', 1, 58, 'Gervais Jastram', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(60, 'carri', '5ec44b7ab000b863d47d5c9ff78596fe', 1, 59, 'Emmeline Carriès-Lemaire', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(61, 'brouard60', 'dde89b21eb2de72ec7f450b0f2fbdaa1', 1, 60, 'Carl Brouard', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(62, 'roumer61', '0a44beba39736a007f4714008ce19b56', 1, 61, 'Emile Roumer', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(63, 'marcellin62', '82d73f18f19f6720afe3198fccbbf3f7', 1, 62, 'Philippe-Thoby Marcellin', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(64, 'heurtelou63', '3ba79ed1854fa1d159840e85ef1ea096', 1, 63, 'Daniel Heurtelou', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(65, 'roumain64', '2ff2b236938f7ec2e6c824abfeeca5bc', 1, 64, 'Jacques Roumain', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(66, 'lesp', '1a9598e6e7504affbd1ca873af3dab18', 1, 65, 'Anthony Lespès', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(67, 'brierre66', '3bfb30693b767aec44515c5adcf119d3', 1, 66, 'Jean Fernand Brierre', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(68, 'dauphin67', 'f05c7c66f7cf2d9106aad0e0f274049b', 1, 67, 'Marcel Dauphin', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(69, 'martineau68', '62575b9d84b99233e82baf0f86968675', 1, 68, 'Fernand Martineau', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(70, 'dorsinville69', 'bce2178658ecff99b6630f34c552a6e0', 1, 69, 'Roger Dorsinville', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(71, 'saint-aude70', 'ef566ee4c223d83a8f2ef94cc66efc5c', 1, 70, 'Clément Magloire Saint-Aude', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(72, 'morisseau-leroy71', '5a8f4deb58232f768002c42106b19adc', 1, 71, 'Félix Morisseau-Leroy', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(73, 'chassagne72', '1f5024a985b7962a682ecd1089943198', 1, 72, 'Roland Chassagne', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(74, 'camille73', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 73, 'Roussan Camille', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '::1', '2019-08-03 15:23:34'),
(75, 'moise74', 'f0c205c683c5cd52ab1226f6f51c5a6e', 1, 74, 'Rodolphe Moise', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(76, 'charles_bernard75', '32415f4f48fa73f4c66d9fa67a32d93f', 1, 75, 'Régnor Charles Bernard', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(77, 'fouch', '8446aa3539dc79699f8f65abd95d0dc5', 1, 76, 'Franck Fouché', 'siges_migration_tool', NULL, '2019-07-03 01:07:40', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(78, 'belance77', 'f78c60f0893f4a5ae00c8e8c16679f6e', 1, 77, 'René Belance', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(79, 'garoute78', '5ea4fb3eadc048682371618ddee6aee0', 1, 78, 'Hamilton Garoute', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(80, 'laraque79', 'ef79cc6f7a312d9d79f5f921d9f0bc57', 1, 79, 'Paul Laraque', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(81, 'saint-louis80', '6ebec680ea459d3e6bdc07a320bf23cd', 1, 80, 'Carlos Saint-Louis', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(82, 'lemoine81', '2e6e9d08a4953453799f4936a109b8ea', 1, 81, 'Lucien Lemoine', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(83, 'chassagne82', '39ec55379615fcfadacb18ca4880f232', 1, 82, 'Raymond Chassagne', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(84, 'philoct', 'df7b631ef33f2b9673a221f0c55c73c3', 1, 83, 'Raymond Philoctète', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(85, 'pierre-louis84', '7c1b6640b00aedc62611d50ed1fabb03', 1, 84, 'Ulysse Pierre-Louis', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(86, 'depestre85', '2fc33cd21de2e7f52ae05ecc78f5172a', 1, 85, 'René Depestre', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(87, 'phelps86', 'dad2901da9226a190c903750f3adf6b7', 1, 86, 'Anthony  Phelps', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(88, 'dorcely87', '9152c55ed959d19084112e3eba6a9aab', 1, 87, 'Roland Dorcely', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(89, 'bajeux88', 'd21ff6e111d52bed2b9c73f4fc8199cc', 1, 88, 'Jean-Claude Bajeux', 'siges_migration_tool', NULL, '2019-07-03 01:07:35', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(91, 'beaug', '641279a411e69c300eee550b3c5f503d', 1, 90, 'Jacqueline Beaugé-Rosier', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(92, 'castera91', '8e7df22b0433ebd7b66c44d2056e27b6', 1, 91, 'Georges Castera', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(93, 'davertige92', '7c6ba3cfbeef519f1b962219f1114a25', 1, 92, 'Davertige Davertige', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(94, 'etienne93', 'd80539972685a143648fb4104aaf0bd3', 1, 93, 'Gérard Etienne', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(95, 'frank94', 'd5bf38a48f85bf55cebbf95b502bb69e', 1, 94, 'Etienne Frank', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(96, 'legagneur95', 'c2d1b248761e98a5315b3b4d327af2b4', 1, 95, 'Serge Legagneur', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(97, 'lerebours96', '799e102957047816b3b481051d8be3cf', 1, 96, 'Michel-Philippe Lerebours', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(98, 'magloire97', 'c06794132cd5590168f8aa0a6f48c272', 1, 97, 'Francis-Séjour Magloire', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(99, 'martineau98', 'fbf77171f06db1164ce6d1dcc1c4ade9', 1, 98, 'Jean-Claude Martineau', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(100, 'm', '1824ef1177f894b3f78ce63f41cb9b6a', 1, 99, 'Jean Métellus', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(101, 'morisseau100', '6f3079a4574ede664488bd3ba31f274e', 1, 100, 'Roland Morisseau', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(102, 'paul101', '8db033dbc8b9aeceb9aacbab6322be84', 1, 101, 'Cauvin L. Paul', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(103, 'tavernier102', '97bbb0c3698639a64df3c89151376921', 1, 102, 'Jamine Tavernier', 'siges_migration_tool', NULL, '2019-07-03 01:07:55', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(105, 'laforest104', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 104, 'Jean-Richard Laforest', 'siges_migration_tool', 'admin', '2019-07-03 01:07:32', '2019-07-24 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(106, 'pierre105', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 105, 'Claude C. Pierre', 'siges_migration_tool', 'admin', '2019-07-03 01:07:32', '2019-07-24 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(107, 'antoine106', 'aa201adf8c287e96cb26e5771d4638b5', 1, 106, 'Yves Antoine', 'siges_migration_tool', NULL, '2019-07-03 01:07:32', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(108, 'campfort107', '4fb0b8e9210fb7092f30831e48cf06b1', 1, 107, 'Gérard Campfort', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(110, 'lescouflair109', '8cb59ca4fd963b083efc44ca1730c37d', 1, 109, 'Rony Lescouflair', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(111, 'badiguy-gilbert110', '85cd0edaec480bf6055f0668376c5681', 1, 110, 'Serge Badiguy-Gilbert', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(112, 'cav', '8fd4a7b966d1927e984a7f5a43c01350', 1, 111, 'Syto Cavé', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(113, 'charlier112', '44372117cbda847f23000bb3db0e934c', 1, 112, 'Jacques Charlier', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(114, 'calvin113', '24ab17bd0a94d997a128a87191d73ba0', 1, 113, 'Jean-Max Calvin', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(115, 'assali114', 'beb9b631f70be6ea68fbc946786167c7', 1, 114, 'Donald Assali', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(116, 'jean115', '93164f9e3ff263bf9294389742abf5d9', 1, 115, 'Yanick Jean', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(117, 'ej', 'a1ac18815d327756b17b02764885f79b', 1, 116, 'Manno Ejèn', 'siges_migration_tool', NULL, '2019-07-03 01:07:33', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(119, 'berrouet-oriol118', 'a5d455f1bbe9bcb6fc7743530b9834a5', 1, 118, 'Robert Berrouet-Oriol', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(120, 'brisson119', '2fde62b9479af2f4834f57b67c3131d7', 1, 119, 'Richard Brisson', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(121, 'charles120', '2d06188765a0363fca8770ab4a3dd3fc', 1, 120, 'Christophe Charles', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(122, 'charles121', '7759eea99579505daef956588cfb84fe', 1, 121, 'Jean-Claude Charles ', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(123, 'gousse122', 'f2c4fb407726f012a8ddb4187f29b3ea', 1, 122, 'Edgard Gousse', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(124, 'laforest123', '93ac3213bc8cb09cd0e5513e52be1db9', 1, 123, 'Jean-Richard Laforest', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(125, 'large124', 'ca9fc2ba5eb7deb57f957fd1af1647a4', 1, 124, 'Josaphat-Robert Large', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(126, 'lescouflair125', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 125, 'Rony Lescouflair', 'siges_migration_tool', 'admin', '2019-07-03 01:07:58', '2019-07-24 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(127, 'madh', '7acd7e211098da3f1b819b08103c0045', 1, 126, 'Serge Madhère', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(128, 'muller127', 'd0744ef59518f6895a40aa211fc9a997', 1, 127, 'Rudolph Muller', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(129, 'pierre128', '569409bc5364baca644e52ebc5958dcc', 1, 128, 'Claude C. Pierre', 'siges_migration_tool', NULL, '2019-07-03 01:07:58', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(130, 'hyppolite129', '1b9fffab94679e6319c832ba2f57fb88', 1, 129, 'Michel-Ange Hyppolite', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(131, 'manuel130', 'dedd13c8c0570583dab6aff3dd3d1b05', 1, 130, 'Robert Manuel', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(132, 'trouillot131', '21f846f7f4ac2571ed29b9270f25e90f', 1, 131, 'Evelyne Trouillot', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(133, 'agnant132', '39905ac5744af4a035703d48dde921de', 1, 132, 'Marie-Célie Agnant', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(134, 'noel133', '964f1e83b890cd1e7d75ee9804c75331', 1, 133, 'Lochard Noel', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(135, 'soukar134', '9c602812978ac724e8cb0960c338e1fe', 1, 134, 'Michel Soukar', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(136, 'saint-natus135', '88bf0498853f8ea6ed8e7e36a40e6818', 1, 135, 'Clotaire Saint-Natus', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(137, 'trouillot136', 'bbdbaa6b30e4ad279dc1e95755c200c9', 1, 136, 'Lyonel Trouillot', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(138, 'guichard137', '8b7a1a29fd0be7903e575db531cf1f58', 1, 137, 'Marie-Claude Guichard', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(139, 'mars138', '803fc770eba38c1b42dc5c8cd654fc29', 1, 138, 'Kettly P. Mars', 'siges_migration_tool', NULL, '2019-07-03 01:07:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(140, 'satyre139', 'd84b0631a5b195d4efa747b442a9b3fa', 1, 139, 'Joubert  Satyre', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(141, 'augustin140', 'dbe79dce16a0b54f01642b3e674b577b', 1, 140, 'Gary  Augustin', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(142, 'narcisse141', 'fe3c21db4aa74bd4b9b75de738ed7646', 1, 141, 'Pierre-Richard Narcisse', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(143, 'roche142', 'd2bcf139e12a0183d0712ea33074268c', 1, 142, 'Jacques Roche', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(144, 'exavier143', 'b0969a332c5ee8a31a2ea9960e12fa80', 1, 143, 'Marc Exavier', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(145, 'batraville144', 'e24e6c6cd64e916b3e05c36751972819', 1, 144, 'Dominique Batraville', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(146, 'dalembert145', '41ae324e512338acc854b3a4daac8699', 1, 145, 'Louis-Philippe Dalembert', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(147, 'saint-eloi146', 'e309907d6420259a8bcb4a51bf6b38c3', 1, 146, 'Rodney Saint-Eloi', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(148, 'henriquez147', 'b1b64206e924479f4f2c7ed2409fc509', 1, 147, 'Pradel Henriquez', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(149, 'dug', '080cf3f70c71eab383a9fc9a376af703', 1, 148, 'Jean Amorce Dugé', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(150, 'edouard149', '70897b26576b12606643c548615f5222', 1, 149, 'Willems Edouard', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(151, 'milce150', '724c3b224f9419984ef5874bb878de1b', 1, 150, 'Jean Ephèle Milce', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(152, 'lh', '56dd34a5e3ac9d45458654a35e6c3727', 1, 151, 'Farah-Martine Lhérisson', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(153, 'proph', '09290d9d71bf9f938c50cb9e40cdd0d1', 1, 152, 'Emmelie Prophète', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(154, 'fouad153', '75110fadb15f548477c704e8c716f359', 1, 153, 'André Fouad', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(155, 'auguste154', '62d93d78961c297b2888786a78cb33a2', 1, 154, 'Bonel Auguste', 'siges_migration_tool', NULL, '2019-07-03 01:07:01', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(156, 'blanc155', 'bf9885a28f6756579368401e36136baf', 1, 155, 'Jean-Claude Blanc', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(157, 'blanco156', '652654877074105579f172e01a76772a', 1, 156, 'Dionisio Blanco', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(158, 'bonaventure157', 'ff13ef0cad4b1d0683450e01e84b4617', 1, 157, 'Jean Louis Bonaventure', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(159, 'carrier158', '80da93c923a34f794574112fe11cd45a', 1, 158, 'Clausel Carrier', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(160, 'charles159', '35c8f903b663e2b7de2da72dc033d32d', 1, 159, 'Ersnt-Jean Charles', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(161, 'constant160', '8fa6336c22cdc87d95b10cff2022c17c', 1, 160, 'Presler Constant', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(162, 'coutard161', 'c4916cf6ba5cfe2c9aa5c3d3829edc85', 1, 161, 'Gabriel Coutard', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(163, 'dambreville162', '8055d27db8905e6bd8b60cc73c4e0c86', 1, 162, 'Claude Dambreville', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(164, 'day163', 'b81c842aba1603d2144805be31e70ecb', 1, 163, 'Jackson Day', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(165, 'delinois164', '1f6896f63c9ab7b68276247d9f8d9db2', 1, 164, 'Amerlin Delinois', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(166, 'desarmes165', '0823fb80b97f1f4b9f4427b6d109e3be', 1, 165, 'Georges Desarmes', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(167, 'dominique166', '83044ddd9db08f65b3c250d3729245d9', 1, 166, 'Clotaire Dominique', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(168, 'dubic167', '364ad26f137c953dcbdd5fd926f4a1ad', 1, 167, 'Nelson Dubic', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(169, 'dreux168', '74ca79f65eec4df10332c3aea50cbef8', 1, 168, 'Nicolas Dreux', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(170, 'duffaut169', '7cf6fd651ae741c8cc88720b89b5ffc7', 1, 169, 'Préfète Duffaut', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(171, 'prosper170', '36b7dad0bdec8919f19b8252982a8b38', 1, 170, 'Eriveaux Prosper', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(172, 'exil171', '6817cd1114ba3ea28973017744cf5fed', 1, 171, 'Levoy Exil', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(173, 'fortun', 'ecfce7d06ef378139703cff23ca2abe8', 1, 172, 'Gérard Fortuné', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(174, 'henry173', '37e0a40aa934e23b9f17d1fe34ac66fd', 1, 173, 'Calixte Henry', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(175, 'hilome174', 'b3ce621b7c2d2fe950399aa1f2ffc966', 1, 174, 'José Hilome', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(176, 'jean-baptiste175', '29858e3c386f0f3e6e0448509e0b9bac', 1, 175, 'Waking Jean-Baptiste', 'siges_migration_tool', NULL, '2019-07-05 20:07:49', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(177, 'jean-louis176', '1389f8d1b3b9fddeb81a92cd8b26f756', 1, 176, 'Eric Jean-Louis', 'siges_migration_tool', NULL, '2019-07-05 20:07:50', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(178, 'jean-louis177', 'b9ea11a19756ca317fdd4057f0266387', 1, 177, 'Henri Jean-Louis', 'siges_migration_tool', NULL, '2019-07-05 20:07:50', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(179, 'jean-louis178', '758ce1ca67043a9285dd47f93619a186', 1, 178, 'Maxan Jean-Louis', 'siges_migration_tool', NULL, '2019-07-05 20:07:50', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(180, 'joseph179', '76650f1ae576ba8b2516dd3caf81e036', 1, 179, 'Emmanuel Joseph', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(181, 'joseph180', '90b53a3337eeed2a1764980c344ed499', 1, 180, 'Lesly Joseph', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(182, 'joseph181', '510d8f8acdb58d6c0b573e00bb8bd2de', 1, 181, 'Raymond Joseph', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(183, 'joseph182', '638b621705736e4f01588506c9dcfc23', 1, 182, 'Reynald Joseph', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(184, 'labbe183', 'fbc478c21ed8b852044fad84f8d31130', 1, 183, 'Serge Labbe', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(185, 'lobbe184', 'abc86ed8928ed12753610601b7ef7ace', 1, 184, 'Arnoux Lobbe', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(186, 'louizor185', 'ba6746c1a946c8bcb3a4ede9453d4f6a', 1, 185, 'Ernst Louizor', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(187, 'lumarc186', '619ac4342699526fc572dfba0fdfee40', 1, 186, 'Evans Lumarc', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(188, 'magloire187', '11549093fd1b2f1852d8c12f6e165163', 1, 187, 'Magda Magloire', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(189, 'merise188', '1e159f4f865f9c6bb614d5500410d01b', 1, 188, 'Fritz Merise', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(190, 'onel189', 'e074bb2395c8fa37986281d8f09c0c0a', 1, 189, 'ONEL Onel', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(191, 'paul190', 'c9eb83c60dc56824c058bea56e217048', 1, 190, 'Dieuseul Paul', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(192, 'augustin191', 'e5797b62fc7df4eba1594a5c07202bed', 1, 191, 'Pierre-Sylvain Augustin', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(193, 'petizil192', '2703d1c43409c9db73c80398fe92b049', 1, 192, 'Durand Petizil', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(194, 'richard193', '78deac60d5d4c02f2f45ed86ec69b5f3', 1, 193, 'Nesly Richard', 'siges_migration_tool', NULL, '2019-07-05 20:07:26', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(196, 'rousseau195', '3e23ddcebaaf5b8473778a231fbb7052', 1, 195, 'Denis Rousseau', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(197, 'saint-fleurant196', '22edf1a19b0b6da7cbcc7b171a8fd653', 1, 196, 'Louisiane Saint-Fleurant', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(198, 'saint-jean197', 'd80b1909a80ec772e4361c6fd463c632', 1, 197, 'Harold Saint-Jean', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(199, 'seide198', 'f885a1dde2ca61694f71da248bed7bd6', 1, 198, 'Jean Adrien Seide', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(200, 'semerand199', 'b327b5a0944e37ddeee1f6a485610d08', 1, 199, 'Galland Semerand', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(201, 'siméon200', '973b2a41b721a1de2fc50be31eb95cdb', 1, 200, 'Michel Siméon', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(202, 'garoute201', '5ceda76d45a72c63e7e66ef578be159f', 1, 201, 'Jean-Claude Garoute', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(203, 'walton202', '14b750e9a568a3113c8640a343d313d9', 1, 202, 'Jean Walton', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(204, 'zachari203', '13a59df3b13a307d33128ecb6b0e1702', 1, 203, 'Pierre Zachari', 'siges_migration_tool', NULL, '2019-07-05 20:07:30', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(205, 'marc204', 'a72afb5d11aadc90cbdeb807cff55761', 1, 204, 'Yucca Marc', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(206, 'hilaire205', '036926c6664af1b31db8238265e256e6', 1, 205, 'Kira Hilaire', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(207, 'prosper206', 'd30d0d73beaddb6175610cc62f7c54b4', 1, 206, 'Kitty Prosper', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(208, 'poulard207', '110c7ba1cabeda9054ba9e42c90cbd21', 1, 207, 'Isabelle Poulard', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(209, 'possible208', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 208, 'Alain Possible', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '2019-08-09 16:25:54'),
(210, 'potter209', 'df20bd5a1b72d354ccb145042bd0342f', 1, 209, 'Harry Potter', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(212, 'weasley211', '81416d7ba71c01140e5a1ddcd1ff7df5', 1, 211, 'Ronald Weasley', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(213, 'malefoy212', '070fe6359d4f83a5fe0123cb7109c544', 1, 212, 'Drago Malefoy', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(214, 'marc213', 'f124758e77fe66eadde18ee8a46b1f00', 1, 213, 'Jean-Baptiste Marc', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(215, 'poulard214', '36d23f5d50a0cf3992841bba8983f3d1', 1, 214, 'Jean Came Poulard', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(216, 'prosper215', 'f01b53371b3a9aa4b84897831641cbe8', 1, 215, 'Metuschael Prosper', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(217, 'hilaire216', 'f0599e25f1ab4dd2d08ced9b04203856', 1, 216, 'Eder Hilaire', 'siges_migration_tool', NULL, '2019-07-23 22:07:43', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(218, 'granger210', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 210, 'Hermione Granger', 'admin', 'LOGIPAM', '2019-07-24 00:00:00', '2019-08-03 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(219, 'jean_gilels210', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 210, 'Jean Gilels', 'LOGIPAM', NULL, '2019-07-30 00:00:00', '2019-07-30 00:00:00', 5, 4, 1, 0, '', '0000-00-00 00:00:00'),
(221, 'poulard218', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 218, 'Jean Came Poulard', 'admin', 'admin', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 4, 8, 0, 0, '', '2019-08-05 14:49:55'),
(222, 'charles219', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 219, 'ROSS TESSY SAMMANTHA Charles', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(223, 'bataille220', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 220, 'Harold Bataille', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(224, 'atis221', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 221, 'Murielle Atis', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(225, 'poulard222', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 222, 'Gabrielle Poulard', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(226, 'jean223', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 223, 'Ronald Jean', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(227, 'charles224', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 224, 'Fania Charles', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(228, 'borgelin225', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 225, 'Clifford Borgelin', 'admin', 'admin', '2019-08-05 00:00:00', '2019-08-25 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(229, 'joseph226', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 226, 'JEAN JOSEPH', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(230, 'viral227', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 227, 'Johnson Viral', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(231, 'poulard228', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 228, 'Jeanne Poulard', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(232, 'charles229', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 229, 'Tara Charles', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '2019-08-05 14:46:17'),
(233, 'desir230', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 230, 'Anglade Désir', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '2019-08-06 14:33:03'),
(234, 'covington231', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 231, 'Jovenul Covington', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(235, 'charles232', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 232, 'Geraldine Charles', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(236, 'desir233', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 233, 'Cassandra Desir', 'admin', 'admin', '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(237, 'joseph234', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 234, 'Carmelite JOSEPH', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(238, 'noel235', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 235, 'David Noel', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(239, 'francois236', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 236, 'Beatrice Francois', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(241, 'michele_jeanty228', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 228, 'MIchele Jeanty', 'admin', NULL, '2019-08-05 00:00:00', '2019-08-05 00:00:00', 5, 4, 1, 0, '', '2019-08-05 14:56:04'),
(242, 'laguerre237', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 237, 'Jacob Laguerre', 'admin', NULL, '2019-08-06 00:00:00', '2019-08-06 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(243, 'jacques238', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 238, 'Vanessa Jacques', 'admin', NULL, '2019-08-06 00:00:00', '2019-08-06 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(244, 'jacques239', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 239, 'Mergens JACQUES', 'admin', NULL, '2019-08-06 00:00:00', '2019-08-06 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(245, 'sabine240', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 240, 'Frédéric Sabine', 'SIGES', NULL, '2019-08-10 00:00:00', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(246, 'poulard241', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 241, 'Gabriel Poulard', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(247, 'sainjour242', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 242, 'Wilna Sainjour', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(248, 'louis243', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 243, 'Annedjee Mitchellina LOUIS', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(249, 'jean244', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 244, 'Vanessa Jean', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(250, 'louis245', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 245, 'Hadassa Klaoudjinn LOUIS', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(251, 'guillaume246', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 246, 'Carl Guillaume', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(253, 'louis248', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 248, 'Jameson Louis', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(254, 'st_juste249', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 249, 'Santia St Juste', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(255, 'pierre250', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 250, 'Emile Pierre', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(256, 'conde251', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 251, 'Margalita CONDE', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(257, 'etienne252', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 252, 'Pipo Etienne', 'admin', 'admin', '2019-08-31 00:00:00', '2019-09-13 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(258, 'emile253', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 253, 'Francois Emile', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(259, 'junette254', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 254, 'LEONARD Junette', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 4, 8, 0, 0, '', '0000-00-00 00:00:00'),
(260, 'neard255', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 255, 'Angelo Néard', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 4, 8, 0, 0, '', '2019-09-06 14:25:03'),
(261, 'roger_meyer256', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 256, 'TIBULLE Roger Meyer', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 4, 8, 0, 0, '', '0000-00-00 00:00:00'),
(262, 'charles257', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 257, 'Sandra Charles', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 4, 8, 0, 0, '', '0000-00-00 00:00:00'),
(263, 'joseph258', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 258, 'Garry Joseph', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 4, 8, 0, 0, '', '0000-00-00 00:00:00'),
(264, 'frack259', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 259, 'Gaston Frack', 'admin', 'admin', '2019-08-31 00:00:00', '2019-08-31 00:00:00', 4, 8, 0, 0, '', '0000-00-00 00:00:00'),
(266, 'conde247', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 247, 'Jeffery CONDE', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(267, 'louis260', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 260, 'Vilner LOUIS', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(268, 'blot53', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 53, 'Probus Blot', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(269, 'large108', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 108, 'Josaphat-Robert Large', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(270, 'albert_buron241', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 241, 'Albert Buron', 'admin', NULL, '2019-08-31 00:00:00', '2019-08-31 00:00:00', 5, 4, 1, 0, '', '2019-08-31 18:51:59'),
(271, 'adonis261', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 261, 'Sasmide ADONIS', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(272, 'adonis262', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 262, 'Pascal ADONIS', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(273, 'gabriel263', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 263, 'Jacques Gabriel', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(274, 'judith_larose262', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 262, 'Judith Larose', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 4, 1, 0, '', '0000-00-00 00:00:00'),
(275, 'pierre_paul140', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 140, 'Pierre Paul', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 4, 1, 0, '', '0000-00-00 00:00:00'),
(276, 'jude_louis243', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 243, 'Jude LOUIS', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 4, 1, 0, '', '0000-00-00 00:00:00'),
(277, 'victor264', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 264, 'Sabruna Victor', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(278, 'carl_henry246', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 246, 'Carl Henry GUILLAUME', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 4, 1, 0, '', '0000-00-00 00:00:00'),
(279, 'darline_etienne252', '5f4dcc3b5aa765d61d8327deb882cf99', 0, 252, 'Darline ETIENNE', 'admin', 'admin', '2019-09-01 00:00:00', '2019-09-13 00:00:00', 5, 4, 1, 0, '', '0000-00-00 00:00:00'),
(280, 'reynald265', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 265, 'Sabruna Reynald', 'admin', NULL, '2019-09-01 00:00:00', '2019-09-01 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(281, 'georges266', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 266, 'Maryse GEORGES', 'admin', 'admin', '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 3, 0, 0, '', '2019-09-06 15:03:47'),
(282, 'jacques267', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 267, 'Coralie JACQUES', 'admin', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(283, 'sylvain268', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 268, 'Judith SYLVAIN', 'admin', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(284, 'jean_charles269', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 269, 'Lorie JEAN CHARLES', 'admin', 'admin', '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(285, 'mathutin270', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 270, 'Johanne MATHUTIN', 'admin', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00');
INSERT INTO `users` (`id`, `username`, `password`, `active`, `person_id`, `full_name`, `create_by`, `update_by`, `date_created`, `date_updated`, `profil`, `group_id`, `is_parent`, `user_id`, `last_ip`, `last_activity`) VALUES
(286, 'pierre271', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 271, 'Titus PIERRE', 'admin', 'admin', '2019-09-05 00:00:00', '2019-09-05 00:00:00', 4, 8, 0, 0, '', '0000-00-00 00:00:00'),
(287, 'phycien272', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 272, 'Natacha PHYCIEN', 'admin', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(288, 'vilbrun273', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 273, 'Sarah VILBRUN', 'admin', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(289, 'charles274', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 274, 'Guilene CHARLES', 'admin', NULL, '2019-09-05 00:00:00', '2019-09-05 00:00:00', 5, 2, 0, 0, '', '0000-00-00 00:00:00'),
(291, 'antoine276', '462379e52fa98ad77e2fb91d1e1c0d22', 1, 276, 'Jean Marc Lewis ANTOINE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(292, 'aristide277', '348fed226324e9014e55a8430254c268', 1, 277, 'Youri Stravinski I. ARISTIDE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(293, 'astremon278', 'c946ae67001ef7653674b9a04981d9f1', 1, 278, 'Johnsley ASTREMON ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(294, 'augustin279', 'b645e33e65636f0944928cb4aa5e760f', 1, 279, 'Hutchins Guervensky AUGUSTIN  ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(295, 'bonamy280', '87071686860b9bff88be183d495d5fca', 1, 280, 'Christie Marie Chelsie  BONAMY ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(296, 'boursiquot281', '3589802d467cfff1b189939af151dc4e', 1, 281, 'Julmène BOURSIQUOT ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(297, 'cange282', 'ffd8a9dbfe5a64e593cba45415b702ac', 1, 282, 'Rood-Christopher CANGE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(298, 'charles283', '941a8ace1524b69cdc60096d82118831', 1, 283, 'Marie Christie CHARLES ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(299, 'charles284', '26c39e4288df6f60790ea4e854cfb8a2', 1, 284, 'Samuel Peterson CHARLES ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(300, 'chenet285', '443c6faa7d9e53353bf1e531334f0ad9', 1, 285, 'Marvin Dudley  CHENET ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(301, 'cheremond286', 'ec6ab6180781f54705c11aab76caa8bb', 1, 286, 'Laurenson CHEREMOND ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(302, 'dacier287', '51a25125f6282c501a92d5d212b6a431', 1, 287, 'Yvenson DACIER ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(303, 'desy288', '3a8b14ca4b3c9822318a98ed449e0355', 1, 288, 'Tchailovsky Westember  DESY ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(304, 'dieudonne289', 'b443e32f947ce98ae664b0f1ab04c633', 1, 289, 'Jiverson DIEUDONNE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(305, 'dort_louis290', '532c8cb61c76c636488b852f2d8a874e', 1, 290, 'Dodley DORT Louis ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(306, 'dorvil291', 'ba519ba81477beba85220c488825d5ad', 1, 291, ' Arnhem Lorentz DORVIL', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(307, 'dorvil292', 'ad54b95f9778e646b263bf381d2774d8', 1, 292, ' Lisna  DORVIL', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(308, 'duperval293', 'd6522a0646c2c411a7f289fcc5b9735f', 1, 293, 'Kervens DUPERVAL ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(309, 'duvert294', '6464fda05f7d5c2f7c87c2646476e072', 1, 294, 'Lourdes W. DUVERT ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(310, 'esperance295', '23253b6285665d4455a6fa90ea3484ee', 1, 295, 'Jonathan ESPERANCE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(311, 'filius296', 'a406b46fedb2f193585966847ebaf343', 1, 296, ' Bwadley FILIUS', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(312, 'fleche297', 'b8dba5fedcd9a3fc26d714334c99fd96', 1, 297, 'Jose  FLECHE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(313, 'fragilas298', '6713c7549bdb3fc5c7b94d7d66749030', 1, 298, 'Jules-Nader FRAGILAS ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(314, 'gere299', '7f55e38cbbb529cf51c02cb897245bc1', 1, 299, 'Zeroberto GERE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(315, 'gourdet300', '190addae8825c1978841d4a354fad3bc', 1, 300, 'Fredlyn GOURDET ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(316, 'jean301', 'cfa8499ae0d4f8323b4e5028886a683e', 1, 301, 'Alan Joey  JEAN ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(317, 'jean-fils302', '4f732bc568ce41c809e2c47cc4d52b1b', 1, 302, 'Mitsouka JEAN-Fils ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(318, 'jean-pierre303', 'de025e2593af824b669c1d6094b4ecd9', 1, 303, 'Chrisley JEAN-PIERRE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(319, 'jocelyn304', 'b5e05c3bcc0aa188574dda5bb389e8b1', 1, 304, 'Steeve  JOCELYN ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(320, 'joseph305', 'c0ad343094a3ca545e0d529c7baf8232', 1, 305, 'Anickson JOSEPH  ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(321, 'joseph306', '9274c32a274f323a024432af7bbc0eca', 1, 306, ' Bleck Dawensky  JOSEPH', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(322, 'joseph307', '4b8ffab68fe0519ed69fc10902e0c3d2', 1, 307, 'Evaninson JOSEPH ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(323, 'joseph308', '40b417f2cac9cdf2f311e876be012036', 1, 308, 'Kenley  JOSEPH ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(324, 'joseph309', '3e4c61dffcce903a2faa80f433eb43e2', 1, 309, 'Winsley JOSEPH ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(325, 'laine310', 'ea6a3a6bc808bc1fec97ee44bcfa37de', 1, 310, ' Cleeford Ralf Yves Gar  LAINE', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(326, 'laveus311', '9fffb95acee0493cb93c21330e9b11be', 1, 311, 'Sebastien LAVEUS ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(327, 'leome312', '634e0cf00cb861730580b0312168555d', 1, 312, 'Bodina Holding  LEOME ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(328, 'louis313', '71cbb2eda9c3b81b08c0fba6dc505d9b', 1, 313, 'Adler Volsky LOUIS ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(329, 'monteau314', '18f0e134db937d6e33ffb392411bf653', 1, 314, 'Dave MONTEAU ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(330, 'morand315', '29234a50651ddb1fc14e4d1dc1ab81f0', 1, 315, 'Antherton Emmanuel Fils  MORAND ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(331, 'myrthyl316', '890273e33f97f769f0b53e4829483096', 1, 316, 'Miss Esperancia MYRTHYL ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(332, 'nausse317', '37b95cdcf2f9d27ba2232b0e0b298845', 1, 317, ' Frantzy NAUSSE', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(333, 'nobert318', '22174535d716a4228d2361373cd85135', 1, 318, ' Jn Gardy NOBERT', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(334, 'paul319', '66ab6102be1faa005dfcdb2197dc6843', 1, 319, ' Johnny PAUL ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(335, 'paul320', 'c58fab63ec867fb46061ebaf8ec58694', 1, 320, 'Amelus PAUL ', 'siges_migration_tool', NULL, '2019-09-06 13:09:37', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(336, 'petiote321', '08aff16ae9fc6a34ba6f3b61013709ee', 1, 321, 'Neyssa  PETIOTE ', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(337, 'raymond322', 'f8a48088310402245c992532cdfff5d2', 1, 322, 'Rikelmey RAYMOND ', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(338, 'remevil323', '39503898eeafb56ef73bb6f1977e1b3a', 1, 323, 'Marc Elton REMEVIL ', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(339, 'richard324', '4e5e633e767e13b226ae0c08c82efe38', 1, 324, 'Sterline RICHARD ', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(340, 'saimpas325', '9dd0df66420b9b5db218defa5b28d59e', 1, 325, 'Herby SAIMPAS ', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(341, 'st_clair326', '881e41a9072fe91265d97b5f2e93e2bd', 1, 326, ' Gallardo  ST CLAIR', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(342, 'telly327', 'c8eb8ff770adf7973a5ff69282149494', 1, 327, 'Emmanuel TELLY ', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(343, 'thelusma328', 'bad9d2b64ad14f6904190d81481aa686', 1, 328, 'Leonardo THELUSMA ', 'siges_migration_tool', NULL, '2019-09-06 13:09:38', NULL, 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(344, 'jean_pierre266', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 266, 'Jean Pierre GEORGES', 'admin', NULL, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 5, 4, 1, 0, '', '2019-09-06 14:06:43'),
(345, 'andre275', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 275, 'Duddley-Fils ANDRE ', 'admin', NULL, '2019-09-06 00:00:00', '2019-09-06 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(346, 'pierre329', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 329, 'André PIERRE', 'admin', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(347, 'laguerre330', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 330, 'Mackendy LAGUERRE', 'admin', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(348, 'moreau331', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 331, 'Raphaella MOREAU', 'admin', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00'),
(349, 'belfort332', '5f4dcc3b5aa765d61d8327deb882cf99', 1, 332, 'Michaella BELFORT', 'admin', NULL, '2019-09-14 00:00:00', '2019-09-14 00:00:00', 5, 3, 0, 0, '', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users_preferences`
--

CREATE TABLE `users_preferences` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `skin` varchar(64) NOT NULL,
  `skin_css` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users_preferences`
--

INSERT INTO `users_preferences` (`id`, `user_id`, `skin`, `skin_css`) VALUES
(1, 1, 'kira', 'skin_kira.css'),
(2, 2, 'siges', 'skin_siges.css'),
(3, 74, 'kira', 'skin_kira.css');

-- --------------------------------------------------------

--
-- Table structure for table `year_migration_check`
--

CREATE TABLE `year_migration_check` (
  `id` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `period` int(11) NOT NULL,
  `postulant` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `passing_grade` int(11) NOT NULL,
  `reportcard_observation` int(11) NOT NULL,
  `appreciations_grid` int(11) NOT NULL,
  `fees` int(11) NOT NULL,
  `taxes` int(11) NOT NULL,
  `pending_balance` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `year_migration_check`
--

INSERT INTO `year_migration_check` (`id`, `academic_year`, `period`, `postulant`, `student`, `course`, `evaluation`, `passing_grade`, `reportcard_observation`, `appreciations_grid`, `fees`, `taxes`, `pending_balance`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 6, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 0, '2019-08-10', '2019-09-11', 'admin', 'admin');

-- --------------------------------------------------------

--
-- Structure for view `code`
--
DROP TABLE IF EXISTS `code`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `code`  AS  select concat(ucase(left(`persons`.`first_name`,2)),ucase(left(`persons`.`last_name`,2)),`persons`.`id`) AS `code_id`,`persons`.`id` AS `id` from `persons` ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academicperiods`
--
ALTER TABLE `academicperiods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_periods_academic_periods1_idx` (`year`);

--
-- Indexes for table `accounting`
--
ALTER TABLE `accounting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_action_module` (`module_id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`,`create_by`);

--
-- Indexes for table `appreciations_grid`
--
ALTER TABLE `appreciations_grid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_year` (`academic_year`),
  ADD KEY `section` (`section`);

--
-- Indexes for table `arrondissements`
--
ALTER TABLE `arrondissements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `arrondissement_name` (`arrondissement_name`),
  ADD KEY `departement` (`departement`);

--
-- Indexes for table `average_by_period`
--
ALTER TABLE `average_by_period`
  ADD PRIMARY KEY (`academic_year`,`evaluation_by_year`,`student`),
  ADD KEY `fk_average_by_period_eval_by_y` (`evaluation_by_year`),
  ADD KEY `fk_average_by_period_person` (`student`);

--
-- Indexes for table `balance`
--
ALTER TABLE `balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`);

--
-- Indexes for table `bareme`
--
ALTER TABLE `bareme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_biiling_student_idx` (`student`),
  ADD KEY `fk_payment_method_idx` (`payment_method`),
  ADD KEY `fk_billings_fee_period` (`fee_period`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `reservation_id` (`reservation_id`);

--
-- Indexes for table `charge_description`
--
ALTER TABLE `charge_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `charge_paid`
--
ALTER TABLE `charge_paid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `id_charge_description` (`id_charge_description`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name_UNIQUE` (`city_name`),
  ADD KEY `fk_cities_arrondissement_idx` (`arrondissement`);

--
-- Indexes for table `cms_album_cat`
--
ALTER TABLE `cms_album_cat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `album_name` (`album_name`);

--
-- Indexes for table `cms_article`
--
ALTER TABLE `cms_article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section` (`section`),
  ADD KEY `fk_menu` (`article_menu`),
  ADD KEY `featured_image` (`featured_image`);

--
-- Indexes for table `cms_doc`
--
ALTER TABLE `cms_doc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `document_name` (`document_name`),
  ADD KEY `document_title` (`document_title`);

--
-- Indexes for table `cms_image`
--
ALTER TABLE `cms_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `label_image` (`label_image`),
  ADD KEY `album` (`album`);

--
-- Indexes for table `cms_menu`
--
ALTER TABLE `cms_menu`
  ADD PRIMARY KEY (`id`),
  ADD KEY `is_parent_menu` (`is_parent_menu`),
  ADD KEY `parent_menu` (`parent_menu`),
  ADD KEY `is_special` (`is_special`);

--
-- Indexes for table `cms_section`
--
ALTER TABLE `cms_section`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `section_name` (`section_name`);

--
-- Indexes for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_contact_info_idx` (`person`),
  ADD KEY `fk_relationship_idx` (`contact_relationship`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_couse_subject_idx` (`subject`),
  ADD KEY `fk_course_teacher_idx` (`teacher`),
  ADD KEY `room_idx` (`room`),
  ADD KEY `fk_course_period_academic_idx` (`academic_period`);

--
-- Indexes for table `custom_field`
--
ALTER TABLE `custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_data`
--
ALTER TABLE `custom_field_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_custom_field_idx` (`field_link`),
  ADD KEY `fk_object_id_idx` (`object_id`),
  ADD KEY `object_type` (`object_type`);

--
-- Indexes for table `cycles`
--
ALTER TABLE `cycles`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `decision_finale`
--
ALTER TABLE `decision_finale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_decision_idx` (`student`),
  ADD KEY `fk_academic_year_decision_idx` (`academic_year`),
  ADD KEY `fk_current_level_idx` (`current_level`),
  ADD KEY `fk_next_level_idx` (`next_level`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department_name_UNIQUE` (`department_name`);

--
-- Indexes for table `department_has_person`
--
ALTER TABLE `department_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `employee` (`employee`);

--
-- Indexes for table `department_in_school`
--
ALTER TABLE `department_in_school`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devises`
--
ALTER TABLE `devises`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `devise_name_UNIQUE` (`devise_name`),
  ADD UNIQUE KEY `devise_symbol_UNIQUE` (`devise_symbol`);

--
-- Indexes for table `devoirs`
--
ALTER TABLE `devoirs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_devoirs_persons_student` (`student`),
  ADD KEY `fk_devoirs_courses` (`course`),
  ADD KEY `fk_devoirs_academicperiods` (`academic_period`);

--
-- Indexes for table `employee_info`
--
ALTER TABLE `employee_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_employee_qualification_idx` (`qualification`),
  ADD KEY `fk_employee_job_status_idx` (`job_status`),
  ADD KEY `fk_employee_field_of_study_idx` (`field_study`),
  ADD KEY `fk_employee_person` (`employee`);

--
-- Indexes for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postulant` (`postulant`),
  ADD KEY `apply_level` (`apply_level`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `payment_method` (`payment_method`);

--
-- Indexes for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_evaluation_year_evaluation_idx` (`evaluation`),
  ADD KEY `fk_evaluation_year_academic_year_idx` (`academic_year`);

--
-- Indexes for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`),
  ADD KEY `subject` (`subject`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `devise` (`devise`),
  ADD KEY `fee` (`fee`);

--
-- Indexes for table `fees_label`
--
ALTER TABLE `fees_label`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field_study`
--
ALTER TABLE `field_study`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `field_name_UNIQUE` (`field_name`);

--
-- Indexes for table `general_average_by_period`
--
ALTER TABLE `general_average_by_period`
  ADD PRIMARY KEY (`academic_year`,`academic_period`,`student`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `student` (`student`);

--
-- Indexes for table `general_config`
--
ALTER TABLE `general_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_name_UNIQUE` (`item_name`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grades_student_idx` (`student`),
  ADD KEY `fk_grades_course_idx` (`course`),
  ADD KEY `fk_grades_evaluation_idx` (`evaluation`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `belongs_to_profil` (`belongs_to_profil`);

--
-- Indexes for table `groups_has_actions`
--
ALTER TABLE `groups_has_actions`
  ADD PRIMARY KEY (`groups_id`,`actions_id`),
  ADD KEY `fk_groups_has_actions_actions1_idx` (`actions_id`),
  ADD KEY `fk_groups_has_actions_groups1_idx` (`groups_id`);

--
-- Indexes for table `groups_has_modules`
--
ALTER TABLE `groups_has_modules`
  ADD PRIMARY KEY (`groups_id`,`modules_id`),
  ADD KEY `fk_groups_has_modules_modules` (`modules_id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD UNIQUE KEY `url` (`url`);

--
-- Indexes for table `homework`
--
ALTER TABLE `homework`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course` (`course`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `homework_submission`
--
ALTER TABLE `homework_submission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `homework_id` (`homework_id`);

--
-- Indexes for table `idcard`
--
ALTER TABLE `idcard`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `person_id` (`person_id`);

--
-- Indexes for table `infraction_type`
--
ALTER TABLE `infraction_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `job_status`
--
ALTER TABLE `job_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_name_UNIQUE` (`status_name`);

--
-- Indexes for table `kinder_cat_concept`
--
ALTER TABLE `kinder_cat_concept`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kinder_concept`
--
ALTER TABLE `kinder_concept`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`),
  ADD KEY `level` (`level`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `teacher` (`teacher`);

--
-- Indexes for table `kinder_evaluation`
--
ALTER TABLE `kinder_evaluation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `concept` (`concept`),
  ADD KEY `mention` (`mention`),
  ADD KEY `from_special_cat` (`from_special_cat`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `period` (`period`);

--
-- Indexes for table `kinder_mention`
--
ALTER TABLE `kinder_mention`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mention_name` (`mention_name`),
  ADD UNIQUE KEY `mention_short_name` (`mention_short_name`);

--
-- Indexes for table `kinder_period`
--
ALTER TABLE `kinder_period`
  ADD PRIMARY KEY (`id`),
  ADD KEY `period_name` (`period_name`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `kinder_teacher`
--
ALTER TABLE `kinder_teacher`
  ADD PRIMARY KEY (`id`),
  ADD KEY `teacher` (`teacher`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `room` (`room`),
  ADD KEY `new_teacher` (`new_teacher`);

--
-- Indexes for table `label_category_for_billing`
--
ALTER TABLE `label_category_for_billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level_name_UNIQUE` (`level_name`),
  ADD KEY `fk_levels_levels1_idx` (`previous_level`),
  ADD KEY `section` (`section`);

--
-- Indexes for table `level_has_person`
--
ALTER TABLE `level_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_level_idx` (`students`),
  ADD KEY `fk_student_level_year_idx` (`academic_year`),
  ADD KEY `fk_level_students_idx` (`level`);

--
-- Indexes for table `loan_of_money`
--
ALTER TABLE `loan_of_money`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_payroll_set` (`person_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from` (`sender`(191),`subject`(191),`is_read`),
  ADD KEY `id_sender` (`id_sender`);

--
-- Indexes for table `menfp_decision`
--
ALTER TABLE `menfp_decision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `menfp_grades`
--
ALTER TABLE `menfp_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `menfp_exam` (`menfp_exam`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `module_short_UNIQUE` (`module_short_name`),
  ADD UNIQUE KEY `module_name_UNIQUE` (`module_name`);

--
-- Indexes for table `other_incomes`
--
ALTER TABLE `other_incomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `id_income_description` (`id_income_description`);

--
-- Indexes for table `other_incomes_description`
--
ALTER TABLE `other_incomes_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passing_grades`
--
ALTER TABLE `passing_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_level_passing_grade_idx` (`level`),
  ADD KEY `fk_academic_period_passing_idx` (`academic_period`),
  ADD KEY `course` (`course`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `method_name_UNIQUE` (`method_name`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_paroll_set` (`id_payroll_set`);

--
-- Indexes for table `payroll_settings`
--
ALTER TABLE `payroll_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`,`academic_year`),
  ADD KEY `fk_payroll_setting_academicperiods` (`academic_year`);

--
-- Indexes for table `payroll_setting_taxes`
--
ALTER TABLE `payroll_setting_taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_payroll_set` (`id_payroll_set`),
  ADD KEY `id_taxe` (`id_taxe`);

--
-- Indexes for table `pending_balance`
--
ALTER TABLE `pending_balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pending_balance_persons` (`student`),
  ADD KEY `fk_pending_balance_academicperiods` (`academic_year`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persons_cities1_idx` (`cities`);

--
-- Indexes for table `persons_has_titles`
--
ALTER TABLE `persons_has_titles`
  ADD PRIMARY KEY (`persons_id`,`titles_id`,`academic_year`),
  ADD KEY `fk_persons_has_titles_titles1_idx` (`titles_id`),
  ADD KEY `fk_persons_has_titles_persons1_idx` (`persons_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `person_history`
--
ALTER TABLE `person_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `postulant`
--
ALTER TABLE `postulant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities` (`cities`),
  ADD KEY `person_liable_relation` (`person_liable_relation`),
  ADD KEY `apply_for_level` (`apply_for_level`),
  ADD KEY `previous_level` (`previous_level`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `email` (`email`),
  ADD KEY `citizenship` (`citizenship`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_name` (`product_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil_has_modules`
--
ALTER TABLE `profil_has_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `profil_id` (`profil_id`);

--
-- Indexes for table `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `qualification_name_UNIQUE` (`qualification_name`);

--
-- Indexes for table `raise_salary`
--
ALTER TABLE `raise_salary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`,`academic_year`);

--
-- Indexes for table `record_checkout`
--
ALTER TABLE `record_checkout`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `record_infraction`
--
ALTER TABLE `record_infraction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indx_stud_infrac` (`student`),
  ADD KEY `infraction_type` (`infraction_type`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `exam_period` (`exam_period`);

--
-- Indexes for table `record_presence`
--
ALTER TABLE `record_presence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `room` (`room`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `exam_period` (`exam_period`);

--
-- Indexes for table `record_special_presence`
--
ALTER TABLE `record_special_presence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `student` (`student`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `relation_name_UNIQUE` (`relation_name`);

--
-- Indexes for table `reportcard_observation`
--
ALTER TABLE `reportcard_observation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_year` (`academic_year`),
  ADD KEY `section` (`section`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reservation_payment_method` (`payment_method`),
  ADD KEY `fk_reservation_academic_year` (`academic_year`);

--
-- Indexes for table `return_history`
--
ALTER TABLE `return_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaction` (`id_transaction`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_room_levels1_idx` (`level`),
  ADD KEY `fk_room_shift_idx` (`shift`);

--
-- Indexes for table `room_has_person`
--
ALTER TABLE `room_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_level_idx` (`students`),
  ADD KEY `fk_student_level_year_idx` (`academic_year`),
  ADD KEY `fk_level_students_idx` (`room`);

--
-- Indexes for table `rpt_custom`
--
ALTER TABLE `rpt_custom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `rpt_custom_cat`
--
ALTER TABLE `rpt_custom_cat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorie_name` (`categorie_name`);

--
-- Indexes for table `sale_transaction`
--
ALTER TABLE `sale_transaction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_transaction` (`id_transaction`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `scalendar`
--
ALTER TABLE `scalendar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_scalendar_academic_year` (`academic_year`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_schedule_course_idx` (`course`);

--
-- Indexes for table `schedule_agenda`
--
ALTER TABLE `schedule_agenda`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_schedule_agenda_academic_year` (`academic_year`),
  ADD KEY `fk_course_agenda_courses` (`course`);

--
-- Indexes for table `scholarship_holder`
--
ALTER TABLE `scholarship_holder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_scholarship_holder_student` (`student`),
  ADD KEY `fk_scholarship_holder_academicperiods` (`academic_year`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_has_cycle`
--
ALTER TABLE `section_has_cycle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cycle` (`cycle`,`section`),
  ADD KEY `section` (`section`),
  ADD KEY `level` (`level`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `sellings`
--
ALTER TABLE `sellings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_products` (`id_products`),
  ADD KEY `transac` (`transaction_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siges_payment`
--
ALTER TABLE `siges_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_method` (`payment_method`),
  ADD KEY `id_siges_payment_set` (`id_siges_payment_set`);

--
-- Indexes for table `siges_payment_set`
--
ALTER TABLE `siges_payment_set`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `devise` (`devise`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `stock_history`
--
ALTER TABLE `stock_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_stock` (`id_stock`);

--
-- Indexes for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_documents_persons` (`id_student`);

--
-- Indexes for table `student_other_info`
--
ALTER TABLE `student_other_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_other_info` (`student`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subjects_subjects1_idx` (`subject_parent`);

--
-- Indexes for table `subject_average`
--
ALTER TABLE `subject_average`
  ADD PRIMARY KEY (`academic_year`,`evaluation_by_year`,`course`),
  ADD KEY `fk_subject_average_evaluation_byyear` (`evaluation_by_year`),
  ADD KEY `fk_subject_average_course` (`course`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title_name_UNIQUE` (`title_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `person_idx` (`person_id`),
  ADD KEY `profil` (`profil`);

--
-- Indexes for table `users_preferences`
--
ALTER TABLE `users_preferences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `skin` (`skin`);

--
-- Indexes for table `year_migration_check`
--
ALTER TABLE `year_migration_check`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academicperiods`
--
ALTER TABLE `academicperiods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `accounting`
--
ALTER TABLE `accounting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=433;
--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `appreciations_grid`
--
ALTER TABLE `appreciations_grid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `arrondissements`
--
ALTER TABLE `arrondissements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;
--
-- AUTO_INCREMENT for table `balance`
--
ALTER TABLE `balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT for table `bareme`
--
ALTER TABLE `bareme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=366;
--
-- AUTO_INCREMENT for table `charge_description`
--
ALTER TABLE `charge_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `charge_paid`
--
ALTER TABLE `charge_paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;
--
-- AUTO_INCREMENT for table `cms_album_cat`
--
ALTER TABLE `cms_album_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `cms_article`
--
ALTER TABLE `cms_article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `cms_doc`
--
ALTER TABLE `cms_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `cms_image`
--
ALTER TABLE `cms_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `cms_menu`
--
ALTER TABLE `cms_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `cms_section`
--
ALTER TABLE `cms_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `contact_info`
--
ALTER TABLE `contact_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;
--
-- AUTO_INCREMENT for table `custom_field`
--
ALTER TABLE `custom_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `custom_field_data`
--
ALTER TABLE `custom_field_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT for table `cycles`
--
ALTER TABLE `cycles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `decision_finale`
--
ALTER TABLE `decision_finale`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;
--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `department_has_person`
--
ALTER TABLE `department_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `department_in_school`
--
ALTER TABLE `department_in_school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `devises`
--
ALTER TABLE `devises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `devoirs`
--
ALTER TABLE `devoirs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `employee_info`
--
ALTER TABLE `employee_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `fees_label`
--
ALTER TABLE `fees_label`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `field_study`
--
ALTER TABLE `field_study`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `general_config`
--
ALTER TABLE `general_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=136;
--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2684;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `homework`
--
ALTER TABLE `homework`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `homework_submission`
--
ALTER TABLE `homework_submission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `idcard`
--
ALTER TABLE `idcard`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `infraction_type`
--
ALTER TABLE `infraction_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=47;
--
-- AUTO_INCREMENT for table `job_status`
--
ALTER TABLE `job_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `kinder_cat_concept`
--
ALTER TABLE `kinder_cat_concept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `kinder_concept`
--
ALTER TABLE `kinder_concept`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;
--
-- AUTO_INCREMENT for table `kinder_evaluation`
--
ALTER TABLE `kinder_evaluation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=68;
--
-- AUTO_INCREMENT for table `kinder_mention`
--
ALTER TABLE `kinder_mention`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `kinder_period`
--
ALTER TABLE `kinder_period`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `kinder_teacher`
--
ALTER TABLE `kinder_teacher`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `label_category_for_billing`
--
ALTER TABLE `label_category_for_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'School level (calling classes dans le system scolaire Haitien)', AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `level_has_person`
--
ALTER TABLE `level_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=430;
--
-- AUTO_INCREMENT for table `loan_of_money`
--
ALTER TABLE `loan_of_money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menfp_decision`
--
ALTER TABLE `menfp_decision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `menfp_grades`
--
ALTER TABLE `menfp_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `other_incomes`
--
ALTER TABLE `other_incomes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `other_incomes_description`
--
ALTER TABLE `other_incomes_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `passing_grades`
--
ALTER TABLE `passing_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT for table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `payroll_settings`
--
ALTER TABLE `payroll_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT for table `payroll_setting_taxes`
--
ALTER TABLE `payroll_setting_taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=151;
--
-- AUTO_INCREMENT for table `pending_balance`
--
ALTER TABLE `pending_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=333;
--
-- AUTO_INCREMENT for table `person_history`
--
ALTER TABLE `person_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `postulant`
--
ALTER TABLE `postulant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `profil_has_modules`
--
ALTER TABLE `profil_has_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `qualifications`
--
ALTER TABLE `qualifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `raise_salary`
--
ALTER TABLE `raise_salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `record_checkout`
--
ALTER TABLE `record_checkout`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `record_infraction`
--
ALTER TABLE `record_infraction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `record_presence`
--
ALTER TABLE `record_presence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=282;
--
-- AUTO_INCREMENT for table `record_special_presence`
--
ALTER TABLE `record_special_presence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `reportcard_observation`
--
ALTER TABLE `reportcard_observation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `return_history`
--
ALTER TABLE `return_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `room_has_person`
--
ALTER TABLE `room_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=436;
--
-- AUTO_INCREMENT for table `rpt_custom`
--
ALTER TABLE `rpt_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `rpt_custom_cat`
--
ALTER TABLE `rpt_custom_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sale_transaction`
--
ALTER TABLE `sale_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scalendar`
--
ALTER TABLE `scalendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `schedule_agenda`
--
ALTER TABLE `schedule_agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `scholarship_holder`
--
ALTER TABLE `scholarship_holder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `section_has_cycle`
--
ALTER TABLE `section_has_cycle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `sellings`
--
ALTER TABLE `sellings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `siges_payment`
--
ALTER TABLE `siges_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `siges_payment_set`
--
ALTER TABLE `siges_payment_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `stock_history`
--
ALTER TABLE `stock_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_documents`
--
ALTER TABLE `student_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `student_other_info`
--
ALTER TABLE `student_other_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=128;
--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=350;
--
-- AUTO_INCREMENT for table `users_preferences`
--
ALTER TABLE `users_preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `year_migration_check`
--
ALTER TABLE `year_migration_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `appreciations_grid`
--
ALTER TABLE `appreciations_grid`
  ADD CONSTRAINT `fk_appreciations_grid_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_appreciations_grid_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `arrondissements`
--
ALTER TABLE `arrondissements`
  ADD CONSTRAINT `arrondissements_ibfk_1` FOREIGN KEY (`departement`) REFERENCES `departments` (`id`);

--
-- Constraints for table `average_by_period`
--
ALTER TABLE `average_by_period`
  ADD CONSTRAINT `fk_average_by_period_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_average_by_period_eval_by_y` FOREIGN KEY (`evaluation_by_year`) REFERENCES `evaluation_by_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_average_by_period_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `balance`
--
ALTER TABLE `balance`
  ADD CONSTRAINT `fk_balance_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `fk_biiling_reservation` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_biiling_student` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_billings_fees` FOREIGN KEY (`fee_period`) REFERENCES `fees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_billing_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `fk_billing_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `charge_paid`
--
ALTER TABLE `charge_paid`
  ADD CONSTRAINT `fk_charge_paid_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_charge_paid_charge_description` FOREIGN KEY (`id_charge_description`) REFERENCES `charge_description` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `fk_cities_arrondissement` FOREIGN KEY (`arrondissement`) REFERENCES `arrondissements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_article`
--
ALTER TABLE `cms_article`
  ADD CONSTRAINT `cms_article_ibfk_1` FOREIGN KEY (`section`) REFERENCES `cms_section` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_article_menu` FOREIGN KEY (`article_menu`) REFERENCES `cms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `cms_image`
--
ALTER TABLE `cms_image`
  ADD CONSTRAINT `cms_image_ibfk_1` FOREIGN KEY (`album`) REFERENCES `cms_album_cat` (`id`);

--
-- Constraints for table `cms_menu`
--
ALTER TABLE `cms_menu`
  ADD CONSTRAINT `cms_menu_ibfk_1` FOREIGN KEY (`parent_menu`) REFERENCES `cms_menu` (`id`);

--
-- Constraints for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD CONSTRAINT `fk_contact_info` FOREIGN KEY (`person`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_relationship` FOREIGN KEY (`contact_relationship`) REFERENCES `relations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `fk_course_period_academic` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_course_room` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_course_teacher` FOREIGN KEY (`teacher`) REFERENCES `persons` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_couse_subject` FOREIGN KEY (`subject`) REFERENCES `subjects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `cycles`
--
ALTER TABLE `cycles`
  ADD CONSTRAINT `fk_cycles_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `decision_finale`
--
ALTER TABLE `decision_finale`
  ADD CONSTRAINT `fk_academic_year_decision` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_current_level` FOREIGN KEY (`current_level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_next_level` FOREIGN KEY (`next_level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_students_decision` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `department_has_person`
--
ALTER TABLE `department_has_person`
  ADD CONSTRAINT `fk_depatment_in_school_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_depatment_in_school_depatment_in_school` FOREIGN KEY (`department_id`) REFERENCES `department_in_school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_depatment_in_school_perso` FOREIGN KEY (`employee`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `devoirs`
--
ALTER TABLE `devoirs`
  ADD CONSTRAINT `fk_devoirs_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_devoirs_academicperiods_` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_devoirs_persons_students` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_info`
--
ALTER TABLE `employee_info`
  ADD CONSTRAINT `fk_employee_field_of_study` FOREIGN KEY (`field_study`) REFERENCES `field_study` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employee_job_status` FOREIGN KEY (`job_status`) REFERENCES `job_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employee_person` FOREIGN KEY (`employee`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employee_qualification` FOREIGN KEY (`qualification`) REFERENCES `qualifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  ADD CONSTRAINT `fk_enrollment_income_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_enrollment_income_level` FOREIGN KEY (`apply_level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_enrollment_income_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD CONSTRAINT `fk_evaluation_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`);

--
-- Constraints for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  ADD CONSTRAINT `fk_evaluation_year_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_evaluation_year_evaluation` FOREIGN KEY (`evaluation`) REFERENCES `evaluations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  ADD CONSTRAINT `examen_menfp_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `examen_menfp_levels` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `examen_menfp_sujects` FOREIGN KEY (`subject`) REFERENCES `subjects` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `fees`
--
ALTER TABLE `fees`
  ADD CONSTRAINT `fk_fees_academic_period` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fees_devise` FOREIGN KEY (`devise`) REFERENCES `devises` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fees_fees_label` FOREIGN KEY (`fee`) REFERENCES `fees_label` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fees_level` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `general_average_by_period`
--
ALTER TABLE `general_average_by_period`
  ADD CONSTRAINT `fk_general_average_period_academicperiod_academicYear` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_general_average_period_academicperiod_period` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_general_average_period_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `fk_grades_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_grades_evaluation` FOREIGN KEY (`evaluation`) REFERENCES `evaluation_by_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_grades_student` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `fk_group_profil_id` FOREIGN KEY (`belongs_to_profil`) REFERENCES `profil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups_has_actions`
--
ALTER TABLE `groups_has_actions`
  ADD CONSTRAINT `fk_groups_has_actions_actions` FOREIGN KEY (`actions_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_groups_has_actions_groups` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups_has_modules`
--
ALTER TABLE `groups_has_modules`
  ADD CONSTRAINT `fk_groups_has_modules_groups` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_groups_has_modules_modules` FOREIGN KEY (`modules_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `homework`
--
ALTER TABLE `homework`
  ADD CONSTRAINT `fk_homework_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_homework_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `homework_submission`
--
ALTER TABLE `homework_submission`
  ADD CONSTRAINT `fk_homework_submission_homework` FOREIGN KEY (`homework_id`) REFERENCES `homework` (`id`),
  ADD CONSTRAINT `fk_homework_submission_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`);

--
-- Constraints for table `idcard`
--
ALTER TABLE `idcard`
  ADD CONSTRAINT `fk_idcard_persons` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `kinder_concept`
--
ALTER TABLE `kinder_concept`
  ADD CONSTRAINT `kinder_concept_ibfk_1` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `kinder_concept_ibfk_4` FOREIGN KEY (`category`) REFERENCES `kinder_cat_concept` (`id`),
  ADD CONSTRAINT `kinder_concept_ibfk_5` FOREIGN KEY (`level`) REFERENCES `levels` (`id`);

--
-- Constraints for table `kinder_evaluation`
--
ALTER TABLE `kinder_evaluation`
  ADD CONSTRAINT `kinder_evaluation_ibfk_1` FOREIGN KEY (`student`) REFERENCES `persons` (`id`),
  ADD CONSTRAINT `kinder_evaluation_ibfk_3` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `kinder_evaluation_ibfk_4` FOREIGN KEY (`from_special_cat`) REFERENCES `kinder_cat_concept` (`id`),
  ADD CONSTRAINT `kinder_evaluation_ibfk_5` FOREIGN KEY (`concept`) REFERENCES `kinder_concept` (`id`),
  ADD CONSTRAINT `kinder_evaluation_ibfk_6` FOREIGN KEY (`period`) REFERENCES `kinder_period` (`id`),
  ADD CONSTRAINT `kinder_evaluation_ibfk_7` FOREIGN KEY (`mention`) REFERENCES `kinder_mention` (`id`);

--
-- Constraints for table `kinder_period`
--
ALTER TABLE `kinder_period`
  ADD CONSTRAINT `kinder_period_ibfk_1` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`);

--
-- Constraints for table `kinder_teacher`
--
ALTER TABLE `kinder_teacher`
  ADD CONSTRAINT `kinder_teacher_ibfk_4` FOREIGN KEY (`new_teacher`) REFERENCES `persons` (`id`),
  ADD CONSTRAINT `kinder_teacher_ibfk_1` FOREIGN KEY (`teacher`) REFERENCES `persons` (`id`),
  ADD CONSTRAINT `kinder_teacher_ibfk_2` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `kinder_teacher_ibfk_3` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `levels`
--
ALTER TABLE `levels`
  ADD CONSTRAINT `fk_levels_levels1` FOREIGN KEY (`previous_level`) REFERENCES `levels` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_levels_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`);

--
-- Constraints for table `level_has_person`
--
ALTER TABLE `level_has_person`
  ADD CONSTRAINT `fk_students_level` FOREIGN KEY (`students`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_student_level_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `level_has_person_ibfk_2` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `loan_of_money`
--
ALTER TABLE `loan_of_money`
  ADD CONSTRAINT `fk_loan_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`);

--
-- Constraints for table `menfp_decision`
--
ALTER TABLE `menfp_decision`
  ADD CONSTRAINT `menfp_decision_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `menfp_decision_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `menfp_grades`
--
ALTER TABLE `menfp_grades`
  ADD CONSTRAINT `menfp_grades_examen_menfp` FOREIGN KEY (`menfp_exam`) REFERENCES `examen_menfp` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `menfp_grades_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `other_incomes`
--
ALTER TABLE `other_incomes`
  ADD CONSTRAINT `fk_other_income_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `fk_other_income_incomes_description` FOREIGN KEY (`id_income_description`) REFERENCES `other_incomes_description` (`id`);

--
-- Constraints for table `passing_grades`
--
ALTER TABLE `passing_grades`
  ADD CONSTRAINT `fk_academic_period_passing` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_level_passing_grade` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_passing_grade_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`);

--
-- Constraints for table `payroll`
--
ALTER TABLE `payroll`
  ADD CONSTRAINT `fk_payroll_payroll_setting` FOREIGN KEY (`id_payroll_set`) REFERENCES `payroll_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payroll_settings`
--
ALTER TABLE `payroll_settings`
  ADD CONSTRAINT `fk_payroll_setting_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payroll_setting_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payroll_setting_taxes`
--
ALTER TABLE `payroll_setting_taxes`
  ADD CONSTRAINT `fk_payroll_setting_taxes_payroll_settings` FOREIGN KEY (`id_payroll_set`) REFERENCES `payroll_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payroll_setting_taxe_taxes` FOREIGN KEY (`id_taxe`) REFERENCES `taxes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pending_balance`
--
ALTER TABLE `pending_balance`
  ADD CONSTRAINT `fk_pending_balance_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pending_balance_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `persons`
--
ALTER TABLE `persons`
  ADD CONSTRAINT `fk_persons_cities1` FOREIGN KEY (`cities`) REFERENCES `cities` (`id`);

--
-- Constraints for table `persons_has_titles`
--
ALTER TABLE `persons_has_titles`
  ADD CONSTRAINT `fk_persons_has_titles_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_has_titles_persons1` FOREIGN KEY (`persons_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_has_titles_titles1` FOREIGN KEY (`titles_id`) REFERENCES `titles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `person_history`
--
ALTER TABLE `person_history`
  ADD CONSTRAINT `fk_person_history_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `postulant`
--
ALTER TABLE `postulant`
  ADD CONSTRAINT `fk_postulant_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `fk_postulant_apply_level` FOREIGN KEY (`apply_for_level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_postulant_cities` FOREIGN KEY (`cities`) REFERENCES `cities` (`id`),
  ADD CONSTRAINT `fk_postulant_previous_level` FOREIGN KEY (`previous_level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_postulant_relation` FOREIGN KEY (`person_liable_relation`) REFERENCES `relations` (`id`);

--
-- Constraints for table `profil_has_modules`
--
ALTER TABLE `profil_has_modules`
  ADD CONSTRAINT `fk_profil_has_modules_module_id` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_profil_has_modules_profil_id` FOREIGN KEY (`profil_id`) REFERENCES `profil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `raise_salary`
--
ALTER TABLE `raise_salary`
  ADD CONSTRAINT `fk_raise_salary_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`);

--
-- Constraints for table `record_checkout`
--
ALTER TABLE `record_checkout`
  ADD CONSTRAINT `record_checkout_ibfk_2` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `record_checkout_ibfk_1` FOREIGN KEY (`student`) REFERENCES `persons` (`id`);

--
-- Constraints for table `record_infraction`
--
ALTER TABLE `record_infraction`
  ADD CONSTRAINT `record_infraction_academic_year` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `record_infraction_ibfk_1` FOREIGN KEY (`student`) REFERENCES `persons` (`id`),
  ADD CONSTRAINT `record_infraction_ibfk_2` FOREIGN KEY (`infraction_type`) REFERENCES `infraction_type` (`id`),
  ADD CONSTRAINT `record_infraction_period_exam` FOREIGN KEY (`exam_period`) REFERENCES `academicperiods` (`id`);

--
-- Constraints for table `record_presence`
--
ALTER TABLE `record_presence`
  ADD CONSTRAINT `record_presence_ibfk_1` FOREIGN KEY (`student`) REFERENCES `persons` (`id`),
  ADD CONSTRAINT `record_presence_room_rooms` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `record_special_presence`
--
ALTER TABLE `record_special_presence`
  ADD CONSTRAINT `record_special_presence_ibfk_2` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `record_special_presence_ibfk_1` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `reportcard_observation`
--
ALTER TABLE `reportcard_observation`
  ADD CONSTRAINT `fk_reportcard_observation_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reportcard_observation_ibfk_1` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_reservation_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_reservation_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `fk_room_levels` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_room_shift` FOREIGN KEY (`shift`) REFERENCES `shifts` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `room_has_person`
--
ALTER TABLE `room_has_person`
  ADD CONSTRAINT `fk_room_students_level` FOREIGN KEY (`students`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_room_students_level_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `fk_room_student_room` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sale_transaction`
--
ALTER TABLE `sale_transaction`
  ADD CONSTRAINT `fk_sale_transaction_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `scholarship_holder`
--
ALTER TABLE `scholarship_holder`
  ADD CONSTRAINT `fk_scholarship_holder_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_scholarship_holder_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `section_has_cycle`
--
ALTER TABLE `section_has_cycle`
  ADD CONSTRAINT `fk_section_has_cycle_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_section_has_cycle_cycles` FOREIGN KEY (`cycle`) REFERENCES `cycles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_section_has_cycle_level` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_section_has_cycle_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `siges_payment`
--
ALTER TABLE `siges_payment`
  ADD CONSTRAINT `fk_siges_payment_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_siges_payment_siges_payment_set` FOREIGN KEY (`id_siges_payment_set`) REFERENCES `siges_payment_set` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `siges_payment_set`
--
ALTER TABLE `siges_payment_set`
  ADD CONSTRAINT `fk_siges_payment_set_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_siges_payment_set_devises` FOREIGN KEY (`devise`) REFERENCES `devises` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD CONSTRAINT `fk_student_documents_persons` FOREIGN KEY (`id_student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_other_info`
--
ALTER TABLE `student_other_info`
  ADD CONSTRAINT `fk_student_other_info_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subject_average`
--
ALTER TABLE `subject_average`
  ADD CONSTRAINT `fk_subject_average_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subject_average_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subject_average_eval_by_year` FOREIGN KEY (`evaluation_by_year`) REFERENCES `evaluation_by_year` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_user_profil_id` FOREIGN KEY (`profil`) REFERENCES `profil` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `year_migration_check`
--
ALTER TABLE `year_migration_check`
  ADD CONSTRAINT `fk_year_migration_check_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
