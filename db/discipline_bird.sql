/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Dec 18, 2018
 */

CREATE TABLE `disc_period_break` ( `id` INT NOT NULL AUTO_INCREMENT , `id_period` INT NOT NULL , `break_point` FLOAT NOT NULL , `academic_year` INT NOT NULL , PRIMARY KEY (`id`), INDEX (`id_period`), INDEX (`academic_year`)) ENGINE = InnoDB;
ALTER TABLE `disc_period_break` ADD FOREIGN KEY (`id_period`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `disc_period_break` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'discipline_ncb', 'Discipline NCB', '1', '0:N\'est pas Nouveau College Bird - 1: Est Nouveau College Bird', '0: Not Nouveau College Bird - 1: Is Nouveau College Bird', 'dis', '2018-12-18 00:00:00', NULL, 'LOGIPAM', NULL);INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'discipline_ncb', 'Discipline NCB', '1', '0:N\'est pas Nouveau College Bird - 1: Est Nouveau College Bird', '0: Not Nouveau College Bird - 1: Is Nouveau College Bird', 'dis', '2018-12-18 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `disc_period_break` (`id`, `id_period`, `break_point`, `academic_year`) VALUES
(1, 2, 70, 1),
(2, 3, 90, 1),
(3, 4, 130, 1),
(4, 5, 160, 1);

ALTER TABLE `disc_period_break` ADD `differentiel` FLOAT NOT NULL AFTER `academic_year`, ADD INDEX (`differentiel`);
UPDATE `disc_period_break` SET `differentiel` = '20' WHERE `disc_period_break`.`id` = 2;
UPDATE `disc_period_break` SET `differentiel` = '30' WHERE `disc_period_break`.`id` = 3;
UPDATE `disc_period_break` SET `differentiel` = '30' WHERE `disc_period_break`.`id` = 4;

CREATE TABLE `disc_grade_period` ( `id` INT NOT NULL AUTO_INCREMENT , `student` INT NOT NULL , `id_period` INT NOT NULL , `grade_value` FLOAT NOT NULL , `academic_year` INT NOT NULL , PRIMARY KEY (`id`), INDEX (`student`), INDEX (`id_period`), INDEX (`academic_year`)) ENGINE = InnoDB; 
ALTER TABLE `disc_grade_period` ADD FOREIGN KEY (`id_period`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `disc_grade_period` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `disc_grade_period` ADD FOREIGN KEY (`student`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `disc_grade_period` ADD `grade_net` FLOAT NULL AFTER `grade_value`; 
ALTER TABLE `disc_grade_period` CHANGE `grade_value` `deduction_value` FLOAT NOT NULL;
ALTER TABLE `disc_grade_period` ADD `differentiel` FLOAT NULL AFTER `grade_net`;