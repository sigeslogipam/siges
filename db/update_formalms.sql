/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  poulardjc
 * Created: Oct 3, 2020
 */

-- 3 octobre 2020 
CREATE TABLE `migration_formalms` ( `id` INT NOT NULL AUTO_INCREMENT , `data_type` VARCHAR(255) NOT NULL , `data` VARCHAR(255) NOT NULL , `is_migrate` BOOLEAN NOT NULL , `date_migration` DATETIME NOT NULL , `migrate_by` VARCHAR(255) NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 16 novembre 2020 
ALTER TABLE `courses` ADD `code_cours` VARCHAR(64) NULL AFTER `weight`;

