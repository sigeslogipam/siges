-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Aug 30, 2018 at 05:07 PM
-- Server version: 5.6.16-1~exp1
-- PHP Version: 5.6.31-4+ubuntu16.04.1+deb.sury.org+4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `siges_ncb`
--

-- --------------------------------------------------------

--
-- Table structure for table `academicperiods`
--

CREATE TABLE `academicperiods` (
  `id` int(11) NOT NULL,
  `name_period` varchar(45) NOT NULL,
  `weight` double DEFAULT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0',
  `date_start` date NOT NULL,
  `date_end` date NOT NULL,
  `is_year` tinyint(1) DEFAULT NULL,
  `previous_academic_year` int(11) NOT NULL COMMENT 'Annee academique precedente',
  `year` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `accounting`
--

CREATE TABLE `accounting` (
  `id` int(11) NOT NULL,
  `old_balance` double NOT NULL,
  `expenses` double NOT NULL,
  `incomes` double NOT NULL,
  `new_balance` double NOT NULL,
  `month` int(3) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `actions`
--

CREATE TABLE `actions` (
  `id` int(11) NOT NULL,
  `action_id` varchar(64) NOT NULL,
  `action_name` varchar(64) NOT NULL,
  `controller` varchar(64) NOT NULL,
  `module_id` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `actions`
--

INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES
(1, 'index', 'Lister les utilisateurs', 'User', 5, NULL, NULL, NULL, NULL),
(2, 'create', 'Creation d\'utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(3, 'update', 'Mise a jour d\'un utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(4, 'delete', 'Suppression d\'un utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(5, 'view', 'Affichage d\'un utilisateur', 'User', 5, NULL, NULL, NULL, NULL),
(6, 'changePassword', 'Modification du mot de passe', 'User', 5, NULL, NULL, NULL, NULL),
(7, 'updateUser', 'Mise a jour d\'un utilisateur en mode vue', 'User', 5, NULL, NULL, NULL, NULL),
(8, 'disableusers', 'Lister les utilisateurs inactifs', 'User', 5, NULL, NULL, NULL, NULL),
(9, 'listForReport', 'Liste personne pour rapport', 'Persons', 9, NULL, NULL, NULL, NULL),
(10, 'viewForReport', 'Vue personne pour rapport', 'Persons', 9, NULL, NULL, NULL, NULL),
(11, 'list', 'Liste eleve ', 'Persons', 9, NULL, NULL, NULL, NULL),
(12, 'update', 'mise a jour d\'une personne', 'Persons', 9, NULL, NULL, NULL, NULL),
(13, 'roomAffectation', 'Affecter des eleves a une salle', 'Persons', 9, NULL, NULL, NULL, NULL),
(14, 'create', 'Ajouter une nouvelle personne', 'Persons', 9, NULL, NULL, NULL, NULL),
(15, 'listArchive', 'Liste des personnes inactifs ', 'Persons', 9, NULL, NULL, NULL, NULL),
(16, 'exTeachers', 'Liste des anciens professeurs', 'Persons', 9, NULL, NULL, NULL, NULL),
(17, 'exEmployees', 'Liste des anciens employes', 'Persons', 9, NULL, NULL, NULL, NULL),
(18, 'exStudents', 'Liste des anciens eleves', 'Persons', 9, NULL, NULL, NULL, NULL),
(19, 'delete', 'Suppression d\'une personne', 'Persons', 9, NULL, NULL, NULL, NULL),
(20, 'index', 'Liste des eleves ayant des infos additinnelles', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(21, 'create', 'Creation des infos additinnelles pour un eleve', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(22, 'update', 'Mise a jour des infos additinnelles pour eleve', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(23, 'view', 'Affichage des infos additinnelles pour eleve', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(24, 'delete', 'Suppression d\'info additionnelle', 'StudentOtherInfo', 9, NULL, NULL, NULL, NULL),
(25, 'index', 'Liste des employes ayant des infos additinnelles', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(26, 'create', 'Creation des infos additionnelles pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(27, 'update', 'Mis a jour des infos additinnelles pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(28, 'view', 'Affichage des infos additinnelles pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(29, 'delete', 'Suppression d\'info additionnelle pour employe', 'Employeeinfo', 9, NULL, NULL, NULL, NULL),
(30, 'index', 'Liste de contact', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(31, 'create', 'Creation de contact pour chaque personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(32, 'update', 'mise a jour de contact pour une personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(33, 'view', 'Affichage de contact pour une personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(34, 'delete', 'Suppression de contact pour une personne', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(35, 'index', 'Liste des departements ayant des employes', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(36, 'create', 'Ajouter un employe dans un departement', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(37, 'update', 'Mise a jour d\'un employe dans un departement', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(38, 'view', 'Affichage d\'un employe dans un departement ', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(39, 'delete', 'Suppression d\'un employe dans un departement', 'DepartmentHasPerson', 9, NULL, NULL, NULL, NULL),
(40, 'index', 'Liste de notes de tous les eleves', 'Grades', 9, NULL, NULL, NULL, NULL),
(41, 'create', 'Ajouter une nouvelle note pour un eleve', 'Grades', 9, NULL, NULL, NULL, NULL),
(42, 'update', 'Mise a jour d\'une note pour un eleve', 'Grades', 9, NULL, NULL, NULL, NULL),
(43, 'view', 'Affichage d\'une note pour un eleve', 'Grades', 9, NULL, NULL, NULL, NULL),
(44, 'delete', 'Suppression d\'une note', 'Grades', 9, NULL, NULL, NULL, NULL),
(45, 'listByRoom', 'Affichage des notes par salle', 'Grades', 9, NULL, NULL, NULL, NULL),
(46, 'balance', 'Liste des eleves ayant une balance sup. a zero', 'Balance', 8, NULL, NULL, NULL, NULL),
(47, 'view', 'Affichage de balance pour un eleve', 'Balance', 8, NULL, NULL, NULL, NULL),
(48, 'create', 'Creation de balance pour un eleve', 'Balance', 8, NULL, NULL, NULL, NULL),
(49, 'update', 'Mise a jour d\'une balance pour un eleve', 'Balance', 8, NULL, NULL, NULL, NULL),
(50, 'index', 'Liste des transactions des eleves avec l\'economat', 'Billings', 8, NULL, NULL, NULL, NULL),
(51, 'create', 'Ajouter d\'une nouvelle transaction pour un eleve', 'Billings', 8, NULL, NULL, NULL, NULL),
(52, 'update', 'Mise a jour d\'une transaction pour un eleve', 'Billings', 8, NULL, NULL, NULL, NULL),
(53, 'view', 'Affichage d\'une transaction pour un eleve', 'Billings', 8, NULL, NULL, NULL, NULL),
(54, 'delete', 'Suppression d\'une transaction', 'Billings', 8, NULL, NULL, NULL, NULL),
(55, 'generalReport', 'Affichage d\'un rapport general', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(56, 'create', 'Creation de bulletin pour une salle', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(57, 'report', 'Affichage d\'un bulletin d\'un eleve', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(58, 'admitted', 'Liste des admis par salle', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(59, 'endYearDecision', 'Prise de decision de fin d\'annee', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(60, 'index', 'Liste des cours de l\'ecole', 'Courses', 7, NULL, NULL, NULL, NULL),
(61, 'create', 'Creation d\'un nouveau cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(62, 'update', 'Mise a jour d\'un cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(63, 'view', 'Affichage d\'un cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(64, 'viewForTeacher', 'Liste des cours de l\'ecole pour professeur', 'Courses', 7, NULL, NULL, NULL, NULL),
(65, 'delete', 'Suppression d\'un cours', 'Courses', 7, NULL, NULL, NULL, NULL),
(66, 'index', 'Liste des matieres enseignees ', 'Subjects', 7, NULL, NULL, NULL, NULL),
(67, 'create', 'Ajouter une nouvelle matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(68, 'update', 'Mise a jour d\'une matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(69, 'view', 'Affichage d\'une matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(70, 'delete', 'Suppression d\'une matiere', 'Subjects', 7, NULL, NULL, NULL, NULL),
(71, 'index', 'Liste des evaluations pour chaque periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(72, 'create', 'Creation d\'une nouvelle evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(73, 'update', 'Mise a jour d\'une evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(74, 'view', 'Affichage d\'une evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(75, 'delete', 'Suppression d\'une evaluation pour une periode', 'Evaluationbyyear', 7, NULL, NULL, NULL, NULL),
(76, 'index', 'Liste des heures de cours pour une salle', 'Schedules', 7, NULL, NULL, NULL, NULL),
(77, 'create', 'Ajouter d\'une heure de cours pour une salle', 'Schedules', 7, NULL, NULL, NULL, NULL),
(78, 'update', 'Mise a jour d\'une heure de cours', 'Schedules', 7, NULL, NULL, NULL, NULL),
(79, 'view', 'Affichage d\'une heure de cours pour une salle', 'Schedules', 7, NULL, NULL, NULL, NULL),
(80, 'viewForTeacher', 'Liste des heures de cours pour professeur', 'Schedules', 7, NULL, NULL, NULL, NULL),
(81, 'viewForUpdate', 'Liste des heures de cours pret a etre modifier', 'Schedules', 7, NULL, NULL, NULL, NULL),
(82, 'delete', 'Suppression d\'une heure de cours', 'Schedules', 7, NULL, NULL, NULL, NULL),
(83, 'index', 'Liste des periodes academiques', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(84, 'create', 'Creation d\'une nouvelle periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(85, 'update', 'Mise a jour d\'une periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(86, 'view', 'Affichage d\'une periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(87, 'delete', 'Suppression d\'une periode academique', 'Academicperiods', 1, NULL, NULL, NULL, NULL),
(88, 'index', 'Liste des departements de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(89, 'create', 'Creation d\'un nouveau departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(90, 'update', 'Mise a jour d\'un departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(91, 'view', 'Affichage d\'un departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(92, 'delete', 'Suppression d\'un departement de l\'ecole', 'DepartmentInSchool', 1, NULL, NULL, NULL, NULL),
(93, 'index', 'Liste des devises acceptees a l\'ecole', 'Devises', 1, NULL, NULL, NULL, NULL),
(94, 'create', 'Ajouter une nouvelle devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(95, 'update', 'Mise a jour d\'une devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(96, 'view', 'Affichage d\'une devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(97, 'delete', 'Suppression d\'une devise', 'Devises', 1, NULL, NULL, NULL, NULL),
(98, 'index', 'Liste des evaluations de l\'ecole', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(99, 'create', 'Ajouter une nouvelle evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(100, 'update', 'Mise a jour d\'une evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(101, 'view', 'Affichage d\'une evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(102, 'delete', 'Suppression d\'une evaluation', 'Evaluations', 1, NULL, NULL, NULL, NULL),
(103, 'index', 'Liste des frais exigees par l\'ecole', 'Fees', 1, NULL, NULL, NULL, NULL),
(104, 'create', 'Ajouter un nouveau frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(105, 'update', 'Mise a jour d\'un frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(106, 'view', 'Affichage d\'un frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(107, 'delete', 'Suppression d\'un frais', 'Fees', 1, NULL, NULL, NULL, NULL),
(108, 'index', 'Liste des domaines d\'etude des employes', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(109, 'create', 'Ajouter un nouveau domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(110, 'update', 'Mise a jour d\'un domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(111, 'view', 'Affichage d\'un domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(112, 'delete', 'Suppression d\'un domaine d\'etude', 'Fieldstudy', 1, NULL, NULL, NULL, NULL),
(113, 'index', 'Liste des champs de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(114, 'admin', 'Liste des champs de configuration generale Pret a modifier', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(115, 'create', 'Creation d\'un nouveau champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(116, 'update', 'Mise a jour d\'un champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(117, 'view', 'Affichage d\'un champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(118, 'delete', 'Suppression d\'un champ de configuration generale', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(119, 'index', 'Liste des statuts d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(120, 'create', 'Creation d\'un nouveau statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(121, 'update', 'Mise a jour d\'un statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(122, 'view', 'Affichage d\'un statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(123, 'delete', 'Suppression d\'un statut d\'emploi', 'Jobstatus', 1, NULL, NULL, NULL, NULL),
(124, 'index', 'Liste des differents niveaux d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(125, 'create', 'Creation d\'un nouveau niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(126, 'update', 'Mise a jour d\'un niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(127, 'view', 'Affichage d\'un niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(128, 'delete', 'Suppression d\'un niveau d\'etude', 'Levels', 1, NULL, NULL, NULL, NULL),
(129, 'index', 'Liste des notes de passage de l\'ecole', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(130, 'create', 'Creation d\'une nouvelle note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(131, 'update', 'Mise a jour d\'une note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(132, 'view', 'Affichage d\'une note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(133, 'delete', 'Suppression d\'une note de passage', 'Passinggrades', 1, NULL, NULL, NULL, NULL),
(134, 'index', 'Liste des methodes de paiement exigees par l\'ecole', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(135, 'create', 'Creation d\'une nouvelle methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(136, 'update', 'Mise a jour d\'une methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(137, 'view', 'Affichage d\'une methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(138, 'delete', 'Suppression d\'une methode de paiement', 'Paymentmethod', 1, NULL, NULL, NULL, NULL),
(139, 'index', 'Liste des differents titres de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(140, 'create', 'Creation d\'un nouveau titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(141, 'update', 'Mise a jour d\'un titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(142, 'view', 'Affichage d\'un titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(143, 'delete', 'Suppression d\'un titre de qualification', 'Qualifications', 1, NULL, NULL, NULL, NULL),
(144, 'index', 'Liste de relations eleve et responsable', 'Relations', 1, NULL, NULL, NULL, NULL),
(145, 'create', 'Creation d\'un nouveau titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(146, 'update', 'Mise a jour d\'un titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(147, 'view', 'Affichage d\'un titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(148, 'delete', 'Suppression d\'un titre de relation', 'Relations', 1, NULL, NULL, NULL, NULL),
(149, 'index', 'Liste de toutes les salles de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(150, 'create', 'Creation d\'une nouvelle salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(151, 'update', 'Mise a jour d\'une salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(152, 'view', 'Affichage d\'une salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(153, 'delete', 'Suppression d\'une salle de classe', 'Rooms', 1, NULL, NULL, NULL, NULL),
(154, 'index', 'Liste de toutes les sections de l\'ecole', 'Sections', 1, NULL, NULL, NULL, NULL),
(155, 'create', 'Creation d\'une nouvelle section', 'Sections', 1, NULL, NULL, NULL, NULL),
(156, 'update', 'Mise a jour d\'une section', 'Sections', 1, NULL, NULL, NULL, NULL),
(157, 'view', 'Affichage d\'une section', 'Sections', 1, NULL, NULL, NULL, NULL),
(158, 'delete', 'Suppression d\'une section', 'Sections', 1, NULL, NULL, NULL, NULL),
(159, 'index', 'Liste de toutes les vacations de l\'ecole', 'Shifts', 1, NULL, NULL, NULL, NULL),
(160, 'create', 'Creation d\'une nouvelle vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(161, 'update', 'Mise a jour d\'une vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(162, 'view', 'Affichage d\'une vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(163, 'delete', 'Suppression d\'une vacation', 'Shifts', 1, NULL, NULL, NULL, NULL),
(164, 'index', 'Liste des positions administratives', 'Titles', 1, NULL, NULL, NULL, NULL),
(165, 'create', 'Creation d\'une nouvelle position', 'Titles', 1, NULL, NULL, NULL, NULL),
(166, 'update', 'Mise a jour d\'une position', 'Titles', 1, NULL, NULL, NULL, NULL),
(167, 'view', 'Affichage d\'une position', 'Titles', 1, NULL, NULL, NULL, NULL),
(168, 'delete', 'Suppression d\'une position', 'Titles', 1, NULL, NULL, NULL, NULL),
(169, 'index', 'Liste de toutes les modules du systeme', 'Modules', 5, NULL, NULL, NULL, NULL),
(170, 'create', 'Creation d\'une nouvelle module du systeme', 'Modules', 5, NULL, NULL, NULL, NULL),
(171, 'update', 'Mise a jour d\'une module', 'Modules', 5, NULL, NULL, NULL, NULL),
(172, 'view', 'Affichage d\'une module', 'Modules', 5, NULL, NULL, NULL, NULL),
(173, 'delete', 'Suppression d\'une module', 'Modules', 5, NULL, NULL, NULL, NULL),
(174, 'index', 'Liste de tous les groupes utilisateur du systeme', 'Groups', 5, NULL, NULL, NULL, NULL),
(175, 'create', 'Creation d\'un nouveau groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(176, 'update', 'Mise a jour d\'un groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(177, 'view', 'Affichage d\'un groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(178, 'delete', 'Suppression d\'un groupe utilisateur', 'Groups', 5, NULL, NULL, NULL, NULL),
(179, 'index', 'Liste de toutes les actions autorisees du systeme', 'Actions', 5, NULL, NULL, NULL, NULL),
(180, 'create', 'Creation d\'une nouvelle action', 'Actions', 5, NULL, NULL, NULL, NULL),
(181, 'update', 'Mise a jour d\'une action', 'Actions', 5, NULL, NULL, NULL, NULL),
(182, 'view', 'Affichage d\'une action', 'Actions', 5, NULL, NULL, NULL, NULL),
(183, 'delete', 'Suppression d\'une action', 'Actions', 5, NULL, NULL, NULL, NULL),
(184, 'validatePublish', 'Validation et Publication des notes', 'Grades', 9, NULL, NULL, NULL, NULL),
(185, 'index', 'Liste des periodes academiques pour INVITE', 'Academicperiods', 10, NULL, NULL, NULL, NULL),
(186, 'balance', 'Liste des eleves ayant une balance sup. a zero  pour INVITE', 'Balance', 10, NULL, NULL, NULL, NULL),
(187, 'index', 'Liste des transactions des eleves avec l\'economat  pour INVITE', 'Billings', 10, NULL, NULL, NULL, NULL),
(188, 'view', 'Affichage des info personnelles parent', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(189, 'update', 'Mise a jour des infos personnelles parent', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(190, 'index', 'Liste des cours que suit l\'eleve', 'Courses', 10, NULL, NULL, NULL, NULL),
(191, 'index', 'Liste des evaluations pour chaque periode  pour INVITE', 'Evaluationbyyear', 10, NULL, NULL, NULL, NULL),
(192, 'index', 'Liste des frais exigees par l\'ecole  pour INVITE', 'Fees', 10, NULL, NULL, NULL, NULL),
(193, 'index', 'Liste des notes (publiees) de l\'eleve', 'Grades', 10, NULL, NULL, NULL, NULL),
(194, 'index', 'Liste des notes de passage de l\'ecole  pour INVITE', 'Passinggrades', 10, NULL, NULL, NULL, NULL),
(195, 'index', 'Liste des methodes de paiement exigees par l\'ecole  pour INVITE', 'Paymentmethod', 10, NULL, NULL, NULL, NULL),
(196, 'report', 'Affichage du bulletin de l\'eleve  pour INVITE', 'Reportcard', 10, NULL, NULL, NULL, NULL),
(197, 'index', 'Liste des heures de cours de l\'eleve  pour INVITE', 'Schedules', 10, NULL, NULL, NULL, NULL),
(198, 'view', 'Affichage des info personnelles eleve', 'StudentOtherInfo', 10, NULL, NULL, NULL, NULL),
(200, 'index', 'Liste des matieres enseignees,  pour INVITE', 'Subjects', 10, NULL, NULL, NULL, NULL),
(201, 'viewcontact', 'Vue des contacts de l\'utilisateur', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(202, 'updateMyContacts', 'Mise a jour de contacts par l\'utilisateur', 'ContactInfo', 9, NULL, NULL, NULL, NULL),
(203, 'viewForUpdate', 'Modifier info personnelle utilisateur', 'Persons', 9, NULL, NULL, NULL, NULL),
(204, 'updateMyInfo', 'Mise a jour des infos personnelles utilisateur systeme', 'Persons', 9, NULL, NULL, NULL, NULL),
(205, 'updateParent', 'Mise a jour des infos personnelles d\'un parent', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(206, 'viewForUpdate', 'Mise a jour des infos personnelles d\'un eleve ....', 'Persons', 10, NULL, NULL, NULL, NULL),
(207, 'updateMyInfo', 'Mise a jour des infos personnelles utilisateur eleve', 'Persons', 10, NULL, NULL, NULL, NULL),
(208, 'index', 'Lister emails', 'Mails', 9, NULL, NULL, NULL, NULL),
(209, 'sendEmail', 'Envoyer Email', 'Persons', 9, NULL, NULL, NULL, NULL),
(210, 'index', 'Affiche le calendrier', 'Calendar', 7, NULL, NULL, NULL, NULL),
(211, 'create', 'Ajouter évenements', 'Calendar', 7, NULL, NULL, NULL, NULL),
(212, 'update', 'Modifier un évenement', 'Calendar', 7, NULL, NULL, NULL, NULL),
(213, 'delete', 'Supprimer un évenement', 'Calendar', 7, NULL, NULL, NULL, NULL),
(214, 'view', 'Voir un évenement en mode admin', 'Calendar', 7, NULL, NULL, NULL, NULL),
(215, 'viewForIndex', 'Voir un évenement en mode utilisateur', 'Calendar', 7, NULL, NULL, NULL, NULL),
(216, 'calendarEvents', 'Evenement du calendrier', 'Calendar', 7, NULL, NULL, NULL, NULL),
(217, 'calendarEvents', 'Evenement du calendrier pour invites', 'Calendar', 10, NULL, NULL, NULL, NULL),
(218, 'viewForIndex', 'afficher les evenements en mod utili', 'Calendar', 10, NULL, NULL, NULL, NULL),
(219, 'viewOnlineUsers', 'Voir les utilisateurs connectés', 'User', 5, NULL, NULL, NULL, NULL),
(220, 'viewDecision', 'Voir la liste de decision finale', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(221, 'classSetup', 'Formation des classes', 'Persons', 9, NULL, NULL, NULL, NULL),
(222, 'create', 'Ajouter une nouvelle configuration payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(223, 'update', 'Modifier  configuration payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(224, 'index', 'Affichage des configurations payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(225, 'view', 'Voir configuration payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(226, 'paverage', 'Palmares des moyennes', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(227, 'admission', 'Admission des postulants', 'Persons', 9, NULL, NULL, NULL, NULL),
(228, 'viewListAdmission', 'Voir la liste d\'inscription', 'Postulant', 9, NULL, NULL, NULL, NULL),
(229, 'levelRoomAffectation', 'Affectation a un niveau et/ou a une salle', 'Persons', 9, NULL, NULL, NULL, NULL),
(230, 'viewAdmissionDetail', 'Afficher toutes les info d\'un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(231, 'index', 'Liste des devoirs soumis par les professeurs', 'Homework', 9, NULL, NULL, NULL, NULL),
(232, 'create', 'Ajouter un devoir', 'Homework', 9, NULL, NULL, NULL, NULL),
(233, 'update', 'Modifier un devoir soumis par les professeurs', 'Homework', 9, NULL, NULL, NULL, NULL),
(234, 'view', '	Afficher les info d\'un devoir soumis par un professeur', 'Homework', 9, NULL, NULL, NULL, NULL),
(235, 'index', 'Lister les devoirs soumis par des professeurs', 'Homework', 10, NULL, NULL, NULL, NULL),
(236, 'view', 'Afficher les devoirs soumis par des professeurs', 'Homework', 10, NULL, NULL, NULL, NULL),
(237, 'index', 'Lister les devoirs soumis par des professeurs', 'homeworkSubmission', 10, NULL, NULL, NULL, NULL),
(238, 'create', 'Soumettre un devoir', 'homeworkSubmission', 10, NULL, NULL, NULL, NULL),
(239, 'generalReport', 'Affichage d\'un rapport general pour INVITE', 'Reportcard', 10, NULL, NULL, NULL, NULL),
(240, 'index', 'Liste type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(241, 'create', 'Créer type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(242, 'view', 'Voir type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(243, 'update', 'Modifier type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(244, 'delete', 'Supprimer type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(245, 'index', 'Liste des enregistrements d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(246, 'create', 'Créer enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(247, 'update', 'Modifier enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(248, 'delete', 'Supprimer enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(249, 'view', 'Voir enregistrement d\'infraction', 'RecordInfraction', 11, NULL, NULL, NULL, NULL),
(250, 'index', 'Lister enregistrement présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(251, 'view', 'Voir une présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(252, 'update', 'Modifier une présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(253, 'recordPresence', 'Enregistrer présence élèves par salle', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(254, 'admin', 'Rapport présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(255, 'create', 'Prendre présence pour un élève', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(256, 'delete', 'Supprimer Présence', 'RecordPresence', 11, NULL, NULL, NULL, NULL),
(257, 'index', 'Liste des avances sur salaire effectues', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(258, 'disableStudents', 'Rendre des élèves inactifs', 'Persons', 9, NULL, NULL, NULL, NULL),
(259, 'create', 'Ajouter des avances sur salaire', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(260, 'update', 'Modifier avance sur salaire', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(261, 'index', 'Liste des devoirs soumis par les élèves', 'homeworkSubmission', 9, NULL, NULL, NULL, NULL),
(262, 'index', 'Liste des payroll effectues', 'Payroll', 8, NULL, NULL, NULL, NULL),
(263, 'create', 'Ajouter un payroll', 'Payroll', 8, NULL, NULL, NULL, NULL),
(264, 'update', 'Modifier un payroll', 'Payroll', 8, NULL, NULL, NULL, NULL),
(265, 'delete', 'Supprimer un payroll', 'Payroll', 8, NULL, NULL, NULL, NULL),
(266, 'view', 'Afficher payroll en mode vue', 'Payroll', 8, NULL, NULL, NULL, NULL),
(267, 'index', 'Liste des obligations a payer', 'Taxes', 8, NULL, NULL, NULL, NULL),
(268, 'create', 'Ajouter une obligation', 'Taxes', 8, NULL, NULL, NULL, NULL),
(269, 'update', 'Modifier une obligation', 'Taxes', 8, NULL, NULL, NULL, NULL),
(270, 'delete', 'Supprimer une obligation', 'Taxes', 8, NULL, NULL, NULL, NULL),
(271, 'view', 'Afficher une obligation en mode vue', 'Taxes', 8, NULL, NULL, NULL, NULL),
(272, 'index', 'Liste de description des autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(273, 'create', 'Ajouter description autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(274, 'update', 'Modifier description autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(275, 'delete', 'Supprimer description autres rentrées', 'OtherIncomesDescription', 8, NULL, NULL, NULL, NULL),
(276, 'index', 'Liste des autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(277, 'create', 'Ajouter autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(278, 'update', 'Modifier autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(279, 'delete', 'Supprimer autres rentrées', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(280, 'view', 'Afficher autres rentrées en mode vue', 'OtherIncomes', 8, NULL, NULL, NULL, NULL),
(281, 'view', 'Afficher un pret en mode vue', 'LoanOfMoney', 8, NULL, NULL, NULL, NULL),
(282, 'admissionSearch', 'Recherche d\'un postulant dans la liste des anciens eleves ', 'Persons', 9, NULL, NULL, NULL, NULL),
(283, 'view', 'Afficher un devoir soumis (INVITE)', 'homeworkSubmission', 10, NULL, NULL, NULL, NULL),
(284, 'index', 'Lister les articles du portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(285, 'create', 'Ajouter un article au portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(286, 'update', 'Modifier un article du portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(287, 'delete', 'Supprimer un article du portail', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(288, 'view', 'Voir discipline pour un élève', 'RecordInfraction', 10, NULL, NULL, NULL, NULL),
(289, 'viewParent', 'Vue parent de la discipline', 'RecordInfraction', 10, NULL, NULL, NULL, NULL),
(290, 'config', 'Configuration ', 'Billings', 8, NULL, NULL, NULL, NULL),
(291, 'disciplineReport', 'Rapport sur la conduite de l\'eleve', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(292, 'index', 'Liste des partenaires', 'Partners', 1, NULL, NULL, NULL, NULL),
(293, 'create', 'Ajouter un nouveau partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(294, 'update', 'Modifier un partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(295, 'delete', 'Supprimer un partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(296, 'view', 'Voir plus de details sur un partenaire', 'Partners', 1, NULL, NULL, NULL, NULL),
(297, 'index', 'Liste des boursiers', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(298, 'create', 'Ajouter un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(299, 'update', 'Modifier un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(300, 'delete', 'Supprimer un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(301, 'view', 'Voir plus de details sur un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(302, 'index', 'Liste des libelles depenses', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(303, 'create', 'Ajouter libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(304, 'update', 'Modifier libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(305, 'delete', 'Supprimer libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(306, 'index', 'Liste des depenses', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(307, 'create', 'Ajouter une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(308, 'update', 'Modifier une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(309, 'delete', 'Supprimer une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(310, 'view', 'Voir plus de details d\'une depense', 'ChargePaid', 8, NULL, NULL, NULL, NULL),
(311, 'view', 'Voir plus de details d\'un libelle depense', 'ChargeDescription', 8, NULL, NULL, NULL, NULL),
(312, 'delete', 'Supprimer un parametre payroll', 'PayrollSettings', 8, NULL, NULL, NULL, NULL),
(313, 'receipt', 'Produire fiche de paie', 'Payroll', 8, NULL, NULL, NULL, NULL),
(314, 'paymentReceipt', 'Recu de paiement', 'Billings', 8, NULL, NULL, NULL, NULL),
(315, 'create', 'Ajouter libelle frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(316, 'update', 'Modifier libelle frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(317, 'delete', 'Supprimer libelle frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(318, 'index', 'Liste des libelles frais', 'FeesLabel', 8, NULL, NULL, NULL, NULL),
(319, 'uploadLogo', 'Upload logo', 'Generalconfig', 1, NULL, NULL, NULL, NULL),
(320, 'etatF', 'Voir les etats financiers', 'Reports', 8, NULL, NULL, NULL, NULL),
(321, 'taxreport', 'Rapport taxe', 'Reports', 8, NULL, NULL, NULL, NULL),
(322, 'create', 'Ajouter une nouvelle vente', 'Sellings', 8, NULL, NULL, NULL, NULL),
(323, 'return', 'Liste des produits retournés', 'Sellings', 8, NULL, NULL, NULL, NULL),
(324, 'returnitem', 'Retourner un produit', 'Sellings', 8, NULL, NULL, NULL, NULL),
(325, 'admin', 'Afficher le rapport de vente', 'Sellings', 8, NULL, NULL, NULL, NULL),
(326, 'emptyCart', 'Vider le panier de vente', 'Sellings', 8, NULL, NULL, NULL, NULL),
(327, 'index', 'Liste des produits en dépot', 'Products', 8, NULL, NULL, NULL, NULL),
(328, 'create', 'Ajouter un produit', 'Stocks', 8, NULL, NULL, NULL, NULL),
(329, 'update', 'Modifier produit en dépôt', 'Products', 8, NULL, NULL, NULL, NULL),
(330, 'delete', 'Supprimer un produit en dépôt', 'Products', 8, NULL, NULL, NULL, NULL),
(331, 'create', 'Ajouter un nouveau produit en dépôt', 'Products', 8, NULL, NULL, NULL, NULL),
(332, 'update', 'Modifier le dépôt', 'Stocks', 8, NULL, NULL, NULL, NULL),
(333, 'uploadLogo', 'Gérer carrousel portal', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(334, 'addCourse', 'Ajouter cours dans l\'horaire', 'Schedules', 7, NULL, NULL, NULL, NULL),
(335, 'schedulesAgenda', 'Liste des heures de cours pour l\'agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(336, 'viewForAgenda', 'Voir plus de details sur un cours en mode agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(337, 'index', 'Vue globale des heures de cours d\'une salle par semaine', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(338, 'addCourse', 'Ajouter cours dans l\'agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(339, 'create', 'Ajouter/Modifier cours dans l\'agenda', 'ScheduleAgenda', 7, NULL, NULL, NULL, NULL),
(340, 'schedulesAgenda', 'Liste des heures de cours pour l\'agenda', 'Persons', 9, NULL, NULL, NULL, NULL),
(341, 'index', 'Vue globale des heures de cours d\'une salle/semaine pour INVITE', 'ScheduleAgenda', 10, NULL, NULL, NULL, NULL),
(342, 'schedulesAgenda', 'Liste des heures de cours pour l\'agenda', 'ScheduleAgenda', 10, NULL, NULL, NULL, NULL),
(343, 'viewForAgenda', 'Voir plus de details sur un cours en mode agenda', 'ScheduleAgenda', 10, NULL, NULL, NULL, NULL),
(344, 'updateArticle', 'Modifier article inline', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(345, 'admin', 'Liste de menu du portail', 'CmsMenu', 12, NULL, NULL, NULL, NULL),
(346, 'updateMenu', 'Modifier menu inline', 'CmsMenu', 12, NULL, NULL, NULL, NULL),
(347, 'index', 'liste des documents charges', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(348, 'create', 'Charger un nouveau document', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(349, 'update', 'Modifier un document charge', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(350, 'delete', 'Supprimer un document charge', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(351, 'create', 'Ajouter des observations pour les bulletins', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(352, 'updateObservation', 'Modifier observation inline', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(353, 'gridcreateCourse', 'Ajouter plusieurs cours par salle', 'Courses', 7, NULL, NULL, NULL, NULL),
(354, 'view', 'Voir les info d\'un document qui a ete charge', 'CmsDoc', 12, NULL, NULL, NULL, NULL),
(355, 'massAddingFees', 'Ajouter plusieurs frais par classe', 'Fees', 1, NULL, NULL, NULL, NULL),
(356, 'massAddingInfractionType', 'Ajouter plusieurs type d\'infraction', 'InfractionType', 11, NULL, NULL, NULL, NULL),
(357, 'massAddingSubjects', 'Créer plusieurs matières', 'Subjects', 7, NULL, NULL, NULL, NULL),
(358, 'index', 'Migration élèves', 'Datamigration', 7, NULL, NULL, NULL, NULL),
(359, 'employees', 'Migration employé', 'Datamigration', 7, NULL, NULL, NULL, NULL),
(360, 'fdm', 'Produire fiche de declaration mensuelle', 'Reports', 8, NULL, NULL, NULL, NULL),
(361, 'massAddingScholarship', 'Ajouter les infos pour un boursier', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(362, 'delete', 'Supprimer un article dans le panier', 'Sellings', 8, NULL, NULL, NULL, NULL),
(363, 'index', 'Liste des fourchettes pour le calcul d\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(364, 'create', 'Ajouter les fourchettes permettant de calculer l\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(365, 'update', 'Modifier les fourchettes permettant de calculer l\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(366, 'delete', 'Supprimer les fourchettes permettant de  calculer l\'IRI', 'Bareme', 8, NULL, NULL, NULL, NULL),
(367, 'create', 'Créer champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(368, 'update', 'Modifier champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(369, 'delete', 'Supprimer champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(370, 'index', 'Lister champ personalisable', 'CustomField', 1, NULL, NULL, NULL, NULL),
(371, 'deletedoc', 'Supprimer un document de l\'eleve', 'Persons', 9, NULL, NULL, NULL, NULL),
(372, 'index', 'Listes des documents archives', 'Documents', 7, NULL, NULL, NULL, NULL),
(373, 'index', 'Liste des observations pour les bulletins', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(374, 'delete', 'Supprimer une observation du bulletin', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(375, 'update', 'Modifier les observations du bulletin', 'ReportcardObservation', 1, NULL, NULL, NULL, NULL),
(376, 'transcriptNotesSearch', 'Recherche pour releve de notes', 'Persons', 9, NULL, NULL, NULL, NULL),
(377, 'transcriptNotes', 'Imprimer releve de notes', 'Persons', 9, NULL, NULL, NULL, NULL),
(378, 'viewcontact', 'Liste des contacts (vue eleve)', 'ContactInfo', 10, NULL, NULL, NULL, NULL),
(379, 'viewApprovePostulant', 'Voir la liste des postulants acceptes', 'Postulant', 9, NULL, NULL, NULL, NULL),
(380, 'decisionAdmission', 'Afficher la liste des postulants pour lesquels on va decider', 'Postulant', 9, NULL, NULL, NULL, NULL),
(381, 'create', 'Ajouter un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(382, 'update', 'Modifier les infos d\'un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(383, 'delete', 'Supprimer un postulant', 'Postulant', 9, NULL, NULL, NULL, NULL),
(384, 'create', 'Encaisser un frais d\'inscription', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(385, 'update', 'Modifier un encaissement de frais d\'inscription', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(386, 'delete', 'Supprimer un encaissement d\'un frais d\'inscription', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(387, 'index', 'Liste paiement des postulants', 'EnrollmentIncome', 8, NULL, NULL, NULL, NULL),
(388, 'exempt', 'Dispenser quelqu\'un de sa dette', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(389, 'index_exempt', 'Liste des dettes annulées', 'Scholarshipholder', 8, NULL, NULL, NULL, NULL),
(390, 'index', 'Afficher la liste des examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(391, 'create', 'Ajouter les examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(392, 'update', 'Modifier examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(393, 'delete', 'Supprimer un examen du MENFP', 'ExamenMenfp', 9, NULL, NULL, NULL, NULL),
(394, 'index', 'Afficher la liste des participants au examen du MENFP', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(395, 'create', 'Ajouter ou Modifier les notes(MENFP) d\'un participant', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(396, 'delete', 'Supprimer une note(MENFP) d\'un participant', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(397, 'view', 'Voir notes et decision d\'un participant au MENFP', 'MenfpGrades', 9, NULL, NULL, NULL, NULL),
(398, 'yearMigrationCheck', 'Migration des donnés de l\'année précédente', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(399, 'periodsSummary', 'Sommaire des periodes', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(400, 'index', 'Afficher la liste des reservations', 'Reservation', 8, NULL, NULL, NULL, NULL),
(401, 'create', 'Ajouter une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(402, 'update', 'Modifier une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(403, 'delete', 'Supprimer une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(404, 'view', 'Afficher une reservation', 'Reservation', 8, NULL, NULL, NULL, NULL),
(405, 'updateEndYearDecision', 'Modifier la décision finale pour une salle', 'Reportcard', 6, NULL, NULL, NULL, NULL),
(406, 'palmares', 'palmares', 'Palmares', 9, NULL, NULL, NULL, NULL),
(407, 'index', 'accueil palmares', 'Palmares', 9, NULL, NULL, NULL, NULL),
(408, 'carrousel', 'Gérer caroussel', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(409, 'lisimaj', 'Liste des images du caroussel', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(410, 'updateCarrousel', 'Mise à jour label image caroussel in line', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(411, 'deleteCaroussel', 'Supprimer image caroussel', 'CmsArticle', 9, NULL, NULL, NULL, NULL),
(412, 'updategrade', 'Modifier les notes depuis le palmares', 'Palmares', 9, NULL, NULL, NULL, NULL),
(413, 'deleteCarrousel', 'Supprimer image carrousel', 'CmsArticle', 12, NULL, NULL, NULL, NULL),
(414, 'validation', 'Valider dans le nouveau palmares', 'Palmares', 9, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `announcements`
--

CREATE TABLE `announcements` (
  `id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `description` text NOT NULL,
  `create_by` varchar(128) NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `arrondissements`
--

CREATE TABLE `arrondissements` (
  `id` int(11) NOT NULL,
  `arrondissement_name` varchar(45) NOT NULL,
  `departement` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `arrondissements`
--

INSERT INTO `arrondissements` (`id`, `arrondissement_name`, `departement`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Port-au-Prince', 2, NULL, NULL, '', ''),
(2, 'Borgne', 1, NULL, NULL, NULL, NULL),
(3, 'Cap-Haitien', 1, NULL, NULL, NULL, NULL),
(4, 'Grande-Rivière Du Nord', 1, NULL, NULL, NULL, NULL),
(5, 'L\'Acul Du Nord', 1, NULL, NULL, NULL, NULL),
(6, 'Limbé', 1, NULL, NULL, NULL, NULL),
(7, 'Plaisance', 1, NULL, NULL, NULL, NULL),
(8, 'Saint-Raphael', 1, NULL, NULL, NULL, NULL),
(9, 'Arcahaie', 2, NULL, NULL, NULL, NULL),
(10, 'Croix-Des-Bouquets', 2, NULL, NULL, NULL, NULL),
(11, 'La Gonâve', 2, NULL, NULL, NULL, NULL),
(12, 'Léogâne', 2, NULL, NULL, NULL, NULL),
(13, 'Dessalines', 3, NULL, NULL, NULL, NULL),
(14, 'Gonaïves', 3, NULL, NULL, NULL, NULL),
(15, 'Gros-Morne', 3, NULL, NULL, NULL, NULL),
(16, 'Marmelade', 3, NULL, NULL, NULL, NULL),
(17, 'Saint-Marc', 3, NULL, NULL, NULL, NULL),
(18, 'Cerca-La-Source', 4, NULL, NULL, NULL, NULL),
(19, 'Hinche', 4, NULL, NULL, NULL, NULL),
(20, 'Lascahobas', 4, NULL, NULL, NULL, NULL),
(21, 'Mirebalais', 4, NULL, NULL, NULL, NULL),
(22, 'Anse-D\'Hainault', 5, NULL, NULL, NULL, NULL),
(23, 'Corail', 5, NULL, NULL, NULL, NULL),
(24, 'Jérémie', 5, NULL, NULL, NULL, NULL),
(25, 'Anse-A-Veau', 6, NULL, NULL, NULL, NULL),
(26, 'Baradères', 6, NULL, NULL, NULL, NULL),
(27, 'Miragoâne', 6, NULL, NULL, NULL, NULL),
(28, 'Fort-Liberté', 7, NULL, NULL, NULL, NULL),
(29, 'Ouanaminthe', 7, NULL, NULL, NULL, NULL),
(30, 'Trou-Du-Nord', 7, NULL, NULL, NULL, NULL),
(31, 'Vallières', 7, NULL, NULL, NULL, NULL),
(32, 'Môle Saint-Nicolas', 8, NULL, NULL, NULL, NULL),
(33, 'Port-De-Paix', 8, NULL, NULL, NULL, NULL),
(34, 'Saint-Louis Du Nord', 8, NULL, NULL, NULL, NULL),
(35, 'Aquin', 9, NULL, NULL, NULL, NULL),
(36, 'Chardonnières', 9, NULL, NULL, NULL, NULL),
(37, 'Côteaux', 9, NULL, NULL, NULL, NULL),
(38, 'Les Cayes', 9, NULL, NULL, NULL, NULL),
(39, 'Port-Salut', 9, NULL, NULL, NULL, NULL),
(40, 'Bainet', 10, NULL, NULL, NULL, NULL),
(41, 'Belle-Anse', 10, NULL, NULL, NULL, NULL),
(42, 'Jacmel', 10, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `average_by_period`
--

CREATE TABLE `average_by_period` (
  `academic_year` int(11) NOT NULL,
  `evaluation_by_year` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `sum` double NOT NULL,
  `average` double NOT NULL,
  `place` int(11) NOT NULL,
  `reportcard_ref` varchar(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `balance`
--

CREATE TABLE `balance` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `balance` double NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `bareme`
--

CREATE TABLE `bareme` (
  `id` int(11) NOT NULL,
  `min_value` double NOT NULL,
  `max_value` double NOT NULL,
  `percentage` double NOT NULL,
  `compteur` int(11) NOT NULL,
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old bareme; 1: new bareme in use',
  `date_created` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `bareme`
--

INSERT INTO `bareme` (`id`, `min_value`, `max_value`, `percentage`, `compteur`, `old_new`, `date_created`, `created_by`) VALUES
(1, 0, 60000, 0, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(2, 60000, 240000, 10, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(3, 240000, 480000, 15, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(4, 480000, 1000000, 25, 1, 1, '2016-09-29 18:09:17', '_developer_'),
(5, 1000000, 1100000, 30, 1, 1, '2016-09-29 18:09:17', '_developer_');

-- --------------------------------------------------------

--
-- Table structure for table `billings`
--

CREATE TABLE `billings` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `fee_period` int(11) NOT NULL,
  `amount_to_pay` float NOT NULL,
  `amount_pay` float NOT NULL,
  `balance` float DEFAULT NULL,
  `reservation_id` int(11) DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `date_pay` date NOT NULL,
  `payment_method` int(11) DEFAULT NULL,
  `comments` varchar(255) DEFAULT NULL,
  `fee_totally_paid` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0: paiement partiel; 1: paiement total ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `created_by` varchar(64) DEFAULT NULL,
  `updated_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `charge_description`
--

CREATE TABLE `charge_description` (
  `id` int(11) NOT NULL,
  `description` varchar(65) NOT NULL,
  `category` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `charge_paid`
--

CREATE TABLE `charge_paid` (
  `id` int(11) NOT NULL,
  `id_charge_description` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_date` date NOT NULL,
  `comment` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `created_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `id` int(11) NOT NULL,
  `city_name` varchar(45) NOT NULL,
  `arrondissement` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`id`, `city_name`, `arrondissement`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Carrefour', 1, NULL, NULL, NULL, NULL),
(2, 'Cite-Soleil', 1, NULL, NULL, NULL, NULL),
(3, 'Delmas', 1, NULL, NULL, NULL, NULL),
(4, 'Tabarre', 1, NULL, NULL, NULL, NULL),
(5, 'Gressier', 1, NULL, NULL, NULL, NULL),
(6, 'Kenscoff', 1, NULL, NULL, NULL, NULL),
(7, 'Petion-Ville', 1, NULL, NULL, NULL, NULL),
(8, 'Port-Au-Prince', 1, NULL, NULL, NULL, NULL),
(9, 'Borgne', 2, NULL, NULL, NULL, NULL),
(10, 'Port-Margot', 2, NULL, NULL, NULL, NULL),
(11, 'Cap-Haitien', 3, NULL, NULL, NULL, NULL),
(12, 'Limonade', 3, NULL, NULL, NULL, NULL),
(13, 'Quartier-Morin', 3, NULL, NULL, NULL, NULL),
(14, 'Bahon', 4, NULL, NULL, NULL, NULL),
(15, 'Grande-Riviere Du Nord', 4, NULL, NULL, NULL, NULL),
(16, 'Acul-Du-Nord', 5, NULL, NULL, NULL, NULL),
(17, 'Milot', 5, NULL, NULL, NULL, NULL),
(18, 'Plaine-Du-Nord', 5, NULL, NULL, NULL, NULL),
(19, 'Bas-Limbe', 6, NULL, NULL, NULL, NULL),
(20, 'Limbe', 6, NULL, NULL, NULL, NULL),
(21, 'Pilate', 7, NULL, NULL, NULL, NULL),
(22, 'Plaisance', 7, NULL, NULL, NULL, NULL),
(23, 'La Victoire', 8, NULL, NULL, NULL, NULL),
(24, 'Pignon', 8, NULL, NULL, NULL, NULL),
(25, 'Ranquitte', 8, NULL, NULL, NULL, NULL),
(26, 'Dondon', 8, NULL, NULL, NULL, NULL),
(27, 'Saint-Raphael', 8, NULL, NULL, NULL, NULL),
(28, 'Arcahaie', 9, NULL, NULL, NULL, NULL),
(29, 'Cabaret', 9, NULL, NULL, NULL, NULL),
(30, 'Cornillon', 10, NULL, NULL, NULL, NULL),
(31, 'Croix Des Bouquets', 10, NULL, NULL, NULL, NULL),
(32, 'Thomazeau', 10, NULL, NULL, NULL, NULL),
(33, 'Fonds-Verrettes', 10, NULL, NULL, NULL, NULL),
(34, 'Ganthier', 10, NULL, NULL, NULL, NULL),
(35, 'Anse-A-Galets', 11, NULL, NULL, NULL, NULL),
(36, 'Pointe-A-Raquette', 11, NULL, NULL, NULL, NULL),
(37, 'Grand-Goave', 12, NULL, NULL, NULL, NULL),
(38, 'Leogane', 12, NULL, NULL, NULL, NULL),
(39, 'Petit-Goave', 12, NULL, NULL, NULL, NULL),
(40, 'Desdunes', 13, NULL, NULL, NULL, NULL),
(41, 'Dessalines', 13, NULL, NULL, NULL, NULL),
(42, 'Grande-Saline', 13, NULL, NULL, NULL, NULL),
(43, 'Petite-Riviere De L\'Artibonite', 13, NULL, NULL, NULL, NULL),
(44, 'Ennery', 14, NULL, NULL, NULL, NULL),
(45, 'Gonaives', 14, NULL, NULL, NULL, NULL),
(46, 'L\'Estere', 14, NULL, NULL, NULL, NULL),
(47, 'Gros-Morne', 15, NULL, NULL, NULL, NULL),
(48, 'Anse-Rouge', 15, NULL, NULL, NULL, NULL),
(49, 'Terre-Neuve', 15, NULL, NULL, NULL, NULL),
(50, 'Marmelade', 16, NULL, NULL, NULL, NULL),
(51, 'Saint-Michel De L\'Attalaye', 16, NULL, NULL, NULL, NULL),
(52, 'La Chapelle', 17, NULL, NULL, NULL, NULL),
(53, 'Saint-Marc', 17, NULL, NULL, NULL, NULL),
(54, 'Verrettes', 17, NULL, NULL, NULL, NULL),
(55, 'Cerca-La-Source', 18, NULL, NULL, NULL, NULL),
(56, 'Thomassique', 18, NULL, NULL, NULL, NULL),
(57, 'Cerca-Carvajal', 19, NULL, NULL, NULL, NULL),
(58, 'Hinche', 19, NULL, NULL, NULL, NULL),
(59, 'Maissade', 19, NULL, NULL, NULL, NULL),
(60, 'Thomonde', 19, NULL, NULL, NULL, NULL),
(61, 'Belladere', 20, NULL, NULL, NULL, NULL),
(62, 'Lascahobas', 20, NULL, NULL, NULL, NULL),
(63, 'Savanette', 20, NULL, NULL, NULL, NULL),
(64, 'Boucan-Carre', 21, NULL, NULL, NULL, NULL),
(65, 'Mirebalais', 21, NULL, NULL, NULL, NULL),
(66, 'Saut-D\'Eau', 21, NULL, NULL, NULL, NULL),
(67, 'Anse D\'Hainault', 22, NULL, NULL, NULL, NULL),
(68, 'Les Irois', 22, NULL, NULL, NULL, NULL),
(69, 'Dame-Marie', 22, NULL, NULL, NULL, NULL),
(70, 'Corail', 23, NULL, NULL, NULL, NULL),
(71, 'Roseaux', 23, NULL, NULL, NULL, NULL),
(72, 'Beaumont', 23, NULL, NULL, NULL, NULL),
(73, 'Pestel', 23, NULL, NULL, NULL, NULL),
(74, 'Abricots', 24, NULL, NULL, NULL, NULL),
(75, 'Bonbon', 24, NULL, NULL, NULL, NULL),
(76, 'Jeremie', 24, NULL, NULL, NULL, NULL),
(77, 'Chambellan', 24, NULL, NULL, NULL, NULL),
(78, 'Moron', 24, NULL, NULL, NULL, NULL),
(79, 'Anse-A-Veau', 25, NULL, NULL, NULL, NULL),
(80, 'Arnaud', 25, NULL, NULL, NULL, NULL),
(81, 'L\'Asile', 25, NULL, NULL, NULL, NULL),
(82, 'Petit-Trou De Nippes', 25, NULL, NULL, NULL, NULL),
(83, 'Plaisance Du Sud', 25, NULL, NULL, NULL, NULL),
(84, 'Baraderes', 26, NULL, NULL, NULL, NULL),
(85, 'Grand-Boucan', 26, NULL, NULL, NULL, NULL),
(86, 'Fonds-Des-Negres', 27, NULL, NULL, NULL, NULL),
(87, 'Miragoane', 27, NULL, NULL, NULL, NULL),
(88, 'Paillant', 27, NULL, NULL, NULL, NULL),
(89, 'Petite Riviere De Nippes', 27, NULL, NULL, NULL, NULL),
(90, 'Ferrier', 28, NULL, NULL, NULL, NULL),
(91, 'Fort-Liberte', 28, NULL, NULL, NULL, NULL),
(92, 'Perches', 28, NULL, NULL, NULL, NULL),
(93, 'Capotille', 29, NULL, NULL, NULL, NULL),
(94, 'Mont-Organise', 29, NULL, NULL, NULL, NULL),
(95, 'Ouanaminthe', 29, NULL, NULL, NULL, NULL),
(96, 'Sainte-Suzanne', 30, NULL, NULL, NULL, NULL),
(97, 'Terrier-Rouge', 30, NULL, NULL, NULL, NULL),
(98, 'Caracol', 30, NULL, NULL, NULL, NULL),
(99, 'Trou-Du-Nord', 30, NULL, NULL, NULL, NULL),
(100, 'Carice', 31, NULL, NULL, NULL, NULL),
(101, 'Mombin Crochu', 31, NULL, NULL, NULL, NULL),
(102, 'Vallieres', 31, NULL, NULL, NULL, NULL),
(103, 'Baie-De-Henne', 32, NULL, NULL, NULL, NULL),
(104, 'Bombardopolis', 32, NULL, NULL, NULL, NULL),
(105, 'Jean Rabel', 32, NULL, NULL, NULL, NULL),
(106, 'Mole Saint-Nicolas', 32, NULL, NULL, NULL, NULL),
(107, 'Bassin Bleu', 33, NULL, NULL, NULL, NULL),
(108, 'Chansolme', 33, NULL, NULL, NULL, NULL),
(109, 'La Tortue', 33, NULL, NULL, NULL, NULL),
(110, 'Port-De-Paix', 33, NULL, NULL, NULL, NULL),
(111, 'Anse-A-Foleur', 34, NULL, NULL, NULL, NULL),
(112, 'Saint-Louis Du Nord', 34, NULL, NULL, NULL, NULL),
(113, 'Aquin', 35, NULL, NULL, NULL, NULL),
(114, 'Cavaillon', 35, NULL, NULL, NULL, NULL),
(115, 'Saint-Louis Du Sud', 35, NULL, NULL, NULL, NULL),
(116, 'Chardonnieres', 36, NULL, NULL, NULL, NULL),
(117, 'Les Anglais', 36, NULL, NULL, NULL, NULL),
(118, 'Tiburon', 36, NULL, NULL, NULL, NULL),
(119, 'Coteaux', 37, NULL, NULL, NULL, NULL),
(120, 'Port-A-Piment', 37, NULL, NULL, NULL, NULL),
(121, 'Roche-A-Bateau', 37, NULL, NULL, NULL, NULL),
(122, 'Camp-Perrin', 38, NULL, NULL, NULL, NULL),
(123, 'Maniche', 38, NULL, NULL, NULL, NULL),
(124, 'Cayes', 38, NULL, NULL, NULL, NULL),
(125, 'Ile-A-Vache', 38, NULL, NULL, NULL, NULL),
(126, 'Chantal', 38, NULL, NULL, NULL, NULL),
(127, 'Torbeck', 38, NULL, NULL, NULL, NULL),
(128, 'Port-Salut', 39, NULL, NULL, NULL, NULL),
(129, 'Arniquet', 39, NULL, NULL, NULL, NULL),
(130, 'Saint-Jean Du Sud', 39, NULL, NULL, NULL, NULL),
(131, 'Bainet', 40, NULL, NULL, NULL, NULL),
(132, 'Cotes De Fer', 40, NULL, NULL, NULL, NULL),
(133, 'Anse-A-Pitre', 41, NULL, NULL, NULL, NULL),
(134, 'Belle-Anse', 41, NULL, NULL, NULL, NULL),
(135, 'Grand-Gosier', 41, NULL, NULL, NULL, NULL),
(136, 'Thiotte', 41, NULL, NULL, NULL, NULL),
(137, 'Jacmel', 42, NULL, NULL, NULL, NULL),
(138, 'La Vallee De Jacmel', 42, NULL, NULL, NULL, NULL),
(139, 'Cayes-Jacmel', 42, NULL, NULL, NULL, NULL),
(140, 'Marigot', 42, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_article`
--

CREATE TABLE `cms_article` (
  `id` int(11) NOT NULL,
  `article_title` varchar(255) NOT NULL,
  `article_description` longtext NOT NULL,
  `article_menu` int(11) DEFAULT NULL,
  `rank_article` tinyint(1) DEFAULT '0',
  `date_create` timestamp NULL DEFAULT NULL,
  `create_by` varchar(128) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  `section` int(11) DEFAULT NULL,
  `last_update` date DEFAULT NULL,
  `set_position` varchar(64) DEFAULT NULL COMMENT 'box or main'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_doc`
--

CREATE TABLE `cms_doc` (
  `id` int(11) NOT NULL,
  `document_name` varchar(128) NOT NULL,
  `document_title` varchar(128) NOT NULL,
  `document_description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_image`
--

CREATE TABLE `cms_image` (
  `id` int(11) NOT NULL,
  `label_image` varchar(255) NOT NULL,
  `type_image` varchar(64) NOT NULL COMMENT 'Carrousel or Logo',
  `nom_image` varchar(255) NOT NULL,
  `is_publish` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `cms_menu`
--

CREATE TABLE `cms_menu` (
  `id` int(11) NOT NULL,
  `menu_label` varchar(64) NOT NULL,
  `menu_position` int(11) NOT NULL,
  `is_home` tinyint(1) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cms_menu`
--

INSERT INTO `cms_menu` (`id`, `menu_label`, `menu_position`, `is_home`, `is_publish`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'Qui sommes nous ?', 1, NULL, 1, NULL, '2016-05-09 23:48:05', NULL, 'admin'),
(2, 'Actualités', 0, NULL, 1, NULL, '2016-05-09 23:48:01', NULL, 'admin'),
(3, 'Admission', 2, NULL, 1, NULL, '2016-05-09 23:28:06', NULL, 'admin'),
(4, 'Info Pratique', 3, NULL, 0, NULL, '2018-08-04 12:30:43', NULL, 'LOGIPAM'),
(5, 'Accueil', 0, 1, 1, NULL, NULL, NULL, NULL),
(6, 'Menu 1', 4, NULL, 0, NULL, '2018-08-04 12:30:50', 'admin', 'LOGIPAM'),
(7, 'Menu 2', 4, NULL, 0, NULL, NULL, 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_section`
--

CREATE TABLE `cms_section` (
  `id` int(11) NOT NULL,
  `section_name` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `create_by` varchar(128) DEFAULT NULL,
  `is_publish` tinyint(1) DEFAULT NULL,
  `date_create` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contact_info`
--

CREATE TABLE `contact_info` (
  `id` int(11) NOT NULL,
  `person` int(11) NOT NULL,
  `contact_name` varchar(45) DEFAULT NULL,
  `contact_relationship` int(11) DEFAULT NULL,
  `profession` varchar(100) NOT NULL,
  `phone` varchar(64) DEFAULT NULL,
  `address` varchar(255) DEFAULT NULL,
  `email` varchar(64) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `one_more` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `teacher` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `weight` float DEFAULT NULL COMMENT 'Weight : Le coefficient du cours',
  `debase` tinyint(1) NOT NULL DEFAULT '0',
  `optional` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0: not optional; 1: optional(reportcard will not take care about))',
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old ; 1: new, in use',
  `reference_id` int(11) DEFAULT NULL COMMENT 'id kou li ranplase a ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field`
--

CREATE TABLE `custom_field` (
  `id` int(11) NOT NULL,
  `field_name` varchar(64) NOT NULL,
  `field_label` varchar(45) DEFAULT NULL,
  `field_type` varchar(45) DEFAULT 'text',
  `value_type` varchar(16) DEFAULT NULL,
  `field_option` text,
  `field_related_to` varchar(45) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `custom_field_data`
--

CREATE TABLE `custom_field_data` (
  `id` bigint(20) NOT NULL,
  `field_link` int(11) NOT NULL,
  `field_data` text,
  `object_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `Cycles`
--

CREATE TABLE `Cycles` (
  `id` int(11) NOT NULL,
  `cycle_description` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `Cycles`
--

INSERT INTO `Cycles` (`id`, `cycle_description`) VALUES
(1, '1er Cycle'),
(3, '2ème Cycle'),
(4, '3ème Cycle');

-- --------------------------------------------------------

--
-- Table structure for table `decision_finale`
--

CREATE TABLE `decision_finale` (
  `id` bigint(20) NOT NULL,
  `student` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `general_average` float DEFAULT NULL,
  `mention` varchar(45) DEFAULT NULL,
  `report_mention` int(11) DEFAULT NULL,
  `comments` varchar(128) DEFAULT NULL,
  `is_move_to_next_year` tinyint(1) DEFAULT NULL,
  `current_level` int(11) DEFAULT NULL,
  `next_level` int(11) DEFAULT NULL,
  `checked` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: poko pase migrasyon; 1: pase migrasyon deja',
  `create_by` varchar(100) DEFAULT NULL,
  `update_by` varchar(100) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Nord', NULL, NULL, '', ''),
(2, 'Ouest', NULL, NULL, '', ''),
(3, 'Artibonite', NULL, NULL, NULL, NULL),
(4, 'Centre', NULL, NULL, NULL, NULL),
(5, 'Grand\'Anse', NULL, NULL, NULL, NULL),
(6, 'Nippes', NULL, NULL, NULL, NULL),
(7, 'Nord-Est', NULL, NULL, NULL, NULL),
(8, 'Nord-Ouest', NULL, NULL, NULL, NULL),
(9, 'Sud', NULL, NULL, NULL, NULL),
(10, 'Sud-Est', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `department_has_person`
--

CREATE TABLE `department_has_person` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `updated_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `department_in_school`
--

CREATE TABLE `department_in_school` (
  `id` int(11) NOT NULL,
  `department_name` varchar(200) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(45) NOT NULL,
  `updated_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `devises`
--

CREATE TABLE `devises` (
  `id` int(11) NOT NULL,
  `devise_name` varchar(45) NOT NULL,
  `devise_symbol` varchar(45) NOT NULL,
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `devises`
--

INSERT INTO `devises` (`id`, `devise_name`, `devise_symbol`, `description`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'Gourde', 'HTG', '', NULL, NULL, NULL, NULL),
(2, 'Dollar', 'USD', NULL, NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `employee_info`
--

CREATE TABLE `employee_info` (
  `id` int(11) NOT NULL,
  `employee` int(11) NOT NULL,
  `hire_date` date DEFAULT NULL,
  `country_of_birth` varchar(45) NOT NULL,
  `university_or_school` varchar(45) DEFAULT NULL,
  `number_of_year_of_study` int(11) DEFAULT NULL,
  `field_study` int(11) DEFAULT NULL,
  `qualification` int(11) DEFAULT NULL,
  `job_status` int(11) DEFAULT NULL,
  `permis_enseignant` varchar(45) NOT NULL,
  `leaving_date` date DEFAULT NULL,
  `comments` text,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `enrollment_income`
--

CREATE TABLE `enrollment_income` (
  `id` int(11) NOT NULL,
  `postulant` int(11) NOT NULL,
  `apply_level` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `comments` varchar(255) NOT NULL,
  `payment_date` date NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evaluations`
--

CREATE TABLE `evaluations` (
  `id` int(11) NOT NULL,
  `evaluation_name` varchar(64) NOT NULL,
  `weight` float DEFAULT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `evaluation_by_year`
--

CREATE TABLE `evaluation_by_year` (
  `id` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `evaluation_date` date NOT NULL,
  `last_evaluation` tinyint(4) NOT NULL DEFAULT '0',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `examen_menfp`
--

CREATE TABLE `examen_menfp` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `subject` int(11) NOT NULL,
  `weight` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fees`
--

CREATE TABLE `fees` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `fee` int(11) NOT NULL,
  `amount` float NOT NULL,
  `devise` int(11) DEFAULT NULL,
  `date_limit_payment` date DEFAULT NULL,
  `checked` tinyint(1) NOT NULL DEFAULT '0',
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fees_label`
--

CREATE TABLE `fees_label` (
  `id` int(11) NOT NULL,
  `fee_label` varchar(100) NOT NULL,
  `status` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: other fees, no pending; 1: tuition fees, pending'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `field_study`
--

CREATE TABLE `field_study` (
  `id` int(11) NOT NULL,
  `field_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_average_by_period`
--

CREATE TABLE `general_average_by_period` (
  `academic_year` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `general_average` double NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `general_config`
--

CREATE TABLE `general_config` (
  `id` int(11) NOT NULL,
  `item_name` varchar(64) NOT NULL,
  `name` varchar(64) NOT NULL,
  `item_value` text,
  `description` text,
  `english_comment` text,
  `category` varchar(12) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `general_config`
--

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'school_name', 'Nom établissement', 'INCONNU', 'Saisir le nom de l\'établissement', 'Enter the name of the school', 'sys', '0000-00-00 00:00:00', '2017-12-06 00:00:00', '', ''),
(2, 'school_address', 'Adresse établissement', 'AYITI', 'Adresse de l\'école', 'School address', 'sys', '0000-00-00 00:00:00', '2017-11-02 00:00:00', '', ''),
(3, 'school_phone_number', 'Tél établissement', '(509)484 690 / 3601 2959 / 3331 7528', 'Saisir les numéros de téléphone, s\'il y a plusieurs numéros, utiliser un \"/\" pour les séparer.', 'Enter the phone number, if there are multiple phone numbers, use a \"/\" to separated them.', 'sys', NULL, '2017-11-02 00:00:00', NULL, NULL),
(4, 'school_director_name', 'Nom Directeur', 'INCONNU', 'Saisir le nom du Directeur de l\'école.', 'Enter the name of the director of the school.', 'sys', NULL, '2017-11-02 00:00:00', NULL, NULL),
(5, 'school_email_address', 'Email établissement', 'info@logipam.com', 'Saisir l\'adresse email de l\'école.', 'Enter school email address.', 'sys', NULL, '2018-06-14 00:00:00', NULL, NULL),
(6, 'school_site_web', 'Site web établissement', 'www.logipam.com/lcs', 'Saisir URL du site web de l\'école. (Exemple: http:://logipam.com)', 'Enter the URL of the school site web (Example: http:://logipam.com)', 'sys', NULL, '2017-11-02 00:00:00', NULL, NULL),
(7, 'academic_success', 'Message pour réussite', 'Success', 'Message pour un élève qui a réussi.', 'Message for a successful student. ', 'acad', NULL, '2017-11-02 00:00:00', NULL, NULL),
(8, 'a_link', '', 'link', NULL, NULL, NULL, NULL, '2015-08-24 00:00:00', NULL, NULL),
(9, 'ppe_number', '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 'cie_number', 'Numéro Carte Identification École', '', 'Numéro de la carte d\'identité d\'etablissement (CIE)', 'ID Number of the school ID Card.', 'sys', NULL, '2017-11-02 00:00:00', NULL, NULL),
(11, 'school_licence_number', 'Numéro de la licence', '', 'Numéro de licence de l\'établissement.', 'Authorisation license of the school. ', 'sys', NULL, '2017-11-02 00:00:00', NULL, NULL),
(12, 'default_vacation', 'Vacation par défaut', 'Matin', 'Vacation  par defaut.', 'Default Shift.', 'acad', '2015-05-20 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(13, 'default_section', 'Section par défaut', 'Fondamental', 'Section par défaut', 'Default Section', 'acad', '2015-05-20 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(14, 'success_mention', 'Mention pour réussite', 'Succès', 'Message pour la mention de réussite', 'Message for success notification. ', 'acad', '2015-07-01 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(15, 'failure_mention', 'Mention pour échec', 'Echec', 'Message en cas d\'échec d\'un élève.', 'Message for failed student.', 'acad', '2015-07-01 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(16, 'decision_finale_success', 'Message de réussite pour décision de fin d\'année', 'Admis(e) au niveau supérieur/Admis(e) en classe supérieure avec probation/Admis(e) ailleurs', 'Option de message en cas de réussite. Sépare chaque option par un \"/\". ', 'Message option in case of success. Separate each option with a \"/\". ', 'acad', '2015-07-01 00:00:00', '2018-06-29 00:00:00', NULL, NULL),
(17, 'decision_finale_failure', 'Message d\'échec pour décision de fin d\'année', 'A refaire à école/A refaire ailleurs', 'Option de message en cas d\'échec d\'un élève. Sépare chaque option par un /.', 'Message option in case of student failed. Separate each option with a /.', 'acad', '2015-07-01 00:00:00', '2018-06-28 00:00:00', NULL, NULL),
(18, 'code1', 'Code liste de formation classe (MENFP)', '', 'Code fourni par le ministère de l\'éducation nationale d\'Haïti pour l\'école.', 'Education department official code for the school. ', 'sys', '2015-08-01 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(19, 'code2', 'Code à 11 chiffres du MENFP', '131-01200060', 'Code a 11 chiffres fourni par le ministere de l\'éducation nationale d\'Haïti.', 'Education department code with 11 digit for the school.', 'sys', '2015-08-01 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(20, 'methode_deduction_note_discipline', 'Méthode déduction note discipline', '0', '0: Valeur ------- 1 : Pourcentage -------\r\nValeur : les deductions affectent la valeur de la note de discipline -------- Pourcentage : les deductions affectent un pourcentage de la note de discipline', '0: Value ------ 1: Percentage ------\r\nValue: the deduction will affect the value of discipline grade ------ \r\nPercentage : the deduction will affect a percentage of discipline grade. ', 'disc', '2015-08-19 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(21, 'note_discipline_initiale', 'Note discipline initiale', '100', 'Valeur initiale de la note de discipline.  Par exemple : 100 ou 20 ou 50. ', 'Initial value for discipline grade. Example : 100 or 20 or 50', 'disc', '2015-08-19 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(22, 'max_amount_loan', 'Montant maximun d\'un prêt', '1000', 'Le montant maximum que l\'on peut donner comme prêter a un employé.', 'Maximun loan can be granted to an employee. ', 'sys', '2015-09-18 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(23, 'School_acronym', 'Acronyme établissement', '()', 'Acronyme du nom de l\'école. Placer entre parenthèse. ', 'Acronym for school name. Place within parenthesis. ', 'econ', '2015-09-19 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(24, 'include_discipline_grade', 'Inclure note discipline', '0', ' 2: ne pas afficher discipline dans le carnet. ------ 1: oui, on doit inclure la note de discipline dans le calcul de la moyenne ------ 0: non, le calcul de la moyenne ne tient pas compte de la note de discipline.', '2:  do not display discipline in reportcard. ----- 1: include discipline grade in calculation of average. ------ 0: no, the calculation of average don\'t include discipline grade. ', 'disc', NULL, '2017-11-02 00:00:00', NULL, NULL),
(25, 'average_base', 'Moyenne de base', '100', 'La base de la moyenne: 10 ou 100.', 'Base average: 10 or 100.', 'acad', NULL, '2017-11-02 00:00:00', NULL, NULL),
(26, 'reportcard_structure', 'Format bulletin', '1', '1: Simple (une évaluation par période, moyenne générale sur demande);  2: Avancé (Plusieurs évaluations sur UNE période, moyenne générale automatique)', '1: Basic (One evaluation by Period, general average on request); 2: Advanced (Many evaluations in ONE Period, general average automatic)', 'acad', NULL, '2017-11-02 00:00:00', NULL, NULL),
(27, 'include_place', 'Inclure rang élève dans bulletin', '1', '0: le rang de l\'eleve ne figure pas dans le bulletin. 1: le rang de l\'eleve figure dans le bulletin.', '0: student rank not include in the reportcard. 1: student rang include in the reportcard. ', 'acad', NULL, '2017-11-02 00:00:00', NULL, NULL),
(28, 'tardy_absence_display', 'Inclure \"retard\" et \"absence\" dans le bulletin', '0', '1:Figure OU 0:Ne figure pas', '1:Display OR 0:Do not display ', 'disc', '2015-12-18 00:00:00', '2018-06-14 00:00:00', 'SIGES', NULL),
(29, 'total_payroll', 'Nombre de payroll par année', '12', 'Nombre de payroll pour l\'annee', 'Number of payroll per year', 'econ', '2015-12-08 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(30, 'limit_payroll_update', 'Délai modifcation payroll', '', 'Nombre de jours (à partir de la date de paiement) durant lesquels le payroll peut etre modifié.', 'Number of days (from the payment date) the payrolls can be updated. ', 'econ', '2015-12-14 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(32, 'currency_name_symbol', 'Nom et symbole monétaire', 'Gourde/HTG', 'Le nom et le symbol de la devise utilisée par l\'école', 'Currency name and symbol used', 'econ', NULL, '2017-11-02 00:00:00', 'SIGES', NULL),
(33, 'facebook_page', 'Page facebook', 'LOGIPAM', 'Adresse de la page facebook de l\'établissement (Exemple : facebook.com/logipam -> Ecrivez : logipam) ', 'Facebook address of the institution. Example : facebook.com/logipam -> type logipam) ', 'sys', '2016-03-25 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(34, 'twitter_page', 'Compte twitter', 'LOGIPAM', 'Nom d\'utilisateur twitter de l\'établissement.', 'Twitter username of the institution.', 'sys', '2016-03-25 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(35, 'youtube_page', 'Page youtube', 'https://youtube.com', 'Adresse de la Chaine YouTube de l\'institution.', 'Address of the YouTube Channel of the instutition.', 'sys', '2016-03-25 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(36, 'devise_school', 'Devise établissement', '', 'La phrase devise de l\'établissement.', 'The motto phrase of the institution.', 'sys', '2016-03-25 00:00:00', '2017-11-02 00:00:00', NULL, NULL),
(37, 'display_schedule_agenda', 'Schedule/Agenda displaying', '2', '0: ne pas afficher l\'agenda; 1: afficher l\'agenda a la place de l\'horaire; 2: afficher l\'agenda ainsi que l\'horaire', '0: do not display; 1: display without schedules; 2: display with schedules', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(38, 'agenda_duration', 'Subdivision des heures', '3', '1: subdivision par 10; 2: subdivision par 15; 3: subdivision par 30; ', '1: subdivide by 10; 2: subdivide by 15; 3: subdivide by 30; ', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(39, 'number_article_per_page', 'Nombre d\'articles par page', '5', 'Nombre d\'articles s\'affichant sur une page du Portail SIGES.', 'Number of articles viewing in a SIGES Portal page.', 'sys', NULL, '2017-11-02 00:00:00', 'admin', NULL),
(40, 'number_article_archive', 'Nombre d\'articles avant archivage', '7', 'Nombre maximal d\'article à afficher avant que le système les place dans la vue d\'archivage du Portail SIGES.', 'Maximum number of article to show before the system put them in the archive view of SIGES Portal', 'sys', NULL, '2017-11-02 00:00:00', 'admin', NULL),
(41, 'theme_portail_siges', 'Thème Portail SIGES', 'zoranj', 'Saisir le thème du portail de SIGES. Choisir entre siges, seriz, zaboka, zoranj, tamaren', 'Type the theme of SIGES portal. Choose between siges, seriz, zaboka, zoranj, tamaren', 'sys', NULL, '2018-08-04 00:00:00', 'admin', NULL),
(42, 'slogan', 'Autre Devise établissement', 'Vous avez reçu gratuitement, donnez gratuitement.', 'La phrase devise de l\'établissement.', 'The motto phrase of the institution.', 'sys', NULL, '2017-11-02 00:00:00', 'admin', NULL),
(43, 'grid_creation', 'Création en grille', '1', '1: Activer la creation en Grille - 0: Desactiver la création en Grille ', '1: Activate grid creation - 0: Desactivate grid creation', 'sys', NULL, '2017-11-02 00:00:00', 'admin', NULL),
(44, 'nb_grid_line', 'Nombre ligne à afficher', '10', 'Nombre de ligne a afficher sur la grille.', 'Number of line to show on the grid.', 'sys', NULL, '2017-11-02 00:00:00', 'admin', NULL),
(45, 'siges_structure_session', 'Utiliser la version session', '0', '0 :pour utiliser la structure de base de SIGES; 1 :pour utiliser la structure session', '0 :to use the basic structure of SIGES; 1 :to use the session structure ', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(46, 'observation_line', 'Ajouter une ligne d observation', '1', '0 :pour cacher la ligne; 1 :pour montrer la ligne', '0 :to hide the line; 1 :to show the line ', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(47, 'display_student_code', 'Ajouter le code de l eleve dans le bulletin', '1', '0 :pour cacher le code; 1 :pour montrer le code', '0 :to hide the code; 1 :to show the code ', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(48, 'display_administration_signature', 'Ajouter la signature de la directtion', '1', ' :pour cacher la signature de la direction; 1 :pour montrer la signature de la direction', '0 :to hide the administration signature; 1 :to show the administration signature ', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(49, 'display_parent_signature', 'Ajouter la signature du responsable', '1', '0 :pour cacher la signature du responsable; 1 :pour montrer la signature du responsable', '0 :to hide parent signature; 1 :to show parent signature ', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(50, 'administration_signature_text', 'Definir le texte sous la ligne de la signature de la direction', 'Direction', 'Definir le texte sous la ligne de signature de la direction', 'Define the text which is under the administration signature line', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(51, 'parent_signature_text', 'Definir le texte sous la ligne de la signature du responsable', 'Parent', 'Definir le texte sous la ligne de signature du responsable', 'Define the text which is under the parent signature line', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(52, 'display_created_date', 'Ajouter la date de creation du bulletin', '0', '0 :pour cacher la date de creation du bulletin; 1 :pour montrer la date de creation du bulletin', '0 :to hide the reportcard created date; 1 :to show the reportcard created date ', NULL, '2016-10-15 00:00:00', '2017-11-02 00:00:00', 'SIGES', NULL),
(53, 'grades_comment', 'Commentaire note', '0', '0: enlever le champ commentaire des notes 1: ajouter le champ commentaire des notes', '0: hide comment field for grades 1: display comment field for grades', NULL, NULL, '2018-06-29 00:00:00', NULL, NULL),
(54, 'transcript_note_text', 'Texte pour relevé de notes', 'La Direction du {name} certifie par la présente que l\'élève <strong>{name1}</strong> a suivi les cours réguliers d\'enseignement classique en <strong>{name2}</strong>, niveau {name3} durant <strong>l\'année académique {name4}</strong>. Il a obtenu les notes suivantes:', 'Texte pour relevé de notes', NULL, NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(55, 'transcript_footer_text', 'Texte bas de page pour relevé de notes', 'En foi de quoi, ce présent document lui est délivré pour servir et valoir ce que de droit.', 'Texte bas de page pour relevé de notes', NULL, NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(56, 'automatic_code', 'Code automatique', '1', '0 : pour inserrer le code de la personne manuellement; 1 : pour laisser à SIGES le soin de fournir le code automatiquement', '0 : to insert person\'s code manually 1 : to let SIGES provides the code automatically', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(57, 'admission_note', 'Remarque pour admission', 'En inscrivant votre enfant au Collège, vous acceptez de vous conformer à ses règlements. <br/>La date prévue pour les exam...', 'Remarque concernant l’admission', 'Note related to enrollment', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(58, 'display_class_enroll', 'Afficher l\'effectif de la salle', '1', '0: ne pas afficher l\'effectif de la salle dans le bulletin 1: Afficher l\'effectif de la salle dans le bulletin ', '0: do not display class enroll in report card 1: display class enroll in report card ', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(59, 'display_period_summary', 'Inclure sommaire dans le bulletin', '0', '0: ne pas afficher le sommaire des periodes dans le bulletin 1: Afficher le sommaire des periodes dans le bulletin', '0: do not display period summary in report card 1: display period summary in report card ', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(69, 'use_period_weight', 'Ponderation des periodes', '0', '0: Ne pas utiliser la ponderation des periodes dans le bulletin 1: Utiliser la ponderation des periodes dans le bulletin', '0: do not care about period weight in report card 1: take care about period weight in report card ', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(70, 'day_for_currentYear_postulant', 'Nombre de jour limite pour candidature', '45', 'Nombre de jour limite a partir de la date debut de l\'annee(la session) en cours pour la reception de candidatures la concernant', 'Deadline from the current academic year(session) starting date to receive his postulants', NULL, NULL, '2017-11-02 00:00:00', NULL, NULL),
(71, 'culmul_discipline_annuel', 'Cumul note discipline annuelle', '1', '0: Cumul par periode - 1: Cumul par annee', '', NULL, '2018-03-07 00:00:00', NULL, NULL, NULL),
(72, 'bonis_set', 'Methode de calcul du bonis', '1', '0: ne pas tenir compte des payrolls sur les 12 mois de l\'annee; 1: tenir compte des payrolls sur les 12 mois de l\'annee; 2: en utilisant un pourcentage.', '0: do not care about payrolls for the year; 1: care about payrolls for the year; 2: using a percentage.', 'sys', NULL, NULL, NULL, NULL),
(73, 'display_behavior_menu', 'Afficher menu discipline', '1', '0: ne pas afficher le menu discipline; 1: afficher le menu discipline.', '0: do not display behavior menu; 1: display behavior menu.', 'sys', NULL, '2018-08-04 00:00:00', NULL, NULL),
(74, 'display_billing_menu', 'Afficher menu economat', '1', '0: ne pas afficher le menu economat; 1: afficher le menu economat.', '0: do not display billing menu; 1: display billing menu.', 'sys', NULL, '2018-08-04 00:00:00', NULL, NULL),
(75, 'display_archives', 'Afficher menu Archives', '0', '0: ne pas afficher le menu ArchiveS; 1: afficher le menu Archives.', '0: do not display Archives menu; 1: display Archives menu.', 'sys', NULL, NULL, NULL, NULL),
(76, 'use_behavior_summary_inDecision', 'Decision finale avec sommaire discipline', '1', '0: ne pas tenir compte du sommaire disciplinaire dans la decision finale; 1: tenir compte du sommaire disciplinaire dans la decision finale', '0: do not care about behavior summary in decision; 1: care about behavior summary in decision', 'sys', NULL, '2018-06-27 00:00:00', NULL, NULL),
(77, 'use_mention_summary', 'Decision finale avec sommaire', '1', '0: ne pas tenir compte du sommaire des periodes; 1: tenir compte du sommaire des periodes', '0: do not care about periods summary; 1: care about periods summary', 'sys', NULL, '2018-06-29 00:00:00', NULL, NULL),
(78, 'use_automatic_mention', 'Mention automatique(Decision Fin Annee)', '1', '0: Ne pas utiliser la mention automatique dans la Decision de Fin d\'Annee 1: Utiliser la mention automatique dans la Decision de Fin d\'Annee', '0: do not use automatic mention for End Year Decision 1: Use automatic mention for End Year Decision', NULL, NULL, NULL, NULL, NULL),
(80, 'header_line_color', 'Couleur de la ligne  d\'en-tête (Header line color)', '0', '0: noire; 1: rouge; 2: bleue; 3: verte; 4: orange', '0: black; 1: red; 2: blue; 3: green; 4: orange', NULL, NULL, NULL, NULL, NULL),
(81, 'header_schoolname_color', 'Couleur de l\'en-tête', '0', '0: noire; 1: rouge; 2: bleue; 3: verte; 4: orange', '0: black; 1: red; 2: blue; 3: green; 4: orange', NULL, NULL, NULL, NULL, NULL),
(82, 'header_position', 'Position des textes de l\'en-tête', '1', '1: à gauche; 2: milieu; 3: à droite', '1: left; 2: center; 3: right', NULL, NULL, NULL, NULL, NULL),
(89, 'display_calendar_in_menu', 'Afficher calendrier dans le menu', '1', '0: ne pas afficher le calendrier dans le menu; 1: afficher le calendrier dans le menu.', '0: do not display calendar in menu; 1: display calendar in menu.', 'sys', NULL, '2018-08-11 00:00:00', NULL, NULL),
(90, 'display_schedules_in_menu', 'Afficher horaire dans le menu', '0', '0: ne pas afficher horaire dans le menu; 1: afficher horaire dans le menu.', '0: do not display schedules in menu; 1: display schedules in menu.', 'sys', NULL, NULL, NULL, NULL),
(101, 'display_announcement_in_menu', 'Afficher annonce dans le menu', '1', '0: ne pas afficher annonce dans le menu; 1: afficher annonce dans le menu.', '0: do not display announcement in menu; 1: display announcement in menu.', 'sys', NULL, NULL, NULL, NULL),
(102, 'display_homework_menu', 'Afficher menu devoir', '0', '0: ne pas afficher le menu devoir; 1: afficher le menu devoir.', '0: do not display homework menu; 1: display homework menu.', 'sys', NULL, NULL, NULL, NULL),
(103, 'display_pos_in_menu', 'Afficher menu point de vente', '0', '0: ne pas afficher le menu point de vente; 1: afficher le menu point de vente.', '0: do not display point of sale menu; 1: display point of sale menu.', 'sys', NULL, NULL, NULL, NULL),
(104, 'display_loan_in_menu', 'Afficher menu prêt', '0', '0: ne pas afficher le menu prêt; 1: afficher le menu prêt.', '0: do not display loan menu; 1: display loan menu.', 'sys', NULL, NULL, NULL, NULL),
(105, 'display_cycle_in_menu', 'Afficher Cycle au menu', '0', '0: ne pas afficher Cycle au menu; 1: afficher Cycle au menu.', '0: do not display Cycle in menu; 1: display Cycle in menu.', 'sys', NULL, NULL, NULL, NULL),
(106, 'set_palmares', 'Choisir Palmarès', '1', '0: Ancien Palmarès | 1: Nouveau Palmarès', '0: Old Grade book | 1: New Grade Book', 'sys', '2018-06-30 00:00:00', NULL, 'logipam', NULL),
(107, 'lower_behaviorGrade_summary', 'Note minimale sommaire discipline', '80', '', '', 'sys', '2018-06-30 00:00:00', NULL, 'logipam', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `grades`
--

CREATE TABLE `grades` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `grade_value` float DEFAULT NULL,
  `rewrite_grade_value` float DEFAULT NULL,
  `validate` tinyint(4) NOT NULL DEFAULT '0',
  `publish` tinyint(4) NOT NULL DEFAULT '0',
  `comment` varchar(255) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `group_name` varchar(32) NOT NULL,
  `belongs_to_profil` int(11) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `group_name`, `belongs_to_profil`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES
(1, 'Developer', 1, '2015-05-04 00:00:00', '2016-10-11 00:00:00', '_developer_', '_developer_'),
(2, 'Default Group', 5, NULL, '2015-05-07 00:00:00', NULL, '_developer_'),
(3, 'Student', 5, NULL, '2015-08-31 00:00:00', NULL, '_developer_'),
(4, 'Parent', 5, NULL, '2015-08-30 00:00:00', NULL, '_developer_'),
(5, 'Direction', 1, NULL, '2016-10-11 00:00:00', NULL, '_developer_'),
(6, 'Administration', 2, NULL, '2016-10-11 00:00:00', NULL, '_developer_'),
(7, 'Economat', 3, NULL, '2016-07-07 00:00:00', NULL, '_developer_'),
(8, 'Teacher', 4, NULL, '2015-09-03 00:00:00', NULL, '_developer_'),
(9, 'Reporter', 6, NULL, '2016-07-04 00:00:00', NULL, '_developer_'),
(10, 'Discipline', 2, '2015-09-05 00:00:00', '2015-09-06 00:00:00', '_developer_', '_developer_'),
(11, 'Pedagogie', 2, '2015-09-06 00:00:00', '2016-10-11 00:00:00', '_developer_', '_developer_'),
(12, 'Publication', 7, '2016-04-14 00:00:00', '2016-04-16 00:00:00', 'master_user', 'master_user'),
(13, 'Economat ADM', 3, '2016-07-09 00:00:00', '2016-07-09 00:00:00', '_developer_', 'admin'),
(14, 'Administrateur systeme', 1, '2016-12-10 00:00:00', NULL, '_developer_', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `groups_has_actions`
--

CREATE TABLE `groups_has_actions` (
  `groups_id` int(11) NOT NULL,
  `actions_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups_has_actions`
--

INSERT INTO `groups_has_actions` (`groups_id`, `actions_id`) VALUES
(1, 1),
(5, 1),
(14, 1),
(1, 2),
(5, 2),
(14, 2),
(1, 3),
(5, 3),
(6, 3),
(7, 3),
(10, 3),
(14, 3),
(1, 4),
(5, 4),
(14, 4),
(1, 5),
(5, 5),
(8, 5),
(14, 5),
(1, 6),
(3, 6),
(4, 6),
(5, 6),
(6, 6),
(7, 6),
(8, 6),
(9, 6),
(10, 6),
(11, 6),
(12, 6),
(13, 6),
(14, 6),
(1, 7),
(5, 7),
(6, 7),
(7, 7),
(10, 7),
(12, 7),
(14, 7),
(1, 8),
(5, 8),
(14, 8),
(1, 9),
(5, 9),
(6, 9),
(7, 9),
(8, 9),
(10, 9),
(11, 9),
(12, 9),
(13, 9),
(14, 9),
(1, 10),
(5, 10),
(6, 10),
(7, 10),
(8, 10),
(10, 10),
(11, 10),
(12, 10),
(13, 10),
(14, 10),
(1, 11),
(5, 11),
(6, 11),
(7, 11),
(8, 11),
(10, 11),
(11, 11),
(12, 11),
(13, 11),
(14, 11),
(1, 12),
(5, 12),
(6, 12),
(7, 12),
(10, 12),
(11, 12),
(13, 12),
(14, 12),
(1, 13),
(5, 13),
(6, 13),
(10, 13),
(14, 13),
(1, 14),
(5, 14),
(6, 14),
(7, 14),
(10, 14),
(11, 14),
(13, 14),
(14, 14),
(1, 15),
(5, 15),
(6, 15),
(7, 15),
(10, 15),
(11, 15),
(12, 15),
(13, 15),
(14, 15),
(1, 16),
(5, 16),
(6, 16),
(7, 16),
(10, 16),
(11, 16),
(12, 16),
(13, 16),
(14, 16),
(1, 17),
(5, 17),
(6, 17),
(7, 17),
(10, 17),
(11, 17),
(12, 17),
(13, 17),
(14, 17),
(1, 18),
(5, 18),
(6, 18),
(7, 18),
(10, 18),
(11, 18),
(12, 18),
(13, 18),
(14, 18),
(1, 19),
(5, 19),
(6, 19),
(7, 19),
(10, 19),
(11, 19),
(13, 19),
(14, 19),
(1, 20),
(5, 20),
(6, 20),
(7, 20),
(10, 20),
(11, 20),
(12, 20),
(13, 20),
(14, 20),
(1, 21),
(5, 21),
(6, 21),
(7, 21),
(10, 21),
(11, 21),
(13, 21),
(14, 21),
(1, 22),
(5, 22),
(6, 22),
(7, 22),
(10, 22),
(11, 22),
(13, 22),
(14, 22),
(1, 23),
(5, 23),
(6, 23),
(7, 23),
(10, 23),
(11, 23),
(12, 23),
(13, 23),
(14, 23),
(1, 24),
(5, 24),
(6, 24),
(7, 24),
(11, 24),
(13, 24),
(14, 24),
(1, 25),
(5, 25),
(6, 25),
(7, 25),
(10, 25),
(11, 25),
(12, 25),
(13, 25),
(14, 25),
(1, 26),
(5, 26),
(6, 26),
(7, 26),
(10, 26),
(11, 26),
(13, 26),
(14, 26),
(1, 27),
(5, 27),
(6, 27),
(7, 27),
(8, 27),
(10, 27),
(11, 27),
(13, 27),
(14, 27),
(1, 28),
(5, 28),
(6, 28),
(7, 28),
(8, 28),
(10, 28),
(11, 28),
(12, 28),
(13, 28),
(14, 28),
(1, 29),
(5, 29),
(6, 29),
(7, 29),
(11, 29),
(13, 29),
(14, 29),
(1, 30),
(5, 30),
(6, 30),
(7, 30),
(10, 30),
(11, 30),
(12, 30),
(13, 30),
(14, 30),
(1, 31),
(5, 31),
(6, 31),
(7, 31),
(10, 31),
(11, 31),
(13, 31),
(14, 31),
(1, 32),
(5, 32),
(6, 32),
(7, 32),
(10, 32),
(11, 32),
(13, 32),
(14, 32),
(1, 33),
(5, 33),
(6, 33),
(7, 33),
(10, 33),
(11, 33),
(12, 33),
(13, 33),
(14, 33),
(1, 34),
(5, 34),
(6, 34),
(7, 34),
(11, 34),
(13, 34),
(14, 34),
(1, 35),
(5, 35),
(6, 35),
(7, 35),
(10, 35),
(11, 35),
(12, 35),
(13, 35),
(14, 35),
(1, 36),
(5, 36),
(6, 36),
(7, 36),
(11, 36),
(13, 36),
(14, 36),
(1, 37),
(5, 37),
(6, 37),
(7, 37),
(10, 37),
(11, 37),
(13, 37),
(14, 37),
(1, 38),
(5, 38),
(6, 38),
(7, 38),
(10, 38),
(11, 38),
(12, 38),
(13, 38),
(14, 38),
(1, 39),
(5, 39),
(6, 39),
(7, 39),
(11, 39),
(13, 39),
(14, 39),
(1, 40),
(5, 40),
(6, 40),
(7, 40),
(8, 40),
(10, 40),
(11, 40),
(12, 40),
(13, 40),
(14, 40),
(1, 41),
(5, 41),
(6, 41),
(8, 41),
(11, 41),
(12, 41),
(14, 41),
(1, 42),
(5, 42),
(6, 42),
(8, 42),
(11, 42),
(12, 42),
(14, 42),
(1, 43),
(5, 43),
(6, 43),
(7, 43),
(8, 43),
(10, 43),
(11, 43),
(12, 43),
(13, 43),
(14, 43),
(1, 44),
(5, 44),
(6, 44),
(8, 44),
(11, 44),
(12, 44),
(14, 44),
(1, 45),
(5, 45),
(6, 45),
(7, 45),
(8, 45),
(10, 45),
(11, 45),
(12, 45),
(13, 45),
(14, 45),
(1, 46),
(5, 46),
(6, 46),
(7, 46),
(13, 46),
(14, 46),
(1, 47),
(5, 47),
(6, 47),
(7, 47),
(13, 47),
(14, 47),
(1, 48),
(5, 48),
(6, 48),
(7, 48),
(13, 48),
(14, 48),
(1, 49),
(5, 49),
(6, 49),
(7, 49),
(13, 49),
(14, 49),
(1, 50),
(5, 50),
(6, 50),
(7, 50),
(13, 50),
(14, 50),
(1, 51),
(5, 51),
(6, 51),
(7, 51),
(13, 51),
(14, 51),
(1, 52),
(5, 52),
(6, 52),
(7, 52),
(13, 52),
(14, 52),
(1, 53),
(5, 53),
(6, 53),
(7, 53),
(13, 53),
(14, 53),
(1, 54),
(5, 54),
(7, 54),
(13, 54),
(14, 54),
(1, 55),
(5, 55),
(6, 55),
(7, 55),
(8, 55),
(10, 55),
(11, 55),
(12, 55),
(13, 55),
(14, 55),
(1, 56),
(5, 56),
(6, 56),
(7, 56),
(10, 56),
(11, 56),
(13, 56),
(14, 56),
(1, 57),
(5, 57),
(6, 57),
(7, 57),
(10, 57),
(11, 57),
(13, 57),
(14, 57),
(1, 58),
(5, 58),
(6, 58),
(7, 58),
(10, 58),
(11, 58),
(13, 58),
(14, 58),
(1, 59),
(5, 59),
(6, 59),
(7, 59),
(10, 59),
(11, 59),
(13, 59),
(14, 59),
(1, 60),
(5, 60),
(6, 60),
(7, 60),
(10, 60),
(11, 60),
(12, 60),
(13, 60),
(14, 60),
(1, 61),
(5, 61),
(6, 61),
(11, 61),
(14, 61),
(1, 62),
(5, 62),
(6, 62),
(11, 62),
(14, 62),
(1, 63),
(5, 63),
(6, 63),
(7, 63),
(10, 63),
(11, 63),
(12, 63),
(13, 63),
(14, 63),
(1, 64),
(5, 64),
(6, 64),
(7, 64),
(8, 64),
(10, 64),
(11, 64),
(12, 64),
(13, 64),
(14, 64),
(1, 65),
(5, 65),
(6, 65),
(11, 65),
(14, 65),
(1, 66),
(5, 66),
(6, 66),
(7, 66),
(10, 66),
(11, 66),
(12, 66),
(13, 66),
(14, 66),
(1, 67),
(5, 67),
(6, 67),
(11, 67),
(14, 67),
(1, 68),
(5, 68),
(6, 68),
(11, 68),
(14, 68),
(1, 69),
(5, 69),
(6, 69),
(7, 69),
(10, 69),
(11, 69),
(12, 69),
(13, 69),
(14, 69),
(1, 70),
(5, 70),
(6, 70),
(11, 70),
(14, 70),
(1, 71),
(5, 71),
(6, 71),
(7, 71),
(8, 71),
(10, 71),
(11, 71),
(12, 71),
(13, 71),
(14, 71),
(1, 72),
(5, 72),
(6, 72),
(11, 72),
(14, 72),
(1, 73),
(5, 73),
(6, 73),
(11, 73),
(14, 73),
(1, 74),
(5, 74),
(6, 74),
(7, 74),
(10, 74),
(11, 74),
(12, 74),
(13, 74),
(14, 74),
(1, 75),
(5, 75),
(6, 75),
(11, 75),
(14, 75),
(1, 76),
(5, 76),
(6, 76),
(7, 76),
(10, 76),
(11, 76),
(12, 76),
(13, 76),
(14, 76),
(1, 77),
(5, 77),
(6, 77),
(10, 77),
(11, 77),
(14, 77),
(1, 78),
(5, 78),
(6, 78),
(10, 78),
(11, 78),
(14, 78),
(1, 79),
(5, 79),
(6, 79),
(7, 79),
(10, 79),
(11, 79),
(12, 79),
(13, 79),
(14, 79),
(1, 80),
(5, 80),
(6, 80),
(7, 80),
(8, 80),
(10, 80),
(11, 80),
(12, 80),
(13, 80),
(14, 80),
(1, 81),
(5, 81),
(6, 81),
(10, 81),
(11, 81),
(12, 81),
(14, 81),
(1, 82),
(5, 82),
(6, 82),
(10, 82),
(11, 82),
(14, 82),
(1, 83),
(5, 83),
(6, 83),
(11, 83),
(14, 83),
(1, 84),
(5, 84),
(6, 84),
(11, 84),
(14, 84),
(1, 85),
(5, 85),
(6, 85),
(11, 85),
(14, 85),
(1, 86),
(5, 86),
(6, 86),
(11, 86),
(14, 86),
(1, 87),
(5, 87),
(6, 87),
(11, 87),
(14, 87),
(1, 88),
(5, 88),
(6, 88),
(11, 88),
(14, 88),
(1, 89),
(5, 89),
(6, 89),
(11, 89),
(14, 89),
(1, 90),
(5, 90),
(6, 90),
(11, 90),
(14, 90),
(1, 91),
(5, 91),
(6, 91),
(11, 91),
(14, 91),
(1, 92),
(5, 92),
(6, 92),
(11, 92),
(14, 92),
(1, 93),
(5, 93),
(6, 93),
(14, 93),
(1, 94),
(5, 94),
(6, 94),
(14, 94),
(1, 95),
(5, 95),
(6, 95),
(14, 95),
(1, 96),
(5, 96),
(6, 96),
(14, 96),
(1, 97),
(5, 97),
(6, 97),
(14, 97),
(1, 98),
(5, 98),
(6, 98),
(11, 98),
(14, 98),
(1, 99),
(5, 99),
(6, 99),
(11, 99),
(14, 99),
(1, 100),
(5, 100),
(6, 100),
(11, 100),
(14, 100),
(1, 101),
(5, 101),
(6, 101),
(11, 101),
(14, 101),
(1, 102),
(5, 102),
(6, 102),
(11, 102),
(14, 102),
(1, 103),
(5, 103),
(6, 103),
(13, 103),
(14, 103),
(1, 104),
(5, 104),
(6, 104),
(13, 104),
(14, 104),
(1, 105),
(5, 105),
(6, 105),
(13, 105),
(14, 105),
(1, 106),
(5, 106),
(6, 106),
(13, 106),
(14, 106),
(1, 107),
(5, 107),
(6, 107),
(13, 107),
(14, 107),
(1, 108),
(5, 108),
(6, 108),
(11, 108),
(14, 108),
(1, 109),
(5, 109),
(6, 109),
(11, 109),
(14, 109),
(1, 110),
(5, 110),
(6, 110),
(11, 110),
(14, 110),
(1, 111),
(5, 111),
(6, 111),
(11, 111),
(14, 111),
(1, 112),
(5, 112),
(6, 112),
(11, 112),
(14, 112),
(1, 113),
(5, 113),
(6, 113),
(14, 113),
(1, 114),
(5, 114),
(6, 114),
(14, 114),
(1, 115),
(5, 115),
(6, 115),
(14, 115),
(1, 116),
(5, 116),
(6, 116),
(14, 116),
(1, 117),
(5, 117),
(6, 117),
(14, 117),
(1, 118),
(5, 118),
(6, 118),
(14, 118),
(1, 119),
(5, 119),
(6, 119),
(14, 119),
(1, 120),
(5, 120),
(6, 120),
(14, 120),
(1, 121),
(5, 121),
(6, 121),
(14, 121),
(1, 122),
(5, 122),
(6, 122),
(14, 122),
(1, 123),
(5, 123),
(6, 123),
(14, 123),
(1, 124),
(5, 124),
(6, 124),
(11, 124),
(14, 124),
(1, 125),
(5, 125),
(6, 125),
(11, 125),
(14, 125),
(1, 126),
(5, 126),
(6, 126),
(11, 126),
(14, 126),
(1, 127),
(5, 127),
(6, 127),
(11, 127),
(14, 127),
(1, 128),
(5, 128),
(6, 128),
(11, 128),
(14, 128),
(1, 129),
(5, 129),
(6, 129),
(11, 129),
(14, 129),
(1, 130),
(5, 130),
(6, 130),
(11, 130),
(14, 130),
(1, 131),
(5, 131),
(6, 131),
(11, 131),
(14, 131),
(1, 132),
(5, 132),
(6, 132),
(11, 132),
(14, 132),
(1, 133),
(5, 133),
(6, 133),
(11, 133),
(14, 133),
(1, 134),
(5, 134),
(6, 134),
(13, 134),
(14, 134),
(1, 135),
(5, 135),
(6, 135),
(13, 135),
(14, 135),
(1, 136),
(5, 136),
(6, 136),
(13, 136),
(14, 136),
(1, 137),
(5, 137),
(6, 137),
(13, 137),
(14, 137),
(1, 138),
(5, 138),
(6, 138),
(13, 138),
(14, 138),
(1, 139),
(5, 139),
(6, 139),
(11, 139),
(14, 139),
(1, 140),
(5, 140),
(6, 140),
(11, 140),
(14, 140),
(1, 141),
(5, 141),
(6, 141),
(11, 141),
(14, 141),
(1, 142),
(5, 142),
(6, 142),
(11, 142),
(14, 142),
(1, 143),
(5, 143),
(6, 143),
(11, 143),
(14, 143),
(1, 144),
(5, 144),
(6, 144),
(11, 144),
(14, 144),
(1, 145),
(5, 145),
(6, 145),
(11, 145),
(14, 145),
(1, 146),
(5, 146),
(6, 146),
(11, 146),
(14, 146),
(1, 147),
(5, 147),
(6, 147),
(11, 147),
(14, 147),
(1, 148),
(5, 148),
(6, 148),
(11, 148),
(14, 148),
(1, 149),
(5, 149),
(6, 149),
(11, 149),
(14, 149),
(1, 150),
(5, 150),
(6, 150),
(11, 150),
(14, 150),
(1, 151),
(5, 151),
(6, 151),
(11, 151),
(14, 151),
(1, 152),
(5, 152),
(6, 152),
(11, 152),
(14, 152),
(1, 153),
(5, 153),
(6, 153),
(11, 153),
(14, 153),
(1, 154),
(5, 154),
(11, 154),
(14, 154),
(1, 155),
(5, 155),
(6, 155),
(11, 155),
(14, 155),
(1, 156),
(5, 156),
(6, 156),
(11, 156),
(14, 156),
(1, 157),
(5, 157),
(6, 157),
(11, 157),
(14, 157),
(1, 158),
(5, 158),
(6, 158),
(11, 158),
(14, 158),
(1, 159),
(5, 159),
(6, 159),
(11, 159),
(14, 159),
(1, 160),
(5, 160),
(6, 160),
(11, 160),
(14, 160),
(1, 161),
(5, 161),
(6, 161),
(11, 161),
(14, 161),
(1, 162),
(5, 162),
(6, 162),
(11, 162),
(14, 162),
(1, 163),
(5, 163),
(6, 163),
(11, 163),
(14, 163),
(1, 164),
(5, 164),
(6, 164),
(11, 164),
(14, 164),
(1, 165),
(5, 165),
(6, 165),
(11, 165),
(14, 165),
(1, 166),
(5, 166),
(6, 166),
(11, 166),
(14, 166),
(1, 167),
(5, 167),
(6, 167),
(11, 167),
(14, 167),
(1, 168),
(5, 168),
(6, 168),
(11, 168),
(14, 168),
(1, 169),
(5, 169),
(14, 169),
(1, 170),
(5, 170),
(14, 170),
(1, 171),
(5, 171),
(14, 171),
(1, 172),
(5, 172),
(14, 172),
(1, 173),
(5, 173),
(14, 173),
(1, 174),
(5, 174),
(14, 174),
(1, 175),
(5, 175),
(14, 175),
(1, 176),
(5, 176),
(14, 176),
(1, 177),
(5, 177),
(14, 177),
(1, 178),
(5, 178),
(14, 178),
(1, 179),
(5, 179),
(14, 179),
(1, 180),
(5, 180),
(14, 180),
(1, 181),
(5, 181),
(14, 181),
(1, 182),
(5, 182),
(14, 182),
(1, 183),
(5, 183),
(14, 183),
(1, 184),
(5, 184),
(6, 184),
(11, 184),
(14, 184),
(1, 185),
(3, 185),
(4, 185),
(5, 185),
(6, 185),
(14, 185),
(1, 186),
(3, 186),
(4, 186),
(5, 186),
(6, 186),
(14, 186),
(1, 187),
(3, 187),
(4, 187),
(5, 187),
(6, 187),
(14, 187),
(1, 188),
(4, 188),
(5, 188),
(6, 188),
(14, 188),
(1, 189),
(4, 189),
(5, 189),
(6, 189),
(14, 189),
(1, 190),
(3, 190),
(4, 190),
(5, 190),
(6, 190),
(14, 190),
(1, 191),
(3, 191),
(4, 191),
(5, 191),
(6, 191),
(14, 191),
(1, 192),
(3, 192),
(4, 192),
(5, 192),
(6, 192),
(14, 192),
(1, 193),
(3, 193),
(4, 193),
(5, 193),
(6, 193),
(14, 193),
(1, 194),
(3, 194),
(4, 194),
(5, 194),
(6, 194),
(14, 194),
(1, 195),
(3, 195),
(4, 195),
(5, 195),
(6, 195),
(14, 195),
(1, 196),
(3, 196),
(4, 196),
(5, 196),
(6, 196),
(14, 196),
(1, 197),
(3, 197),
(4, 197),
(5, 197),
(6, 197),
(14, 197),
(1, 198),
(3, 198),
(4, 198),
(5, 198),
(6, 198),
(14, 198),
(1, 200),
(3, 200),
(4, 200),
(5, 200),
(6, 200),
(14, 200),
(1, 201),
(5, 201),
(6, 201),
(7, 201),
(8, 201),
(10, 201),
(11, 201),
(12, 201),
(13, 201),
(14, 201),
(1, 202),
(5, 202),
(6, 202),
(7, 202),
(8, 202),
(10, 202),
(11, 202),
(12, 202),
(13, 202),
(14, 202),
(1, 203),
(5, 203),
(6, 203),
(7, 203),
(8, 203),
(10, 203),
(11, 203),
(12, 203),
(13, 203),
(14, 203),
(1, 204),
(5, 204),
(6, 204),
(7, 204),
(8, 204),
(10, 204),
(11, 204),
(12, 204),
(13, 204),
(14, 204),
(1, 205),
(4, 205),
(5, 205),
(6, 205),
(14, 205),
(1, 206),
(3, 206),
(5, 206),
(6, 206),
(14, 206),
(1, 207),
(3, 207),
(5, 207),
(6, 207),
(14, 207),
(1, 208),
(5, 208),
(6, 208),
(7, 208),
(10, 208),
(11, 208),
(12, 208),
(13, 208),
(14, 208),
(1, 209),
(5, 209),
(6, 209),
(7, 209),
(10, 209),
(11, 209),
(12, 209),
(13, 209),
(14, 209),
(1, 210),
(5, 210),
(6, 210),
(7, 210),
(8, 210),
(10, 210),
(11, 210),
(12, 210),
(13, 210),
(14, 210),
(1, 211),
(5, 211),
(6, 211),
(7, 211),
(10, 211),
(11, 211),
(12, 211),
(13, 211),
(14, 211),
(1, 212),
(5, 212),
(6, 212),
(7, 212),
(10, 212),
(11, 212),
(12, 212),
(13, 212),
(14, 212),
(1, 213),
(5, 213),
(6, 213),
(7, 213),
(10, 213),
(11, 213),
(12, 213),
(13, 213),
(14, 213),
(1, 214),
(5, 214),
(6, 214),
(7, 214),
(8, 214),
(10, 214),
(11, 214),
(12, 214),
(13, 214),
(14, 214),
(1, 215),
(5, 215),
(6, 215),
(7, 215),
(8, 215),
(10, 215),
(11, 215),
(12, 215),
(13, 215),
(14, 215),
(1, 216),
(5, 216),
(6, 216),
(7, 216),
(8, 216),
(10, 216),
(11, 216),
(12, 216),
(13, 216),
(14, 216),
(1, 217),
(2, 217),
(3, 217),
(4, 217),
(5, 217),
(6, 217),
(14, 217),
(1, 218),
(2, 218),
(3, 218),
(4, 218),
(5, 218),
(6, 218),
(14, 218),
(1, 219),
(5, 219),
(14, 219),
(1, 220),
(5, 220),
(6, 220),
(7, 220),
(10, 220),
(11, 220),
(13, 220),
(14, 220),
(1, 221),
(5, 221),
(6, 221),
(7, 221),
(10, 221),
(11, 221),
(13, 221),
(14, 221),
(1, 222),
(5, 222),
(7, 222),
(13, 222),
(1, 223),
(5, 223),
(7, 223),
(13, 223),
(1, 224),
(5, 224),
(7, 224),
(13, 224),
(1, 225),
(5, 225),
(6, 225),
(7, 225),
(13, 225),
(1, 226),
(5, 226),
(6, 226),
(7, 226),
(10, 226),
(11, 226),
(13, 226),
(14, 226),
(1, 227),
(5, 227),
(6, 227),
(7, 227),
(10, 227),
(11, 227),
(13, 227),
(14, 227),
(1, 228),
(5, 228),
(6, 228),
(7, 228),
(10, 228),
(11, 228),
(13, 228),
(14, 228),
(1, 229),
(5, 229),
(6, 229),
(10, 229),
(14, 229),
(1, 230),
(5, 230),
(6, 230),
(7, 230),
(10, 230),
(11, 230),
(13, 230),
(14, 230),
(1, 231),
(5, 231),
(6, 231),
(7, 231),
(8, 231),
(10, 231),
(11, 231),
(12, 231),
(13, 231),
(14, 231),
(1, 232),
(5, 232),
(6, 232),
(8, 232),
(11, 232),
(12, 232),
(14, 232),
(1, 233),
(5, 233),
(6, 233),
(8, 233),
(12, 233),
(14, 233),
(1, 234),
(5, 234),
(6, 234),
(8, 234),
(11, 234),
(12, 234),
(14, 234),
(1, 235),
(3, 235),
(4, 235),
(5, 235),
(6, 235),
(14, 235),
(1, 236),
(3, 236),
(4, 236),
(5, 236),
(6, 236),
(14, 236),
(1, 237),
(3, 237),
(4, 237),
(5, 237),
(6, 237),
(14, 237),
(1, 238),
(3, 238),
(5, 238),
(6, 238),
(14, 238),
(1, 239),
(3, 239),
(4, 239),
(5, 239),
(6, 239),
(14, 239),
(1, 240),
(5, 240),
(6, 240),
(10, 240),
(11, 240),
(14, 240),
(1, 241),
(5, 241),
(6, 241),
(10, 241),
(11, 241),
(14, 241),
(1, 242),
(5, 242),
(6, 242),
(10, 242),
(11, 242),
(14, 242),
(1, 243),
(5, 243),
(6, 243),
(10, 243),
(11, 243),
(14, 243),
(1, 244),
(5, 244),
(6, 244),
(10, 244),
(11, 244),
(14, 244),
(1, 245),
(5, 245),
(6, 245),
(10, 245),
(11, 245),
(14, 245),
(1, 246),
(5, 246),
(6, 246),
(10, 246),
(11, 246),
(14, 246),
(1, 247),
(5, 247),
(6, 247),
(10, 247),
(11, 247),
(14, 247),
(1, 248),
(5, 248),
(6, 248),
(10, 248),
(11, 248),
(14, 248),
(1, 249),
(5, 249),
(6, 249),
(10, 249),
(11, 249),
(14, 249),
(1, 250),
(5, 250),
(6, 250),
(10, 250),
(11, 250),
(14, 250),
(1, 251),
(5, 251),
(6, 251),
(10, 251),
(11, 251),
(14, 251),
(1, 252),
(5, 252),
(6, 252),
(10, 252),
(11, 252),
(14, 252),
(1, 253),
(5, 253),
(6, 253),
(10, 253),
(11, 253),
(14, 253),
(1, 254),
(5, 254),
(6, 254),
(10, 254),
(11, 254),
(14, 254),
(1, 255),
(5, 255),
(6, 255),
(10, 255),
(11, 255),
(14, 255),
(1, 256),
(5, 256),
(6, 256),
(10, 256),
(11, 256),
(14, 256),
(1, 257),
(5, 257),
(7, 257),
(13, 257),
(1, 258),
(5, 258),
(6, 258),
(14, 258),
(1, 259),
(5, 259),
(7, 259),
(13, 259),
(1, 260),
(5, 260),
(7, 260),
(13, 260),
(1, 261),
(5, 261),
(6, 261),
(8, 261),
(10, 261),
(11, 261),
(12, 261),
(14, 261),
(1, 262),
(5, 262),
(7, 262),
(12, 262),
(13, 262),
(1, 263),
(5, 263),
(7, 263),
(13, 263),
(1, 264),
(5, 264),
(7, 264),
(13, 264),
(1, 265),
(5, 265),
(7, 265),
(13, 265),
(1, 266),
(5, 266),
(6, 266),
(7, 266),
(8, 266),
(10, 266),
(11, 266),
(12, 266),
(13, 266),
(1, 267),
(5, 267),
(7, 267),
(13, 267),
(14, 267),
(1, 268),
(5, 268),
(7, 268),
(13, 268),
(1, 269),
(5, 269),
(7, 269),
(13, 269),
(1, 270),
(5, 270),
(7, 270),
(13, 270),
(1, 271),
(5, 271),
(7, 271),
(12, 271),
(13, 271),
(1, 272),
(5, 272),
(7, 272),
(13, 272),
(14, 272),
(1, 273),
(5, 273),
(7, 273),
(13, 273),
(14, 273),
(1, 274),
(5, 274),
(7, 274),
(13, 274),
(14, 274),
(1, 275),
(5, 275),
(7, 275),
(13, 275),
(14, 275),
(1, 276),
(5, 276),
(6, 276),
(7, 276),
(13, 276),
(14, 276),
(1, 277),
(5, 277),
(6, 277),
(7, 277),
(13, 277),
(14, 277),
(1, 278),
(5, 278),
(6, 278),
(7, 278),
(13, 278),
(14, 278),
(1, 279),
(5, 279),
(7, 279),
(13, 279),
(14, 279),
(1, 280),
(5, 280),
(6, 280),
(7, 280),
(13, 280),
(14, 280),
(1, 281),
(5, 281),
(6, 281),
(7, 281),
(8, 281),
(10, 281),
(11, 281),
(12, 281),
(13, 281),
(1, 282),
(5, 282),
(6, 282),
(13, 282),
(14, 282),
(1, 283),
(3, 283),
(4, 283),
(5, 283),
(14, 283),
(1, 284),
(5, 284),
(11, 284),
(12, 284),
(14, 284),
(1, 285),
(5, 285),
(11, 285),
(12, 285),
(14, 285),
(1, 286),
(5, 286),
(11, 286),
(12, 286),
(14, 286),
(1, 287),
(5, 287),
(12, 287),
(14, 287),
(1, 288),
(3, 288),
(5, 288),
(14, 288),
(1, 289),
(4, 289),
(5, 289),
(14, 289),
(1, 290),
(5, 290),
(7, 290),
(13, 290),
(1, 291),
(5, 291),
(6, 291),
(7, 291),
(10, 291),
(12, 291),
(13, 291),
(14, 291),
(1, 292),
(5, 292),
(6, 292),
(14, 292),
(1, 293),
(5, 293),
(6, 293),
(14, 293),
(1, 294),
(5, 294),
(6, 294),
(14, 294),
(1, 295),
(5, 295),
(6, 295),
(14, 295),
(1, 296),
(5, 296),
(6, 296),
(14, 296),
(1, 297),
(5, 297),
(7, 297),
(13, 297),
(14, 297),
(1, 298),
(5, 298),
(6, 298),
(7, 298),
(13, 298),
(14, 298),
(1, 299),
(5, 299),
(6, 299),
(7, 299),
(13, 299),
(14, 299),
(1, 300),
(5, 300),
(7, 300),
(13, 300),
(14, 300),
(1, 301),
(5, 301),
(6, 301),
(7, 301),
(13, 301),
(14, 301),
(1, 302),
(5, 302),
(7, 302),
(13, 302),
(14, 302),
(1, 303),
(5, 303),
(7, 303),
(13, 303),
(14, 303),
(1, 304),
(5, 304),
(7, 304),
(13, 304),
(14, 304),
(1, 305),
(5, 305),
(7, 305),
(13, 305),
(14, 305),
(1, 306),
(5, 306),
(7, 306),
(13, 306),
(14, 306),
(1, 307),
(5, 307),
(7, 307),
(13, 307),
(14, 307),
(1, 308),
(5, 308),
(6, 308),
(7, 308),
(13, 308),
(14, 308),
(1, 309),
(5, 309),
(7, 309),
(13, 309),
(14, 309),
(1, 310),
(5, 310),
(7, 310),
(13, 310),
(14, 310),
(1, 311),
(5, 311),
(7, 311),
(13, 311),
(14, 311),
(1, 312),
(5, 312),
(7, 312),
(13, 312),
(1, 313),
(5, 313),
(7, 313),
(12, 313),
(13, 313),
(1, 314),
(5, 314),
(6, 314),
(7, 314),
(12, 314),
(13, 314),
(1, 315),
(5, 315),
(7, 315),
(13, 315),
(14, 315),
(1, 316),
(5, 316),
(7, 316),
(13, 316),
(14, 316),
(1, 317),
(5, 317),
(7, 317),
(13, 317),
(14, 317),
(1, 318),
(5, 318),
(7, 318),
(13, 318),
(14, 318),
(1, 319),
(5, 319),
(6, 319),
(14, 319),
(1, 320),
(5, 320),
(7, 320),
(13, 320),
(1, 321),
(5, 321),
(7, 321),
(13, 321),
(1, 322),
(5, 322),
(6, 322),
(7, 322),
(13, 322),
(14, 322),
(1, 323),
(5, 323),
(6, 323),
(7, 323),
(13, 323),
(14, 323),
(1, 324),
(5, 324),
(6, 324),
(7, 324),
(13, 324),
(14, 324),
(1, 325),
(5, 325),
(6, 325),
(7, 325),
(13, 325),
(14, 325),
(1, 326),
(5, 326),
(6, 326),
(7, 326),
(13, 326),
(14, 326),
(1, 327),
(5, 327),
(6, 327),
(7, 327),
(13, 327),
(14, 327),
(1, 328),
(5, 328),
(6, 328),
(7, 328),
(13, 328),
(14, 328),
(1, 329),
(5, 329),
(6, 329),
(7, 329),
(13, 329),
(14, 329),
(1, 330),
(5, 330),
(6, 330),
(7, 330),
(13, 330),
(14, 330),
(1, 331),
(5, 331),
(6, 331),
(7, 331),
(13, 331),
(14, 331),
(1, 332),
(5, 332),
(6, 332),
(7, 332),
(13, 332),
(14, 332),
(1, 333),
(5, 333),
(11, 333),
(12, 333),
(14, 333),
(1, 334),
(5, 334),
(6, 334),
(11, 334),
(14, 334),
(1, 335),
(5, 335),
(6, 335),
(8, 335),
(11, 335),
(14, 335),
(1, 336),
(5, 336),
(6, 336),
(8, 336),
(11, 336),
(14, 336),
(1, 337),
(5, 337),
(6, 337),
(8, 337),
(11, 337),
(14, 337),
(1, 338),
(5, 338),
(6, 338),
(11, 338),
(14, 338),
(1, 339),
(5, 339),
(6, 339),
(11, 339),
(14, 339),
(1, 340),
(5, 340),
(6, 340),
(7, 340),
(10, 340),
(11, 340),
(13, 340),
(14, 340),
(1, 341),
(3, 341),
(4, 341),
(5, 341),
(14, 341),
(1, 342),
(3, 342),
(4, 342),
(5, 342),
(14, 342),
(1, 343),
(3, 343),
(4, 343),
(5, 343),
(14, 343),
(1, 344),
(5, 344),
(12, 344),
(14, 344),
(1, 345),
(5, 345),
(12, 345),
(14, 345),
(1, 346),
(5, 346),
(12, 346),
(14, 346),
(1, 347),
(5, 347),
(12, 347),
(14, 347),
(1, 348),
(5, 348),
(12, 348),
(14, 348),
(1, 349),
(5, 349),
(12, 349),
(14, 349),
(1, 350),
(5, 350),
(12, 350),
(14, 350),
(1, 351),
(5, 351),
(6, 351),
(14, 351),
(1, 352),
(5, 352),
(6, 352),
(14, 352),
(1, 353),
(5, 353),
(6, 353),
(11, 353),
(14, 353),
(1, 354),
(5, 354),
(12, 354),
(14, 354),
(1, 355),
(5, 355),
(6, 355),
(11, 355),
(13, 355),
(14, 355),
(1, 356),
(5, 356),
(6, 356),
(10, 356),
(11, 356),
(14, 356),
(1, 357),
(5, 357),
(6, 357),
(14, 357),
(1, 358),
(5, 358),
(6, 358),
(14, 358),
(1, 359),
(5, 359),
(6, 359),
(14, 359),
(1, 360),
(5, 360),
(7, 360),
(13, 360),
(1, 361),
(5, 361),
(6, 361),
(7, 361),
(13, 361),
(14, 361),
(1, 362),
(5, 362),
(7, 362),
(13, 362),
(14, 362),
(1, 363),
(5, 363),
(14, 363),
(1, 364),
(5, 364),
(1, 365),
(5, 365),
(1, 366),
(5, 366),
(1, 367),
(5, 367),
(6, 367),
(14, 367),
(1, 368),
(5, 368),
(14, 368),
(1, 369),
(5, 369),
(14, 369),
(1, 370),
(5, 370),
(14, 370),
(1, 371),
(5, 371),
(6, 371),
(11, 371),
(14, 371),
(1, 372),
(5, 372),
(6, 372),
(7, 372),
(11, 372),
(13, 372),
(14, 372),
(1, 373),
(5, 373),
(6, 373),
(11, 373),
(14, 373),
(1, 374),
(5, 374),
(6, 374),
(11, 374),
(14, 374),
(1, 375),
(5, 375),
(6, 375),
(11, 375),
(14, 375),
(1, 376),
(5, 376),
(6, 376),
(11, 376),
(14, 376),
(1, 377),
(5, 377),
(6, 377),
(7, 377),
(11, 377),
(13, 377),
(14, 377),
(1, 378),
(3, 378),
(4, 378),
(5, 378),
(1, 379),
(5, 379),
(6, 379),
(10, 379),
(11, 379),
(13, 379),
(1, 380),
(5, 380),
(6, 380),
(13, 380),
(1, 381),
(5, 381),
(6, 381),
(10, 381),
(11, 381),
(13, 381),
(1, 382),
(5, 382),
(6, 382),
(10, 382),
(11, 382),
(13, 382),
(1, 383),
(5, 383),
(6, 383),
(10, 383),
(11, 383),
(13, 383),
(1, 384),
(5, 384),
(6, 384),
(7, 384),
(13, 384),
(1, 385),
(5, 385),
(6, 385),
(7, 385),
(13, 385),
(1, 386),
(5, 386),
(6, 386),
(7, 386),
(13, 386),
(1, 387),
(5, 387),
(6, 387),
(7, 387),
(13, 387),
(1, 388),
(5, 388),
(13, 388),
(1, 389),
(5, 389),
(13, 389),
(1, 390),
(5, 390),
(6, 390),
(11, 390),
(14, 390),
(1, 391),
(5, 391),
(6, 391),
(11, 391),
(14, 391),
(1, 392),
(5, 392),
(6, 392),
(11, 392),
(14, 392),
(1, 393),
(5, 393),
(6, 393),
(11, 393),
(14, 393),
(1, 394),
(5, 394),
(6, 394),
(11, 394),
(14, 394),
(1, 395),
(5, 395),
(6, 395),
(11, 395),
(14, 395),
(1, 396),
(5, 396),
(6, 396),
(11, 396),
(14, 396),
(1, 397),
(5, 397),
(6, 397),
(11, 397),
(14, 397),
(1, 398),
(5, 398),
(14, 398),
(1, 399),
(5, 399),
(6, 399),
(10, 399),
(11, 399),
(14, 399),
(1, 400),
(5, 400),
(6, 400),
(7, 400),
(13, 400),
(14, 400),
(1, 401),
(5, 401),
(6, 401),
(7, 401),
(13, 401),
(14, 401),
(1, 402),
(5, 402),
(6, 402),
(7, 402),
(13, 402),
(14, 402),
(1, 403),
(5, 403),
(6, 403),
(7, 403),
(13, 403),
(14, 403),
(1, 404),
(5, 404),
(6, 404),
(7, 404),
(13, 404),
(14, 404),
(1, 405),
(5, 405),
(1, 406),
(5, 406),
(6, 406),
(7, 406),
(11, 406),
(13, 406),
(14, 406),
(1, 407),
(5, 407),
(7, 407),
(13, 407),
(1, 408),
(5, 408),
(1, 409),
(5, 409),
(1, 410),
(5, 410),
(1, 411),
(5, 411),
(1, 412),
(5, 412),
(6, 412),
(11, 412),
(14, 412),
(1, 413),
(5, 413),
(1, 414),
(5, 414),
(6, 414),
(11, 414),
(14, 414);

-- --------------------------------------------------------

--
-- Table structure for table `groups_has_modules`
--

CREATE TABLE `groups_has_modules` (
  `groups_id` int(11) NOT NULL,
  `modules_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `groups_has_modules`
--

INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES
(1, 1),
(5, 1),
(6, 1),
(7, 1),
(8, 1),
(9, 1),
(10, 1),
(11, 1),
(12, 1),
(13, 1),
(14, 1),
(1, 5),
(2, 5),
(3, 5),
(4, 5),
(5, 5),
(6, 5),
(7, 5),
(8, 5),
(9, 5),
(10, 5),
(11, 5),
(12, 5),
(13, 5),
(14, 5),
(1, 6),
(5, 6),
(6, 6),
(7, 6),
(8, 6),
(9, 6),
(10, 6),
(11, 6),
(12, 6),
(13, 6),
(14, 6),
(1, 7),
(5, 7),
(6, 7),
(7, 7),
(8, 7),
(9, 7),
(10, 7),
(11, 7),
(12, 7),
(13, 7),
(14, 7),
(1, 8),
(5, 8),
(6, 8),
(7, 8),
(8, 8),
(9, 8),
(10, 8),
(11, 8),
(12, 8),
(13, 8),
(14, 8),
(1, 9),
(5, 9),
(6, 9),
(7, 9),
(8, 9),
(9, 9),
(10, 9),
(11, 9),
(12, 9),
(13, 9),
(14, 9),
(1, 10),
(2, 10),
(3, 10),
(4, 10),
(5, 10),
(6, 10),
(10, 10),
(11, 10),
(14, 10),
(1, 11),
(5, 11),
(6, 11),
(7, 11),
(8, 11),
(9, 11),
(10, 11),
(11, 11),
(12, 11),
(13, 11),
(14, 11),
(1, 12),
(5, 12),
(6, 12),
(7, 12),
(8, 12),
(9, 12),
(10, 12),
(11, 12),
(12, 12),
(13, 12),
(14, 12);

-- --------------------------------------------------------

--
-- Table structure for table `help`
--

CREATE TABLE `help` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `categorie` varchar(64) NOT NULL,
  `create_date` datetime DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `homework`
--

CREATE TABLE `homework` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `limit_date_submission` date NOT NULL,
  `given_date` date NOT NULL,
  `attachment_ref` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `homework_submission`
--

CREATE TABLE `homework_submission` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `homework_id` int(11) NOT NULL,
  `date_submission` date NOT NULL,
  `comment` varchar(255) NOT NULL,
  `attachment_ref` varchar(100) NOT NULL,
  `grade_value` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `infraction_type`
--

CREATE TABLE `infraction_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `deductible_value` float DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `infraction_type`
--

INSERT INTO `infraction_type` (`id`, `name`, `description`, `deductible_value`) VALUES
(1, 'Tapage en classe', '', NULL),
(2, 'Bagarre', 'Quand deux ou plusieurs élèves se battent dans une salle de classe ou sur la cours de l\'école.', 5);

-- --------------------------------------------------------

--
-- Table structure for table `job_status`
--

CREATE TABLE `job_status` (
  `id` int(11) NOT NULL,
  `status_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `job_status`
--

INSERT INTO `job_status` (`id`, `status_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Contractuel', '2015-08-20 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `label_category_for_billing`
--

CREATE TABLE `label_category_for_billing` (
  `id` int(11) NOT NULL,
  `category` varchar(220) NOT NULL,
  `income_expense` varchar(3) NOT NULL COMMENT 'ri: income; di: expense'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `label_category_for_billing`
--

INSERT INTO `label_category_for_billing` (`id`, `category`, `income_expense`) VALUES
(1, 'Donations and grants', 'ri'),
(2, 'Other incomes', 'ri'),
(3, 'Rent expenses', 'di'),
(4, 'Amenities and services', 'di'),
(5, 'Staff', 'di'),
(6, 'Tax', 'di'),
(7, 'Other expenses', 'di');

-- --------------------------------------------------------

--
-- Table structure for table `levels`
--

CREATE TABLE `levels` (
  `id` int(11) NOT NULL COMMENT 'School level (calling classes dans le system scolaire Haitien)',
  `level_name` varchar(45) NOT NULL,
  `short_level_name` varchar(45) NOT NULL,
  `previous_level` int(11) DEFAULT NULL,
  `section` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `levels`
--

INSERT INTO `levels` (`id`, `level_name`, `short_level_name`, `previous_level`, `section`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(6, 'Sixième Année', '6ème AF', NULL, 1, '2014-09-23 00:00:00', '2017-08-31 00:00:00', 'admin', NULL),
(7, 'Septième Année', '7ème AF', 6, 1, '2014-09-23 00:00:00', '2017-08-31 00:00:00', 'admin', NULL),
(8, 'Huitième Année', '8ème AF', 7, 1, '2014-09-23 00:00:00', '2017-08-31 00:00:00', 'admin', NULL),
(9, 'Neuvième Année', '9ème AF', 8, 1, '2014-09-23 00:00:00', '2017-08-31 00:00:00', 'admin', NULL),
(10, 'Secondaire 1', 'Sec I', 9, 2, '2014-09-23 00:00:00', '2017-08-31 00:00:00', 'admin', NULL),
(11, 'Secondaire 2', 'Sec 2', 10, 2, '2014-09-23 00:00:00', '2017-08-31 00:00:00', 'admin', NULL),
(12, 'Secondaire 3', 'Sec 3', 11, 2, '2014-09-23 00:00:00', '2017-08-31 00:00:00', 'admin', NULL),
(13, 'Secondaire 4', 'Sec 4', 12, 2, '2015-08-20 00:00:00', '2017-08-31 00:00:00', 'admin', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `level_has_person`
--

CREATE TABLE `level_has_person` (
  `id` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `loan_of_money`
--

CREATE TABLE `loan_of_money` (
  `id` int(11) NOT NULL,
  `loan_date` date NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `payroll_month` int(3) NOT NULL,
  `deduction_percentage` int(3) NOT NULL,
  `solde` double NOT NULL,
  `paid` int(1) NOT NULL DEFAULT '0',
  `number_of_month_repayment` int(5) NOT NULL,
  `remaining_month_number` int(5) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `mails`
--

CREATE TABLE `mails` (
  `id` int(11) NOT NULL,
  `sender` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
  `sender_name` varchar(255) DEFAULT NULL,
  `receivers` text CHARACTER SET utf8,
  `subject` varchar(255) CHARACTER SET utf8 NOT NULL,
  `message` text CHARACTER SET utf8 NOT NULL,
  `is_read` int(1) DEFAULT NULL,
  `id_sender` int(11) DEFAULT NULL,
  `date_sent` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `is_delete` int(11) DEFAULT NULL,
  `is_my_send` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `menfp_decision`
--

CREATE TABLE `menfp_decision` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `total_grade` double NOT NULL,
  `average` double DEFAULT NULL,
  `mention` varchar(100) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `menfp_grades`
--

CREATE TABLE `menfp_grades` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `menfp_exam` int(11) NOT NULL,
  `grade` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `modules`
--

CREATE TABLE `modules` (
  `id` int(11) NOT NULL,
  `module_short_name` varchar(64) NOT NULL,
  `module_name` varchar(64) NOT NULL,
  `mod_lateral_menu` varchar(255) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `modules`
--

INSERT INTO `modules` (`id`, `module_short_name`, `module_name`, `mod_lateral_menu`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES
(1, 'configuration', 'Paramètre Ecole', '//layouts/menuSchoolSetting', NULL, NULL, NULL, NULL),
(5, 'users', 'Utilisateur', '//layouts/menuUser', NULL, NULL, NULL, NULL),
(6, 'reports', 'Reports', '//layouts/menuReportManager', NULL, NULL, NULL, NULL),
(7, 'schoolconfig', 'Gestion académique', '//layouts/menuAcademicSetting', NULL, NULL, NULL, NULL),
(8, 'billings', 'Facturation', '//layouts/menuBilling', NULL, NULL, NULL, NULL),
(9, 'academic', 'Académique', '//layouts/menuStudentManager', NULL, NULL, NULL, NULL),
(10, 'guest', 'Invite', NULL, NULL, NULL, NULL, NULL),
(11, 'discipline', 'Discipline', '', NULL, NULL, NULL, NULL),
(12, 'portal', 'Portal', '', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `other_incomes`
--

CREATE TABLE `other_incomes` (
  `id` int(11) NOT NULL,
  `id_income_description` int(11) NOT NULL,
  `amount` double NOT NULL,
  `income_date` date NOT NULL,
  `academic_year` int(11) NOT NULL,
  `description` varchar(255) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `other_incomes_description`
--

CREATE TABLE `other_incomes_description` (
  `id` int(11) NOT NULL,
  `income_description` varchar(65) NOT NULL,
  `category` int(11) NOT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `partners`
--

CREATE TABLE `partners` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(200) NOT NULL,
  `phone` varchar(25) NOT NULL,
  `activity_field` varchar(200) NOT NULL,
  `contact` varchar(200) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `passing_grades`
--

CREATE TABLE `passing_grades` (
  `id` int(11) NOT NULL,
  `level` int(11) DEFAULT NULL,
  `course` int(11) DEFAULT NULL,
  `academic_period` int(11) NOT NULL,
  `minimum_passing` float NOT NULL,
  `level_or_course` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0: level, 1: course',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(20) DEFAULT NULL,
  `update_by` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payment_method`
--

CREATE TABLE `payment_method` (
  `id` int(11) NOT NULL,
  `method_name` varchar(45) NOT NULL,
  `description` text,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `payment_method`
--

INSERT INTO `payment_method` (`id`, `method_name`, `description`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'Virement bancaire', 'Le parent apporte la fiche bancaire. L\'économe enregistre le paiement dans le registre de paiement. Le parent de l\'élève doit payer intégralement pour que sa fiche soit acceptée. ', NULL, '2018-01-17 00:00:00', NULL, NULL),
(2, 'Cash', '', '2018-01-17 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `payroll`
--

CREATE TABLE `payroll` (
  `id` int(11) NOT NULL,
  `id_payroll_set` int(11) NOT NULL,
  `id_payroll_set2` int(11) DEFAULT NULL COMMENT 'recois l''id_payrollsetting professeur s''il est a la fois employe et professeur',
  `payroll_month` int(3) NOT NULL,
  `payroll_date` date NOT NULL,
  `missing_hour` int(11) NOT NULL,
  `taxe` double NOT NULL,
  `gross_salary` double NOT NULL,
  `percentage` double DEFAULT NULL,
  `net_salary` double NOT NULL,
  `payment_date` date NOT NULL,
  `cash_check` varchar(45) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_settings`
--

CREATE TABLE `payroll_settings` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `an_hour` tinyint(2) NOT NULL DEFAULT '0' COMMENT '0:no, 1:yes',
  `number_of_hour` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `as` int(2) DEFAULT '0' COMMENT '0: employee; 1: teacher',
  `old_new` tinyint(2) NOT NULL DEFAULT '1' COMMENT '0: old setting; 1: new setting',
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `created_by` varchar(65) NOT NULL,
  `updated_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `payroll_setting_taxes`
--

CREATE TABLE `payroll_setting_taxes` (
  `id` int(11) NOT NULL,
  `id_payroll_set` int(11) NOT NULL,
  `id_taxe` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `pending_balance`
--

CREATE TABLE `pending_balance` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `balance` double NOT NULL,
  `is_paid` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0: not yet; 1: paid',
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `persons`
--

CREATE TABLE `persons` (
  `id` int(11) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `gender` varchar(45) DEFAULT NULL,
  `blood_group` varchar(10) NOT NULL,
  `birthday` date DEFAULT NULL,
  `id_number` varchar(50) DEFAULT NULL,
  `is_student` tinyint(1) DEFAULT NULL,
  `adresse` varchar(255) DEFAULT NULL,
  `phone` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `nif_cin` varchar(100) NOT NULL,
  `cities` int(11) DEFAULT NULL,
  `citizenship` varchar(45) NOT NULL,
  `mother_first_name` varchar(55) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `identifiant` varchar(100) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `matricule` varchar(100) DEFAULT NULL COMMENT 'student for ( Ministere Edu. Nat)',
  `paid` tinyint(2) DEFAULT NULL COMMENT 'for admission list. 0: not yet paid; 1: already paid; NULL: left admission list ',
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL,
  `active` tinyint(1) DEFAULT NULL,
  `inactive_reason` tinyint(4) DEFAULT NULL COMMENT '1:abandon 2:expulsion 3:maladie 4:voyage',
  `image` varchar(50) DEFAULT NULL,
  `comment` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `persons`
--

INSERT INTO `persons` (`id`, `last_name`, `first_name`, `gender`, `blood_group`, `birthday`, `id_number`, `is_student`, `adresse`, `phone`, `email`, `nif_cin`, `cities`, `citizenship`, `mother_first_name`, `identifiant`, `matricule`, `paid`, `date_created`, `date_updated`, `create_by`, `update_by`, `active`, `inactive_reason`, `image`, `comment`) VALUES
(1, 'Super', 'Admin', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, '', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 2, NULL, NULL, '');

-- --------------------------------------------------------

--
-- Table structure for table `persons_has_titles`
--

CREATE TABLE `persons_has_titles` (
  `persons_id` int(11) NOT NULL,
  `titles_id` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `person_history`
--

CREATE TABLE `person_history` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `entry_hire_date` datetime DEFAULT NULL,
  `leaving_date` datetime NOT NULL,
  `reason` varchar(255) DEFAULT NULL,
  `disable_date` datetime NOT NULL,
  `title` varchar(100) DEFAULT NULL,
  `profil` varchar(32) NOT NULL,
  `job_status_name` varchar(100) NOT NULL,
  `last_level_name` varchar(70) DEFAULT NULL,
  `academic_year` varchar(70) NOT NULL,
  `created_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `postulant`
--

CREATE TABLE `postulant` (
  `id` int(11) NOT NULL,
  `first_name` varchar(120) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `gender` varchar(45) NOT NULL,
  `blood_group` varchar(10) NOT NULL,
  `birthday` date NOT NULL,
  `cities` int(11) DEFAULT NULL,
  `adresse` varchar(255) NOT NULL,
  `phone` varchar(45) NOT NULL,
  `health_state` varchar(255) NOT NULL,
  `person_liable` varchar(100) NOT NULL,
  `person_liable_phone` varchar(65) NOT NULL,
  `person_liable_adresse` varchar(255) NOT NULL,
  `person_liable_relation` int(11) DEFAULT NULL,
  `apply_for_level` int(11) NOT NULL,
  `previous_level` int(11) NOT NULL,
  `previous_school` varchar(255) NOT NULL,
  `school_date_entry` date NOT NULL,
  `last_average` double DEFAULT NULL,
  `status` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `product_name` varchar(128) NOT NULL,
  `type` int(11) NOT NULL,
  `description` text,
  `stock_alert` int(11) NOT NULL DEFAULT '0',
  `is_forsale` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `profil`
--

CREATE TABLE `profil` (
  `id` int(11) NOT NULL,
  `profil_name` varchar(64) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profil`
--

INSERT INTO `profil` (`id`, `profil_name`) VALUES
(1, 'Admin'),
(2, 'Manager'),
(3, 'Billing'),
(4, 'Teacher'),
(5, 'Guest'),
(6, 'Reporter'),
(7, 'Information');

-- --------------------------------------------------------

--
-- Table structure for table `profil_has_modules`
--

CREATE TABLE `profil_has_modules` (
  `id` int(11) NOT NULL,
  `profil_id` int(11) NOT NULL,
  `module_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `profil_has_modules`
--

INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES
(1, 1, 1),
(2, 1, 5),
(3, 1, 6),
(4, 1, 7),
(5, 1, 8),
(6, 1, 9),
(7, 1, 10),
(8, 2, 1),
(9, 2, 5),
(10, 2, 6),
(11, 2, 7),
(12, 2, 9),
(13, 2, 10),
(14, 3, 5),
(15, 3, 6),
(16, 3, 8),
(17, 4, 5),
(18, 4, 6),
(19, 4, 7),
(20, 4, 9),
(21, 5, 5),
(22, 5, 10),
(23, 6, 5),
(24, 6, 6),
(25, 1, 11),
(26, 2, 11),
(27, 2, 8),
(28, 3, 7),
(29, 3, 9),
(30, 1, 12),
(31, 2, 12),
(32, 4, 8),
(33, 7, 7),
(34, 7, 9),
(35, 7, 12),
(36, 7, 6),
(37, 7, 8),
(38, 7, 5),
(39, 3, 1),
(40, 3, 11),
(41, 3, 12),
(42, 6, 1),
(43, 6, 7),
(44, 6, 8),
(45, 6, 9),
(46, 6, 11),
(47, 6, 12),
(48, 7, 1),
(49, 7, 11);

-- --------------------------------------------------------

--
-- Table structure for table `qualifications`
--

CREATE TABLE `qualifications` (
  `id` int(11) NOT NULL,
  `qualification_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `raise_salary`
--

CREATE TABLE `raise_salary` (
  `id` int(11) NOT NULL,
  `person_id` int(11) NOT NULL,
  `amount` double NOT NULL,
  `raising_date` date NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_infraction`
--

CREATE TABLE `record_infraction` (
  `id` bigint(20) NOT NULL,
  `student` int(20) NOT NULL,
  `infraction_type` int(11) NOT NULL,
  `record_by` varchar(64) NOT NULL,
  `incident_date` date NOT NULL,
  `academic_period` int(11) DEFAULT NULL,
  `exam_period` int(11) DEFAULT NULL,
  `incident_description` text NOT NULL,
  `decision_description` text,
  `value_deduction` float DEFAULT NULL,
  `general_comment` text,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `create_by` varchar(65) DEFAULT NULL,
  `update_by` varchar(65) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `record_presence`
--

CREATE TABLE `record_presence` (
  `id` int(11) NOT NULL,
  `student` int(20) NOT NULL,
  `room` int(11) DEFAULT NULL,
  `academic_period` int(11) DEFAULT NULL,
  `exam_period` int(11) DEFAULT NULL,
  `date_record` date NOT NULL,
  `presence_type` int(11) NOT NULL,
  `comments` text,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `relations`
--

CREATE TABLE `relations` (
  `id` int(11) NOT NULL,
  `relation_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `relations`
--

INSERT INTO `relations` (`id`, `relation_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Père ', NULL, NULL, NULL, NULL),
(3, 'Mère', NULL, NULL, NULL, NULL),
(5, 'Autres', NULL, NULL, NULL, NULL),
(6, 'Tante', '2018-06-14 00:00:00', NULL, NULL, NULL),
(7, 'Soeur', '2018-06-14 00:00:00', NULL, NULL, NULL),
(8, 'Oncle', '2018-06-14 00:00:00', NULL, NULL, NULL),
(9, 'Frère', '2018-06-14 00:00:00', NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reportcard_observation`
--

CREATE TABLE `reportcard_observation` (
  `id` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `start_range` float NOT NULL,
  `end_range` float NOT NULL,
  `comment` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `reservation`
--

CREATE TABLE `reservation` (
  `id` int(11) NOT NULL,
  `postulant_student` int(11) NOT NULL,
  `is_student` tinyint(2) NOT NULL,
  `amount` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `already_checked` tinyint(2) NOT NULL DEFAULT '0',
  `comments` varchar(255) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(45) NOT NULL,
  `update_by` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `return_history`
--

CREATE TABLE `return_history` (
  `id` int(11) NOT NULL,
  `id_transaction` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `return_amount` float NOT NULL,
  `return_quantity` int(11) NOT NULL,
  `date_return` datetime NOT NULL,
  `return_by` varchar(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rooms`
--

CREATE TABLE `rooms` (
  `id` int(11) NOT NULL,
  `room_name` varchar(45) NOT NULL,
  `short_room_name` varchar(45) NOT NULL,
  `level` int(11) NOT NULL,
  `shift` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `room_has_person`
--

CREATE TABLE `room_has_person` (
  `id` int(11) NOT NULL,
  `room` int(11) NOT NULL,
  `students` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `rpt_custom`
--

CREATE TABLE `rpt_custom` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL,
  `data` text NOT NULL,
  `parameters` text,
  `academic_year` int(11) DEFAULT NULL,
  `categorie` int(11) NOT NULL,
  `variables` text,
  `setup_variable` text,
  `create_by` varchar(64) NOT NULL,
  `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rpt_custom`
--

INSERT INTO `rpt_custom` (`id`, `title`, `data`, `parameters`, `academic_year`, `categorie`, `variables`, `setup_variable`, `create_by`, `create_date`) VALUES
(1, 'Liste des matieres par année académique et par classe', 'SELECT DISTINCT(s.subject_name) AS \'Nom matière\', s.short_subject_name AS \'Code matière\' FROM courses c INNER JOIN subjects s ON (s.id = c.subject) INNER JOIN rooms r ON (r.id = c.room) INNER JOIN levels l ON (l.id = r.level) \r\nWHERE academic_period = {%annee_academique%} AND l.id = {%classe%}                                                                        ', '', NULL, 4, 'annee_academique,classe', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC\",\"dynamic-combo1\":\"SELECT id,level_name FROM `levels` ORDER BY id ASC\"}}', '_developer_', '2018-05-12 22:54:54'),
(2, 'Liste des cours par année académique et par salle', ' SELECT DISTINCT(s.subject_name) AS \'Nom matière\', s.short_subject_name AS \'Code matière\', CONCAT(p.first_name,\' \',p.last_name) AS \'Nom Professeurs\', IF(p.gender=1,\'Féminin\',\'Masculin\') AS \'SEXE\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN subjects s ON (s.id = c.subject) \r\nINNER JOIN rooms r ON (r.id = c.room) \r\nWHERE academic_period = {%annee_academique%} AND r.id = {%salle%}      \r\nORDER BY s.subject_name ASC                                                                 ', '', NULL, 4, 'annee_academique,salle', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC\",\"dynamic-combo1\":\"SELECT id,short_room_name FROM rooms ORDER BY short_room_name\"}}', '_developer_', '2018-05-12 17:09:21'),
(3, 'Liste des cours par année académique, par salle et par coefficient', 'SELECT DISTINCT(s.subject_name) AS \'Nom matière\', s.short_subject_name AS \'Code matière\',c.weight AS \'Coefficient\', CONCAT(p.first_name,\' \',p.last_name) AS \'Nom Professeurs\', IF(p.gender=1,\'Féminin\',\'Masculin\') AS \'SEXE\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN subjects s ON (s.id = c.subject) \r\nINNER JOIN rooms r ON (r.id = c.room) \r\nWHERE academic_period = {%annee_academique%} AND r.id = {%salle%} AND c.weight = {%coefficient%}                                                                     ', '', NULL, 4, 'annee_academique,salle,coefficient', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\",\"txt\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"static-combo2\":\"\",\"dynamic-combo0\":\"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC\",\"dynamic-combo1\":\"SELECT id, room_name FROM rooms\",\"dynamic-combo2\":\"\"}}', '_developer_', '2018-05-16 13:05:18'),
(4, 'Nombre de cours total pour une année donnée', 'SELECT count(c.id) AS \'Nombre de cours\' FROM courses c \r\nWHERE c.academic_period = {%annee_academique%}', '', NULL, 4, 'annee_academique', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC\"}}', '_developer_', '2018-05-16 14:37:23'),
(5, 'Nombre de cours par année académique et par salle', 'SELECT count(c.id) AS \'Nombre de cours\' FROM courses c \r\nINNER JOIN rooms r ON (r.id = c.room) \r\nWHERE c.academic_period = {%annee_academique%} AND r.id = {%salle%}                                                                             ', '', NULL, 4, 'annee_academique,salle', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id,name_period FROM academicperiods WHERE is_year = 1 ORDER BY id DESC\",\"dynamic-combo1\":\"SELECT id, room_name FROM rooms\"}}', '_developer_', '2018-05-16 20:47:59'),
(6, 'Liste des professeurs actifs par classe pour l\'année en cours', 'SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', l.short_level_name AS \'Classe\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end AND l.id = {%classe%}    ORDER BY Nom ASC                                                                                                                                                                   ', '', NULL, 5, 'classe', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id, level_name FROM levels\"}}', '_developer_', '2018-05-16 15:14:33'),
(7, 'Liste de tous les professeurs', 'SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\' , l.level_name AS \'Classe\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end\r\nORDER BY l.level_name                                                                                                                           ', '', NULL, 5, '', NULL, '_developer_', '2018-05-17 00:26:58'),
(8, 'Liste éleves par statut pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\')  AS \'Sexe\', DATE_FORMAT(p.birthday,\"%e %M %Y\" ) AS \'Date de naissance\'  FROM persons p \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id)\r\nINNER JOIN academicperiods a ON (a.id = lhp.academic_year) \r\nWHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active  = {%statut%} ORDER BY p.last_name ASC                                                                                                                                                                                                                       ', '', NULL, 1, 'statut', '{\"data_type\":[\"static-combobox\"],\"param_value\":{\"static-combo0\":\"2:Nouveau,1:Actif,0:Inactif\",\"dynamic-combo0\":\"\"}}', '_developer_', '2018-07-05 12:33:58'),
(9, 'Liste élèves actifs par salle pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\')  AS \'Sexe\', p.phone AS \'Téléphone\', r.short_room_name AS \'Salle\'  FROM persons p \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id)\r\nINNER JOIN academicperiods a ON (a.id = lhp.academic_year) \r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id) \r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nWHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active  IN (1,2)  AND rhp.room = {%salle%}  ORDER BY p.last_name ASC                                        ', '', NULL, 1, 'salle', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id, short_room_name FROM `rooms` order BY short_room_name ASC\"}}', '_developer_', '2018-07-05 13:51:12'),
(10, 'Moyenne par période et par salle pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\', r.short_room_name AS \'Salle\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  \r\nINNER JOIN persons p ON (p.id = abp.student)\r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE eby.academic_year = {%periode%} AND r.id = {%salle%}                                                                                                ', '', NULL, 1, 'periode,salle', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\",\"dynamic-combo1\":\"SELECT id,short_room_name FROM rooms ORDER BY short_room_name\"}}', '_developer_', '2018-07-05 14:42:32'),
(11, 'Moyenne par periode et par classe pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\',  l.short_level_name AS \'Classe\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  \r\nINNER JOIN persons p ON (p.id = abp.student)\r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE eby.academic_year = {%periode%} AND l.id = {%classe%}                                                                                               ', '', NULL, 1, 'periode,classe', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\",\"dynamic-combo1\":\"SELECT id,short_level_name FROM `levels` ORDER BY short_level_name\"}}', '_developer_', '2018-07-05 11:34:13'),
(12, 'Moyenne entre une intervalle par classe et par periode pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\',  l.short_level_name AS \'Classe\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  INNER JOIN persons p ON (p.id = abp.student)INNER JOIN room_has_person rhp ON (rhp.students = p.id)INNER JOIN rooms r ON (r.id = rhp.room)INNER JOIN levels l ON (l.id = r.level)INNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) INNER JOIN academicperiods a ON (a.id = eby.academic_year)WHERE eby.academic_year = {%periode%} AND l.id = {%classe%}   AND abp.average BETWEEN {%moyenne_minimale%} AND {%moyenne_maximale%}                                               ', '', NULL, 1, 'periode,classe,moyenne_minimale,moyenne_maximale', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\",\"txt\",\"txt\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"static-combo2\":\"\",\"static-combo3\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\",\"dynamic-combo1\":\"SELECT id,short_level_name FROM `levels` ORDER BY short_level_name\",\"dynamic-combo2\":\"\",\"dynamic-combo3\":\"\"}}', '_developer_', '2018-07-18 15:59:33'),
(13, 'Liste d\'elèves par salle, par matière, par période et sur condition d\'une note x pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\',\'Féminin\') AS \'Sexe\', s.subject_name AS \'Matière\', g.grade_value AS \'Note\', c.weight AS \'Coefficient\', a.name_period AS \'Période\', r.short_room_name AS \'Salle\'\r\nFROM grades g \r\nINNER JOIN persons p ON (p.id = g.student)\r\nINNER JOIN courses c ON (c.id = g.course) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN subjects s ON (s.id = c.subject) \r\nINNER JOIN evaluation_by_year eby ON (eby.id = g.evaluation) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE r.id = {%salle%}  AND s.id = {%matiere%} AND a.id = {%periode%} AND grade_value {%condition%}  {%note%}                                                                                     ', '', NULL, 1, 'salle,matiere,periode,condition,note', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\",\"dynamic-combobox\",\"static-combobox\",\"txt\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"static-combo2\":\"\",\"static-combo3\":\"=:Egale,>:Sup\\u00e9rieure,<:Inf\\u00e9rieure,>=:Sup\\u00e9rieure ou \\u00e9gale,<=:Inf\\u00e9rieure ou \\u00e9gale\",\"static-combo4\":\"\",\"dynamic-combo0\":\"SELECT id, short_room_name FROM `rooms` order BY short_room_name ASC\",\"dynamic-combo1\":\"SELECT id, subject_name FROM subjects ORDER BY subject_name ASC\",\"dynamic-combo2\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\",\"dynamic-combo3\":\"\",\"dynamic-combo4\":\"\"}}', '_developer_', '2018-08-04 10:58:49'),
(14, 'Liste d\'elèves  par période et sur condition d\'une moyenne x pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender = 0, \'Masculin\', \'Féminin\') AS \'Sexe\',  l.short_level_name AS \'Classe\', a.name_period AS \'Période\' , abp.average AS \'Moyenne\' FROM  `average_by_period` abp  \r\nINNER JOIN persons p ON (p.id = abp.student)\r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nWHERE eby.academic_year = {%periode%} AND  abp.average {%condition%}  {%valeur_moyenne%}                                                                                                                      ', '', NULL, 1, 'periode,condition,valeur_moyenne', '{\"data_type\":[\"dynamic-combobox\",\"static-combobox\",\"txt\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"=:Egale,>:Sup\\u00e9rieure,<:Inf\\u00e9rieure,>=:Sup\\u00e9rieure ou \\u00e9gale,<=:Inf\\u00e9rieure ou \\u00e9gale\",\"static-combo2\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\",\"dynamic-combo1\":\"\",\"dynamic-combo2\":\"\"}}', '_developer_', '2018-07-21 18:41:17'),
(15, 'Liste de décision finale par année académique et par classe', 'SELECT UPPER(p.last_name) AS \'Nom\', p.first_name AS \'Prénom\', IF(p.gender = 0, \'Masculin\',\'Féminin\') AS \'Sexe\', DATE_FORMAT(p.birthday,\"%e %M %Y\") AS \'Date de naissance\', c.city_name AS \'Lieu de naissance\', df.general_average AS \'Moyenne générale\', l.short_level_name AS \'Classe\', df.mention AS \'Mention\'  FROM decision_finale df \r\nINNER JOIN persons p ON (p.id = df.student) \r\nINNER JOIN cities c ON (c.id = p.cities)\r\nINNER JOIN levels l ON (df.current_level = l.id)\r\nWHERE df.academic_year = {%annee_academique%} AND df.current_level = {%classe%};                                                                                                                                                                     ', '', NULL, 1, 'annee_academique,classe', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1 ORDER BY id DESC\",\"dynamic-combo1\":\"SELECT id, level_name FROM levels\"}}', '_developer_', '2018-07-14 15:08:19'),
(16, 'Tri des élèves désactivés par motif pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\', DATE_FORMAT(p.birthday,\"%e %M %Y\" ) AS \'Date de naissance\' FROM persons p INNER JOIN level_has_person lhp ON (lhp.students = p.id) INNER JOIN academicperiods a ON (a.id = lhp.academic_year) WHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active = 0 and inactive_reason={%motif%} ORDER BY p.last_name ASC                                                                                                                                                                         ', '', NULL, 1, 'motif', '{\"data_type\":[\"static-combobox\"],\"param_value\":{\"static-combo0\":\"1:Abandon,2:Expulsion,3:Maladie,4:Voyage\",\"dynamic-combo0\":\"\"}}', '_developer_', '2018-07-18 07:38:46'),
(17, 'Liste éleves désactivés pour l\'année en cours', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\',  l.level_name AS \'Classe\', CASE WHEN inactive_reason =1 THEN \'Abandon\' WHEN inactive_reason =2 THEN \'Expulsion\' WHEN inactive_reason =3 THEN \'Maladie\'  WHEN inactive_reason =4 THEN \'Voyage\'  ELSE NULL END AS \'Motif\' FROM persons p INNER JOIN level_has_person lhp ON (lhp.students = p.id) INNER JOIN levels l ON (lhp.level = l.id) INNER JOIN academicperiods a ON (a.id = lhp.academic_year) WHERE NOW() BETWEEN a.date_start AND a.date_end AND p.active = 0 ORDER BY p.last_name ASC                                                                                                                                                                                                                                                                                                                                                 ', '', NULL, 1, '', NULL, '_developer_', '2018-07-18 08:11:02'),
(18, 'Liste élèves en retard de paiement par année académique', 'SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\',  IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\', SUM(ba.balance) AS \'Montant\', l.short_level_name AS \'Classe\' , a.name_period AS \'Année académique\'  FROM persons p INNER JOIN balance ba ON (ba.student = p.id)\r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id)\r\nINNER JOIN levels l ON (l.id = lhp.level) \r\nINNER JOIN academicperiods a ON (a.id = lhp.academic_year) \r\nWHERE ba.balance > 0 AND lhp.academic_year = {%annee_academique%} \r\nGROUP BY p.id ', '', NULL, 6, 'annee_academique', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1 ORDER BY id DESC\"}}', '_developer_', '2018-07-23 17:53:28'),
(19, 'Liste élèves en retard de paiement par salle par année académique', ' SELECT p.first_name AS \'Prénom\', p.last_name AS \'Nom\', IF(p.gender=0,\'Masculin\',\'Féminin\') AS \'Sexe\', SUM(ba.balance) AS \'Montant\', r.short_room_name AS \'Salle\', a.name_period AS \'Année académique\' FROM persons p \r\nINNER JOIN balance ba ON (ba.student = p.id) \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id) \r\nINNER JOIN levels l ON (l.id = lhp.level) \r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN academicperiods a ON (a.id = lhp.academic_year) \r\nWHERE ba.balance > 0 AND lhp.academic_year = {%annee_academique%} AND r.id = {%salle%} GROUP BY p.id                                                                                                                                                                  ', '', NULL, 6, 'salle,annee_academique', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id, room_name FROM `rooms` \",\"dynamic-combo1\":\"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1  ORDER BY id DESC\"}}', '_developer_', '2018-07-23 20:17:19'),
(21, 'Total recette-scolarite entre 2 dates', 'SELECT SUM(amount_pay) AS \'Total recette scolarite\' FROM `billings` b INNER JOIN fees f ON(b.fee_period=f.id) INNER JOIN fees_label fl ON(f.fee=fl.id) WHERE fl.status=1 AND date_pay BETWEEN \'{%date_debut%}\' AND \'{%date_fin%}\'                                                 ', '', NULL, 6, 'date_debut,date_fin', '{\"data_type\":[\"date\",\"date\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"\",\"dynamic-combo1\":\"\"}}', '_developer_', '2018-07-18 09:34:15'),
(22, 'Total autres-recettes entre 2 dates', 'SELECT SUM(amount) AS \'Total autres-recettes \' FROM `other_incomes` oi INNER JOIN other_incomes_description oid ON(oi.id_income_description=oid.id) WHERE  income_date BETWEEN \'{%date_debut%}\' AND \'{%date_fin%}\'                         ', '', NULL, 6, 'date_debut,date_fin', '{\"data_type\":[\"date\",\"date\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"\",\"dynamic-combo1\":\"\"}}', '_developer_', '2018-07-18 09:44:02'),
(23, 'Total recette-scolarite par versement et par année académiquee', 'SELECT fl.fee_label AS \'Versement\', SUM(amount_pay) AS \'Total recette scolarite\' FROM `billings` b INNER JOIN fees f ON(b.fee_period=f.id) INNER JOIN fees_label fl ON(f.fee=fl.id) WHERE fl.status=1 AND fl.id={%versement%} AND b.academic_year={%annee_academique%}                             ', '', NULL, 6, 'versement,annee_academique', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id, fee_label FROM `fees_label` WHERE status = 1  AND fee_label NOT LIKE \'Pending balance\' \",\"dynamic-combo1\":\"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1  ORDER BY id DESC\"}}', '_developer_', '2018-07-19 22:16:04'),
(24, 'Total recette-scolarite par mois et par année académiquee', 'SELECT CASE WHEN MONTH(date_pay) =1 THEN \'Janvier\' WHEN MONTH(date_pay) =2 THEN \'Février\' WHEN MONTH(date_pay) =3 THEN \'Mars\'  WHEN MONTH(date_pay) =4 THEN \'Avril\'   WHEN MONTH(date_pay) =5 THEN \'MAi\'  WHEN MONTH(date_pay) =6 THEN \'Juin\'  WHEN MONTH(date_pay) =7 THEN \'Juillet\'  WHEN MONTH(date_pay) =8 THEN \'Août\'  WHEN MONTH(date_pay) =9 THEN \'Septembre\'  WHEN MONTH(date_pay) =10 THEN \'Octobre\'  WHEN MONTH(date_pay) =11 THEN \'Novembre\'  WHEN MONTH(date_pay) =12 THEN \'Décembre\' ELSE NULL END AS \'MOIS\', SUM(amount_pay) AS \'Total recette scolarite\' FROM `billings` b INNER JOIN fees f ON(b.fee_period=f.id) INNER JOIN fees_label fl ON(f.fee=fl.id) WHERE fl.status=1 AND MONTH(date_pay)={%mois%} AND b.academic_year={%annee_academique%}                                                 ', '', NULL, 6, 'mois,annee_academique', '{\"data_type\":[\"static-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"1:Janvier,2:F\\u00e9vrier,3:Mars,4:Avril,5:Mai,6:Juin,7:Juillet,8:Ao\\u00fbt,9:Septembre,10:Octobre,11:Novembre,12:D\\u00e9cembre\",\"static-combo1\":\"\",\"dynamic-combo0\":\"\",\"dynamic-combo1\":\"SELECT id, name_period FROM `academicperiods` WHERE is_year = 1  ORDER BY id DESC\"}}', '_developer_', '2018-07-18 10:12:05'),
(25, 'Liste des plus fortes moyennes des salles par période pour l\'année en cours', 'SELECT  r.short_room_name AS \'Salle\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', a.name_period AS \'Période\', abp.average AS \'Moyenne\' FROM persons p \r\nINNER JOIN average_by_period abp ON (abp.student = p.id) \r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nINNER JOIN \r\n(\r\n    SELECT student, max(average) max_average \r\n    FROM average_by_period av INNER JOIN room_has_person rop ON (rop.students = av.student) \r\n    INNER JOIN evaluation_by_year e ON (e.id = av.evaluation_by_year) \r\n	INNER JOIN academicperiods a ON (a.id = e.academic_year)\r\n    WHERE e.academic_year = {%periode%}\r\n    GROUP BY rop.room\r\n)\r\nc ON  abp.average = c.max_average\r\nWHERE eby.academic_year = {%periode%}                                                                                                                                                                                                                                                                                                                               ', '', NULL, 1, 'periode', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\"}}', '_developer_', '2018-07-23 17:21:43'),
(26, 'Liste des plus fortes moyennes des classes par période pour l\'année en cours', 'SELECT  l.short_level_name AS \'Classe\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', a.name_period AS \'Période\', abp.average AS \'Moyenne\' FROM persons p \r\nINNER JOIN average_by_period abp ON (abp.student = p.id) \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id)\r\nINNER JOIN levels l ON (l.id = lhp.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nINNER JOIN \r\n(\r\n    SELECT student, max(average) max_average \r\n    FROM average_by_period av INNER JOIN level_has_person lep ON (lep.students = av.student) \r\n    INNER JOIN evaluation_by_year e ON (e.id = av.evaluation_by_year) \r\n    INNER JOIN academicperiods a ON (a.id = e.academic_year)\r\n    WHERE e.academic_year = {%periode%}\r\n    GROUP BY lep.level\r\n)\r\nc ON  abp.average = c.max_average\r\nWHERE eby.academic_year = {%periode%}', '', NULL, 1, 'periode', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\"}}', '_developer_', '2018-07-23 17:48:13'),
(27, 'Liste des plus faibles moyennes des classes par période pour l\'année en cours', 'SELECT  l.short_level_name AS \'Classe\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', a.name_period AS \'Période\', abp.average AS \'Moyenne\' FROM persons p \r\nINNER JOIN average_by_period abp ON (abp.student = p.id) \r\nINNER JOIN level_has_person lhp ON (lhp.students = p.id)\r\nINNER JOIN levels l ON (l.id = lhp.level)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nINNER JOIN \r\n(\r\n    SELECT student, min(average) min_average \r\n    FROM average_by_period av INNER JOIN level_has_person lep ON (lep.students = av.student) \r\n    INNER JOIN evaluation_by_year e ON (e.id = av.evaluation_by_year) \r\n    INNER JOIN academicperiods a ON (a.id = e.academic_year)\r\n    WHERE e.academic_year = {%periode%}\r\n    GROUP BY lep.level\r\n)\r\nc ON  abp.average = c.min_average\r\nWHERE eby.academic_year = {%periode%}                        ', '', NULL, 1, 'periode', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\"}}', '_developer_', '2018-07-23 17:51:31'),
(28, 'Liste des plus faibles moyennes des salles par période pour l\'année en cours', 'SELECT  r.short_room_name AS \'Salle\', p.first_name AS \'Prénom\', p.last_name AS \'Nom\', a.name_period AS \'Période\', abp.average AS \'Moyenne\' FROM persons p \r\nINNER JOIN average_by_period abp ON (abp.student = p.id) \r\nINNER JOIN room_has_person rhp ON (rhp.students = p.id)\r\nINNER JOIN rooms r ON (r.id = rhp.room)\r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year) \r\nINNER JOIN academicperiods a ON (a.id = eby.academic_year)\r\nINNER JOIN \r\n(\r\n    SELECT student, min(average) min_average \r\n    FROM average_by_period av INNER JOIN room_has_person rop ON (rop.students = av.student) \r\n    INNER JOIN evaluation_by_year e ON (e.id = av.evaluation_by_year) \r\n	INNER JOIN academicperiods a ON (a.id = e.academic_year)\r\n    WHERE e.academic_year = {%periode%}\r\n    GROUP BY rop.room\r\n)\r\nc ON  abp.average = c.min_average\r\nWHERE eby.academic_year = {%periode%}                                                                                                                                                                                                                                                                                                                                                       ', '', NULL, 1, 'periode', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\"}}', '_developer_', '2018-07-23 17:31:39'),
(29, 'Liste des professeurs actifs par salle pour l\'année en cours', 'SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', r.short_room_name AS \'Salle\' FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end AND r.id = {%salle%}    ORDER BY Nom ASC                            ', '', NULL, 5, 'salle', '{\"data_type\":[\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"dynamic-combo0\":\"SELECT id, room_name FROM rooms\"}}', '_developer_', '2018-07-21 15:07:44'),
(30, 'Liste des professeurs inactifs pour l\'année en cours', '                            SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', if(p.active=0,\'Inactif\',\'Actif\') AS \'Statut\'  FROM courses c \r\nINNER JOIN persons p ON (p.id = c.teacher) \r\nINNER JOIN rooms r ON (r.id = c.room)\r\nINNER JOIN levels l ON (l.id = r.level)\r\nINNER JOIN academicperiods ap ON (ap.id = c.academic_period) \r\nWHERE NOW() BETWEEN ap.date_start AND ap.date_end AND p.active=0\r\n                                                   ', '', NULL, 5, '', NULL, '_developer_', '2018-07-21 15:16:17'),
(31, 'Liste des employés inactifs ', '                             SELECT DISTINCT(p.first_name) AS \'Prénom\', p.last_name AS \'Nom\', if(p.gender=1,\'Féminin\',\'Masculin\') AS \'Sexe\', if(p.active=0,\'Inactif\',\'Actif\') AS \'Statut\'  FROM persons p \r\nWHERE p.id NOT IN(SELECT teacher from courses) AND p.active=0                        ', '', NULL, 5, '', NULL, '_developer_', '2018-07-21 15:20:18'),
(32, 'Taux de reussite par salle et par période pour une année académique donnée', 'SELECT r.short_room_name AS \'Salle\', COUNT(abp.student) AS Effectif, \r\n(SELECT COUNT(moy.student) FROM average_by_period moy \r\n INNER JOIN room_has_person rhp1 ON (rhp1.students = moy.student)  INNER JOIN rooms rm ON (rm.id = rhp1.room)\r\n INNER JOIN evaluation_by_year ebi ON (ebi.id = moy.evaluation_by_year)\r\n WHERE moy.average >= \r\n(SELECT  minimum_passing FROM passing_grades  pg INNER JOIN rooms ro ON (ro.level = pg.level)  WHERE ro.id = r.id GROUP BY r.short_room_name) \r\n AND ebi.academic_year  = {%periode%}  AND rm.id = r.id\r\n \r\n) AS Succes, CONCAT(ROUND(((SELECT COUNT(moy.student) FROM average_by_period moy \r\n INNER JOIN room_has_person rhp1 ON (rhp1.students = moy.student)  INNER JOIN rooms rm ON (rm.id = rhp1.room) \r\n INNER JOIN evaluation_by_year ebi ON (ebi.id = moy.evaluation_by_year)\r\n WHERE moy.average >= \r\n(SELECT  minimum_passing FROM passing_grades  pg INNER JOIN rooms ro ON (ro.level = pg.level)  WHERE ro.id = r.id GROUP BY r.short_room_name) \r\n AND ebi.academic_year  = {%periode%} AND rm.id = r.id \r\n \r\n)/COUNT(abp.student))*100,2),\'%\') AS \'Taux de réussite\'\r\nFROM average_by_period abp INNER JOIN room_has_person rhp ON (rhp.students = abp.student)  INNER JOIN rooms r ON (r.id = rhp.room) \r\nINNER JOIN evaluation_by_year eby ON (eby.id = abp.evaluation_by_year)\r\nWHERE eby.academic_year  =  {%periode%}  AND abp.academic_year = {%annee_academique%}\r\nGROUP BY r.short_room_name                                                                        ', '', NULL, 1, 'periode,annee_academique', '{\"data_type\":[\"dynamic-combobox\",\"dynamic-combobox\"],\"param_value\":{\"static-combo0\":\"\",\"static-combo1\":\"\",\"static-combo2\":\"\",\"static-combo3\":\"\",\"dynamic-combo0\":\"SELECT id, name_period FROM `academicperiods` WHERE year = (SELECT id FROM academicperiods WHERE NOW() BETWEEN date_start AND date_end AND is_year = 1) AND is_year =0 ORDER BY name_period\",\"dynamic-combo1\":\"SELECT id, name_period FROM academicperiods WHERE is_year = 1\"}}', 'developer', '2018-07-23 15:41:57');

-- --------------------------------------------------------

--
-- Table structure for table `rpt_custom_cat`
--

CREATE TABLE `rpt_custom_cat` (
  `id` int(11) NOT NULL,
  `categorie_name` varchar(255) NOT NULL,
  `cat` varchar(32) DEFAULT NULL,
  `date_create` datetime NOT NULL,
  `date_update` datetime NOT NULL,
  `create_by` varchar(255) NOT NULL,
  `update_by` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `rpt_custom_cat`
--

INSERT INTO `rpt_custom_cat` (`id`, `categorie_name`, `cat`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES
(1, 'Elèves', 'stud', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(4, 'Matières et Cours', 'mec', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(5, 'Staff', 'prof', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', ''),
(6, 'Economat', 'eco', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `sale_transaction`
--

CREATE TABLE `sale_transaction` (
  `id` int(11) NOT NULL,
  `id_transaction` int(11) NOT NULL,
  `amount_sale` float NOT NULL,
  `discount` float DEFAULT NULL,
  `amount_receive` float NOT NULL,
  `amount_balance` float NOT NULL,
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scalendar`
--

CREATE TABLE `scalendar` (
  `id` int(11) NOT NULL,
  `c_title` varchar(255) DEFAULT NULL,
  `location` varchar(200) DEFAULT NULL,
  `description` varchar(1000) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `is_all_day_event` smallint(6) NOT NULL,
  `color` varchar(200) DEFAULT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE `schedules` (
  `id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `day_course` varchar(32) NOT NULL,
  `time_start` time NOT NULL,
  `time_end` time NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `schedule_agenda`
--

CREATE TABLE `schedule_agenda` (
  `id` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `c_description` varchar(255) DEFAULT NULL,
  `start_date` date DEFAULT NULL,
  `end_date` date DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `is_all_day_event` smallint(6) NOT NULL DEFAULT '0',
  `color` varchar(200) DEFAULT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `scholarship_holder`
--

CREATE TABLE `scholarship_holder` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `partner` int(11) DEFAULT NULL,
  `fee` int(11) DEFAULT NULL COMMENT 'Do not let NULL value if it is not a whole scholarship please specify',
  `percentage_pay` double NOT NULL,
  `is_internal` tinyint(1) NOT NULL DEFAULT '0',
  `academic_year` int(11) NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(11) NOT NULL,
  `section_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `section_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Fondamental', '2014-09-23 00:00:00', '2014-09-23 00:00:00', NULL, NULL),
(2, 'Secondaire', '2014-09-23 00:00:00', '2015-08-20 02:08:20', NULL, NULL),
(3, 'Préscolaire', '2015-08-29 00:00:00', '2015-08-29 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `section_has_cycle`
--

CREATE TABLE `section_has_cycle` (
  `id` int(11) NOT NULL,
  `cycle` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `level` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sellings`
--

CREATE TABLE `sellings` (
  `id` int(11) NOT NULL,
  `transaction_id` int(11) NOT NULL,
  `id_products` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `selling_date` datetime NOT NULL,
  `client_name` varchar(128) DEFAULT NULL,
  `sell_by` varchar(64) DEFAULT NULL,
  `amount_receive` float DEFAULT NULL,
  `amount_selling` float DEFAULT NULL,
  `amount_balance` float DEFAULT NULL,
  `discount` float DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `update_date` datetime DEFAULT NULL,
  `unit_selling_price` float DEFAULT NULL,
  `is_return` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `session`
--

CREATE TABLE `session` (
  `id` char(32) NOT NULL,
  `expire` int(11) DEFAULT NULL,
  `data` longblob,
  `user_id` int(11) NOT NULL,
  `last_activity` datetime NOT NULL,
  `last_ip` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `shifts`
--

CREATE TABLE `shifts` (
  `id` int(11) NOT NULL,
  `shift_name` varchar(45) NOT NULL,
  `time_start` time DEFAULT NULL,
  `time_end` time DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shifts`
--

INSERT INTO `shifts` (`id`, `shift_name`, `time_start`, `time_end`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Matin', '07:00:00', '16:10:00', '2014-09-23 00:00:00', '2018-06-14 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `siges_payment`
--

CREATE TABLE `siges_payment` (
  `id` int(11) NOT NULL,
  `id_siges_payment_set` int(11) NOT NULL,
  `amount_pay` double NOT NULL,
  `balance` double NOT NULL,
  `payment_method` int(11) NOT NULL,
  `payment_date` date NOT NULL,
  `comment` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `siges_payment_set`
--

CREATE TABLE `siges_payment_set` (
  `id` int(11) NOT NULL,
  `old_balance` double NOT NULL,
  `amount_to_pay` double NOT NULL,
  `devise` int(11) DEFAULT NULL,
  `display_on` date DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `id` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `acquisition_date` date NOT NULL,
  `buiying_price` float DEFAULT NULL,
  `selling_price` float DEFAULT NULL,
  `is_donation` tinyint(1) DEFAULT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_create` datetime DEFAULT NULL,
  `date_update` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `stock_history`
--

CREATE TABLE `stock_history` (
  `id` int(11) NOT NULL,
  `id_stock` int(11) NOT NULL,
  `id_product` int(11) NOT NULL,
  `quantity` int(11) NOT NULL,
  `buying_date` date NOT NULL,
  `buying_price` float NOT NULL,
  `selling_price` float NOT NULL,
  `create_by` varchar(64) NOT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_documents`
--

CREATE TABLE `student_documents` (
  `id` int(11) NOT NULL,
  `id_student` int(11) NOT NULL,
  `file_name` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `student_other_info`
--

CREATE TABLE `student_other_info` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `school_date_entry` date DEFAULT NULL,
  `leaving_date` datetime NOT NULL,
  `previous_school` varchar(255) DEFAULT NULL,
  `previous_level` varchar(45) DEFAULT NULL,
  `apply_for_level` varchar(45) DEFAULT NULL,
  `health_state` varchar(255) NOT NULL,
  `father_full_name` varchar(45) NOT NULL,
  `mother_full_name` varchar(100) NOT NULL,
  `person_liable` varchar(100) NOT NULL,
  `person_liable_phone` varchar(65) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime NOT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(11) NOT NULL,
  `subject_name` varchar(45) NOT NULL,
  `short_subject_name` varchar(5) DEFAULT NULL,
  `is_subject_parent` tinyint(1) DEFAULT NULL,
  `subject_parent` int(11) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL COMMENT '	',
  `create_by` varchar(45) DEFAULT NULL COMMENT '	',
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `subject_name`, `short_subject_name`, `is_subject_parent`, `subject_parent`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Mathématiques', 'math', 1, NULL, '2014-10-04 01:10:10', '2017-10-10 00:00:00', NULL, NULL),
(2, 'Lettres', 'let', 1, NULL, '2014-11-03 07:11:09', '2017-10-10 00:00:00', NULL, NULL),
(4, 'Algèbre', 'Alg', 0, 1, '2014-10-04 01:10:49', '2017-10-10 00:00:00', NULL, NULL),
(8, 'Biologie', 'biol', 0, NULL, '2014-10-11 11:10:56', '2017-10-10 00:00:00', NULL, NULL),
(9, 'Sciences physiques ', 'ScPh', 0, 43, '2014-10-11 11:10:11', '2017-10-10 00:00:00', NULL, NULL),
(10, 'Sciences sociales', 'ScSoc', 1, NULL, '2014-10-11 11:10:06', '2017-10-10 00:00:00', NULL, NULL),
(11, 'Histoire', 'hist', 0, 10, '2014-10-11 11:10:59', '2017-10-10 00:00:00', NULL, NULL),
(12, 'Géographie', 'geog', 0, 10, '2014-10-11 11:10:50', '2017-10-10 00:00:00', NULL, NULL),
(15, 'Chimie', 'chim', 0, NULL, '2014-10-11 11:10:59', '2017-10-10 00:00:00', NULL, NULL),
(16, 'Discipline', 'disci', 0, NULL, '2014-10-11 11:10:15', '2017-10-10 00:00:00', NULL, NULL),
(18, 'Trigonométrie ', 'trigo', 0, 1, '2014-10-11 11:10:52', '2017-10-10 00:00:00', NULL, NULL),
(21, 'Analyse linéaire', 'anall', 0, 1, '2014-10-11 12:10:02', '2017-10-10 00:00:00', NULL, NULL),
(22, 'Analyse combinatoire', 'analc', 0, 1, '2014-10-11 12:10:41', '2017-10-10 00:00:00', NULL, NULL),
(23, 'Physiques', 'phys', 1, NULL, '2014-10-16 05:10:30', '2017-10-10 00:00:00', NULL, NULL),
(24, 'Espagnol', 'esp', 0, 25, '2014-10-04 01:10:44', '2017-10-10 00:00:00', NULL, NULL),
(25, 'Langues', 'lang', 1, NULL, '2014-10-04 01:10:47', '2017-10-10 00:00:00', NULL, NULL),
(26, 'Créole', 'creo', 0, 25, '2014-10-11 11:10:20', '2017-10-10 00:00:00', NULL, NULL),
(38, 'Grammaire / Vocabulaire / Orthographe', 'G-V-O', 0, 2, '2014-11-03 07:11:21', '2017-10-10 00:00:00', NULL, NULL),
(43, 'Sciences Expérimentales  ', 'ScExp', 1, NULL, '2014-11-03 08:11:11', '2017-10-10 00:00:00', NULL, NULL),
(46, 'Géographie / Histoire', 'ge-hi', 0, 10, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(47, 'Anglais', 'angl', 0, 25, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(48, 'Expression orale et écrite / Stylistique', 'EOE-S', 0, 2, '2017-10-10 00:00:00', '2017-10-13 00:00:00', NULL, NULL),
(49, 'Sciences naturelles', 'ScNat', 0, 43, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(50, 'Electricité', 'elect', 0, 23, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(51, 'Optique', 'opt', 0, 23, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(52, 'Magnétisme', 'magn', 0, 23, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(53, 'Thermodynamique et électricité', 'th-el', 0, 23, '2017-10-10 00:00:00', '2017-11-10 00:00:00', NULL, NULL),
(54, 'Informatique', 'infor', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(55, 'Géologie', 'geol', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(56, 'Biologie & Géologie', 'BiGeo', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(57, 'Géométrie', 'geom', 0, 1, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(58, 'Laboratoire', 'labo', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(59, 'Pièces classiques', 'pClas', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(60, 'Religion', 'rel', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(61, 'Culture littéraire', 'culli', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(62, 'Français', 'fran', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(63, 'Méthodologie', 'metho', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(64, 'Physique', 'phy2', 0, 23, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(65, 'Economie', 'eco', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(66, 'Education à la citoyenneté', 'educ', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(67, 'Histoire de l\'Art', 'hiArt', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(68, 'Analyse, Algèbre & Géométrie', 'A-A-G', 0, 1, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(69, 'Musique', 'musiq', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(70, 'Analyse & Géométrie', 'AnGeo', 0, 1, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(71, 'Philosophie', 'phil', 0, NULL, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(72, 'Probabilité', 'prob', 0, 1, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(73, 'Analyse, Suite & Complexe', 'A-S-C', 0, 1, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(74, 'Algèbre & Géométrie', 'AlGeo', 0, 1, '2017-10-10 00:00:00', '2017-10-10 00:00:00', NULL, NULL),
(75, 'Analyse & Algèbre', 'An-Al', 0, 1, '2017-10-11 00:00:00', '2017-10-11 00:00:00', NULL, NULL),
(76, 'Condensateur & Alternatif', 'CoAlt', 0, 23, '2017-10-11 00:00:00', '2018-02-06 00:00:00', NULL, NULL),
(77, 'Grammaire, Vocabulaire & Stylistique', 'GVS', 0, 2, '2017-10-13 00:00:00', '2017-10-13 00:00:00', NULL, NULL),
(78, 'Stylistique', 'styl', 0, 2, '2017-10-13 00:00:00', '2017-10-13 00:00:00', NULL, NULL),
(79, 'Expression orale et écrite', 'expOE', 0, 2, '2017-10-13 00:00:00', '2017-10-13 00:00:00', NULL, NULL),
(80, 'Géométrie et trigonométrie', 'getri', 0, 1, '2017-11-08 00:00:00', '2017-11-08 00:00:00', NULL, NULL),
(81, 'Mécanique', 'meca', 0, 23, '2017-11-10 00:00:00', '2017-11-10 00:00:00', NULL, NULL),
(82, 'Algèbre, Trigonométrie, Analyse combinatoire', 'ATAc', 0, 1, '2017-11-14 00:00:00', '2017-11-14 00:00:00', NULL, NULL),
(83, 'Savoir-vivre', 's-v', 0, NULL, '2017-11-20 00:00:00', '2017-11-20 00:00:00', NULL, NULL),
(84, 'Magnétisme, Condensateur et Alternatif', 'M-C-A', 0, 23, '2017-12-05 00:00:00', '2017-12-05 00:00:00', NULL, NULL),
(85, 'Electricité et Optique', 'El-Op', 0, 23, '2017-12-05 00:00:00', '2017-12-05 00:00:00', NULL, NULL),
(86, 'Algèbre, Géométrie et Probabilité', 'A-G-P', 0, 1, '2017-12-05 00:00:00', '2017-12-05 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subject_average`
--

CREATE TABLE `subject_average` (
  `academic_year` int(11) NOT NULL,
  `evaluation_by_year` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `average` double NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(100) NOT NULL,
  `update_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `taxes`
--

CREATE TABLE `taxes` (
  `id` int(11) NOT NULL,
  `taxe_description` varchar(120) NOT NULL,
  `employeur_employe` int(2) DEFAULT NULL COMMENT '0: employe; 1: employeur',
  `taxe_value` double NOT NULL,
  `particulier` int(1) NOT NULL DEFAULT '0' COMMENT '0: for general taxes, 1: for a particular tax ',
  `academic_year` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- --------------------------------------------------------

--
-- Table structure for table `titles`
--

CREATE TABLE `titles` (
  `id` int(11) NOT NULL,
  `title_name` varchar(45) NOT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `titles`
--

INSERT INTO `titles` (`id`, `title_name`, `date_created`, `date_updated`, `create_by`, `update_by`) VALUES
(1, 'Secretaire', '2014-10-04 00:00:00', '2014-10-04 00:00:00', NULL, NULL),
(2, 'Responsable de discipline', '2014-10-04 00:00:00', '2014-10-04 00:00:00', NULL, NULL),
(3, 'Directrice administrative', '2014-10-04 00:00:00', '2014-10-04 00:00:00', NULL, NULL),
(4, 'Censeur', '2014-10-04 00:00:00', '2014-10-04 00:00:00', NULL, NULL),
(5, 'Responsable de cycle', '2014-10-04 00:00:00', '2014-10-04 00:00:00', NULL, NULL),
(6, 'Econome', '2014-10-04 00:00:00', '2014-10-04 00:00:00', NULL, NULL),
(7, 'Aide titulaire ', '2015-08-20 00:00:00', '2015-08-20 00:00:00', NULL, NULL),
(8, 'Petit personnel', '2015-08-20 00:00:00', '2015-08-20 00:00:00', NULL, NULL),
(9, 'Responsable informatique', '2017-10-09 00:00:00', '2017-10-09 00:00:00', NULL, NULL),
(10, 'Doyen(ne) académique', '2017-10-09 00:00:00', '2017-10-09 00:00:00', NULL, NULL),
(11, 'Directrice académique', '2017-10-09 00:00:00', '2017-10-09 00:00:00', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(128) NOT NULL,
  `active` tinyint(1) NOT NULL,
  `person_id` int(11) NOT NULL,
  `full_name` varchar(255) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `profil` int(11) DEFAULT NULL,
  `group_id` int(10) DEFAULT NULL,
  `is_parent` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `last_ip` varchar(100) NOT NULL,
  `last_activity` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `active`, `person_id`, `full_name`, `create_by`, `update_by`, `date_created`, `date_updated`, `profil`, `group_id`, `is_parent`, `user_id`, `last_ip`, `last_activity`) VALUES
(1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 1, 1, 'Admin', 'admin', NULL, '2014-01-10 00:00:00', '2014-01-10 00:00:00', 1, 5, NULL, 0, '190.115.154.134', '2018-08-27 13:56:13'),
(2, 'super_user', '21232f297a57a5a743894a0e4a801fc3', 1, 0, 'Super User', 'admin', NULL, '2015-03-07 00:00:00', '2015-03-07 00:00:00', 1, 1, NULL, 0, '190.115.183.253', '2018-08-28 15:36:52');

-- --------------------------------------------------------

--
-- Table structure for table `users_preferences`
--

CREATE TABLE `users_preferences` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `skin` varchar(64) NOT NULL,
  `skin_css` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `year_migration_check`
--

CREATE TABLE `year_migration_check` (
  `id` int(11) NOT NULL,
  `academic_year` int(11) NOT NULL,
  `period` int(11) NOT NULL,
  `postulant` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `evaluation` int(11) NOT NULL,
  `passing_grade` int(11) NOT NULL,
  `reportcard_observation` int(11) NOT NULL,
  `fees` int(11) NOT NULL,
  `taxes` int(11) NOT NULL,
  `pending_balance` int(11) NOT NULL,
  `date_created` date NOT NULL,
  `date_updated` date NOT NULL,
  `create_by` varchar(65) NOT NULL,
  `update_by` varchar(65) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `academicperiods`
--
ALTER TABLE `academicperiods`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_periods_academic_periods1_idx` (`year`);

--
-- Indexes for table `accounting`
--
ALTER TABLE `accounting`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_action_module` (`module_id`);

--
-- Indexes for table `announcements`
--
ALTER TABLE `announcements`
  ADD PRIMARY KEY (`id`),
  ADD KEY `title` (`title`,`create_by`);

--
-- Indexes for table `arrondissements`
--
ALTER TABLE `arrondissements`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `arrondissement_name` (`arrondissement_name`),
  ADD KEY `departement` (`departement`);

--
-- Indexes for table `average_by_period`
--
ALTER TABLE `average_by_period`
  ADD PRIMARY KEY (`academic_year`,`evaluation_by_year`,`student`),
  ADD KEY `fk_average_by_period_eval_by_y` (`evaluation_by_year`),
  ADD KEY `fk_average_by_period_person` (`student`);

--
-- Indexes for table `balance`
--
ALTER TABLE `balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`);

--
-- Indexes for table `bareme`
--
ALTER TABLE `bareme`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `billings`
--
ALTER TABLE `billings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_biiling_student_idx` (`student`),
  ADD KEY `fk_payment_method_idx` (`payment_method`),
  ADD KEY `fk_billings_fee_period` (`fee_period`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `reservation_id` (`reservation_id`);

--
-- Indexes for table `charge_description`
--
ALTER TABLE `charge_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `charge_paid`
--
ALTER TABLE `charge_paid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `id_charge_description` (`id_charge_description`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `city_name_UNIQUE` (`city_name`),
  ADD KEY `fk_cities_arrondissement_idx` (`arrondissement`);

--
-- Indexes for table `cms_article`
--
ALTER TABLE `cms_article`
  ADD PRIMARY KEY (`id`),
  ADD KEY `section` (`section`),
  ADD KEY `fk_menu` (`article_menu`);

--
-- Indexes for table `cms_doc`
--
ALTER TABLE `cms_doc`
  ADD PRIMARY KEY (`id`),
  ADD KEY `document_name` (`document_name`),
  ADD KEY `document_title` (`document_title`);

--
-- Indexes for table `cms_image`
--
ALTER TABLE `cms_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `label_image` (`label_image`);

--
-- Indexes for table `cms_menu`
--
ALTER TABLE `cms_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_section`
--
ALTER TABLE `cms_section`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `section_name` (`section_name`);

--
-- Indexes for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_contact_info_idx` (`person`),
  ADD KEY `fk_relationship_idx` (`contact_relationship`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_couse_subject_idx` (`subject`),
  ADD KEY `fk_course_teacher_idx` (`teacher`),
  ADD KEY `room_idx` (`room`),
  ADD KEY `fk_course_period_academic_idx` (`academic_period`);

--
-- Indexes for table `custom_field`
--
ALTER TABLE `custom_field`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `custom_field_data`
--
ALTER TABLE `custom_field_data`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_custom_field_idx` (`field_link`),
  ADD KEY `fk_object_id_idx` (`object_id`);

--
-- Indexes for table `Cycles`
--
ALTER TABLE `Cycles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `decision_finale`
--
ALTER TABLE `decision_finale`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_decision_idx` (`student`),
  ADD KEY `fk_academic_year_decision_idx` (`academic_year`),
  ADD KEY `fk_current_level_idx` (`current_level`),
  ADD KEY `fk_next_level_idx` (`next_level`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department_name_UNIQUE` (`department_name`);

--
-- Indexes for table `department_has_person`
--
ALTER TABLE `department_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `department_id` (`department_id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `employee` (`employee`);

--
-- Indexes for table `department_in_school`
--
ALTER TABLE `department_in_school`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `devises`
--
ALTER TABLE `devises`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `devise_name_UNIQUE` (`devise_name`),
  ADD UNIQUE KEY `devise_symbol_UNIQUE` (`devise_symbol`);

--
-- Indexes for table `employee_info`
--
ALTER TABLE `employee_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_employee_qualification_idx` (`qualification`),
  ADD KEY `fk_employee_job_status_idx` (`job_status`),
  ADD KEY `fk_employee_field_of_study_idx` (`field_study`),
  ADD KEY `fk_employee_person` (`employee`);

--
-- Indexes for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  ADD PRIMARY KEY (`id`),
  ADD KEY `postulant` (`postulant`),
  ADD KEY `apply_level` (`apply_level`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `payment_method` (`payment_method`);

--
-- Indexes for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_evaluation_year_evaluation_idx` (`evaluation`),
  ADD KEY `fk_evaluation_year_academic_year_idx` (`academic_year`);

--
-- Indexes for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`),
  ADD KEY `subject` (`subject`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `fees`
--
ALTER TABLE `fees`
  ADD PRIMARY KEY (`id`),
  ADD KEY `level` (`level`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `devise` (`devise`),
  ADD KEY `fee` (`fee`);

--
-- Indexes for table `fees_label`
--
ALTER TABLE `fees_label`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `field_study`
--
ALTER TABLE `field_study`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `field_name_UNIQUE` (`field_name`);

--
-- Indexes for table `general_average_by_period`
--
ALTER TABLE `general_average_by_period`
  ADD PRIMARY KEY (`academic_year`,`academic_period`,`student`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `student` (`student`);

--
-- Indexes for table `general_config`
--
ALTER TABLE `general_config`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `item_name_UNIQUE` (`item_name`),
  ADD KEY `name` (`name`);

--
-- Indexes for table `grades`
--
ALTER TABLE `grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_grades_student_idx` (`student`),
  ADD KEY `fk_grades_course_idx` (`course`),
  ADD KEY `fk_grades_evaluation_idx` (`evaluation`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`),
  ADD KEY `belongs_to_profil` (`belongs_to_profil`);

--
-- Indexes for table `groups_has_actions`
--
ALTER TABLE `groups_has_actions`
  ADD PRIMARY KEY (`groups_id`,`actions_id`),
  ADD KEY `fk_groups_has_actions_actions1_idx` (`actions_id`),
  ADD KEY `fk_groups_has_actions_groups1_idx` (`groups_id`);

--
-- Indexes for table `groups_has_modules`
--
ALTER TABLE `groups_has_modules`
  ADD PRIMARY KEY (`groups_id`,`modules_id`),
  ADD KEY `fk_groups_has_modules_modules` (`modules_id`);

--
-- Indexes for table `help`
--
ALTER TABLE `help`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title` (`title`),
  ADD UNIQUE KEY `url` (`url`);

--
-- Indexes for table `homework`
--
ALTER TABLE `homework`
  ADD PRIMARY KEY (`id`),
  ADD KEY `course` (`course`),
  ADD KEY `person_id` (`person_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `homework_submission`
--
ALTER TABLE `homework_submission`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `homework_id` (`homework_id`);

--
-- Indexes for table `infraction_type`
--
ALTER TABLE `infraction_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `job_status`
--
ALTER TABLE `job_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `status_name_UNIQUE` (`status_name`);

--
-- Indexes for table `label_category_for_billing`
--
ALTER TABLE `label_category_for_billing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `levels`
--
ALTER TABLE `levels`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `level_name_UNIQUE` (`level_name`),
  ADD KEY `fk_levels_levels1_idx` (`previous_level`),
  ADD KEY `section` (`section`);

--
-- Indexes for table `level_has_person`
--
ALTER TABLE `level_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_level_idx` (`students`),
  ADD KEY `fk_student_level_year_idx` (`academic_year`),
  ADD KEY `fk_level_students_idx` (`level`);

--
-- Indexes for table `loan_of_money`
--
ALTER TABLE `loan_of_money`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_payroll_set` (`person_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `mails`
--
ALTER TABLE `mails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `from` (`sender`(191),`subject`(191),`is_read`),
  ADD KEY `id_sender` (`id_sender`);

--
-- Indexes for table `menfp_decision`
--
ALTER TABLE `menfp_decision`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `menfp_grades`
--
ALTER TABLE `menfp_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `menfp_exam` (`menfp_exam`);

--
-- Indexes for table `modules`
--
ALTER TABLE `modules`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `module_short_UNIQUE` (`module_short_name`),
  ADD UNIQUE KEY `module_name_UNIQUE` (`module_name`);

--
-- Indexes for table `other_incomes`
--
ALTER TABLE `other_incomes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `id_income_description` (`id_income_description`);

--
-- Indexes for table `other_incomes_description`
--
ALTER TABLE `other_incomes_description`
  ADD PRIMARY KEY (`id`),
  ADD KEY `category` (`category`);

--
-- Indexes for table `partners`
--
ALTER TABLE `partners`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `passing_grades`
--
ALTER TABLE `passing_grades`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_level_passing_grade_idx` (`level`),
  ADD KEY `fk_academic_period_passing_idx` (`academic_period`),
  ADD KEY `course` (`course`);

--
-- Indexes for table `payment_method`
--
ALTER TABLE `payment_method`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `method_name_UNIQUE` (`method_name`);

--
-- Indexes for table `payroll`
--
ALTER TABLE `payroll`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_paroll_set` (`id_payroll_set`);

--
-- Indexes for table `payroll_settings`
--
ALTER TABLE `payroll_settings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`,`academic_year`),
  ADD KEY `fk_payroll_setting_academicperiods` (`academic_year`);

--
-- Indexes for table `payroll_setting_taxes`
--
ALTER TABLE `payroll_setting_taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_payroll_set` (`id_payroll_set`),
  ADD KEY `id_taxe` (`id_taxe`);

--
-- Indexes for table `pending_balance`
--
ALTER TABLE `pending_balance`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_pending_balance_persons` (`student`),
  ADD KEY `fk_pending_balance_academicperiods` (`academic_year`);

--
-- Indexes for table `persons`
--
ALTER TABLE `persons`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_persons_cities1_idx` (`cities`);

--
-- Indexes for table `persons_has_titles`
--
ALTER TABLE `persons_has_titles`
  ADD PRIMARY KEY (`persons_id`,`titles_id`,`academic_year`),
  ADD KEY `fk_persons_has_titles_titles1_idx` (`titles_id`),
  ADD KEY `fk_persons_has_titles_persons1_idx` (`persons_id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `person_history`
--
ALTER TABLE `person_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`);

--
-- Indexes for table `postulant`
--
ALTER TABLE `postulant`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cities` (`cities`),
  ADD KEY `person_liable_relation` (`person_liable_relation`),
  ADD KEY `apply_for_level` (`apply_for_level`),
  ADD KEY `previous_level` (`previous_level`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `product_name` (`product_name`),
  ADD KEY `type` (`type`);

--
-- Indexes for table `profil`
--
ALTER TABLE `profil`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `profil_has_modules`
--
ALTER TABLE `profil_has_modules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `module_id` (`module_id`),
  ADD KEY `profil_id` (`profil_id`);

--
-- Indexes for table `qualifications`
--
ALTER TABLE `qualifications`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `qualification_name_UNIQUE` (`qualification_name`);

--
-- Indexes for table `raise_salary`
--
ALTER TABLE `raise_salary`
  ADD PRIMARY KEY (`id`),
  ADD KEY `person_id` (`person_id`,`academic_year`);

--
-- Indexes for table `record_infraction`
--
ALTER TABLE `record_infraction`
  ADD PRIMARY KEY (`id`),
  ADD KEY `indx_stud_infrac` (`student`),
  ADD KEY `infraction_type` (`infraction_type`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `exam_period` (`exam_period`);

--
-- Indexes for table `record_presence`
--
ALTER TABLE `record_presence`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `room` (`room`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `exam_period` (`exam_period`);

--
-- Indexes for table `relations`
--
ALTER TABLE `relations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `relation_name_UNIQUE` (`relation_name`);

--
-- Indexes for table `reportcard_observation`
--
ALTER TABLE `reportcard_observation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_year` (`academic_year`),
  ADD KEY `section` (`section`);

--
-- Indexes for table `reservation`
--
ALTER TABLE `reservation`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_reservation_payment_method` (`payment_method`),
  ADD KEY `fk_reservation_academic_year` (`academic_year`);

--
-- Indexes for table `return_history`
--
ALTER TABLE `return_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_transaction` (`id_transaction`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `rooms`
--
ALTER TABLE `rooms`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_room_levels1_idx` (`level`),
  ADD KEY `fk_room_shift_idx` (`shift`);

--
-- Indexes for table `room_has_person`
--
ALTER TABLE `room_has_person`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_students_level_idx` (`students`),
  ADD KEY `fk_student_level_year_idx` (`academic_year`),
  ADD KEY `fk_level_students_idx` (`room`);

--
-- Indexes for table `rpt_custom`
--
ALTER TABLE `rpt_custom`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `rpt_custom_cat`
--
ALTER TABLE `rpt_custom_cat`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categorie_name` (`categorie_name`);

--
-- Indexes for table `sale_transaction`
--
ALTER TABLE `sale_transaction`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id_transaction` (`id_transaction`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `scalendar`
--
ALTER TABLE `scalendar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_scalendar_academic_year` (`academic_year`);

--
-- Indexes for table `schedules`
--
ALTER TABLE `schedules`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_schedule_course_idx` (`course`);

--
-- Indexes for table `schedule_agenda`
--
ALTER TABLE `schedule_agenda`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_schedule_agenda_academic_year` (`academic_year`),
  ADD KEY `fk_course_agenda_courses` (`course`);

--
-- Indexes for table `scholarship_holder`
--
ALTER TABLE `scholarship_holder`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_scholarship_holder_student` (`student`),
  ADD KEY `fk_scholarship_holder_academicperiods` (`academic_year`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `section_has_cycle`
--
ALTER TABLE `section_has_cycle`
  ADD PRIMARY KEY (`id`),
  ADD KEY `cycle` (`cycle`,`section`),
  ADD KEY `section` (`section`),
  ADD KEY `level` (`level`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `sellings`
--
ALTER TABLE `sellings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_products` (`id_products`),
  ADD KEY `transac` (`transaction_id`);

--
-- Indexes for table `session`
--
ALTER TABLE `session`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shifts`
--
ALTER TABLE `shifts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `siges_payment`
--
ALTER TABLE `siges_payment`
  ADD PRIMARY KEY (`id`),
  ADD KEY `payment_method` (`payment_method`),
  ADD KEY `id_siges_payment_set` (`id_siges_payment_set`);

--
-- Indexes for table `siges_payment_set`
--
ALTER TABLE `siges_payment_set`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`),
  ADD KEY `devise` (`devise`);

--
-- Indexes for table `stocks`
--
ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`);

--
-- Indexes for table `stock_history`
--
ALTER TABLE `stock_history`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_product` (`id_product`),
  ADD KEY `id_stock` (`id_stock`);

--
-- Indexes for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_documents_persons` (`id_student`);

--
-- Indexes for table `student_other_info`
--
ALTER TABLE `student_other_info`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_student_other_info` (`student`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_subjects_subjects1_idx` (`subject_parent`);

--
-- Indexes for table `subject_average`
--
ALTER TABLE `subject_average`
  ADD PRIMARY KEY (`academic_year`,`evaluation_by_year`,`course`),
  ADD KEY `fk_subject_average_evaluation_byyear` (`evaluation_by_year`),
  ADD KEY `fk_subject_average_course` (`course`);

--
-- Indexes for table `taxes`
--
ALTER TABLE `taxes`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- Indexes for table `titles`
--
ALTER TABLE `titles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `title_name_UNIQUE` (`title_name`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD KEY `group_id` (`group_id`),
  ADD KEY `person_idx` (`person_id`),
  ADD KEY `profil` (`profil`);

--
-- Indexes for table `users_preferences`
--
ALTER TABLE `users_preferences`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`),
  ADD KEY `skin` (`skin`);

--
-- Indexes for table `year_migration_check`
--
ALTER TABLE `year_migration_check`
  ADD PRIMARY KEY (`id`),
  ADD KEY `academic_year` (`academic_year`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `academicperiods`
--
ALTER TABLE `academicperiods`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `accounting`
--
ALTER TABLE `accounting`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `actions`
--
ALTER TABLE `actions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=415;

--
-- AUTO_INCREMENT for table `announcements`
--
ALTER TABLE `announcements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `arrondissements`
--
ALTER TABLE `arrondissements`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `balance`
--
ALTER TABLE `balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `bareme`
--
ALTER TABLE `bareme`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `billings`
--
ALTER TABLE `billings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `charge_description`
--
ALTER TABLE `charge_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `charge_paid`
--
ALTER TABLE `charge_paid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=141;

--
-- AUTO_INCREMENT for table `cms_article`
--
ALTER TABLE `cms_article`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_doc`
--
ALTER TABLE `cms_doc`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_image`
--
ALTER TABLE `cms_image`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_menu`
--
ALTER TABLE `cms_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `cms_section`
--
ALTER TABLE `cms_section`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `contact_info`
--
ALTER TABLE `contact_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_field`
--
ALTER TABLE `custom_field`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `custom_field_data`
--
ALTER TABLE `custom_field_data`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `Cycles`
--
ALTER TABLE `Cycles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `decision_finale`
--
ALTER TABLE `decision_finale`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `department_has_person`
--
ALTER TABLE `department_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `department_in_school`
--
ALTER TABLE `department_in_school`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `devises`
--
ALTER TABLE `devises`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `employee_info`
--
ALTER TABLE `employee_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evaluations`
--
ALTER TABLE `evaluations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fees`
--
ALTER TABLE `fees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `fees_label`
--
ALTER TABLE `fees_label`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `field_study`
--
ALTER TABLE `field_study`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `general_config`
--
ALTER TABLE `general_config`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=108;

--
-- AUTO_INCREMENT for table `grades`
--
ALTER TABLE `grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `help`
--
ALTER TABLE `help`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homework`
--
ALTER TABLE `homework`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `homework_submission`
--
ALTER TABLE `homework_submission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `infraction_type`
--
ALTER TABLE `infraction_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `job_status`
--
ALTER TABLE `job_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `label_category_for_billing`
--
ALTER TABLE `label_category_for_billing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `levels`
--
ALTER TABLE `levels`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'School level (calling classes dans le system scolaire Haitien)', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `level_has_person`
--
ALTER TABLE `level_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `loan_of_money`
--
ALTER TABLE `loan_of_money`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mails`
--
ALTER TABLE `mails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menfp_decision`
--
ALTER TABLE `menfp_decision`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `menfp_grades`
--
ALTER TABLE `menfp_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `modules`
--
ALTER TABLE `modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `other_incomes`
--
ALTER TABLE `other_incomes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `other_incomes_description`
--
ALTER TABLE `other_incomes_description`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `partners`
--
ALTER TABLE `partners`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `passing_grades`
--
ALTER TABLE `passing_grades`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payment_method`
--
ALTER TABLE `payment_method`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `payroll`
--
ALTER TABLE `payroll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll_settings`
--
ALTER TABLE `payroll_settings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `payroll_setting_taxes`
--
ALTER TABLE `payroll_setting_taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `pending_balance`
--
ALTER TABLE `pending_balance`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `persons`
--
ALTER TABLE `persons`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `person_history`
--
ALTER TABLE `person_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `postulant`
--
ALTER TABLE `postulant`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profil`
--
ALTER TABLE `profil`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `profil_has_modules`
--
ALTER TABLE `profil_has_modules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;

--
-- AUTO_INCREMENT for table `qualifications`
--
ALTER TABLE `qualifications`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `raise_salary`
--
ALTER TABLE `raise_salary`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `record_infraction`
--
ALTER TABLE `record_infraction`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `record_presence`
--
ALTER TABLE `record_presence`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `relations`
--
ALTER TABLE `relations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `reportcard_observation`
--
ALTER TABLE `reportcard_observation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `reservation`
--
ALTER TABLE `reservation`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `return_history`
--
ALTER TABLE `return_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rooms`
--
ALTER TABLE `rooms`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `room_has_person`
--
ALTER TABLE `room_has_person`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `rpt_custom`
--
ALTER TABLE `rpt_custom`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `rpt_custom_cat`
--
ALTER TABLE `rpt_custom_cat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `sale_transaction`
--
ALTER TABLE `sale_transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scalendar`
--
ALTER TABLE `scalendar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `schedules`
--
ALTER TABLE `schedules`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `schedule_agenda`
--
ALTER TABLE `schedule_agenda`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `scholarship_holder`
--
ALTER TABLE `scholarship_holder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `section_has_cycle`
--
ALTER TABLE `section_has_cycle`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `sellings`
--
ALTER TABLE `sellings`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `shifts`
--
ALTER TABLE `shifts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `siges_payment`
--
ALTER TABLE `siges_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `siges_payment_set`
--
ALTER TABLE `siges_payment_set`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `stock_history`
--
ALTER TABLE `stock_history`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_documents`
--
ALTER TABLE `student_documents`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `student_other_info`
--
ALTER TABLE `student_other_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=87;

--
-- AUTO_INCREMENT for table `taxes`
--
ALTER TABLE `taxes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `titles`
--
ALTER TABLE `titles`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users_preferences`
--
ALTER TABLE `users_preferences`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

--
-- AUTO_INCREMENT for table `year_migration_check`
--
ALTER TABLE `year_migration_check`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `actions_ibfk_1` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `arrondissements`
--
ALTER TABLE `arrondissements`
  ADD CONSTRAINT `arrondissements_ibfk_1` FOREIGN KEY (`departement`) REFERENCES `departments` (`id`);

--
-- Constraints for table `average_by_period`
--
ALTER TABLE `average_by_period`
  ADD CONSTRAINT `fk_average_by_period_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_average_by_period_eval_by_y` FOREIGN KEY (`evaluation_by_year`) REFERENCES `evaluation_by_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_average_by_period_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `balance`
--
ALTER TABLE `balance`
  ADD CONSTRAINT `fk_balance_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `billings`
--
ALTER TABLE `billings`
  ADD CONSTRAINT `fk_biiling_reservation` FOREIGN KEY (`reservation_id`) REFERENCES `reservation` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_biiling_student` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_billings_fees` FOREIGN KEY (`fee_period`) REFERENCES `fees` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_billing_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `fk_billing_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `charge_paid`
--
ALTER TABLE `charge_paid`
  ADD CONSTRAINT `fk_charge_paid_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_charge_paid_charge_description` FOREIGN KEY (`id_charge_description`) REFERENCES `charge_description` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `fk_cities_arrondissement` FOREIGN KEY (`arrondissement`) REFERENCES `arrondissements` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `cms_article`
--
ALTER TABLE `cms_article`
  ADD CONSTRAINT `cms_article_ibfk_1` FOREIGN KEY (`section`) REFERENCES `cms_section` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_article_menu` FOREIGN KEY (`article_menu`) REFERENCES `cms_menu` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `contact_info`
--
ALTER TABLE `contact_info`
  ADD CONSTRAINT `fk_contact_info` FOREIGN KEY (`person`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_relationship` FOREIGN KEY (`contact_relationship`) REFERENCES `relations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `courses`
--
ALTER TABLE `courses`
  ADD CONSTRAINT `fk_course_period_academic` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_course_room` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_course_teacher` FOREIGN KEY (`teacher`) REFERENCES `persons` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_couse_subject` FOREIGN KEY (`subject`) REFERENCES `subjects` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE;

--
-- Constraints for table `custom_field_data`
--
ALTER TABLE `custom_field_data`
  ADD CONSTRAINT `fk_custom_field` FOREIGN KEY (`field_link`) REFERENCES `custom_field` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_object_id` FOREIGN KEY (`object_id`) REFERENCES `persons` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `decision_finale`
--
ALTER TABLE `decision_finale`
  ADD CONSTRAINT `fk_academic_year_decision` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_current_level` FOREIGN KEY (`current_level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_next_level` FOREIGN KEY (`next_level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_students_decision` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `department_has_person`
--
ALTER TABLE `department_has_person`
  ADD CONSTRAINT `fk_depatment_in_school_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_depatment_in_school_depatment_in_school` FOREIGN KEY (`department_id`) REFERENCES `department_in_school` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_depatment_in_school_perso` FOREIGN KEY (`employee`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `employee_info`
--
ALTER TABLE `employee_info`
  ADD CONSTRAINT `fk_employee_field_of_study` FOREIGN KEY (`field_study`) REFERENCES `field_study` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employee_job_status` FOREIGN KEY (`job_status`) REFERENCES `job_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employee_person` FOREIGN KEY (`employee`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_employee_qualification` FOREIGN KEY (`qualification`) REFERENCES `qualifications` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `enrollment_income`
--
ALTER TABLE `enrollment_income`
  ADD CONSTRAINT `fk_enrollment_income_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_enrollment_income_level` FOREIGN KEY (`apply_level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_enrollment_income_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `evaluations`
--
ALTER TABLE `evaluations`
  ADD CONSTRAINT `fk_evaluation_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`);

--
-- Constraints for table `evaluation_by_year`
--
ALTER TABLE `evaluation_by_year`
  ADD CONSTRAINT `fk_evaluation_year_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_evaluation_year_evaluation` FOREIGN KEY (`evaluation`) REFERENCES `evaluations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `examen_menfp`
--
ALTER TABLE `examen_menfp`
  ADD CONSTRAINT `examen_menfp_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `examen_menfp_levels` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `examen_menfp_sujects` FOREIGN KEY (`subject`) REFERENCES `subjects` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `fees`
--
ALTER TABLE `fees`
  ADD CONSTRAINT `fk_fees_academic_period` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fees_devise` FOREIGN KEY (`devise`) REFERENCES `devises` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fees_fees_label` FOREIGN KEY (`fee`) REFERENCES `fees_label` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_fees_level` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `general_average_by_period`
--
ALTER TABLE `general_average_by_period`
  ADD CONSTRAINT `fk_general_average_period_academicperiod_academicYear` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_general_average_period_academicperiod_period` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_general_average_period_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `grades`
--
ALTER TABLE `grades`
  ADD CONSTRAINT `fk_grades_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_grades_evaluation` FOREIGN KEY (`evaluation`) REFERENCES `evaluation_by_year` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_grades_student` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups`
--
ALTER TABLE `groups`
  ADD CONSTRAINT `fk_group_profil_id` FOREIGN KEY (`belongs_to_profil`) REFERENCES `profil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups_has_actions`
--
ALTER TABLE `groups_has_actions`
  ADD CONSTRAINT `fk_groups_has_actions_actions` FOREIGN KEY (`actions_id`) REFERENCES `actions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_groups_has_actions_groups` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `groups_has_modules`
--
ALTER TABLE `groups_has_modules`
  ADD CONSTRAINT `fk_groups_has_modules_groups` FOREIGN KEY (`groups_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_groups_has_modules_modules` FOREIGN KEY (`modules_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `homework`
--
ALTER TABLE `homework`
  ADD CONSTRAINT `fk_homework_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_homework_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `homework_submission`
--
ALTER TABLE `homework_submission`
  ADD CONSTRAINT `fk_homework_submission_homework` FOREIGN KEY (`homework_id`) REFERENCES `homework` (`id`),
  ADD CONSTRAINT `fk_homework_submission_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`);

--
-- Constraints for table `levels`
--
ALTER TABLE `levels`
  ADD CONSTRAINT `fk_levels_levels1` FOREIGN KEY (`previous_level`) REFERENCES `levels` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_levels_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`);

--
-- Constraints for table `level_has_person`
--
ALTER TABLE `level_has_person`
  ADD CONSTRAINT `fk_students_level` FOREIGN KEY (`students`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_student_level_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `level_has_person_ibfk_2` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `loan_of_money`
--
ALTER TABLE `loan_of_money`
  ADD CONSTRAINT `fk_loan_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`);

--
-- Constraints for table `menfp_decision`
--
ALTER TABLE `menfp_decision`
  ADD CONSTRAINT `menfp_decision_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `menfp_decision_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `menfp_grades`
--
ALTER TABLE `menfp_grades`
  ADD CONSTRAINT `menfp_grades_examen_menfp` FOREIGN KEY (`menfp_exam`) REFERENCES `examen_menfp` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `menfp_grades_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `other_incomes`
--
ALTER TABLE `other_incomes`
  ADD CONSTRAINT `fk_other_income_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `fk_other_income_incomes_description` FOREIGN KEY (`id_income_description`) REFERENCES `other_incomes_description` (`id`);

--
-- Constraints for table `passing_grades`
--
ALTER TABLE `passing_grades`
  ADD CONSTRAINT `fk_academic_period_passing` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_level_passing_grade` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_passing_grade_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`);

--
-- Constraints for table `payroll`
--
ALTER TABLE `payroll`
  ADD CONSTRAINT `fk_payroll_payroll_setting` FOREIGN KEY (`id_payroll_set`) REFERENCES `payroll_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payroll_settings`
--
ALTER TABLE `payroll_settings`
  ADD CONSTRAINT `fk_payroll_setting_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payroll_setting_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `payroll_setting_taxes`
--
ALTER TABLE `payroll_setting_taxes`
  ADD CONSTRAINT `fk_payroll_setting_taxes_payroll_settings` FOREIGN KEY (`id_payroll_set`) REFERENCES `payroll_settings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_payroll_setting_taxe_taxes` FOREIGN KEY (`id_taxe`) REFERENCES `taxes` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `pending_balance`
--
ALTER TABLE `pending_balance`
  ADD CONSTRAINT `fk_pending_balance_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_pending_balance_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `persons`
--
ALTER TABLE `persons`
  ADD CONSTRAINT `fk_persons_cities1` FOREIGN KEY (`cities`) REFERENCES `cities` (`id`);

--
-- Constraints for table `persons_has_titles`
--
ALTER TABLE `persons_has_titles`
  ADD CONSTRAINT `fk_persons_has_titles_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_has_titles_persons1` FOREIGN KEY (`persons_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_persons_has_titles_titles1` FOREIGN KEY (`titles_id`) REFERENCES `titles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `person_history`
--
ALTER TABLE `person_history`
  ADD CONSTRAINT `fk_person_history_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `postulant`
--
ALTER TABLE `postulant`
  ADD CONSTRAINT `fk_postulant_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_postulant_apply_level` FOREIGN KEY (`apply_for_level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_postulant_cities` FOREIGN KEY (`cities`) REFERENCES `cities` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_postulant_previous_level` FOREIGN KEY (`previous_level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_postulant_relation` FOREIGN KEY (`person_liable_relation`) REFERENCES `relations` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `profil_has_modules`
--
ALTER TABLE `profil_has_modules`
  ADD CONSTRAINT `fk_profil_has_modules_module_id` FOREIGN KEY (`module_id`) REFERENCES `modules` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_profil_has_modules_profil_id` FOREIGN KEY (`profil_id`) REFERENCES `profil` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `raise_salary`
--
ALTER TABLE `raise_salary`
  ADD CONSTRAINT `fk_raise_salary_person` FOREIGN KEY (`person_id`) REFERENCES `persons` (`id`);

--
-- Constraints for table `record_infraction`
--
ALTER TABLE `record_infraction`
  ADD CONSTRAINT `record_infraction_academic_year` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `record_infraction_ibfk_1` FOREIGN KEY (`student`) REFERENCES `persons` (`id`),
  ADD CONSTRAINT `record_infraction_ibfk_2` FOREIGN KEY (`infraction_type`) REFERENCES `infraction_type` (`id`),
  ADD CONSTRAINT `record_infraction_period_exam` FOREIGN KEY (`exam_period`) REFERENCES `academicperiods` (`id`);

--
-- Constraints for table `record_presence`
--
ALTER TABLE `record_presence`
  ADD CONSTRAINT `record_presence_ibfk_1` FOREIGN KEY (`student`) REFERENCES `persons` (`id`),
  ADD CONSTRAINT `record_presence_room_rooms` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`);

--
-- Constraints for table `reportcard_observation`
--
ALTER TABLE `reportcard_observation`
  ADD CONSTRAINT `fk_reportcard_observation_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `reportcard_observation_ibfk_1` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `reservation`
--
ALTER TABLE `reservation`
  ADD CONSTRAINT `fk_reservation_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `fk_reservation_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `rooms`
--
ALTER TABLE `rooms`
  ADD CONSTRAINT `fk_room_levels` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_room_shift` FOREIGN KEY (`shift`) REFERENCES `shifts` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `room_has_person`
--
ALTER TABLE `room_has_person`
  ADD CONSTRAINT `fk_room_students_level` FOREIGN KEY (`students`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_room_students_level_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`),
  ADD CONSTRAINT `fk_room_student_room` FOREIGN KEY (`room`) REFERENCES `rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `sale_transaction`
--
ALTER TABLE `sale_transaction`
  ADD CONSTRAINT `fk_sale_transaction_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `scholarship_holder`
--
ALTER TABLE `scholarship_holder`
  ADD CONSTRAINT `fk_scholarship_holder_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_scholarship_holder_person` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `section_has_cycle`
--
ALTER TABLE `section_has_cycle`
  ADD CONSTRAINT `fk_section_has_cycle_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_section_has_cycle_cycles` FOREIGN KEY (`cycle`) REFERENCES `Cycles` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_section_has_cycle_level` FOREIGN KEY (`level`) REFERENCES `levels` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_section_has_cycle_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `siges_payment`
--
ALTER TABLE `siges_payment`
  ADD CONSTRAINT `fk_siges_payment_siges_payment_set` FOREIGN KEY (`id_siges_payment_set`) REFERENCES `siges_payment_set` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_siges_payment_payment_method` FOREIGN KEY (`payment_method`) REFERENCES `payment_method` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `siges_payment_set`
--
ALTER TABLE `siges_payment_set`
  ADD CONSTRAINT `fk_siges_payment_set_academicperiods` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_siges_payment_set_devises` FOREIGN KEY (`devise`) REFERENCES `devises` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_documents`
--
ALTER TABLE `student_documents`
  ADD CONSTRAINT `fk_student_documents_persons` FOREIGN KEY (`id_student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `student_other_info`
--
ALTER TABLE `student_other_info`
  ADD CONSTRAINT `fk_student_other_info_persons` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `subject_average`
--
ALTER TABLE `subject_average`
  ADD CONSTRAINT `fk_subject_average_acad` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subject_average_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_subject_average_eval_by_year` FOREIGN KEY (`evaluation_by_year`) REFERENCES `evaluation_by_year` (`id`) ON UPDATE CASCADE;

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE SET NULL ON UPDATE SET NULL,
  ADD CONSTRAINT `fk_user_profil_id` FOREIGN KEY (`profil`) REFERENCES `profil` (`id`) ON DELETE SET NULL ON UPDATE SET NULL;

--
-- Constraints for table `year_migration_check`
--
ALTER TABLE `year_migration_check`
  ADD CONSTRAINT `fk_year_migration_check_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
