/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: Jul 9, 2019
 */

CREATE TABLE `article_by_level` ( 
    `id_article` INT NOT NULL AUTO_INCREMENT , 
    `id_product` INT NOT NULL ,
     `id_level` INT NOT NULL , 
     `id_academicyear` INT NOT NULL , 
     `description_article` TEXT NOT NULL , 
     `created_by` VARCHAR(255) NULL , 
     `updated_by` INT(255) NULL , 
     `created_date` DATETIME NULL , 
     `updated_date` DATETIME NULL ,
      PRIMARY KEY (`id_article`, `id_product`, `id_level`, `id_academicyear`)) ENGINE = InnoDB;


-- Relation entre les tables "levels, Products, academicperiod"

ALTER TABLE `article_by_level` 
ADD FOREIGN KEY (`id_product`) REFERENCES `products`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; 
ALTER TABLE `article_by_level` ADD FOREIGN KEY (`id_level`) REFERENCES `levels`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `article_by_level` ADD FOREIGN KEY (`id_academicyear`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;


-- New changes 
ALTER TABLE `sellings` ADD `id_level` INT NULL AFTER `id_products`; 
ALTER TABLE `sellings` ADD `id_student` INT NULL AFTER `id_level`, ADD `id_academic_year` INT NULL AFTER `id_student`;
