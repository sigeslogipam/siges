/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  abelfort
 * Created: Jun 22, 2020
 */
-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2019 at 01:10 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-12+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_2_0`
--

-- --------------------------------------------------------

--
-- Table structure for table `case_type`
--

CREATE TABLE `case_type` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` varchar(2050) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `case_type`
--



--
-- Indexes for dumped tables
--

--
-- Indexes for table `case_type`
--
ALTER TABLE `case_type`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `case_type`
--
ALTER TABLE `case_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 26, 2019 at 01:11 PM
-- Server version: 5.7.26-0ubuntu0.18.04.1
-- PHP Version: 5.6.40-12+ubuntu18.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `demo_2_0`
--

-- --------------------------------------------------------

--
-- Table structure for table `record_case`
--

CREATE TABLE `record_case` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `case_type` int(11) NOT NULL,
  `record_by` varchar(255) NOT NULL,
  `incident_date` date NOT NULL,
  `academic_period` int(11) NOT NULL,
  `exam_period` int(11) DEFAULT NULL,
  `case_description` text NOT NULL,
  `final_decision` varchar(255) DEFAULT NULL,
  `date_created` date DEFAULT NULL,
  `date_updated` date DEFAULT NULL,
  `created_by` varchar(255) DEFAULT NULL,
  `updated_by` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `record_case`
--



--
-- Indexes for dumped tables
--

--
-- Indexes for table `record_case`
--
ALTER TABLE `record_case`
  ADD PRIMARY KEY (`id`),
  ADD KEY `student` (`student`),
  ADD KEY `case_type` (`case_type`),
  ADD KEY `academic_period` (`academic_period`),
  ADD KEY `exam_period` (`exam_period`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `record_case`
--
ALTER TABLE `record_case`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `record_case`
--
ALTER TABLE `record_case`
  ADD CONSTRAINT `fk_case_acadperiod` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_case_casetype` FOREIGN KEY (`case_type`) REFERENCES `case_type` (`id`) ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_case_student` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

-- 27/01/2021
ALTER TABLE `contact_info` ADD `main_contact` INT NOT NULL DEFAULT '0' COMMENT '0:n\'est pas principal; 1:est principal' AFTER `email`;

-- 25/01/2021
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_formalms', 'Afficher e-learning', '0', "0: n\'afficher pas\r\n 1: Affiche", ' ', 'sys', '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_infirmerie', 'Afficher infirmerie', '1', "0: n\'afficher pas\r\n 1: Affiche", ' ', 'sys', '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL);




-- 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) 
VALUES (NULL, 'multiple_devise', 'Devise Multiple', '0', '1: Activer devise multiple 0: Desactiver devise multiple', NULL, 'sys', '1', '2020-12-03 00:00:00', NULL, 'LOGIPAM', NULL);
