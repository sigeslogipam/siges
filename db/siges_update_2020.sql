/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  poulardjc
 * Created: Dec 3, 2020
 */

-- Activer la gestion en mode multidevise 






-- 28/04/2021
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_average_student_view', 'Afficher Moyenne pour la vue des parents', '0', "0: n\'afficher pas\r\n 1: Affiche", ' ', 'sys', '2021-04-28 00:00:00', NULL, 'LOGIPAM', NULL);



-- 27/01/2021
ALTER TABLE `contact_info` ADD `main_contact` INT NOT NULL DEFAULT '0' COMMENT '0:n\'est pas principal; 1:est principal' AFTER `email`;

-- 25/01/2021
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_formalms', 'Afficher e-learning', '0', "0: n\'afficher pas\r\n 1: Affiche", ' ', 'sys', '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_infirmerie', 'Afficher infirmerie', '1', "0: n\'afficher pas\r\n 1: Affiche", ' ', 'sys', '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL);




-- 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) 
VALUES (NULL, 'multiple_devise', 'Devise Multiple', '0', '1: Activer devise multiple 0: Desactiver devise multiple', NULL, 'sys', '1', '2020-12-03 00:00:00', NULL, 'LOGIPAM', NULL);

