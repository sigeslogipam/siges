/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Author:  jcppoulard
 * Created: May 9, 2019
 */

-- 9 mai 2019 
ALTER TABLE `cms_article` ADD `featured_image` VARCHAR(255) NULL AFTER `section`, ADD INDEX (`featured_image`);

-- 11 mai 2019 
ALTER TABLE `announcements` CHANGE `description` `description` TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

-- 12 mai 2019 
ALTER TABLE `cms_image` ADD `album_name` VARCHAR(255) NULL AFTER `is_publish`, ADD INDEX (`album_name`);

-- 22 mai 2019 
CREATE TABLE `cms_album_cat` ( `id` INT NOT NULL AUTO_INCREMENT , `album_name` VARCHAR(255) NOT NULL , `create_date` DATETIME NULL , `create_by` VARCHAR(64) NULL , `update_date` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), UNIQUE (`album_name`)) ENGINE = InnoDB;
ALTER TABLE `cms_image` ADD `album` INT NOT NULL AFTER `is_publish`, ADD INDEX (`album`);
ALTER TABLE `cms_image` CHANGE `album` `album` INT(11) NULL;
update `cms_image` set album = null; 
ALTER TABLE `cms_image` ADD FOREIGN KEY (`album`) REFERENCES `cms_image`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
-- 23 mai 2019 
ALTER TABLE `cms_image` DROP FOREIGN KEY `cms_image_ibfk_1`; 
ALTER TABLE `cms_image` ADD CONSTRAINT `cms_image_ibfk_1` FOREIGN KEY (`album`) REFERENCES `cms_album_cat`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- Insertion categorie album 
INSERT INTO `cms_album_cat` (`id`, `album_name`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (NULL, 'Entree des classes', '2019-05-23 00:00:00', 'LOGIPAM', NULL, NULL), (NULL, 'Excursions', '2019-05-23 00:00:00', 'LOGIPAM', NULL, NULL);
INSERT INTO `cms_album_cat` (`id`, `album_name`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES (NULL, 'Graduation Kindergarden', '2019-05-23 00:00:00', 'LOGIPAM', NULL, NULL), (NULL, 'Graduation Philo', '2019-05-23 00:00:00', 'LOGIPAM', NULL, NULL);
ALTER TABLE `cms_image` DROP `album_name`;

-- Modification pour les sous menu 
ALTER TABLE `cms_menu` ADD `is_parent_menu` BOOLEAN NOT NULL AFTER `is_publish`, ADD INDEX (`is_parent_menu`);
ALTER TABLE `cms_menu` ADD `parent_menu` INT NULL AFTER `is_parent_menu`, ADD INDEX (`parent_menu`);
ALTER TABLE `cms_menu` ADD FOREIGN KEY (`parent_menu`) REFERENCES `cms_menu`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- modification des menus -- May be we need to skip this part and leave all menu like their are 
UPDATE `cms_menu` SET `is_parent_menu` = '1' WHERE `cms_menu`.`id` = 6;
UPDATE `cms_menu` SET `is_parent_menu` = '1' WHERE `cms_menu`.`id` = 7;
UPDATE `cms_menu` SET `is_parent_menu` = '1' WHERE `cms_menu`.`id` = 3;

ALTER TABLE `cms_menu` CHANGE `menu_position` `menu_position` INT(11) NULL;
UPDATE `cms_menu` SET `is_publish` = '1' WHERE `cms_menu`.`id` = 3;

-- 5 juin 2019 
ALTER TABLE `cms_menu` CHANGE `is_parent_menu` `is_parent_menu` TINYINT(1) NULL;

-- Mise a jour de la table cms_menu 
INSERT INTO `cms_menu` (`id`, `menu_label`, `menu_position`, `is_home`, `is_publish`, `is_parent_menu`, `parent_menu`, `date_create`, `date_update`, `create_by`, `update_by`)
 VALUES (NULL, 'Menu 1', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'LOGIPAM', NULL), 
(NULL, 'Menu 2', NULL, NULL, NULL, NULL, NULL, '2019-06-05 00:00:00', NULL, 'LOGIPAM', NULL);

-- 7 juin 2019 
Update general_config SET name = "Nom complémentaire de l'établissement" WHERE item_name = 'devise_school'; 
Update general_config SET name = "Devise de l'établissement" WHERE item_name = 'slogan'; 

-- 12 juin 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'is_admission_open', "Ouverture de l\'admission", '0', "0: La période d\'admission est fermée /\r\n1: La période d\'admission est ouverte", NULL, 'portal', '2019-06-12 00:00:00', NULL, 'LOGIPAM', NULL);

-- 17 juin 2019 
ALTER TABLE `postulant` CHANGE `previous_level` `previous_level` INT(11) NULL;
ALTER TABLE `postulant` CHANGE `blood_group` `blood_group` VARCHAR(10) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `birthday` `birthday` DATE NULL, CHANGE `adresse` `adresse` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `phone` `phone` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `health_state` `health_state` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `person_liable` `person_liable` VARCHAR(100) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `person_liable_phone` `person_liable_phone` VARCHAR(65) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `person_liable_adresse` `person_liable_adresse` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `previous_school` `previous_school` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `school_date_entry` `school_date_entry` DATE NULL, CHANGE `status` `status` INT(11) NULL, CHANGE `academic_year` `academic_year` INT(11) NULL, CHANGE `date_created` `date_created` DATETIME NULL, CHANGE `date_updated` `date_updated` DATETIME NULL, CHANGE `create_by` `create_by` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL, CHANGE `update_by` `update_by` VARCHAR(45) CHARACTER SET utf8 COLLATE utf8_general_ci NULL;

-- 20 juin 2019 
ALTER TABLE `postulant` ADD `email` VARCHAR(128) NULL AFTER `phone`, ADD INDEX (`email`);
ALTER TABLE `postulant` ADD `citizenship` VARCHAR(128) NULL AFTER `email`, ADD INDEX (`citizenship`);
ALTER TABLE `custom_field_data` ADD `object_type` VARCHAR(12) NULL AFTER `object_id`, ADD INDEX (`object_type`);

-- 21 juin 2019
UPDATE `cms_menu` SET `is_parent_menu` = NULL WHERE  menu_label = "Accueil";

-- 25 juin 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_relay_host', "Hote de l\'email de relais", 'mail.slogipam.com', 'Email de relais pour lancer des mails par SMTP.', NULL, 'portal', '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'email_relay_username', 'Nom utilisateur email de relais', 'sigessystem@slogipam.com', 'Nom utilisateur email de relais. Pour un compte gmail saisir votre email.', NULL, 'portal', '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'host_relay_password', 'Mot de passe du relais email', 'VilokanVulcan007#', 'Mot de passe du relais email, pour gmail, votre mot de passe gmail', NULL, 'portal', '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'host_relay_port', 'Port du relais mail', '465', 'Pour gmail.com 587', NULL, 'portal', '2019-06-25 00:00:00', NULL, 'LOGIPAM', NULL);

-- 27 juin 2019 
INSERT INTO `cms_menu` (`id`, `menu_label`, `menu_position`, `is_home`, `is_publish`, `is_parent_menu`, `parent_menu`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, "Condition d\'admission", '0', '1', '1', NULL, NULL, '2019-06-27 00:00:00', NULL, 'LOGIPAM', NULL);
ALTER TABLE `cms_menu` ADD `is_special` BOOLEAN NULL AFTER `parent_menu`, ADD INDEX (`is_special`);
UPDATE `cms_menu` SET `is_special` = '1' WHERE `cms_menu`.`menu_label` = "Condition d'admission";

-- 30 juin 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'instruction_admission', 'Instruction admission', "Veuillez imprimer ce formulaire d\'inscription <strong>en double (2) exemplaires</strong>, puis passer au {school_name} sise au <strong>{school_address}</strong> pour payer les frais d\'inscriptions avant le \r\n<strong>{date_limite}</strong>", "Instruction pour le proicessus d\'inscription en ligne.", NULL, 'portal', '2019-06-30 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'admission_pieces', 'Pieces requises admission', "Ici il y aura la liste des peices requise pour l\'admission. \r\n", "Liste des pieces requises pour admission", NULL, 'portal', '2019-06-30 00:00:00', NULL, 'LOGIPAM', NULL);

-- 02 juillet 2019
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'template_email_admission', 'Message email de reception admission', 'Merci {personne_responsable} d\'avoir inscrit votre enfant {nom_postulant} pour admision en classe de {classe_admission} en date du {date_inscription} dans notre etablissement. Pour completer le processus d\'inscription veuillez passer a l\'adresse de l\'ecole {school_name}, sis a {school_address} avec le formulaire d\'inscription que vous avez imprim&eacute;. il est possible de re-imprimer en cliquant sur ce lien {link_form}. \r\nConserver le numero d\'inscription <strong>{admission_number}</strong>. \r\n-- Cette partie du texte doit etre modifiée ou enlevée\r\nL\'ecole peut ajouter la date de d\'examen, diverses conditions de validation des inscriptions, la liste des pieces requises etc...', 'Template de l\'email pour la reception de l\'admission. ', NULL, 'portal', '2019-07-02 00:00:00', NULL, 'LOGIPAM', NULL);
UPDATE `general_config` SET `item_value` = "Cher(e) {personne_responsable},\r\nMerci d\'avoir inscrit votre enfant {nom_postulant} en classe de {classe_admission} en date du {date_inscription} dans notre établissement. Pour compléter le processus d\'inscription, veuillez passer à la direction de l\'école {school_name}, à l’adresse {school_address}, muni du formulaire d\'inscription dûment rempli que vous avez imprim&eacute;. Il est possible de réimprimer le formulaire en cliquant sur ce lien {link_form}. \r\nConservez précieusement le numéro d\'inscription <strong>{admission_number}</strong>, car il sera utilisé comme code d’identifiant lors des publications des résultats sur notre site.\r\nAttention. Il est extrêmement important de compléter l’inscription à la direction de l’école dans les 5 jours ouvrables qui suivent, sinon votre application en ligne sera rejetée. Il vous faudra recommencer le processus.\r\nListe de pièces à apporter au moment de l’inscription\r\n•	Acte de naissance ou extrait des archives (original et copie)\r\n•	Deux photos d’identité de date récente\r\n•	Bulletin de la dernière étape du dernier établissement scolaire fréquenté\r\n•	Certificat médical attestant les vaccinations obligatoires, délivré par le pédiatre de l’enfant\r\n•	Frais d’inscription de  x gourdes." WHERE item_name = 'template_email_admission';

-- 09 juillet 2019 
ALTER TABLE `postulant` ADD `is_validate` BOOLEAN NULL AFTER `academic_year`, ADD `is_online` BOOLEAN NULL AFTER `is_validate`;
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_subscription_billing', "Afficher Econamat dans l\'inscription", '0', "0 - Ne pas afficher l\'economat dans l\'inscription\r\n1 - Afficher l\'economat dans l\'inscription", "0 - Ne pas afficher l\'economat dans l\'inscription\r\n1 - Afficher l\'economat dans l\'inscription", 'sys', '2019-07-09 00:00:00', NULL, 'LOGIPAM', NULL);

-- 13 juillet 2019 

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'publish_admission_result', 'Publication résultats admission', '0', '0: Ne pas publier les résultats sur le site web\r\n1: Publier les résultats sur le site web', '0: Do not publish on web site\r\n1: Publish on web site', 'sys', '2019-07-13 00:00:00', NULL, 'LOGIPAM', NULL);

-- 13 juillet 2019 --
ALTER TABLE `payment_method` ADD `is_default` TINYINT(2) NOT NULL DEFAULT '0' AFTER `description`;

-- 17 juillet 2019 kindergarden module 
-- Active ou desactive le module kindergarden
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'kindergarden_module', 'Module Kindergarden', '0', '0 : Pas Activé\r\n1 : Activé', '0 : Not active\r\n1 : Active', 'sys', '2019-07-17 00:00:00', NULL, 'LOGIPAM', NULL);
-- Choix du nom de la section des classes du kindergarden
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'kindergarden_section', 'Nom section kindergarden', 'Prescolaire', 'Un nom pour la section kindergarden\r\nPar défaut : Prescolaire ', 'A name for the kindegarden section. \r\nDefault section : Prescolaire ', 'sys', '2019-07-17 00:00:00', NULL, 'LOGIPAM', NULL);

-- 18 juillet 2019
-- Kindergarden base de donnees 
CREATE TABLE `kinder_period` ( `id` INT NOT NULL AUTO_INCREMENT , `period_name` VARCHAR(32) NOT NULL , `academic_year` INT NOT NULL , `date_start` DATE NOT NULL , `date_end` DATE NOT NULL , `is_last_period` BOOLEAN NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`period_name`), INDEX (`academic_year`)) ENGINE = InnoDB;
ALTER TABLE `kinder_period` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `kinder_mention` ( `id` INT NOT NULL , `mention_name` VARCHAR(64) NOT NULL , `mention_short_name` VARCHAR(32) NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), UNIQUE (`mention_name`), UNIQUE (`mention_short_name`)) ENGINE = InnoDB;

CREATE TABLE `kinder_cat_mention` ( `id` INT NOT NULL , `cat_name` VARCHAR(128) NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
RENAME TABLE `kinder_cat_mention` TO `kinder_cat_concept`;
ALTER TABLE `kinder_cat_concept` ADD `cat_special` BOOLEAN NULL AFTER `cat_name`;

CREATE TABLE `kinder_concept` ( `id` INT NOT NULL , `concept_name` TEXT NOT NULL , `category` INT NOT NULL , `level` INT NOT NULL , `academic_year` INT NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`category`), INDEX (`level`), INDEX (`academic_year`)) ENGINE = InnoDB;
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`category`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_concept` ADD `teacher` INT NOT NULL AFTER `academic_year`, ADD INDEX (`teacher`);
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`teacher`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

CREATE TABLE `kinder_evaluation` ( `id` INT NOT NULL AUTO_INCREMENT , `student` INT NOT NULL , `concept` INT NOT NULL , `mention` INT NOT NULL , `free_comments` TEXT NULL , `from_special_cat` INT NULL , `academic_year` INT NOT NULL , `create_by` VARCHAR(64) NULL , `update_by` VARCHAR(64) NULL , `create_date` DATETIME NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`), INDEX (`student`), INDEX (`concept`), INDEX (`mention`), INDEX (`from_special_cat`), INDEX (`academic_year`)) ENGINE = InnoDB;
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`student`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`concept`) REFERENCES `kinder_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`from_special_cat`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `kinder_mention` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE kinder_concept DROP FOREIGN KEY kinder_concept_ibfk_2;
ALTER TABLE kinder_evaluation DROP FOREIGN KEY kinder_evaluation_ibfk_4;
ALTER TABLE `kinder_cat_concept` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;

ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`category`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`from_special_cat`) REFERENCES `kinder_cat_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE kinder_evaluation DROP FOREIGN KEY kinder_evaluation_ibfk_2;
ALTER TABLE `kinder_concept` CHANGE `id` `id` INT(11) NOT NULL AUTO_INCREMENT;
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`concept`) REFERENCES `kinder_concept`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;
ALTER TABLE `kinder_concept` ADD FOREIGN KEY (`level`) REFERENCES `levels`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `kinder_evaluation` ADD `period` INT NOT NULL AFTER `academic_year`, ADD INDEX (`period`);
ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`period`) REFERENCES `kinder_period`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `kinder_evaluation` ADD FOREIGN KEY (`mention`) REFERENCES `kinder_mention`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE `kinder_evaluation` CHANGE `mention` `mention` INT(11) NULL;
ALTER TABLE `kinder_evaluation` CHANGE `concept` `concept` INT(11) NULL; 

-- 24 juillet 2019 
INSERT INTO `modules` (`id`, `module_short_name`, `module_name`, `mod_lateral_menu`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES (NULL, 'kindergarden', 'Kindergarden', NULL, NULL, NULL, NULL, NULL);
INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '1', '14');

-- 27 juillet 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'presence_method', 'Methode de prise de présence', '1', '0 : Methode de prise de présence manuelle \r\n1 : Methode de présence par scanner et manuelle \r\n2 : Méthode de prise de retard \r\n', '0 : Methode de prise de présence manuelle \r\n1 : Methode de présence par scanner et manuelle \r\n2 : Méthode de prise de retard ', 'disc', '2019-07-27 00:00:00', NULL, 'LOGIPAM', NULL);
ALTER TABLE `record_presence` CHANGE `date_record` `date_record` DATETIME NOT NULL;
ALTER TABLE `record_presence` ADD `input_method` VARCHAR(255) NULL AFTER `comments`;
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'time_zone', 'Fuseau Horaire en PHP', 'America/New_York', 'Pour une liste complete visiter : https://www.php.net/manual/en/timezones.php\r\n\r\nPour les ameriques : \r\nhttps://www.php.net/manual/en/timezones.america.php\r\n', 'Pour une liste complete visiter : https://www.php.net/manual/en/timezones.php\r\n\r\nPour les ameriques : \r\nhttps://www.php.net/manual/en/timezones.america.php\r\n', 'sys', '2019-07-27 00:00:00', NULL, 'LOGIPAM', NULL);

-- 29 juillet 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'attendance_time', 'Heure de présence', '08:00', "Affiche l\'heure de presence au format : H:m (Exemple : 08:00 ou 16:00)", "Affiche l\'heure de presence au format : H:m (Exemple : 08:00 ou 16:00)", 'disc', '2019-07-29 00:00:00', NULL, 'LOGIPAM', NULL);

-- 31 juillet 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'message_late', 'Message de retard', 'Cher parent,\r\nPar cette note, la direction de {school_name} vous informe que votre enfant {student_name} actuellement scolarisé(e) en classe de {room} s’est présenté(e) en retard, soit à {time} en date du {date}. \r\nLa direction vous remercie de bien vouloir en discuter avec votre enfant et de prendre les mesures nécessaires pour éviter un cumul de retards susceptibles d’impacter ses notes disciplianires.\r\nEn vous remerciant de votre vigilance, la direction vous prie de croire, Cher parent, en ses salutations les plus respectueuses.\r\n', 'Note à envoyer au parent pour les retards non motivés', 'Note à envoyer au parent pour les retards non motivés', 'disc', '2019-07-31 00:00:00', NULL, 'LOGIPAM', NULL);

-- 3 Aout 2019 

INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '2', '14');
INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '3', '14');
INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '4', '14');
INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '5', '14');
INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '6', '14');
INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '7', '14');

INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES ('1', '14');
INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES ('6', '14');
INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES ('2', '14');

INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES ('3', '14');
INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES ('4', '14');
INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES ('5', '14');
INSERT INTO `groups_has_modules` (`groups_id`, `modules_id`) VALUES ('7', '14');


INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES (NULL, 'create', 'Créer evaluation kindergarden', 'KinderEvaluation', '14', NULL, NULL, NULL, NULL);
INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES (NULL, 'update', 'Modifier evaluation kindergarden', 'KinderEvaluation', '14', NULL, NULL, NULL, NULL);
INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES (NULL, 'list', 'Liste kindergarden', 'KinderEvaluation', '14', NULL, NULL, NULL, NULL);
INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES (NULL, 'index', 'Liste evaluation kindergarden', 'KinderEvaluation', '14', NULL, NULL, NULL, NULL);

INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES (NULL, 'index', 'Configuration Kindergarden', 'KinderPeriod', '14', NULL, NULL, NULL, NULL);

-- 08 Aout 2019
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'size_paper_kinder', 'Taille papier Kindergarden', 'Legal', 'Letter : papier 8.5*14\n Legal :papier 8.5*14\nLa taille du papier pour le bulletin du Kindergarden.', 'The size of paper for the report card for Kindergarden.', 'sys', '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'orientation_paper_kinder', 'Orientation papier kindergarden', 'P', 'P: Portrait\r\nL: Landscape', 'P: Portrait\r\nL: Landscape', 'sys', '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL);

-- 09 Aout 2019 
UPDATE general_config SET name = 'Couleur bordure de la carte', `description` = 'Definir la couleur des bordures de la carte', `english_comment` = 'Define border color of ID Card', `category` = 'sys', `date_update` = '2019-08-09 00:00:00', `update_by` = 'LOGIPAM' WHERE general_config.item_name = 'main_color';
UPDATE `general_config` SET `name` = 'Couleur de la ligne de la carte', `description` = 'Couleur de la ligne de la carte', `english_comment` = 'Color of the line on the ID Card' WHERE `general_config`.item_name = 'line_color';
UPDATE `general_config` SET `name` = 'Couleur arriere plan de la carte', `description` = 'Couleur arriere plan de la carte', `english_comment` = 'Background color of the ID Card', `category` = 'sys', `date_update` = '2019-08-09 00:00:00', `update_by` = 'LOGIPAM' WHERE `general_config`.item_name = 'balance_color';
UPDATE `general_config` SET `name` = 'Couleur secondaire de la carte', `description` = 'Couleur secondaire de la carte', `english_comment` = 'Secondary color of the ID Card', `category` = 'sys', `date_update` = '2019-08-09 00:00:00', `update_by` = 'LOGIPAM' WHERE `general_config`.item_name = 'secondary_color';
DELETE FROM `general_config` WHERE `general_config`.item_name = 'a_link'; 

-- 10 Aout 2019
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'library_display', 'Afficher bibliotheque', '0', "0: n\'afficher pas\r\n 1: Affiche", ' ', 'sys', '2019-08-08 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'orientation_paper_palmares', 'Orientation papier palmares', 'L', "P: Portrait\r\nL: Landscape", "P: Portrait\r\nL: Landscape", 'sys', '2019-08-08 00:00:00', '2019-08-08 00:00:00', 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'size_paper_palmares', 'Taille papier Palmares', 'Legal', 'Legal : papier 8.5*11\r\nLetter :papier 8.5*14\r\nLa taille du papier pour le palmares', 'The size of paper for gradesbook report', 'sys', '2019-08-08 00:00:00', '2019-08-08 00:00:00', 'LOGIPAM', NULL);



-- 14 Aout 2019
  INSERT INTO `users` ( `username`, `password`, `active`, `person_id`, `full_name`, `create_by`, `update_by`, `date_created`, `date_updated`, `profil`, `group_id`, `is_parent`, `user_id`, `last_ip`, `last_activity`) VALUES ('logipam_it', 'db96776d74500d8024ed2fadd44b6b12', 1, 0, 'Logipam IT', 'admin', NULL, '2018-09-15 00:00:00', '2018-09-15 00:00:00', 1, 1, NULL, 0, '190.115.178.135', '2019-01-22 00:13:43');
  INSERT INTO `users` ( `username`, `password`, `active`, `person_id`, `full_name`, `create_by`, `update_by`, `date_created`, `date_updated`, `profil`, `group_id`, `is_parent`, `user_id`, `last_ip`, `last_activity`) VALUES ('logipam', 'a0d86a602ec17a641850970ee71718d8', 1, 0, 'Logipam', 'admin', NULL, '2018-09-15 00:00:00', '2018-09-15 00:00:00', 1, 1, NULL, 0, '190.115.178.135', '2019-01-22 00:13:43');

-- 14 Aout 2019 
UPDATE `cms_menu` SET `is_special` = '1' WHERE menu_label = 'Accueil';

-- 18 Aout 2019
INSERT INTO `cms_menu` (`id`, `menu_label`, `menu_position`, `is_home`, `is_publish`, `is_parent_menu`, `parent_menu`, `is_special`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'Parent Menu', NULL, NULL, '0', '1', NULL, NULL, '2019-08-18 00:00:00', NULL, 'LOGIPAM', NULL);


-- 22 Aout 2019
-- pou module Communication
ALTER TABLE `general_config` ADD `visibility` INT(5) NOT NULL DEFAULT '0' AFTER `category`;
UPDATE `general_config` SET `visibility` = '1',`category` = 'dev' WHERE `general_config`.`item_name` = 'library_display';
UPDATE `general_config` SET `visibility` = '1',`category` = 'dev' WHERE `general_config`.`item_name` = 'display_homework_menu'; 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_communication', "Module communication", 0, '', NULL, 'dev',1, '2019-08-22 00:00:00', NULL, 'LOGIPAM', NULL);

-- 24 Aout 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'message_absent', 'Message absence', "Cher parent,\r\nPar cette note, la direction de {school_name} vous informe que votre enfant {student_name} actuellement scolarisé(e) en classe de {room} ne s’est présenté(e) en classe, soit à la date du {date}. \r\nLa direction vous remercie de bien vouloir en discuter avec votre enfant et de prendre les mesures nécessaires pour éviter un cumul d\'absence susceptibles d’impacter ses notes disciplianires.\r\nEn vous remerciant de votre vigilance, la direction vous prie de croire, Cher parent, en ses salutations les plus respectueuses.\r\n", 'Note à envoyer au parent pour les absences.', 'Note à envoyer au parent pour les absences.', 'disc', '0', '2019-08-24 00:00:00', NULL, 'LOGIPAM', NULL);

-- 02 Septembre 2019
-- Prendre presence dans les evenements speciaux
CREATE TABLE `record_special_presence` ( `id` INT NOT NULL AUTO_INCREMENT , `student` INT NOT NULL , `academic_year` INT NOT NULL , `date_record` DATETIME NOT NULL , `presence_type` INT NOT NULL , `date_created` DATETIME NOT NULL , `create_by` VARCHAR(64) NOT NULL , `date_updated` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `record_special_presence` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION; ALTER TABLE `record_special_presence` ADD FOREIGN KEY (`student`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE NO ACTION;

-- 03 septembre 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'special_attendance', 'Presence special', '1', '0: Pas de présence spéciale\r\n1: Présence spéciale ', '0: No special attendance\r\n1: Special attendance', 'disc','0', '2019-09-03 00:00:00', NULL, 'LOGIPAM', NULL);


-- 04 septembre 2019 Checkout 
CREATE TABLE `record_checkout` ( `id` INT NOT NULL AUTO_INCREMENT , `student` INT NOT NULL , `academic_year` INT NOT NULL , `date_record` DATETIME NOT NULL , `presence_type` INT NOT NULL , `date_created` DATETIME NOT NULL , `create_by` VARCHAR(64) NOT NULL , `date_updated` DATETIME NULL , `update_by` VARCHAR(64) NULL , PRIMARY KEY (`id`), INDEX (`student`), INDEX (`academic_year`)) ENGINE = InnoDB;
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'module_checkout', 'Activer checkout', '1', '0: Module checkout pas activé\r\n1: Module checkout activé', '0: Module checkout not active\r\n1: Module checkout active', 'disc', '2019-09-04 00:00:00', NULL, 'LOGIPAM', NULL);


-- 05 septembre 2019 
-- Structure de la table `appreciations_grid`
CREATE TABLE `appreciations_grid` (
  `id` int(11) NOT NULL,
  `section` int(11) NOT NULL,
  `start_range` float NOT NULL,
  `end_range` float NOT NULL,
  `comment` varchar(255) NOT NULL,
  `short_text` varchar(5) NOT NULL,    
  `academic_year` int(11) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL,
  `update_by` varchar(64) DEFAULT NULL,
  `create_date` datetime DEFAULT NULL,
  `update_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Index pour la table `appreciations_grid`
ALTER TABLE `appreciations_grid`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_academic_year` (`academic_year`),
  ADD KEY `section` (`section`);

-- AUTO_INCREMENT pour la table `appreciations_grid`
ALTER TABLE `appreciations_grid`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- Contraintes pour la table `appreciations_grid`
ALTER TABLE `appreciations_grid`
  ADD CONSTRAINT `fk_appreciations_grid_section` FOREIGN KEY (`section`) REFERENCES `sections` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_appreciations_grid_academicperiod` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

-- update table year_migration_check
ALTER TABLE `year_migration_check` ADD `appreciations_grid` INT(11) NOT NULL AFTER `reportcard_observation`;



-- 06 septembre 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'remarques_generales', 'Remarques générales', ' ', ' ', '', 'sys','0', '2019-09-06 00:00:00', NULL, 'LOGIPAM', NULL);

-- 07 septembre 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_portal', "Activer/Desactiver portail", 1, '', NULL, 'sys','0', '2019-09-07 00:00:00', NULL, 'LOGIPAM', NULL);


CREATE TABLE `devoirs` (
  `id` int(11) NOT NULL,
  `student` int(11) NOT NULL,
  `course` int(11) NOT NULL,
  `academic_period` int(11) NOT NULL,
  `grade_value` float DEFAULT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `date_created` datetime DEFAULT NULL,
  `date_updated` datetime DEFAULT NULL,
  `create_by` varchar(45) DEFAULT NULL,
  `update_by` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Index pour la table `devoirs`
ALTER TABLE `devoirs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_devoirs_persons_student` (`student`),
  ADD KEY `fk_devoirs_courses` (`course`),
  ADD KEY `fk_devoirs_academicperiods` (`academic_period`);

-- AUTO_INCREMENT pour la table `devoirs`
ALTER TABLE `devoirs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1;

-- Contraintes pour la table `devoirs`
ALTER TABLE `devoirs`
  ADD CONSTRAINT `fk_devoirs_course` FOREIGN KEY (`course`) REFERENCES `courses` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_devoirs_academicperiods_` FOREIGN KEY (`academic_period`) REFERENCES `academicperiods` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `fk_devoirs_persons_students` FOREIGN KEY (`student`) REFERENCES `persons` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;


-- 08 septembre 2019 
ALTER TABLE `record_checkout` ADD FOREIGN KEY (`student`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `record_checkout` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- 09 septembre 2019 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_homework_grade_menu', "Activer/Desactiver notes devoir", 1, '0: N\afficher pas le menu notes devoir; 1: Afficher le menu notes devoir', NULL, 'dev','1', '2019-09-07 00:00:00', NULL, 'LOGIPAM', NULL);

ALTER TABLE `courses` ADD `homework_weight` FLOAT NULL DEFAULT NULL AFTER `weight`;



-- 11 septembre 2019
UPDATE `groups` SET `group_name` = 'Surveillant' WHERE `groups`.`id` = 18;

-- 14 septembre 2019 
ALTER TABLE `kinder_concept` CHANGE `teacher` `teacher` INT(11) NULL;
ALTER TABLE kinder_concept DROP FOREIGN KEY kinder_concept_ibfk_3;



-- 14 septembre 2019   
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'signature_au', "Signature dans la carte", 0, '0: N\afficher pas la signature dans la carte; 1: Afficher la signature dans la carte', NULL, 'dev','1', '2019-09-14 00:00:00', NULL, 'LOGIPAM', NULL);

-- 15 septembre 2019 pour la cration 
CREATE TABLE `kinder_teacher` ( `id` INT NOT NULL AUTO_INCREMENT , `teacher` INT NOT NULL , `room` INT NOT NULL , `academic_year` INT NOT NULL , `new_teacher` INT NULL , `create_by` VARCHAR(255) NULL , `create_date` DATETIME NULL , `update_by` VARCHAR(255) NULL , `update_date` DATETIME NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;
ALTER TABLE `kinder_teacher` ADD FOREIGN KEY (`teacher`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_teacher` ADD FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_teacher` ADD FOREIGN KEY (`room`) REFERENCES `rooms`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT; ALTER TABLE `kinder_teacher` ADD FOREIGN KEY (`new_teacher`) REFERENCES `persons`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;

-- 17 Septembre 2019 
UPDATE `cms_menu` SET `is_special` = '1' WHERE menu_label = 'Accueil';


-- 18 septembre 2019   
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_average_class', "Moyenne classe dans le bulletin", 1, '0: N\afficher pas la moyenne classe dans le bulletin; 1: Afficher la moyenne classe dans le bulletin', NULL, 'dev','1', '2019-09-18 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_student_stage', "Niveau de performance dans le bulletin", 1, '0: N\afficher pas le niveau de performance dans le bulletin; 1: Afficher le iveau de performance dans le bulletin', NULL, 'dev','1', '2019-09-18 00:00:00', NULL, 'LOGIPAM', NULL);
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_total_in_subjectparent', "Total des sous matières dans le bulletin", 0, '0: N\afficher pas le total des sous matières dans le bulletin; 1: Afficher le total des sous matières dans le bulletin', NULL, 'dev','1', '2019-09-18 00:00:00', NULL, 'LOGIPAM', NULL);

-- 19 Septembre 2019 
-- Uniquement pour le college Bird 
INSERT INTO `disc_period_break` (`id`, `id_period`, `break_point`, `academic_year`, `differentiel`) VALUES (NULL, '11', '80', '1', '0'), (NULL, '12', '100', '1', '20'), (NULL, '13', '130', '1', '30'), (NULL, '14', '160', '1', '30');
UPDATE `disc_period_break` SET `academic_year` = '6' WHERE `disc_period_break`.`id` = 8;
UPDATE `disc_period_break` SET `academic_year` = '6' WHERE `disc_period_break`.`id` = 7;
UPDATE `disc_period_break` SET `academic_year` = '6' WHERE `disc_period_break`.`id` = 6;
UPDATE `disc_period_break` SET `academic_year` = '6' WHERE `disc_period_break`.`id` = 5;

-- Poiur tous les autres a rouler sur toutes les bases 
UPDATE `general_config` SET `description` = 'Letter : papier 8.5*11\r\nLegal :  papier 8.5*14\r\nLa taille du papier pour le bulletin du Kindergarden.' WHERE  item_name = 'size_paper_kinder';
UPDATE `general_config` SET `description` = 'Letter : papier 8.5*11\r\nLegal :papier 8.5*14\r\nLa taille du papier pour le palmares' WHERE item_name = 'size_paper_palmares';

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'size_paper_bulletin', 'Taille papier bulletin', 'Letter', 'Legal : papier 8.5*14\r\nLetter :papier 8.5*11\r\nLa taille du papier pour le bulletin', 'The size of paper for reportcard', 'sys', 1, '2019-08-08 00:00:00', '2019-08-08 00:00:00', 'LOGIPAM', NULL);

-- 20 Septembre 2019
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'top_content_color', 'Couleur au dessus du contenu de la carte', 'red', 'Definir la couleur au dessus du contenu de la carte', 'Define color top content of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'top_top_content_color', 'Couleur au dessus du dessus du contenu de la carte', 'red', 'Definir la couleur au dessus du dessus du contenu de la carte', 'Define color top of the top content of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'bottom_content_color', 'Couleur au dessous du contenu de la carte', 'red', 'Definir la couleur au dessous du contenu de la carte', 'Define color bottom content of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'bottom_bottom_content_color', 'Couleur au bas du dessous du contenu de la carte', 'red', 'Definir la couleur au bas du dessous du contenu de la carte', 'Define color below bottom content of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'left_right_color', 'Couleur côté gauche et droit de la carte', 'red', 'Definir la couleur du côté gauche et du côté droit de la carte', 'Define side color left and right of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'main_corner_color', 'Couleur des coins de la carte', 'red', 'Definir la couleur des coins de la carte', 'Define color main corner of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'inside_corner_color', 'Couleur du bout intérieur horizontal des coins de la carte', 'red', 'Definir la couleur du bout intérieur horizontal des coins de la carte', 'Define color horizontal inside end of main corner of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');

-- 21 Septembre 2019
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'inside_vertical_corner_color', 'Couleur du bout intérieur vertical des coins de la carte', 'red', 'Definir la couleur du bout intérieur vertical des coins de la carte', 'Define color vertical inside end of main corner of ID Card', 'dev', 1, NULL, '2019-09-21 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'left_right_center_color', 'Couleur côté central gauche et droit de la carte', 'red', 'Definir la couleur du côté central gauche et du côté central droit de la carte', 'Define center side color left and right of ID Card', 'dev', 1, NULL, '2019-09-20 00:00:00', NULL, 'LOGIPAM');


-- 29 Septembre 2019 
UPDATE `general_config` SET `name` = 'Numero template de la carte' WHERE  item_name = 'idcard_template';

-- 11 Octobre 2019
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_guest_dashboard', 'Dashboard pour parents et élèves', '1', '0: n affiche pas le dashboard pour parents et élèves; 1:affiche pas le dashboard pour parents et élèves' , '', 'sys', 0, NULL, '2019-10-11 00:00:00', NULL, 'LOGIPAM');

CREATE TABLE `audit_log` ( `id` INT NOT NULL AUTO_INCREMENT , `id_element` VARCHAR(255) NOT NULL , `name_element` LONGTEXT NOT NULL , `element_type` VARCHAR(255) NOT NULL , `action_type` VARCHAR(255) NOT NULL , `date_action` TIMESTAMP NOT NULL , PRIMARY KEY (`id`)) ENGINE = InnoDB;

-- 16 Decembre 2019

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'report_each_eval_in_period', 'Produire bulletin pour chaque évaluation de la période', '1', '0: Ne pas produire de bulletin pour chaque évaluation de la période; 1: Produire de bulletin pour chaque évaluation de la période' , '', 'sys', 0, NULL, '2019-10-11 00:00:00', NULL, 'LOGIPAM');


-- 20 Janvier 2020
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'ns_reportcard', 'Bulletin Nouveau S', '2', '0: Bulletin Nouveau Secondaire; 1: Bulletin Hicus; 2: Bulletin stjn' , '', 'sys', 0, NULL, '2020-01-20 00:00:00', NULL, 'LOGIPAM');

-- 23 janvier 2020
ALTER TABLE `custom_field` ADD `order_field` INT NULL AFTER `field_option`;


-- 29 janvier 2020
ALTER TABLE `room_has_person` ADD `titulaire` INT(11) NULL DEFAULT NULL AFTER `academic_year`;
ALTER TABLE `room_has_person` ADD INDEX `fk_room_person_persons` (`titulaire`);

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_room_reportcard', 'Afficher la salle de classe dans le bulletin ', '1', '0: Ne pas afficher la salle dans le bulletin; 1: Afficher la salle dans le bulletin' , '', 'sys', 0, NULL, '2020-01-29 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_periodDate_reportcard', 'Afficher la date de la période dans le bulletin', '0', '0: Ne pas afficher la date de la période dans le bulletin; 1: Afficher la date de la période dans le bulletin' , '', 'sys', 0, NULL, '2020-01-29 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_titulaire_reportcard', 'Afficher le titulaire de la salle dans le bulletin', '0', '0: Ne pas afficher le titulaire de la salle dans le bulletin; 1: Afficher le titulaire de la salle dans le bulletin' , '', 'sys', 0, NULL, '2020-01-29 00:00:00', NULL, 'LOGIPAM');
-- INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_school_website', 'Afficher le site web dans l\'entête des pages', '0', '0: Ne pas afficher le site web dans l\'entête des pages; 1: Afficher le site web dans l\'entête des pages' , '', 'sys', 0, NULL, '2020-01-29 00:00:00', NULL, 'LOGIPAM');


--30 janvier 2020 
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'classe_inscription', 'Classe Inscription', NULL, 'Saisir les id des classes d\'inscription separe par une virgule. 
Exemple : 2,3,6
Il s\'agit des id de la table levels. ', NULL, 'sys', '0', '2020-01-30 00:00:00', NULL, 'LOGIPAM', NULL);

-- 04 fevrier 2020
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_code_card', 'Afficher le code dans la carte', '0', '0: Ne pas afficher le code dans la carte; 1: Afficher le code dans la carte' , '', 'sys', 0, NULL, '2020-01-29 00:00:00', NULL, 'LOGIPAM');

-- 11 fevrier 2020
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'include_class_average', 'Inclure la moyenne de la classe dans le bulletin', '0', '0: Ne pas inclure la moyenne de la classe; 1: Inclure la moyenne de la classe' , '', 'sys', 0, NULL, '2020-02-11 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'code_institution', 'Code système de linstitution', '', 'Code système de linstitution' , '', 'sys', 1, NULL, '2020-02-11 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'include_titulaire_signature', 'Inclure la signature du titulaire dans le bulletin', '0', '0 : Ne pas Inclure la signature du titulaire dans le bulletin; 1 : Inclure la signature du titulaire dans le bulletin' , '', 'sys', 0, NULL, '2020-02-11 00:00:00', NULL, 'LOGIPAM');


-- #############################################################################################################
-- mars
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_total_in_subjectparent', 'Inclure la note max des matières parentes dans le bulletin', '0', '0 : Ne pas Inclure la note max des matières parentes dans le bulletin; 1 : Inclure la note max des matières parentes dans le bulletin' , '', 'sys', 0, NULL, '2020-02-11 00:00:00', NULL, 'LOGIPAM');
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_total_close_subject', 'Inclure la note max à côté des matières dans le bulletin', '0', '0 : Ne pas Inclure la note max à côté des matières dans le bulletin; 1 : Inclure la note max à côté des matières dans le bulletin' , '', 'sys', 0, NULL, '2020-02-11 00:00:00', NULL, 'LOGIPAM');

-- 10 mars 2020
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'manual_behavior_grade', 'Note discipline manuelle dans le bulletin(format 1)', '0', '0 : La note vient automatiquement des infractions; 1 : On saisie la note manuellement dans le bulletin' , '', 'sys', 0, NULL, '2020-02-11 00:00:00', NULL, 'LOGIPAM');

-- 15 mars 2020
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `visibility`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'reportcard_format_by_cycle', 'Format des bulletins par cycle)', '', 'Parametrage du format des bulletins par cycle. Utiliser cette notation " Stucture,Nouveau secondaire,Id cycle " pour un cycle; on mettra un "/", sans laisser d\'espace, pour ajouter les données pour un autre cycle' , '', 'sys', 0, NULL, '2020-02-11 00:00:00', NULL, 'LOGIPAM');



ALTER TABLE `average_by_period` ADD `max_grade` DOUBLE NOT NULL DEFAULT '0' AFTER `sum`;
INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'display_summary_for_subjects', 'Afficher le sommaire des matieres dans le bulletin', '1', '0: ne pa afficher sommaire pour les matieres; 1: affichersommaire pour les matieres;', '', NULL, NULL, NULL, NULL, NULL);

-- 23/10/2020
INSERT INTO `actions` (`id`, `action_id`, `action_name`, `controller`, `module_id`, `create_date`, `update_date`, `create_by`, `update_by`) VALUES (NULL, 'temporary', 'Imprimer les cartes temporaires', 'Idcard', '13', NULL, NULL, NULL, NULL);

