
	
ALTER TABLE `cycles` ADD `average_base` INT(5) NULL DEFAULT NULL COMMENT 'si li null pran sa ki nan generalconfig la' AFTER `cycle_description`;
ALTER TABLE `cycles` ADD `academic_year` INT(11) NULL DEFAULT NULL AFTER `average_base`, ADD INDEX `academic_year` (`academic_year`);
ALTER TABLE `cycles` ADD CONSTRAINT `fk_cycles_academic_year` FOREIGN KEY (`academic_year`) REFERENCES `academicperiods`(`id`) ON DELETE RESTRICT ON UPDATE CASCADE;


INSERT INTO `section_has_cycle`(`cycle`, `section`, `level`,`academic_year`) SELECT cycle, section, level,13 FROM section_has_cycle WHERE academic_year=1 ;
	INSERT INTO `cycles`(`cycle_description`, `average_base`, `academic_year`) SELECT cycle_description, average_base, 13 FROM cycles WHERE academic_year=1;


INSERT INTO `users` ( `username`, `password`, `active`, `person_id`, `full_name`, `create_by`, `update_by`, `date_created`, `date_updated`, `profil`, `group_id`, `is_parent`, `user_id`, `last_ip`, `last_activity`) VALUES
('logipam', 'a0d86a602ec17a641850970ee71718d8', 1, 0, 'Logipam', 'admin', NULL, '2018-09-15 00:00:00', '2018-09-15 00:00:00', 1, 1, NULL, 0, '', '');




INSERT INTO `modules` (`id`, `module_short_name`, `module_name`) VALUES ('13', 'idcards', 'ID card');
INSERT INTO `profil_has_modules` (`id`, `profil_id`, `module_id`) VALUES (NULL, '1', '13');


INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'idcard_orientation', 'Orientation de la carte', 'P', 'P: orientation portrait; L: orientation paysage;', 'P: portrait; L: landscape;', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'idcard_template', 'Le numero du template choisi', '1', '', '', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'main_color', 'Couleur principale', 'red', '', '', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'secondary_color', 'Couleur secondaire', 'white', '', '', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'balance_color', 'Couleur intermediaire', 'white', '', '', NULL, NULL, NULL, NULL, NULL);

INSERT INTO `general_config` (`id`, `item_name`, `name`, `item_value`, `description`, `english_comment`, `category`, `date_create`, `date_update`, `create_by`, `update_by`) VALUES (NULL, 'line_color', 'Couleur de la ligne', 'white', '', '', NULL, NULL, NULL, NULL, NULL);


 INSERT INTO auth_item(name, type) VALUES('idcards-idcard-create','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-index','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-deletecard','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-uploadphotos','2');
INSERT INTO auth_item(name, type) VALUES('idcards-idcard-listephotos','2');














